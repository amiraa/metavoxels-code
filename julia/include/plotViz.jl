using Plots

##############Final Displacement#####################
function plotFinalDisplacement(name)
    setupViz=getSetup1("$(name)/0")
    maxNumFiles=setupViz["maxNumFiles"]
    setupVizLast=getSetup1("$(name)/$(Int(maxNumFiles-1))");
    nodes=setupVizLast["nodes"]
    disX=[]
    disY=[]
    disZ=[]
    for node in nodes
        append!(disX,[node["displacement"]["x"]])
        append!(disY,[node["displacement"]["y"]])
        append!(disZ,[node["displacement"]["z"]])
    end
    display(Plots.scatter(disX,title="final x displacement",label="",ylabel="displacement",xlabel="node ID"))
    display(Plots.scatter(disY,title="final y displacement",label="",ylabel="displacement",xlabel="node ID")) 
    display(Plots.scatter(disZ,title="final z displacement",label="",ylabel="displacement",xlabel="node ID")) 
end

function plotFinalDisplacement(name,axis)
    setupViz=getSetup1("$(name)/0")
    maxNumFiles=setupViz["maxNumFiles"]
    setupVizLast=getSetup1("$(name)/$(Int(maxNumFiles-1))");
    nodes=setupVizLast["nodes"]
    dis=[]
    for node in nodes
        append!(dis,[node["displacement"][axis]])
    end
    display(Plots.scatter(dis,title="final $(axis) displacement",label="",ylabel="displacement",xlabel="node ID"))
end

function getFinalDisplacement(name,id,axis)
    setupViz=getSetup1("$(name)/0")
    maxNumFiles=setupViz["maxNumFiles"]
    setupVizLast=getSetup1("$(name)/$(Int(maxNumFiles-1))");
    node=setupVizLast["nodes"][id]
    return node["displacement"][axis]
end

function plotFinalDisplacementFEA(setup,name)
    nodes=setup["nodes"]
    disXFEA=[]
    disYFEA=[]
    disZFEA=[]
    for node in nodes
        append!(disXFEA,[node["displacement"]["x"]])
        append!(disYFEA,[node["displacement"]["y"]])
        append!(disZFEA,[node["displacement"]["z"]])
    end

    setupViz=getSetup1("$(name)/0")
    maxNumFiles=setupViz["maxNumFiles"]
    setupVizLast=getSetup1("$(name)/$(Int(maxNumFiles-1))");
    nodes=setupVizLast["nodes"]
    disX=[]
    disY=[]
    disZ=[]
    for node in nodes
        append!(disX,[node["displacement"]["x"]])
        append!(disY,[node["displacement"]["y"]])
        append!(disZ,[node["displacement"]["z"]])
    end
    Plots.scatter(disX,title="final x displacement",label="//",ylabel="displacement",xlabel="node ID",legend = :outertopright)
    display(Plots.scatter!(disXFEA,title="final x displacement",label="FEA",ylabel="displacement",xlabel="node ID",alpha = 0.5,markershape  =:utriangle,legend = :outertopright))

    Plots.scatter(disY,title="final y displacement",label="//",ylabel="displacement",xlabel="node ID",legend = :outertopright)
    display(Plots.scatter!(disYFEA,title="final y displacement",label="FEA",ylabel="displacement",xlabel="node ID",alpha = 0.5,markershape  =:utriangle,legend = :outertopright)) 

    Plots.scatter(disZ,title="final z displacement",label="//",ylabel="displacement",xlabel="node ID",legend = :outertopright)
    display(Plots.scatter!(disZFEA,title="final z displacement",label="FEA",ylabel="displacement ",xlabel="node ID",alpha = 0.5,markershape  =:utriangle,legend = :outertopright)) 
    return disX,disXFEA,disY,disYFEA,disZ,disZFEA
end

function plotFinalDisplacementComparison(setup1,setup2,setup3)
    nodes1=setup1["nodes"]
    disX1=[]
    disY1=[]
    disZ1=[]
    pX1=[]
    pY1=[]
    pZ1=[]
    for node in nodes1
        if node["position"]["y"]/3.75<20
            append!(disX1,[node["displacement"]["x"]/3.75])
            append!(disY1,[node["displacement"]["y"]/3.75])
            append!(disZ1,[node["displacement"]["z"]/3.75])
            append!(pX1,[node["position"]["x"]/3.75])
            append!(pY1,[node["position"]["y"]/3.75])
            append!(pZ1,[node["position"]["z"]/3.75])
        end
    end
    nodes2=setup2["nodes"]
    disX2=[]
    disY2=[]
    disZ2=[]
    pX2=[]
    pY2=[]
    pZ2=[]
    for node in nodes2
        if node["position"]["y"]<20
                append!(disX2,[node["displacement"]["x"]/setupViz["scale"]])
                append!(disY2,[node["displacement"]["y"]/setupViz["scale"]])
                append!(disZ2,[node["displacement"]["z"]/setupViz["scale"]])
                append!(pX2,[node["position"]["x"]/setupViz["scale"]])
                append!(pY2,[node["position"]["y"]/setupViz["scale"]])
                append!(pZ2,[node["position"]["z"]/setupViz["scale"]])
        end
    end

    nodes3=setup3["nodes"]
    disX3=[]
    disY3=[]
    disZ3=[]
    pX3=[]
    pY3=[]
    pZ3=[]
    for node in nodes3
        if  haskey(setup3,"multiscale")
            if node["position"]["y"]<20
                if node["scale"]!=2
                    append!(disX3,[node["displacement"]["x"]/setupViz["scale"]])
                    append!(disY3,[node["displacement"]["y"]/setupViz["scale"]])
                    append!(disZ3,[node["displacement"]["z"]/setupViz["scale"]])
                    append!(pX3,[node["position"]["x"]/setupViz["scale"]])
                    append!(pY3,[node["position"]["y"]/setupViz["scale"]])
                    append!(pZ3,[node["position"]["z"]/setupViz["scale"]])
                end
            end
        end
    end
    Plots.scatter(pX1,disX1,title="final x displacement",label="",ylabel="displacement",xlabel="pos X")
    Plots.scatter!(pX2,disX2,title="final x displacement",label="",alpha = 0.3,markershape  =:utriangle)
    display(scatter!(pX3,disX3,title="final x displacement",label="",alpha = 0.3,markershape  =:rect))

    Plots.scatter(pX1,disY1,title="final y displacement",label="Oriented",ylabel="displacement",xlabel="pos X",alpha = 0.6)
    Plots.scatter!(pX2,disY2,title="final y displacement",label="Hierarchical",alpha = 0.6,markershape  =:utriangle)
    display(Plots.scatter!(pX3,disY3,title="final y displacement",label="Multiscale",alpha = 0.6,markershape  =:rect)) 

    Plots.scatter(pX1,disZ1,title="final z displacement",label="",ylabel="displacement",xlabel="pos X")
    Plots.scatter!(pX2,disZ2,title="final z displacement",label="",alpha = 0.3,markershape  =:utriangle)
    display(Plots.scatter!(pX3,disZ3,title="final z displacement",label="",alpha = 0.3,markershape  =:rect)) 

end
#################DisplacementTroughTimeSteps##################
function plotDisplacementTroughTimeSteps(name,id)
    setupViz=getSetup1("$(name)/0")
    maxNumFiles=setupViz["maxNumFiles"]
    numTimeSteps=setupViz["numTimeSteps"]
    x=(1:maxNumFiles).*floor(numTimeSteps/maxNumFiles)
    
    disX=[]
    disY=[]
    disZ=[]
    for i in 0:(maxNumFiles-1)
        setupViz=getSetup1("$(name)/$(Int(i))");
        node=setupViz["nodes"][id]
        append!(disX,[node["displacement"]["x"]/setupViz["scale"]])
        append!(disY,[node["displacement"]["y"]/setupViz["scale"]])
        append!(disZ,[node["displacement"]["z"]/setupViz["scale"]])

    end
    display(Plots.plot(x,disX,title="x displacement",label="",ylabel="displacement",xlabel="timestep"))
    display(Plots.plot(x,disY,title="y displacement",label="",ylabel="displacement",xlabel="timestep"))
    display(Plots.plot(x,disZ,title="z displacement",label="",ylabel="displacement",xlabel="timestep"))
    return disX,disY,disZ
end

function plotDisplacementTroughTimeSteps(name,id,axis,skip)
    setupViz=getSetup1("$(name)/0")
    maxNumFiles=setupViz["maxNumFiles"]
    numTimeSteps=setupViz["numTimeSteps"]
    x=(1:maxNumFiles-skip).*floor(numTimeSteps/maxNumFiles)
    
    dis=[]
    for i in 0:(maxNumFiles-1-skip)
        setupViz=getSetup1("$(name)/$(Int(i))");
        node=setupViz["nodes"][id]
        append!(dis,[node["displacement"][axis]/setupViz["scale"]])
    end
    setupViz=getSetup1("$(name)/0")
    x=x.*setupViz["dt"]
    display(Plots.plot(x,dis,title="$(axis) displacement",label="",ylabel="displacement",xlabel="timestep"))
    return dis
end

function plotDisplacementTroughTimeSteps(name)
    setupViz=getSetup1("$(name)/0")
    maxNumFiles=setupViz["maxNumFiles"]
    numTimeSteps=setupViz["numTimeSteps"]
    x=(1:maxNumFiles).*floor(numTimeSteps/maxNumFiles)
    
    disX=[]
    disY=[]
    disZ=[]
    for i in 0:(maxNumFiles-1)
        setupViz=getSetup1("$(name)/$(Int(i))");
        node=setupViz["nodes"][end]
        append!(disX,[node["displacement"]["x"]/setupViz["scale"]])
        append!(disY,[node["displacement"]["y"]/setupViz["scale"]])
        append!(disZ,[node["displacement"]["z"]/setupViz["scale"]])

    end
    setupViz=getSetup1("$(name)/0")
    x=x.*setupViz["dt"]
    display(Plots.plot(x,disX,title="x displacement",label="",ylabel="displacement",xlabel="time (sec)",legend = :outertopright))
    display(Plots.plot(x,disY,title="y displacement",label="",ylabel="displacement",xlabel="time (sec)",legend = :outertopright))
    display(Plots.plot(x,disZ,title="z displacement",label="",ylabel="displacement",xlabel="time (sec)",legend = :outertopright))
    return disX,disY,disZ
end
#############Stress######################

function plotStressTroughTimeSteps(name)
    setupViz=getSetup1("$(name)/0")
    maxNumFiles=setupViz["maxNumFiles"]
    numTimeSteps=setupViz["numTimeSteps"]
    x=(1:maxNumFiles).*floor(numTimeSteps/maxNumFiles)
    
    s=[]
    
    for i in 0:(maxNumFiles-1)
        setupViz=getSetup1("$(name)/$(Int(i))");
        edge=setupViz["edges"][end]
        append!(s,[edge["stress"]])

    end
    display(Plots.plot(x,s,title="x",label="",ylabel="stress",xlabel="timestep"))
    
end

function plotFinalStress(name)
    setupViz=getSetup1("$(name)/0")
    maxNumFiles=setupViz["maxNumFiles"]
    setupVizLast=getSetup1("$(name)/$(Int(maxNumFiles-1))");
    edges=setupVizLast["edges"]
    s=[]
    
    for edge in edges
        append!(s,[edge["stress"]])
        
    end
    # println(s)
    display(Plots.scatter(s,title="final stress",label="",ylabel="stress",xlabel="node ID"))
end

###############Strain####################
function plotStressStrainTroughTimeSteps(name,id)
    setupViz=getSetup1("$(name)/0")
    maxNumFiles=setupViz["maxNumFiles"]
    numTimeSteps=setupViz["numTimeSteps"]
    x=(1:maxNumFiles).*floor(numTimeSteps/maxNumFiles)
    
    stress=[]
    strain=[]
    
    for i in 0:(maxNumFiles-1)
        setupViz=getSetup1("$(name)/$(Int(i))");
        edge=setupViz["edges"][id]
        node1=setupViz["nodes"][edge["source"]+1]
        node2=setupViz["nodes"][edge["target"]+1]
        p11=Vector3(node1["position"]["x"],
                    node1["position"]["y"],
                    node1["position"]["z"])
        p21=Vector3(node2["position"]["x"],
                    node2["position"]["y"],
                    node2["position"]["z"])
        p1=Vector3( node1["position"]["x"]+node1["displacement"]["x"],
                    node1["position"]["y"]+node1["displacement"]["y"],
                    node1["position"]["z"]+node1["displacement"]["z"])
        p2=Vector3( node2["position"]["x"]+node2["displacement"]["x"],
                    node2["position"]["y"]+node2["displacement"]["y"],
                    node2["position"]["z"]+node2["displacement"]["z"])
        append!(stress,[edge["stress"]])
        append!(strain,[(lengthVector3(p1-p2)/lengthVector3(p11-p21))-1.0])

    end
    
    # display(plot(stress./strain,title="yougs modulus",label=""))
    display(Plots.plot(x,stress./(strain.+1.0),title="",label="",ylabel="stress/strain",xlabel="timestep"))
    display(Plots.plot(x,stress,title="stress",label="",ylabel="stress",xlabel="timestep"))
    display(Plots.plot(x,strain,title="strain",label="",ylabel="strain",xlabel="timestep"))
    display(Plots.plot(stress,strain,title="stress strain",label="",ylabel="stress",xlabel="strain"))

    
end

function plotPrescribedTroughTimeSteps(name,id,strainData,stressData)
    setupViz=getSetup1("$(name)/0")
    maxNumFiles=setupViz["maxNumFiles"]
    numTimeSteps=setupViz["numTimeSteps"]
    x=(1:maxNumFiles).*floor(numTimeSteps/maxNumFiles)
    
    stress=[]
    strain=[]
    currentID=1
    prescribedX=[]
    prescribedY=[]
    
    for i in 0:(maxNumFiles-1)
        setupViz=getSetup1("$(name)/$(Int(i))");
        edge=setupViz["edges"][id]
        node1=setupViz["nodes"][edge["source"]+1]
        node2=setupViz["nodes"][edge["target"]+1]
        p11=Vector3(node1["position"]["x"],
                    node1["position"]["y"],
                    node1["position"]["z"])
        p21=Vector3(node2["position"]["x"],
                    node2["position"]["y"],
                    node2["position"]["z"])
        p1=Vector3( node1["position"]["x"]+node1["displacement"]["x"],
                    node1["position"]["y"]+node1["displacement"]["y"],
                    node1["position"]["z"]+node1["displacement"]["z"])
        p2=Vector3( node2["position"]["x"]+node2["displacement"]["x"],
                    node2["position"]["y"]+node2["displacement"]["y"],
                    node2["position"]["z"]+node2["displacement"]["z"])
        append!(stress,[edge["stress"]])
        append!(strain,[(lengthVector3(p1-p2)/lengthVector3(p11-p21))-1.0])
        if currentID<=length(strainData)
            if (strain[end]>strainData[currentID]) ||(currentID>4&&strain[end]<0.3)
                append!(prescribedX,[i*floor(numTimeSteps/maxNumFiles)])
                append!(prescribedY,[stressData[currentID]/(strainData[currentID]+1.0)])
                currentID+=1
            end
        end

    end
    # prescribedX[4]=0.9e4
    # println(currentID)
    # println(length(strainData))
    # println(prescribedX)
    # display(plot(stress./strain,title="yougs modulus",label=""))
    Plots.plot(x,stress./(strain.+1.0),title="",label="Observed",ylabel="stress/strain",xlabel="timestep")
    Plots.scatter!(prescribedX,prescribedY,title="",label="",color=:black)
    display(Plots.plot!(prescribedX,prescribedY,title="",label="Prescibed",color=:black))


    
end

##################################

#############tendon#####################

function plotDetailedTendon(simName)
    setupViz=getSetup1("$(simName)/0")
    maxNumFiles=Int(setupViz["maxNumFiles"])
    numTimeSteps=setupViz["numTimeSteps"]
    x=(1:maxNumFiles).*floor(numTimeSteps/maxNumFiles)
    disX=[]
    disY=[]

    disX1=[]
    disY1=[]
    for i in 0:(maxNumFiles-1)
        setupViz=getSetup1("$(simName)/$(Int(i))");
        nodes=setupViz["nodes"]
        append!(disX,[[]]);append!(disY,[[]]);
        append!(disX1,[[]]);append!(disY1,[[]]);
        for node in nodes
            if node["position"]["z"]>30 && node["position"]["y"]>30 
                append!(disX[Int(i+1)],[node["position"]["x"]./setupViz["scale"]+node["displacement"]["x"]./setupViz["scale"]])
                append!(disY[Int(i+1)],[node["position"]["y"]./setupViz["scale"]+node["displacement"]["y"]./setupViz["scale"]])
            end
            if node["position"]["z"]>30 && node["position"]["y"]<10 
                append!(disX1[Int(i+1)],[node["position"]["x"]./setupViz["scale"]+node["displacement"]["x"]./setupViz["scale"]])
                append!(disY1[Int(i+1)],[node["position"]["y"]./setupViz["scale"]+node["displacement"]["y"]./setupViz["scale"]])
            end
        end
    end
    Plots.plot(disX[1,:],disY[1,:],legend=false, aspect_ratio=:equal)
    Plots.plot!(disX1[1,:],disY1[1,:],legend=false, aspect_ratio=:equal)
    for i in 1:5:maxNumFiles
        Plots.plot!(disX[i,:],disY[i,:],legend=false, aspect_ratio=:equal)
        Plots.plot!(disX1[i,:],disY1[i,:],legend=false, aspect_ratio=:equal)
    end
    display(Plots.plot!())
end

function plotDetailedTendonVSphysical(simName,nodesPhys,skip=0)
    setupViz=getSetup1("$(simName)/0")
    maxNumFiles=Int(setupViz["maxNumFiles"])
    numTimeSteps=setupViz["numTimeSteps"]
    x=(1:maxNumFiles).*floor(numTimeSteps/maxNumFiles)
    disX=[]
    disY=[]

    disX1=[]
    disY1=[]
    for i in 0:(maxNumFiles-1)
        setupViz=getSetup1("$(simName)/$(Int(i))");
        nodes=setupViz["nodes"]
        append!(disX,[[]]);append!(disY,[[]]);
        append!(disX1,[[]]);append!(disY1,[[]]);
        for node in nodes
            if node["position"]["z"]>30 && node["position"]["y"]>30 
                append!(disX[Int(i+1)],[node["position"]["x"]./setupViz["scale"]+node["displacement"]["x"]])
                append!(disY[Int(i+1)],[node["position"]["y"]./setupViz["scale"]+node["displacement"]["y"]])
            end
            if node["position"]["z"]>30 && node["position"]["y"]<10 
                append!(disX1[Int(i+1)],[node["position"]["x"]./setupViz["scale"]+node["displacement"]["x"]./setupViz["scale"]])
                append!(disY1[Int(i+1)],[node["position"]["y"]./setupViz["scale"]+node["displacement"]["y"]./setupViz["scale"]])
            end
        end
    end
    Plots.plot(disX[1,:]  ,disY[1,:],label="MetaVoxels", aspect_ratio=:equal,color=palette(:default)[1],legend = :outertopright)
    Plots.plot!(disX1[1,:]  ,disY1[1,:],label="", aspect_ratio=:equal,color=palette(:default)[1])
    for i in 1:maxNumFiles-skip
        Plots.plot!(disX[i,:]  ,disY[i,:],label="", aspect_ratio=:equal,color=palette(:default)[1])
        Plots.plot!(disX1[i,:] ,disY1[i,:],label="", aspect_ratio=:equal,color=palette(:default)[1])
    end
    Plots.plot!(nodesPhys[1:5,1,1].+75/2,nodesPhys[1:5,1,2],label="Physical Experiment", aspect_ratio=:equal,color=palette(:default)[2])
    Plots.plot!(nodesPhys[6:10,1,1].+75/2,nodesPhys[6:10,1,2],label="", aspect_ratio=:equal,color=palette(:default)[2])
    for i=2:12
        Plots.plot!(nodesPhys[1:5,i,1].+75/2,nodesPhys[1:5,i,2],label="", aspect_ratio=:equal,color=palette(:default)[2])
        Plots.plot!(nodesPhys[6:10,i,1].+75/2,nodesPhys[6:10,i,2],label="", aspect_ratio=:equal,color=palette(:default)[2])
    end
    display(Plots.plot!())
end

##################################

#olot nodes 2D (x and z) and show concave hull
function plot2D(name)
    setupViz=getSetup1("$(name)/0")
    posX=[]
    posY=[]
    posZ=[]
    points1=[]
    nodes=setupViz["nodes"]
    scale=setupViz["scale"]*setupViz["voxelSize"]
    for node in nodes
        append!(posX,[node["position"]["x"]/scale+0.51])
        append!(posY,[node["position"]["y"]/scale+0.51])
        append!(posZ,[node["position"]["z"]/scale+0.51])
        append!(points1,[[node["position"]["x"]/scale+0.51,node["position"]["z"]/scale+0.51]])
    end
    
    maxNumFiles=setupViz["maxNumFiles"]
    setupVizLast=getSetup1("$(name)/$(Int(maxNumFiles-1))");
    nodes=setupVizLast["nodes"]
    disX=[]
    disY=[]
    disZ=[]
    points2=[]
    for node in nodes
        append!(disX,[node["position"]["x"]/scale+node["displacement"]["x"]/scale+0.51])
        append!(disY,[node["position"]["y"]/scale+node["displacement"]["y"]/scale+0.51])
        append!(disZ,[node["position"]["z"]/scale+node["displacement"]["z"]/scale+0.51])
        append!(points2,[[node["position"]["x"]/scale+node["displacement"]["x"]/scale+0.51,node["position"]["z"]/scale+node["displacement"]["z"]/scale+0.51]])

    end
    

    hull1 = concave_hull(points1,100)
    hull2 = concave_hull(points2,100)

    hull1 = concave_hull(points1)
    hull2 = concave_hull(points2)

    
    Plots.scatter(posX,posZ,label="", aspect_ratio=:equal,markershape=:square)
    Plots.plot!(hull1)
    Plots.plot!(hull2)
    display(Plots.scatter!(disX,disZ,label="",markershape=:square,c=:black))
    
    return hull1,hull2

end


function plotTop(name,thresholdMin,thresholdMax=1000,num=1)
    for ii=1:num
        setupViz=getSetup1("$(name)/0")
        maxNumFiles=setupViz["maxNumFiles"]
        ts=Int(floor(ii/num*maxNumFiles))
        setupVizLast=getSetup1("$(name)/$(Int(ts-1))");
        nodes=setupVizLast["nodes"]
        edges=setupVizLast["edges"]
        disX=[];disY=[];disZ=[]
        posX=[]; posY=[];posZ=[]
        IDs=[]
        i=1
        for node in nodes
            if node["position"]["y"]>thresholdMin && node["position"]["y"]<thresholdMax
                append!(disX,[node["displacement"]["x"]])
                append!(disY,[node["displacement"]["y"]])
                append!(disZ,[node["displacement"]["z"]])
                append!(posX,[node["position"]["x"]])
                append!(posY,[node["position"]["y"]])
                append!(posZ,[node["position"]["z"]])
                #append!(IDs,[parse(Int64,node["id"][2:end])])
                append!(IDs,[i])
            end
            i+=1
        end
        if ii==1
            Plots.scatter(posX,posZ,title="Top",label="",ylabel="x",xlabel="z", aspect_ratio=:equal,color=:cyan)
        end
        Plots.scatter!(posX.+disX,posZ.+disZ,title="Top",label="",ylabel="x",xlabel="z", aspect_ratio=:equal,color=RGB(-(ii-num)/num,-(ii-num)/num,-(ii-num)/num))

        # display(Plots.scatter(posZ,posX,title="f",label="",ylabel="d",xlabel="d", aspect_ratio=:equal))
        for edge in edges
            if in(IDs).(edge["source"]+1) && in(IDs).(edge["target"]+1)
                s=edge["source"]+1;t=edge["target"]+1;
                if ii==1
                    Plots.plot!([nodes[s]["position"]["x"],nodes[t]["position"]["x"]],[nodes[s]["position"]["z"],nodes[t]["position"]["z"]],legend=false,color=:cyan)
                end
                Plots.plot!([nodes[s]["position"]["x"]+nodes[s]["displacement"]["x"],
                        nodes[t]["position"]["x"]+nodes[t]["displacement"]["x"]],
                    [nodes[s]["position"]["z"]+nodes[s]["displacement"]["z"],
                        nodes[t]["position"]["z"]+nodes[t]["displacement"]["z"]]
                    ,legend=false,color=RGB(-(ii-num)/num,-(ii-num)/num,-(ii-num)/num),linestyle=:dash)
            end 
        end
        
    end
    display(Plots.plot!())
end

