###########################GPU###############################################
function lbm2DGPU(nx,ny,maxIter,setboundary2D,radius)
    #*
    # configuration
    #*
    omega = 1.0;
    density = 1.0;
    fraction = 0.1 # average fraction of occupied nodes

    maxIter2=100


    deltaU=1e-7;

    #*
    # setup of variables
    #*
    avu=1; prevavu=1;
    ts=0;
    avus = zeros(Float64, maxIter)

    # single-particle distribution function
    # particle densities conditioned on one of the 9 possible velocities
    F1 = repeat([density/9], outer= [nx, ny, 9]);
    DENSITY=dropdims(sum(F1,dims=3),dims=3);
    F=CuArray(F1);
    FEQ=CuArray(F1);
    DENSITY=CuArray(DENSITY)
    

    msize = nx*ny;
    CI = 0:msize:msize*7; # offsets of different directions e_a in the F matrix 
    BOUND,ON,numactivenodes,TO_REFLECT,REFLECTED=setboundary2D(nx,ny,radius);
    BOUNCEDBACK = CuArray(F1[TO_REFLECT]);

    #*
    # constants
    #*
    t1 = 4/9;
    t2 = 1/9;
    t3 = 1/36;
    c_squ = 1/3;
    count=0

    UX=CUDA.zeros(nx,ny);
    UY=CUDA.zeros(nx,ny);
    
    U_SQU=CUDA.zeros(nx,ny);
    
    t=@timed LBM2DGPU!(UX,UY,F,FEQ,DENSITY,
                ON,TO_REFLECT,REFLECTED,BOUNCEDBACK,
                omega,deltaU,
                t1,t2,t3,c_squ,U_SQU )
    time=t[2]
    println("First time steps took $time seconds")
    
    ts=1
    while (ts<maxIter)
    # while ((ts<maxIter) & (1e-10 < abs((prevavu-avu)/avu))) | (ts<maxIter2)
        LBM2DGPU!(UX,UY,F,FEQ,DENSITY,
                ON,TO_REFLECT,REFLECTED,BOUNCEDBACK,
                omega,deltaU,
                t1,t2,t3,c_squ,U_SQU )
        # prevavu = avu;
        # avu = sum(sum(Array(UX)))/numactivenodes;
        # avus[ts+1] = avu;
        ts=ts+1

    end
    display(ts)
    return UX,UY
end

function LBM2DGPU!(UX,UY,F,FEQ,DENSITY,
    ON,TO_REFLECT,REFLECTED,BOUNCEDBACK,
    omega,deltaU,
    t1,t2,t3,c_squ,U_SQU )
    #*
    # Streaming
    #*

    # particles at (x,y)=[2,1] were at [1, 1] before: 1 points right (1,0)
    CUDA.@sync F[:,:,1] .= F[[nx; 1:nx-1],:,1];

    # particles at [1,2] were at [1, 1] before: 3 points up (0,1)
    CUDA.@sync F[:,:,3] .= F[:,[ny; 1:ny-1],3];

    # particles at [1,1] were at [2, 1] before: 5 points left (-1,0)
    CUDA.@sync F[:,:,5] .= F[[2:nx; 1],:,5];

    # particles at [1,1] were at [1, 2] before: 7 points down (0,-1)
    CUDA.@sync F[:,:,7] .= F[:,[2:ny; 1],7];

    # particles at [2,2] were at [1, 1] before: 2 points to (1,1)
    CUDA.@sync F[:,:,2] .= F[[nx; 1:nx-1],[ny; 1:ny-1],2];

    # particles at [1,2] were at [2, 1] before: 4 points to (-1, 1)
    CUDA.@sync F[:,:,4] .= F[[2:nx; 1],[ny; 1:ny-1],4];

    # particles at [1,1] were at [2, 2] before: 6 points to (-1, -1)
    CUDA.@sync F[:,:,6] .= F[[2:nx; 1],[2:ny; 1],6];

    # particles at [2,1] were at [1, 2] before: 8 points down (1,-1)
    CUDA.@sync F[:,:,8] .= F[[nx; 1:nx-1],[2:ny; 1],8];

    CUDA.@sync DENSITY[:] .= sum(F,dims=3)[:];

    # 1,2,8 are moving to the right, 4,5,6 to the left
    # 3, 7 and 9 don't move in the x direction
    
    CUDA.@sync UX[:] .= ((sum(F[:,:,[1, 2, 8]],dims=3) -sum(F[:,:,[4, 5, 6]],dims=3))./DENSITY)[:];
    # 2,3,4 are moving up, 6,7,8 down
    # 1, 5 and 9 don't move in the y direction
    CUDA.@sync UY[:] .= ((sum(F[:,:,[2, 3, 4]],dims=3) -sum(F[:,:,[6, 7, 8]],dims=3))./DENSITY)[:];

    CUDA.@sync UX[1,1:ny] .= UX[1,1:ny] .+ deltaU; #Increase inlet pressure

    CUDA.@sync UX[ON] .= 0.0; 
    CUDA.@sync UY[ON] .= 0.0; 
    CUDA.@sync DENSITY[ON] .= 0.0;

    CUDA.@sync U_SQU .= UX.^2 .+ UY.^2;

    # Calculate equilibrium distribution: stationary (a = 0)
    CUDA.@sync FEQ[:,:,9] .= t1*DENSITY.*(1 .-U_SQU/(2*c_squ));

    # nearest-neighbours
    CUDA.@sync FEQ[:,:,1] .= t2*DENSITY.*(1 .+UX/c_squ+0.5*(UX/c_squ).^2-U_SQU/(2*c_squ));
    CUDA.@sync FEQ[:,:,3] .= t2*DENSITY.*(1 .+UY/c_squ+0.5*(UY/c_squ).^2-U_SQU/(2*c_squ));
    CUDA.@sync FEQ[:,:,5] .= t2*DENSITY.*(1 .-UX/c_squ+0.5*(UX/c_squ).^2-U_SQU/(2*c_squ));
    CUDA.@sync FEQ[:,:,7] .= t2*DENSITY.*(1 .-UY/c_squ+0.5*(UY/c_squ).^2-U_SQU/(2*c_squ));

    # next-nearest neighbours
    CUDA.@sync FEQ[:,:,2] .= t3*DENSITY.*(1 .+(UX.+UY)/c_squ+0.5*((UX.+UY)/c_squ).^2-U_SQU/(2*c_squ));
    CUDA.@sync FEQ[:,:,4] .= t3*DENSITY.*(1 .+(-UX.+UY)/c_squ+0.5*((-UX.+UY)/c_squ).^2-U_SQU/(2*c_squ));
    CUDA.@sync FEQ[:,:,6] .= t3*DENSITY.*(1 .+(-UX.-UY)/c_squ+0.5*((-UX.-UY)/c_squ).^2-U_SQU/(2*c_squ));
    CUDA.@sync FEQ[:,:,8] .= t3*DENSITY.*(1 .+(UX.-UY)/c_squ+0.5*((UX.-UY)/c_squ).^2-U_SQU/(2*c_squ));

    CUDA.@sync BOUNCEDBACK .= F[TO_REFLECT]; #Densities bouncing back at next timestep

    CUDA.@sync F .= omega .*FEQ + (1 .-omega) .*F;

    CUDA.@sync F[REFLECTED] .= BOUNCEDBACK;

    
end
##########################################################################
function lbm3DGPU(nx,ny,nz,maxIter)
    omega=1.0; 
    density=1.0;
    t1=1/3; 
    t2=1/18; 
    t3=1/36;

    F1=repeat([density/19],outer=[nx,ny,nz,19]); 
    
    DENSITY=dropdims(sum(F1,dims=4),dims=4);
    F=CuArray(F1);
    FEQ=CuArray(F1);
    DENSITY=CuArray(DENSITY)


    matsize=nx*ny*nz;
    CI=0:matsize:matsize*19;

    maxIter2=100

    BOUND=fill(false,nx,ny,nz);
    for i=1:nx
        for j=1:ny
            for k=1:nz
                BOUND[i,j,k]=((i-5)^2+(j-6)^2+(k-7)^2)<6;
            end
        end
    end

    BOUND[:,:,1].=true;
    BOUND[:,1,:].=true;

    ON=findall(BOUND[:]); #matrix offset of each Occupied Node


    TO_REFLECT=[ON.+CI[2] ON.+CI[3] ON.+CI[4] ON.+CI[5] ON.+CI[6] ON.+CI[7] ON.+CI[8] ON.+CI[9] ON.+CI[10] ON.+CI[11] ON.+CI[12] ON.+CI[13] ON.+CI[14] ON.+CI[15] ON.+CI[16] ON.+CI[17] ON.+CI[18] ON.+CI[19]];
    REFLECTED=[ON.+CI[3] ON.+CI[2] ON.+CI[5] ON.+CI[4] ON.+CI[7] ON.+CI[6] ON.+CI[11] ON.+CI[10] ON.+CI[9] ON.+CI[8] ON.+CI[15] ON.+CI[14] ON.+CI[13] ON.+CI[12] ON.+CI[19] ON.+CI[18] ON.+CI[17] ON.+CI[16]];
    BOUNCEDBACK = CuArray(F1[TO_REFLECT]);

    avu=1; 
    prevavu=1; 
    ts=0; 
    deltaU=1e-7; 
    numactivenodes=sum(sum(sum(1 .-Int.(BOUND))));

    UX=CUDA.zeros(nx,ny,nz);
    UY=CUDA.zeros(nx,ny,nz);
    UZ=CUDA.zeros(nx,ny,nz);

    U_SQU=CUDA.zeros(nx,ny,nz);

    t=@timed LBM3DGPU!(UX,UY,UZ,F,FEQ,DENSITY,
            ON,TO_REFLECT,REFLECTED,BOUNCEDBACK,
            omega,deltaU,t1,t2,t3,U_SQU)
    time=t[2]
    println("First time steps took $time seconds")

    while (ts<maxIter)
    # while (ts<maxIter && 1e-10<abs((prevavu-avu)/avu)) || ts<maxIter2
        LBM3DGPU!(UX,UY,UZ,F,FEQ,DENSITY,
                ON,TO_REFLECT,REFLECTED,BOUNCEDBACK,
                omega,deltaU,t1,t2,t3,U_SQU)

        # prevavu =avu;
        # avu=sum(Array(UX))/numactivenodes;
        ts=ts+1;
    end

    display(ts)

    return UX,UY,UZ

end

function LBM3DGPU!(UX,UY,UZ,F,FEQ,DENSITY,
    ON,TO_REFLECT,REFLECTED,BOUNCEDBACK,
    omega,deltaU,t1,t2,t3,U_SQU)

    CUDA.@sync F[:,:,:,2].=F[:,:,[nz;1:nz-1],2];
    CUDA.@sync F[:,:,:,3].=F[:,:,[2:nz;1],3];
    CUDA.@sync F[:,:,:,4].=F[:,[ny;1:ny-1],:,4];
    CUDA.@sync F[:,:,:,5].=F[:,[2:ny;1],:,5];	
    CUDA.@sync F[:,:,:,6].=F[[nx;1:nx-1],:,:,6];
    CUDA.@sync F[:,:,:,7].=F[[2:nx;1],:,:,7];	
    #next-nearest neighbours
    CUDA.@sync F[:,:,:,8] .= F[[nx;1:nx-1],[ny;1:ny-1],:,8];
    CUDA.@sync F[:,:,:,9] .= F[[nx;1:nx-1],[2:ny;1],:,9];
    CUDA.@sync F[:,:,:,10].=F[[2:nx;1],[ny;1:ny-1],:,10];
    CUDA.@sync F[:,:,:,11].=F[[2:nx;1],[2:ny;1],:,11];	
    CUDA.@sync F[:,:,:,12].=F[[nx;1:nx-1],:,[nz;1:nz-1],12];
    CUDA.@sync F[:,:,:,13].=F[[nx;1:nx-1],:,[2:nz;1],13];
    CUDA.@sync F[:,:,:,14].=F[[2:nx;1],:,[nz;1:nz-1],14];
    CUDA.@sync F[:,:,:,15].=F[[2:nx;1],:,[2:nz;1],15];
    CUDA.@sync F[:,:,:,16].=F[:,[ny;1:ny-1],[nz;1:nz-1],16];
    CUDA.@sync F[:,:,:,17].=F[:,[ny;1:ny-1],[2:nz;1],17];
    CUDA.@sync F[:,:,:,18].=F[:,[2:ny;1],[nz;1:nz-1],18];
    CUDA.@sync F[:,:,:,19].=F[:,[2:ny;1],[2:nz;1],19];
    CUDA.@sync BOUNCEDBACK.=F[TO_REFLECT]; #Densities bouncing back at next timestep
    # Relax; calculate equilibrium state (FEQ) with equivalent speed and density to F 
    CUDA.@sync DENSITY[:] .= sum(F,dims=4)[:];
    CUDA.@sync UX[:].=(sum(F[:,:,:,[6,8 , 9 ,12 ,13]],dims=4)-  sum(F[:,:,:,  [7, 10, 11, 14, 15]],dims=4))[:]./DENSITY[:];
    CUDA.@sync UY[:].=(sum(F[:,:,:,[4, 8 , 10, 16, 17]],dims=4)- sum(F[:,:,:,  [5, 9 ,11 ,18 ,19]],dims=4))[:]./DENSITY[:];
    CUDA.@sync UZ[:].=(sum(F[:,:,:,[2,12, 14, 16, 18]],dims=4)- sum(F[:,:,:,  [3, 13, 15, 17, 19]],dims=4))[:]./DENSITY[:];
    
    CUDA.@sync UX[1,:,:].=UX[1,:,:].+deltaU; #Increase inlet pressure

    CUDA.@sync UX[ON].=0.0; 
    CUDA.@sync UY[ON].=0.0; 
    CUDA.@sync UZ[ON].=0.0; 
    CUDA.@sync DENSITY[ON].=0.0;

    CUDA.@sync U_SQU .= UX.^2 .+ UY.^2 .+ UZ.^2;
    
    # Calculate equilibrium distribution: stationary
    CUDA.@sync FEQ[:,:,:,1].=t1.*DENSITY.*(1 .-3*U_SQU/2);
    # nearest-neighbours
    CUDA.@sync FEQ[:,:,:,2].=t2.*DENSITY.*(1 .+ 3*UZ + 9/2*UZ.^2 .- 3/2*U_SQU);
    CUDA.@sync FEQ[:,:,:,3].=t2.*DENSITY.*(1 .- 3*UZ + 9/2*UZ.^2 .- 3/2*U_SQU);
    CUDA.@sync FEQ[:,:,:,4].=t2.*DENSITY.*(1 .+ 3*UY + 9/2*UY.^2 .- 3/2*U_SQU);
    CUDA.@sync FEQ[:,:,:,5].=t2.*DENSITY.*(1 .- 3*UY + 9/2*UY.^2 .- 3/2*U_SQU);
    CUDA.@sync FEQ[:,:,:,6].=t2.*DENSITY.*(1 .+ 3*UX + 9/2*UX.^2 .- 3/2*U_SQU);
    CUDA.@sync FEQ[:,:,:,7].=t2.*DENSITY.*(1 .- 3*UX + 9/2*UX.^2 .- 3/2*U_SQU);
    # next-nearest neighbours
    CUDA.@sync FEQ[:,:,:,8] .=t3.*DENSITY.*(1 .+ 3*(UX.+UY)  .+ 9/2*((UX.+UY)).^2  .- 3*U_SQU/2);
    CUDA.@sync FEQ[:,:,:,9] .=t3.*DENSITY.*(1 .+ 3*(UX.-UY)  .+ 9/2*((UX.-UY)).^2  .- 3*U_SQU/2);
    CUDA.@sync FEQ[:,:,:,10].=t3.*DENSITY.*(1 .+ 3*(UY.-UX) .+ 9/2*((UY.-UX)).^2 .- 3*U_SQU/2);
    CUDA.@sync FEQ[:,:,:,11].=t3.*DENSITY.*(1 .+ 3*(-UY.-UX) .+ 9/2*((-UY.-UX)).^2 .- 3*U_SQU/2);
    CUDA.@sync FEQ[:,:,:,12].=t3.*DENSITY.*(1 .+ 3*(UX.+UZ) .+ 9/2*((UX.+UZ)).^2 .- 3*U_SQU/2);
    CUDA.@sync FEQ[:,:,:,13].=t3.*DENSITY.*(1 .+ 3*(UX.-UZ) .+ 9/2*((UX.-UZ)).^2 .- 3*U_SQU/2);
    CUDA.@sync FEQ[:,:,:,14].=t3.*DENSITY.*(1 .+ 3*(UZ.-UX) .+ 9/2*((UZ.-UX)).^2 .- 3*U_SQU/2);
    CUDA.@sync FEQ[:,:,:,15].=t3.*DENSITY.*(1 .+ 3*(-UZ.-UX) .+ 9/2*((-UZ.-UX)).^2 .- 3*U_SQU/2);
    CUDA.@sync FEQ[:,:,:,16].=t3.*DENSITY.*(1 .+ 3*(UY.+UZ) .+ 9/2*((UY.+UZ)).^2 .- 3*U_SQU/2);
    CUDA.@sync FEQ[:,:,:,17].=t3.*DENSITY.*(1 .+ 3*(UY.-UZ) .+ 9/2*((UY.-UZ)).^2 .- 3*U_SQU/2);
    CUDA.@sync FEQ[:,:,:,18].=t3.*DENSITY.*(1 .+ 3*(UZ.-UY) .+ 9/2*((UZ.-UY)).^2 .- 3*U_SQU/2);
    CUDA.@sync FEQ[:,:,:,19].=t3.*DENSITY.*(1 .+ 3*(-UZ.-UY) .+ 9/2*((-UZ.-UY)).^2 .- 3*U_SQU/2);
    
    CUDA.@sync F .= omega .*FEQ .+ (1 .-omega) .*F;

    CUDA.@sync F[REFLECTED].=BOUNCEDBACK;

    
end

##########################################################################
