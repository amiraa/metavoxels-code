function rotation_matrix(element_vector, x_axis, y_axis,z_axis)
    # find the direction cosines
    L=norm(element_vector)
    l = (element_vector[1])/L
	m = (element_vector[2])/L
	n = (element_vector[3])/L
	D = ( l^2+ m^2+n^2)^0.5
    
    transMatrix=[[l m  n  0  0  0  0  0  0  0  0  0];[-m/D l/D  0  0  0  0  0  0  0  0  0  0];[ -l*n/D  -m*n/D  D  0  0  0  0  0  0  0  0  0];[ 0  0  0       l       m  n  0  0  0  0  0  0];[ 0  0  0    -m/D     l/D  0  0  0  0  0  0  0];[ 0  0  0  -l*n/D  -m*n/D  D  0  0  0  0  0  0];[ 0  0  0  0  0  0       l       m  n  0  0  0];[ 0  0  0  0  0  0    -m/D     l/D  0  0  0  0];[ 0  0  0  0  0  0  -l*n/D  -m*n/D  D  0  0  0];[ 0  0  0  0  0  0  0  0  0       l       m  n];[ 0  0  0  0  0  0  0  0  0    -m/D     l/D  0];[ 0  0  0  0  0  0  0  0  0  -l*n/D  -m*n/D  D]]
	
    return transMatrix
end

function get_transformation_matrix(dx)
    # from https://github.com/robbievanleeuwen/feastruct/tree/master
    """Returns the transformation matrix for an EulerBernoulli3D_2N element.

    :returns: Element transformation matrix
    :rtype: :class:`numpy.ndarray`
    """
    # dx = node_coords[1] - node_coords[0]
    l0 = norm(dx);
    c = dx / l0;
    # (_, _, _, c) = self.get_geometric_properties()

    # construct rotation matrix
    cxx = c[0+1];
    cyx = c[1+1];
    czx = c[2+1];
    d = sqrt(cxx * cxx + cyx * cyx);
    cxy = -cyx / d;
    cyy = cxx / d;
    czy = 0;
    cxz = -cxx * czx / d;
    cyz = -cyx * czx / d;
    czz = d;

    gamma = [
        [cxx  cyx  czx];
        [cxy  cyy  czy];
        [cxz  cyz  czz]];

    T = zeros((12, 12));
    T[0+1:3, 1+0:3] = gamma;
    T[3+1:6, 1+3:6] = gamma;
    T[6+1:9, 1+6:9] = gamma;
    T[9+1:12,1+9:12] = gamma;

    return T
end

function get_stresses(setup)
    nodes      = setup["nodes"]
    edges      = setup["edges"]
    ndofs      = length(nodes)*6

    x_axis     = [1 0 0]
    y_axis     = [0 1 0]
    z_axis     = [0 0 1]

    # find the stresses in each member
    stresses=zeros(length(edges))
    max11=-10e6
    min11=10e6
    for edge in edges
        #degrees_of_freedom = properties["degrees_of_freedom"]

        element=parse(Int,edge["id"][2:end])

        # find the nodes that the lements connects
        fromNode = nodes[edge["source"]+1]
        toNode = nodes[edge["target"]+1]

        # the coordinates for each node
        fromPoint = [fromNode["position"]["x"] fromNode["position"]["y"] fromNode["position"]["z"]]
        toPoint = [toNode["position"]["x"] toNode["position"]["y"] toNode["position"]["z"]]

        # find the degrees of freedom for each node
        dofs = convert(Array{Int}, fromNode["degrees_of_freedom"])
        dofs=vcat(dofs,convert(Array{Int}, toNode["degrees_of_freedom"]))

        element_vector=toPoint-fromPoint
        
       

        # find rotated mass and stifness matrices
        tau = rotation_matrix(element_vector, x_axis,y_axis,z_axis)
        tau = get_transformation_matrix(element_vector)
        # println(element_vector)
        # i1=parse(Int,fromNode["id"][2:end])
        # i2=parse(Int,toNode["id"][2:end])
        
        # global_displacements=[X[(i1)*6+1] X[(i1)*6+2] X[(i1)*6+3] X[(i1)*6+4] X[(i1)*6+5] X[(i1)*6+6] X[(i2)*6+1] X[(i2)*6+2] X[(i2)*6+3] X[(i2)*6+4] X[(i2)*6+5] X[(i2)*6+6]] # todo change
        global_displacements=[fromNode["displacement"]["x"] fromNode["displacement"]["y"] fromNode["displacement"]["z"] fromNode["angle"]["x"] fromNode["angle"]["y"] fromNode["angle"]["z"] toNode["displacement"]["x"] toNode["displacement"]["y"] toNode["displacement"]["z"] toNode["angle"]["x"] toNode["angle"]["y"] toNode["angle"]["z"]] # todo change
        # println(global_displacements)
        # nodal displacement

        q=tau*transpose(global_displacements)
        # println(q)
        # calculate the strain and stresses
        strain =(q[7]-q[1])/norm(element_vector)
        E = edge["material"]["stiffness"]# youngs modulus
        stress=E.*strain
        edge["stress"]=stress
        if stress>max11
            max11=stress
        end
        if stress<min11
            min11=stress
        end
        # println(element)
        # println(stress)
    end

    
    
    setup["viz"]["minStress"]=min11
    setup["viz"]["maxStress"]=max11
    return stresses
end

function get_matrices(setup)
    
    nodes      = setup["nodes"]
    edges      = setup["edges"]
    ndofs      = length(nodes)*6

    x_axis     = [1 0 0]
    y_axis     = [0 1 0]
    z_axis     = [0 0 1]

    M = zeros((ndofs,ndofs))
    K = zeros((ndofs,ndofs))

    setup["mass"]=0

    for edge in edges
        #degrees_of_freedom = properties["degrees_of_freedom"]

        element=parse(Int,edge["id"][2:end])

        # find the nodes that the lements connects
        fromNode = nodes[edge["source"]+1]
        toNode = nodes[edge["target"]+1]

        # the coordinates for each node
        fromPoint = [fromNode["position"]["x"] fromNode["position"]["y"] fromNode["position"]["z"]]
        toPoint = [toNode["position"]["x"] toNode["position"]["y"] toNode["position"]["z"]]

        # find the degrees of freedom for each node
        dofs = convert(Array{Int}, fromNode["degrees_of_freedom"])
        dofs=vcat(dofs,convert(Array{Int}, toNode["degrees_of_freedom"]))

        element_vector=toPoint-fromPoint
        

        # find element mass and stifness matrices
        length   = norm(element_vector)
        rho      = edge["material"]["density"]
        area     = edge["material"]["area"]
        E        = edge["material"]["stiffness"]
        A = edge["material"]["area"]
        nu= edge["material"]["poissonRatio"]# youngs modulus
        G = E * 1 / 3
        # G = E / (1 + nu) / 2  # MPa
        setup["mass"]+=A*rho*length
        # println(length)
        # println(A)

        if !haskey(edge["material"], "profile") || edge["material"]["profile"]=="rec"
            
            b=sqrt(A)
            h=b

            ixx=b*h^3/12 #rectangle filled
            iyy=h*b^3/12 #rectangle filled

        
            j=b*h*(b*b+h*h)/12 #rectangle filled #polar moment of inertia

            # S = h * b
            # Sy = (
            #     S
            #     * (6 + 12 * nu + 6 * nu ^ 2)
            #     / (7 + 12 * nu + 4 * nu ^ 2)
            # )
            # Sz = Sy
            # ixx = 1 / 12 * h ^ 3 * b
            # iyy = 1 / 12 * b ^ 3 * h
            # Q = 1 / 3 - 0.2244 / (min(h / b, b / h) + 0.1607)
            # j = Q * min(h * b ^3, b * h ^ 3)

            
        elseif edge["material"]["profile"]=="pipe"

            Ro=edge["material"]["radius"]
            Ri=edge["material"]["radius"]-edge["material"]["thickness"]
            
            
            ixx= pi/4*(Ro^4-Ri^4) #https://calcresource.com/moment-of-inertia-ctube.html
            iyy= pi/4*(Ro^4-Ri^4)

            j=ixx+iyy
            
        end

        l0=length
        l02 = l0 * l0
        l03 = l0 * l0 * l0

        # Cm = rho * area * length /6.0
        # Ck= E * area / length 

        # m = [[2 1];[1 2]]
        # k = [[1 -1];[-1 1]]
        Ix = ixx
        rx2 = Ix/A
        L = length
        L2 = L^2

        m = zeros(12, 12)

        # FROM https://github.com/airinnova/framat/blob/master/src/framat/_element.py

        m[1+0, 1+0] = 140;
        m[1+0, 1+6] = 70; m[1+6, 1+0] = 70;
        m[1+1, 1+1] = 156;
        m[1+1, 1+5] = 22*L; m[1+5, 1+1] = 22*L;
        m[1+1, 1+7] = 54;m[1+7, 1+1] = 54;
        m[1+1, 1+11] = -13*L;m[1+11, 1+1] = -13*L;
        m[1+2, 1+2] = 156;
        m[1+2, 1+4] = -22*L;m[1+4, 1+2] = -22*L;
        m[1+2, 1+8] = 54; m[1+8, 1+2] = 54;
        m[1+2, 1+10] = 13*L; m[1+10, 1+2] = 13*L;
        m[1+3, 1+3] = 140*rx2;
        m[1+3, 1+9] = 70*rx2; m[1+9, 1+3] = 70*rx2;
        m[1+4, 1+4] = 4*L2;
        m[1+4, 1+8] = -13*L; m[1+8, 1+4] = -13*L;
        m[1+4, 1+10] = -3*L2; m[1+10, 1+4] = -3*L2;
        m[1+5, 1+5] = 4*L2;
        m[1+5, 1+7] = 13*L; m[1+7, 1+5] = 13*L;
        m[1+5, 1+11] = -3*L2; m[1+11, 1+5] = -3*L2;
        m[1+6, 1+6] = 140;
        m[1+7, 1+7] = 156;
        m[1+7, 1+11] = -22*L; m[1+11, 1+7] = -22*L;
        m[1+8, 1+8] = 156;
        m[1+8, 1+10] = 22*L;m[1+10, 1+8] = 22*L;
        m[1+9, 1+9] = 140*rx2;
        m[1+10,1+10] = 4*L2;
        m[1+11,1+11] = 4*L2;
        m = m.*(rho*A*L)/420

        

        k = [[E*A/l0  0  0  0  0  0  -E*A/l0  0  0  0  0  0];[0  12*E*ixx/l03  0  0  0  6*E*ixx/l02  0  -12*E*ixx/l03  0  0  0  6*E*ixx/l02];[0  0  12*E*iyy/l03  0  -6*E*iyy/l02  0  0  0  -12*E*iyy/l03  0  -6*E*iyy/l02  0];[0  0  0  G*j/l0  0  0  0  0  0  -G*j/l0  0  0];[0  0  -6*E*iyy/l02  0  4*E*iyy/l0  0  0  0  6*E*iyy/l02  0  2*E*iyy/l0  0];[0  6*E*ixx/l02  0  0  0  4*E*ixx/l0  0  -6*E*ixx/l02  0  0  0  2*E*ixx/l0];[-E*A/l0  0  0  0  0  0  E*A/l0  0  0  0  0  0];[0  -12*E*ixx/l03  0  0  0  -6*E*ixx/l02  0  12*E*ixx/l03  0  0  0  -6*E*ixx/l02];[0  0  -12*E*iyy/l03  0  6*E*iyy/l02  0  0  0  12*E*iyy/l03  0  6*E*iyy/l02  0];[0  0  0  -G*j/l0  0  0  0  0  0  G*j/l0  0  0];[0  0  -6*E*iyy/l02  0  2*E*iyy/l0  0  0  0  6*E*iyy/l02  0  4*E*iyy/l0  0];[0  6*E*ixx/l02  0  0  0  2*E*ixx/l0  0  -6*E*ixx/l02  0  0  0  4*E*ixx/l0]]

        # find rotated mass and stifness matrices
        tau = rotation_matrix(element_vector, x_axis,y_axis,z_axis)
        tau = get_transformation_matrix(element_vector)

        m_r=transpose(tau)*m*tau
        k_r=transpose(tau)*k*tau


        # change from element to global coordinate
        index= dofs.+1

        B=zeros((12,ndofs))
        for i in 1:12
              B[i,index[i]]=1.0
        end

        
        M_rG= transpose(B)*m_r*B
        K_rG= transpose(B)*k_r*B

        # M += Cm .* M_rG
        # K += Ck .* K_rG
        M +=  M_rG
        K +=  K_rG
        

    end

    # construct the force vector
    F=zeros(ndofs)
    remove_indices=[];
    for node in nodes
        #insert!(F,i, value);
        #F=vcat(F,value)
        i=parse(Int,node["id"][2:end])
        f=node["force"]
        # println(f)
        F[(i)*6+1]=f["x"]
        F[(i)*6+2]=f["y"]
        F[(i)*6+3]=f["z"]
        F[(i)*6+4]=0
        F[(i)*6+5]=0
        F[(i)*6+6]=0
        dofs = convert(Array{Int}, node["degrees_of_freedom"]).+1
        restrained_dofs=node["restrained_degrees_of_freedom"]
        for (index, value) in enumerate(dofs)
            if restrained_dofs[index]
                append!( remove_indices, value)
            end
        end
    end

    # println(remove_indices)
    # print(K)
    # print(F)
    # println(remove_indices)

    M = M[setdiff(1:end, remove_indices), :]
    K = K[setdiff(1:end, remove_indices), :]

    M = M[:,setdiff(1:end, remove_indices)]
    K = K[:,setdiff(1:end, remove_indices)]

    F = F[setdiff(1:end, remove_indices)]
    return M,K,F
end

function updateDisplacement(setup, X)
    nodes= setup["nodes"]
    i=1
    for node in nodes
        if !node["restrained_degrees_of_freedom"][1]
            node["displacement"]["x"]=X[i]
            i=i+1
        end
        if !node["restrained_degrees_of_freedom"][2]
            node["displacement"]["y"]=X[i]
            i=i+1
        end
        if !node["restrained_degrees_of_freedom"][3]
            node["displacement"]["z"]=X[i]
            i=i+1
        end
        if !node["restrained_degrees_of_freedom"][4]
            node["angle"]["x"]=X[i]
            i=i+1
        end
        if !node["restrained_degrees_of_freedom"][5]
            node["angle"]["y"]=X[i]
            i=i+1
        end
        if !node["restrained_degrees_of_freedom"][6]
            node["angle"]["z"]=X[i]
            i=i+1
        end
    end
end

function updateModes(setup,modeNumber)
    evecs=setup["evecs"]
    nodes= setup["nodes"]
    V=evecs[:,modeNumber]
    i=1
    for node in nodes
        if !node["restrained_degrees_of_freedom"][1]
            node["displacement"]["x"]=V[i]
            i=i+1
        end
        if !node["restrained_degrees_of_freedom"][2]
            node["displacement"]["y"]=V[i]
            i=i+1
        end
        if !node["restrained_degrees_of_freedom"][3]
            node["displacement"]["z"]=V[i]
            i=i+1
        end
        if !node["restrained_degrees_of_freedom"][4]
            node["angle"]["x"]=V[i]* 180/π
            i=i+1
        end
        if !node["restrained_degrees_of_freedom"][5]
            node["angle"]["y"]=V[i]* 180/π
            i=i+1
        end
        if !node["restrained_degrees_of_freedom"][6]
            node["angle"]["z"]=V[i]* 180/π
            i=i+1
        end
    end
    # println("here1")
    stresses=get_stresses(setup);
end


function simple_check(xs)
    any(!(x -> isnan(x) || !isfinite(x) ||x<0) ,xs)
end

function solveFea(setup)
	# determine the global matrices
	M,K,F=get_matrices(setup);
    
    #println(M)
    #println(K)
    #println(F)
    
    evals=eigvals(K,M)
    evecs=eigvecs(K,M)
    ff=simple_check.(real.(evals))
    evals=real.(evals)[ff]
    frequencies=sqrt.(evals)./pi/2
    evecs=real.(evecs[:,ff])
    setup["frequencies"]=frequencies
    setup["evecs"]=evecs
    
    # display(frequencies)
    if !haskey(setup,"getModes")
        # println(K)
        X=inv(K)*F;
        # display(X)
        updateDisplacement(setup, X);
    end
    
    
    if haskey(setup,"getModes")
        modeNumber=setup["getModes"]
        updateModes(setup,modeNumber) #save modes instead of displacements

    end

    # determine the stresses in each element
    # println("here")
    stresses=get_stresses(setup);
    
    
    return setup

end


function export_save(setup,fileName)
    # pass data as a json string (how it shall be displayed in a file)
    stringdata = JSON.json(setup)

    maxNumFiles=setup["maxNumFiles"]-1
    println(fileName*string(maxNumFiles)*".json")

    # write the file with the stringdata variable information
    open(fileName*string(maxNumFiles)*".json", "w") do f
            write(f, stringdata)
    end

    open(fileName*string(0)*".json", "w") do f
        write(f, stringdata)
    end
end