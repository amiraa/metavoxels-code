################## static forces ##########################

# function floorEnabled()
#     return false
# end

# function gravityEnabled()
#     return false
# end

# function staticSimulation()
#     return true
# end

# function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
#     return N_force
# end

# function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
#     return N_fixedDisplacement
# end

# function updateTemperature(currentRestLength,currentTimeStep,mat)
#     return currentRestLength
# end

# ################## snake forces ##########################
# function floorEnabled()
#     return setup["floorEnabled"]
# end

# function gravityEnabled()
#     return setup["gravityEnabled"]
# end

# function staticSimulation()
#     return setup["staticSimulation"]
# end

# function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
#     # a0=1.0;
#     # l=1.0 ;
#     # f=2.0;
#     # amp=0.5;
#     # x=convert(Float64,N_position.z)/(75.0*15.0);
#     # x=1.0;
#     # t=currentTimeStep/10000.0;
#     # ax=1.0-0.825*(x-1.0)+1.0625*(x*x-1.0);
#     # z= a0* ax *CUDA.sin(2.0*3.14159265359*(x/l-f*t+amp)); 
#     # totalForce =totalForce +Vector3(z,0.0,0.0);
    
#     a0=100.0;
#     l=1.0 ;
#     f=10.0;
#     amp=0.25;
#     x=convert(Float64,N_position.z)/(75.0*15.0);
#     # x=1.0;
    
#     max=10000.0;
    
    
#     m1= (mod(convert(Float64,currentTimeStep) ÷ (max/f),2.0) ==0.0)
#     m2= (mod(convert(Float64,currentTimeStep) ÷ (max/f),2.0) !=0.0)
#     t=currentTimeStep/max;
#     ax=1.0-0.825*(x-1.0)+1.0625*(x*x-1.0);
    
#     z= a0* ax *CUDA.sin(2.0*3.14159265359*(x/l-f*t+amp))
#     z1= z*  m1;
#     z2= z* -m2;

#     return Vector3(z1+z2,0.0,0.0)
# end

# ##################rover forces##########################

# function floorEnabled()
#     return true
# end

# function gravityEnabled()
#     return true
# end

# function staticSimulation()
#     return false
# end
# function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
#     if(convert(Float64,N_force.z)>0.0)
#         pi=3.14159265359

#         Bx=convert(Float64,N_position.x) 
#         By=convert(Float64,N_position.y) 
#         voxelSize=5.0*15.0
#         d=0.000000001 #magnitude

#         Ax= ( Bx ÷ voxelSize)*voxelSize
#         Ay= ( By ÷ voxelSize)*voxelSize
#         # Ay=0.0

#         x=Bx+d*(Ay-By)/CUDA.sqrt((Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By))
#         y=By-d*(Ax-Bx)/CUDA.sqrt((Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By))

#         a0=100.0;
#         l=1.0 ;
#         f=5.0;
#         amp=0.25;
#         amp=0.0;
#         max=10000.0;
#         t=currentTimeStep/max;

#         z= a0* CUDA.cos(2.0*3.14159265359*(f*t+amp))
#         amp=0.25;
#         y= a0* CUDA.sin(2.0*3.14159265359*(f*t+amp))
#         if(convert(Float64,N_force.z)>1.5)
#             z=-z
#             y=-y
#         end

#         return Vector3(0.0,y,z)
#     else
#         return Vector3(0.0,0.0,0.0)
#     end

# end

# ##################tendon forces##########################
# function floorEnabled()
#     return false
# end

# function gravityEnabled()
#     return false
# end

# function staticSimulation()
#     return true
# end

# function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
#     return Vector3(0.0,0.0,0.0);

#     # if(convert(Float64,N_force.z)>1.0)
#     #     pi=3.14159265359

#     #     Bx=convert(Float64,N_position.x) 
#     #     By=convert(Float64,N_position.y) 
#     #     voxelSize=5.0*15.0
#     #     d=0.000000001 #magnitude

#     #     Ax= ( Bx ÷ voxelSize)*voxelSize
#     #     Ay= ( By ÷ voxelSize)*voxelSize
#     #     # Ay=0.0

#     #     x=Bx+d*(Ay-By)/CUDA.sqrt((Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By))
#     #     y=By-d*(Ax-Bx)/CUDA.sqrt((Ax-Bx)*(Ax-Bx)+(Ay-By)*(Ay-By))

#     #     a0=100.0;
#     #     l=1.0 ;
#     #     f=5.0;
#     #     amp=0.25;
#     #     amp=0.0;
#     #     max=10000.0;
#     #     t=currentTimeStep/max;

#     #     z= a0* CUDA.cos(2.0*3.14159265359*(f*t+amp))
#     #     amp=0.25;
#     #     y= a0* CUDA.sin(2.0*3.14159265359*(f*t+amp))
#     #     if(convert(Float64,N_force.z)>1.5)
#     #         z=-z
#     #         y=-y
#     #     end

#     #     return Vector3(0.0,y,z)
#     # else
#     #     return N_force
#     # end

# end

# function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
#     return N_force
# end


#######################non linear test#############
# function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
#     if (currentTimeStep>20&&currentTimeStep<=400)
#         return N_force
#     else
#         return Vector3(0.0,0.0,0.0)
#     end
# end


#######################thermal expansion#############
# function updateTemperature(currentRestLength,currentTimeStep,mat)
#     if currentTimeStep<1000
#         temp=-5.0*currentTimeStep/1000
#         # currentRestLength=mat.L*(1.0+temp*mat.cTE)
#         currentRestLength=0.5*mat.L*(2.0+temp*mat.cTE)
#         # @cuprintln("currentRestLength $currentRestLength")
#         # @cuprintln("mat.cTE $(mat.cTE)")
#     elseif currentTimeStep==2500
#         temp=0
#         currentRestLength=0.5*mat.L*(2.0+temp*mat.cTE)
#         # @cuprintln("currentRestLength $currentRestLength")
#     end
#     return currentRestLength
# end
