# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

using CUDA

function run_updateNodesGPU!(dt,currentTimeStep,N_position, N_restrained,N_displacement, N_angle,N_currPosition,
        N_linMom,N_angMom,N_intForce,N_intMoment,N_force,N_fixedDisplacement,N_moment,N_orient,N_edgeID,N_edgeFirst,N_material,N_poissonStrain,
        E_intForce1,E_intMoment1,E_intForce2,E_intMoment2,E_axis, E_strain,E_material,E_maxStrain)
    N=length(N_intForce)
    numblocks = ceil(Int, N/256)
    CUDA.@sync begin
        @cuda threads=256 blocks=numblocks updateNodesGPU!(dt,currentTimeStep,N_position, N_restrained,N_displacement, 
            N_angle,N_currPosition,N_linMom,N_angMom,N_intForce,N_intMoment,N_force,N_fixedDisplacement,N_moment,N_orient,N_edgeID,N_edgeFirst,N_material,N_poissonStrain,
            E_intForce1,E_intMoment1,E_intForce2,E_intMoment2,E_axis, E_strain,E_material,E_maxStrain)
    end
end

function run_updateEdgesGPU!(dt,currentTimeStep,E_source,E_target,E_sourceNodalCoordinate,E_targetNodalCoordinate,
        E_stress,E_axis,E_currentRestLength,E_pos2,E_angle1v,E_angle2v,
        E_angle1,E_angle2,E_intForce1,E_intMoment1,E_intForce2,E_intMoment2,
        E_damp,E_smallAngle,E_material,
        E_strain,E_maxStrain,E_strainOffset,E_currentTransverseArea,E_currentTransverseStrainSum,
        N_currPosition,N_orient,N_poissonStrain)
    N=length(E_source)
    numblocks = ceil(Int, N/256)
    CUDA.@sync begin
        @cuda threads=256 blocks=numblocks updateEdgesGPU!(dt,currentTimeStep,E_source,E_target,E_sourceNodalCoordinate,E_targetNodalCoordinate,
            E_stress,E_axis,E_currentRestLength,E_pos2,E_angle1v,
            E_angle2v,E_angle1,E_angle2,E_intForce1,E_intMoment1,E_intForce2,
            E_intMoment2,E_damp,E_smallAngle,E_material,
            E_strain,E_maxStrain,E_strainOffset,E_currentTransverseArea,E_currentTransverseStrainSum,
            N_currPosition,N_orient,N_poissonStrain)
    end
end