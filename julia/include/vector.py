import math

class Vector3:
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

    def __add__(self, other):
        if isinstance(other, Vector3):
            return Vector3(self.x + other.x, self.y + other.y, self.z + other.z)
        elif isinstance(other, (int, float)):
            return Vector3(self.x + other, self.y + other, self.z + other)
        else:
            raise TypeError("Unsupported type for addition")

    def __sub__(self, other):
        if isinstance(other, Vector3):
            return Vector3(self.x - other.x, self.y - other.y, self.z - other.z)
        elif isinstance(other, (int, float)):
            return Vector3(self.x - other, self.y - other, self.z - other)
        else:
            raise TypeError("Unsupported type for subtraction")

    def __mul__(self, other):
        if isinstance(other, Vector3):
            return Vector3(self.x * other.x, self.y * other.y, self.z * other.z)
        elif isinstance(other, (int, float)):
            return Vector3(self.x * other, self.y * other, self.z * other)
        else:
            raise TypeError("Unsupported type for multiplication")

    @staticmethod
    def normalize(vector):
        length = math.sqrt(vector.x**2 + vector.y**2 + vector.z**2)
        if length > 0:
            return Vector3(vector.x / length, vector.y / length, vector.z / length)
        return Vector3()

    @staticmethod
    def dot(v1, v2):
        return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z

    @staticmethod
    def cross(v1, v2):
        x = v1.y * v2.z - v1.z * v2.y
        y = v1.z * v2.x - v1.x * v2.z
        z = v1.x * v2.y - v1.y * v2.x
        return Vector3(x, y, z)

    def addX(self, value):
        return Vector3(self.x + value, self.y, self.z)

    def addY(self, value):
        return Vector3(self.x, self.y + value, self.z)

    def addZ(self, value):
        return Vector3(self.x, self.y, self.z + value)

    def length(self):
        return math.sqrt(self.x**2 + self.y**2 + self.z**2)

    def __str__(self):
        return f"x:{self.x}, y:{self.y}, z:{self.z}"
    
class Quaternion:
    def __init__(self, x=0.0, y=0.0, z=0.0, w=1.0):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    @staticmethod
    def normalize(q):
        length = math.sqrt(q.x**2 + q.y**2 + q.z**2 + q.w**2)
        if length > 0:
            return Quaternion(q.x / length, q.y / length, q.z / length, q.w / length)
        return Quaternion()

    @staticmethod
    def multiply(q1, q2):
        x = q1.w*q2.x + q1.x*q2.w + q1.y*q2.z - q1.z*q2.y
        y = q1.w*q2.y - q1.x*q2.z + q1.y*q2.w + q1.z*q2.x
        z = q1.w*q2.z + q1.x*q2.y - q1.y*q2.x + q1.z*q2.w
        w = q1.w*q2.w - q1.x*q2.x - q1.y*q2.y - q1.z*q2.z
        return Quaternion(x, y, z, w)

    def conjugate(self):
        return Quaternion(-self.x, -self.y, -self.z, self.w)

    def __str__(self):
        return f"x:{self.x}, y:{self.y}, z:{self.z}, w:{self.w}"

# Other functions like setFromUnitVectors, quatToMatrix, etc., will be converted to either static methods or 
# standalone functions, as Python does not support extending base classes like Julia.

# Example of a standalone function
def set_from_unit_vectors(v_from, v_to):
    EPS = 0.000000001
    r = Vector3.dot(v_from, v_to) + 1

    if r < EPS:
        r = 0
        if abs(v_from.x) > abs(v_from.z):
            qx = -v_from.y
            qy = v_from.x
            qz = 0
        else:
            qx = 0
            qy = -v_from.z
            qz = v_from.y
    else:
        cross = Vector3.cross(v_from, v_to)
        qx = cross.x
        qy = cross.y
        qz = cross.z

    return Quaternion.normalize(Quaternion(qx, qy, qz, r))

class RotationMatrix:
    def __init__(self, te1=0.0, te2=0.0, te3=0.0, te4=0.0, te5=0.0, te6=0.0, te7=0.0, te8=0.0, te9=0.0, te10=0.0, te11=0.0, te12=0.0, te13=0.0, te14=0.0, te15=0.0, te16=0.0):
        self.te1 = te1
        self.te2 = te2
        self.te3 = te3
        self.te4 = te4
        self.te5 = te5
        self.te6 = te6
        self.te7 = te7
        self.te8 = te8
        self.te9 = te9
        self.te10 = te10
        self.te11 = te11
        self.te12 = te12
        self.te13 = te13
        self.te14 = te14
        self.te15 = te15
        self.te16 = te16

# Function conversions
def to_axis_x_vector3(pv, axis, sys=0):
    xaxis = Vector3(1.0, 0.0, 0.0)
    vector = Vector3.normalize(axis)
    q = set_from_unit_vectors(vector, xaxis)
    v = apply_quaternion1(pv, q)
    return Vector3(v.x, v.y, v.z)

def to_axis_original_vector3(pv, axis, sys=0):
    xaxis = Vector3(1.0, 0.0, 0.0)
    vector = Vector3.normalize(axis)
    q = set_from_unit_vectors(xaxis, vector)
    v = apply_quaternion1(pv, q)
    return Vector3(v.x, v.y, v.z)

# Other utility functions
def apply_quaternion1(e, q2):
    x, y, z = e.x, e.y, e.z
    qx, qy, qz, qw = q2.x, q2.y, q2.z, q2.w

    ix = qw * x + qy * z - qz * y
    iy = qw * y + qz * x - qx * z
    iz = qw * z + qx * y - qy * x
    iw = - qx * x - qy * y - qz * z

    xx = ix * qw + iw * - qx + iy * - qz - iz * - qy
    yy = iy * qw + iw * - qy + iz * - qx - ix * - qz
    zz = iz * qw + iw * - qz + ix * - qy - iy * - qx

    return Vector3(xx, yy, zz)

# Additional functions like RotateVec3D, RotateVec3DInv, ToRotationVector, FromRotationVector, etc., 
# would be defined in a similar manner.

# Example of another standalone function
def rotate_vec3d(a, f):
    fx, fy, fz = f.x, f.y, f.z
    tw = fx * a.x + fy * a.y + fz * a.z
    tx = fx * a.w - fy * a.z + fz * a.y
    ty = fx * a.z + fy * a.w - fz * a.x
    tz = -fx * a.y + fy * a.x + fz * a.w

    return Vector3((a.w * tx + a.x * tw + a.y * tz - a.z * ty),
                   (a.w * ty - a.x * tz + a.y * tw + a.z * tx),
                   (a.w * tz + a.x * ty - a.y * tx + a.z * tw))


def from_angle_to_pos_x(angle, rotate_from, sys=0):
    if rotate_from.x == 0.0 and rotate_from.y == 0.0 and rotate_from.z == 0.0:
        return Quaternion(angle.x, angle.y, angle.z, angle.w)

    y_over_x = rotate_from.y / rotate_from.x
    z_over_x = rotate_from.z / rotate_from.x
    small_angle_rad = 1.732e-2
    small_angle_w = 0.9999625
    pi = math.pi
    discard_angle_rad = 1e-7

    if abs(y_over_x) < small_angle_rad and abs(z_over_x) < small_angle_rad:
        x = 0.0
        y = 0.5 * z_over_x
        z = -0.5 * y_over_x
        w = 1.0 + 0.5 * (-y * y - z * z)
        return Quaternion(x, y, z, w)

    rot_from_norm = Vector3.normalize(rotate_from)
    theta = math.acos(rot_from_norm.x)

    if theta > pi - discard_angle_rad:
        return Quaternion(0.0, 1.0, 0.0, 0.0)

    axis_mag_inv = 1.0 / math.sqrt(rot_from_norm.z * rot_from_norm.z + rot_from_norm.y * rot_from_norm.y)
    a = 0.5 * theta
    s = math.sin(a)
    w = math.cos(a)

    x = 0
    y = rot_from_norm.z * axis_mag_inv * s
    z = -rot_from_norm.y * axis_mag_inv * s

    return Quaternion(x, y, z, w)

def pow(x, y):
    return math.exp(y * math.log(x))


# Utility Functions
def set_from_rotation_matrix(m, sys=0):
    m11 = m.te1
    m12 = m.te5
    m13 = m.te9
    m21 = m.te2
    m22 = m.te6
    m23 = m.te10
    m31 = m.te3
    m32 = m.te7
    m33 = m.te11

    y = math.asin(max(-1.0, min(1.0, m13)))

    if abs(m13) < 0.9999999:
        x = math.atan2(-m23, m33)
        z = math.atan2(-m12, m11)
    else:
        x = math.atan2(m32, m22)
        z = 0.0

    return Vector3(x, y, z)

def set_quaternion_from_euler(euler, sys=0):
    x, y, z = euler.x, euler.y, euler.z
    c1 = math.cos(x / 2.0)
    c2 = math.cos(y / 2.0)
    c3 = math.cos(z / 2.0)
    s1 = math.sin(x / 2.0)
    s2 = math.sin(y / 2.0)
    s3 = math.sin(z / 2.0)

    qx = s1 * c2 * c3 + c1 * s2 * s3
    qy = c1 * s2 * c3 - s1 * c2 * s3
    qz = c1 * c2 * s3 + s1 * s2 * c3
    qw = c1 * c2 * c3 - s1 * s2 * s3

    return Quaternion(qx, qy, qz, qw)

def rotate_vec3d_inv(a, f):
    fx, fy, fz = f.x, f.y, f.z
    tw = a.x * fx + a.y * fy + a.z * fz
    tx = a.w * fx - a.y * fz + a.z * fy
    ty = a.w * fy + a.x * fz - a.z * fx
    tz = a.w * fz - a.x * fy + a.y * fx
    return Vector3((tw * a.x + tx * a.w + ty * a.z - tz * a.y),
                   (tw * a.y - tx * a.z + ty * a.w + tz * a.x),
                   (tw * a.z + tx * a.y - ty * a.x + tz * a.w))

def to_rotation_vector(a, sys=0):
    x, y, z, w = a.x, a.y, a.z, a.w
    if w >= 1.0 or w <= -1.0:
        return Vector3(0.0, 0.0, 0.0)

    square_length = 1.0 - w * w
    sl_thresh_acos2sqrt = 2.4e-3

    if square_length < sl_thresh_acos2sqrt:
        factor = 2.0 * math.sqrt((2.0 - 2.0 * w) / square_length)
    else:
        factor = 2.0 * math.acos(w) / math.sqrt(square_length)

    return Vector3(x * factor, y * factor, z * factor)

def from_rotation_vector(vec_in, sys=0):
    theta = vec_in * Vector3(0.5, 0.5, 0.5)
    ntheta = math.sqrt(theta.x**2 + theta.y**2 + theta.z**2)
    theta_mag2 = ntheta * ntheta
    dbl_epsilonx24 = 5.328e-15

    if theta_mag2 * theta_mag2 < dbl_epsilonx24:
        qw = 1.0 - 0.5 * theta_mag2
        s = 1.0 - theta_mag2 / 6.0
    else:
        theta_mag = math.sqrt(theta_mag2)
        qw = math.cos(theta_mag)
        s = math.sin(theta_mag) / theta_mag

    qx, qy, qz = theta.x * s, theta.y * s, theta.z * s

    return Quaternion(qx, qy, qz, qw)

def multiply_quaternions(q, f):
    x = q.w * f.x + q.x * f.w + q.y * f.z - q.z * f.y
    y = q.w * f.y - q.x * f.z + q.y * f.w + q.z * f.x
    z = q.w * f.z + q.x * f.y - q.y * f.x + q.z * f.w
    w = q.w * f.w - q.x * f.x - q.y * f.y - q.z * f.z
    return Quaternion(x, y, z, w)

def quat_to_matrix(quaternion):
    x, y, z, w = quaternion.x, quaternion.y, quaternion.z, quaternion.w
    x2, y2, z2 = x + x, y + y, z + z
    xx, xy, xz = x * x2, x * y2, x * z2
    yy, yz, zz = y * y2, y * z2, z * z2
    wx, wy, wz = w * x2, w * y2, w * z2

    te1 = (1 - (yy + zz))
    te2 = (xy + wz)
    te3 = (xz - wy)
    te4 = 0

    te5 = (xy - wz)
    te6 = (1 - (xx + zz))
    te7 = (yz + wx)
    te8 = 0

    te9 = (xz + wy)
    te10 = (yz - wx)
    te11 = (1 - (xx + yy))
    te12 = 0

    te13 = 0  # Assuming position is always (0, 0, 0)
    te14 = 0
    te15 = 0
    te16 = 1

    return RotationMatrix(te1, te2, te3, te4, te5, te6, te7, te8, te9, te10, te11, te12, te13, te14, te15, te16)

def conjugate(q):
    return Quaternion(-q.x, -q.y, -q.z, q.w)


def rotate_vec3d(a, f):
    fx, fy, fz = f.x, f.y, f.z
    tw = fx * a.x + fy * a.y + fz * a.z
    tx = fx * a.w - fy * a.z + fz * a.y
    ty = fx * a.z + fy * a.w - fz * a.x
    tz = -fx * a.y + fy * a.x + fz * a.w

    return Vector3((a.w * tx + a.x * tw + a.y * tz - a.z * ty),
                   (a.w * ty - a.x * tz + a.y * tw + a.z * tx),
                   (a.w * tz + a.x * ty - a.y * tx + a.z * tw))

def rotate_vec3d_inv(a, f):
    fx, fy, fz = f.x, f.y, f.z
    tw = a.x * fx + a.y * fy + a.z * fz
    tx = a.w * fx - a.y * fz + a.z * fy
    ty = a.w * fy + a.x * fz - a.z * fx
    tz = a.w * fz - a.x * fy + a.y * fx

    return Vector3((tw * a.x + tx * a.w + ty * a.z - tz * a.y),
                   (tw * a.y - tx * a.z + ty * a.w + tz * a.x),
                   (tw * a.z + tx * a.y - ty * a.x + tz * a.w))

def to_rotation_vector(a, sys=0):
    if abs(a.w) > 1.0:
        a.w = 1.0 if a.w > 0 else -1.0

    square_length = 1.0 - a.w * a.w
    if square_length < 1e-8:
        return Vector3(0.0, 0.0, 0.0)

    angle = 2.0 * math.acos(a.w)
    s = math.sqrt(square_length)

    return Vector3(a.x / s * angle, a.y / s * angle, a.z / s * angle)

def from_rotation_vector(vec_in, sys=0):
    theta = math.sqrt(vec_in.x**2 + vec_in.y**2 + vec_in.z**2) / 2.0
    sin_theta = math.sin(theta)

    return Quaternion(sin_theta * vec_in.x / theta,
                      sin_theta * vec_in.y / theta,
                      sin_theta * vec_in.z / theta,
                      math.cos(theta))