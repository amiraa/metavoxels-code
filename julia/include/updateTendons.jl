function updateTendons!(sys,i,currentTimeStep, N_position, N_force, N_currPosition,tendonIds,mu)
    # mu = 0.5
    # tendonIds = [5,7,17,25,33]
    # println(tendonIds)

    if i in tendonIds
        ind = findall(x -> x == i, tendonIds)[1]
    else
        return
    end

    tension=tendonForce!(currentTimeStep)

    T = Array{Float64}(undef, length(tendonIds))
    R = Array{Vector3}(undef, length(tendonIds))

    e_out = normalizeVector3(N_currPosition[tendonIds[2]] - N_currPosition[tendonIds[1]])
    T[1] = tension
    R[1] = tension * e_out

    for j = 2:(length(tendonIds) - 1)
        e_in = normalizeVector3(N_currPosition[tendonIds[j]] - N_currPosition[tendonIds[j - 1]])
        e_out = normalizeVector3(N_currPosition[tendonIds[j + 1]] - N_currPosition[tendonIds[j]])
        theta = acos(round(dotVector3(e_in, e_out),digits=8)) / 2
        
        T[j] = T[j - 1] * (-mu * sin(theta) + cos(theta)) / (cos(theta) + mu * sin(theta))
        R[j] = T[j] * e_out - T[j - 1] * e_in
    end

    e_in = normalizeVector3(N_currPosition[tendonIds[end]] - N_currPosition[tendonIds[end - 1]])
    R[end] = - T[end - 1] * (e_in)

    # if mod(currentTimeStep, 10000) == 0 && ind == 2
    #     println(T)
    # end
    
    # N_force[i]= N_force[i]+ R[ind]
    if mod(currentTimeStep, 10000) == 0 && ind==1
        println(T)
        # println(R)
    end

    N_force[i]= R[ind]

    return
    
end

function tendonForce!(currentTimeStep)
    tension=0
    factor=60.0
    factor=60.0*2
    # Ramp tension to 95 Newtons over the first 300000 steps
    if currentTimeStep < 100000 
        tension = factor * currentTimeStep / 100000
    else
        tension = factor
    end
    return tension
end


function updateTendonsGPU!(dt,currentTimeStep,N_position,N_force,N_currPosition,tendonIds,mu)
    index = (blockIdx().x - 1) * blockDim().x + threadIdx().x
    stride = blockDim().x * gridDim().x
    ## @cuprintln("thread $index, block $stride")
    N=length(N_position)
    for i = index:stride:N
        updateTendons!(1,i,currentTimeStep,N_position,N_force,N_currPosition,tendonIds,mu)
    end
    return
end


function updateTendonsCPU!(dt,currentTimeStep,N_position,N_force,N_currPosition,tendonIds,mu)
    N=length(N_position)
    Threads.@threads for i = 1:N
        updateTendons!(0,i,currentTimeStep,N_position,N_force,N_currPosition,tendonIds,mu)
    end
    return
end