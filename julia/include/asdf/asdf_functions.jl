function getVisQuadtree(adaptive,inside,trueSDF)
    verts = GeometryTypes.Point{3, Float64}[]
    faces = GeometryTypes.Face{3, Int}[]
    count=0
    countInside=0
    for leaf in allleaves(adaptive.root)
        for face in RegionTrees.faces(leaf.boundary)
            if (inside)
                if (trueSDF(RegionTrees.center(leaf.boundary))<0.0)
                    i = length(verts)
                    for vert in face
                        push!(verts, GeometryTypes.Point(vert[1], vert[2], vert[3]))
                    end
                    push!(faces, GeometryTypes.Face(i+1, i+2, i+4))
                    push!(faces, GeometryTypes.Face(i+4, i+3, i+1))
                    countInside+=1
                end
            else
                i = length(verts)
                for vert in face
                    push!(verts, GeometryTypes.Point(vert[1], vert[2], vert[3]))
                end
                push!(faces, GeometryTypes.Face(i+1, i+2, i+4))
                push!(faces, GeometryTypes.Face(i+4, i+3, i+1))
                count+=1
            end
            
        end
    end
    if inside
        print("inside:")
        println(countInside)
    else
        print("all:")
        println(count)
    end
    return GeometryTypes.HomogenousMesh(verts, faces)
end

function getVisQuadtreeNonConvex(adaptive,inside,voxels,resolution,verts_min,verts_max)
    verts = GeometryTypes.Point{3, Float64}[]
    faces = GeometryTypes.Face{3, Int}[]
    count=0
    countInside=0
    for leaf in allleaves(adaptive.root)
        for face in RegionTrees.faces(leaf.boundary)
            if (inside)
                p=(RegionTrees.center(leaf.boundary))
                pointX=Int(round(p[1],digits=0)); pointY=Int(round(p[2],digits=0)); pointZ=Int(round(p[3],digits=0))
                if pointInsideVoxelGrid([p[1] p[2] p[3]],voxels,resolution,verts_min,verts_max)
                # if pointInsideVoxelGrid([pointX pointY pointZ],voxels,resolution,verts_min,verts_max)
                    i = length(verts)
                    for vert in face
                        push!(verts, GeometryTypes.Point(vert[1], vert[2], vert[3]))
                    end
                    push!(faces, GeometryTypes.Face(i+1, i+2, i+4))
                    push!(faces, GeometryTypes.Face(i+4, i+3, i+1))
                    countInside+=1
                end
            else
                i = length(verts)
                for vert in face
                    push!(verts, GeometryTypes.Point(vert[1], vert[2], vert[3]))
                end
                push!(faces, GeometryTypes.Face(i+1, i+2, i+4))
                push!(faces, GeometryTypes.Face(i+4, i+3, i+1))
                count+=1
            end
            
        end
    end
    if inside
        print("inside:")
        println(countInside)
    else
        print("all:")
        println(count)
    end
    return GeometryTypes.HomogenousMesh(verts, faces)
end

function getHyperCubes(adaptive,trueSDF)
    cubes=[]
    count=0
    for leaf in allleaves(adaptive.root)
        for face in RegionTrees.faces(leaf.boundary)
            if(trueSDF(RegionTrees.center(leaf.boundary))<0.0)# if inside
                #println(leaf.boundary.widths)
                append!(cubes,[leaf.boundary])
            end
            count+=1
        end
    end
    cubes=unique(cubes)
    println(count)
    return cubes
end

function getOrderedBins(adaptive,trueSDF)
    function getSDF(x)
        trueSDF(RegionTrees.center(x))
    end
    
    cubes=[]
    count=0
    for leaf in allleaves(adaptive.root)
        for face in RegionTrees.faces(leaf.boundary)
            if(trueSDF(RegionTrees.center(leaf.boundary))<0.0)# if inside
                #println(leaf.boundary.widths)
                append!(cubes,[leaf.boundary])
            end
            count+=1
        end
    end
    cubes=unique(cubes)
    
    sort!(cubes, by = x -> x.origin[2]);
    
    #put in bins by y value (down first)
    orderedY=[]
    currentY=cubes[1].origin[2];
    currentLayer=1
    append!(orderedY,[[]]);
    for cube in cubes
        if(cube.origin[2]==currentY)
            append!(orderedY[currentLayer],[cube]);
        else
            append!(orderedY,[[]]);
            currentY=cube.origin[2]
            currentLayer=currentLayer+1
            append!(orderedY[currentLayer],[cube]);
        end
    end
    
    
    #for each orderY bin put in bins according to size
    orderedYandSize=[]
    count=1
    for binY in orderedY
        append!(orderedYandSize,[[]]);
        orderedY[count]=sort(binY, rev = true,by = x -> x.widths[1])
        count+=1
    end
    #now that it is sorted in reverse put in bins
    countY=1
    for binY in orderedY
        currentSize=binY[1].widths[1]
        currentSizeLayer=1
        append!(orderedYandSize[countY],[[]]);
        for cube in binY
            if(currentSize==cube.widths[1])
                append!(orderedYandSize[countY][currentSizeLayer],[cube]);
            else
                append!(orderedYandSize[countY],[[]]);
                currentSize=cube.widths[1]
                currentSizeLayer=currentSizeLayer+1
                append!(orderedYandSize[countY][currentSizeLayer],[cube]);
            end
        end
        countY+=1
    end
    
    
    #for each orderY bin and size bin order according to distance value
    orderedYandSizeandSDF=[]
    countY=1
    for binY in orderedYandSize
        append!(orderedYandSizeandSDF,[[]]);
        countSize=1
        for binSize in binY
            append!(orderedYandSizeandSDF[countY],[[]]);
            orderedYandSize[countY][countSize]=sort(binSize,by = x -> getSDF(x))
            countSize+=1
        end
        countY+=1
    end
    #now that it is sorted sdf (in size bins in layers bins)
    countY=1
    for binY in orderedYandSize
        countSize=1
        for binSize in binY
            currentSDF=getSDF(binSize[1])
            currentSDFLayer=1
            append!(orderedYandSizeandSDF[countY][countSize],[[]]);
            for cube in binSize
                if(currentSDF==getSDF(cube))
                    append!(orderedYandSizeandSDF[countY][countSize][currentSDFLayer],[cube]);
                else
                    append!(orderedYandSizeandSDF[countY][countSize],[[]]);
                    currentSDF=getSDF(cube)
                    currentSDFLayer=currentSDFLayer+1
                    append!(orderedYandSizeandSDF[countY][countSize][currentSDFLayer],[cube]);
                end
            end
            countSize+=1 
        end
        countY+=1
    end
    
    return orderedYandSizeandSDF
   
end

function getOrderedBinsSize(adaptive,trueSDF)
    function getSDF(x)
        trueSDF(RegionTrees.center(x))
    end
    
    cubes=[]
    count=0
    for leaf in allleaves(adaptive.root)
        for face in RegionTrees.faces(leaf.boundary)
            if(trueSDF(RegionTrees.center(leaf.boundary))<0.0)# if inside
                #println(leaf.boundary.widths)
                append!(cubes,[leaf.boundary])
            end
            count+=1
        end
    end
    cubes=unique(cubes)
    
    sort!(cubes, rev = true, by = x -> x.widths[1]);
    
    #put in bins by y value (down first)
    orderedY=[]
    currentY=cubes[1].widths[1];
    currentLayer=1
    append!(orderedY,[[]]);
    for cube in cubes
        if(cube.widths[1]==currentY)
            append!(orderedY[currentLayer],[cube]);
        else
            append!(orderedY,[[]]);
            currentY=cube.widths[1]
            currentLayer=currentLayer+1
            append!(orderedY[currentLayer],[cube]);
        end
    end
    
    
    #for each orderY bin put in bins according to size
    orderedYandSize=[]
    count=1
    for binY in orderedY
        append!(orderedYandSize,[[]]);
        orderedY[count]=sort(binY,by = x -> x.origin[2])
        count+=1
    end
    #now that it is sorted in reverse put in bins
    countY=1
    for binY in orderedY
        currentSize=binY[1].origin[2]
        currentSizeLayer=1
        append!(orderedYandSize[countY],[[]]);
        for cube in binY
            if(currentSize==cube.origin[2])
                append!(orderedYandSize[countY][currentSizeLayer],[cube]);
            else
                append!(orderedYandSize[countY],[[]]);
                currentSize=cube.origin[2]
                currentSizeLayer=currentSizeLayer+1
                append!(orderedYandSize[countY][currentSizeLayer],[cube]);
            end
        end
        countY+=1
    end
    
    
    #for each orderY bin and size bin order according to distance value
    orderedYandSizeandSDF=[]
    countY=1
    for binY in orderedYandSize
        append!(orderedYandSizeandSDF,[[]]);
        countSize=1
        for binSize in binY
            append!(orderedYandSizeandSDF[countY],[[]]);
            orderedYandSize[countY][countSize]=sort(binSize,by = x -> getSDF(x))
            countSize+=1
        end
        countY+=1
    end
    #now that it is sorted sdf (in size bins in layers bins)
    countY=1
    for binY in orderedYandSize
        countSize=1
        for binSize in binY
            currentSDF=getSDF(binSize[1])
            currentSDFLayer=1
            append!(orderedYandSizeandSDF[countY][countSize],[[]]);
            for cube in binSize
                if(currentSDF==getSDF(cube))
                    append!(orderedYandSizeandSDF[countY][countSize][currentSDFLayer],[cube]);
                else
                    append!(orderedYandSizeandSDF[countY][countSize],[[]]);
                    currentSDF=getSDF(cube)
                    currentSDFLayer=currentSDFLayer+1
                    append!(orderedYandSizeandSDF[countY][countSize][currentSDFLayer],[cube]);
                end
            end
            countSize+=1 
        end
        countY+=1
    end
    
    return orderedYandSizeandSDF
   
end

function mapp(value, x1, y1, x2, y2)
	return (value .- x1) .* (y2 .- x2) ./ (y1 .- x1) .+ x2;
end

function meshgrid(vx::AbstractVector{T}, vy::AbstractVector{T},vz::AbstractVector{T}) where {T}
  m, n, o = length(vy), length(vx), length(vz)
  vx = reshape(vx, 1, n, 1)
  vy = reshape(vy, m, 1, 1)
  vz = reshape(vz, 1, 1, o)
  om = ones(Int, m)
  on = ones(Int, n)
  oo = ones(Int, o)
  return hcat(vx[om, :, oo][:], vy[:, on, oo][:], vz[om, on, :][:])
end

function _voxelize(mesh, resolution = 32,empty=false)
    v=GeometryTypes.vertices(mesh)
    f=GeometryTypes.faces(mesh)
    f=reshape(Int.(collect(Iterators.flatten(Array.(f)))),3,size(f)[1])
    v=reshape(collect(Iterators.flatten(Array.(v))),3,size(v)[1])'
    verts_max=[maximum(v'[1,:,:]) maximum(v'[2,:,:]) maximum(v'[3,:,:])]
    verts_min=[minimum(v'[1,:,:]) minimum(v'[2,:,:]) minimum(v'[3,:,:])]
    verts = ((v .- verts_min) ./ (verts_max - verts_min))
    


    points = verts
    smallest_side = (1.0 / resolution)^2

    v1 = verts[ f[1, :],:]'
    v2 = verts[ f[2, :],:]'
    v3 = verts[ f[3, :],:]'

    points = verts'

    while true
        side_1 = sum((v1 .- v2) .^ 2, dims = 1)
        side_2 = sum((v2 .- v3) .^ 2, dims = 1)
        side_3 = sum((v3 .- v1) .^ 2, dims = 1)
        sides = cat(side_1, side_2, side_3, dims = 1)
        sides = reshape(maximum(sides, dims = 1), :)

        keep = sides .> smallest_side
        !(any(keep)) && break

        v1 = v1[:, keep]
        v2 = v2[:, keep]
        v3 = v3[:, keep]

        v4 = (v1 + v3) / 2
        v5 = (v1 + v2) / 2
        v6 = (v2 + v3) / 2

        points = cat(points, v4, v5, v6, dims = 2)
        vertex_set = [v1, v2, v3, v4, v5, v6]
        new_traingles = [
            1 4 5
            5 2 6
            5 4 6
            4 3 6
        ]
        new_verts = []
        for i = 1:4
            for j = 1:3
                if i == 1
                    push!(new_verts, vertex_set[new_traingles[i, j]])
                else
                    new_verts[j] =
                        cat(new_verts[j], vertex_set[new_traingles[i, j]], dims = 2)
                end
            end
        end

        v1, v2, v3 = new_verts
    end

    voxels = zeros(resolution, resolution, resolution)
    idx =
        (
            (round.(Int, points .* (resolution - 1), RoundToZero) .+ resolution) .%
            resolution
        ) .+ 1
    voxels[CartesianIndex.(idx[1, :], idx[2, :], idx[3, :])] .= 1
    if !empty
        res=resolution
        newVoxels=zeros(res+2,res+2,res+2)
        newVoxels[2:end-1,2:end-1,2:end-1]=copy(voxels)
        cnewVoxels=copy(newVoxels)
        for x=1:res+2
            for y=1:res+2
                count=0
                preval=0
                for z=1:res+2
                    if(cnewVoxels[x,y,z]==0)
                        if(count%2==0)
                            newVoxels[x,y,z]=1
                        end
                    else
                        if preval!=1
                            count+=1
                        end

                    end
                    preval=cnewVoxels[x,y,z]

                end
                z=res+2
                if newVoxels[x,y,z]==0
                    on=true
                    for z=res+2:-1:1
                        if on
                            if newVoxels[x,y,z]==0
                                newVoxels[x,y,z]=1
                            else
                                on=false
                            end
                        end
                    end
                end
            end
        end
    
        newVoxels=1.0 .-newVoxels
        voxels=newVoxels[2:end-1,2:end-1,2:end-1]

        # CI=findall(Bool.(newVoxels))
        # xs=[ i[1] for i in CI ];
        # ys=[ i[2] for i in CI ];
        # zs=[ i[3] for i in CI ];
        # newVoxels1=newVoxels[minimum(xs):maximum(xs), minimum(ys):maximum(ys),minimum(zs):maximum(zs)];
    end

    shift=verts_min
    scale=verts_max - verts_min
    return voxels,verts_min,verts_max
end

function pointsInMesh(points,mesh, resolution = 32,empty=false)
    voxels,verts_min,verts_max=_voxelize(mesh, resolution,empty);
    return pointsInsideVoxelGrid(points,voxels,resolution,verts_min,verts_max);
    
end

function pointsInsideVoxelGrid(points,voxels,resolution,verts_min,verts_max) #point [x y z]

    # step=1/resolution
    # x=verts_min[1]:abs(verts_max[1]-verts_min[1])/resolution:verts_max[1]
    # y=verts_min[2]:abs(verts_max[2]-verts_min[2])/resolution:verts_max[2]
    # z=verts_min[3]:abs(verts_max[3]-verts_min[3])/resolution:verts_max[3]
    # x=x[1:resolution]
    # y=y[1:resolution];
    # z=z[1:resolution];

    # m=meshgrid(x,y,z);
    # cc=Int.(round.(mapp(m, verts_min, verts_max, [1 1 1], [resolution resolution resolution]),digits=0));
    # on=voxels[CartesianIndex.(cc[:,1],cc[:,2],cc[:,3])];
    # points=m[Bool.(on),:];

    cc=Int.(round.(mapp(points, verts_min, verts_max, [1 1 1], [resolution resolution resolution]),digits=0));
    on=fill(false,size(points)[1])
    # if cc[:,1].>=1 && cc[:,1].<=resolution && cc[:,2].>=1 && cc[:,2].<=resolution &&cc[:,3].>=1 && cc[:,3].<=resolution
        on=voxels[CartesianIndex.(cc[:,1],cc[:,2],cc[:,3])];
    # end
    return Bool.(on)
    
end

function pointInsideVoxelGrid(point,voxels,resolution,verts_min,verts_max) #point [x y z]



    cc=Int.(round.(mapp(point, verts_min, verts_max, [1 1 1], [resolution resolution resolution]),digits=0))';
    on=false
    if cc[1].>=1 && cc[1].<=resolution && cc[2].>=1 && cc[2].<=resolution &&cc[3].>=1 && cc[3].<=resolution
        on=voxels[CartesianIndex.(cc[1],cc[2],cc[3])];
    end
    return Bool.(on)
    
end

