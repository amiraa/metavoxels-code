

function signed_distance1(mesh, point)
    verts = GeometryTypes.vertices(mesh)
    fs = GeometryTypes.faces(mesh)
    m=[ReferenceDistance.interior_distance(verts[f], point) for f in fs]
    int_dist = maximum(m[.!isnan.(m)])
    if int_dist <= 0
        return int_dist
    else
        m=[ReferenceDistance.exterior_distance(verts[f], point) for f in fs]
        ext_dist = minimum(m[.!isnan.(m)])
        return ext_dist
    end
end

function signed_distance1(mesh)
    point -> signed_distance1(mesh, point )
end


function adjustedSignedDistance(mesh, point)
    tol=0.00
    verts = GeometryTypes.vertices(mesh)
    fs = GeometryTypes.faces(mesh)
    int=false
    m=[ReferenceDistance.interior_distance(verts[f], point) for f in fs]
    int_dist = maximum(m[.!isnan.(m)])
    if int_dist <= 0
        int=true
    end
    int_dist = minimum(m[.!isnan.(m)])
    if int_dist >= 0
        if int
            int_dist = minimum([int_dist,tol])
        end
        return -int_dist
    else
        m=[ReferenceDistance.exterior_distance(verts[f], point) for f in fs]
        ext_dist = minimum(m[.!isnan.(m)])
        if int
            ext_dist = minimum([ext_dist,tol])
        end
        return ext_dist
    end
end

function adjustedSignedDistance(mesh )
    point -> adjustedSignedDistance(mesh, point )
end


function exteriorDistanceConcave(mesh,voxels,resolution,verts_min,verts_max,point)
    dist=adjustedSignedDistance(mesh, point)

    if pointInsideVoxelGrid([point[1] point[2] point[3]],voxels,resolution,verts_min,verts_max)
        return 0.0;
    else
        return 1.0;
    end
end

function exteriorDistanceConcave(mesh,voxels,resolution,verts_min,verts_max)
    point -> exteriorDistanceConcave(mesh, voxels, resolution,verts_min,verts_max, point)
end

function signedDistanceConcave(mesh,voxels,resolution,verts_min,verts_max,point)
    dist=adjustedSignedDistance(mesh, point)

    if pointInsideVoxelGrid([point[1] point[2] point[3]],voxels,resolution,verts_min,verts_max)
        return -abs(dist);
    else
        return abs(dist);
    end
end

function signedDistanceConcave(mesh,voxels,resolution,verts_min,verts_max)
    point -> signedDistanceConcave(mesh, voxels, resolution,verts_min,verts_max, point)
end


