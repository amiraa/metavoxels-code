# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

function updateDataAndSave!(metavoxel,setup,fileName,sys="GPU")
    nodes      = setup["nodes"]
    edges      = setup["edges"]
    
    setup["animation"]["showDisplacement"]=false
    voxCount=size(nodes)[1]
    linkCount=size(edges)[1]
    if (sys=="GPU")
        N_displacement=Array(metavoxel["N_displacementGPU"])
        N_position=Array(metavoxel["N_positionGPU"])
        N_angle=Array(metavoxel["N_angleGPU"])
        E_stress=Array(metavoxel["E_stressGPU"])
        N_vel=Array(metavoxel["N_velGPU"])
        N_velAngl=Array(metavoxel["N_velAnglGPU"])
        N_linMom=Array(metavoxel["N_linMomGPU"])
        N_angMom=Array(metavoxel["N_angMomGPU"])
        N_material=Array(metavoxel["N_materialGPU"])
    else
        # CPU
        N_displacement=Array(metavoxel["N_displacement"])
        N_position=Array(metavoxel["N_position"])
        N_angle=Array(metavoxel["N_angle"])
        E_stress=Array(metavoxel["E_stress"])
        N_vel=Array(metavoxel["N_vel"])
        N_velAngl=Array(metavoxel["N_velAngl"])
        N_linMom=Array(metavoxel["N_linMom"])
        N_angMom=Array(metavoxel["N_angMom"])
        N_material=Array(metavoxel["N_material"])
    end
    
    setup["viz"]["maxStress"]=maximum(E_stress)
    setup["viz"]["minStress"]=minimum(E_stress)
    setup["animation"]["exaggeration"]=1.0

    i=1
	for edge in edges
        edge["stress"]=E_stress[i]
        i=i+1

    end

    scale1=1.0
    if haskey(setup,"scale")
        scale1=setup["scale"]
    end
    scale1=1.0
    scale2=1.0
    scale=scale1/scale2

    # if haskey(setup,"voxelSize")
    #     setup["voxelSize"]=setup["voxelSize"]*scale1
    # end

    
    if haskey(setup,"supports")
        supports      = setup["supports"]
        for support in supports
            support[1]["min"]["x"]=support[1]["min"]["x"]*scale1
            support[1]["min"]["y"]=support[1]["min"]["y"]*scale1
            support[1]["min"]["z"]=support[1]["min"]["z"]*scale1
            support[1]["max"]["x"]=support[1]["max"]["x"]*scale1
            support[1]["max"]["y"]=support[1]["max"]["y"]*scale1
            support[1]["max"]["z"]=support[1]["max"]["z"]*scale1
        end
    end

    if haskey(setup,"loads")
        loads         = setup["loads"]
        for load in loads
            load[1]["min"]["x"]=load[1]["min"]["x"]*scale1
            load[1]["min"]["y"]=load[1]["min"]["y"]*scale1
            load[1]["min"]["z"]=load[1]["min"]["z"]*scale1
            load[1]["max"]["x"]=load[1]["max"]["x"]*scale1
            load[1]["max"]["y"]=load[1]["max"]["y"]*scale1
            load[1]["max"]["z"]=load[1]["max"]["z"]*scale1
        end
    end

    if haskey(setup,"materials")
        materials         = setup["materials"]
        for material in materials
            material[1]["min"]["x"]=material[1]["min"]["x"]*scale1
            material[1]["min"]["y"]=material[1]["min"]["y"]*scale1
            material[1]["min"]["z"]=material[1]["min"]["z"]*scale1
            material[1]["max"]["x"]=material[1]["max"]["x"]*scale1
            material[1]["max"]["y"]=material[1]["max"]["y"]*scale1
            material[1]["max"]["z"]=material[1]["max"]["z"]*scale1
        end
    end

    if haskey(setup,"fixedDisplacements")
        disps         = setup["fixedDisplacements"]
        for disp in disps
            disp[1]["min"]["x"]=disp[1]["min"]["x"]*scale1
            disp[1]["min"]["y"]=disp[1]["min"]["y"]*scale1
            disp[1]["min"]["z"]=disp[1]["min"]["z"]*scale1
            disp[1]["max"]["x"]=disp[1]["max"]["x"]*scale1
            disp[1]["max"]["y"]=disp[1]["max"]["y"]*scale1
            disp[1]["max"]["z"]=disp[1]["max"]["z"]*scale1
        end
    end
 
    i=1          
	for node in nodes
        node["posTimeSteps"]=[]
        node["angTimeSteps"]=[]

        node["vel"]=Dict()
        node["vel"]["x"]= (N_vel[i].x)*scale1
        node["vel"]["y"]= (N_vel[i].y)*scale1
        node["vel"]["z"]= (N_vel[i].z)*scale1

        node["linMom"]=Dict()
        node["linMom"]["x"]= (N_linMom[i].x)*scale1
        node["linMom"]["y"]= (N_linMom[i].y)*scale1
        node["linMom"]["z"]= (N_linMom[i].z)*scale1

        node["velAngl"]=Dict()
        node["velAngl"]["x"]= (N_velAngl[i].x)
        node["velAngl"]["y"]= (N_velAngl[i].y)
        node["velAngl"]["z"]= (N_velAngl[i].z)

        node["angMom"]=Dict()
        node["angMom"]["x"]= (N_angMom[i].x)
        node["angMom"]["y"]= (N_angMom[i].y)
        node["angMom"]["z"]= (N_angMom[i].z)

        node["material"]["massInverse"]= (N_material[i].massInverse)
        node["material"]["momentInertiaInverse"]= (N_material[i].momentInertiaInverse)

        # node["degrees_of_freedom"]=""
        
        if !haskey(setup,"multiscale") || node["parent"]==""
            node["position"]["x"]= (N_position[i].x)*scale
            node["position"]["y"]= (N_position[i].y)*scale
            node["position"]["z"]= (N_position[i].z)*scale


            node["displacement"]["x"]= N_displacement[i].x*scale
            node["displacement"]["y"]= N_displacement[i].y*scale
            node["displacement"]["z"]= N_displacement[i].z*scale
            
            
            node["angle"]["x"]= N_angle[i].x
            node["angle"]["y"]= N_angle[i].y
            node["angle"]["z"]= N_angle[i].z

            
        else
            # node["position"]["x"]= node["position"]["x"]*scale
            # node["position"]["y"]= node["position"]["y"]*scale
            # node["position"]["z"]= node["position"]["z"]*scale

            # todo ? make sure this is correct

            # println(node["parent"])
            # println("parent: $(node["parent"]), id: $(node["id"]), scale: $(node["scale"])" )


            parent=parse(Int,node["parent"][2:end])+1
            index=parse(Int,node["id"][2:end])
            # relPos=Vector3(node["position"]["x"]/scale/2,node["position"]["y"]/scale/2,node["position"]["z"]/scale/2)
            relPos=getRelativePosition(index,setup["voxelSize"]/2.0)
            # relPos=Vector3(0,0,0)

            displacementss=Vector3(convert(Float64,N_displacement[parent].x),convert(Float64,N_displacement[parent].y),convert(Float64,N_displacement[parent].z))
            angless=Vector3(convert(Float64,N_angle[parent].x),convert(Float64,N_angle[parent].y),convert(Float64,N_angle[parent].z))
            # println(angless)
            # println(displacementss)
            displacementss=displacementss+applyQuaternion1(relPos,setQuaternionFromEuler1(angless))-relPos
            # println(displacementss)
            # relPos=getRelativePosition(index,setup["voxelSize"]/2.0)
            # println( angless)
            # println( applyQuaternion1(relPos,setQuaternionFromEuler1(angless)) )
            # relPos=getRelativePosition(index,setup["voxelSize"]/2.0)
            # println( applyQuaternion1(relPos,setQuaternionFromEuler(ToRotationVector(N_orient[parent]))) )

            


            node["displacement"]["x"]= displacementss.x*scale
            node["displacement"]["y"]= displacementss.y*scale
            node["displacement"]["z"]= displacementss.z*scale
            
            
            node["angle"]["x"]= angless.x
            node["angle"]["y"]= angless.y
            node["angle"]["z"]= angless.z

            
        end

        i=i+1
    end

    
    # pass data as a json string (how it shall be displayed in a file)
    stringdata = JSON.json(setup)

    # write the file with the stringdata variable information
    open(fileName, "w") do f
            write(f, stringdata)
    end
    
end

####################utils#############################

function getSetup(fileName)
    setup = Dict()
    name=string("../json/$(fileName)",".json")
    open(name, "r") do f
       #global setup
        dicttxt = String(read(f))  # file information to string
        setup=JSON.parse(dicttxt)  # parse and transform data
    end
    setup=setup["setup"]
    setup["viz"]["colorMaps"]=""
    return setup
end


function getSetupFullPath(path)
    setup = Dict()
    open(path, "r") do f
       #global setup
        dicttxt = String(read(f))  # file information to string
        setup=JSON.parse(dicttxt)  # parse and transform data
    end
    setup=setup["setup"]
    setup["viz"]["colorMaps"]=""
    return setup
end

function getSetup1(fileName)
    setup = Dict()
    # name=string("../json/setupTestUni$latticeSize",".json")
    name=string("../json/$(fileName)",".json")
    open(name, "r") do f
       #global setup
        dicttxt = String(read(f))  # file information to string
        setup=JSON.parse(dicttxt)  # parse and transform data
    end
    return setup
end

function getSetup1FullPath(path)
    setup = Dict()

    open(path, "r") do f
       #global setup
        dicttxt = String(read(f))  # file information to string
        setup=JSON.parse(dicttxt)  # parse and transform data
    end
    return setup
end

function exportJuliaSettingsUsingNode(setup,name)
    #export prev. settings to json
    fileName="../json/$(name)Init.json"
    setup1=Dict()
    setup1["setup"]=setup
    stringdata = JSON.json(setup1)
    open(fileName, "w") do f
            write(f, stringdata)
    end
    #run node.js to draw the gerometry using rhino3dm
    mycommand = `node ../node/app1.js $(name)`
    run(mycommand)
end

function getFinalDisplacement(path,id,axis)
    setupViz=getSetup1FullPath("$(path)0.json")
    maxNumFiles=setupViz["maxNumFiles"]
    setupVizLast=getSetup1FullPath("$(path)$(Int(maxNumFiles-1)).json");
    nodes=setupVizLast["nodes"]
    node=setupVizLast["nodes"][id]
    dis=node["displacement"][axis]
    for node in nodes
        if (node["displacement"][axis]>dis)
            dis=node["displacement"][axis]
        end
    end
    return dis
end

#########################################
