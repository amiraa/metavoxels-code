using Plots: display
# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021

# Based on https://github.com/IainHaslam/exolete

#################################################################

using LinearAlgebra

#2d plot
using Plots

#3d 
using GLMakie
# using Makie

if GPU
    using CUDA;
    include("./lbmGPU.jl");
end

using BenchmarkTools
using Test # do I need?
using FileIO, ImageIO, Colors, FixedPointNumbers, Images
using ConcaveHull
using Luxor

# Makie.AbstractPlotting.inline!(true)

jet_me=Plots.cgrad([:blue,:cyan,:yellow,:red,:black])
# jet_me=Plots.ColorGradient([:blue,:cyan,:yellow,:red,:black])

##########################################################################


##########################################################################
function lbm2D(nx,ny,maxIter,setboundary2D,radius)

    anim = Plots.Animation()
    #*
    # configuration
    #*
    omega = 1.0;
    density = 1.0;
    fraction = 0.1 # average fraction of occupied nodes
    
    maxIter2=100


    deltaU=1e-7;

    #*
    # setup of variables
    #*
    avu=1; prevavu=1;
    ts=0;
    avus = zeros(Float64, maxIter)

    # single-particle distribution function
    # particle densities conditioned on one of the 9 possible velocities
    F = repeat([density/9], outer= [nx, ny, 9]);
    FEQ = F;
    
    BOUND,ON,numactivenodes,TO_REFLECT,REFLECTED=setboundary2D(nx,ny,radius);

    
    #*
    # constants
    #*
    t1 = 4/9;
    t2 = 1/9;
    t3 = 1/36;
    c_squ = 1/3;
    count=0

    UX=zeros(nx,ny);
    UY=zeros(nx,ny);


    #*
    # main loop
    #*
    # anim = @animate while ((ts<4000) & (1e-10 < abs((prevavu-avu)/avu))) | (ts<100)
    while ((ts<maxIter) & (1e-10 < abs((prevavu-avu)/avu))) | (ts<maxIter2)
        #*
        # Streaming
        #*
        
        # particles at (x,y)=[2,1] were at [1, 1] before: 1 points right (1,0)
        F[:,:,1] = F[[nx; 1:nx-1],:,1];
        
        # particles at [1,2] were at [1, 1] before: 3 points up (0,1)
        F[:,:,3] = F[:,[ny; 1:ny-1],3];
        
        # particles at [1,1] were at [2, 1] before: 5 points left (-1,0)
        F[:,:,5] = F[[2:nx; 1],:,5];
        
        # particles at [1,1] were at [1, 2] before: 7 points down (0,-1)
        F[:,:,7] = F[:,[2:ny; 1],7];
        
        
        # particles at [2,2] were at [1, 1] before: 2 points to (1,1)
        F[:,:,2] = F[[nx; 1:nx-1],[ny; 1:ny-1],2];
        
        # particles at [1,2] were at [2, 1] before: 4 points to (-1, 1)
        F[:,:,4] = F[[2:nx; 1],[ny; 1:ny-1],4];
        
        # particles at [1,1] were at [2, 2] before: 6 points to (-1, -1)
        F[:,:,6] = F[[2:nx; 1],[2:ny; 1],6];
        
        # particles at [2,1] were at [1, 2] before: 8 points down (1,-1)
        F[:,:,8] = F[[nx; 1:nx-1],[2:ny; 1],8];
        
        DENSITY = sum(F,dims=3);
        
        # 1,2,8 are moving to the right, 4,5,6 to the left
        # 3, 7 and 9 don't move in the x direction
        UX = (sum(F[:,:,[1, 2, 8]],dims=3)-sum(F[:,:,[4, 5, 6]],dims=3))./DENSITY;
        # 2,3,4 are moving up, 6,7,8 down
        # 1, 5 and 9 don't move in the y direction
        UY = (sum(F[:,:,[2, 3, 4]],dims=3)-sum(F[:,:,[6, 7, 8]],dims=3))./DENSITY;
        
        UX[1,1:ny] = UX[1,1:ny] .+ deltaU; #Increase inlet pressure
        
        UX[ON] .= 0; UY[ON] .= 0; DENSITY[ON] .= 0;
        
        U_SQU = UX.^2+UY.^2;
        U_C2 = UX+UY;
        U_C4 = -UX+UY;
        U_C6 = -U_C2;
        U_C8 = -U_C4;
        
        # Calculate equilibrium distribution: stationary (a = 0)
        FEQ[:,:,9] = t1*DENSITY.*(1 .-U_SQU/(2*c_squ));
        
        # nearest-neighbours
        FEQ[:,:,1] = t2*DENSITY.*(1 .+UX/c_squ+0.5*(UX/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,3] = t2*DENSITY.*(1 .+UY/c_squ+0.5*(UY/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,5] = t2*DENSITY.*(1 .-UX/c_squ+0.5*(UX/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,7] = t2*DENSITY.*(1 .-UY/c_squ+0.5*(UY/c_squ).^2-U_SQU/(2*c_squ));
        
        # next-nearest neighbours
        FEQ[:,:,2] = t3*DENSITY.*(1 .+U_C2/c_squ+0.5*(U_C2/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,4] = t3*DENSITY.*(1 .+U_C4/c_squ+0.5*(U_C4/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,6] = t3*DENSITY.*(1 .+U_C6/c_squ+0.5*(U_C6/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,8] = t3*DENSITY.*(1 .+U_C8/c_squ+0.5*(U_C8/c_squ).^2-U_SQU/(2*c_squ));
        
        BOUNCEDBACK = F[TO_REFLECT]; #Densities bouncing back at next timestep
        
        F = omega*FEQ + (1-omega)*F;
        
        F[REFLECTED] = BOUNCEDBACK;
        
        prevavu = avu;
        avu = sum(sum(UX))/numactivenodes;
        avus[ts+1] = avu;
        ts = ts+1;
        if ts%20==0 &&ts>maxIter-500
            # display2D(UX,UY,ts)
            # frame(anim)
            # count=count+1
        end
        
    end
    # toc()

    # display2D(UX,UY,ts)

    return anim, UX,UY


end

function lbm2D1(nx,ny,maxIter,setboundary2D,radius,dynamic=false,verbose=false,saveEvery=10)

    anim = Plots.Animation()
    animF = Plots.Animation()
    #*
    # configuration
    #*
    omega = 1.0;
    density = 1.0;
    fraction = 0.1 # average fraction of occupied nodes
    
    maxIter2=100


    deltaU=1e-7;
    deltaU=0.1;

    #*
    # setup of variables
    #*
    avu=1; prevavu=1;
    ts=0;
    avus = zeros(Float64, maxIter)

    # single-particle distribution function
    # particle densities conditioned on one of the 9 possible velocities
    F = repeat([density/9], outer= [nx, ny, 9]);
    FEQ = F;
    
    BOUND,ON,numactivenodes,TO_REFLECT,REFLECTED=setboundary2D(nx,ny,radius,0);

    
    #*
    # constants
    #*
    t1 = 4/9;
    t2 = 1/9;
    t3 = 1/36;
    c_squ = 1/3;
    count=0

    UX=zeros(nx,ny);
    UY=zeros(nx,ny);


    #*
    # main loop
    #*
    # anim = @animate while ((ts<4000) & (1e-10 < abs((prevavu-avu)/avu))) | (ts<100)
    while ((ts<maxIter) & (1e-10 < abs((prevavu-avu)/avu))) | (ts<maxIter2)
        #*
        # Streaming
        #*
        if dynamic
            BOUND,ON,numactivenodes,TO_REFLECT,REFLECTED=setboundary2D(nx,ny,radius,ts);
        end
        
        # particles at (x,y)=[2,1] were at [1, 1] before: 1 points right (1,0)
        # F[:,:,1] = F[[nx; 1:nx-1],:,1];
        F[:,:,1] = F[[1; 1:nx-1],:,1];
        
        # particles at [1,2] were at [1, 1] before: 3 points up (0,1)
        F[:,:,3] = F[:,[ny; 1:ny-1],3];
        
        # particles at [1,1] were at [2, 1] before: 5 points left (-1,0)
        # F[:,:,5] = F[[2:nx; 1],:,5];
        F[:,:,5] = F[[2:nx; nx],:,5];
        
        # particles at [1,1] were at [1, 2] before: 7 points down (0,-1)
        F[:,:,7] = F[:,[2:ny; 1],7];
        
        
        # particles at [2,2] were at [1, 1] before: 2 points to (1,1)
        # F[:,:,2] = F[[nx; 1:nx-1],[ny; 1:ny-1],2];
        F[:,:,2] = F[[1; 1:nx-1],[ny; 1:ny-1],2];

        
        # particles at [1,2] were at [2, 1] before: 4 points to (-1, 1)
        # F[:,:,4] = F[[2:nx; 1],[ny; 1:ny-1],4];
        F[:,:,4] = F[[2:nx; nx],[ny; 1:ny-1],4];
        
        # particles at [1,1] were at [2, 2] before: 6 points to (-1, -1)
        # F[:,:,6] = F[[2:nx; 1],[2:ny; 1],6];
        F[:,:,6] = F[[2:nx; nx],[2:ny; 1],6];
        
        # particles at [2,1] were at [1, 2] before: 8 points down (1,-1)
        # F[:,:,8] = F[[nx; 1:nx-1],[2:ny; 1],8];
        F[:,:,8] = F[[1; 1:nx-1],[2:ny; 1],8];
        
        DENSITY = sum(F,dims=3);
        
        # 1,2,8 are moving to the right, 4,5,6 to the left
        # 3, 7 and 9 don't move in the x direction
        UX = (sum(F[:,:,[1, 2, 8]],dims=3)-sum(F[:,:,[4, 5, 6]],dims=3))./DENSITY;
        # 2,3,4 are moving up, 6,7,8 down
        # 1, 5 and 9 don't move in the y direction
        UY = (sum(F[:,:,[2, 3, 4]],dims=3)-sum(F[:,:,[6, 7, 8]],dims=3))./DENSITY;
        
        
        # UX[1,1:ny] = UX[1,1:ny] .+ deltaU; #Increase inlet pressure
        UX[2,1:ny].=deltaU;
        UY[2,1:ny].=0;
        DENSITY[2,1:ny].=1;

        # UX[2:nx,2].=deltaU;
        # UY[2:nx,2].=0;
        # DENSITY[1:nx,2].=1;

        # UX[1:nx,1].=deltaU;
        # UY[1:nx,1].=0;
        # DENSITY[1:nx,1].=1;
        
        UX[ON] .= 0;
        UY[ON] .= 0;
        DENSITY[ON] .= 1;
        
        U_SQU = UX.^2+UY.^2;
        U_C2 = UX+UY;
        U_C4 = -UX+UY;
        U_C6 = -U_C2;
        U_C8 = -U_C4;
        
        # Calculate equilibrium distribution: stationary (a = 0)
        FEQ[:,:,9] = t1*DENSITY.*(1 .-U_SQU/(2*c_squ));
        
        # nearest-neighbours
        FEQ[:,:,1] = t2*DENSITY.*(1 .+UX/c_squ+0.5*(UX/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,3] = t2*DENSITY.*(1 .+UY/c_squ+0.5*(UY/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,5] = t2*DENSITY.*(1 .-UX/c_squ+0.5*(UX/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,7] = t2*DENSITY.*(1 .-UY/c_squ+0.5*(UY/c_squ).^2-U_SQU/(2*c_squ));
        
        # next-nearest neighbours
        FEQ[:,:,2] = t3*DENSITY.*(1 .+U_C2/c_squ+0.5*(U_C2/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,4] = t3*DENSITY.*(1 .+U_C4/c_squ+0.5*(U_C4/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,6] = t3*DENSITY.*(1 .+U_C6/c_squ+0.5*(U_C6/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,8] = t3*DENSITY.*(1 .+U_C8/c_squ+0.5*(U_C8/c_squ).^2-U_SQU/(2*c_squ));
        
        BOUNCEDBACK = F[TO_REFLECT]; #Densities bouncing back at next timestep
        
        F = omega*FEQ + (1-omega)*F;
        
        F[REFLECTED] = BOUNCEDBACK;
        
        prevavu = avu;
        avu = sum(sum(UX))/numactivenodes;
        # avus[ts+1] = avu;
        ts = ts+1;
        if ts%saveEvery==0
            display2D(UX,UY,BOUND,ts,false)
            # display2D(UX,UY,ts,verbose)
            frame(anim)
            displayPressureForces2D(BOUND,UX,UY,ts,verbose)
            frame(animF)
            count=count+1
        end
        
    end
    # toc()

    # display2D(UX,UY,ts,true)
    # displayPressureForces2D(BOUND,UX,UY,ts,true)
    pres,vel=displayPressureForces2D(BOUND,UX,UY,ts,false)

    return  UX,UY,pres,vel,anim,animF


end

function concurrentlbm2D!(nx,ny,UX,UY,F,FEQ,maxIter,boundaryProblem,setup,t)

    # configuration
    omega = 1.0; density = 1.0;
    fraction = 0.1 # average fraction of occupied nodes
    # maxIter2=maxIter/2
    deltaU=0.1;

    # setup of variables
    # avu=1; prevavu=1;avus = zeros(Float64, maxIter);
    ts=0;
    

    # single-particle distribution function
    # particle densities conditioned on one of the 9 possible velocities
    # F = repeat([density/9], outer= [nx, ny, 9]);
    # FEQ = F;
    # UX=zeros(nx,ny); UY=zeros(nx,ny);
    
    BOUND,ON,numactivenodes,TO_REFLECT,REFLECTED=boundaryProblem(nx,ny,setup,0);

    
    # constants
    t1 = 4/9; t2 = 1/9; t3 = 1/36; c_squ = 1/3;
    count=0

    


    # main loop
    while ((ts<maxIter))
        # Streaming
        # particles at (x,y)=[2,1] were at [1, 1] before: 1 points right (1,0)
        F[:,:,1] = F[[1; 1:nx-1],:,1];
        # F[:,:,1] = F[[nx; 1:nx-1],:,1];

        
        # particles at [1,2] were at [1, 1] before: 3 points up (0,1)
        F[:,:,3] = F[:,[ny; 1:ny-1],3];
        # F[:,:,3] = F[:,[ny; 1:ny-1],3];

        
        # particles at [1,1] were at [2, 1] before: 5 points left (-1,0)
        F[:,:,5] = F[[2:nx; nx],:,5];
        # F[:,:,5] = F[[2:nx; 1],:,5];
        
        # particles at [1,1] were at [1, 2] before: 7 points down (0,-1)
        F[:,:,7] = F[:,[2:ny; 1],7];
        # F[:,:,7] = F[:,[2:ny; 1],7];
        
        # particles at [2,2] were at [1, 1] before: 2 points to (1,1)
        F[:,:,2] = F[[1; 1:nx-1],[ny; 1:ny-1],2];
        # F[:,:,2] = F[[nx; 1:nx-1],[ny; 1:ny-1],2];

        # particles at [1,2] were at [2, 1] before: 4 points to (-1, 1)
        F[:,:,4] = F[[2:nx; nx],[ny; 1:ny-1],4];
        #F[:,:,4] = F[[2:nx; 1],[ny; 1:ny-1],4];
        
        # particles at [1,1] were at [2, 2] before: 6 points to (-1, -1)
        F[:,:,6] = F[[2:nx; nx],[2:ny; 1],6];
        # F[:,:,6] = F[[2:nx; 1],[2:ny; 1],6];
        
        # particles at [2,1] were at [1, 2] before: 8 points down (1,-1)
        F[:,:,8] = F[[1; 1:nx-1],[2:ny; 1],8];
        # F[:,:,8] = F[[nx; 1:nx-1],[2:ny; 1],8];
        
        DENSITY = sum(F,dims=3);
        
        # 1,2,8 are moving to the right, 4,5,6 to the left
        # 3, 7 and 9 don't move in the x direction
        UX = (sum(F[:,:,[1, 2, 8]],dims=3)-sum(F[:,:,[4, 5, 6]],dims=3))./DENSITY;
        # 2,3,4 are moving up, 6,7,8 down
        # 1, 5 and 9 don't move in the y direction
        UY = (sum(F[:,:,[2, 3, 4]],dims=3)-sum(F[:,:,[6, 7, 8]],dims=3))./DENSITY;
        
        #Increase inlet pressure
        UX[2,1:ny].=deltaU;
        UY[2,1:ny].=0;
        DENSITY[2,1:ny].=1;
        
        UX[ON] .= 0;
        UY[ON] .= 0;
        DENSITY[ON] .= 1;
        
        U_SQU = UX.^2+UY.^2;
        U_C2 = UX+UY;
        U_C4 = -UX+UY;
        U_C6 = -U_C2;
        U_C8 = -U_C4;
        
        # Calculate equilibrium distribution: stationary (a = 0)
        FEQ[:,:,9] = t1*DENSITY.*(1 .-U_SQU/(2*c_squ));
        
        # nearest-neighbours
        FEQ[:,:,1] = t2*DENSITY.*(1 .+UX/c_squ+0.5*(UX/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,3] = t2*DENSITY.*(1 .+UY/c_squ+0.5*(UY/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,5] = t2*DENSITY.*(1 .-UX/c_squ+0.5*(UX/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,7] = t2*DENSITY.*(1 .-UY/c_squ+0.5*(UY/c_squ).^2-U_SQU/(2*c_squ));
        
        # next-nearest neighbours
        FEQ[:,:,2] = t3*DENSITY.*(1 .+U_C2/c_squ+0.5*(U_C2/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,4] = t3*DENSITY.*(1 .+U_C4/c_squ+0.5*(U_C4/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,6] = t3*DENSITY.*(1 .+U_C6/c_squ+0.5*(U_C6/c_squ).^2-U_SQU/(2*c_squ));
        FEQ[:,:,8] = t3*DENSITY.*(1 .+U_C8/c_squ+0.5*(U_C8/c_squ).^2-U_SQU/(2*c_squ));
        
        BOUNCEDBACK = F[TO_REFLECT]; #Densities bouncing back at next timestep
        
        F = omega*FEQ + (1-omega)*F;
        
        F[REFLECTED] = BOUNCEDBACK;

        ts = ts+1;
    end
    # display(ts)

    # display2D(UX,UY,t,false)
    display2D(UX,UY,BOUND,t,false) 
    frame(anim)
    return UX,UY,F,FEQ,BOUND
end

function lbm3D(nx,ny,nz,maxIter,saveEvery=200)
    # 3D Lattice Boltzmann (BGK) model of a fluid.
    # D3Q19 model. At each timestep, particle densities propagate
    # outwards in the directions indicated in the figure. An
    # equivalent 'equilibrium' density is found, and the densities
    # relax towards that state, in a proportion governed by omega.
   
    
    # nx=12;ny=nx;nz=nx; 
    omega=1.0; density=1.0;t1=1/3; t2=1/18; t3=1/36;

    F=repeat([density/19],outer=[nx,ny,nz,19]); 
    FEQ=F; 
    matsize=nx*ny*nz;
    CI=0:matsize:matsize*19;

    maxIter2=100

    BOUND=fill(false,nx,ny,nz);
    for i=1:nx
        for j=1:ny
            for k=1:nz
                BOUND[i,j,k]=((i-5)^2+(j-6)^2+(k-7)^2)<6;
            end
        end
    end

    # BOUND[:,:,1].=true;
    # BOUND[:,1,:].=true;

    # BOUND[1,:,:].=true;
    # BOUND[:,1,:].=true;

    ON=findall(BOUND[:]); #matrix offset of each Occupied Node


    TO_REFLECT=[ON.+CI[2] ON.+CI[3] ON.+CI[4] ON.+CI[5] ON.+CI[6] ON.+CI[7] ON.+CI[8] ON.+CI[9] ON.+CI[10] ON.+CI[11] ON.+CI[12] ON.+CI[13] ON.+CI[14] ON.+CI[15] ON.+CI[16] ON.+CI[17] ON.+CI[18] ON.+CI[19]];
    REFLECTED=[ON.+CI[3] ON.+CI[2] ON.+CI[5] ON.+CI[4] ON.+CI[7] ON.+CI[6] ON.+CI[11] ON.+CI[10] ON.+CI[9] ON.+CI[8] ON.+CI[15] ON.+CI[14] ON.+CI[13] ON.+CI[12] ON.+CI[19] ON.+CI[18] ON.+CI[17] ON.+CI[16]];


    ts=0; 
    # deltaU=1e-7;
    deltaU=0.1;
    numactivenodes=sum(sum(sum(1 .-Int.(BOUND))));

    UX=zeros(nx,ny,nz);
    UY=zeros(nx,ny,nz);
    UZ=zeros(nx,ny,nz);

    anim=Plots.Animation()
    anim2D=Plots.Animation()


    while (ts<maxIter)
        # while (ts<maxIter && 1e-10<abs((prevavu-avu)/avu)) || ts<maxIter2
        # Propagate
        # boundary conditions: x direction open, rest closed
        #nearest-neighbours
        F[:,:,:,2]=F[:,:,[nz;1:nz-1],2];
        F[:,:,:,3]=F[:,:,[2:nz;1],3];
        F[:,:,:,4]=F[:,[ny;1:ny-1],:,4];
        F[:,:,:,5]=F[:,[2:ny;1],:,5];	
        F[:,:,:,6]=F[[1;1:nx-1],:,:,6];
        F[:,:,:,7]=F[[2:nx;nx],:,:,7];

        # F[:,:,:,2]=F[:,:,[nz;1:nz-1],2];
        # F[:,:,:,3]=F[:,:,[2:nz;1],3];
        # F[:,:,:,4]=F[:,[ny;1:ny-1],:,4];
        # F[:,:,:,5]=F[:,[2:ny;1],:,5];	
        # F[:,:,:,6]=F[[nx;1:nx-1],:,:,6];
        # F[:,:,:,7]=F[[2:nx;1],:,:,7];



        #next-nearest neighbours
        F[:,:,:,8]= F[[1;1:nx-1],[ny;1:ny-1],:,8];
        F[:,:,:,9]= F[[1;1:nx-1],[2:ny;1],:,9];
        F[:,:,:,10]=F[[2:nx;nx],[ny;1:ny-1],:,10];
        F[:,:,:,11]=F[[2:nx;nx],[2:ny;1],:,11];	
        F[:,:,:,12]=F[[1;1:nx-1],:,[nz;1:nz-1],12];
        F[:,:,:,13]=F[[1;1:nx-1],:,[2:nz;1],13];
        F[:,:,:,14]=F[[2:nx;nx],:,[nz;1:nz-1],14];
        F[:,:,:,15]=F[[2:nx;nx],:,[2:nz;1],15];
        F[:,:,:,16]=F[:,[ny;1:ny-1],[nz;1:nz-1],16];
        F[:,:,:,17]=F[:,[ny;1:ny-1],[2:nz;1],17];
        F[:,:,:,18]=F[:,[2:ny;1],[nz;1:nz-1],18];
        F[:,:,:,19]=F[:,[2:ny;1],[2:nz;1],19];

        # F[:,:,:,8]= F[[nx;1:nx-1],[ny;1:ny-1],:,8];
        # F[:,:,:,9]= F[[nx;1:nx-1],[2:ny;1],:,9];
        # F[:,:,:,10]=F[[2:nx;1],[ny;1:ny-1],:,10];
        # F[:,:,:,11]=F[[2:nx;1],[2:ny;1],:,11];	
        # F[:,:,:,12]=F[[nx;1:nx-1],:,[nz;1:nz-1],12];
        # F[:,:,:,13]=F[[nx;1:nx-1],:,[2:nz;1],13];
        # F[:,:,:,14]=F[[2:nx;1],:,[nz;1:nz-1],14];
        # F[:,:,:,15]=F[[2:nx;1],:,[2:nz;1],15];
        # F[:,:,:,16]=F[:,[ny;1:ny-1],[nz;1:nz-1],16];
        # F[:,:,:,17]=F[:,[ny;1:ny-1],[2:nz;1],17];
        # F[:,:,:,18]=F[:,[2:ny;1],[nz;1:nz-1],18];
        # F[:,:,:,19]=F[:,[2:ny;1],[2:nz;1],19];

        


        # Relax; calculate equilibrium state (FEQ) with equivalent speed and density to F 
        DENSITY = sum(F,dims=4);
        UX=(sum(F[:,:,:,[6,8 , 9 ,12 ,13]],dims=4)-  sum(F[:,:,:,  [7, 10, 11, 14, 15]],dims=4))./DENSITY;
        UY=(sum(F[:,:,:,[4,8 , 10, 16, 17]],dims=4)- sum(F[:,:,:,  [5, 9 ,11 ,18 ,19]],dims=4))./DENSITY;
        UZ=(sum(F[:,:,:,[2,12, 14, 16, 18]],dims=4)- sum(F[:,:,:,  [3, 13, 15, 17, 19]],dims=4))./DENSITY;
        
        # UX[1,:,:]=UX[1,:,:].+deltaU; #Increase inlet pressure

        #Increase inlet pressure
        # UX[2,1:ny,1:nz].=UX[2,1:ny,1:nz].+deltaU;
        UX[nx,1:ny,1:nz].=-deltaU;
        UY[nx,1:ny,1:nz].=0;
        UZ[nx,1:ny,1:nz].=0;
        DENSITY[2,1:ny,1:nz].=1;

        UX[ON].=0; 
        UY[ON].=0; 
        UZ[ON].=0; 

        # DENSITY[ON].=0;
        DENSITY[ON].=1; 

        U_SQU=UX.^2+UY.^2+UZ.^2;
        U8=UX+UY;
        U9=UX-UY;
        U10=-UX+UY;
        U11=-U8;
        U12=UX+UZ;
        U13=UX-UZ;
        U14=-U13;
        U15=-U12;
        U16=UY+UZ;
        U17=UY-UZ;
        U18=-U17;
        U19=-U16;
        # Calculate equilibrium distribution: stationary
        FEQ[:,:,:,1]=t1*DENSITY.*(1 .-3*U_SQU/2);
        # nearest-neighbours
        FEQ[:,:,:,2]=t2*DENSITY.*(1 .+ 3*UZ + 9/2*UZ.^2 .- 3/2*U_SQU);
        FEQ[:,:,:,3]=t2*DENSITY.*(1 .- 3*UZ + 9/2*UZ.^2 .- 3/2*U_SQU);
        FEQ[:,:,:,4]=t2*DENSITY.*(1 .+ 3*UY + 9/2*UY.^2 .- 3/2*U_SQU);
        FEQ[:,:,:,5]=t2*DENSITY.*(1 .- 3*UY + 9/2*UY.^2 .- 3/2*U_SQU);
        FEQ[:,:,:,6]=t2*DENSITY.*(1 .+ 3*UX + 9/2*UX.^2 .- 3/2*U_SQU);
        FEQ[:,:,:,7]=t2*DENSITY.*(1 .- 3*UX + 9/2*UX.^2 .- 3/2*U_SQU);
        # next-nearest neighbours
        FEQ[:,:,:,8] =t3*DENSITY.*(1 .+ 3*U8  .+ 9/2*(U8).^2  .- 3*U_SQU/2);
        FEQ[:,:,:,9] =t3*DENSITY.*(1 .+ 3*U9  .+ 9/2*(U9).^2  .- 3*U_SQU/2);
        FEQ[:,:,:,10]=t3*DENSITY.*(1 .+ 3*U10 .+ 9/2*(U10).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,11]=t3*DENSITY.*(1 .+ 3*U11 .+ 9/2*(U11).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,12]=t3*DENSITY.*(1 .+ 3*U12 .+ 9/2*(U12).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,13]=t3*DENSITY.*(1 .+ 3*U13 .+ 9/2*(U13).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,14]=t3*DENSITY.*(1 .+ 3*U14 .+ 9/2*(U14).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,15]=t3*DENSITY.*(1 .+ 3*U15 .+ 9/2*(U15).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,16]=t3*DENSITY.*(1 .+ 3*U16 .+ 9/2*(U16).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,17]=t3*DENSITY.*(1 .+ 3*U17 .+ 9/2*(U17).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,18]=t3*DENSITY.*(1 .+ 3*U18 .+ 9/2*(U18).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,19]=t3*DENSITY.*(1 .+ 3*U19 .+ 9/2*(U19).^2 .- 3*U_SQU/2);

        BOUNCEDBACK=F[TO_REFLECT]; #Densities bouncing back at next timestep

        F=omega*FEQ+(1-omega)*F;

        F[REFLECTED]=BOUNCEDBACK;


        ts=ts+1;

        if ts%saveEvery==0 
            display3D(UX,UY,UZ,BOUND,ts)
            print(ts)
            print(",")

            # # display2D(UX[:,:,Int(nz/2)],UY[:,:,Int(nz/2)],BOUND[:,:,Int(nz/2)],ts,false)
            display2D(UX[:,Int(ny/2),:],UZ[:,Int(ny/2),:],BOUND[:,Int(ny/2),:],ts,false)
            frame(anim2D)

        end
    end

    # display3D(UX,UY,UZ,ts)

    # display2D(UX[:,:,5],UY[:,:,5],ts)
    # display2D(UX[:,5,:],UZ[:,5,:],ts)

    ###### turn saved frames into gif ######
    step=saveEvery
    #A = Array{Array{RGB{Normed{UInt8,8}},2},1}()
    #img_path = "res3d/"*string(step)*".png"
    #img = load(img_path)
    #A=reshape(img, (size(img)[1], size(img)[2],1))
    #print(step)
    #print(",")
    
    for i in step:step:maxIter
        img_path = "res3d/"*string(i)*".png"
        #A = cat(A, load(img_path), dims=3)
        Plots.plot(load(img_path),axis=nothing, legend=false, border=:none)
        frame(anim)
    end
    #save("test.gif", A)
    ######


    return UX,UY,UZ,BOUND,anim,anim2D

end

function concurrentlbm3D!(nx,ny,nz,UX,UY,UZ,F,FEQ,maxIter,boundaryProblem,setup,t)

    # configuration & setup of variables
    omega=1.0; deltaU=0.1; 
    
    
    BOUND,ON,TO_REFLECT,REFLECTED=boundaryProblem(nx,ny,nz,setup,0);
    
    # constants
    t1=1/3; t2=1/18; t3=1/36;
    count=0; ts=0;


    # main loop
    while (ts<maxIter)
        # Propagate
        # boundary conditions: x direction open, rest closed
        #nearest-neighbours
        F[:,:,:,2]=F[:,:,[nz;1:nz-1],2];
        F[:,:,:,3]=F[:,:,[2:nz;1],3];
        F[:,:,:,4]=F[:,[ny;1:ny-1],:,4];
        F[:,:,:,5]=F[:,[2:ny;1],:,5];	
        F[:,:,:,6]=F[[1;1:nx-1],:,:,6];
        F[:,:,:,7]=F[[2:nx;nx],:,:,7];

        #next-nearest neighbours
        F[:,:,:,8]= F[[1;1:nx-1],[ny;1:ny-1],:,8];
        F[:,:,:,9]= F[[1;1:nx-1],[2:ny;1],:,9];
        F[:,:,:,10]=F[[2:nx;nx],[ny;1:ny-1],:,10];
        F[:,:,:,11]=F[[2:nx;nx],[2:ny;1],:,11];	
        F[:,:,:,12]=F[[1;1:nx-1],:,[nz;1:nz-1],12];
        F[:,:,:,13]=F[[1;1:nx-1],:,[2:nz;1],13];
        F[:,:,:,14]=F[[2:nx;nx],:,[nz;1:nz-1],14];
        F[:,:,:,15]=F[[2:nx;nx],:,[2:nz;1],15];
        F[:,:,:,16]=F[:,[ny;1:ny-1],[nz;1:nz-1],16];
        F[:,:,:,17]=F[:,[ny;1:ny-1],[2:nz;1],17];
        F[:,:,:,18]=F[:,[2:ny;1],[nz;1:nz-1],18];
        F[:,:,:,19]=F[:,[2:ny;1],[2:nz;1],19];



        # Relax; calculate equilibrium state (FEQ) with equivalent speed and density to F 
        DENSITY = sum(F,dims=4);
        UX=(sum(F[:,:,:,[6,8 , 9 ,12 ,13]],dims=4)-  sum(F[:,:,:,  [7, 10, 11, 14, 15]],dims=4))./DENSITY;
        UY=(sum(F[:,:,:,[4,8 , 10, 16, 17]],dims=4)- sum(F[:,:,:,  [5, 9 ,11 ,18 ,19]],dims=4))./DENSITY;
        UZ=(sum(F[:,:,:,[2,12, 14, 16, 18]],dims=4)- sum(F[:,:,:,  [3, 13, 15, 17, 19]],dims=4))./DENSITY;
        
        #Increase inlet pressure
        # UX[2,1:ny,1:nz].=UX[2,1:ny,1:nz].+deltaU;
        UX[nx,1:ny,1:nz].=-deltaU;
        UY[nx,1:ny,1:nz].=0;
        UZ[nx,1:ny,1:nz].=0;
        DENSITY[2,1:ny,1:nz].=1;
        
        UX[ON].=0; 
        UY[ON].=0; 
        UZ[ON].=0; 
        # DENSITY[ON].=0;
        DENSITY[ON].=1; 
        
        U_SQU=UX.^2+UY.^2+UZ.^2;
        U8=UX+UY;
        U9=UX-UY;
        U10=-UX+UY;
        U11=-U8;
        U12=UX+UZ;
        U13=UX-UZ;
        U14=-U13;
        U15=-U12;
        U16=UY+UZ;
        U17=UY-UZ;
        U18=-U17;
        U19=-U16;
        
        # Calculate equilibrium distribution: stationary
        FEQ[:,:,:,1]=t1*DENSITY.*(1 .-3*U_SQU/2);
        # nearest-neighbours
        FEQ[:,:,:,2]=t2*DENSITY.*(1 .+ 3*UZ + 9/2*UZ.^2 .- 3/2*U_SQU);
        FEQ[:,:,:,3]=t2*DENSITY.*(1 .- 3*UZ + 9/2*UZ.^2 .- 3/2*U_SQU);
        FEQ[:,:,:,4]=t2*DENSITY.*(1 .+ 3*UY + 9/2*UY.^2 .- 3/2*U_SQU);
        FEQ[:,:,:,5]=t2*DENSITY.*(1 .- 3*UY + 9/2*UY.^2 .- 3/2*U_SQU);
        FEQ[:,:,:,6]=t2*DENSITY.*(1 .+ 3*UX + 9/2*UX.^2 .- 3/2*U_SQU);
        FEQ[:,:,:,7]=t2*DENSITY.*(1 .- 3*UX + 9/2*UX.^2 .- 3/2*U_SQU);
        # next-nearest neighbours
        FEQ[:,:,:,8] =t3*DENSITY.*(1 .+ 3*U8  .+ 9/2*(U8).^2  .- 3*U_SQU/2);
        FEQ[:,:,:,9] =t3*DENSITY.*(1 .+ 3*U9  .+ 9/2*(U9).^2  .- 3*U_SQU/2);
        FEQ[:,:,:,10]=t3*DENSITY.*(1 .+ 3*U10 .+ 9/2*(U10).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,11]=t3*DENSITY.*(1 .+ 3*U11 .+ 9/2*(U11).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,12]=t3*DENSITY.*(1 .+ 3*U12 .+ 9/2*(U12).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,13]=t3*DENSITY.*(1 .+ 3*U13 .+ 9/2*(U13).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,14]=t3*DENSITY.*(1 .+ 3*U14 .+ 9/2*(U14).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,15]=t3*DENSITY.*(1 .+ 3*U15 .+ 9/2*(U15).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,16]=t3*DENSITY.*(1 .+ 3*U16 .+ 9/2*(U16).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,17]=t3*DENSITY.*(1 .+ 3*U17 .+ 9/2*(U17).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,18]=t3*DENSITY.*(1 .+ 3*U18 .+ 9/2*(U18).^2 .- 3*U_SQU/2);
        FEQ[:,:,:,19]=t3*DENSITY.*(1 .+ 3*U19 .+ 9/2*(U19).^2 .- 3*U_SQU/2);

        BOUNCEDBACK=F[TO_REFLECT]; #Densities bouncing back at next timestep

        F=omega*FEQ+(1-omega)*F;

        F[REFLECTED]=BOUNCEDBACK;


        ts=ts+1;

    end

    display3D(UX,UY,UZ,BOUND,t) 
    # frame(anim)
    return UX,UY,UZ,F,FEQ,BOUND
end

##########################################################################

##################Poblems and boundary conditions#################################################
function setboundary2D(nx,ny,radius,t=0) #circle in the middle
    msize = nx*ny;
    CI = 0:msize:msize*7; # offsets of different directions e_a in the F matrix 
    #BOUND = rand(nx,ny) .> 1 - fraction; # random domain
    center = [nx/2, ny/2]
    BOUND = [norm([i, j]-center) < radius for i=1:nx, j=1:ny]
    
    # a=repeat(-15:15,outer=[31,1]); 
    # global BOUND=(a.^2 .+ a'.^2).<16; 
    # global BOUND[1:nx,[1 ny]].=1;


    ON = findall(BOUND[:]); # matrix offset of each Occupied Node

    numactivenodes=sum(sum(1 .-BOUND));

    # linear indices in F of occupied nodes
    TO_REFLECT=[ON.+CI[1] ON.+CI[2] ON.+CI[3] ON.+CI[4] ON.+CI[5] ON.+CI[6] ON.+CI[7] ON.+CI[8]];

    # Right <-> Left: 1 <-> 5; Up <-> Down: 3 <-> 7
    #(1,1) <-> (-1,-1): 2 <-> 6; (1,-1) <-> (-1,1): 4 <-> 8
    REFLECTED= [ON.+CI[5] ON.+CI[6] ON.+CI[7] ON.+CI[8] ON.+CI[1] ON.+CI[2] ON.+CI[3] ON.+CI[4]];

    return BOUND,ON,numactivenodes,TO_REFLECT,REFLECTED
end

function setdynamicboundary2D(nx,ny,radius,t) #circle in the middle #dynamic 
    msize = nx*ny;
    CI = 0:msize:msize*7; # offsets of different directions e_a in the F matrix 
    #BOUND = rand(nx,ny) .> 1 - fraction; # random domain
    
    amp=10  #size
    freq=1/30 # times/sec
    y=Int.(round.(amp*sin.(t*freq*(2*π)),digits=0))

    center = [nx/2, ny/2+y]

    BOUND = [norm([i, j]-center) < radius for i=1:nx, j=1:ny]
    
    # a=repeat(-15:15,outer=[31,1]); 
    # global BOUND=(a.^2 .+ a'.^2).<16; 
    # global BOUND[1:nx,[1 ny]].=1;


    ON = findall(BOUND[:]); # matrix offset of each Occupied Node

    numactivenodes=sum(sum(1 .-BOUND));

    # linear indices in F of occupied nodes
    TO_REFLECT=[ON.+CI[1] ON.+CI[2] ON.+CI[3] ON.+CI[4] ON.+CI[5] ON.+CI[6] ON.+CI[7] ON.+CI[8]];

    # Right <-> Left: 1 <-> 5; Up <-> Down: 3 <-> 7
    #(1,1) <-> (-1,-1): 2 <-> 6; (1,-1) <-> (-1,1): 4 <-> 8
    REFLECTED= [ON.+CI[5] ON.+CI[6] ON.+CI[7] ON.+CI[8] ON.+CI[1] ON.+CI[2] ON.+CI[3] ON.+CI[4]];

    return BOUND,ON,numactivenodes,TO_REFLECT,REFLECTED
end

function setboundaryMetavoxels2D(nx,ny,setup,t) #circle in the middle #dynamic 
    msize = nx*ny;
    CI = 0:msize:msize*7; # offsets of different directions e_a in the F matrix 
    #BOUND = rand(nx,ny) .> 1 - fraction; # random domain
    
    # amp=10  #size
    # freq=1/30 # times/sec
    # y=Int.(round.(amp*sin.(t*freq*(2*π)),digits=0))

    # center = [nx/2, ny/2+y]
    nodes=setup["nodes"]
    scale=setup["scale"]*setup["voxelSize"] #later see scale
    scale=1/voxelScaleToLBM*setup["voxelSize"] #make it part of setup
    shift=-0.500001
    disX=[];disY=[];disZ=[];
    points=[]
    for node in nodes #later change to make it faster
        append!(disX,[node["position"]["x"]/scale+node["displacement"]["x"]/scale+shift])
        append!(disY,[node["position"]["y"]/scale+node["displacement"]["y"]/scale+shift])
        append!(disZ,[node["position"]["z"]/scale+node["displacement"]["z"]/scale+shift])


        append!(points,[ [(node["position"]["x"] + node["displacement"]["x"])/scale+shift,
                    (node["position"]["z"] + node["displacement"]["z"])/scale+shift ]])

    end
    hull = concave_hull(points,100) #convex instead
    hull = concave_hull(points) #convex instead
    # Plots.scatter(disX,disZ,label="", aspect_ratio=:equal,markershape=:square,xlim=(1,nx),ylim=(1,ny))
    # display(Plots.plot!(hull))

    pp=fill(Luxor.Point(0, 0),size(hull.vertices)[1]+1)

    count=1
    for v in hull.vertices
        pp[count]=Luxor.Point(v[1], v[2])
        count+=1
    end
    pp[end]=Luxor.Point(hull.vertices[1][1], hull.vertices[1][2])

    function getinside(xx,yy)
        return Luxor.isinside(Luxor.Point.(xx, yy), pp) 
    end

    BOUND = [getinside(i,j) for i=1:nx, j=1:ny]

    ON = findall(BOUND[:]); # matrix offset of each Occupied Node

    numactivenodes=sum(sum(1 .-BOUND));

    # linear indices in F of occupied nodes
    TO_REFLECT=[ON.+CI[1] ON.+CI[2] ON.+CI[3] ON.+CI[4] ON.+CI[5] ON.+CI[6] ON.+CI[7] ON.+CI[8]];

    # Right <-> Left: 1 <-> 5; Up <-> Down: 3 <-> 7
    #(1,1) <-> (-1,-1): 2 <-> 6; (1,-1) <-> (-1,1): 4 <-> 8
    REFLECTED= [ON.+CI[5] ON.+CI[6] ON.+CI[7] ON.+CI[8] ON.+CI[1] ON.+CI[2] ON.+CI[3] ON.+CI[4]];

    return BOUND,ON,numactivenodes,TO_REFLECT,REFLECTED
end

function setboundary3D(nx,ny,nz,setup,t=0) #sphere in the middle
 
    matsize=nx*ny*nz;
    CI=0:matsize:matsize*19;

    BOUND=fill(false,nx,ny,nz);
    for i=1:nx
        for j=1:ny
            for k=1:nz
                BOUND[i,j,k]=((i-5)^2+(j-6)^2+(k-7)^2)<6;
            end
        end
    end

    ON=findall(BOUND[:]); #matrix offset of each Occupied Node


    TO_REFLECT=[ON.+CI[2] ON.+CI[3] ON.+CI[4] ON.+CI[5] ON.+CI[6] ON.+CI[7] ON.+CI[8] ON.+CI[9] ON.+CI[10] ON.+CI[11] ON.+CI[12] ON.+CI[13] ON.+CI[14] ON.+CI[15] ON.+CI[16] ON.+CI[17] ON.+CI[18] ON.+CI[19]];
    REFLECTED=[ON.+CI[3] ON.+CI[2] ON.+CI[5] ON.+CI[4] ON.+CI[7] ON.+CI[6] ON.+CI[11] ON.+CI[10] ON.+CI[9] ON.+CI[8] ON.+CI[15] ON.+CI[14] ON.+CI[13] ON.+CI[12] ON.+CI[19] ON.+CI[18] ON.+CI[17] ON.+CI[16]];

    
    return BOUND,ON,TO_REFLECT,REFLECTED
end

function setboundaryMetavoxels3D(nx,ny,nz,setup,t) 
    matsize=nx*ny*nz;
    CI=0:matsize:matsize*19; # offsets of different directions e_a in the F matrix 

    # center = [nx/2, ny/2+y]
    nodes=setup["nodes"]
    scale=setup["scale"]*setup["voxelSize"] #later see scale
    scale=1/voxelScaleToLBM*setup["voxelSize"] #later see scale
    shift=-0.500001
    disX=[];disY=[];disZ=[];

    points=[]
    for k in 1:nz
        append!(points,[[]])
    end
    for node in nodes #later change to make it faster
        append!(disX,[node["position"]["x"]/scale+node["displacement"]["x"]/scale+shift])
        append!(disY,[node["position"]["y"]/scale+node["displacement"]["y"]/scale+shift])
        append!(disZ,[node["position"]["z"]/scale+node["displacement"]["z"]/scale+shift])
        # append!(disX,[node["position"]["x"]/scale+node["displacement"]["x"]/scale+shift])
        # append!(disY,[node["position"]["y"]/scale+node["displacement"]["y"]/scale+shift])
        # append!(disZ,[node["position"]["z"]/scale+node["displacement"]["z"]/scale+shift])

        k=Int(round((node["position"]["y"] + node["displacement"]["y"])/scale+shift))
        if (k<nz&& k>0)
            append!(points[k],[ [(node["position"]["x"] + node["displacement"]["x"])/scale+shift,
                    (node["position"]["z"] + node["displacement"]["z"])/scale+shift ]])
        end
        for kk in 1:voxelScaleToLBM-1
            if (k<nz)
                append!(points[k+kk],[ [(node["position"]["x"] + node["displacement"]["x"])/scale+shift,
                            (node["position"]["z"] + node["displacement"]["z"])/scale+shift ]])
            end
        end

    end
    # display(disX)
    # display(disY)
    # display(disZ)

    hulls=[]
    for k in 1:nz
        points[k]=unique(points[k])
        if size(points[k])[1]>0
            if size(points[k])[1]==1#one point
                append!(hulls,[[0,points[k][1][1],points[k][1][2]]])
            elseif size(points[k])[1]==2#two points
                append!(hulls,[[3,points[k][1][1],points[k][1][2],points[k][2][1],points[k][2][2]]])
            elseif all(y->y[2]==points[k][1][2], points[k]) #line verticle
                append!(hulls,[[2,minimum([points[k][i][1] for i=1:size(points[k])[1]]),maximum([points[k][i][1] for i=1:size(points[k])[1]])]])
            elseif all(y->y[1]==points[k][1][1], points[k]) #line horizontal
                append!(hulls,[[1,minimum([points[k][i][2] for i=1:size(points[k])[1]]),maximum([points[k][i][2] for i=1:size(points[k])[1]])]])
            else #plane
                hull = concave_hull(points[k]) #convex instead
                append!(hulls,[hull])
            end
        else #nothing
            append!(hulls,[1])
        end
    end

    BOUND=fill(false,nx,ny,nz);


    
    # Plots.scatter(disX,disZ,label="", aspect_ratio=:equal,markershape=:square,xlim=(1,nx),ylim=(1,ny))
    # display(Plots.plot!(hull))

    
    function getinside(xx,yy,zz)
        hull=hulls[zz]
        tolerance=0.0001
        if hull!=1
            if (hull isa Array) 
                # &&  (size(hull)[1]==3)
                if hull[1]==0 #one point
                    return (isapprox(hull[2], xx,atol=tolerance) && isapprox(hull[3], yy,atol=tolerance))
                elseif hull[1]==1 #line verticle
                    return yy<=hull[3]+tolerance && yy>=hull[2]-tolerance
                elseif hull[1]==2 #line horizontal
                    return xx<=hull[3]+tolerance && xx>=hull[2]-tolerance
                elseif hull[1]==3 #two points
                    return (isapprox(hull[2], xx,atol=tolerance) && isapprox(hull[3], yy,atol=tolerance)) || (isapprox(hull[4], xx,atol=tolerance) && isapprox(hull[5], yy,atol=tolerance))
                end
            else #plane
                pp=fill(Luxor.Point(0, 0),size(hull.vertices)[1]+1)
                count=1
                for v in hull.vertices
                    pp[count]=Luxor.Point(v[1], v[2])
                    count+=1
                end
                pp[end]=Luxor.Point(hull.vertices[1][1], hull.vertices[1][2])
                return Luxor.isinside(Luxor.Point.(xx, yy), pp)
            end
        else #nothing
            return false
        end
    end

    BOUND = [getinside(i,j,k) for i=1:nx, j=1:ny,k=1:nz]
    # display(sum(BOUND))

    ON = findall(BOUND[:]); # matrix offset of each Occupied Node


    TO_REFLECT=[ON.+CI[2] ON.+CI[3] ON.+CI[4] ON.+CI[5] ON.+CI[6] ON.+CI[7] ON.+CI[8] ON.+CI[9] ON.+CI[10] ON.+CI[11] ON.+CI[12] ON.+CI[13] ON.+CI[14] ON.+CI[15] ON.+CI[16] ON.+CI[17] ON.+CI[18] ON.+CI[19]];
    REFLECTED=[ON.+CI[3] ON.+CI[2] ON.+CI[5] ON.+CI[4] ON.+CI[7] ON.+CI[6] ON.+CI[11] ON.+CI[10] ON.+CI[9] ON.+CI[8] ON.+CI[15] ON.+CI[14] ON.+CI[13] ON.+CI[12] ON.+CI[19] ON.+CI[18] ON.+CI[17] ON.+CI[16]];

    return BOUND,ON,TO_REFLECT,REFLECTED
end

##########################################################################

##################display#################################################
function display2D(UX,UY,BOUND,ts,verbose=false)
    #display(heatmap(sqrt.(UX[2:nx,:]'.^2 .+ UY[2:nx,:]'.^2),c=:viridis,legend=false))

    maxVal= maximum(sqrt.(UX[2:nx,:]'.^2 .+ UY[2:nx,:]'.^2))
    if verbose
        # display(maxVal)
    end
    # maxVal=0.05

    P=sqrt.(UX[1:nx,:]'.^2 .+ UY[1:nx,:]'.^2)
    P[BOUND'].=10.0
    # P[1,:].=10.0
    Plots.heatmap(P[:,2:nx],title="t=$ts",c=jet_me,legend=false, aspect_ratio=:equal,clim=(0,maxVal))#,clim=(0,2e-5))
    xs = 1:Int(nx)-1
    ys = 1:Int(ny)-1

    res(y,x)=(1.0.*UX[2:nx,:]'[Int(x),Int(y)], 1.0.*UY[2:nx,:]'[Int(x),Int(y)])
    col(y,x)=norm(res(y,x))
    xxs = [x for x in xs for y in ys]
    yys = [y for x in xs for y in ys]

    # quiver!(xxs, yys, quiver=res,c=:viridis,line_z=col,arrowsize =col)
    # display(Plots.quiver!(xxs, yys, quiver=res,arrowscale =col))
    if verbose
        display(Plots.quiver!(xxs, yys, quiver=res))
    else
        Plots.quiver!(xxs, yys, quiver=res)
    end


    
end

function display3D(UX,UY,UZ,BOUND,ts=1)
    
    res3d=dropdims(sqrt.(UX.^2 .+UY.^2 .+UZ.^2),dims=4);
    # println( maximum(res3d))
    # volume(res3d, algorithm = :absorption ,transparency=true,colorrange=(minimum(res3d), maximum(res3d)),backgroundcolor = "#000000",colormap=:jet)

    fig = Figure(resolution =(800,800))
    cam=cam3d_cad!
    ax = LScene(fig, scenekw = (camera = cam, show_axis = true))
    # cam.eyeposition[] = Float32[0., 1., 0.]
    # minimum(res3d)
    # sc=volume!(res3d, algorithm = :absorption ,transparency=true,colorrange=(0, maximum(res3d)),backgroundcolor = "#000000",colormap=:jet)
    sc=volume!(BOUND, algorithm = :iso, isorange = 1.0, isovalue = 1.0,colorrange=(0, 2))
    sc=volume!(res3d, algorithm = :absorption ,transparency=true,colorrange=(0, maximum(res3d)),backgroundcolor = "#FFFFFF",colormap=:jet)

    # sc=volume!(res3d, algorithm = :absorption ,transparency=true,colorrange=(0, 0.0012),backgroundcolor = "#000000",colormap=:jet)

    update_cam!(ax.scene, Vec3f0(0, 1, 0), Vec3f0(0, 0, 0))

    axis = ax.scene[OldAxis]
    tstyle = axis[:names]
    # tstyle[:textsize] = 12
    # tstyle[:gap] = 5
    # axis[:ticks][:textcolor] = :black
    # axis[:ticks][:textsize] = 7
    fig[1,1] = ax
    save("./res3d/$(ts).png", fig, px_per_unit = 1)
    # fig
    display(fig)
    
end

function display3D(UX,UY,UZ,BOUND,ts=1)
    
    res3d=dropdims(sqrt.(UX.^2 .+UY.^2 .+UZ.^2),dims=4);
    # println( maximum(res3d))
    # volume(res3d, algorithm = :absorption ,transparency=true,colorrange=(minimum(res3d), maximum(res3d)),backgroundcolor = "#000000",colormap=:jet)

    fig = Figure(resolution =(800,800))
    cam=cam3d_cad!
    ax = LScene(fig, scenekw = (camera = cam, show_axis = true))
    
    sc=volume!(BOUND, algorithm = :iso, isorange = 1.0, isovalue = 1.0,colorrange=(0, 2))
    sc=volume!(res3d, algorithm = :absorption ,transparency=true,colorrange=(0, maximum(res3d)),backgroundcolor = "#FFFFFF",colormap=:jet)


    update_cam!(ax.scene, Vec3f0(0, 1, 0), Vec3f0(0, 0, 0))

    cmap = RGBAf0.(to_colormap(:jet, 100), 0.6)
    sc.colormap[] = cmap

    axis = ax.scene[OldAxis]
    tstyle = axis[:names]
    # tstyle[:textsize] = 12
    # tstyle[:gap] = 5
    # axis[:ticks][:textcolor] = :black
    # axis[:ticks][:textsize] = 7
    fig[1,1] = ax
    save("./res3d/$(ts).png", fig, px_per_unit = 1)

    ##########################

    fig = Figure(resolution =(800,800))
    cam=cam3d_cad!
    ax = LScene(fig, scenekw = (camera = cam, show_axis = true))
    sc=volume!(BOUND, algorithm = :iso, isorange = 1.0, isovalue = 1.0,colorrange=(0, 2))
    sc=volume!(res3d, algorithm = :absorption ,transparency=true,colorrange=(0, maximum(res3d)),backgroundcolor = "#FFFFFF",colormap=:jet)
    # update_cam!(ax.scene, Vec3f0(0, 1, 0), Vec3f0(0, 0, 0))

    cmap = RGBAf0.(to_colormap(:jet, 100), 0.6)
    sc.colormap[] = cmap

    axis = ax.scene[OldAxis]
    tstyle = axis[:names]
    # tstyle[:textsize] = 12
    # tstyle[:gap] = 5
    # axis[:ticks][:textcolor] = :black
    # axis[:ticks][:textsize] = 7
    fig[1,1] = ax
    save("./res3d1/$(ts).png", fig, px_per_unit = 1)
    # fig
    display(fig)
    
end

function displayPressureForces2D(BOUND,UX,UY,ts,verbose)
    nx,ny=size(BOUND)
    edge,Neigh=getEdgeNeigh2D(BOUND);
    #keys(Neigh)

    maxVal=0.05

    UX1=reshape(UX,size(UX)[1],size(UX)[2]);
    UY1=reshape(UY,size(UY)[1],size(UY)[2]);
    getVel=x -> [sum(UX1[Neigh[x]]),sum(UY1[Neigh[x]])]

    pres=zeros(size(UX1))
    vel=fill([0.,0.],size(UX1))
    pres[edge].=norm.(getVel.(edge));
    vel[edge].=getVel.(edge);

    Plots.heatmap(pres', aspect_ratio=:equal,c=jet_me,title="t=$ts",clim=(0,maxVal),legend=false)
    xs = 1:Int(nx)-1
    ys = 1:Int(ny)-1
    getV(x,y)=vel[Int.(x),Int.(y)]
    xxs = [x for x in xs for y in ys];
    yys = [y for x in xs for y in ys];

    if verbose
        display(Plots.quiver!(xxs, yys, quiver=getV))
    else
        Plots.quiver!(xxs, yys, quiver=getV)
    end
    return pres,vel
end

function metaVoxelsPressureForces2D(nx,ny,BOUND,UX,UY,ts)
   
    edge,Neigh=getEdgeNeigh2D(BOUND);
    #keys(Neigh)

    maxVal=0.05

    UX1=reshape(UX,size(UX)[1],size(UX)[2]);
    UY1=reshape(UY,size(UY)[1],size(UY)[2]);
    getVel=x -> [sum(UX1[Neigh[x]]),sum(UY1[Neigh[x]])]

    presD=zeros(size(UX1))
    presPlot=zeros(size(UX1))
    presPlot[BOUND].=0.01
    
    velD=fill([0.,0.],size(UX1))
    presD[edge].=norm.(getVel.(edge));
    presPlot[edge].+=norm.(getVel.(edge));
    velD[edge].=getVel.(edge);

    Plots.heatmap(presPlot', aspect_ratio=:equal,c=jet_me,title="t=$ts",clim=(0,maxVal+0.01),legend=false)
    xs = 1:Int(nx)-1
    ys = 1:Int(ny)-1
    getV(x,y)=velD[Int.(x),Int.(y)]
    xxs = [x for x in xs for y in ys];
    yys = [y for x in xs for y in ys];


    Plots.quiver!(xxs, yys, quiver=getV)
    frame(animF)

    # println(sum(velD))
    # println(sum(UX))
    # println(sum(UY))

    return presD,velD
end

function metaVoxelsPressureForces3D(nx,ny,nz,BOUND,UX,UY,UZ,ts)
   
    edge,Neigh=getEdgeNeigh3D(BOUND);
    #keys(Neigh)

    maxVal=0.05

    UX1=reshape(UX,size(UX)[1],size(UX)[2],size(UX)[3]);
    UY1=reshape(UY,size(UY)[1],size(UY)[2],size(UY)[3]);
    UZ1=reshape(UZ,size(UZ)[1],size(UZ)[2],size(UZ)[3]);
    getVel=x -> [sum(UX1[Neigh[x]]),sum(UY1[Neigh[x]]),sum(UZ1[Neigh[x]])]

    presD=zeros(size(UX1))
    presPlot=zeros(size(UX1))
    presPlot[BOUND].=0.01
    
    velD=fill([0.,0.],size(UX1))
    presD[edge].=norm.(getVel.(edge));
    presPlot[edge].+=norm.(getVel.(edge));
    velD[edge].=getVel.(edge);

    # Plots.heatmap(presPlot', aspect_ratio=:equal,c=jet_me,title="t=$ts",clim=(0,maxVal+0.01),legend=false)
    xs = 1:Int(nx)-1
    ys = 1:Int(ny)-1
    zs = 1:Int(nz)-1
    getV(x,y,z)=vel[Int.(x),Int.(y),Int.(z)]
    xxs = [x for x in xs for y in ys for z in zs];
    yys = [y for x in xs for y in ys for z in zs];
    zzs = [z for x in xs for y in ys for z in zs];



    # Plots.quiver!(xxs, yys, quiver=getV)
    # frame(animF)

    return presD,velD
end

################################ utils ####################################
function getEdgeNeigh2D(BOUND)
    nx,ny=size(BOUND)
    ON2D = findall(Bool.(BOUND));
    bottom=filter(n -> n[1]>1 && BOUND[n[1]-1,n[2]]== 0, ON2D)
    top=filter(n -> n[1]<size(BOUND)[1] && BOUND[n[1]+1,n[2]]== 0, ON2D)
    left=filter(n -> n[2]>1 && BOUND[n[1],n[2]-1]== 0, ON2D)
    right=filter(n -> n[2]<size(BOUND)[2] && BOUND[n[1],n[2]+1]== 0, ON2D)
    edge=union(top,bottom,left,right);

    Neigh = Dict()
    ind=[[-1,0],[1,0],[0,-1],[0,1]];
    count=1
    for arr in [bottom,top,left,right]
        for a in arr
            c=CartesianIndex(a[1]+ind[count][1],a[2]+ind[count][2])
            if !haskey(Neigh,a)
                Neigh[a]=[c]
            else
                append!(Neigh[a],[c])
            end
        end
        count+=1
    end
    return edge,Neigh
end

function getEdgeNeigh3D(BOUND)
    nx,ny,nz=size(BOUND)
    ON2D = findall(Bool.(BOUND));


    bottom=filter(n -> n[1]>1 && BOUND[n[1]-1,n[2],n[3]]== 0, ON2D)
    top=   filter(n -> n[1]<size(BOUND)[1] && BOUND[n[1]+1,n[2],n[3]]== 0, ON2D)
    left=  filter(n -> n[2]>1 && BOUND[n[1],n[2]-1,n[3]]== 0, ON2D)
    right= filter(n -> n[2]<size(BOUND)[2] && BOUND[n[1],n[2]+1,n[3]]== 0, ON2D)
    back=  filter(n -> n[3]>1 && BOUND[n[1],n[2],n[3]-1]== 0, ON2D)
    front= filter(n -> n[3]<size(BOUND)[3] && BOUND[n[1],n[2],n[3]+1]== 0, ON2D)


    edge=union(top,bottom,left,right,back,front);

    Neigh = Dict()
    ind=[[-1,0,0],[1,0,0],[0,-1,0],[0,1,0],[0,0,-1],[0,0,1]];
    count=1
    for arr in [bottom,top,left,right,back,front]
        for a in arr
            c=CartesianIndex(a[1]+ind[count][1],a[2]+ind[count][2],a[3]+ind[count][3])
            if !haskey(Neigh,a)
                Neigh[a]=[c]
            else
                append!(Neigh[a],[c])
            end
        end
        count+=1
    end
    return edge,Neigh
end

##########################################################################
if logging
    println("Loaded LBM Library!")
end

##########################################################################
