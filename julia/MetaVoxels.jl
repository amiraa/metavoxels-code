# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020
#################################################################

using LinearAlgebra
import JSON
using StaticArrays, BenchmarkTools
using Base.Threads
import Base: +, * , -, ^

include("./include/vector.jl") #utils for vectors and quaternions
include("./include/material.jl") #utils for node and edge material
include("./include/export.jl") #export simulation data to json
include("./include/run.jl") #turn setup to cuda arrays and run simulation
include("./include/updateEdges.jl") #edges properties update
include("./include/externalForces.jl") #external forces applied to the system
include("./include/forces.jl") #force integration
include("./include/updateTendons.jl") #nodes properties update
include("./include/updateNodes.jl") #nodes properties update
include("./include/beamFEA.jl") #solve problem with FEA

if GPU
    include("./include/cuda_calls.jl") #cuda related calls
end

if plotting
    # gr(size=(300,300), html_output_format=:png)
    include("./include/plotViz.jl") #plotting and getting data out for analysis
end

if (logging)
    println("Loaded MetaVoxels Functions!")
end
