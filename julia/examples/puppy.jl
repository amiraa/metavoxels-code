# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

######################### 1. Voxel Design ###########################
setup = Dict()

### 1.b Draw Lattice

rhino=false

voxelSize=0.001
latticeSizeX=2+8
latticeSizeY=8
latticeSizeZ=5

gridSize=10

setup["gridSize"]=gridSize

setup["rhino"]=false
setup["useVoxelList"]=true


setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=1e4; #scale for visualization
setup["hierarchical"]=true; #hierachical simualtion


#simulation params
setup["numTimeSteps"]=30000*5.0/1.0; #num of timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=true; #if there is change in temperature
setup["globalDamping"]=0.15; # (usually from 0.1 to 0.4)


#visualization params
setup["maxNumFiles"]=50; #num of saved timesteps for visualization


######################### 2.b. Materials #########################

density=50.0
stiffness=1e6

#default neural material
material0= Dict()
material0["area"]=voxelSize*voxelSize
material0["density"]=density
material0["stiffness"]=stiffness*10.0
material0["poissonRatio"]=0.0
material0["cTE"]=0.0 #coefficient of thermal expansion

#first material leg1
material11= Dict()
material11["area"]=voxelSize*voxelSize
material11["density"]=density
material11["stiffness"]=stiffness
material11["poissonRatio"]=0.1
material11["cTE"]=-0.1 #coefficient of thermal expansion


#second material leg1
material12= Dict()
material12["area"]=voxelSize*voxelSize
material12["density"]=density
material12["stiffness"]=stiffness
material12["poissonRatio"]=0.1
material12["cTE"]=0.1 #coefficient of thermal expansion


#first material leg2
material21= Dict()
material21["area"]=voxelSize*voxelSize
material21["density"]=density
material21["stiffness"]=stiffness
material21["poissonRatio"]=-0.1
material21["cTE"]=-0.1 #coefficient of thermal expansion


#second material leg2
material22= Dict()
material22["area"]=voxelSize*voxelSize
material22["density"]=density
material22["stiffness"]=stiffness
material22["poissonRatio"]=-0.1
material22["cTE"]=0.1 #coefficient of thermal expansion


#first material leg1
material11_shift= Dict()
material11_shift["area"]=voxelSize*voxelSize
material11_shift["density"]=density
material11_shift["stiffness"]=stiffness
material11_shift["poissonRatio"]=0.1
material11_shift["cTE"]=-0.5 #coefficient of thermal expansion


#second material leg1
material12_shift= Dict()
material12_shift["area"]=voxelSize*voxelSize
material12_shift["density"]=density
material12_shift["stiffness"]=stiffness
material12_shift["poissonRatio"]=0.1
material12_shift["cTE"]=0.5 #coefficient of thermal expansion


#first material leg2
material21_shift= Dict()
material21_shift["area"]=voxelSize*voxelSize
material21_shift["density"]=density
material21_shift["stiffness"]=stiffness
material21_shift["poissonRatio"]=-0.1
material21_shift["cTE"]=-0.5 #coefficient of thermal expansion


#second material leg2
material22_shift= Dict()
material22_shift["area"]=voxelSize*voxelSize
material22_shift["density"]=density
material22_shift["stiffness"]=stiffness
material22_shift["poissonRatio"]=-0.1
material22_shift["cTE"]=0.5 #coefficient of thermal expansion



setup["materials"]=[

];


#//allowed x, yand z are from -gridSize to +gridSize (floor is at 0)
setup["voxelList"]=[

    [[0,7,0],material0],#top
    [[1,7,0],material0],#top
    [[0,7,1],material0],#top
    [[1,7,1],material0],#top
    [[0,7,2],material0],#top
    [[1,7,2],material0],#top
    [[0,7,3],material0],#top
    [[1,7,3],material0],#top
    [[0,7,4],material0],#top
    [[1,7,4],material0],#top

    [[0,6,0],material0],#top
    [[1,6,0],material0],#top
    [[0,6,1],material0],#top
    [[1,6,1],material0],#top
    [[0,6,2],material0],#top
    [[1,6,2],material0],#top
    [[0,6,3],material0],#top
    [[1,6,3],material0],#top
    [[0,6,4],material0],#top
    [[1,6,4],material0],#top

    

    [[0+2,7,0],material0],#top
    [[1+2,7,0],material0],#top
    [[0+2,7,1],material0],#top
    [[1+2,7,1],material0],#top
    [[0+2,7,2],material0],#top
    [[1+2,7,2],material0],#top
    [[0+2,7,3],material0],#top
    [[1+2,7,3],material0],#top
    [[0+2,7,4],material0],#top
    [[1+2,7,4],material0],#top
    [[0+2,6,0],material0],#top
    [[1+2,6,0],material0],#top
    [[0+2,6,1],material0],#top
    [[1+2,6,1],material0],#top
    [[0+2,6,2],material0],#top
    [[1+2,6,2],material0],#top
    [[0+2,6,3],material0],#top
    [[1+2,6,3],material0],#top
    [[0+2,6,4],material0],#top
    [[1+2,6,4],material0],#top


    [[0+4,7,0],material0],#top
    [[1+4,7,0],material0],#top
    [[0+4,7,1],material0],#top
    [[1+4,7,1],material0],#top
    [[0+4,7,2],material0],#top
    [[1+4,7,2],material0],#top
    [[0+4,7,3],material0],#top
    [[1+4,7,3],material0],#top
    [[0+4,7,4],material0],#top
    [[1+4,7,4],material0],#top
    [[0+4,6,0],material0],#top
    [[1+4,6,0],material0],#top
    [[0+4,6,1],material0],#top
    [[1+4,6,1],material0],#top
    [[0+4,6,2],material0],#top
    [[1+4,6,2],material0],#top
    [[0+4,6,3],material0],#top
    [[1+4,6,3],material0],#top
    [[0+4,6,4],material0],#top
    [[1+4,6,4],material0],#top

    [[0+6,7,0],material0],#top
    [[1+6,7,0],material0],#top
    [[0+6,7,1],material0],#top
    [[1+6,7,1],material0],#top
    [[0+6,7,2],material0],#top
    [[1+6,7,2],material0],#top
    [[0+6,7,3],material0],#top
    [[1+6,7,3],material0],#top
    [[0+6,7,4],material0],#top
    [[1+6,7,4],material0],#top
    [[0+6,6,0],material0],#top
    [[1+6,6,0],material0],#top
    [[0+6,6,1],material0],#top
    [[1+6,6,1],material0],#top
    [[0+6,6,2],material0],#top
    [[1+6,6,2],material0],#top
    [[0+6,6,3],material0],#top
    [[1+6,6,3],material0],#top
    [[0+6,6,4],material0],#top
    [[1+6,6,4],material0],#top

    [[0+8,7,0],material0],#top
    [[1+8,7,0],material0],#top
    [[0+8,7,1],material0],#top
    [[1+8,7,1],material0],#top
    [[0+8,7,2],material0],#top
    [[1+8,7,2],material0],#top
    [[0+8,7,3],material0],#top
    [[1+8,7,3],material0],#top
    [[0+8,7,4],material0],#top
    [[1+8,7,4],material0],#top
    [[0+8,6,0],material0],#top
    [[1+8,6,0],material0],#top
    [[0+8,6,1],material0],#top
    [[1+8,6,1],material0],#top
    [[0+8,6,2],material0],#top
    [[1+8,6,2],material0],#top
    [[0+8,6,3],material0],#top
    [[1+8,6,3],material0],#top
    [[0+8,6,4],material0],#top
    [[1+8,6,4],material0],#top

    ########################################################## front legs ####################################################################
    
    ########################upper##################################
    [[0,5,0],material12],#leg1 front
    [[0,5,1],material12],#leg1 front
    [[0,4,0],material12],#leg1 front
    [[0,4,1],material12],#leg1 front
    [[0,3,0],material12],#leg1 front
    [[0,3,1],material12],#leg1 front


    [[1,5,0],material11],#leg1 back
    [[1,5,1],material11],#leg1 back
    [[1,4,0],material11],#leg1 back
    [[1,4,1],material11],#leg1 back
    [[1,3,0],material11],#leg1 back
    [[1,3,1],material11],#leg1 back

    
    [[0,5,3],material22],#leg2 front
    [[0,5,4],material22],#leg2 front
    [[0,4,3],material22],#leg2 front
    [[0,4,4],material22],#leg2 front
    [[0,3,3],material22],#leg2 front
    [[0,3,4],material22],#leg2 front


    [[1,5,4],material21],#leg2 back
    [[1,5,3],material21],#leg2 back
    [[1,4,3],material21],#leg2 back
    [[1,4,4],material21],#leg2 back
    [[1,3,3],material21],#leg2 back
    [[1,3,4],material21],#leg2 back


    ########################lower##################################


    [[0,2,0],material0],#leg1 front
    [[0,2,1],material0],#leg1 front
    [[0,1,0],material0],#leg1 front
    [[0,1,1],material0],#leg1 front
    [[0,0,0],material0],#leg1 front
    [[0,0,1],material0],#leg1 front


    [[1,2,0],material12],#leg1 back 2
    [[1,2,1],material12],#leg1 back 2
    [[1,1,0],material12],#leg1 back 2
    [[1,1,1],material12],#leg1 back 2
    [[1,0,0],material12],#leg1 back 2
    [[1,0,1],material12],#leg1 back 2

    [[0,2,3],material0],#leg2 front 2
    [[0,2,4],material0],#leg2 front 2
    [[0,1,3],material0],#leg2 front 2
    [[0,1,4],material0],#leg2 front 2
    [[0,0,3],material0],#leg2 front 2
    [[0,0,4],material0],#leg2 front 2


    [[1,2,3],material22],#leg2 back
    [[1,2,4],material22],#leg2 back
    [[1,0,3],material22],#leg2 back
    [[1,0,4],material22],#leg2 back
    [[1,1,3],material22],#leg2 back
    [[1,1,4],material22],#leg2 back

    ########################################################## back legs ####################################################################
    
    ########################upper##################################
    # [[0+4,5,0],material22],#leg1 front
    # [[0+4,5,1],material22],#leg1 front
    # [[0+4,4,0],material22],#leg1 front
    # [[0+4,4,1],material22],#leg1 front
    # [[0+4,3,0],material22],#leg1 front
    # [[0+4,3,1],material22],#leg1 front

    # [[1+4,5,0],material21],#leg1 back
    # [[1+4,5,1],material21],#leg1 back
    # [[1+4,4,0],material21],#leg1 back
    # [[1+4,4,1],material21],#leg1 back
    # [[1+4,3,0],material21],#leg1 back
    # [[1+4,3,1],material21],#leg1 back
    
    # [[0+4,5,3],material12],#leg2 front
    # [[0+4,5,4],material12],#leg2 front
    # [[0+4,4,3],material12],#leg2 front
    # [[0+4,4,4],material12],#leg2 front
    # [[0+4,3,3],material12],#leg2 front
    # [[0+4,3,4],material12],#leg2 front

    # [[1+4,5,4],material11],#leg2 back
    # [[1+4,5,3],material11],#leg2 back
    # [[1+4,4,3],material11],#leg2 back
    # [[1+4,4,4],material11],#leg2 back
    # [[1+4,3,3],material11],#leg2 back
    # [[1+4,3,4],material11],#leg2 back


    ########################lower##################################


    # [[0+4,2,0],material0],#leg1 front
    # [[0+4,2,1],material0],#leg1 front
    # [[0+4,1,0],material0],#leg1 front
    # [[0+4,1,1],material0],#leg1 front
    # [[0+4,0,0],material0],#leg1 front
    # [[0+4,0,1],material0],#leg1 front


    # [[1+4,2,0],material22],#leg1 back 2
    # [[1+4,2,1],material22],#leg1 back 2
    # [[1+4,1,0],material22],#leg1 back 2
    # [[1+4,1,1],material22],#leg1 back 2
    # [[1+4,0,0],material22],#leg1 back 2
    # [[1+4,0,1],material22],#leg1 back 2

    # [[0+4,2,3],material0],#leg2 front 2
    # [[0+4,2,4],material0],#leg2 front 2
    # [[0+4,1,3],material0],#leg2 front 2
    # [[0+4,1,4],material0],#leg2 front 2
    # [[0+4,0,3],material0],#leg2 front 2
    # [[0+4,0,4],material0],#leg2 front 2

    
    # [[1+4,2,3],material12],#leg2 back
    # [[1+4,2,4],material12],#leg2 back
    # [[1+4,0,3],material12],#leg2 back
    # [[1+4,0,4],material12],#leg2 back
    # [[1+4,1,3],material12],#leg2 back
    # [[1+4,1,4],material12],#leg2 back


    # ########################################################## front legs ####################################################################
    
    # ########################upper##################################
    [[0+8,5,0],material12_shift],#leg1 front
    [[0+8,5,1],material12_shift],#leg1 front
    [[0+8,4,0],material12_shift],#leg1 front
    [[0+8,4,1],material12_shift],#leg1 front
    [[0+8,3,0],material12_shift],#leg1 front
    [[0+8,3,1],material12_shift],#leg1 front

    [[1+8,5,0],material11_shift],#leg1 back
    [[1+8,5,1],material11_shift],#leg1 back
    [[1+8,4,0],material11_shift],#leg1 back
    [[1+8,4,1],material11_shift],#leg1 back
    [[1+8,3,0],material11_shift],#leg1 back
    [[1+8,3,1],material11_shift],#leg1 back

    
    [[0+8,5,3],material22_shift],#leg2 front
    [[0+8,5,4],material22_shift],#leg2 front
    [[0+8,4,3],material22_shift],#leg2 front
    [[0+8,4,4],material22_shift],#leg2 front
    [[0+8,3,3],material22_shift],#leg2 front
    [[0+8,3,4],material22_shift],#leg2 front

    [[1+8,5,4],material21_shift],#leg2 back
    [[1+8,5,3],material21_shift],#leg2 back
    [[1+8,4,3],material21_shift],#leg2 back
    [[1+8,4,4],material21_shift],#leg2 back
    [[1+8,3,3],material21_shift],#leg2 back
    [[1+8,3,4],material21_shift],#leg2 back


    ########################lower##################################


    [[0+8,2,0],material0],#leg1 front
    [[0+8,2,1],material0],#leg1 front
    [[0+8,1,0],material0],#leg1 front
    [[0+8,1,1],material0],#leg1 front
    [[0+8,0,0],material0],#leg1 front
    [[0+8,0,1],material0],#leg1 front

    [[1+8,2,0],material12_shift],#leg1 back 2
    [[1+8,2,1],material12_shift],#leg1 back 2
    [[1+8,1,0],material12_shift],#leg1 back 2
    [[1+8,1,1],material12_shift],#leg1 back 2
    [[1+8,0,0],material12_shift],#leg1 back 2
    [[1+8,0,1],material12_shift],#leg1 back 2

    [[0+8,2,3],material0],#leg2 front 2
    [[0+8,2,4],material0],#leg2 front 2
    [[0+8,1,3],material0],#leg2 front 2
    [[0+8,1,4],material0],#leg2 front 2
    [[0+8,0,3],material0],#leg2 front 2
    [[0+8,0,4],material0],#leg2 front 2

    [[1+8,2,3],material22_shift],#leg2 back
    [[1+8,2,4],material22_shift],#leg2 back
    [[1+8,0,3],material22_shift],#leg2 back
    [[1+8,0,4],material22_shift],#leg2 back
    [[1+8,1,3],material22_shift],#leg2 back
    [[1+8,1,4],material22_shift],#leg2 back

        
];
######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]
# dof=[false,true,false,true,true,true]


boundingBoxSupport1=Dict()
boundingBoxSupport1["min"]=Dict()
boundingBoxSupport1["max"]=Dict()


boundingBoxSupport1["min"]["x"]=0;
boundingBoxSupport1["min"]["y"]=voxelSize*(latticeSizeY-2);
boundingBoxSupport1["min"]["z"]=0;

boundingBoxSupport1["max"]["x"]=voxelSize*(latticeSizeX);
boundingBoxSupport1["max"]["y"]=voxelSize*(latticeSizeY);
boundingBoxSupport1["max"]["z"]=voxelSize*(latticeSizeZ);

setup["supports"]=[
        # [boundingBoxSupport1,dof]
    ];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads
load1=Dict()
load1["x"]=0.0
load1["y"]=0.0
load1["z"]=0.0

boundingBoxLoad1=Dict()
boundingBoxLoad1["min"]=Dict()
boundingBoxLoad1["max"]=Dict()

boundingBoxLoad1["min"]["x"]=0
boundingBoxLoad1["min"]["y"]=voxelSize*(latticeSizeY-1);
boundingBoxLoad1["min"]["z"]=0;

boundingBoxLoad1["max"]["x"]=voxelSize*(latticeSizeX);
boundingBoxLoad1["max"]["y"]=voxelSize*(latticeSizeY);
boundingBoxLoad1["max"]["z"]=voxelSize*(latticeSizeZ);


setup["loads"]=[
        # [boundingBoxLoad1,load1]
    ];

setup["fixedDisplacements"]=[];


#### 2.d.2 Dynamic Loads
function floorEnabled()
    return true
end

function gravityEnabled()
    return true
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)

    return N_force
end

# if no temperature:
# function updateTemperature(currentRestLength,currentTimeStep,mat)
#     return currentRestLength
# end

# function updateTemperature(currentRestLength,currentTimeStep,mat)
#     if currentTimeStep<1000
#         temp=-5.0*currentTimeStep/1000
#         currentRestLength=0.5*mat.L*(2.0+temp*mat.cTE)
#     elseif currentTimeStep==2500
#         temp=0
#         currentRestLength=0.5*mat.L*(2.0+temp*mat.cTE)
#     end
#     return currentRestLength
# end


function updateTemperature(currentRestLength,currentTimeStep,mat)
    cte=mat.cTE
    factor=0.4;
    interval=2000.0*5.0/1.0

    currentTimeStep1=currentTimeStep
    if (mat.nu<0.0)
        currentTimeStep1+=interval
    end

    if abs(cte)>0.3
        currentTimeStep1+=interval/2.0
    end


    t=floor(currentTimeStep1/interval)
    t1=currentTimeStep1-t*interval

    

    if t%2.0!=0.0
        if (cte<0.0)
            currentRestLength=mat.L-factor*mat.L*(1.0-(t1)/interval);
        elseif (cte>0.0)
            currentRestLength=mat.L-factor*mat.L*((t1)/interval);
        else
            currentRestLength=mat.L;
        end
    else
        if (cte>0.0)
            currentRestLength=mat.L-factor*mat.L*(1.0-(t1)/interval);
        elseif (cte<0.0)
            currentRestLength=mat.L-factor*mat.L*((t1)/interval);
        else
            currentRestLength=mat.L;
        end
    end
    return currentRestLength
end


function floorPenetration(x,y,nomSize)
    floor=0.001*1.2
    # floor=0.0
    p=0.0
    d=10.0
    if(y<floor)
    # if(y<floor&& (x<5.0*d || x>=14.0*d))
        p=floor-y
    end
    return p
end
#Returns the interference (in meters) between the collision envelope of this voxel and the floor at Z=0. Positive numbers correspond to interference. If the voxel is not touching the floor 0 is returned.


function penetrationStiffness(E,nomSize)
    return (1.0*E*nomSize)
end 

function floorForce!(dt,pTotalForce,pos,linMom,FloorStaticFriction,N_material)
    E=convert(Float64,N_material.E)
    nomSize=convert(Float64,N_material.nomSize)
    mass=convert(Float64,N_material.mass)
    massInverse=convert(Float64,N_material.massInverse)
    muStatic=convert(Float64,N_material.muStatic)*1.0
    muKinetic=convert(Float64,N_material.muKinetic)*100.0
    _2xSqMxExS=convert(Float64,N_material._2xSqMxExS)
    zetaCollision=convert(Float64,N_material.zetaCollision)
    
    CurPenetration = floorPenetration(convert(Float64,pos.x),convert(Float64,pos.y),nomSize); #for now use the average.


    if (CurPenetration>=0.0)
        vel = linMom*Vector3((massInverse),(massInverse),(massInverse)) #Returns the 3D velocity of this voxel in m/s (GCS)
        horizontalVel= Vector3(convert(Float64,vel.x), 0.0, convert(Float64,vel.z));
        
        normalForce = penetrationStiffness(E,nomSize)*CurPenetration;
        pTotalForce=Vector3( pTotalForce.x, convert(Float64,pTotalForce.y) + normalForce - collisionDampingTranslateC(_2xSqMxExS,zetaCollision)*convert(Float64,vel.y),pTotalForce.z)
        #in the z direction: k*x-C*v - spring and damping

        if (FloorStaticFriction) #If this voxel is currently in static friction mode (no lateral motion) 
            # assert(horizontalVel.Length2() == 0);
            surfaceForceSq = convert(Float64,(pTotalForce.x*pTotalForce.x + pTotalForce.z*pTotalForce.z)); #use squares to avoid a square root
            frictionForceSq = (muStatic*normalForce)*(muStatic*normalForce);


            if (surfaceForceSq > frictionForceSq) 
                FloorStaticFriction=false; #if we're breaking static friction, leave the forces as they currently have been calculated to initiate motion this time step
            end

        else  #even if we just transitioned don't process here or else with a complete lack of momentum it'll just go back to static friction
            #add a friction force opposing velocity according to the normal force and the kinetic coefficient of friction
            leng=sqrt((convert(Float64,vel.x) * convert(Float64,vel.x)) + (0.0 * 0.0) + (convert(Float64,vel.z) * convert(Float64,vel.z)))
            if(leng>0)
                horizontalVel= Vector3(convert(Float64,vel.x)/(leng),0.0,convert(Float64,vel.z)/(leng))
            else
                horizontalVel= Vector3(convert(Float64,vel.x)*(leng),0.0,convert(Float64,vel.z)*(leng))

            end   
            pTotalForce = pTotalForce- Vector3(muKinetic*normalForce,muKinetic*normalForce,muKinetic*normalForce) * horizontalVel;
        end

    else 
        FloorStaticFriction=false;
    end
    
    
    
    return pTotalForce,FloorStaticFriction
end
