# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

######################### 1. Voxel Design ###########################
setup = Dict()

# vibration analysis of voxel beam


### 1.b Draw Lattice

rhino=false


# voxelSize=75
voxelSize=75/1000
# voxelSize=75


latticeSizeX=1
latticeSizeY=5
latticeSizeZ=1

gridSize=10

setup["gridSize"]=gridSize

setup["rhino"]=false
setup["useVoxelList"]=false


setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=1.0; #scale for visualization
setup["hierarchical"]=false; #hierachical simualtion


#simulation params
# setup["numTimeSteps"]=50000/100; #total num of timesteps for simulation
setup["numTimeSteps"]=3000000; #num of saved timesteps for simulation
# setup["numTimeSteps"]=200000/2;  #total num of saved timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=false; #if there is change in temperature
setup["globalDamping"]=0.01; # (usually from 0.1 to 0.4)
setup["globalDamping"]=0.001; # (usually from 0.1 to 0.4)
setup["globalDamping"]=0.00; # (usually from 0.1 to 0.4)
setup["globalDamping"]=0.0001; # (usually from 0.1 to 0.4)




#visualization params
setup["maxNumFiles"]=1200; #num of saved timesteps for visualization, make sure it's smaller than numTimeSteps


######################### 2.b. Materials #########################

#default material
# material1= Dict()
# material1["area"]=2.38*2.38
# material1["density"]=7.85e-9 / 3
# material1["stiffness"]=2000
# material1["poissonRatio"]=0.35
# material1["cTE"]=0.0 #coefficient of thermal expansion

material1= Dict()
material1["area"]=2.38/1000*2.38/1000
material1["density"]=7.85e-9 / 3 *100^3
material1["stiffness"]=2000e9
material1["poissonRatio"]=0.35
material1["cTE"]=0.0 #coefficient of thermal expansion

# material1= Dict()
# material1["area"]=2.38*2.38
# material1["density"]=0.028
# material1["stiffness"]=2000
# material1["poissonRatio"]=0.35
# material1["cTE"]=0.0 #coefficient of thermal expansion

#large bounding box for default material
boundingBoxMaterial1=Dict()
boundingBoxMaterial1["min"]=Dict()
boundingBoxMaterial1["max"]=Dict()

boundingBoxMaterial1["min"]["x"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["y"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["z"]=-voxelSize*gridSize;

boundingBoxMaterial1["max"]["x"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["y"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["z"]= voxelSize*gridSize;



setup["materials"]=[
    [boundingBoxMaterial1,material1]
    ];

######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]

boundingBoxSupport1=Dict()
boundingBoxSupport1["min"]=Dict()
boundingBoxSupport1["max"]=Dict()


boundingBoxSupport1["min"]["x"]=0.0;
boundingBoxSupport1["min"]["y"]=0.0;
boundingBoxSupport1["min"]["z"]=0.0;

boundingBoxSupport1["max"]["x"]= voxelSize*(latticeSizeX);
boundingBoxSupport1["max"]["y"]= voxelSize/8;
boundingBoxSupport1["max"]["z"]= voxelSize*(latticeSizeZ);

setup["supports"]=[
        [boundingBoxSupport1,dof]
    ];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads
load1=Dict()
load1["x"]=200.0
load1["y"]=0.0
load1["z"]=0.0

boundingBoxLoad1=Dict()
boundingBoxLoad1["min"]=Dict()
boundingBoxLoad1["max"]=Dict()

boundingBoxLoad1["min"]["x"]=voxelSize*(latticeSizeX)-voxelSize/4;
boundingBoxLoad1["min"]["y"]=voxelSize*(latticeSizeY)-voxelSize/4;
boundingBoxLoad1["min"]["z"]=0;

boundingBoxLoad1["max"]["x"]=voxelSize*(latticeSizeX);
boundingBoxLoad1["max"]["y"]=voxelSize*(latticeSizeY);
boundingBoxLoad1["max"]["z"]=voxelSize*(latticeSizeZ);


setup["loads"]=[
        [boundingBoxLoad1,load1]
    ];

setup["fixedDisplacements"]=[];

#### 2.d.2 Dynamic Loads
function floorEnabled()
    return false
end

function gravityEnabled()
    return false
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    return N_force
end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    return currentRestLength
end


###########################################################################