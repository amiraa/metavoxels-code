# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

######################### 1. Voxel Design ###########################
setup = Dict()

### 1.b Draw Lattice
voxelSize=75
latticeSizeX=1
latticeSizeY=1
latticeSizeZ=1

gridSize=10

setup["gridSize"]=gridSize

setup["rhino"]=true
setup["useVoxelList"]=false

setup["rhinoFileName"]="../julia/examples/CAD_Rhino/tendon.3dm";
setup["useMaterialList"]=true;
setup["layerIndex"]=["1","2","3","4","5","6","7"];


setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=0.5; #scale for visualization
setup["hierarchical"]=false; #hierachical simualtion


#simulation params
setup["numTimeSteps"]=80000;  #total num of saved timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=true; #if there is change in temperature
setup["globalDamping"]=0.01; # (usually from 0.1 to 0.4)
setup["globalDamping"]=0.0001; # (usually from 0.1 to 0.4)
setup["globalDamping"]=0; # (usually from 0.1 to 0.4)
setup["globalDamping"]=0.0001; # (usually from 0.1 to 0.4)



#visualization params
setup["maxNumFiles"]=50; #num of saved timesteps for visualization, make sure it's smaller than numTimeSteps


######################### 2.b. Materials #########################

#default material
material1= Dict()
material1["area"]=2.38*2.38 #mm ##force N
material1["density"]=7.85e-9 / 3
material1["density"]=0.028 #kg/mm^3
material1["stiffness"]=2000.0 #
material1["poissonRatio"]=0.35
material1["cTE"]=0.0 #coefficient of thermal expansion

#compliant material
material2= Dict()
material2["area"]=2.38*2.38
material2["density"]=7.85e-9 / 3
material2["density"]=0.028
material2["stiffness"]=500.0
material2["poissonRatio"]=0.35
material2["cTE"]=0.0 #coefficient of thermal expansion

tendonStifness=2000
factor=1.4
#tendon material upper
material3= Dict()
material3["area"]=2.38*2.38
material3["density"]=7.85e-9 / 3
material3["density"]=0.028
material3["stiffness"]=tendonStifness
material3["poissonRatio"]=0.35
material3["cTE"]=0.28*factor #coefficient of thermal expansion

#tendon material upper
material4= Dict()
material4["area"]=2.38*2.38
material4["density"]=7.85e-9 / 3
material4["density"]=0.028
material4["stiffness"]=tendonStifness
material4["poissonRatio"]=0.35
material4["cTE"]=0.15*factor #coefficient of thermal expansion

#tendon material upper
material5= Dict()
material5["area"]=2.38*2.38
material5["density"]=7.85e-9 / 3
material5["density"]=0.028
material5["stiffness"]=tendonStifness
material5["poissonRatio"]=0.35
material5["cTE"]=0.01*factor #coefficient of thermal expansion

#tendon material upper
material6= Dict()
material6["area"]=2.38*2.38
material6["density"]=7.85e-9 / 3
material6["density"]=0.028
material6["stiffness"]=tendonStifness
material6["poissonRatio"]=0.35
material6["cTE"]=0.08*factor #coefficient of thermal expansion

#tendon material upper
material7= Dict()
material7["area"]=2.38*2.38
material7["density"]=7.85e-9 / 3
material7["density"]=0.028
material7["stiffness"]=tendonStifness
material7["poissonRatio"]=0.35
material7["cTE"]=0.05*factor #coefficient of thermal expansion



#large bounding box for default material
boundingBoxMaterial1=Dict()
boundingBoxMaterial1["min"]=Dict()
boundingBoxMaterial1["max"]=Dict()

boundingBoxMaterial1["min"]["x"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["y"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["z"]=-voxelSize*gridSize;

boundingBoxMaterial1["max"]["x"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["y"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["z"]= voxelSize*gridSize;




setup["materials"]=[
    [boundingBoxMaterial1,material1],
    # [boundingBoxMaterial2,material3],
    # [boundingBoxMaterial3,material2]
];
setup["materialList"]=[
    material1,
    material2,
    material3,
    material4,
    material5,
    material6,
    material7
];

######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]

boundingBoxSupport1=Dict()
boundingBoxSupport1["min"]=Dict()
boundingBoxSupport1["max"]=Dict()


boundingBoxSupport1["min"]["x"]=-voxelSize/4;
boundingBoxSupport1["min"]["y"]=0.0-voxelSize/8;
boundingBoxSupport1["min"]["z"]=0.0-voxelSize/8;

boundingBoxSupport1["max"]["x"]= voxelSize/4;
boundingBoxSupport1["max"]["y"]= voxelSize*(latticeSizeY)+voxelSize/8;
boundingBoxSupport1["max"]["z"]= voxelSize*(latticeSizeZ)+voxelSize/8;

setup["supports"]=[
        [boundingBoxSupport1,dof]
];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads
load1=Dict()
load1["x"]=0.0
load1["y"]=0.0
load1["z"]=0.0

boundingBoxLoad1=Dict()
boundingBoxLoad1["min"]=Dict()
boundingBoxLoad1["max"]=Dict()

boundingBoxLoad1["min"]["x"]=0-voxelSize/8;
boundingBoxLoad1["min"]["y"]=voxelSize*(latticeSizeY)-voxelSize/4;
boundingBoxLoad1["min"]["z"]=0-voxelSize/8;

boundingBoxLoad1["max"]["x"]=voxelSize*(latticeSizeX)+voxelSize/8;
boundingBoxLoad1["max"]["y"]=voxelSize*(latticeSizeY)+voxelSize/8;
boundingBoxLoad1["max"]["z"]=voxelSize*(latticeSizeZ)+voxelSize/8;


setup["loads"]=[
        # [boundingBoxLoad1,load1]
];

setup["fixedDisplacements"]=[];

#### 2.d.2 Dynamic Loads
function floorEnabled()
    return false
end

function gravityEnabled()
    return false
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    return N_force
end

# function updateTemperature(currentRestLength,currentTimeStep,mat)
#     return currentRestLength
# end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    if currentTimeStep<40000
        temp=-2.0*currentTimeStep/40000 
        currentRestLength=mat.L+mat.L*(temp*mat.cTE)
    # elseif currentTimeStep==25000
    #     temp=0
    #     currentRestLength=mat.L+mat.L*(temp*mat.cTE)
    end
    return currentRestLength
end


###########################################################################