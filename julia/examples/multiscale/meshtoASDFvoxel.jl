using EnhancedGJK ,GeometryTypes,LinearAlgebra,BenchmarkTools;
# make sure MeshIO is old version (0.3.2) to use geometry types and not gemoetry basics
# Pkg.add(Pkg.PackageSpec(;name="MeshIO", version="0.3.2"))
using MeshIO ,FileIO,CoordinateTransformations,RegionTrees;
using MeshCat;

# using GeometryBasics
import StaticArrays: SVector
import JSON

include("../../include/asdf/adaptive_distance_fields.jl")
using .AdaptivelySampledDistanceFields
include("../../include/asdf/reference_distance.jl")
include("../../include/asdf/asdf_functions.jl")
######################### Import Mesh and SDF Functions ###########################

wing =load("../julia/examples/CAD_Rhino/wing_rot.stl")

## if convex mesh
# origin = SVector(-10., -3., -12)
# widths = SVector(24., 24, 24)

# insideOnly=true

# maxDivision=5 #how many divisions
# minDivision=6 #how many divisions


# minSize=widths[1]/(2^minDivision) #min voxel size
# maxSize=widths[1]/(2^maxDivision) #max voxel size
# atol=minSize*widths[1]/2*0.1
# println("Min Vox Size=$(minSize),Max Vox Size=$(maxSize), atol=$(atol)")


# rtol=0.0
# adaptive = AdaptivelySampledDistanceFields.ASDF(sWing, origin, widths, tol1, tol2)
# boxmesh=getVisQuadtree(adaptive,true, sdfWing)
# save("../CAD/WingMultiscale.stl", boxmesh)

## if concave mesh
resolution=128*2
mesh=wing
empty=false
voxels,verts_min,verts_max=_voxelize(mesh, resolution,empty);
###########################

origin = SVector(-10., -4., -12)
widths = SVector(24., 24, 24) #should all be the same
w=widths[1]

insideOnly=true

maxDivision=5 #how many divisions (biggest)
minDivision=5 #how many divisions (smallest)

minSize=w/(2^minDivision) #min voxel size
maxSize=w/(2^maxDivision) #max voxel size
atol=minSize*w/2*0.1 
rtol=0.0

println("Desired Min Vox Size=$(minSize),Max Vox Size=$(maxSize), atol=$(atol)")

#######
# s_wing=ReferenceDistance.signed_distance(wing)
sdfWing=signed_distance1(wing)
sWing=adjustedSignedDistance(wing) #use this when convex mesh

sWingConcave=signedDistanceConcave(wing,voxels,resolution,verts_min,verts_max) 
sExWingConcave=exteriorDistanceConcave(wing,voxels,resolution,verts_min,verts_max) 

#####

adaptive = AdaptivelySampledDistanceFields.ASDF(sExWingConcave, origin, widths, rtol, atol,maxSize)
# adaptive = AdaptivelySampledDistanceFields.ASDF(sWing, origin, widths, rtol, atol,maxSize)
# adaptive = AdaptivelySampledDistanceFields.ASDF(sWingConcave, origin, widths, rtol, atol,maxSize)

# boxmesh=getVisQuadtreeNonConvex(adaptive,insideOnly,voxels,resolution,verts_min,verts_max)
# save("../CAD/WingMultiscale1.stl", boxmesh)

######################### Export Voxels/cubes ###########################

cubes=[]

for leaf in allleaves(adaptive.root)
    for face in RegionTrees.faces(leaf.boundary)
        p=(RegionTrees.center(leaf.boundary))
        pointX=Int(round(p[1],digits=0)); pointY=Int(round(p[2],digits=0)); pointZ=Int(round(p[3],digits=0))
        if pointInsideVoxelGrid([p[1] p[2] p[3]],voxels,resolution,verts_min,verts_max)     #println(leaf.boundary.widths)
            append!(cubes,[leaf.boundary])
        end
    end
end
cubes=unique(cubes)

sort!(cubes, by = x -> x.widths[2]);
minSize=cubes[1].widths[1]
maxSize=cubes[end].widths[1]
println("Real Min Vox Size=$(minSize),Max Vox Size=$(maxSize)")


sort!(cubes, by = x -> x.origin[1]);
minOrigin1=cubes[1].origin[1]
sort!(cubes, by = x -> x.origin[2]);
minOrigin2=cubes[1].origin[2]
sort!(cubes, by = x -> x.origin[3]);
minOrigin3=cubes[1].origin[3]

minOrigin=[minOrigin1,minOrigin2,minOrigin3].+minSize/2.0

voxs=[]
for cube in cubes
    size=cube.widths[1]/minSize
    append!(voxs,[[((cube.origin.-minOrigin)./minSize .+(0.5*size)) ,[size]]])
end



voxx = Dict()
voxx["voxs"]=voxs
# pass data as a json string (how it shall be displayed in a file)
stringdata = JSON.json(voxx)
# write the file with the stringdata variable information
open("../json/voxs.json", "w") do f
        write(f, stringdata)
end



