# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

######################### 1. Voxel Design ###########################
setup = Dict()

### 1.b Draw Lattice


voxelSize=0.001
latticeSizeX=1
latticeSizeY=1
latticeSizeZ=1
setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ
gridSize=10
setup["gridSize"]=10

setup["rhino"]=false;
setup["useVoxelList"]=true;

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=1e4; #scale for visualization
setup["hierarchical"]=true; #hierachical simualtion
setup["multiscale"]=true; #multiscale simualtion



#simulation params
setup["numTimeSteps"]=1000; #num of saved timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=false; #if there is change in temperature
setup["globalDamping"]=0.2; # (usually from 0.1 to 0.4)


#visualization params
setup["maxNumFiles"]=10; #num of saved timesteps for visualization


######################### 2.b. Materials #########################

#default material
material1= Dict()
material1["area"]=voxelSize*voxelSize
material1["density"]=1e3
material1["stiffness"]=1e6
material1["poissonRatio"]=0.0
material1["cTE"]=0.0 #coefficient of thermal expansion
material1["scale"]=1.0 #for multiscale simulation

material2= Dict()
material2["area"]=voxelSize*voxelSize
material2["density"]=1e3
material2["stiffness"]=1e6*2.0
material2["poissonRatio"]=0.0
material2["cTE"]=0.0 #coefficient of thermal expansion
material2["scale"]=2.0 #for multiscale simulation


#//allowed x, yand z are from -gridSize to +gridSize (floor is at 0)
setup["voxelList"]=[];
for i in 1:5
    append!(setup["voxelList"], [[[0,i,1],material1]])
    append!(setup["voxelList"], [[[0,i,2],material1]])
end

append!(setup["voxelList"], [[[1,5,1],material1]])
append!(setup["voxelList"], [[[1,5,2],material1]])

append!(setup["voxelList"], [[[2,5,1],material1]])
append!(setup["voxelList"], [[[2,5,2],material1]])

append!(setup["voxelList"], [[[3,4,1],material1]])
append!(setup["voxelList"], [[[3,4,2],material1]])

append!(setup["voxelList"], [[[4,4,1],material1]])
append!(setup["voxelList"], [[[4,4,2],material1]])

for i in 3:5
    append!(setup["voxelList"], [[[i,3,1],material1]])
    append!(setup["voxelList"], [[[i,3,2],material1]])
end

for i in 5:7
    append!(setup["voxelList"], [[[i,2,1],material1]])
    append!(setup["voxelList"], [[[i,2,2],material1]])
end

for i in 5:8
    append!(setup["voxelList"], [[[i,1,1],material1]])
    append!(setup["voxelList"], [[[i,1,2],material1]])
end

append!(setup["voxelList"], [[[1.5,1.5,1.5],material2]])
append!(setup["voxelList"], [[[3.5,1.5,1.5],material2]])
append!(setup["voxelList"], [[[1.5,3.5,1.5],material2]])

# setup["voxelList"]=[
#     [[0.5,0.5,0.5],material2],

#     [[2,0,0],material1],
#     [[2,0,1],material1],
    
#     [[2,1,0],material1],
#     [[2,1,1],material1],

#     [[1,2,0],material1],
#     [[1,2,1],material1],

#     [[0,2,0],material1],
#     [[0,2,1],material1],

#     [[-1,0,0],material1],
#     [[-1,0,1],material1],
    
#     [[-1,1,0],material1],
#     [[-1,1,1],material1],

#     [[1,-1,0],material1],
#     [[1,-1,1],material1],

#     [[0,-1,0],material1],
#     [[0,-1,1],material1],

    

#     # [[0.5,0.5,2.5],material2],
# ];



###############################################################

setup["materials"]=[
    # [boundingBoxMaterial1,material1],
    # [boundingBoxMaterial2,material2]
];

######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]

boundingBoxSupport1=Dict()
boundingBoxSupport1["min"]=Dict()
boundingBoxSupport1["max"]=Dict()


boundingBoxSupport1["min"]["x"]= 0;
boundingBoxSupport1["min"]["y"]= voxelSize;
boundingBoxSupport1["min"]["z"]= voxelSize;

boundingBoxSupport1["max"]["x"]= voxelSize*1;
boundingBoxSupport1["max"]["y"]= voxelSize*6;
boundingBoxSupport1["max"]["z"]= voxelSize*3;

setup["supports"]=[
        [boundingBoxSupport1,dof]
    ];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads
load1=Dict()
load1["x"]=0.0
load1["y"]=-0.00215
# load1["y"]=-0.01
load1["z"]=0.0

boundingBoxLoad1=Dict()
boundingBoxLoad1["min"]=Dict()
boundingBoxLoad1["max"]=Dict()

boundingBoxLoad1["min"]["x"]=voxelSize*1;
boundingBoxLoad1["min"]["y"]=(5)*voxelSize;
boundingBoxLoad1["min"]["z"]=voxelSize;

boundingBoxLoad1["max"]["x"]=voxelSize*3;
boundingBoxLoad1["max"]["y"]=(6)*voxelSize;
boundingBoxLoad1["max"]["z"]=voxelSize*3;

boundingBoxLoad2=Dict()
boundingBoxLoad2["min"]=Dict()
boundingBoxLoad2["max"]=Dict()

boundingBoxLoad2["min"]["x"]=voxelSize*3;
boundingBoxLoad2["min"]["y"]=(4)*voxelSize;
boundingBoxLoad2["min"]["z"]=voxelSize;

boundingBoxLoad2["max"]["x"]=voxelSize*5;
boundingBoxLoad2["max"]["y"]=(5)*voxelSize;
boundingBoxLoad2["max"]["z"]=voxelSize*3;


boundingBoxLoad3=Dict()
boundingBoxLoad3["min"]=Dict()
boundingBoxLoad3["max"]=Dict()

boundingBoxLoad3["min"]["x"]=voxelSize*5;
boundingBoxLoad3["min"]["y"]=(3)*voxelSize;
boundingBoxLoad3["min"]["z"]=voxelSize;

boundingBoxLoad3["max"]["x"]=voxelSize*6;
boundingBoxLoad3["max"]["y"]=(4)*voxelSize;
boundingBoxLoad3["max"]["z"]=voxelSize*3;

boundingBoxLoad4=Dict()
boundingBoxLoad4["min"]=Dict()
boundingBoxLoad4["max"]=Dict()

boundingBoxLoad4["min"]["x"]=voxelSize*6;
boundingBoxLoad4["min"]["y"]=(2)*voxelSize;
boundingBoxLoad4["min"]["z"]=voxelSize;

boundingBoxLoad4["max"]["x"]=voxelSize*8;
boundingBoxLoad4["max"]["y"]=(3)*voxelSize;
boundingBoxLoad4["max"]["z"]=voxelSize*3;

boundingBoxLoad5=Dict()
boundingBoxLoad5["min"]=Dict()
boundingBoxLoad5["max"]=Dict()

boundingBoxLoad5["min"]["x"]=voxelSize*8;
boundingBoxLoad5["min"]["y"]=(1)*voxelSize;
boundingBoxLoad5["min"]["z"]=voxelSize;

boundingBoxLoad5["max"]["x"]=voxelSize*9;
boundingBoxLoad5["max"]["y"]=(2)*voxelSize;
boundingBoxLoad5["max"]["z"]=voxelSize*3;


setup["loads"]=[
        [boundingBoxLoad1,load1],
        [boundingBoxLoad2,load1],
        [boundingBoxLoad3,load1],
        [boundingBoxLoad4,load1],
        [boundingBoxLoad5,load1],
    ];

setup["fixedDisplacements"]=[

];


#### 2.d.2 Dynamic Loads
function floorEnabled()
    return false
end

function gravityEnabled()
    return false
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    return N_force
end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    return currentRestLength
end


