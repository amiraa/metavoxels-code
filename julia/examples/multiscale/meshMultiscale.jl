# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020


######################### 1. Voxel Design ###########################
setup = Dict()

### 1.b Draw Lattice


voxelSize=0.001
latticeSizeX=1
latticeSizeY=1
latticeSizeZ=1
gridSize=50

setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ
setup["gridSize"]=gridSize

setup["rhino"]=false;
setup["useVoxelList"]=true;

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=1e4; #scale for visualization
setup["hierarchical"]=true; #hierachical simualtion
setup["multiscale"]=true; #multiscale simualtion



#simulation params
setup["numTimeSteps"]=1000; #num of saved timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=false; #if there is change in temperature
setup["globalDamping"]=0.2; # (usually from 0.1 to 0.4)


#visualization params
setup["maxNumFiles"]=10; #num of saved timesteps for visualization


######################### 2.b. Materials #########################

#default material
material1= Dict()
material1["area"]=voxelSize*voxelSize
material1["density"]=1e3
material1["stiffness"]=1e6
material1["poissonRatio"]=0.0
material1["cTE"]=0.0 #coefficient of thermal expansion
material1["scale"]=1.0 #for multiscale simulation

material2= Dict()
material2["area"]=voxelSize*voxelSize
material2["density"]=1e3
material2["stiffness"]=1e6*2.0
material2["poissonRatio"]=0.0
material2["cTE"]=0.0 #coefficient of thermal expansion
material2["scale"]=2.0 #for multiscale simulation


#//allowed x, yand z are from -gridSize to +gridSize (floor is at 0)
setup["voxelList"]=[];
for vox in voxs
    if vox[2][1]==1
        append!(setup["voxelList"], [[[vox[1][1],vox[1][3],vox[1][2]],material1]])
    elseif vox[2][1]==2
        append!(setup["voxelList"], [[[vox[1][1],vox[1][3],vox[1][2]],material2]])
    end
end




###############################################################

setup["materials"]=[
    # [boundingBoxMaterial1,material1],
    # [boundingBoxMaterial2,material2]
];

######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]

boundingBoxSupport1=Dict()
boundingBoxSupport1["min"]=Dict()
boundingBoxSupport1["max"]=Dict()


boundingBoxSupport1["min"]["x"]= 0;
boundingBoxSupport1["min"]["y"]= 0;
boundingBoxSupport1["min"]["z"]= 0;

boundingBoxSupport1["max"]["x"]= voxelSize*2;
boundingBoxSupport1["max"]["y"]= voxelSize*10;
boundingBoxSupport1["max"]["z"]= voxelSize*gridSize;

setup["supports"]=[
        [boundingBoxSupport1,dof]
    ];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads
load1=Dict()
load1["x"]=0.0
load1["y"]=-0.0215
load1["z"]=0.0

boundingBoxLoad1=Dict()
boundingBoxLoad1["min"]=Dict()
boundingBoxLoad1["max"]=Dict()

boundingBoxLoad1["min"]["x"]=0;
boundingBoxLoad1["min"]["y"]=0;
boundingBoxLoad1["min"]["z"]=0;

boundingBoxLoad1["max"]["x"]=voxelSize*gridSize;
boundingBoxLoad1["max"]["y"]=voxelSize*gridSize;
boundingBoxLoad1["max"]["z"]=voxelSize*gridSize;



setup["loads"]=[
        [boundingBoxLoad1,load1],

    ];

setup["fixedDisplacements"]=[

];


#### 2.d.2 Dynamic Loads
function floorEnabled()
    return false
end

function gravityEnabled()
    return false
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    return N_force
end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    return currentRestLength
end


