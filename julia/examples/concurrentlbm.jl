# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

######################### 2D LBM Setup ###########################


######################### 1. Voxel Design ###########################


################################################
### 1.b Draw Lattice
setup = Dict()

voxelSize=0.001

latticeSizeX=nx
latticeSizeY=1
latticeSizeZ=ny
gridSize=nx*2.0


setup["gridSize"]=gridSize

setup["rhino"]=false
setup["useVoxelList"]=true
setup["logging"]=logging


setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=1e4; #scale for visualization
setup["hierarchical"]=true; #hierachical simualtion


#simulation params
setup["numTimeSteps"]=200; #num of timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=false; #if there is change in temperature
setup["globalDamping"]=0.15; # (usually from 0.1 to 0.4)


#visualization params
setup["maxNumFiles"]=5; #num of saved timesteps for visualization


######################### 2.b. Materials #########################

#default neural material
material1= Dict()
material1["area"]=voxelSize*voxelSize
material1["density"]=1e3
material1["stiffness"]=1e6
material1["poissonRatio"]=0.0
material1["cTE"]=0.0 #coefficient of thermal expansion

#large bounding box for default material
boundingBoxMaterial1=Dict()
boundingBoxMaterial1["min"]=Dict()
boundingBoxMaterial1["max"]=Dict()

boundingBoxMaterial1["min"]["x"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["y"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["z"]=-voxelSize*gridSize;

boundingBoxMaterial1["max"]["x"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["y"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["z"]= voxelSize*gridSize;

setup["materials"]=[
    [boundingBoxMaterial1,material1]
];

#


#//allowed x, yand z are from -gridSize to +gridSize (floor is at 0)

setup["voxelList"]=[]


BOUND1=zeros(nx,ny);
BOUND1[Int((nx/2-2)/2):Int((nx/2+2)/2),2:ny-20].=1
BOUND1=Bool.(BOUND1.>0)
ON2D1 = findall(BOUND1);
for o in ON2D1
    append!(setup["voxelList"],[ [[o[1],0,o[2]],material1] ])
end
 



######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]


center = [nx/4, ny/4]
boundingBoxSupport=Dict()
boundingBoxSupport["min"]=Dict()
boundingBoxSupport["max"]=Dict()

boundingBoxSupport["min"]["x"]=(center[1]-4)*voxelSize;
boundingBoxSupport["min"]["y"]=-voxelSize;
boundingBoxSupport["min"]["z"]=2*voxelSize;

boundingBoxSupport["max"]["x"]=(center[1]+4)*voxelSize;
boundingBoxSupport["max"]["y"]=voxelSize;
boundingBoxSupport["max"]["z"]=3*voxelSize+voxelSize*2;

setup["supports"]=[
        [boundingBoxSupport,dof]
    ];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads

#get loads from LBM calculation
#TODO make sure later conversion is correct (friction..)
#CALLED ELSEWHERE
edgeList=findall(x->x!=0,presD)

setup["loads"]=[];

sc=0.0
if t==1
    sc=0.5
end
for e in edgeList 
    load2=Dict()
    load2["x"]=velD[e][1]*sc
    load2["y"]=0.0
    load2["z"]=velD[e][2]*sc
    

    boundingBoxLoad2=Dict()
    boundingBoxLoad2["min"]=Dict()
    boundingBoxLoad2["max"]=Dict()

    boundingBoxLoad2["min"]["x"]=e[1]/voxelScaleToLBM*voxelSize
    boundingBoxLoad2["min"]["y"]=-voxelSize;
    boundingBoxLoad2["min"]["z"]=e[2]/voxelScaleToLBM*voxelSize;

    boundingBoxLoad2["max"]["x"]=e[1]/voxelScaleToLBM*voxelSize+voxelSize;
    boundingBoxLoad2["max"]["y"]=voxelSize;
    boundingBoxLoad2["max"]["z"]=e[2]/voxelScaleToLBM*voxelSize+voxelSize;

    append!(setup["loads"],[[boundingBoxLoad2,load2]])
end


setup["fixedDisplacements"]=[];


#### 2.d.2 Dynamic Loads
function floorEnabled()
    return false
end

function gravityEnabled()
    return false
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    if currentTimeStep==0
        display(sum(velD))
    end

    edgeList=findall(x->x!=0,presD)

    sc=0.5
    loadX=0
    loadY=0
    loadZ=0

    for e in edgeList 
        minx=e[1]/voxelScaleToLBM*voxelSize
        miny=-voxelSize;
        minz=e[2]/voxelScaleToLBM*voxelSize;

        maxx=e[1]/voxelScaleToLBM*voxelSize+voxelSize;
        maxy=voxelSize;
        maxz=e[2]/voxelScaleToLBM*voxelSize+voxelSize;

        if N_currentPosition.x>=minx && N_currentPosition.x<=maxx && N_currentPosition.z>=minz && N_currentPosition.z<=maxz
            loadX+=velD[e][1]*sc;
            loadY+=0.0;
            loadZ+=velD[e][2]*sc;
            # println("here")
        end
    end
    return Vector3(loadX,loadY,loadZ)
end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    return currentRestLength
end
