# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

######################### 1. Voxel Design ###########################
setup = Dict()

### 1.b Draw Lattice

rhino=false

voxelSize=75
latticeSizeX=4
latticeSizeY=1
latticeSizeZ=4

gridSize=10

setup["gridSize"]=gridSize

setup["rhino"]=false
setup["useVoxelList"]=false


setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=0.25; #scale for visualization
setup["hierarchical"]=false; #hierachical simualtion


#simulation params
setup["numTimeSteps"]=10000; #total num of timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=false; #if there is change in temperature
setup["globalDamping"]=0.01; # (usually from 0.1 to 0.4)



#visualization params
setup["maxNumFiles"]=50; #num of saved timesteps for visualization, make sure it's bigger than numTimeSteps


######################### 2.b. Materials #########################

#default material
material1= Dict()
material1["area"]=2.38*2.38
material1["density"]=7.85e-9 / 3
material1["density"]=0.028
material1["stiffness"]=2000.0
material1["poissonRatio"]=0.35
material1["cTE"]=0.0 #coefficient of thermal expansion

#large bounding box for default material
boundingBoxMaterial1=Dict()
boundingBoxMaterial1["min"]=Dict()
boundingBoxMaterial1["max"]=Dict()

boundingBoxMaterial1["min"]["x"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["y"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["z"]=-voxelSize*gridSize;

boundingBoxMaterial1["max"]["x"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["y"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["z"]= voxelSize*gridSize;



setup["materials"]=[
    [boundingBoxMaterial1,material1]
    ];

######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]

boundingBoxSupport1=Dict()
boundingBoxSupport1["min"]=Dict()
boundingBoxSupport1["max"]=Dict()


boundingBoxSupport1["min"]["x"]=0.0;
boundingBoxSupport1["min"]["y"]=0.0;
boundingBoxSupport1["min"]["z"]=0.0;

boundingBoxSupport1["max"]["x"]= voxelSize/4;
boundingBoxSupport1["max"]["y"]= voxelSize*(latticeSizeY);
boundingBoxSupport1["max"]["z"]= voxelSize*(latticeSizeZ);

setup["supports"]=[
        [boundingBoxSupport1,dof]
    ];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads
load1=Dict()
load1["x"]=0.0
load1["y"]= 1.0 
load1["z"]=0.0

load2=Dict()
load2["x"]=0.0
load2["y"]= 2.0
load2["z"]=0.0

load3=Dict()
load3["x"]=0.0
load3["y"]= 3.0
load3["z"]=0.0

load4=Dict()
load4["x"]=0.0
load4["y"]= 4.0
load4["z"]=0.0

boundingBoxLoad1=Dict()
boundingBoxLoad1["min"]=Dict()
boundingBoxLoad1["max"]=Dict()
boundingBoxLoad1["min"]["x"]=voxelSize*(latticeSizeX)-voxelSize;
boundingBoxLoad1["min"]["y"]=voxelSize*(latticeSizeY)-voxelSize/4;
boundingBoxLoad1["min"]["z"]=0;
boundingBoxLoad1["max"]["x"]=voxelSize*(latticeSizeX);
boundingBoxLoad1["max"]["y"]=voxelSize*(latticeSizeY);
boundingBoxLoad1["max"]["z"]=voxelSize*(1);

boundingBoxLoad2=Dict()
boundingBoxLoad2["min"]=Dict()
boundingBoxLoad2["max"]=Dict()
boundingBoxLoad2["min"]["x"]=voxelSize*(latticeSizeX)-voxelSize;
boundingBoxLoad2["min"]["y"]=voxelSize*(latticeSizeY)-voxelSize/4;
boundingBoxLoad2["min"]["z"]=voxelSize*(1);
boundingBoxLoad2["max"]["x"]=voxelSize*(latticeSizeX);
boundingBoxLoad2["max"]["y"]=voxelSize*(latticeSizeY);
boundingBoxLoad2["max"]["z"]=voxelSize*(2);

boundingBoxLoad3=Dict()
boundingBoxLoad3["min"]=Dict()
boundingBoxLoad3["max"]=Dict()
boundingBoxLoad3["min"]["x"]=voxelSize*(latticeSizeX)-voxelSize;
boundingBoxLoad3["min"]["y"]=voxelSize*(latticeSizeY)-voxelSize/4;
boundingBoxLoad3["min"]["z"]=voxelSize*(2);
boundingBoxLoad3["max"]["x"]=voxelSize*(latticeSizeX);
boundingBoxLoad3["max"]["y"]=voxelSize*(latticeSizeY);
boundingBoxLoad3["max"]["z"]=voxelSize*(3);

boundingBoxLoad4=Dict()
boundingBoxLoad4["min"]=Dict()
boundingBoxLoad4["max"]=Dict()
boundingBoxLoad4["min"]["x"]=voxelSize*(latticeSizeX)-voxelSize;
boundingBoxLoad4["min"]["y"]=voxelSize*(latticeSizeY)-voxelSize/4;
boundingBoxLoad4["min"]["z"]=voxelSize*(3);
boundingBoxLoad4["max"]["x"]=voxelSize*(latticeSizeX);
boundingBoxLoad4["max"]["y"]=voxelSize*(latticeSizeY);
boundingBoxLoad4["max"]["z"]=voxelSize*(latticeSizeZ);


setup["loads"]=[
        [boundingBoxLoad1,load1],
        [boundingBoxLoad2,load2],
        [boundingBoxLoad3,load3],
        [boundingBoxLoad4,load4]
    ];

setup["fixedDisplacements"]=[];

#### 2.d.2 Dynamic Loads
function floorEnabled()
    return false
end

function gravityEnabled()
    return false
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    numTimeSteps=10000
    if (currentTimeStep<numTimeSteps/4)
        if ( convert(Float64,N_force.y)==1.0)
            return Vector3(0,-10.0,0)
        end
    elseif (currentTimeStep<2*numTimeSteps/4&&currentTimeStep>numTimeSteps/4)
        if (convert(Float64,N_force.y)==2.0)
            return Vector3(0,-10.0,0)
        end
    elseif (currentTimeStep<3*numTimeSteps/4&&currentTimeStep>2*numTimeSteps/4)
        if (convert(Float64,N_force.y)==3.0)
            return Vector3(0,-10.0,0)
        end
    elseif (currentTimeStep>3*numTimeSteps/4)
        if (convert(Float64,N_force.y)==4.0)
            return Vector3(0,-10.0,0)
        end
    end

    return Vector3(0,0,0)
end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    return currentRestLength
end


###########################################################################