# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

######################### 1. Voxel Design ###########################
setup = Dict()

### 1.b Draw Lattice

rhino=false

voxelSize=0.001
latticeSizeX=1
latticeSizeY=1
latticeSizeZ=1
setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ
gridSize=10
setup["gridSize"]=gridSize

setup["rhino"]=false;
setup["useVoxelList"]=true;

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=1e4; #scale for visualization
setup["hierarchical"]=true; #hierachical simualtion


#simulation params
setup["numTimeSteps"]=10000; #num of saved timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=false; #if there is change in temperature
setup["globalDamping"]=0.2; # (usually from 0.1 to 0.4)


#visualization params
setup["maxNumFiles"]=3; #num of saved timesteps for visualization

######################### 2.b. Materials #########################

#default material
material1= Dict()
material1["area"]=voxelSize*voxelSize
material1["density"]=2e3
material1["stiffness"]=1e6
material1["poissonRatio"]=0.0
material1["cTE"]=0.0 #coefficient of thermal expansion


#second material
material2= Dict()
material2["area"]=voxelSize*voxelSize
material2["density"]=1e3
material2["stiffness"]=1e6
material2["poissonRatio"]=0.0
material2["cTE"]=0.0 #coefficient of thermal expansion

height=3

#//allowed x, yand z are from -gridSize to +gridSize (floor is at 0)
setup["voxelList"]=[
        [[0,height,0],material1],#spine
        [[0,height,1],material1],#spine
        [[0,height,2],material1],#spine
        [[0,height,3],material1],#spine
        [[0,height,4],material1],#spine

        [[-1,height,0],material2],#leg1
        [[-2,height,0],material2],#leg1
        [[-3,height,0],material2],#leg1

        [[1,height,0],material2],#leg2
        [[2,height,0],material2],#leg2
        [[3,height,0],material2],#leg2

        [[-1,height,4],material2],#leg3
        [[-2,height,4],material2],#leg3
        [[-3,height,4],material2],#leg3

        [[1,height,4],material2],#leg4
        [[2,height,4],material2],#leg4
        [[3,height,4],material1] #leg4
];
###############################################################

setup["materials"]=[
];

######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]

setup["supports"]=[
];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads
load1=Dict()
load1["x"]=0.0
load1["y"]=0.0
load1["z"]=1.0

load2=Dict()
load2["x"]=0.0
load2["y"]=0.0
load2["z"]=2.0

boundingBoxLoad1=Dict()
boundingBoxLoad1["min"]=Dict()
boundingBoxLoad1["max"]=Dict()

boundingBoxLoad1["min"]["x"]=voxelSize;
boundingBoxLoad1["min"]["y"]=(height)*voxelSize;
boundingBoxLoad1["min"]["z"]=-voxelSize/2;

boundingBoxLoad1["max"]["x"]=voxelSize*2.0;
boundingBoxLoad1["max"]["y"]=(height+2)*voxelSize;
boundingBoxLoad1["max"]["z"]=voxelSize/2+voxelSize*5.0;


boundingBoxLoad2=Dict()
boundingBoxLoad2["min"]=Dict()
boundingBoxLoad2["max"]=Dict()

boundingBoxLoad2["min"]["x"]=-voxelSize
boundingBoxLoad2["min"]["y"]=(height)*voxelSize;
boundingBoxLoad2["min"]["z"]=-voxelSize/2;

boundingBoxLoad2["max"]["x"]=0;
boundingBoxLoad2["max"]["y"]=(height+2)*voxelSize;
boundingBoxLoad2["max"]["z"]=voxelSize/2+voxelSize*5.0;


setup["loads"]=[
        [boundingBoxLoad1,load1],
        [boundingBoxLoad2,load2],
];


setup["fixedDisplacements"]=[
];


#### 2.d.2 Dynamic Loads
function floorEnabled()
    return true
end

function gravityEnabled()
    return true
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    return currentRestLength
end

# function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
#     return Vector3(0,0,0);
#     return N_force
# end



# function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
#     if (convert(Float64,N_force.z)>0.0)
#         pi=3.14159265359
    
#         a0=0.5; #magnitude
#         l=1.0 ;
#         f=5.0;
#         amp=0.25;
#         amp=0.0;
#         max=10000.0;
#         t=currentTimeStep/max;

#         z= a0* CUDA.cos(2.0*3.14159265359*(f*t+amp))
#         amp=0.25;
#         y= a0* CUDA.sin(2.0*3.14159265359*(f*t+amp))
#         if (convert(Float64,N_force.z)>1.5)
#             z=-z
#             y=-y
#         end

#         return Vector3(0.0,y,z)
#     else
#         return Vector3(0.0,0.0,0.0)
#     end

# end

# function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
#     if (convert(Float64,N_force.z)>0.0)
#         pi=3.14159265359
    
#         a0=0.5; #magnitude
#         l=1.0 ;
#         f=5.0;
#         amp=0.25;
#         amp=0.0;
#         max=10000.0;
#         t=currentTimeStep/max;

#         z= a0* cos(2.0*3.14159265359*(f*t+amp))
#         amp=0.25;
#         y= a0* sin(2.0*3.14159265359*(f*t+amp))
#         if (convert(Float64,N_force.z)>1.5)
#             z=-z
#             y=-y
#         end

#         return Vector3(0.0,y,z)
#     else
#         return Vector3(0.0,0.0,0.0)
#     end

# end

strY=-0.5:1/10000:0.5
strZ=-0.5:1/10000:0.5
function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    if (convert(Float64,N_force.z)>0.0)
  
        z= strZ[currentTimeStep+1]
        y= strY[currentTimeStep+1]
        if (convert(Float64,N_force.z)>1.5)
            z=-z
            y=-y
        end
        return Vector3(0.0,y,z)
    else
        return Vector3(0.0,0.0,0.0)
    end

end

