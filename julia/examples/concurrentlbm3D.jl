# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021

######################### 1. Voxel Design ###########################


################################################
### 1.b Draw Lattice
setup = Dict()

voxelSize=0.001

latticeSizeX=nx
latticeSizeY=ny
latticeSizeZ=nz
gridSize=nx*2.0


setup["gridSize"]=gridSize
setup["rhino"]=false
setup["logging"]=logging


setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=1e4; #scale for visualization
setup["hierarchical"]=true; #hierachical simualtion


#simulation params
setup["numTimeSteps"]=200; #num of timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=false; #if there is change in temperature
setup["globalDamping"]=0.15; # (usually from 0.1 to 0.4)


#visualization params
setup["maxNumFiles"]=5; #num of saved timesteps for visualization


######################### 2.b. Materials #########################

#default neural material
material1= Dict()
material1["area"]=voxelSize*voxelSize
material1["density"]=1e3
material1["stiffness"]=1e6
material1["poissonRatio"]=0.0
material1["cTE"]=0.0 #coefficient of thermal expansion
material1["scale"]=1.0 #for multiscale simulation

material2= Dict()
material2["area"]=voxelSize*voxelSize
material2["density"]=1e3
material2["stiffness"]=1e6*2.0
material2["poissonRatio"]=0.0
material2["cTE"]=0.0 #coefficient of thermal expansion
material2["scale"]=2.0 #for multiscale simulation

#large bounding box for default material
boundingBoxMaterial1=Dict()
boundingBoxMaterial1["min"]=Dict()
boundingBoxMaterial1["max"]=Dict()

boundingBoxMaterial1["min"]["x"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["y"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["z"]=-voxelSize*gridSize;

boundingBoxMaterial1["max"]["x"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["y"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["z"]= voxelSize*gridSize;

setup["materials"]=[
    [boundingBoxMaterial1,material1]
];

#
#//allowed x, yand z are from -gridSize to +gridSize (floor is at 0)

# initial design
# # voxelScaleToLBM=2;
# BOUND1=zeros(Int(nx/2),Int(ny/2),Int(nz/2))
# BOUND1[Int(floor((nx/2-2)/4)):Int(floor((nx/2+2)/2)),1:3,2:Int((nz-4*2))].=1
# BOUND1=Bool.(BOUND1.>0)
# ON2D1 = findall(BOUND1);
# # voxelScaleToLBM=1;
# # BOUND1=zeros(nx,ny,nz)
# # BOUND1[Int(nx/2-2):Int(nx/2+2),2:6,4:nz-4].=1
# # BOUND1=Bool.(BOUND1.>0)
# # ON2D1 = findall(BOUND1);
# setup["useVoxelList"]=true
# setup["voxelList"]=[]
# for o in ON2D1
#     append!(setup["voxelList"],[ [[o[1],o[3],o[2]],material1] ])
# end


# latticeSizeX=7
# latticeSizeY=7
# latticeSizeZ=7
# setup["latticeSizeX"]=latticeSizeX
# setup["latticeSizeY"]=latticeSizeY
# setup["latticeSizeZ"]=latticeSizeZ
# setup["useVoxelList"]=false

setup["useVoxelList"]=true
setup["gridSize"]=50
setup["multiscale"]=true; #multiscale simualtion
setup["voxelList"]=[];
voxShift=[4,4,4]
for vox in voxs
    if vox[2][1]==1
        append!(setup["voxelList"], [[[vox[1][2]+voxShift[1],vox[1][3]+voxShift[2],vox[1][1]+voxShift[3]],material1]])
    elseif vox[2][1]==2
        append!(setup["voxelList"], [[[vox[1][2]+voxShift[1],vox[1][3]+voxShift[2],vox[1][1]+voxShift[3]],material]])
    end
end

######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]

boundingBoxSupport=Dict()
boundingBoxSupport["min"]=Dict()
boundingBoxSupport["max"]=Dict()

# boundingBoxSupport["min"]["x"]=0*voxelSize;
# boundingBoxSupport["min"]["z"]=0*voxelSize;
# boundingBoxSupport["min"]["y"]=0*voxelSize;

# boundingBoxSupport["max"]["x"]=nx*voxelSize;
# boundingBoxSupport["max"]["z"]=nz*voxelSize;
# boundingBoxSupport["max"]["y"]=4*voxelSize;

# boundingBoxSupport["min"]["x"]= 0;
# boundingBoxSupport["min"]["y"]= 0;
# boundingBoxSupport["min"]["z"]= 0;

# boundingBoxSupport["max"]["x"]= voxelSize;
# boundingBoxSupport["max"]["y"]= voxelSize*(latticeSizeY);
# boundingBoxSupport["max"]["z"]= voxelSize*(latticeSizeZ);


boundingBoxSupport["min"]["x"]= 0;
boundingBoxSupport["min"]["y"]= 0;
boundingBoxSupport["min"]["z"]= 0;

boundingBoxSupport["max"]["z"]= voxelSize*(2+4);
boundingBoxSupport["max"]["y"]= voxelSize*(10+4);
boundingBoxSupport["max"]["x"]= voxelSize*gridSize;


setup["supports"]=[
        [boundingBoxSupport,dof]
    ];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads

#get loads from LBM calculation
#TODO make sure later conversion is correct (friction..)

##CALLED ELSEWHERE
edgeList=findall(x->x!=0,presD)

setup["loads"]=[];

sc=0.0
if t==1
    sc=0.0005
end
for e in edgeList 
    load2=Dict()
    load2["x"]=velD[e][1]*sc
    load2["y"]=velD[e][3]*sc
    load2["z"]=velD[e][2]*sc
    

    boundingBoxLoad2=Dict()
    boundingBoxLoad2["min"]=Dict();boundingBoxLoad2["max"]=Dict();

    boundingBoxLoad2["min"]["x"]=round(e[1]/voxelScaleToLBM)*voxelSize;
    boundingBoxLoad2["min"]["y"]=round(e[3]/voxelScaleToLBM)*voxelSize;
    boundingBoxLoad2["min"]["z"]=round(e[2]/voxelScaleToLBM)*voxelSize;

    boundingBoxLoad2["max"]["x"]=round(e[1]/voxelScaleToLBM)*voxelSize+voxelSize/voxelScaleToLBM;
    boundingBoxLoad2["max"]["y"]=round(e[3]/voxelScaleToLBM)*voxelSize+voxelSize/voxelScaleToLBM;
    boundingBoxLoad2["max"]["z"]=round(e[2]/voxelScaleToLBM)*voxelSize+voxelSize/voxelScaleToLBM;

    append!(setup["loads"],[[boundingBoxLoad2,load2]])
end


setup["fixedDisplacements"]=[];


#### 2.d.2 Dynamic Loads
function floorEnabled()
    return false
end

function gravityEnabled()
    return false
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    

    edgeList=findall(x->x!=0,presD)

    sc=0.0005
    loadX=0
    loadY=0
    loadZ=0

    for e in edgeList 
        minx=round(e[1]/voxelScaleToLBM)*voxelSize
        miny=round(e[3]/voxelScaleToLBM)*voxelSize;
        minz=round(e[2]/voxelScaleToLBM)*voxelSize;

        maxx=round(e[1]/voxelScaleToLBM)+voxelSize/voxelScaleToLBM;
        maxy=round(e[3]/voxelScaleToLBM)+voxelSize/voxelScaleToLBM;
        maxz=round(e[2]/voxelScaleToLBM)+voxelSize/voxelScaleToLBM;

        if N_currentPosition.x>=minx && N_currentPosition.x<=maxx && N_currentPosition.z>=minz && N_currentPosition.z<=maxz && N_currentPosition.y>=miny && N_currentPosition.y<=maxy
            loadX+=velD[e][1]*sc;
            loadY+=velD[e][3]*sc;
            loadZ+=velD[e][2]*sc;
            # println("here")
        end
    end
    return Vector3(loadX,loadY,loadZ)
end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    return currentRestLength
end
