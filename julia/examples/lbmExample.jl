# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020


##################### lbm simulation ########################
radius = 5.

nx = 32;
ny = 16;

center = [nx/2, ny/2]
BOUND = [norm([i, j]-center) < radius for i=1:nx, j=1:ny]
ON2D = findall(BOUND);


maxIter=100
dynamic=false
verbose=false
saveEvery=2
problem=setdynamicboundary2D


UXD,UYD,presD,velD,animD,animFD=lbm2D1(nx,ny,maxIter,problem,radius,dynamic,verbose,saveEvery);

display(gif(animD, fps = 10))
display(gif(animFD, fps = 10))

######################### 1. Voxel Design ###########################
setup = Dict()


################################################
### 1.b Draw Lattice


voxelSize=0.001

latticeSizeX=nx
latticeSizeY=1
latticeSizeZ=ny
gridSize=nx


setup["gridSize"]=gridSize

setup["rhino"]=false
setup["useVoxelList"]=true


setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=1e4; #scale for visualization
setup["hierarchical"]=true; #hierachical simualtion


#simulation params
setup["numTimeSteps"]=1000; #num of timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=false; #if there is change in temperature
setup["globalDamping"]=0.15; # (usually from 0.1 to 0.4)


#visualization params
setup["maxNumFiles"]=20; #num of saved timesteps for visualization


######################### 2.b. Materials #########################

#default neural material
material1= Dict()
material1["area"]=voxelSize*voxelSize
material1["density"]=1e3
material1["stiffness"]=1e6
material1["poissonRatio"]=0.0
material1["cTE"]=0.0 #coefficient of thermal expansion

#large bounding box for default material
boundingBoxMaterial1=Dict()
boundingBoxMaterial1["min"]=Dict()
boundingBoxMaterial1["max"]=Dict()

boundingBoxMaterial1["min"]["x"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["y"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["z"]=-voxelSize*gridSize;

boundingBoxMaterial1["max"]["x"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["y"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["z"]= voxelSize*gridSize;

setup["materials"]=[
    [boundingBoxMaterial1,material1]
];

#


#//allowed x, yand z are from -gridSize to +gridSize (floor is at 0)
setup["voxelList"]=[]
for o in ON2D
    append!(setup["voxelList"],[ [[o[1],0,o[2]],material1] ])
end



######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]
# dof=[false,true,false,true,true,true]


boundingBoxSupport=Dict()
boundingBoxSupport["min"]=Dict()
boundingBoxSupport["max"]=Dict()

boundingBoxSupport["min"]["x"]=center[1]*voxelSize;
boundingBoxSupport["min"]["y"]=-voxelSize;
boundingBoxSupport["min"]["z"]=(center[2]-3)*voxelSize;

boundingBoxSupport["max"]["x"]=center[1]*voxelSize+voxelSize;
boundingBoxSupport["max"]["y"]=voxelSize;
boundingBoxSupport["max"]["z"]=(center[2]-3)*voxelSize+voxelSize;

setup["supports"]=[
        [boundingBoxSupport,dof]
    ];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads

#get loads from LBM calculation
#TODO make sure later conversion is correct (friction..)
edgeList=findall(x->x!=0,presD)

setup["loads"]=[];

for e in edgeList 
    load2=Dict()
    load2["x"]=velD[e][1]*2.0
    load2["y"]=0.0
    load2["z"]=velD[e][2]*2.0

    boundingBoxLoad2=Dict()
    boundingBoxLoad2["min"]=Dict()
    boundingBoxLoad2["max"]=Dict()

    boundingBoxLoad2["min"]["x"]=e[1]*voxelSize
    boundingBoxLoad2["min"]["y"]=-voxelSize;
    boundingBoxLoad2["min"]["z"]=e[2]*voxelSize;

    boundingBoxLoad2["max"]["x"]=e[1]*voxelSize+voxelSize;
    boundingBoxLoad2["max"]["y"]=voxelSize;
    boundingBoxLoad2["max"]["z"]=e[2]*voxelSize+voxelSize;

    append!(setup["loads"],[[boundingBoxLoad2,load2]])
end


setup["fixedDisplacements"]=[];


#### 2.d.2 Dynamic Loads
function floorEnabled()
    return false
end

function gravityEnabled()
    return false
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    return N_force
end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    return currentRestLength
end
