# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

######################### 1. Voxel Design ###########################
setup = Dict()

### 1.b Draw Lattice

rhino=true

voxelSize=62.825
latticeSizeX=15
latticeSizeY=3
latticeSizeZ=4

gridSize=20

setup["gridSize"]=gridSize

setup["rhino"]=rhino
setup["rhinoFileName"]="../julia/examples/CAD_Rhino/wing.3dm";
setup["useMaterialList"]=false;
setup["layerIndex"]="1";
setup["useVoxelList"]=false


setup["latticeSizeX"]=1
setup["latticeSizeY"]=1
setup["latticeSizeZ"]=1

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=0.1; #scale for visualization
setup["hierarchical"]=false; #hierachical simualtion


#simulation params
setup["numTimeSteps"]=100000; #num of timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=false; #if there is change in temperature
setup["globalDamping"]=0.2; # (usually from 0.1 to 0.4)


#visualization params
setup["maxNumFiles"]=10; #num of saved timesteps for visualization, make sure it's smaller than numTimeSteps


######################### 2.b. Materials #########################

#default material
material1= Dict()
material1["area"]=2.38*2.38
material1["density"]=0.028
material1["stiffness"]=200.0
material1["poissonRatio"]=0.35
material1["cTE"]=0.0 #coefficient of thermal expansion

#large bounding box for default material
boundingBoxMaterial1=Dict()
boundingBoxMaterial1["min"]=Dict()
boundingBoxMaterial1["max"]=Dict()

boundingBoxMaterial1["min"]["x"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["y"]=-voxelSize*gridSize;
boundingBoxMaterial1["min"]["z"]=-voxelSize*gridSize;

boundingBoxMaterial1["max"]["x"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["y"]= voxelSize*gridSize;
boundingBoxMaterial1["max"]["z"]= voxelSize*gridSize;

#material2
material2= Dict()
material2["area"]=2.38*2.38
material2["density"]=0.028
material2["stiffness"]=200.0
material2["poissonRatio"]=0.35
material2["cTE"]=0.0 #coefficient of thermal expansion

#large bounding box for default material
boundingBoxMaterial2=Dict()
boundingBoxMaterial2["min"]=Dict()
boundingBoxMaterial2["max"]=Dict()

boundingBoxMaterial2["min"]["x"]=6*voxelSize-voxelSize/4.0;
boundingBoxMaterial2["min"]["y"]=-voxelSize/2.0;
boundingBoxMaterial2["min"]["z"]=-voxelSize/2.0;

boundingBoxMaterial2["max"]["x"]=6*voxelSize+voxelSize/4.0;
boundingBoxMaterial2["max"]["y"]=voxelSize*(latticeSizeY)+voxelSize/4.0;
boundingBoxMaterial2["max"]["z"]=voxelSize*(latticeSizeZ)+voxelSize/4.0;


setup["materials"]=[
    [boundingBoxMaterial1,material1],
    [boundingBoxMaterial2,material2]
    ];

######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]

boundingBoxSupport1=Dict()
boundingBoxSupport1["min"]=Dict()
boundingBoxSupport1["max"]=Dict()


boundingBoxSupport1["min"]["x"]=-voxelSize/2.0;
boundingBoxSupport1["min"]["y"]=-voxelSize/2.0;
boundingBoxSupport1["min"]["z"]=-voxelSize/2.0;

boundingBoxSupport1["max"]["x"]= voxelSize/4.0;
boundingBoxSupport1["max"]["y"]= voxelSize*(latticeSizeY)+voxelSize/4.0;
boundingBoxSupport1["max"]["z"]= voxelSize*(latticeSizeZ)+voxelSize/4.0;

setup["supports"]=[
        [boundingBoxSupport1,dof]
    ];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads
load1=Dict()
load1["x"]=0.0
load1["y"]=-1.0 # mass of the whole structure/number nodes
load1["z"]=0.0

boundingBoxLoad1=Dict()
boundingBoxLoad1["min"]=Dict()
boundingBoxLoad1["max"]=Dict()

boundingBoxLoad1["min"]["x"]=-voxelSize*gridSize;
boundingBoxLoad1["min"]["y"]=-voxelSize*gridSize;
boundingBoxLoad1["min"]["z"]=-voxelSize*gridSize;

boundingBoxLoad1["max"]["x"]=voxelSize*gridSize;
boundingBoxLoad1["max"]["y"]=voxelSize*gridSize;
boundingBoxLoad1["max"]["z"]=voxelSize*gridSize;


load2=Dict()
load2["x"]=-1.0
load2["y"]=0
load2["z"]=0

boundingBoxLoad2=Dict()
boundingBoxLoad2["min"]=Dict()
boundingBoxLoad2["max"]=Dict()

boundingBoxLoad2["min"]["x"]=6*voxelSize-voxelSize/4.0;
boundingBoxLoad2["min"]["y"]=-voxelSize/2.0;
boundingBoxLoad2["min"]["z"]=-voxelSize/2.0;

boundingBoxLoad2["max"]["x"]=6*voxelSize+voxelSize/4.0;
boundingBoxLoad2["max"]["y"]=voxelSize/4.0;
boundingBoxLoad2["max"]["z"]=voxelSize*(latticeSizeZ)+voxelSize/4.0;




setup["loads"]=[
        [boundingBoxLoad1,load1],
        [boundingBoxLoad2,load2],

    ];

#### 2.d.2 Fixed Displacements
dis1=Dict()
dis1["x"]=0.0
dis1["y"]=-5e-3
dis1["z"]=0.0

boundingBoxDis1=Dict()
boundingBoxDis1["min"]=Dict()
boundingBoxDis1["max"]=Dict()

boundingBoxDis1["min"]["x"]=-voxelSize/4.0;
boundingBoxDis1["min"]["y"]=voxelSize*(latticeSizeY)-voxelSize/8;
boundingBoxDis1["min"]["z"]=-voxelSize/2.0;

boundingBoxDis1["max"]["x"]=voxelSize*(latticeSizeX)+voxelSize/4.0;
boundingBoxDis1["max"]["y"]=voxelSize*(latticeSizeY)+voxelSize/2.0;
boundingBoxDis1["max"]["z"]=voxelSize*(latticeSizeZ)+voxelSize/4.0;


setup["fixedDisplacements"]=[
        # [boundingBoxDis1,dis1]
    ];

#### 2.d.3 Dynamic Loads
function floorEnabled()
    return false
end

function gravityEnabled()
    return false
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    mass=-1
    if N_force.x !=0
        load=1*currentTimeStep/100000
        return Vector3(load,mass,0)
    else
        return N_force
    end
end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    # @cuprintln("currentRestLength $currentRestLength")
    return currentRestLength
end


###########################################################################