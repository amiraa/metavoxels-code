# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

######################### 1. Voxel Design ###########################
setup = Dict()

### 1.b Draw Lattice

rhino=false

voxelSize=0.001
latticeSizeX=2
latticeSizeY=8
latticeSizeZ=5

gridSize=10

setup["gridSize"]=gridSize

setup["rhino"]=false
setup["useVoxelList"]=true


setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=1e4; #scale for visualization
setup["hierarchical"]=true; #hierachical simualtion


#simulation params
setup["numTimeSteps"]=10000; #num of timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=true; #if there is change in temperature
setup["globalDamping"]=0.15; # (usually from 0.1 to 0.4)


#visualization params
setup["maxNumFiles"]=2; #num of saved timesteps for visualization


######################### 2.b. Materials #########################

density=50.0
stiffness=1e6

#default neural material
material0= Dict()
material0["area"]=voxelSize*voxelSize
material0["density"]=density
material0["stiffness"]=stiffness
material0["poissonRatio"]=0.0
material0["cTE"]=0.0 #coefficient of thermal expansion

#second material leg1
material11= Dict()
material11["area"]=voxelSize*voxelSize
material11["density"]=density
material11["stiffness"]=stiffness
material11["poissonRatio"]=0.1
material11["cTE"]=-0.1 #coefficient of thermal expansion


#second material leg1
material12= Dict()
material12["area"]=voxelSize*voxelSize
material12["density"]=density
material12["stiffness"]=stiffness
material12["poissonRatio"]=0.1
material12["cTE"]=0.1 #coefficient of thermal expansion


#second material leg2
material21= Dict()
material21["area"]=voxelSize*voxelSize
material21["density"]=density
material21["stiffness"]=stiffness
material21["poissonRatio"]=-0.1
material21["cTE"]=-0.1 #coefficient of thermal expansion


#second material leg2
material22= Dict()
material22["area"]=voxelSize*voxelSize
material22["density"]=density
material22["stiffness"]=stiffness
material22["poissonRatio"]=-0.1
material22["cTE"]=0.1 #coefficient of thermal expansion



setup["materials"]=[

];

# height=-1.5
height=0


#//allowed x, yand z are from -gridSize to +gridSize (floor is at 0)
setup["voxelList"]=[

    [[0,7+height,0],material0],#top
    [[1,7+height,0],material0],#top
    [[0,7+height,1],material0],#top
    [[1,7+height,1],material0],#top
    [[0,7+height,2],material0],#top
    [[1,7+height,2],material0],#top
    [[0,7+height,3],material0],#top
    [[1,7+height,3],material0],#top
    [[0,7+height,4],material0],#top
    [[1,7+height,4],material0],#top

    [[0,6+height,0],material0],#top
    [[1,6+height,0],material0],#top
    [[0,6+height,1],material0],#top
    [[1,6+height,1],material0],#top
    [[0,6+height,2],material0],#top
    [[1,6+height,2],material0],#top
    [[0,6+height,3],material0],#top
    [[1,6+height,3],material0],#top
    [[0,6+height,4],material0],#top
    [[1,6+height,4],material0],#top


    # [[0-2,7+height0],material0],#top
    # [[1-2,7+height0],material0],#top
    # [[0-2,7+height1],material0],#top
    # [[1-2,7+height1],material0],#top
    # [[0-2,7+height2],material0],#top
    # [[1-2,7+height2],material0],#top
    # [[0-2,7+height3],material0],#top
    # [[1-2,7+height3],material0],#top
    # [[0-2,7+height4],material0],#top
    # [[1-2,7+height4],material0],#top
    # [[0-2,6+height0],material0],#top
    # [[1-2,6+height0],material0],#top
    # [[0-2,6+height1],material0],#top
    # [[1-2,6+height1],material0],#top
    # [[0-2,6+height2],material0],#top
    # [[1-2,6+height2],material0],#top
    # [[0-2,6+height3],material0],#top
    # [[1-2,6+height3],material0],#top
    # [[0-2,6+height4],material0],#top
    # [[1-2,6+height4],material0],#top

    # # [[1-4,7+height,0],material0],#top
    # # [[1-4,7+height,1],material0],#top
    # [[1-4,7+height,2],material0],#top
    # # [[1-4,7+height,3],material0],#top
    # # [[1-4,7+height,4],material0],#top
    # # [[1-4,6+height,0],material0],#top
    # # [[1-4,6+height,1],material0],#top
    # [[1-4,6+height,2],material0],#top
    # # [[1-4,6+height,3],material0],#top
    # # [[1-4,6+height,4],material0],#top


    ########################upper##################################


    [[0,5+height,0],material12],#leg1 front
    [[0,5+height,1],material12],#leg1 front
    [[0,4+height,0],material12],#leg1 front
    [[0,4+height,1],material12],#leg1 front
    [[0,3+height,0],material12],#leg1 front
    [[0,3+height,1],material12],#leg1 front

    [[1,5+height,0],material11],#leg1 back
    [[1,5+height,1],material11],#leg1 back
    [[1,4+height,0],material11],#leg1 back
    [[1,4+height,1],material11],#leg1 back
    [[1,3+height,0],material11],#leg1 back
    [[1,3+height,1],material11],#leg1 back


    [[0,5+height,3],material22],#leg2 front
    [[0,5+height,4],material22],#leg2 front
    [[0,4+height,3],material22],#leg2 front
    [[0,4+height,4],material22],#leg2 front
    [[0,3+height,3],material22],#leg2 front
    [[0,3+height,4],material22],#leg2 front

    [[1,5+height,4],material21],#leg2 back
    [[1,5+height,3],material21],#leg2 back
    [[1,4+height,3],material21],#leg2 back
    [[1,4+height,4],material21],#leg2 back
    [[1,3+height,3],material21],#leg2 back
    [[1,3+height,4],material21],#leg2 back


    ########################lower##################################


    [[0,2+height,0],material0],#leg1 front
    [[0,2+height,1],material0],#leg1 front
    [[0,1+height,0],material0],#leg1 front
    [[0,1+height,1],material0],#leg1 front
    [[0,0+height,0],material0],#leg1 front
    [[0,0+height,1],material0],#leg1 front

    [[1,2+height,0],material12],#leg1 back 2
    [[1,2+height,1],material12],#leg1 back 2
    [[1,1+height,0],material12],#leg1 back 2
    [[1,1+height,1],material12],#leg1 back 2
    [[1,0+height,0],material12],#leg1 back 2
    [[1,0+height,1],material12],#leg1 back 2


    [[0,2+height,3],material0],#leg2 front 2
    [[0,2+height,4],material0],#leg2 front 2
    [[0,1+height,3],material0],#leg2 front 2
    [[0,1+height,4],material0],#leg2 front 2
    [[0,0+height,3],material0],#leg2 front 2
    [[0,0+height,4],material0],#leg2 front 2

    [[1,2+height,3],material22],#leg2 back
    [[1,2+height,4],material22],#leg2 back
    [[1,0+height,3],material22],#leg2 back
    [[1,0+height,4],material22],#leg2 back
    [[1,1+height,3],material22],#leg2 back
    [[1,1+height,4],material22],#leg2 back

        
];
######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]
dof=[false,true,false,true,true,true]


boundingBoxSupport1=Dict()
boundingBoxSupport1["min"]=Dict()
boundingBoxSupport1["max"]=Dict()


boundingBoxSupport1["min"]["x"]=0;
boundingBoxSupport1["min"]["y"]=voxelSize*(latticeSizeY-2+height);
boundingBoxSupport1["min"]["z"]=0;

boundingBoxSupport1["max"]["x"]=voxelSize*(latticeSizeX);
boundingBoxSupport1["max"]["y"]=voxelSize*(latticeSizeY+height);
boundingBoxSupport1["max"]["z"]=voxelSize*(latticeSizeZ);

setup["supports"]=[
        # [boundingBoxSupport1,dof]
    ];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads
load1=Dict()
load1["x"]=0.0
load1["y"]=0.0
load1["z"]=0.0

boundingBoxLoad1=Dict()
boundingBoxLoad1["min"]=Dict()
boundingBoxLoad1["max"]=Dict()

boundingBoxLoad1["min"]["x"]=0
boundingBoxLoad1["min"]["y"]=voxelSize*(latticeSizeY-1+height);
boundingBoxLoad1["min"]["z"]=0;

boundingBoxLoad1["max"]["x"]=voxelSize*(latticeSizeX);
boundingBoxLoad1["max"]["y"]=voxelSize*(latticeSizeY+height);
boundingBoxLoad1["max"]["z"]=voxelSize*(latticeSizeZ);


setup["loads"]=[
        # [boundingBoxLoad1,load1]
    ];

setup["fixedDisplacements"]=[];


#### 2.d.2 Dynamic Loads
function floorEnabled()
    return true
end

function gravityEnabled()
    return true
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)

    return N_force
end

# if no temperature:
# function updateTemperature(currentRestLength,currentTimeStep,mat)
#     return currentRestLength
# end

# function updateTemperature(currentRestLength,currentTimeStep,mat)
#     if currentTimeStep<1000
#         temp=-5.0*currentTimeStep/1000
#         currentRestLength=0.5*mat.L*(2.0+temp*mat.cTE)
#     elseif currentTimeStep==2500
#         temp=0
#         currentRestLength=0.5*mat.L*(2.0+temp*mat.cTE)
#     end
#     return currentRestLength
# end


function floorPenetration(x,y,nomSize)
    floor=0.001*1.2
    # floor=0.0
    p=0.0
    d=10.0
    if(y<floor)
    # if(y<floor&& (x<5.0*d || x>=14.0*d))
        p=floor-y
    end
    return p
end
#Returns the interference (in meters) between the collision envelope of this voxel and the floor at Z=0. Positive numbers correspond to interference. If the voxel is not touching the floor 0 is returned.


function penetrationStiffness(E,nomSize)
    return (1.0*E*nomSize)
end 


# function updateTemperature(currentRestLength,currentTimeStep,mat)
#     cte=mat.cTE
#     factor=0.4;
#     interval=2000.0

#     currentTimeStep1=currentTimeStep
#     if (mat.nu<0.0)
#         currentTimeStep1+=interval
#     end
#     t=floor(currentTimeStep1/interval)
#     t1=currentTimeStep1-t*interval

#     if t%2.0!=0.0
#         if (cte<0.0)
#             currentRestLength=mat.L-factor*mat.L*(1.0-(t1)/interval);
#         elseif (cte>0.0)
#             currentRestLength=mat.L-factor*mat.L*((t1)/interval);
#         else
#             currentRestLength=mat.L;
#         end
#     else
#         if (cte>0.0)
#             currentRestLength=mat.L-factor*mat.L*(1.0-(t1)/interval);
#         elseif (cte<0.0)
#             currentRestLength=mat.L-factor*mat.L*((t1)/interval);
#         else
#             currentRestLength=mat.L;
#         end
#     end
#     return currentRestLength
# end
strX=0:0.5/10000:0.5
strY=0:0.5/10000:0.5
strZ=0:0.5/10000:0.5
strK=0:0.5/10000:0.5
function updateTemperature(currentRestLength,currentTimeStep,mat)
    cte=mat.cTE
    pois=mat.nu
    factor=0.4;
    val1=sin(strX[currentTimeStep+1]*10.0)
    val2=sin(strZ[currentTimeStep+1]*10.0)
    val3=sin(strY[currentTimeStep+1]*10.0)
    val4=sin(strK[currentTimeStep+1]*10.0)
    

    if ((cte<0.0&&pois<0.0)  )
        currentRestLength=mat.L-factor*mat.L*(val1);
    elseif ((cte>0.0&&pois>0.0) )
        currentRestLength=mat.L-factor*mat.L*(val2);
    elseif ((cte<0.0&&pois>0.0)  )
        currentRestLength=mat.L-factor*mat.L*(val3);
    elseif ((cte>0.0&&pois<0.0) )
        currentRestLength=mat.L-factor*mat.L*(val4);
    else
        currentRestLength=mat.L;
    end
    # println("here")
   
    return currentRestLength
end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    cte=mat.cTE
    pois=mat.nu
    factor=0.4;

    val1=strY[currentTimeStep+1]
    val2=strZ[currentTimeStep+1]

    

    if ((cte<0.0&&pois<0.0)  )
        currentRestLength=mat.L-factor*mat.L*(1.0-val1);
    elseif ((cte>0.0&&pois>0.0) )
        currentRestLength=mat.L-factor*mat.L*(1.0-val2);
    elseif ((cte<0.0&&pois>0.0)  )
        currentRestLength=mat.L-factor*mat.L*(1.0-val1);
    elseif ((cte>0.0&&pois<0.0) )
        currentRestLength=mat.L-factor*mat.L*(1.0-val2);
    else
        currentRestLength=mat.L;
    end
    # println("here")
   
    return currentRestLength
end


