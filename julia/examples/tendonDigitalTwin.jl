# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021

######################### 1. Voxel Design ###########################
setup = Dict()

### 1.b Draw Lattice

rhino=false

voxelSize=0.001
latticeSizeX=2
latticeSizeY=9
latticeSizeZ=2

gridSize=10

setup["gridSize"]=gridSize

setup["rhino"]=false
setup["useVoxelList"]=true


setup["latticeSizeX"]=latticeSizeX
setup["latticeSizeY"]=latticeSizeY
setup["latticeSizeZ"]=latticeSizeZ

######################### 2. Boundary Conditions #########################

######################### 2.a. Global Settings #########################

#scaling params
setup["voxelSize"]=voxelSize; #voxel size
setup["scale"]=1e4; #scale for visualization
setup["hierarchical"]=true; #hierachical simualtion


#simulation params
setup["numTimeSteps"]=100; #num of timesteps for simulation

setup["poisson"]=false; # account for poisson ration (only for hierarchical)
setup["linear"]=true; # linear vs non-linear
setup["thermal"]=true; #if there is change in temperature
setup["globalDamping"]=0.15; # (usually from 0.1 to 0.4)
setup["globalDamping"]=0.01; # (usually from 0.1 to 0.4)


#visualization params
setup["maxNumFiles"]=3; #num of saved timesteps for visualization


######################### 2.b. Materials #########################

density=1e3
density=50.0
stiffness=1e6



#default neural material
material0= Dict()
material0["area"]=voxelSize*voxelSize
material0["density"]=density
material0["stiffness"]=stiffness
material0["poissonRatio"]=0.0
material0["cTE"]=0.0 #coefficient of thermal expansion

#second material leg1
material11= Dict()
material11["area"]=voxelSize*voxelSize
material11["density"]=density
material11["stiffness"]=stiffness
material11["poissonRatio"]=0.1
material11["cTE"]=0.1 #coefficient of thermal expansion

#second material leg1
material12= Dict()
material12["area"]=voxelSize*voxelSize
material12["density"]=density
material12["stiffness"]=stiffness
material12["poissonRatio"]=0.1
material12["cTE"]=-0.1 #coefficient of thermal expansion


#second material leg1
material21= Dict()
material21["area"]=voxelSize*voxelSize
material21["density"]=density
material21["stiffness"]=stiffness
material21["poissonRatio"]=0.1
material21["cTE"]=-0.2 #coefficient of thermal expansion

#second material leg1
material22= Dict()
material22["area"]=voxelSize*voxelSize
material22["density"]=density
material22["stiffness"]=stiffness
material22["poissonRatio"]=0.1
material22["cTE"]=0.2 #coefficient of thermal expansion


setup["materials"]=[
];

# height=-1.5
height=0


#//allowed x, yand z are from -gridSize to +gridSize (floor is at 0)
setup["voxelList"]=[

    [[0,0+height,0],material0],#top
    [[1,0+height,0],material0],#top
    [[0,0+height,1],material0],#top
    [[1,0+height,1],material0],#top

    [[0,1+height,0],material0],#top
    [[1,1+height,0],material0],#top
    [[0,1+height,1],material0],#top
    [[1,1+height,1],material0],#top
    ########################lower##################################

    [[0,2+height,0],material11],#leg1 front
    [[0,2+height,1],material11],#leg1 front

    [[0,3+height,0],material11],#leg1 front
    [[0,3+height,1],material11],#leg1 front

    [[1,2+height,0],material12],#leg1 back
    [[1,2+height,1],material12],#leg1 back

    [[1,3+height,0],material12],#leg1 back 
    [[1,3+height,1],material12],#leg1 back 

    ########################middle##################################
    [[0,4+height,0],material0],#leg1 front
    [[0,4+height,1],material0],#leg1 front

    [[0,5+height,0],material0],#leg1 front
    [[0,5+height,1],material0],#leg1 front

    [[1,4+height,0],material0],#leg1 back
    [[1,4+height,1],material0],#leg1 back

    [[1,5+height,0],material0],#leg1 back
    [[1,5+height,1],material0],#leg1 back

    ########################upper##################################

    [[0,6+height,0],material21],#leg1 front
    [[0,6+height,1],material21],#leg1 front
    
    [[0,7+height,0],material21],#leg1 front
    [[0,7+height,1],material21],#leg1 front

    [[0,8+height,0],material21],#leg1 front
    [[0,8+height,1],material21],#leg1 front

    [[0,9+height,0],material21],#leg1 front
    [[0,9+height,1],material21],#leg1 front

    [[1,6+height,0],material22],#leg1 back
    [[1,6+height,1],material22],#leg1 back

    [[1,7+height,0],material22],#leg1 back
    [[1,7+height,1],material22],#leg1 back

    [[1,8+height,0],material22],#leg1 back
    [[1,8+height,1],material22],#leg1 back

    [[1,9+height,0],material22],#leg1 back
    [[1,9+height,1],material22],#leg1 back

        
];
######################### 2.c. Supports #########################

#x,y,z,rx,ry,rz (default is pinned joing i.e [false,false,false,true,true,true])
dof=[true,true,true,true,true,true]
# dof=[false,true,false,true,true,true]


boundingBoxSupport1=Dict()
boundingBoxSupport1["min"]=Dict()
boundingBoxSupport1["max"]=Dict()


boundingBoxSupport1["min"]["x"]=0;
boundingBoxSupport1["min"]["y"]=voxelSize*(0+height);
boundingBoxSupport1["min"]["z"]=0;

boundingBoxSupport1["max"]["x"]=voxelSize*(latticeSizeX);
boundingBoxSupport1["max"]["y"]=voxelSize*(2+height);
boundingBoxSupport1["max"]["z"]=voxelSize*(latticeSizeZ);

setup["supports"]=[
        [boundingBoxSupport1,dof]
    ];

######################### 2.d. Loads #########################
#### 2.d.1 Static Loads
load1=Dict()
load1["x"]=0.0
load1["y"]=0.0
load1["z"]=0.0

boundingBoxLoad1=Dict()
boundingBoxLoad1["min"]=Dict()
boundingBoxLoad1["max"]=Dict()

boundingBoxLoad1["min"]["x"]=0
boundingBoxLoad1["min"]["y"]=voxelSize*(latticeSizeY-1+height);
boundingBoxLoad1["min"]["z"]=0;

boundingBoxLoad1["max"]["x"]=voxelSize*(latticeSizeX);
boundingBoxLoad1["max"]["y"]=voxelSize*(latticeSizeY+height);
boundingBoxLoad1["max"]["z"]=voxelSize*(latticeSizeZ);


setup["loads"]=[
        # [boundingBoxLoad1,load1]
    ];

setup["fixedDisplacements"]=[];


#### 2.d.2 Dynamic Loads
function floorEnabled()
    return false
end

function gravityEnabled()
    return false
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    return N_force
end

# if no temperature:
# function updateTemperature(currentRestLength,currentTimeStep,mat)
#     return currentRestLength
# end

# function updateTemperature(currentRestLength,currentTimeStep,mat)
#     if currentTimeStep<1000
#         temp=-5.0*currentTimeStep/1000
#         currentRestLength=0.5*mat.L*(2.0+temp*mat.cTE)
#     elseif currentTimeStep==2500
#         temp=0
#         currentRestLength=0.5*mat.L*(2.0+temp*mat.cTE)
#     end
#     return currentRestLength
# end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    maxcte=0.1
    maxtemp=5
    tempFactor1=factor1/6000/2.0
    tempFactor2=factor2/6000
    temp=0.0
    # print(factor1)
    if abs(mat.cTE)==0.1 #lower
        if mat.cTE>0
            temp=tempFactor1*maxtemp
        else
            temp=-tempFactor1*maxtemp
        end
    elseif abs(mat.cTE)==0.2
        if mat.cTE>0
            temp=tempFactor2*maxtemp
        else
            temp=-tempFactor2*maxtemp
        end
    else
        return currentRestLength
    end
    currentRestLength=0.5*mat.L*(2.0+temp*maxcte)
    return currentRestLength
end




