# MetaVoxels Code

Code for the design, simulation and optimization of MetaVoxels.

## Documentation

- Here: https://gitlab.cba.mit.edu/amiraa/metavoxels

## Interactive Demos 

- [Tendons Modeling](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexTendon.html)
- [Precomputed 5*5 cuboct Voxel](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexScaling.html)
- [Precomputed 4*1 chiral Voxel](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexChiral.html)
- [Boundary Conditions](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexBoundaryConditions.html)
- [Hierarchal multi-material 3*3 Base Voxel (old cpu version (to fix))](https://amiraa.pages.cba.mit.edu/metavoxels-code/demos/indexHierarchical.html)