// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020


var fileName=process.argv.slice(2);

var http = require('http');

var finalhandler = require('finalhandler');
var serveStatic = require('serve-static');

var serve = serveStatic(`${__dirname}/../`);

var server = http.createServer(function(req, res) {
  var done = finalhandler(req, res);
  serve(req, res, done);
});

var port=8000;
server.listen(port);

console.log(`Server listening on port ${port}`);
console.log(`Open http://localhost:${port}/demos/indexTutorialGraph.html in your browser`);
// console.log(setup)