// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020


// "use strict";
var fileName=process.argv.slice(2);
var node=true;
var fs = require('fs');
var THREE=require('three');
var tf=require('@tensorflow/tfjs');
const {performance, PerformanceObserver} = require('perf_hooks');
const editJsonFile = require("edit-json-file");
eval(fs.readFileSync("./lib/rhino3dm.js")+''); 
eval(fs.readFileSync("./lib/js-colormaps.js")+'');


////////////////////load 
let inputFile = editJsonFile(`${__dirname}/json/emptySetup.json`);
// let outputFile = editJsonFile(`${__dirname}/json/setupTestUni`+latticeSize +`.json`);
let outputFile = editJsonFile(`${__dirname}/json/`+fileName+`.json`);
var setup=inputFile.toObject();

function saveJSON(){
  // console.log(setup);
  outputFile.set("setup", setup);
  outputFile.save();
}

// //////////////////////////////////
eval(fs.readFileSync("./visualization/utils.js")+''); 

// eval(fs.readFileSync("./fea/beamFea.js")+''); 
eval(fs.readFileSync('./visualization/geometry.js')+'');
eval(fs.readFileSync('./visualization/draw/'+fileName+'.js')+'');



//////////////////////spawn julia////////////////////////

// const { spawn } = require('child_process');
// console.log("hi")
// // const ls = spawn('ls', ['', '../']);
// const ls = spawn('julia', ['julia/p5.jl', __dirname]);
// // const npm = which.sync('npm');
// // console.log(npm)
// // const child = spawn('dir');


// ls.stdout.on('data', (data) => {
//   console.log(`stdout: ${data}`);
// });

// ls.stderr.on('data', (data) => {
//   console.error(`stderr: ${data}`);
// });

// ls.on('close', (code) => {
//   console.log(`child process exited with code ${code}`);
// });


//////////////////////////////serve /////////////////

// var http = require('http');

// var finalhandler = require('finalhandler');
// var serveStatic = require('serve-static');

// var serve = serveStatic("./");

// var server = http.createServer(function(req, res) {
//   var done = finalhandler(req, res);
//   serve(req, res, done);
// });

// var port=8080;
// server.listen(8080);

// console.log(`Server listening on port ${port}`);
// console.log(setup)



