import logging
import math
import gym
from gym import spaces
from gym.utils import seeding
import numpy as np
# import sys
# import cv2
# import math
import math as Math

#julia
import julia
from julia.api import Julia
jl = Julia(compiled_modules=False)
from julia import Main
Main.include("./julia/initRover.jl")


class RoverEnv(gym.Env):

  def __init__(self):
    """
    Data set is a tuple of 
    [0] input data: [nSamples x nInputs]
    [1] labels:     [nSamples x 1]

    Example data sets are given at the end of this file
    """

    # self.t = 0          # Current batch number
    # self.t_limit = 0    # Number of batches if you want to use them (we didn't)
    # self.batch   = 1000 # Number of images per batch
    self.seed()
    self.viewer = None

    # self.trainSet = trainSet
    # self.target   = target

    # nInputs = np.shape(trainSet)[1]
    # high = np.array([1.0]*nInputs)
    self.action_space = spaces.Box(np.array(0,dtype=np.float32), \
                                   np.array(1,dtype=np.float32))
    self.observation_space = spaces.Box(np.array(0,dtype=np.float32), \
                                   np.array(1,dtype=np.float32))

    self.state = None
    # self.trainOrder = None
    # self.currIndx = None

    # pixels = pixelGrid(resolution).T
    # self.X=pixels[0]
    # self.Y=pixels[1]

    self.maxTimestep=10000

    self.time= np.linspace(0,self.maxTimestep-1, self.maxTimestep)

    Main.strY= np.linspace(0.0,1.0, self.maxTimestep)
    Main.strZ= np.linspace(0.0,1.0, self.maxTimestep)

    # Main.strY=turnArrayToString(y,self.maxTimestep)
    # Main.strZ=turnArrayToString(z,self.maxTimestep)

    # Main.eval("function externalForce(currentTimeStep,N_position,N_force)\n strY="+strY+";\n strZ="+strZ+";\n if (convert(Float64,N_force.z)>0.0) \n z=strZ[currentTimeStep+1];\n y=strY[currentTimeStep+1];\n if (convert(Float64,N_force.z)>1.5) \n z=-z;\n y=-y;\nend\nreturn Vector3(0.0,y,z);\n else\n return Vector3(0.0,0.0,0.0);\n end\n end")

    # self.target=circle(self.resolution,False)
    # self.target=Main.circle(self.resolution, 1,False)
    # matplotlib.image.imsave("./img/target.png", self.target.reshape(self.resolution,self.resolution))
    self.current=0
    self.bestScore=-100000

  def seed(self, seed=None):
    ''' Randomly select from training set'''
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def reset(self):
    ''' Initialize State'''    
    # print('Lucky number', np.random.randint(10)) # same randomness?
    # self.trainOrder = np.random.permutation(len(self.target))
    # self.t = 0 # timestep
    # self.currIndx = self.trainOrder[self.t:self.t+self.batch]
    # self.state = self.trainSet[self.currIndx,:]

    self.state= np.array([np.linspace(0,self.maxTimestep-1, self.maxTimestep)]).T-self.maxTimestep/2.0
    # self.current=0

    return self.state
  
  def step(self, action):
    ''' 
    Judge Classification, increment to next batch
    action - [batch x output] - softmax output
    '''
    # y = self.target[self.currIndx]
    # m = y.shape[0]

    # log_likelihood = -np.log(action[range(m),y])
    # print(action.shape)
    # print(self.target.shape)
    # print(sum(action))
    

    aaa=np.reshape(action,(self.maxTimestep,4))
    x= aaa[:,0]
    y= aaa[:,1]
    z= aaa[:,2]
    k= aaa[:,3]
    # print(sum(y))
    # print(sum(z))
    if(min(y)-max(y)!=0):
      y=maprange((min(y),max(y)),(-1.0,1.0), y)
    if(min(z)-max(z)!=0):   
      z=maprange((min(z),max(z)),(-1.0,1.0), z)
    if(min(x)-max(x)!=0):   
      x=maprange((min(x),max(x)),(-1.0,1.0), x)
    if(min(k)-max(k)!=0):   
      k=maprange((min(k),max(k)),(-1.0,1.0), k)


    Main.strX=x
    Main.strY=y
    Main.strZ=z
    Main.strK=k
    # Main.strY=turnArrayToString(y,self.maxTimestep)
    # Main.strZ=turnArrayToString(z,self.maxTimestep)

    # Main.eval("function externalForce(currentTimeStep,N_position,N_force)\n strY="+strY+";\n strZ="+strZ+";\n if (convert(Float64,N_force.z)>0.0) \n z=strZ[currentTimeStep+1];\n y=strY[currentTimeStep+1];\n if (convert(Float64,N_force.z)>1.5) \n z=-z;\n y=-y;\nend\nreturn Vector3(0.0,y,z);\n else\n return Vector3(0.0,0.0,0.0);\n end\n end")

    Main.include("./julia/runRover.jl")

    # if (self.current>100):
    #  self.current=0

    loss = Main.target
    # print(loss)
    # result = np.all(action == action[0])
    # if(result):
    #   loss=1000000
    
    # print(sum(y))
    # print(sum(z))
    if loss is not None:
        reward = loss

        if reward>self.bestScore:
            print(loss)
            self.bestScore=reward
            np.savetxt("./results/rover/"+str(self.current)+"_"+str(int(reward))+".out", (Main.strX,Main.strY,Main.strZ,Main.strK))   # X is an array
            self.current+=1



    self.state= np.array([np.linspace(0,self.maxTimestep-1, self.maxTimestep)]).T-self.maxTimestep/2.0
    
    done = True

    obs = self.state
    return obs, reward, done, {}


# -- Data Sets ----------------------------------------------------------- -- #

def turnArrayToString(a,max):
    strA="["
    strA+=str(a[0])
    for i in range(1, int(max)):
        strA+=","
        strA+=str(a[i])
    strA+="]"
    return strA
  
def maprange(a, b, s):
    (a1, a2), (b1, b2) = a, b
    return  b1 + ((s - a1) * (b2 - b1) / (a2 - a1))
 
