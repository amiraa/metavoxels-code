
import logging
import math
import gym
from gym import spaces
from gym.utils import seeding
import numpy as np
# import sys
# import cv2
# import math
import math as Math
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt


class FrepEnv(gym.Env):

  def __init__(self):
    """
    Data set is a tuple of 
    [0] input data: [nSamples x nInputs]
    [1] labels:     [nSamples x 1]

    Example data sets are given at the end of this file
    """

    # self.t = 0          # Current batch number
    # self.t_limit = 0    # Number of batches if you want to use them (we didn't)
    # self.batch   = 1000 # Number of images per batch
    self.seed()
    self.viewer = None

    # self.trainSet = trainSet
    # self.target   = target

    # nInputs = np.shape(trainSet)[1]
    # high = np.array([1.0]*nInputs)
    self.action_space = spaces.Box(np.array(0,dtype=np.float32), \
                                   np.array(1,dtype=np.float32))
    self.observation_space = spaces.Box(np.array(0,dtype=np.float32), \
                                   np.array(1,dtype=np.float32))

    self.state = None
    # self.trainOrder = None
    # self.currIndx = None

    # pixels = pixelGrid(resolution).T
    # self.X=pixels[0]
    # self.Y=pixels[1]

    self.resolution=32
    self.target=circle(self.resolution,False)
    matplotlib.image.imsave("./img/target.png", self.target.reshape(self.resolution,self.resolution))
    self.current=0
    self.bestScore=-100000

  def seed(self, seed=None):
    ''' Randomly select from training set'''
    self.np_random, seed = seeding.np_random(seed)
    return [seed]
  
  def reset(self):
    ''' Initialize State'''    
    # print('Lucky number', np.random.randint(10)) # same randomness?
    # self.trainOrder = np.random.permutation(len(self.target))
    # self.t = 0 # timestep
    # self.currIndx = self.trainOrder[self.t:self.t+self.batch]
    # self.state = self.trainSet[self.currIndx,:]

    self.state= pixelGrid(self.resolution)
    # self.current=0

    return self.state
  
  def step(self, action):
    ''' 
    Judge Classification, increment to next batch
    action - [batch x output] - softmax output
    '''
    # y = self.target[self.currIndx]
    # m = y.shape[0]

    # log_likelihood = -np.log(action[range(m),y])
    # print(action.shape)
    # print(self.target.shape)
    # print(sum(action))

    action=np.where( action<0.5, 0.0,1.0)


    # if (self.current>100):
    #  self.current=0

    loss = lossFunction(action,self.target)
    result = np.all(action == action[0])
    if(result):
      loss=1000000
    reward = -loss

    if reward>self.bestScore:
      self.bestScore=reward
      matplotlib.image.imsave("./img/"+str(self.current)+".png", action.reshape(self.resolution,self.resolution))
      self.current+=1


    # if self.t_limit > 0: # We are doing batches
    #   reward *= (1/self.t_limit) # average
    #   self.t += 1
    #   done = False
    #   if self.t >= self.t_limit:
    #     done = True
    #   self.currIndx = self.trainOrder[(self.t*self.batch):\
    #                                   (self.t*self.batch + self.batch)]

    #   self.state = self.trainSet[self.currIndx,:]
    # else:

    self.state= pixelGrid(self.resolution)
    
    done = True

    obs = self.state
    return obs, reward, done, {}


# -- Data Sets ----------------------------------------------------------- -- #

def pixelGrid(resolution=64):
  x = np.linspace(-1,1, resolution)
  X,Y = np.meshgrid(x,x)
  return np.vstack([X.flatten(),Y.flatten()]).T

def circle (resolution,thresholded):
  pixels = pixelGrid(resolution).T
  X=pixels[0]
  Y=pixels[1]
  frep1= ((1)*(1)-((X-(0))*(X-(0))+(Y-(0))*(Y-(0))))
  if thresholded:
    frep1=np.where( frep1<0.5, 0.0,1.0)
  return frep1

def gear (resolution,thresholded,threshold,scale):
  pixels = pixelGrid(resolution).T
  frep1=np.zeros((1,pixels.shape[1]))
  for i in range(resolution*resolution):
    X=pixels[0,i]*scale
    Y=pixels[1,i]*scale
    # frep1[0,i]= max(min(min(3+1-Math.sqrt(X*X+Y*Y),(Math.pi+Math.atan2(Y,X))%1.0471975511965976-(Math.sqrt(Math.pow(max(2.8190778623577253,Math.sqrt(X*X+Y*Y))/2.8190778623577253,2)-1)-Math.acos(2.8190778623577253/max(2.8190778623577253,Math.sqrt(X*X+Y*Y))))),-(Math.sqrt(Math.pow(max(2.8190778623577253,Math.sqrt(X*X+Y*Y))/2.8190778623577253,2)-1)-Math.acos(2.8190778623577253/max(2.8190778623577253,Math.sqrt(X*X+Y*Y))))-(-0.5534075433329717+(Math.pi+Math.atan2(Y,X))%1.0471975511965976)),1.9-Math.sqrt(X*X+Y*Y))

    frep1[0,i]= max(min(min(2.5+1-Math.sqrt(X*X+Y*Y),(Math.pi+Math.atan2(Y,X))%1.2566370614359172-(Math.sqrt(Math.pow(max(2.3492315519647713,Math.sqrt(X*X+Y*Y))/2.3492315519647713,2)-1)-Math.acos(2.3492315519647713/max(2.3492315519647713,Math.sqrt(X*X+Y*Y))))),-(Math.sqrt(Math.pow( max(2.3492315519647713,Math.sqrt(X*X+Y*Y))/2.3492315519647713,2)-1)-Math.acos(2.3492315519647713/max(2.3492315519647713,Math.sqrt(X*X+Y*Y))))-(-0.6581272984526315+(Math.pi+Math.atan2(Y,X))%1.2566370614359172)),1.4-Math.sqrt(X*X+Y*Y))
    # frep1[0,i]=min(min(min(((1)*(1)-((((0)+(X-(0))/((0.5)+(1.5)*(Y-(-1))/(2)))-(0))*(((0)+(X-(0))/((0.5)+(1.5)*(Y-(-1))/(2)))-(0))+(Y-(0))*(Y-(0)))),-(((0.16666666666666666)*(0.16666666666666666)-((((0)+(X-(0))/((0.5)+(1.5)*(Y-(-1))/(2)))-(0.4))*(((0)+(X-(0))/((0.5)+(1.5)*(Y-(-1))/(2)))-(0.4))+(Y-(0.4))*(Y-(0.4)))))),-(((0.16666666666666666)*(0.16666666666666666)-(((0-(((0)+(X-(0))/((0.5)+(1.5)*(Y-(-1))/(2)))))-(0.4))*((0-(((0)+(X-(0))/((0.5)+(1.5)*(Y-(-1))/(2)))))-(0.4))+(Y-(0.4))*(Y-(0.4)))))),-(min(((0)-Y),(min(((1)*(1)-((((0)+(((0)+(X-(0))/((0.5)+(1.5)*(Y-(-1))/(2)))-(0))/(0.7))-(0))*(((0)+(((0)+(X-(0))/((0.5)+(1.5)*(Y-(-1))/(2)))-(0))/(0.7))-(0))+(((0)+(Y-(0))/(0.7))-(0))*(((0)+(Y-(0))/(0.7))-(0)))),-(((1)*(1)-((((0)+(((0)+(((0)+(X-(0))/((0.5)+(1.5)*(Y-(-1))/(2)))-(0))/(0.75))-(0))/(0.7))-(0))*(((0)+(((0)+(((0)+(X-(0))/((0.5)+(1.5)*(Y-(-1))/(2)))-(0))/(0.75))-(0))/(0.7))-(0))+(((0)+(((0)+(Y-(0))/(0.75))-(0))/(0.7))-(0))*(((0)+(((0)+(Y-(0))/(0.75))-(0))/(0.7))-(0))))))))))
  if thresholded:
    frep1=np.where( frep1<threshold, 0.0,1.0)
  return frep1

def lossFunction(im,frep1):
  return np.mean(np.absolute(im-frep1))
  

 
