# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021
####################################################

plotting=false
GPU=false
logging=false

include("../../julia/MetaVoxels.jl") 

include("../../julia/examples/walkingRobot.jl")
# include("../../julia/examples/rover.jl")

include("../../julia/MetaVoxels.jl") 

simName="tutorial"

path="../../json/$(simName).json"
savedDataFolderPath="../../json/$(simName)/" # make sure this folder exists, this is where the simulation result will be saved


path="../json/$(simName).json"
savedDataFolderPath="../json/$(simName)/" # make sure this folder exists, this is where the simulation result will be saved

println("Initiated Rover!")


