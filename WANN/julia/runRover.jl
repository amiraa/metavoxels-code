# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021
####################################################

include("../../julia/MetaVoxels.jl") 

function floorPenetration(x,y,nomSize)
    floor=0.001*1.2
    # floor=0.0
    p=0.0
    d=10.0
    if(y<floor)
    # if(y<floor&& (x<5.0*d || x>=14.0*d))
        p=floor-y
    end
    return p
end
function penetrationStiffness(E,nomSize)
    return (1.0*E*nomSize)
end 
#!< returns the stiffness with which this voxel will resist penetration. This is calculated according to E*A/L with L = voxelSize/2.


setupSim=[]
setupSim=getSetupFullPath(path); #get simulation 
runMetaVoxels!(setupSim,savedDataFolderPath,"CPU");
target=getFinalDisplacement(savedDataFolderPath,1,"x");
