# using Images

function pixelGrid(res=64)
    m=collect(Iterators.product(-1.0:2.0/(res-1):1.0, -1:2.0/(res-1):1.0))[:] #equivilant to meshgrid
    m=reshape(collect(Iterators.flatten(m)),2,length(m))
    return m
end

function circle(resolution=64,radius=1,thresholded=true)
    pixels = pixelGrid(resolution);
    X=pixels[1,:];
    Y=pixels[2,:];
    frep1= ((radius).*(radius).-((X.-(0)).*(X.-(0)).+(Y.-(0)).*(Y.-(0))));
    if thresholded
        frep1[frep1.<0.5].=0.0;
        frep1[frep1.>=0.5].=1.0;
    end
    #eturn reshape(frep1,resolution,resolution)
    return frep1
end

function lossFunction(im,frep1)
    return sum(abs.(im.-frep1))/length(im)
end