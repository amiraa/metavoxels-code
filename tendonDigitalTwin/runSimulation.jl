GPU=false # if you want to use the gpu or not
plotting=false # if going to plot graphs
logging=false # if you want the simulation to be verbose

simName= "tutorial" # set name for simulation
factor1=3000*1
factor2=3000*1

simName= ARGS[1] # set name for simulation

load=parse(Bool,ARGS[2])

factor1=parse(Int64,ARGS[3])
factor2=parse(Int64,ARGS[4])

if load
    include("../julia/examples/tendonDigitalTwin.jl") #template for multimaterial hierarchical voxels with different thermal coefficient of thermal expansion 
end
include("../julia/MetaVoxels.jl")

function exportJuliaSettingsUsingNode(setup,name)
    #export prev. settings to json
    fileName="./json/$(name)Init.json"
    setup1=Dict()
    setup1["setup"]=setup
    stringdata = JSON.json(setup1)
    open(fileName, "w") do f
            write(f, stringdata)
    end
    #run node.js to draw the gerometry using rhino3dm
    mycommand = `node ./node/app1.js $(name)`
    run(mycommand)
end

function getSetup(fileName)
    setup = Dict()
    name=string("./json/$(fileName)",".json")
    open(name, "r") do f
       #global setup
        dicttxt = String(read(f))  # file information to string
        setup=JSON.parse(dicttxt)  # parse and transform data
    end
    setup=setup["setup"]
    setup["viz"]["colorMaps"]=""
    return setup
end

function floorEnabled()
    return false
end

function gravityEnabled()
    return false
end

function externalDisplacement(currentTimeStep,N_position,N_fixedDisplacement)
    return N_fixedDisplacement
end

function externalForce(currentTimeStep,N_position,N_currentPosition,N_force)
    return N_force
end

function updateTemperature(currentRestLength,currentTimeStep,mat)
    maxcte=0.1
    maxtemp=5
    tempFactor1=factor1/6000/2.0
    tempFactor2=factor2/6000
    temp=0.0
    # print(factor1)
    if abs(mat.cTE)==0.1 #lower
        if mat.cTE>0
            temp=tempFactor1*maxtemp
        else
            temp=-tempFactor1*maxtemp
        end
    elseif abs(mat.cTE)==0.2
        if mat.cTE>0
            temp=tempFactor2*maxtemp
        else
            temp=-tempFactor2*maxtemp
        end
    else
        return currentRestLength
    end
    currentRestLength=0.5*mat.L*(2.0+temp*maxcte)
    return currentRestLength
end

if load
    exportJuliaSettingsUsingNode(setup,simName)
end

setupSim=getSetup(simName); #get simulation from"./json/$(simName).json"
savedDataFolderPath="./json/$(simName)/" # make sure this folder exists, this is where the simulation result will be saved
runMetaVoxels!(setupSim,savedDataFolderPath,"CPU");