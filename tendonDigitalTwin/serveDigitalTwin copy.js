// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2021

//to run call 'node ./tendonDigitalTwin/serveDigitalTwin.js' from "./metavoxels-code"

const { exec } = require("child_process");

var express = require('express');
var fs = require('fs');
var app = express();
var path = require('path');
var setup,cmods;

// will parse incoming JSON data and convert it into an object literal for you
app.use(express.json({limit: '50mb', extended: true}));
app.use(express.urlencoded({limit: '50mb', extended: true}));

//serve the html
app.use(express.static(__dirname + '/../')); // exposes index.html, per below


app.post("/", function(req, res) {
  // each key in req.body will match the keys in the data object that you passed in
  var myObject = req.body.data;
  setup=JSON.parse(myObject.foo);

  // console.log(setup)
  runSimulation(setup)
  // cmods=setup.cmods;
  
  // if(setup.run){
  //   console.log("run CMODS!");
  //   runCMODS();
  // }else{
  //   console.log("reading data");
  // }

  // console.log(setup);
  // myObject.foo === "bar"
  res.send("I am done");
});

port = 8000;
app.listen(port);
console.log(`Open http://localhost:${port}/demos/indexTendonDigitalTwin.html in your browser`);

function runSimulation(setup){

  var command="julia ./tendonDigitalTwin/runSimulation.jl tutorial 1 -3000 3000"

  var juliaLoc="\"C:/Users/amira/AppData/Local/Programs/Julia\ 1.5.2/bin/julia\""
  var command=juliaLoc+" ./tendonDigitalTwin/runSimulation.jl "+setup.simName+" "+setup.load+" "+setup.lowerTendon+" "+setup.upperTendon+""
  console.log(command)

  exec(command, (error, stdout, stderr) => {
      if (error) {
          console.log(`error: ${error.message}`);
          return;
      }
      if (stderr) {
          console.log(`stderr: ${stderr}`);
          return;
      }
      console.log(`stdout: ${stdout}`);
      console.log("done sim")

      // app.post("/", function(req, res) {
      //   // each key in req.body will match the keys in the data object that you passed in
        
      //   res.send(setup);
      // });
      // app.get('/', function(req, res){
      //   res.send("Hello world!");
      // });
    
      
  });

  
}

