# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2022

display(Threads.nthreads())

include("./julia/include/_topologyOptimization.jl")
Makie.inline!(true)
using Folds


Macro_struct = [10.0, 6.0, 4., 10, 6, 4, [0.5,0.5],1.5,0.2];
Macro_struct = [6.0, 3.0, 2., 6, 3, 2, [0.5,0.5],1.5,0.2];
Micro_struct = [0.1, 0.1, 0.1, 100, 100, 100, 0.25,2,0.1];
penal = 3;
saveItr=5;
θ=3
maxloop=20

prob=Canteliver3D
fabric=true
mgcg=[false,true]
voxels=1

ratioD=0.0;Micro_Vol=0.25;num=5;
library=[ratioD,Micro_Vol,num]
verbose=true
parallel=true

Macro_xPhys2,Micro_xPhys2,DHs2,a2,aYX,aYZ,aXZ=Uclustering3DStrainLibrary(Macro_struct, Micro_struct,prob,library,verbose);
