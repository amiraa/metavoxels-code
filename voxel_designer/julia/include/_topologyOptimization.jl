#======================================================================================================================#
# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020
#======================================================================================================================#

#########################################################################################################

using StaticArrays: minimum
using Base: return_types
using Plots: display

using StaticArrays, BenchmarkTools, SparseArrays
using LinearAlgebra, Statistics
# import JSON
using NLopt
using Plots

# using Images
using VectorizedRoutines #matlab.meshgrid

using JLD #load and save data
# using Folds
gr(size=(400,300))


# Pkg.add(Pkg.PackageSpec(;name="FileIO", version="1.4.5"))
# using Flux3D
# using Makie
# Makie.AbstractPlotting.set_theme!(show_axis = false, scale=false)
# AbstractPlotting.inline!(true)

#########################################################################################################

####### utilities
include("./utils.jl")

include("./filter.jl")
include("./element.jl")
include("./OC.jl")

include("./plot.jl")


####### testcases
include("./problems.jl")

####### optimization
include("./top2D.jl")
include("./top3D.jl")
include("./topmgcg.jl")

# include("./timeSpace.jl")


# multiscale and homogenization
include("./asymp_homogenization.jl")
include("./multimaterial.jl")
include("./microstructure_topX.jl")
# include("./multiscale.jl")

# concurrent
include("./concurrent2D.jl")
include("./concurrent3D.jl")

# strain library
include("./microstructureDesignUe.jl")
include("./strain.jl");
include("./strain3D.jl");

# clustering
include("./clustering2D.jl");
include("./clustering3D.jl");

# fabrication constraints
include("./fabrication.jl")
# include("./maxlength2D.jl")

#########################################################################################################

println("Loaded Topology Optimization Library!")

#########################################################################################################