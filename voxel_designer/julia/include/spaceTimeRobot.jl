# Function: Space-Time Topology Optimization for Additive Manufacturing: 
#           Concurrent Optimization of Structural Layout and Fabrication Sequence
# Author:  Weiming Wang (wwmdlut@gmail.com)
# Version: 2020-05-14
# Usage:   [xPhys, tPhys] = Space_Time_TopOpt_Robot(90, 30, 500, 8, 0.6, 0, 0, 0.5);
# volfrac: volume constraint
# nStage:  the number of layers
# nloop:   the number of iterations
# nely:    dimension in y-axis
# nelx:    dimension in x-axis
# Theta:   parameter in objective function
# If Upper_Lower_Print = 1 and Upper_And_Lower_Print = 0, one robot prints up and down from left to right; 
# If Upper_Lower_Print = 0 and Upper_And_Lower_Print = 1, two robots print simultaneously one is up and one is down from left to right;
# If Upper_Lower_Print = 0 and Upper_And_Lower_Print = 0, one robot prints on the top from left to right.
# You cannot choose Upper_And_Lower_Print = 1 and Upper_Lower_Print = 1

function Space_Time_TopOpt_Robot(nelx,nely,problem,nloop, nStage, volfrac,Upper_Lower_Print, Upper_And_Lower_Print, Theta)
    ## Locations of the robots
    tPrint = range(0, stop=1, length=nStage+1);
    xRobot = range(1, stop=nelx, length=nStage + 1);
    yRobot = (zeros(length(xRobot)) .+ 1);
    fPoint = (floor.(xRobot).-1)*(nely.+1).+yRobot;

    if Upper_Lower_Print == 1
        fPointLower = fPoint + nely;
        fPointUpper = fPoint;
        fPoint = [fPointUpper[1:2:end] [fPointLower[2:2:end]; 0]];
        fPoint = fPoint';
        fPoint = fPoint[:];
        fPoint[end] = [];
        fPoint[end] = nelx*(nely+1)+1;
        fPoint = unique(fPoint);
        xRobot = floor(xRobot[:]);
        yRobot = [yRobot[1:2:end] [yRobot[2:2:end]+nely; zeros(mod(length(xRobot), 2), 1)]]';
        yRobot = yRobot[:];
        yRobot = yRobot[1:length(xRobot), :];
        PT = tPrint[1:end-1];
    elseif Upper_And_Lower_Print == 1
        fPointLower = fPoint .+ nely;
        fPointUpper = fPoint;
        fPoint = [fPointUpper fPointLower];
        xRobot = [xRobot xRobot]';
        xRobot = xRobot[:];
        yRobot = [yRobot yRobot.+nely]';
        yRobot = yRobot[:];
        PT = [tPrint[1:end-1]' tPrint[1:end-1]']';
        PT = PT[:];

    else
        PT = tPrint[1:end-1];
        fPoint[end] = nelx*(nely+1)+1;
    end

    rRobot = nely+20; # radius of robot arm
    s = repeat(1 : nely, 1, nelx);
    yElement = s[:];
    t = repeat(1:nelx, 1,nely)'
    xElement = t[:];
    V = [xElement yElement];


    v = V - repeat([xRobot[1] yRobot[1]], size(V)[1] ,1);
    distance= sqrt.(sum(v.^2, dims=2)');

    for i = 2 : size(xRobot)[1]
        v = V - repeat([xRobot[i] yRobot[i]], size(V)[1] ,1);
        distance=[distance; sqrt.(sum(v.^2, dims=2)')];
    end

    # eControl = cell(length(PT), 1);
    eControl =Array{Any}(undef, length(PT));
    flag = zeros(nely*nelx, length(PT));
    for i = 1 : length(PT)
        t = distance[i, :];
        CI=findall(Bool.(t .< rRobot))
        yt=[ ii[1] for ii in CI ];
        temp = unique(yt);
        eControl[i] = temp;
        flag[temp, i] .= 1;
    end

    ## Lower and Upper bounds for elements
    tUpper = zeros(nely*nelx);
    tLower = zeros(nely*nelx);
    for i = 1 : size(flag, 1)
        CI=findall(Bool.(flag[i, :] .== 1))
        yy=[ i[1] for i in CI ];

        tt = PT[yy];

        tm = minimum(tt); 
        tM = maximum(tt);
        tLower[i] = tm;
        if Bool(Upper_And_Lower_Print)
            tUpper[i] = tM+PT[3]-PT[1];
        else
            tUpper[i] = tM+PT[2]-PT[1];
        end
    end

    ## CONNECTIVITY MATRIX / LAPLACE MATRIX
    lrmin = 2;
    iH = ones(convert(Int,nelx*nely*(2*(ceil(lrmin)-1)+1)^2),1)
    jH = ones(Int,size(iH))
    sH = zeros(size(iH))
    k = 0;
    for i1 = 1:nelx
        for j1 = 1:nely
            e1 = (i1-1)*nely+j1
            for i2 = max(i1-(ceil(lrmin)-1),1):min(i1+(ceil(lrmin)-1),nelx)
                for j2 = max(j1-(ceil(lrmin)-1),1):min(j1+(ceil(lrmin)-1),nely)
                    e2 = (i2-1)*nely+j2
                    if e1 == e2
                        continue;
                    end
                    k = k+1
                    iH[k] = e1
                    jH[k] = e2
                    sH[k] = 1
                end
            end
        end
    end
    L = sparse(vec(iH),vec(jH),vec(sH))
    M = repeat(sum(L, dims=2), 1, size(L)[2]);
    # E = eye(size(L)); 
    E = Matrix(1.0I, size(L)[1], size(L)[2]);
    L = E - L./M;
    L = sparse(L)

    ## MATERIAL PROPERTIES
    Emax = 1;
    Emin = 1e-9;
    nu = 0.3;

    ## INITIALIZE SVERAL PARAMETERS
    penal = 3;      # stiffness penalty
    rmin = 2;     # density filter radius

    ## PREPARE FINITE ELEMENT ANALYSIS
    ## Stiffness matrix for element
    KE=lk();

    ## PREPARE FILTER DENSITY
    H ,Hs=make_filter(nelx,nely,rmin);

    ## INITIALIZE ITERATION
    beta = 1;       # beta continuation
    eta = 0.5;      # projection threshold, fixed at 0.5
    x = fill(volfrac,nely,nelx);
    xTilde = x;
    xPhys = (tanh.(beta*eta) .+ tanh.(beta*(xTilde .-eta))) / (tanh.(beta*eta)  .+ tanh.(beta*(1 .-eta)));

    ## Initial time field
    s = repeat(1 : nely, 1, nelx);
    yElement = s[:].-0.5;
    t = repeat(1:nelx, 1, nely)';
    xElement = t[:].-0.5;
    V = [xElement yElement];

    vect = V - repeat(V[1, :],  1,size(V)[1])';
    dis2 = sum(vect.*vect, dims=2);
    tPhys = sqrt.(dis2) / maximum(sqrt.(dis2));
    tPhys = reshape(tPhys, nely, nelx);
    t = tPhys;

    ## Freedom of degree and loads
    U,F,freedofs=problem(nelx,nely)

    ##
    xold1 = reshape(x,nely*nelx,1);
    xold2 = reshape(x,nely*nelx,1);
    xold1 = [xold1; tLower];
    xold2 = [xold2; tLower];
    low = 0;
    upp = 0;
    rou = 10;

    while loop=1:nloop
        
        if mod(loop, 30) == 0 && rou < 100
            rou = rou + 10;
        end
        
        if mod(loop, 20) == 0 && beta < 40
            beta = beta + 2;
        end
        
        ## OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        dv = ones(nely,nelx);
        dx = beta * (1 .-tanh.(beta*(xTilde .-eta)).*tanh.(beta*(xTilde .-eta))) / (tanh.(beta*eta) + tanh.(beta*(1-eta)));
        dv[:] = H*(dv[:].*dx[:]./Hs);
        dc = zeros(nely, nelx);
        dt = zeros(nely, nelx);
        
        ## Objective function
        c, dcx = Cal_c_ce_whole(nelx, nely, KE, xPhys, Emin, Emax, penal, freedofs, F);
        obj = c;
        dc[:] = dc[:] + H*(dcx[:].*dx[:]./Hs);
        
        
        tP = range(0, stop=1, length=nStage + 1)
        for i = 1 : nStage - 1
            fp = [ fPoint[i + 1]];
            force = [-0.5];
        
            if Bool(Upper_And_Lower_Print)
                fp = [fPoint[i+1, 1]; fPoint[i+1, 2]];
                force = [ -0.5;  -0.5];
            end
        
            ti = tP[i+1];
            c, dcx, dct = Cal_c_ce_for_weightOfRobot(nelx, nely, KE, xPhys, tPhys,Emin, Emax, penal, ti, fp, force, rou, freedofs);
            obj = obj + Theta*c;
            dc[:] = dc[:] + Theta*H*(dcx[:].*dx[:]./Hs);
            dt[:] = dt[:] + Theta*H*(dct[:]./Hs);
        end
        
        df0 = [dc dt];
        
        ## UPDATE OF DESIGN VARIABLES AND PHYSICAL DENSITIES
        df0dx = df0[:];
        n=length(df0dx);
        move = 0.01;
        tmove = 0.01;
        xmin=max.(0.0, x[:].-move);
        xmax=min.(1, x[:].+move);
        tmin = max.(tLower, t[:].-tmove);
        tmax = min.(tUpper, t[:].+tmove);
        xmin = [xmin; tmin];
        xmax = [xmax; tmax];
        xval = [x[:]; t[:]];
        f0val = obj;
        
        ## Global volume constraint
        fval = sum(sum(xPhys)) / (nelx*nely*volfrac) - 1;
        dfdx= [dv[:]'/(nelx*nely*volfrac) zeros(1, nely*nelx)];
        
        ## Continuity constraint
        LL = L;
        kk = 2*(nely*nelx); # controlling the smoothness of the time field
        A = LL*tPhys[:];
        B = A.^2/(nely*nelx);
        fval = [fval; kk*(sum(B) .-1.0e-6)];
        dft = kk*2*LL'*A;
        dft = H*(dft./Hs)/(nely*nelx);
        dfdx = [dfdx; zeros(1, nely*nelx)  dft'];
        
        ## Starting point
        fval = [fval; tPhys[1] - 1.0e-9];
        dfdx = [dfdx; zeros(1, nely*nelx) (H*(Matrix(1.0I,1, nely*nelx)'./Hs))'];
        
        ## Volumen constraints per layer
        percent = 1/nStage;
        tP = range(0, stop=1, length=nStage+1);
        for i = 1 : nStage
            ##
            ti = tP[i+1];
            ft = 1 .- (tanh.(rou*ti) .+ tanh.(rou*(tPhys .- ti)))/(tanh.(rou*ti) .+ tanh.(rou*(1 .-ti)));
            dfdt = -(rou*(tanh.(rou*(tPhys .- ti)).^2 .- 1))/(tanh.(rou*(ti .- 1)) - tanh.(rou*ti));
            xtJoint = xPhys.*ft;
            fval = [fval; sum(xtJoint[:])/(nelx*nely*volfrac) - i*percent];
            dfx = ft/(nelx*nely*volfrac);
            dfx = H*(dfx[:].*dx[:]./Hs);
            dft = xPhys.*dfdt/(nelx*nely*volfrac);
            dft = H*(dft[:]./Hs);
            dfdx = [dfdx; dfx[:]' dft[:]'];
            
            #
            fval = [fval; -sum(xtJoint[:])/(nelx*nely*volfrac) .+ i*percent .- 1.0e-5];
            dfdx = [dfdx; -dfx[:]' 0.0.-dft[:]'];
        end
        
        ## Solving with MMA
        m=length(fval);
        mdof = 1:m;
        a0 = 1;
        a = zeros(m);
        c_ = ones(m)*1000;
        d = zeros(m);
        
        xmma, ymma, zmma, lam, xsi, eta_, mu, zet, s, low, upp =  mmasub(m, n, loop, xval, xmin, xmax, xold1, xold2,f0val, df0dx, fval[mdof], dfdx[mdof,:],low, upp, a0, a, c_, d);
        
        xnew = reshape(xmma, nely, Int(size(xmma)[1]/nely))
        xold2 = xold1;
        xold1 = xval;
        s = xnew[:, 1:nelx];
        xTilde[:] = (H*s[:])./Hs;
        xPhys = (tanh.(beta*eta) .+ tanh.(beta*(xTilde .-eta))) / (tanh.(beta*eta) .+ tanh.(beta*(1 .-eta)));
        x = s;
        
        ##
        t = xnew[:, nelx+1 : end];
        tPhys = t;
        tPhys[:] =  (H*t[:])./Hs;
        
        ## Pring results
        fval[2] = fval[2]/kk;
        println(" It:$loop Obj:$(round.(obj,digits=2)) Vol:$(round.(sum(xPhys)/(nelx*nely),digits=2)) Cons:$(round.(fval,digits=2))")

        
        ## Draw
        if mod(loop, 10) == 0
            mc = cgrad(:thermal, nStage, categorical = true)

            (Plots.heatmap(( 0.0.-xPhys),xaxis=nothing,showaxis = false,clims=(-1.0, 0.0),yaxis=nothing,legend=nothing,fc=:grays,aspect_ratio=:equal))#,clims=(0.0, 1.0)

            ss = range(0, stop=size(mc)[1], length=nStage+1);
            if Bool(Upper_Lower_Print)
                rx = zeros(size(fPoint)[1], 1);
                rx[2:2:end] = fPoint[2:2:end]/(nely+1) - 0.5;
                rx[1:2:end] = (fPoint[1:2:end] - 1)/(nely+1) + 0.5; 
                ry = yRobot; 
                ry[1:2:end].=0 .+ 0.5
                ry[2:2:end].=nely .+ 0.5
            elseif Bool(Upper_And_Lower_Print)
                rx = zeros(size(fPoint)[1], 2);
                rx[:, 1] .= (fPoint[:, 1] .- 1) ./(nely .+1) .+ 0.5;
                rx[:, 2] .= fPoint[:, 2] ./(nely+1) .- 0.5;
                rx = rx'; rx = rx[:];
                ry = yRobot; 
                ry[1:2:end].=0 .+ 0.5
                ry[2:2:end].=nely .+ 0.5
            else
                rx = (fPoint - 1)/(nely+1) + 0.5;
                ry = yRobot-2.2;
                ry=0 .+ 0.5
            end

            for kk = 1:nStage
                if Bool(Upper_And_Lower_Print)
                    scatter!([rx[2*kk-1]], [ry[2*kk-1]],c=mc[kk])
                    scatter!([rx[2*kk]], [ry[2*kk]],c=mc[kk])
                else
                    scatter!([rx[kk]], [ry[kk]],c=mc[kk])
                end
            end

            display(scatter!([nelx+0.5], [nely+0.5],c=mc[nStage]))
            # # title('Density Field');



            heatmap(tPhys,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,clims=(0.0, 1.0),aspect_ratio=:equal)#,clims=(0.0, 1.0)
            (contourf!(tPhys, levels = minimum(tPhys):(maximum(tPhys)-minimum(tPhys))/nStage:maximum(tPhys),w=0.1, linecolor=:black))
            ss =  range(0, stop=size(mc)[1], length=nStage+1);
            for kk = 1:nStage
                if Bool(Upper_And_Lower_Print)
                    scatter!([rx[2*kk-1]], [ry[2*kk-1]],c=mc[kk])
                    scatter!([rx[2*kk]], [ry[2*kk]],c=mc[kk])
                else
                    scatter!([rx[kk]], [ry[kk]],c=mc[kk])
                end
            end
            #

            display(scatter!([nelx+0.5], [nely+0.5],c=mc[nStage]))
        end
    end
    return xPhys, tPhys

end




##
function Cal_c_ce_for_weightOfRobot(nelx, nely, KE, xPhys, tPhys,Emin, Emax, penal, ti, fpoint, force, lamda, freedofs)

    ft = 1 .- (tanh.(lamda .*ti) .+ tanh.(lamda*(tPhys .- ti)))/(tanh.(lamda*ti) .+ tanh.(lamda*(1 .-ti)));
    dfdt = -(lamda*(tanh.(lamda*(tPhys .- ti)).^2 .- 1))/(tanh.(lamda*(ti .- 1)) .- tanh.(lamda*ti));
    xtJoint = xPhys.*ft;

    ##
    nodenrs = reshape(1:(1+nelx)*(1+nely),1+nely,1+nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,nelx*nely,1)
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*nely.+[2 3 0 1] -2 -1],nelx*nely,1)
    iK = convert(Array{Int},reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1))
    sK = reshape(KE[:]*(Emin .+xtJoint[:]'.^penal*(Emax-Emin)),64*nelx*nely,1);

    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2

    F = zeros((nely+1)*(nelx+1), 2);
    F = F'; F = F[:];
    F[2*fpoint] = F[2*fpoint]+force;

    ##
    U = zeros(2*(nely+1)*(nelx+1), 1);
    U[freedofs] = K[freedofs, freedofs]\Array(F[freedofs]);

    ce = zeros(nely, nelx); 
    ce[1 : nely, 1 : nelx] = reshape(sum((U[edofMat]*KE).*U[edofMat],dims=2), nely, nelx);
    c = sum(sum((Emin.+xtJoint.^penal*(Emax-Emin)).*ce));
    dcx = -penal*(Emax-Emin)*xtJoint.^(penal-1).*ce.*ft;
    dct = -penal*(Emax-Emin)*xtJoint.^(penal-1).*ce.*xPhys.*dfdt;
    return c, dcx, dct

end

# Publication
# Weiming Wang, Dirk Munro, Charlie C.L. Wang, Fred van keulen, Jun Wu,
# Space-Time Topology Optimization for Additive Manufacturing: 
# Concurrent Optimization of Structural Layout and Fabrication Sequence. 
# Structural and Multidisciplinary Optimization, 61:1-18 (2020).