#======================================================================================================================#
# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2022
#======================================================================================================================#

###################################################################
# use dispAnimation2D to display the macrostructure deformation of resultant microstructure
function MicroTop2DU(Ue,Micro_struct, penal,saveItr=5,maxloop = 200,fab=false,thetaD=0,ratioD=0)
    ## USER-DEFINED LOOP PARAMETERS
    E0 = 1; Emin = 1e-9; nu = 0.3;θ=1;

    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2];
    Micro_nelx   = Int(Micro_struct[3]); Micro_nely  = Int(Micro_struct[4]);
    Micro_Vol   = Micro_struct[5];
    Micro_nele = Int(Micro_nelx*Micro_nely);
    Micro_rmin=(Micro_struct[6])
    Micro_move=(Micro_struct[7])

    ss=6
    
    # PREPARE FILTER
    Micro_H,Micro_Hs = make_filter(Micro_nelx, Micro_nely, Micro_rmin);

    ## Connectivity matrix for continuity 
    # L=connectivityMatrix(Micro_nelx,Micro_nely);


    # jet_me=ColorGradient([:white,:cyan,:yellow,:red,:black])
    # jet_me=ColorGradient([:blue,:cyan,:yellow,:red,:black])
    # t=:dark
    t=:Spectral_11
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    # cscheme=ColorGradient(cschemeList)
    cscheme=cgrad(cschemeList)
    


    # INITIALIZE ITERATION
    # Micro_x=[]
    # for i=1:θ
    #     append!(Micro_x ,[ones(Micro_nely,Micro_nelx)]);
    # end

    

    Micro_x=[]
    for i=1:θ
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx).*Micro_Vol]);
    end


    # for l=1:θ
    #     # if fab
    #     #     # Micro_x[l]=addFabConstraint2D(Micro_x[l],ss);
    #     # else
    #         Micro_x[l].=1.0 
    #         for i = 1:Micro_nelx
    #             for j = 1:Micro_nely
    #                 if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2) < min(Micro_nelx,Micro_nely)/32
    #                     Micro_x[l][j,i] = Emin+0.1;
    #                 end
    #             end
    #         end
    #     # end
    # end


    for l=1:θ
        if fab
            Micro_x[l]=addFabConstraint2D(Micro_x[l],ss);
        else
            Micro_x[l].=1.0 
            for i = 1:Micro_nelx
                for j = 1:Micro_nely
                    if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2) < min(Micro_nelx,Micro_nely)/16
                        Micro_x[l][j,i] =Micro_Vol #Emin+0.1;
                    end
                end
            end
        end
    end

    #mma
    Micro_xold1=[]
    Micro_xold2=[]
    Micro_low=[];
    Micro_upp=[];
    for i=1:θ
        append!(Micro_xold1 ,[copy(Micro_x[i])]);
        append!(Micro_xold2 ,[copy(Micro_x[i])]);
        append!(Micro_low ,[0]);
        append!(Micro_upp ,[0]);
    end



    beta = 1;

    Micro_xTilde = Micro_x[1];
    Micro_xPhys=[];
    Micro_dc = []
    Micro_dv = []
    Micro_change=[]
    DHs=[]
    dDHs=[]
    for i=1:θ
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM2D(Micro_xPhys1, Micro_length, Micro_width, E0, Emin, nu, penal);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end

    loopbeta = 0; loop = 0; 
    while loop < maxloop

        

        loop = loop+1; loopbeta = loopbeta+1;
        
        for i=1:θ
            Micro_dc[i]=zeros(Micro_nely, Micro_nelx);
            Micro_dv[i]=ones(Micro_nely, Micro_nelx);
        end

        for ii=1:θ
            for i = 1:Micro_nele
                dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i];
                        dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i];
                        dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i]];
                dKE = elementMatVec2D(0.1, 0.1, dDHe);
                dce = sum((Ue'*dKE).*Ue');
                Micro_dc[ii][i]  = -dce

                # dce = sum((U[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                # Micro_dc[ii][i] = -sum(sum((Emin .+Macro_xPhys[Bool.(Macro_masks[ii][:])][:].^eta*(1 .-Emin)).*dce));
            end
        end

        
        
        # FILTERING AND MODIFICATION OF SENSITIVITIES
        for i=1:θ
            Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta);
            Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
            Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
        end

        for i=1:θ
            if fab
                Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
                Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
            end
            Micro_x[i],Micro_xTilde, Micro_xPhys[i], Micro_change1 = OC2D_2(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, Micro_move, beta,fab,ss);
            # Micro_x[i], Micro_xPhys[i], Micro_change1,Micro_low[i],Micro_upp[i],Micro_xold1[i],Micro_xold2[i]= OC2DMMA(Micro_x[i],Micro_xold1[i],Micro_xold2[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, Micro_move, beta,loop,Micro_low[i],Micro_upp[i]);
            
            Micro_change[i]=Micro_change1;
            Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx);
            if fab
                Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
                Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
            end
        end

        for i=1:θ
            DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, penal);
            DHs[i]=DH;
        end

        
        # PRINT RESULTS
        println("$loop  Micro_Vol:$(round.(mean.(Micro_xPhys[:][:]),digits=2))  Micro_ch:$(round.(Micro_change,digits=2))")
        
        if mod(loop,saveItr)==0 
            
            space=fill(0,Micro_nely,1)
            Micro_xPhys_all=space;
            Micro_xPhys_all1=space;

            for i=1:θ
                thres=0.75;
                Micro_xPhys_temp=copy(Micro_xPhys[i])
                Micro_xPhys_temp[Micro_xPhys_temp.<thres].=0.0
                Micro_xPhys_temp[Micro_xPhys_temp.>=thres].=i
                Micro_xPhys_temp1=copy(Micro_xPhys[i])
                Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys_temp);
                Micro_xPhys_all=hcat(Micro_xPhys_all,space);
                Micro_xPhys_all1=hcat(Micro_xPhys_all1,Micro_xPhys_temp1);
                Micro_xPhys_all1=hcat(Micro_xPhys_all1,space);
            end
            display(Plots.heatmap(1.0.-Micro_xPhys_all1,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))

            display(Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
            (Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))

            # Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(1, θ),fc=jet_me,aspect_ratio=:equal)
            frame(Micro_anim)
            

        end

        if beta < 512 && (loopbeta >= 50  || Micro_change[1] <= 0.01 )
            beta = 2*beta; loopbeta = 0;  Micro_change[1] = 1;
            display("Parameter beta increased to $beta.");
        end

    end

    ##calculate and display properties
    savefig("./img/library/1/Micro_theta_$(thetaD)_ratio_$(ratioD)_vol_$(Micro_Vol).png") # Saves the CURRENT_PLOT as a .png
    save("./img/library/1/Micro_theta_$(thetaD)_ratio_$(ratioD)_vol_$(Micro_Vol).jld", "data", Micro_xPhys[1])


    ##calculate and display properties
    n=3
    for i=1:θ
        Micro_xPhys_temp=copy(Micro_xPhys[i])
        Micro_xPhys_temp[Micro_xPhys_temp.<0.75].=0.0
        Micro_xPhys_temp[Micro_xPhys_temp.>=0.75].=i
        
        display(Plots.heatmap(repeat(Micro_xPhys_temp,n,n),clims=(0, θ),fc=cscheme,aspect_ratio=:equal,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing))
        # savefig("./img/finalArrayMicro$(i).png") # Saves the CURRENT_PLOT as a .png
        
        U,S,V = svd(DHs[i]);
        sigma = S;
        k = sum(sigma .> 1e-15);
        SH = (U[:,1:k] * diagm(0=>(1 ./sigma[1:k])) * V[:,1:k]')'; # more stable SVD (pseudo)inverse
        EH = [1/SH[1,1], 1/SH[2,2]]; # effective Young's modulus
        GH = [1/SH[3,3]]; # effective shear modulus
        vH = [-SH[2,1]/SH[1,1],-SH[1,2]/SH[2,2]]; # effective Poisson's ratio
        display( "young:$(round.(EH,digits=3)), poisson:$(round.(vH,digits=3)), shear:$(round.(GH,digits=3))" )

        
        # savefig("./img/finalArrayMicro1$(i).png") # Saves the CURRENT_PLOT as a .png
        # UPDATE HEAVISIDE REGULARIZATION PARAMETER
        
    end
           
    return Micro_xPhys[1],Micro_Vol[1],DHs[1]
end

function MicroTop2DU_Connect(Ue,Micro_struct, penal,saveItr=5,maxloop = 200,fab=false,thetaD=0,ratioD=0)
    ## USER-DEFINED LOOP PARAMETERS
    E0 = 1; Emin = 1e-9; nu = 0.3;θ=1;

    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2];
    Micro_nelx   = Int(Micro_struct[3]); Micro_nely  = Int(Micro_struct[4]);
    Micro_Vol   = Micro_struct[5];
    Micro_nele = Int(Micro_nelx*Micro_nely);
    Micro_rmin=(Micro_struct[6])
    Micro_move=(Micro_struct[7])

    ss=6
    
    # PREPARE FILTER
    Micro_H,Micro_Hs = make_filter(Micro_nelx, Micro_nely, Micro_rmin);

    ## Connectivity matrix for continuity 
    KE=lk()
    nodenrs = reshape(1:(1+Micro_nelx)*(1+Micro_nely),1+Micro_nely,1+Micro_nelx)
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,Micro_nelx*Micro_nely,1)
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*Micro_nely.+[2 3 0 1] -2 -1],Micro_nelx*Micro_nely,1)
    iK = convert(Array{Int},reshape(kron(edofMat,ones(8,1))',64*Micro_nelx*Micro_nely,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,8))',64*Micro_nelx*Micro_nely,1))

    # U_3,F_3,freedofs_3=microStructureConnection_normal(Micro_nelx,Micro_nely)

    U_1,F_1,freedofs_1=microStructureConnection_shear1(Micro_nelx,Micro_nely)
    U_2,F_2,freedofs_2=microStructureConnection_shear2(Micro_nelx,Micro_nely)

    U_3,F_3,freedofs_3=microStructureConnection_normal1(Micro_nelx,Micro_nely)
    U_4,F_4,freedofs_4=microStructureConnection_normal2(Micro_nelx,Micro_nely)


    # jet_me=ColorGradient([:white,:cyan,:yellow,:red,:black])
    # jet_me=ColorGradient([:blue,:cyan,:yellow,:red,:black])
    # t=:dark
    t=:Spectral_11
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    # cscheme=ColorGradient(cschemeList)
    cscheme=cgrad(cschemeList)
    


    # INITIALIZE ITERATION
    Micro_x=[]
    for i=1:θ
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx)]);
    end

    # for i = 1:Micro_nelx
    #     for j = 1:Micro_nely
    #         if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2) < min(Micro_nelx,Micro_nely)/30#4
    #             for l=1:θ
    #                 Micro_x[l][j,i] = Emin+0.1;
    #             end
    #         end
    #     end
    # end

    Micro_x=[]
    for i=1:θ
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx).*Micro_Vol]);
    end

    for i=1:θ
        if fab
            Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
        end
    end

    #mma
    Micro_xold1=[]
    Micro_xold2=[]
    Micro_low=[];
    Micro_upp=[];
    for i=1:θ
        append!(Micro_xold1 ,[copy(Micro_x[i])]);
        append!(Micro_xold2 ,[copy(Micro_x[i])]);
        append!(Micro_low ,[0]);
        append!(Micro_upp ,[0]);
    end



    beta = 1;

    Micro_xTilde = Micro_x[1];
    Micro_xPhys=[];
    Micro_dc = []
    Micro_dv = []
    Micro_change=[]
    DHs=[]
    dDHs=[]
    for i=1:θ
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM2D(Micro_xPhys1, Micro_length, Micro_width, E0, Emin, nu, penal);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end

    loopbeta = 0; loop = 0; 
    while loop < maxloop
        loop = loop+1; loopbeta = loopbeta+1;
        for i=1:θ
            DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, penal);
            DHs[i]=DH;
        end
        for i=1:θ
            Micro_dc[i]=zeros(Micro_nely, Micro_nelx);
            Micro_dv[i]=ones(Micro_nely, Micro_nelx);
        end

        for ii=1:θ
            for i = 1:Micro_nele
                dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i];
                        dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i];
                        dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i]];
                dKE = elementMatVec2D(0.1, 0.1, dDHe);
                dce = sum((Ue'*dKE).*Ue');
                Micro_dc[ii][i]  = -dce

                # dce = sum((U[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                # Micro_dc[ii][i] = -sum(sum((Emin .+Macro_xPhys[Bool.(Macro_masks[ii][:])][:].^eta*(1 .-Emin)).*dce));
            end
            sK = reshape(KE[:]*(Emin.+Micro_xPhys[ii][:]'.^penal*(E0-Emin)),64*Micro_nelx*Micro_nely,1)
            K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;


            @timed U_1[freedofs_1] = K[freedofs_1,freedofs_1] \ Array(F_1[freedofs_1])
            @timed U_2[freedofs_2] = K[freedofs_2,freedofs_2] \ Array(F_2[freedofs_2])
            @timed U_3[freedofs_3] = K[freedofs_3,freedofs_3] \ Array(F_3[freedofs_3])
            @timed U_4[freedofs_4] = K[freedofs_4,freedofs_4] \ Array(F_4[freedofs_4])

            # Objective function and sensitivity analysis
            ce = reshape(sum((U_1[edofMat]*KE).*U_1[edofMat],dims=2),Micro_nely,Micro_nelx);
            c = sum(sum((Emin.+Micro_xPhys[ii].^penal*(E0-Emin)).*ce));
            dc = -penal*(E0-Emin)*Micro_xPhys[ii].^(penal-1).*ce;
            Micro_dc[ii]+=dc/5000;

            ce = reshape(sum((U_2[edofMat]*KE).*U_2[edofMat],dims=2),Micro_nely,Micro_nelx);
            c = sum(sum((Emin.+Micro_xPhys[ii].^penal*(E0-Emin)).*ce));
            dc = -penal*(E0-Emin)*Micro_xPhys[ii].^(penal-1).*ce;
            Micro_dc[ii]+=dc/5000;

            ce = reshape(sum((U_3[edofMat]*KE).*U_3[edofMat],dims=2),Micro_nely,Micro_nelx);
            c = sum(sum((Emin.+Micro_xPhys[ii].^penal*(E0-Emin)).*ce));
            dc = -penal*(E0-Emin)*Micro_xPhys[ii].^(penal-1).*ce;
            Micro_dc[ii]+=dc/5000;

            ce = reshape(sum((U_4[edofMat]*KE).*U_4[edofMat],dims=2),Micro_nely,Micro_nelx);
            c = sum(sum((Emin.+Micro_xPhys[ii].^penal*(E0-Emin)).*ce));
            dc = -penal*(E0-Emin)*Micro_xPhys[ii].^(penal-1).*ce;
            Micro_dc[ii]+=dc/5000;


        end

        
        
        # FILTERING AND MODIFICATION OF SENSITIVITIES
        for i=1:θ
            Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta);
            Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
            Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
        end

        for i=1:θ
            if fab
                Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
                Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
            end
            Micro_x[i],Micro_xTilde, Micro_xPhys[i], Micro_change1 = OC2D_2(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, Micro_move, beta,fab,ss);
            # Micro_x[i], Micro_xPhys[i], Micro_change1,Micro_low[i],Micro_upp[i],Micro_xold1[i],Micro_xold2[i]= OC2DMMA(Micro_x[i],Micro_xold1[i],Micro_xold2[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, Micro_move, beta,loop,Micro_low[i],Micro_upp[i]);
            
            Micro_change[i]=Micro_change1;
            Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx);
            if fab
                Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
                Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
            end
        end

        

        
        # PRINT RESULTS
        println("$loop  Micro_Vol:$(round.(mean.(Micro_xPhys[:][:]),digits=2))  Micro_ch:$(round.(Micro_change,digits=2))")
        
        if mod(loop,saveItr)==0 
            
            space=fill(0,Micro_nely,1)
            Micro_xPhys_all=space;
            Micro_xPhys_all1=space;

            for i=1:θ
                thres=0.75;
                Micro_xPhys_temp=copy(Micro_xPhys[i])
                Micro_xPhys_temp[Micro_xPhys_temp.<thres].=0.0
                Micro_xPhys_temp[Micro_xPhys_temp.>=thres].=i
                Micro_xPhys_temp1=copy(Micro_xPhys[i])
                Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys_temp);
                Micro_xPhys_all=hcat(Micro_xPhys_all,space);
                Micro_xPhys_all1=hcat(Micro_xPhys_all1,Micro_xPhys_temp1);
                Micro_xPhys_all1=hcat(Micro_xPhys_all1,space);
            end
            display(Plots.heatmap(1.0.-Micro_xPhys_all1,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))

            display(Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
            (Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))

            # Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(1, θ),fc=jet_me,aspect_ratio=:equal)
            frame(Micro_anim)
            

        end

        if beta < 512 && (loopbeta >= 20  || Micro_change[1] <= 0.01 )
            beta = 2*beta; loopbeta = 0;  Micro_change[1] = 1;
            display("Parameter beta increased to $beta.");
        end

    end

    ##calculate and display properties
    savefig("./img/library/1/Connect_Micro_theta_$(thetaD)_ratio_$(ratioD)_vol_$(Micro_Vol).png") # Saves the CURRENT_PLOT as a .png
    save("./img/library/1/Connect_Micro_theta_$(thetaD)_ratio_$(ratioD)_vol_$(Micro_Vol).jld", "data", Micro_xPhys[1])

    n=3
    for i=1:θ
        Micro_xPhys_temp=copy(Micro_xPhys[i])
        Micro_xPhys_temp[Micro_xPhys_temp.<0.75].=0.0
        Micro_xPhys_temp[Micro_xPhys_temp.>=0.75].=i
        
        display(Plots.heatmap(repeat(Micro_xPhys_temp,n,n),clims=(0, θ),fc=cscheme,aspect_ratio=:equal,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing))
        # savefig("./img/library/3/TrialArrayMicro_$(thetaD)_vol_$(Micro_Vol).png") # Saves the CURRENT_PLOT as a .png
        
        U,S,V = svd(DHs[i]);
        sigma = S;
        k = sum(sigma .> 1e-15);
        SH = (U[:,1:k] * diagm(0=>(1 ./sigma[1:k])) * V[:,1:k]')'; # more stable SVD (pseudo)inverse
        EH = [1/SH[1,1], 1/SH[2,2]]; # effective Young's modulus
        GH = [1/SH[3,3]]; # effective shear modulus
        vH = [-SH[2,1]/SH[1,1],-SH[1,2]/SH[2,2]]; # effective Poisson's ratio
        display( "young:$(round.(EH,digits=3)), poisson:$(round.(vH,digits=3)), shear:$(round.(GH,digits=3))" )

        
        # savefig("./img/finalArrayMicro1$(i).png") # Saves the CURRENT_PLOT as a .png
        # UPDATE HEAVISIDE REGULARIZATION PARAMETER
        
    end
           
    return Micro_xPhys[1],Micro_Vol[1],DHs[1]
end

include("./mma/mmasub.jl");
function MicroTop2DU_max(Ue,Micro_struct, maxParameters,saveItr=5,maxloop = 200,fab=false,thetaD=0)

    alpha=maxParameters[1];
    alphamax=maxParameters[2];
    beta=maxParameters[3];
    betamax=maxParameters[4];
    d_beta=maxParameters[5];
    d_eta=maxParameters[6];
    eta=maxParameters[7];
    etamax=maxParameters[8];
    
    ## USER-DEFINED LOOP PARAMETERS
    E0 = 1; Emin = 1e-4; nu = 0.3;
    stop = 0;
    
    θ=1;

    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2];
    Micro_nelx   = Int(Micro_struct[3]); Micro_nely  = Int(Micro_struct[4]);
    Micro_Vol   = Micro_struct[5];
    Micro_nele = Int(Micro_nelx*Micro_nely);
    # Micro_rmin=(Micro_struct[6])
    rmin_s=Micro_struct[6][1];
    rmin_v=Micro_struct[6][2];
    rmax=Micro_struct[6][3];
    Micro_move=(Micro_struct[7])

    rmin_m=rmax+rmin_v;
    rmin_xv=rmin_m;
    nele    = Micro_nelx*Micro_nely;
    dc_s    = ones(Float64,Micro_nely,Micro_nelx);
    dc_v    = ones(Float64,Micro_nely,Micro_nelx);
    dc_xv   = ones(Float64,Micro_nely,Micro_nelx);
    dv_s    = ones(Float64,Micro_nely,Micro_nelx);
    dv_v    = ones(Float64,Micro_nely,Micro_nelx);
    dv_xv   = ones(Float64,Micro_nely,Micro_nelx);

    ss=6
    
    # PREPARE FILTER
    # Micro_H,Micro_Hs = make_filter(Micro_nelx, Micro_nely, Micro_rmin);
    H_s,Hs_s= make_filter(Micro_nelx,Micro_nely,rmin_s);
    H_v,Hs_v= make_filter(Micro_nelx,Micro_nely,rmin_v);
    H_xv,Hs_xv= make_filter(Micro_nelx,Micro_nely,rmin_xv);

    KE=lk()
    nodenrs = reshape(1:(1+Micro_nelx)*(1+Micro_nely),1+Micro_nely,1+Micro_nelx)
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,Micro_nelx*Micro_nely,1)
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*Micro_nely.+[2 3 0 1] -2 -1],Micro_nelx*Micro_nely,1)
    iK = convert(Array{Int},reshape(kron(edofMat,ones(8,1))',64*Micro_nelx*Micro_nely,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,8))',64*Micro_nelx*Micro_nely,1))
    U_1,F_1,freedofs_1=microStructureConnection_shear(Micro_nelx,Micro_nely)
    U_2,F_2,freedofs_2=microStructureConnection_shear1(Micro_nelx,Micro_nely)

    ## Connectivity matrix for continuity 
    # L=connectivityMatrix(Micro_nelx,Micro_nely);


    # jet_me=ColorGradient([:white,:cyan,:yellow,:red,:black])
    # jet_me=ColorGradient([:blue,:cyan,:yellow,:red,:black])
    # t=:dark
    t=:Spectral_11
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    # cscheme=ColorGradient(cschemeList)
    cscheme=cgrad(cschemeList)
    


    # INITIALIZE ITERATION
    Micro_x=[]
    for i=1:θ
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx).*Micro_Vol]);

    end

    # for i = 1:Micro_nelx
    #     for j = 1:Micro_nely
    #         if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2) < min(Micro_nelx,Micro_nely)/16
    #             for l=1:θ
    #                 Micro_x[l][j,i] = Emin;
    #             end
    #         end
    #     end
    # end

    # Micro_x=[]
    # for i=1:θ
    #     append!(Micro_x ,[ones(Micro_nely,Micro_nelx).*Micro_Vol]);
    # end

    for i=1:θ
        if fab
            Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
        end
    end

    #mma
    Micro_xold1=[]
    Micro_xold2=[]
    Micro_low=[];
    Micro_upp=[];
    for i=1:θ
        append!(Micro_xold1 ,[copy(Micro_x[i])]);
        append!(Micro_xold2 ,[copy(Micro_x[i])]);
        append!(Micro_low ,[0]);
        append!(Micro_upp ,[0]);
    end


    Micro_xTilde = Micro_x[1];
    Micro_xPhys=[];
    Micro_dc = []
    Micro_dv = []
    Micro_change=[]
    DHs=[]
    dDHs=[]

    xTilde_s = Micro_xTilde;
    xTilde_v = Micro_xTilde;
    xTilde_xv = Micro_xTilde;
    xPhys_s= Micro_xTilde;
    xPhys_v= Micro_xTilde;
    xPhys_xv=Micro_xTilde;
    weightx_s,weightx_v,weightx_xv = w_function(alpha,Micro_x[1]);
    for i=1:θ

        weightx_s,weightx_v,weightx_xv = w_function(alpha,Micro_x[1]);
        xTilde_s = weightx_s;
        xTilde_v = weightx_v;
        xTilde_xv = weightx_xv;

        xTilde_s[:] = (H_s*xTilde_s[:])./Hs_s;
        xTilde_v[:] = (H_v*xTilde_v[:])./Hs_v;
        xTilde_xv[:] = (H_xv*xTilde_xv[:])./Hs_xv;

        xPhys_s = 1 .-exp.(-beta .*xTilde_s) .+xTilde_s.*exp.(-beta);
        xPhys_v = 1 .-exp.(-beta .*xTilde_v) .+xTilde_v.*exp.(-beta);
        xPhys_xv =1 .-exp.(-beta .*xTilde_xv) .+xTilde_xv.*exp.(-beta);
        Micro_xPhys1 = (xPhys_s+xPhys_xv-xPhys_v)/2;


        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM2D(Micro_xPhys1, Micro_length, Micro_width, E0, Emin, nu, eta);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end

    # INITIALIZE MMA OPTIMIZER
    m     = 1;                # The number of general constraints.
    n     = Micro_nely*Micro_nelx;             # The number of design variables x_j.
    xmin  = zeros(n);       # Column vector with the lower bounds for the variables x_j.
    xmax  = ones(n);        # Column vector with the upper bounds for the variables x_j.
    xold1 = copy(Micro_x[1]);             # xval, one iteration ago (provided that iter>1).
    xold2 = copy(Micro_x[1]);             # xval, two iterations ago (provided that iter>2).
    low   = copy(Micro_x[1]);        # Column vector with the lower asymptotes from the previous iteration (provided that iter>1).
    upp   = copy(Micro_x[1]);        # Column vector with the upper asymptotes from the previous iteration (provided that iter>1).
    a0    = 1;                # The constants a_0 in the term a_0*z.
    a     = zeros(m);       # Column vector with the constants a_i in the terms a_i*z.
    c_MMA = 100*ones(m);  # Column vector with the constants c_i in the terms c_i*y_i. Initially 10000
    d     = zeros(m);       # Column vector with the constants d_i in the terms 0.5*d_i*(y_i)^2.

    

    loopbeta = 0; loop = 0; 
    while Micro_change[1] > 0.001

        

        loop = loop+1; loopbeta = loopbeta+1;
        c=0;
        for i=1:θ
            Micro_dc[i]=zeros(Micro_nely, Micro_nelx);
            Micro_dv[i]=ones(Micro_nely, Micro_nelx);
            DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, eta);
            KEE=elementMatVec2D(0.1, 0.1, DH);
            ce = sum((Ue'*KEE).*Ue');
            c=sum(-ce);
        end
        
        for ii=1:θ
            for i = 1:Micro_nele
                dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i];
                        dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i];
                        dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i]];
                dKE = elementMatVec2D(0.1, 0.1, dDHe);
                dce = sum((Ue'*dKE).*Ue');
                Micro_dc[ii][i]  = -dce

                # dce = sum((U[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                # Micro_dc[ii][i] = -sum(sum((Emin .+Macro_xPhys[Bool.(Macro_masks[ii][:])][:].^eta*(1 .-Emin)).*dce));
            end

            sK = reshape(KE[:]*(Emin.+Micro_xPhys[ii][:]'.^eta*(E0-Emin)),64*Micro_nelx*Micro_nely,1)
            K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;


            @timed U_1[freedofs_1] = K[freedofs_1,freedofs_1] \ Array(F_1[freedofs_1])
            @timed U_2[freedofs_2] = K[freedofs_2,freedofs_2] \ Array(F_2[freedofs_2])
            # Objective function and sensitivity analysis
            ce = reshape(sum((U_1[edofMat]*KE).*U_1[edofMat],dims=2),Micro_nely,Micro_nelx);
            c+= sum(sum((Emin.+Micro_xPhys[ii].^eta*(E0-Emin)).*ce))/1000;
            dc = -eta*(E0-Emin)*Micro_xPhys[ii].^(eta-1).*ce;
            Micro_dc[ii]+=dc/1000;
            ce = reshape(sum((U_2[edofMat]*KE).*U_2[edofMat],dims=2),Micro_nely,Micro_nelx);
            c+= sum(sum((Emin.+Micro_xPhys[ii].^eta*(E0-Emin)).*ce))/1000;
            dc = -eta*(E0-Emin)*Micro_xPhys[ii].^(eta-1).*ce;
            Micro_dc[ii]+=dc/1000;
        end

        
        
        # FILTERING AND MODIFICATION OF SENSITIVITIES
        for i=1:θ
            dxTilde_s,dxTilde_v,dxTilde_xv     = dw_function(alpha,Micro_x[i]);

            dx_s =(beta.*exp.(-beta .*xTilde_s).+exp.(-beta));
            dx_v =(beta.*exp.(-beta .*xTilde_v).+exp.(-beta));
            dx_xv=(beta.*exp.(-beta .*xTilde_xv).+exp.(-beta));

            dc_s[:] = H_s*(Micro_dc[i][:].*dx_s[:]./Hs_s);
            dc_v[:] = H_v*(Micro_dc[i][:].*dx_v[:]./Hs_v);
            dc_xv[:] = H_xv*(Micro_dc[i][:].*dx_xv[:]./Hs_xv);


            dc_s=dc_s .*dxTilde_s;
            dc_v=dc_v .*dxTilde_v;
            dc_xv=dc_xv .*dxTilde_xv;


            Micro_dc[i]=0.5*(dc_s+dc_xv-dc_v);

            dv_s[:] = H_s*(Micro_dv[i][:].*dx_s[:]./Hs_s);
            dv_v[:] = H_v*(Micro_dv[i][:].*dx_v[:]./Hs_v);
            dv_xv[:] = H_xv*(Micro_dv[i][:].*dx_xv[:]./Hs_xv);

            dv_s=dv_s .*dxTilde_s;
            dv_v=dv_v .*dxTilde_v;
            dv_xv=dv_xv .*dxTilde_xv;
        
            Micro_dv[i]=0.5*(dv_s+dv_xv-dv_v);


            # Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta);
            # Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
            # Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
        end

        for i=1:θ
            if fab
                Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
                Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
            end

            # METHOD OF MOVING ASYMPTOTES
            xval  = Micro_x[i][:];
            f0val = c;
            df0dx = Micro_dc[i][:];
            fval  = sum(Micro_xPhys[i][:])/(Micro_Vol*nele) - 1;
            dfdx  = Micro_dv[i][:]' / (Micro_Vol*nele);

            xmma, ymma, zmma, lam, xsi, eta_, mu, zet, s, low, upp =  mmasub(m, n, loop, xval, xmin, xmax[:], xold1[:], xold2[:], f0val,df0dx,fval,dfdx,low[:],upp[:],a0,a,c_MMA,d,beta);

            # Update MMA Variables
            xnew     = reshape(xmma,Micro_nely,Micro_nelx);
            if fab
                xnew=addFabConstraint2D(xnew,ss);
            end
            
            xnew_s,xnew_v,xnew_xv = w_function(alpha,xnew);  #void weighting function 

            xTilde_s[:] = (H_s*xnew_s[:])./Hs_s;
            xTilde_v[:] = (H_v*xnew_v[:])./Hs_v;
            xTilde_xv[:] = (H_xv*xnew_xv[:])./Hs_xv;

            xPhys_s = 1 .-exp.(-beta .*xTilde_s)+xTilde_s.*exp.(-beta);
            xPhys_v = 1 .-exp.(-beta .*xTilde_v)+xTilde_v.*exp.(-beta);            
            xPhys_xv = 1 .-exp.(-beta .*xTilde_xv)+xTilde_xv.*exp.(-beta);
            Micro_xPhys[i] = (xPhys_s+xPhys_xv-xPhys_v)/2;

            if fab
                Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
                Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
            end

            xold2    =  copy(xold1);
            xold1    = copy(Micro_x[i]);


            Micro_change[1] = maximum(abs.(xnew[:]-Micro_x[i][:] )); 
            Micro_x[i] = xnew;

            
            
            
        end

        for i=1:θ
            DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, eta);
            DHs[i]=DH;
        end

        
        # PRINT RESULTS
        println("$loop c:$c  Micro_Vol:$(round.(mean.(Micro_xPhys[:][:]),digits=2))  Micro_ch:$(round.(Micro_change,digits=2))")
        
        if mod(loop,saveItr)==0 
            
            space=fill(0,Micro_nely,1)
            Micro_xPhys_all=space;
            Micro_xPhys_all1=space;

            for i=1:θ
                thres=0.75;
                Micro_xPhys_temp=copy(Micro_xPhys[i])
                Micro_xPhys_temp[Micro_xPhys_temp.<thres].=0.0
                Micro_xPhys_temp[Micro_xPhys_temp.>=thres].=i
                Micro_xPhys_temp1=copy(Micro_xPhys[i])
                Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys_temp);
                Micro_xPhys_all=hcat(Micro_xPhys_all,space);
                Micro_xPhys_all1=hcat(Micro_xPhys_all1,Micro_xPhys_temp1);
                Micro_xPhys_all1=hcat(Micro_xPhys_all1,space);
            end

            # gr(size=(800,600))
            # jet_me=ColorGradient([:blue,:cyan,:yellow,:red])
            # p1=Plots.heatmap(Micro_x[1],showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=jet_me,aspect_ratio=:equal)


            # xgraph  = 0:0.001:1.0;
            # ygraph_s,ygraph_v,ygraph_m = w_function(alpha,xgraph);
            # p2=Plots.plot(xgraph,ygraph_s,label="");
            # p2=Plots.plot!(xgraph,ygraph_v,label="");
            # p2=Plots.plot!(xgraph,ygraph_m,label="");
            # p2=Plots.plot!(xgraph,1 .- exp.(-beta .*xgraph) .+ xgraph .*exp.(-beta),label="");
  

            # p3=Plots.heatmap(Micro_xPhys[1]   ,title="",showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=jet_me,aspect_ratio=:equal)
            # p4= Plots.heatmap(weightx_s ,title="weightx_s ",showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=jet_me,aspect_ratio=:equal)
            # p5= Plots.heatmap(xTilde_s  ,title="xTilde_s  ",showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=jet_me,aspect_ratio=:equal)
            # p6= Plots.heatmap(xPhys_s   ,title="xPhys_s   ",showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=jet_me,aspect_ratio=:equal)
            # p7= Plots.heatmap(weightx_v ,title="weightx_v ",showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=jet_me,aspect_ratio=:equal)
            # p8= Plots.heatmap(xTilde_v  ,title="xTilde_v  ",showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=jet_me,aspect_ratio=:equal)
            # p9= Plots.heatmap(xPhys_v   ,title="xPhys_v   ",showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=jet_me,aspect_ratio=:equal)
            # p10=Plots.heatmap(weightx_xv,title="weightx_xv",showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=jet_me,aspect_ratio=:equal)
            # p11=Plots.heatmap(xTilde_xv ,title="xTilde_xv ",showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=jet_me,aspect_ratio=:equal)
            # p12=Plots.heatmap(xPhys_xv  ,title="xPhys_xv  ",showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=jet_me,aspect_ratio=:equal)



            # display(Plots.plot(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12, layout = (4, 3)))

            gr(size=(400,300))

            display(Plots.heatmap(1.0.-Micro_xPhys_all1,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))

            display(Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
            (Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))



            

            # Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(1, θ),fc=jet_me,aspect_ratio=:equal)
            frame(Micro_anim)
            

        end


        ## UPDATE HEAVISIDE REGULARIZATION PARAMETER
        if  (loopbeta >= maxloop || Micro_change[1] <= 0.001) #(beta < betamax || eta < etamax) &&
            # beta = min(d_beta*beta,betamax);      #beta MULTIPLIES by factor of d_beta every iteration
            beta = min(d_beta+beta,betamax);        #beta ADDS d_beta every iteration
            eta = min(etamax,eta + d_eta);
            alpha = min(alphamax,alpha+ d_beta);
            loopbeta = 0;
            # max_it = 40;
            Micro_change[1] = min(1,3-floor(beta/betamax)-floor(eta/etamax)-stop);
            stop = floor(beta/betamax)*floor(eta/etamax);
            println("Parameter beta equal to $beta  ");
            println("Parameter eta equal to  $eta   ");
            println("Parameter alpha equal to $alpha" );
        end 

        

    end

    savefig("./img/library/111/MinS_$(Int(rmin_s))_MinV_$(Int(rmin_v))_MaxS_$(Int(rmax))_Theta_$(thetaD)_vol_$((Micro_Vol)).png") # Saves the CURRENT_PLOT as a .png
    save("./img/library/111/MinS_$(Int(rmin_s))_MinV_$(Int(rmin_v))_MaxS_$(Int(rmax))_Theta_$(thetaD)_vol_$((Micro_Vol)).jld", "data", Micro_xPhys[1])
    # load("data.jld")["data"]


    ##calculate and display properties
    n=3
    for i=1:θ
        Micro_xPhys_temp=copy(Micro_xPhys[i])
        Micro_xPhys_temp[Micro_xPhys_temp.<0.75].=0.0
        Micro_xPhys_temp[Micro_xPhys_temp.>=0.75].=i
        
        display(Plots.heatmap(repeat(Micro_xPhys_temp,n,n),clims=(0, θ),fc=cscheme,aspect_ratio=:equal,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing))
        # savefig("./img/finalArrayMicro$(i).png") # Saves the CURRENT_PLOT as a .png
        # savefig("./img/library/33/MinS_$(Int(rmin_s))_MinV_$(Int(rmin_v))_MaxS_$(Int(rmax))_Theta_$(thetaD)_vol_$((Micro_Vol)).png") # Saves the CURRENT_PLOT as a .png

        U,S,V = svd(DHs[i]);
        sigma = S;
        k = sum(sigma .> 1e-15);
        SH = (U[:,1:k] * diagm(0=>(1 ./sigma[1:k])) * V[:,1:k]')'; # more stable SVD (pseudo)inverse
        EH = [1/SH[1,1], 1/SH[2,2]]; # effective Young's modulus
        GH = [1/SH[3,3]]; # effective shear modulus
        vH = [-SH[2,1]/SH[1,1],-SH[1,2]/SH[2,2]]; # effective Poisson's ratio
        display( "young:$(round.(EH,digits=3)), poisson:$(round.(vH,digits=3)), shear:$(round.(GH,digits=3))" )

        
        # savefig("./img/finalArrayMicro1$(i).png") # Saves the CURRENT_PLOT as a .png
        # UPDATE HEAVISIDE REGULARIZATION PARAMETER
        
    end

    # savefig("./img/library/111/MinS_$(Int(rmin_s))_MinV_$(Int(rmin_v))_MaxS_$(Int(rmax))_Theta_$(thetaD)_vol_$((Micro_Vol)).png") # Saves the CURRENT_PLOT as a .png
           
    return Micro_xPhys[1],Micro_Vol[1],DHs[1]
end

###################################################################
# cuboct vs strain library vs  full 3D  vs  vs 2D3D
function MicroTop3DU_Lib(Ue, Micro_struct,library, penal=3,mgcg=[false,true],size3D=20,saveItr=5,maxloop = 200,fab=false)
    unit=10;
    # displayElementDeformation3D(Ue,unit)
    
    Macro_length = 1; Macro_width = 1; Macro_Height = 1;
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = 1; Macro_nely  = 1; Macro_nelz   = 1;
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Micro_Vol_FM    = Micro_struct[7][1];
    Micro_rmin=Micro_struct[8]

    Micro_move=Micro_struct[9]

    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; 
    Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;Micro_nele2D = Micro_nelx*Micro_nely;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];
    Macro_xPhys = ones(Macro_nely,Macro_nelx,Macro_nelz)
    E0 = 1; Emin = 1e-9; nu = 0.3;penal=3;

    ###################################################################
    #cuboct
    cuboct=load("./img/library/1/cuboct_vol_$((Micro_Vol_FM)).jld")["data"];
    cuboctDH, dDH = EBHM2D(cuboct, Micro_length, Micro_width, E0, Emin, nu, penal);
    # cuboct3D ,cuboctDH3=visualize3D2DMicrostructure(cuboct,false)
    

    ###################################################################
    #library
    Umat=zeros(1,8*3)
    Umat[1,:].=Ue
    dissYX,dissZY,dissZX,X,Macro_xPhys_2D=reshapeU2DSlices_new(Umat,Macro_nely, Macro_nelx, Macro_nelz,Macro_xPhys,true);

    order=[1,2,3,4,5,6,7,8];
    n=size(X)[1];
    Θ=zeros(n);ΘD=zeros(n);
    von_mises=zeros(n);
    ratio=zeros(n);
    for i=1:n
        Ue1 = normalize(X[i]);
        # display(Ue1)
        Θ[i] , von_mises[i], ratio[i],α1,α2 =getPrincipalStrains(Ue1);
        # display(Θ[i]*180/pi)
    end
    ΘD=Θ.*180/π;
    E0 = 1; Emin = 1e-9; nu = 0.3;penal=3;
    θ=Int((num-1)*4+1)+1
    Micro_xPhys,DHs=loadVisualizeLibrary(library)
    Micro_x=Micro_xPhys
    t=:darktest
    cscheme=cgrad(t, θ, categorical = true)
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[cscheme[i]])
    end
    # cscheme=ColorGradient(cschemeList)
    cscheme1=cgrad(cschemeList)

    function findNearestList(theta)
        if theta<0
            theta+=180
        elseif theta>180
            theta-=180
        end
        findnearest(A::AbstractArray,t) = findmin(abs.(A.-t))[2]
        values=[]
        for i in range(0.0, 180, length=θ)
            append!(values,[i])
        end
        return values[findnearest(values,theta)]
    end
    function findNearestListIndex(theta)
        if theta<0
            theta+=180
        elseif theta>180
            theta-=180
        end
        findnearest(A::AbstractArray,t) = findmin(abs.(A.-t))[2]
        values=[]
        for i in range(0.0, 180, length=θ)
            append!(values,[i])
        end
        return findnearest(values,theta)
    end

    ΘDNearest=findNearestList.(ΘD)
    Uclustered=findNearestListIndex.(ΘD)
    a=Uclustered
    display(a[1])
    display(θ)
    for i=1:n
        Ue1 = X[i];
        if sum(Ue1 .== 0) ==0
            a[i]=θ #if no load cuboct
        end
    end
    ######
    aYX,aZY,aZX=reshape_back_reshapeU2DSlices_new(θ,a,Macro_nely, Macro_nelx, Macro_nelz,true)
    
    ###################################################################
    θTempList="";DH="";
    yy=1;xx=1;zz=1
    thickness=10;
    xPhys3D=zeros(100,100,100);
    for thick in 0:thickness-1
        θTemp=Int(aYX[Int((zz-1)*2+1)][Int(yy),Int(xx)]) ; xPhys3D[:,:,1+thick]=Micro_xPhys[θTemp];
        θTemp=Int(aYX[Int((zz-1)*2+2)][Int(yy),Int(xx)]) ; xPhys3D[:,:,end-thick]=Micro_xPhys[θTemp];
        θTemp=Int(aZY[Int((xx-1)*2+1)][Int(zz),Int(yy)]) ; xPhys3D[:,1+thick,:]=Micro_xPhys[θTemp]';
        θTemp=Int(aZY[Int((xx-1)*2+2)][Int(zz),Int(yy)]) ; xPhys3D[:,end-thick,:]=Micro_xPhys[θTemp]';
        θTemp=Int(aZX[Int((yy-1)*2+1)][Int(zz),Int(xx)]) ; xPhys3D[1+thick,:,:]=Micro_xPhys[θTemp]';
        θTemp=Int(aZX[Int((yy-1)*2+2)][Int(zz),Int(xx)]) ; xPhys3D[end-thick,:,:]=Micro_xPhys[θTemp]';
    end

    # θTemp=Int(aYX[Int((zz-1)*2+1)][Int(yy),Int(xx)]) ;xPhys3D[:,:,1:thickness]=Micro_x[θTemp];
    # θTemp=Int(aYX[Int((zz-1)*2+2)][Int(yy),Int(xx)]) ;xPhys3D[:,:,end-thickness+1:end]=Micro_x[θTemp];
    # θTemp=Int(aZY[Int((xx-1)*2+1)][Int(zz),Int(yy)]) ;xPhys3D[:,1:thickness,:]=Micro_x[θTemp]';
    # θTemp=Int(aZY[Int((xx-1)*2+2)][Int(zz),Int(yy)]) ;xPhys3D[:,end-thickness+1:end,:]=Micro_x[θTemp]';
    # θTemp=Int(aZX[Int((yy-1)*2+1)][Int(zz),Int(xx)]) ;xPhys3D[1:thickness,:,:]=Micro_x[θTemp]';
    # θTemp=Int(aZX[Int((yy-1)*2+2)][Int(zz),Int(xx)]) ;xPhys3D[end-thickness+1:end,:,:]=Micro_x[θTemp]';
           
    

    xPhys3D1=reshape(xPhys3D,Micro_nely,Micro_nelx,Micro_nelz,1,1)
    xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)

    scene= GLMakie.volume( permutedims(xPhys3D1, [2, 1, 3]), algorithm = :iso, isorange = 0.3, isovalue = 1.0,colormap=:grays)
    display(scene)   
    # DH, dDH = EBHM3D(xPhys3D1, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
    # dispFlag = 1;plotFlag = 1;outOption = "struct";
    # dens=sum(xPhys3D1)/(size(xPhys3D1)[1]*size(xPhys3D1)[2]*size(xPhys3D1)[3])
    # props, SH = evaluateCH(DH, dens, outOption, dispFlag);
    # fig=visual(DH);
    xPhys3DLibrary=xPhys3D1

    dens=getDens(xPhys3D1)

    DH, dDH = EBHM3D(xPhys3D1, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
    c=0
    for i = 1:length(xPhys3D1)
        dDHe = [dDH[1,1][i] dDH[1,2][i] dDH[1,3][i] dDH[1,4][i] dDH[1,5][i] dDH[1,6][i];
                dDH[2,1][i] dDH[2,2][i] dDH[2,3][i] dDH[2,4][i] dDH[2,5][i] dDH[2,6][i];
                dDH[3,1][i] dDH[3,2][i] dDH[3,3][i] dDH[3,4][i] dDH[3,5][i] dDH[3,6][i];
                dDH[4,1][i] dDH[4,2][i] dDH[4,3][i] dDH[4,4][i] dDH[4,5][i] dDH[4,6][i];
                dDH[5,1][i] dDH[5,2][i] dDH[5,3][i] dDH[5,4][i] dDH[5,5][i] dDH[5,6][i];
                dDH[6,1][i] dDH[6,2][i] dDH[6,3][i] dDH[6,4][i] dDH[6,5][i] dDH[6,6][i]];
        dKE = elementMatVec3D(Macro_Elex, Macro_Eley, Macro_Elez, dDHe);
        dce = sum((Umat*dKE).*Umat,dims=2);
        c+=sum(dce)
        # c+=sum((Umat*dKE))
    end
    println("Obj:$(c) Vol:$(dens)")
    dispFlag = 1;plotFlag = 1;outOption = "struct";
    props, SH = evaluateCH(DH, dens, outOption, dispFlag);
    fig=visual(DH);
    
    #  
    return xPhys3DLibrary     
end

function MicroTop3DU_2D(Ue, Micro_struct,library, penal=3,mgcg=[false,true],size3D=20,saveItr=5,maxloop = 200,fab=false)
    unit=10;
    # displayElementDeformation3D(Ue,unit)
    
    Macro_length = 1; Macro_width = 1; Macro_Height = 1;
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = 1; Macro_nely  = 1; Macro_nelz   = 1;
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Micro_Vol_FM    = Micro_struct[7][1];
    Micro_rmin=Micro_struct[8]

    Micro_move=Micro_struct[9]

    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; 
    Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;Micro_nele2D = Micro_nelx*Micro_nely;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];
    Macro_xPhys = ones(Macro_nely,Macro_nelx,Macro_nelz)
    E0 = 1; Emin = 1e-9; nu = 0.3;penal=3;

    ###################################################################
    #cuboct
    cuboct=load("./img/library/1/cuboct_vol_$((Micro_Vol_FM)).jld")["data"];
    cuboctDH, dDH = EBHM2D(cuboct, Micro_length, Micro_width, E0, Emin, nu, penal);
    # cuboct3D ,cuboctDH3=visualize3D2DMicrostructure(cuboct,false)
    

    ###################################################################
    #library
    Umat=zeros(1,8*3)
    Umat[1,:].=Ue

    

    ################################################################################
    ##### 2d 3D

    
    # beta = 1;
    # Micro_H,Micro_Hs = make_filter(Micro_nelx, Micro_nely, Micro_rmin);
    # Micro_Vol=[];Micro_x=[];Micro_xPhys3D=[];
    # θ=6
    # ss=6
    # for i=1:θ
    #     append!(Micro_Vol,[Micro_Vol_FM])
    #     append!(Micro_x ,[ones(Micro_nely,Micro_nelx).*Micro_Vol[i]]);
    #     Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);

            
    # end
    # Micro_xTilde = Micro_x[1];
    # Micro_xPhys=[];
    # Micro_dc = [];
    # Micro_dv = [];
    # Micro_change=[];
    # DHs=[]
    # dDHs=[]
    # MasksTemp=[]
    # for i=1:θ
    #     Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
    #     append!(Micro_xPhys ,[Micro_xPhys1]);
    #     append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx)]);
    #     append!(Micro_dv ,[ones(Micro_nely, Micro_nelx)]);
    #     append!(Micro_change ,[1.0]);
    #     DH, dDH = EBHM2D(Micro_xPhys1, Micro_length, Micro_width, E0, Emin, nu, penal);
    #     append!(DHs ,[DH]);
    #     append!(dDHs ,[dDH]);
    #     append!(MasksTemp,[[]])

    #     Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
    # end
    θ=6
    a=[1,2,3,4,5,6]
    aYX,aZY,aZX=reshape_back_reshapeU2DSlices_new(θ,a,Macro_nely, Macro_nelx, Macro_nelz,false)
    dissYX,dissZY,dissZX,U22D1,Macro_xPhys_2D=reshapeU2DSlices_new(Umat,Macro_nely, Macro_nelx, Macro_nelz,Macro_xPhys);
    Micro_xPhys=[];
    DHs=[];
    for i=1:θ
        # display(U22D1[i])
        Micro_xPhys1,vvv,DH1=MicroTop2DU_Connect(U22D1[i],[Micro_length, Micro_width, Micro_nelx, Micro_nely, Micro_Vol_FM, Micro_rmin,Micro_move], penal,maxloop,maxloop ,true)
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(DHs ,[DH1]);
    end
    # θTemp=Int(aYX[Int((zz-1)*2+1)][Int(yy),Int(xx)]) ;append!(MasksTemp[θTemp],[1]);
    # θTemp=Int(aYX[Int((zz-1)*2+2)][Int(yy),Int(xx)]) ;append!(MasksTemp[θTemp],[1]);
    # θTemp=Int(aZY[Int((xx-1)*2+1)][Int(zz),Int(yy)]) ;append!(MasksTemp[θTemp],[1]);
    # θTemp=Int(aZY[Int((xx-1)*2+2)][Int(zz),Int(yy)]) ;append!(MasksTemp[θTemp],[1]);
    # θTemp=Int(aZX[Int((yy-1)*2+1)][Int(zz),Int(xx)]) ;append!(MasksTemp[θTemp],[1]);
    # θTemp=Int(aZX[Int((yy-1)*2+2)][Int(zz),Int(xx)]) ;append!(MasksTemp[θTemp],[1]);

    # t=:darktest #t=:darktest
    # # theme(t);
    # cschemeList=[]
    # append!(cschemeList,[:white])
    # for i=1:θ
    #     append!(cschemeList,[palette(t)[i]])
    # end
    # cscheme=cgrad(cschemeList); #cscheme=ColorGradient(cschemeList);


    # Micro_penal=penal
    # loopbeta = 0; loop = 0; Macro_change = 1; beta = 1;
    # while loop < maxloop && ( Bool.(sum(Micro_change .>= 0.01)>=1))
    #     loop = loop+1; loopbeta = loopbeta+1;

    #     if mod(loop,saveItr)==0 || loop==1
    #         gr(size=(400,100))
    #         space=fill(0,Micro_nely,1)
    #         Micro_xPhys_all=space;
    #         Micro_xPhys_all1=space;
    #         for i=1:θ
    #             Micro_xPhys_temp=copy(Micro_xPhys[i])
    #             Micro_xPhys_temp1=copy(Micro_xPhys[i])
    #             thres=0.75;
    #             Micro_xPhys_temp[Micro_xPhys_temp.<thres].=0.0
    #             Micro_xPhys_temp[Micro_xPhys_temp.>=thres].=i
    #             Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys_temp);
    #             Micro_xPhys_all=hcat(Micro_xPhys_all,space);
    #             Micro_xPhys_all1=hcat(Micro_xPhys_all1,Micro_xPhys_temp1);
    #             Micro_xPhys_all1=hcat(Micro_xPhys_all1,space);
    #         end
    #         display(Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
    #         display(Plots.heatmap(1.0.-Micro_xPhys_all1,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))
    #         (Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))

    #         gr(size=(400,300))
    #         # for i=1:θ
    #         #     scene = Scene(resolution = (400, 400))
    #         #     scene= volume!(Micro_xPhys[i], algorithm = :iso,isorange = 0.4, isovalue = 0.9,colormap=:grays)
    #         #     display(scene)
    #         #     save("./img/Micro_xPhys_$(i)_$(Micro_Vol[i])_$(penal)_$(rmin)_$(loop).png",scene)
    #         # end

    #     end

    #     # FE-ANALYSIS AT TWO SCALES
    #     for i=1:θ
    #         DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, Micro_penal);
    #         DHs[i]=DH;
    #         dDHs[i]=dDH;
    #     end
    #     U22D=zeros(size(U22D1)[1],size(U22D1[1])[1]) #me being lazy trying to fix dimensions
    #     for i in 1:size(U22D1)[1]
    #         U22D[i,:].=U22D1[i]
    #     end
        
    #     for ii=1:θ
    #         for i = 1:Micro_nele2D
    #             dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i];
    #                     dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i];
    #                     dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i]];
    #             dKE = elementMatVec2D(Macro_Elex, Macro_Eley, dDHe);
    #             dce = sum((U22D[a.==ii,:]*dKE).*U22D[a.==ii,:],dims=2);
    #             Micro_dc[ii][i] = -sum(sum((Emin .+Macro_xPhys[:][MasksTemp[ii]][:].^Micro_penal*(1 .-Emin)).*dce));
    #         end
    #     end


    #     for i=1:θ
    #         Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta);
    #         Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
    #         Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
    #     end

        
    #     for i=1:θ
    #         ss=6
    #         Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
    #         Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
    #         Micro_x[i], Micro_xPhys[i], Micro_change1 = OC2D(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele2D, Micro_move, beta,true,ss);
    #         # Micro_x[i], Micro_xPhys[i], Micro_change1,Micro_low[i],Micro_upp[i],Micro_xold1[i],Micro_xold2[i]= OC2DMMA(Micro_x[i],Micro_xold1[i],Micro_xold2[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele2D, Micro_move, beta,loop,Micro_low[i],Micro_upp[i]);
    #         Micro_change[i]=Micro_change1;
    #         Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx);
    #         Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
    #         Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
    #     end
        

        
    # end

    θTempList="";DH="";
    yy=1;xx=1;zz=1
    thickness=10;
    xPhys3D=zeros(Micro_nely,Micro_nelx,Micro_nelz);

    for thick in 0:thickness-1
        θTemp=Int(aYX[Int((zz-1)*2+1)][Int(yy),Int(xx)]) ; xPhys3D[:,:,1+thick]=Micro_xPhys[θTemp];
        θTemp=Int(aYX[Int((zz-1)*2+2)][Int(yy),Int(xx)]) ; xPhys3D[:,:,end-thick]=Micro_xPhys[θTemp];
        θTemp=Int(aZY[Int((xx-1)*2+1)][Int(zz),Int(yy)]) ; xPhys3D[:,1+thick,:]=Micro_xPhys[θTemp]';
        θTemp=Int(aZY[Int((xx-1)*2+2)][Int(zz),Int(yy)]) ; xPhys3D[:,end-thick,:]=Micro_xPhys[θTemp]';
        θTemp=Int(aZX[Int((yy-1)*2+1)][Int(zz),Int(xx)]) ; xPhys3D[1+thick,:,:]=Micro_xPhys[θTemp]';
        θTemp=Int(aZX[Int((yy-1)*2+2)][Int(zz),Int(xx)]) ; xPhys3D[end-thick,:,:]=Micro_xPhys[θTemp]';
    end
       

    xPhys3D1=reshape(xPhys3D,Micro_nely,Micro_nelx,Micro_nelz,1,1)
    xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
    display(getDens(xPhys3D1))
    # xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
    scene= GLMakie.volume( permutedims(xPhys3D1, [2, 1, 3]), algorithm = :iso, isorange = 0.5, isovalue = 1.0,colormap=:grays)
    display(scene)   
    # DH, dDH = EBHM3D(xPhys3D1, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
    # dispFlag = 1;plotFlag = 1;outOption = "struct";
    # dens=sum(xPhys3D1)/(size(xPhys3D1)[1]*size(xPhys3D1)[2]*size(xPhys3D1)[3])
    # props, SH = evaluateCH(DH, dens, outOption, dispFlag);
    # fig=visual(DH);
    
    xPhys3D2D=xPhys3D1


    dens=getDens(xPhys3D1)

    DH, dDH = EBHM3D(xPhys3D1, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
    
    c=0
    for i = 1:length(xPhys3D1)
        dDHe = [dDH[1,1][i] dDH[1,2][i] dDH[1,3][i] dDH[1,4][i] dDH[1,5][i] dDH[1,6][i];
                dDH[2,1][i] dDH[2,2][i] dDH[2,3][i] dDH[2,4][i] dDH[2,5][i] dDH[2,6][i];
                dDH[3,1][i] dDH[3,2][i] dDH[3,3][i] dDH[3,4][i] dDH[3,5][i] dDH[3,6][i];
                dDH[4,1][i] dDH[4,2][i] dDH[4,3][i] dDH[4,4][i] dDH[4,5][i] dDH[4,6][i];
                dDH[5,1][i] dDH[5,2][i] dDH[5,3][i] dDH[5,4][i] dDH[5,5][i] dDH[5,6][i];
                dDH[6,1][i] dDH[6,2][i] dDH[6,3][i] dDH[6,4][i] dDH[6,5][i] dDH[6,6][i]];
        dKE = elementMatVec3D(Macro_Elex, Macro_Eley, Macro_Elez, dDHe);
        dce = sum((Umat*dKE).*Umat,dims=2);
        c+=sum(dce)
        # c+=sum((Umat*dKE))
    end
    println("Obj:$(c) Vol:$(dens)")

    
    dispFlag = 1;plotFlag = 1;outOption = "struct";
    props, SH = evaluateCH(DH, dens, outOption, dispFlag);
    fig=visual(DH);
    #  
    return xPhys3D2D     
end

function MicroTop3DU_3D(Ue, Micro_struct, penal=3,mgcg=[false,true],size3D=20,saveItr=5,maxloop = 200,fab=false)
    unit=10;
    # displayElementDeformation3D(Ue,unit)
    fabric=fab[1];
    discrete=fab[2];
    connect=fab[3];

    Macro_length = 1; Macro_width = 1; Macro_Height = 1;
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = 1; Macro_nely  = 1; Macro_nelz   = 1;
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Micro_Vol_FM    = Micro_struct[7];
    Micro_rmin=Micro_struct[8]

    Micro_move=Micro_struct[9]

    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; 
    Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;Micro_nele2D = Micro_nelx*Micro_nely;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];
    Macro_xPhys = ones(Macro_nely,Macro_nelx,Macro_nelz)
    E0 = 1; Emin = 1e-9; nu = 0.3;

    ###################################################################
    #library
    Umat=zeros(1,8*3)
    Umat[1,:].=Ue
    ################################################################################
    #### Full 3D

    Micro_nelx   = size3D; Micro_nely  = size3D; Micro_nelz   = size3D;
    Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;Micro_nele2D = Micro_nelx*Micro_nely;

    Micro_H,Micro_Hs = make_filter3D(Micro_nelx, Micro_nely, Micro_nelz, Micro_rmin);
    ss=2
    θ=1;
    Micro_x=[]
    Macro_masks=[]
    for ii=1:θ
        append!(Micro_x ,[Micro_Vol_FM.*ones(Micro_nely,Micro_nelx,Micro_nelz)]);
        # Micro_x[ii][Int(Micro_nely/2)-2:Int(Micro_nely/2)+2,Int(Micro_nelx/2)-2:Int(Micro_nelx/2)+2,Int(Micro_nelz/2)-2:Int(Micro_nelz/2)+2] .= 0;
        for i = 1:Micro_nelx
            for j = 1:Micro_nely 
                for k = 1:Micro_nelz
                    vall=3
                    if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2+(k-Micro_nelz/2-0.5)^2) < min(min(Micro_nelx,Micro_nely),Micro_nelz)/vall
                        Micro_x[ii][k,j,i] = Micro_Vol_FM/3.0;
                    end
                end
            end
        end
        if fabric
            Micro_x[ii].=addFabricationConstraints(ss,Emin,Micro_x[ii],Micro_nelx,Micro_nely,Micro_nelz,discrete)
        end
        append!(Macro_masks,[[1]])
    end
    beta = 1;

    Micro_xTilde = Micro_x;
    Micro_xPhys=[];
    Micro_dc = []
    Micro_dv = []
    Micro_change=[]
    DHs=[]
    dDHs=[]
    for i=1:θ
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde[i]) .+ Micro_xTilde[i] * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx, Micro_nelz)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx, Micro_nelz)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM3D(Micro_xPhys[i], Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end
    t=:darktest
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=cgrad(cschemeList)
    loopbeta = 0; loop = 0; Macro_change = 1;
    while loop < maxloop && ( Bool.(sum(Micro_change .>= 0.01)>=1))
        loop = loop+1; loopbeta = loopbeta+1;
        Threads.@threads for i=1:θ
            # if fabric
            #     Micro_x[i].=addFabricationConstraints(ss,Emin,Micro_x[i],Micro_nelx,Micro_nely,Micro_nelz,discrete)
            #     Micro_xPhys[i].=addFabricationConstraints(ss,Emin,Micro_xPhys[i],Micro_nelx,Micro_nely,Micro_nelz,discrete)
            # end
            # scene= GLMakie.volume(Micro_xPhys[i], algorithm = :iso, isorange = 0.2, isovalue = 0.8,colormap=:grays)

            DH, dDH = EBHM3D(Micro_xPhys[i], Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
            DHs[i]=DH;
            dDHs[i]=dDH;
        end

        
        for i=1:θ
            Micro_dc[i]=zeros(Micro_nely, Micro_nelx, Micro_nelz);
            Micro_dv[i]=ones(Micro_nely, Micro_nelx, Micro_nelz);
        end
        Micro_c=[]
        for ii=1:θ
            append!(Micro_c,[0])
            for i = 1:Micro_nele
                dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i] dDHs[ii][1,4][i] dDHs[ii][1,5][i] dDHs[ii][1,6][i];
                        dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i] dDHs[ii][2,4][i] dDHs[ii][2,5][i] dDHs[ii][2,6][i];
                        dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i] dDHs[ii][3,4][i] dDHs[ii][3,5][i] dDHs[ii][3,6][i];
                        dDHs[ii][4,1][i] dDHs[ii][4,2][i] dDHs[ii][4,3][i] dDHs[ii][4,4][i] dDHs[ii][4,5][i] dDHs[ii][4,6][i];
                        dDHs[ii][5,1][i] dDHs[ii][5,2][i] dDHs[ii][5,3][i] dDHs[ii][5,4][i] dDHs[ii][5,5][i] dDHs[ii][5,6][i];
                        dDHs[ii][6,1][i] dDHs[ii][6,2][i] dDHs[ii][6,3][i] dDHs[ii][6,4][i] dDHs[ii][6,5][i] dDHs[ii][6,6][i]];
                dKE = elementMatVec3D(Macro_Elex, Macro_Eley, Macro_Elez, dDHe);
                
                dce = sum((Umat[Bool.(Macro_masks[ii][:]),:]*dKE).*Umat[Bool.(Macro_masks[ii][:]),:],dims=2);
                Micro_c[ii]+=sum(dce)
                Micro_dc[ii][i] = -sum(sum(sum((Emin .+Macro_xPhys[Bool.(Macro_masks[ii][:])][:].^penal*(1-Emin)).*dce)));
            end
        end

        for i=1:θ
            Micro_dx = beta .*exp.(-beta .*Micro_xTilde[i]) .+exp.(-beta);
            if connect
                Q=DHs[i];dQ=dDHs[i];
                # shear
                c=-(DHs[i][4,4]+DHs[i][5,5]+DHs[i][6,6]);
                dc=-(dDHs[i][4,4]+dDHs[i][5,5]+dDHs[i][6,6]);
                # bulk
                # c = -(Q[1,1]+Q[2,2]+Q[3,3]+Q[1,2]+Q[2,3]+Q[1,3]);
                # dc = -(dQ[1,1]+dQ[2,2]+dQ[3,3]+dQ[1,2]+dQ[2,3]+dQ[1,3]);
                Micro_dc[i].+=dc .*5;

            end
            Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
            Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
            
        end

        for i=1:θ
            voxels=true;
            Micro_x[i], Micro_xPhys[i],Micro_xTilde[i], Micro_change1 = OC(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol_FM, Micro_nele, Micro_move, beta,voxels,fabric,ss);
            Micro_change[i]=Micro_change1;
            Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx,Micro_nelz);
            if fabric
                Micro_x[i].=addFabricationConstraints(ss,Emin,Micro_x[i],Micro_nelx,Micro_nely,Micro_nelz,discrete)
                Micro_xPhys[i].=addFabricationConstraints(ss,Emin,Micro_xPhys[i],Micro_nelx,Micro_nely,Micro_nelz,discrete)
            end
        end
        println("It:$loop Obj:$(Micro_c[1]) Vol:$(mean(Micro_xPhys[1][:])) ch:$(Micro_change[1]) ")
        if mod(loop,saveItr)==0
            
            for i=1:θ
                temp=copy(Micro_xPhys[i])
                # display(sum(Micro_xPhys[i]))
                temp[Micro_xPhys[i].<0.6].=0.0
                temp[Micro_xPhys[i].>=0.6].+=(i-1)
                # scene = Scene(resolution = (200, 200))
                scene= GLMakie.volume( permutedims(temp[:,end:-1:1,:], [2, 1, 3]),colorrange=(0.0, θ), algorithm = :iso,isorange = 0.3, isovalue = i,colormap=cscheme)
                display(scene)
                # save("./img/Micro_xPhys3U_3_$(i)_$(Micro_Vol[i])_$(Micro_rmin)_$(loop).png",scene)
            end


        end

    end

    xPhys3D1=Micro_xPhys[1]
    scene= GLMakie.volume( permutedims(xPhys3D1[:,end:-1:1,:], [2, 1, 3]), algorithm = :iso, isorange = 0.3, isovalue = 1.0,colormap=:grays)
    display(scene)   
    

    xPhys3D3D=xPhys3D1
    ################################################################################
    dens=getDens(xPhys3D1)
    display(dens)
    DH, dDH = EBHM3D(xPhys3D1, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
    dispFlag = 1;plotFlag = 1;outOption = "struct";
    props, SH = evaluateCH(DH, dens, outOption, dispFlag);
    fig=visual(DH);
    
    
    return xPhys3D3D     
end



###################################################################

function show25Micro(xPhys,iso1)

    scene= GLMakie.volume( permutedims(xPhys[:,end:-1:1,:], [2, 1, 3]), algorithm = :iso, isorange = iso1, isovalue = 1.0,colormap=:grays)
    display(scene) 
    Makie.inline!(true)

    Makie.inline!(false)

    scene= GLMakie.volume( permutedims(xPhys[:,end:-1:1,:], [2, 1, 3]), algorithm = :iso, isorange = iso1, isovalue = 1.0,colormap=:grays)
    display(scene) 
    Makie.inline!(true)
end

function getTriangle(xPhys)
    inds=findall(x-> x>=0, xPhys)
    xPhysnew=copy(xPhys)

    for ind in inds
        # display(ind)
        X=ind[1];Y=ind[2];Z=ind[3]*2;
        Z1=40-ind[3]*2;

        if min((21)-Z,min(Z-(0),min((21)-Y-((Z-(0))/((21)-(0)))*((21)-(0))/2,min(Y-(0)-((Z-(0))/((21)-(0)))*((21)-(0))/2,min(X-(0)-((Z-(0))/((21)-(0)))*((21)-(0))/2,(21)-X-((Z-(0))/((21)-(0)))*((21)-(0))/2))))) >=0
            # xPhysnew[ind]=2.0
        else
            xPhysnew[ind]=0
        end


    end
    Macro_xPhys_det=xPhysnew[:,end:-1:1,:]
    return Macro_xPhys_det
end

using Images

function show25DFace(xPhys,fileName="mmm") #show and export obj
    # show25DFace(xPhys,"1");
    # show25DFace(mapslices(rotr90,xPhys,dims=[1,3]),"2");
    # show25DFace(mapslices(rotl90,xPhys,dims=[1,3]),"3");
    # show25DFace(mapslices(rot180,xPhys,dims=[1,3]),"4");
    # show25DFace(mapslices(rotr90,xPhys,dims=[2,3]),"5");
    # show25DFace(mapslices(rotl90,xPhys,dims=[2,3]),"6");

    Makie.inline!(false)
    inds=findall(x-> x>=0, xPhys)
    xPhysnew=copy(xPhys)

    for ind in inds
        # display(ind)
        X=ind[1];Y=ind[2];Z=ind[3]*2;
        Z1=40-ind[3]*2;

        if min((21)-Z,min(Z-(0),min((21)-Y-((Z-(0))/((21)-(0)))*((21)-(0))/2,min(Y-(0)-((Z-(0))/((21)-(0)))*((21)-(0))/2,min(X-(0)-((Z-(0))/((21)-(0)))*((21)-(0))/2,(21)-X-((Z-(0))/((21)-(0)))*((21)-(0))/2))))) >=0
            # xPhysnew[ind]=2.0
        else
            xPhysnew[ind]=0
        end


    end
    Macro_xPhys_det=xPhysnew[:,end:-1:1,:]
    
    rmin=2
    factor=2
    size3D=size(Macro_xPhys_det)[1]*factor;
    
    
    Macro_xPhys_det=imresize(Macro_xPhys_det, (size3D,size3D,size3D))
    nely=size(Macro_xPhys_det)[1]
    nelx=size(Macro_xPhys_det)[2]
    nelz=size(Macro_xPhys_det)[3]
    
    
    Macro_H,Macro_Hs = make_filter3D(nelx, nely, nelz, rmin); 
    
    # Macro_xPhys_det[:]=Macro_H*(Macro_xPhys_det[:]./Macro_Hs)
    
    Makie.inline!(true)

    scene= GLMakie.volume( permutedims(Macro_xPhys_det, [2, 1, 3]),figure=(resolution=(300, 300), fontsize=10), 
        algorithm = :iso, isorange = 0.55, isovalue = 1.0,colormap=:grays)
    display(scene) 
    Makie.inline!(false)

    scene= GLMakie.volume( permutedims(Macro_xPhys_det, [2, 1, 3]),figure=(resolution=(300, 300), fontsize=10), 
        algorithm = :iso, isorange = 0.55, isovalue = 1.0,colormap=:grays)
    display(scene) 
    Makie.inline!(true)
    
    
    voxx=zeros(2+size(Macro_xPhys_det)[1],2+size(Macro_xPhys_det)[2],2+size(Macro_xPhys_det)[3])
    
    
    
    
    Macro_xPhys_detTemp=copy(Macro_xPhys_det)
    # Macro_xPhys_detTemp[Macro_xPhys_det.<0.6].=0.0
    # Macro_xPhys_detTemp[Macro_xPhys_det.>=0.6].=1.0
    voxx[2:end-1,2:end-1,2:end-1].=Macro_xPhys_detTemp
    points,faces = isosurface(1.0 .-voxx, MarchingCubes(iso=0.55),origin=SVector(-nely/2,-nelx/2,-nelz/2), widths = SVector(nely*1.0,nelx,nelz))
    pointss=reshape(collect(Iterators.flatten(points)),3,length(points))
    facess=reshape(collect(Iterators.flatten(faces)),3,length(faces))
    mm=TriMesh([pointss], [facess])
    Makie.inline!(true)
    display(Flux3D.visualize(mm))
    save_trimesh("./objs/3D/$(fileName).obj",mm)
    
    
    return Macro_xPhys_det
end
###################################################################