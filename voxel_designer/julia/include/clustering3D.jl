#======================================================================================================================#
# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021
#======================================================================================================================#

# divide macrostructure into θ parts


##############################################################
#3D Clustering for nearest neighbors in 2D strain library
function Uclustering3DStrainLibrary(Macro_struct, Micro_struct,prob,library,verbose=true)
    # USER-DEFINED LOOP PARAMETERS
    E0 = 1; Emin = 1e-9; nu = 0.3;penal=3;

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2]; Macro_Height = Macro_struct[3];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = Int(Macro_struct[4]); Macro_nely  = Int(Macro_struct[5]); Macro_nelz   = Int(Macro_struct[6]);
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Macro_Vol = Macro_struct[7][2]; 
    Macro_Vol_FM    = Macro_struct[7][1];
    Micro_Vol=Micro_Vol_FM    = Micro_struct[7][1];
    Micro_rmin=Micro_struct[8]
    Macro_rmin=Macro_struct[8]

    Macro_max_change=Macro_struct[9];Micro_max_change=Micro_struct[9]

    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;Micro_nele2D = Micro_nelx*Micro_nely;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];

    # if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    # end
    Macro_xPhys = ones(Macro_nely,Macro_nelx,Macro_nelz)
    if Macro_Vol<1.0
        Macro_xPhys,anim=topologyOptimization3d(Macro_nelx,Macro_nely,Macro_nelz,prob,Macro_Vol,Macro_rmin,penal,false)
    end
    scene= GLMakie.volume(permutedims(Macro_xPhys, [2, 1, 3]), algorithm = :iso, isorange = 0.4, isovalue = 1.0,colormap=:grays)
    display(scene)

    KE=lk_H8(nu)
    # PREPARE FINITE ELEMENT ANALYSIS
    U,F,freedofs=prob(Macro_nelx,Macro_nely,Macro_nelz);
    fixeddofs  = setdiff(1:Macro_ndof,freedofs)

    # edofMat = repeat(3 .*nodeids[:] .+1,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nelx+1)*(Macro_nely+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]], Macro_nele, 1);
    nodenrs = reshape(1:(1+Macro_nelx)*(1+Macro_nely)*(1+Macro_nelz),1+Macro_nely,1+Macro_nelx,1+Macro_nelz)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,Macro_nelx*Macro_nely*Macro_nelz,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nely+1)*(Macro_nelx+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]],Macro_nelx*Macro_nely*Macro_nelz,1)

    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*Macro_nele,1))

    
    
    ### IF MGCG
    if Macro_mgcg
        Macro_nl=4
        Macro_Pu=Array{Any}(undef, Macro_nl-1);
        for l = 1:Macro_nl-1
            Macro_Pu[l,1] = prepcoarse3(Macro_nelz/2^(l-1),Macro_nely/2^(l-1),Macro_nelx/2^(l-1));
        end
        fixeddofs  = setdiff(1:Macro_ndof,freedofs)
        Macro_N = ones(Macro_ndof); Macro_N[fixeddofs] .= 0;
        Macro_Null = spdiagm(Macro_ndof,Macro_ndof,0 => Macro_N);
    end
    ### IF MGCG
    if Macro_mgcg
        K = Array{Any}(undef, Macro_nl);
        sK = reshape(Kes.*(Emin.+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1)

        K[1,1] = sparse(vec(iK),vec(jK),vec(sK));
        K[1,1] = Macro_Null'*K[1,1]*Macro_Null - (Macro_Null .-sparse(1.0I, Macro_ndof, Macro_ndof)); 


        for l = 1:Macro_nl-1
            K[l+1,1] = Macro_Pu[l,1]'*(K[l,1]*Macro_Pu[l,1]);
        end

        Lfac = factorize(Matrix(Hermitian(K[Macro_nl,1]))).L ;Ufac = Lfac';
        cgtol=1e-10;
        cgmax=100;
        cgiters,cgres,U = MGCG(K,F[:,1],U[:,1],Lfac,Ufac,Macro_Pu,Macro_nl,1,cgtol,cgmax);

    else
        sK = reshape(KE[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;

        U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
    end
    ce = reshape(sum((U[edofMat]*KE).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz)
    c = sum(sum(sum((Emin.+Macro_xPhys.^penal*(E0-Emin)).*ce)))
    display("compliance before $c")

    #################################################
    E0 = 1; Emin = 1e-9; nu = 0.3;penal=3;
    #cuboct
    cuboct=load("./img/library/1/cuboct_vol_$((Micro_Vol)).jld")["data"];
    cuboctDH, dDH = EBHM2D(cuboct, Micro_length, Micro_width, E0, Emin, nu, penal);
    cuboct3D ,cuboctDH3=visualize3D2DMicrostructure(cuboct,false)
    Ke = elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, cuboctDH3);
    sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
    U[freedofs] = K[freedofs,freedofs]\Array(F[freedofs]);
    # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
    ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);
    c = sum(sum(sum((Emin .+Macro_xPhys.^penal*(1-Emin)).*ce)));
    display("cuboct compliance: $c") 
    ###########################################################
    # disY,disX,disZ,X,XX=reshapeU2DSlices(U[edofMat],Macro_nely, Macro_nelx, Macro_nelz);
    dissYX,dissZY,dissZX,X,Macro_xPhys_2D=reshapeU2DSlices_new(U[edofMat],Macro_nely, Macro_nelx, Macro_nelz,Macro_xPhys);

    # displayAllUPlotGrid(disY)#todo fix ratios
    # displayAllUPlotGrid(disX)#todo fix ratios
    # displayAllUPlotGrid(disZ)#todo fix ratios
    # display(Plots.heatmap(1.0.-Macro_xPhys[:,:,2],showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))
    ## principal strain
    order=[1,2,3,4,5,6,7,8];

    n=size(X)[1];
    Θ=zeros(n);ΘD=zeros(n);
    von_mises=zeros(n);
    ratio=zeros(n);
    for i=1:n
        Ue = X[i];
        Θ[i] , von_mises[i], ratio[i],α1,α2 =getPrincipalStrains(Ue);
    end
    ΘD=Θ.*180/π;

    #################
    

    θ=Int((num-1)*4+1) +1 #cuboct
    # display(θ)
    Micro_xPhys,DHs=loadVisualizeLibrary(library)
    Micro_x=Micro_xPhys
    # for i=1:θ
    #     Micro_x[i]=reshape(Flux.AdaptiveMaxPool((Micro_nelx, Micro_nelx))(Micro_xPhys[i]),Micro_nelx,Micro_nelx)
    # end
    # Micro_xPhys=Micro_x
    #################

    ##Clustering here
    #can remove scheme
    # t=:dark
    t=:darktest
    cscheme=cgrad(t, θ-1, categorical = true)
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ-1
        append!(cschemeList,[cscheme[i]])
    end
    append!(cschemeList,[:black])
    cscheme1=cgrad(cschemeList)

    function findNearestList(theta)
        if theta<0
            theta+=180
        elseif theta>180
            theta-=180
        end
        findnearest(A::AbstractArray,t) = findmin(abs.(A.-t))[2]
        values=[]
        for i in range(0.0, 180, length=θ-1)
            append!(values,[i])
        end
        return values[findnearest(values,theta)]
    end
    function findNearestListIndex(theta)
        if theta<0
            theta+=180
        elseif theta>180
            theta-=180
        end
        findnearest(A::AbstractArray,t) = findmin(abs.(A.-t))[2]
        values=[]
        for i in range(0.0, 180, length=θ-1)
            append!(values,[i])
        end
        return findnearest(values,theta)
    end
    # display(minimum(ΘD))
    # display(maximum(ΘD))
    ΘDNearest=findNearestList.(ΘD)
    Uclustered=findNearestListIndex.(ΘD)
    a=Uclustered


    for i=1:n
        Ue = X[i];
        if sum(Ue)==0 || ratio[i]>0.99 ||ratio[i]<-0.99
            a[i]=θ #if no load cuboct
        end
    end
    ######
    aYX,aZY,aZX=reshape_back_reshapeU2DSlices_new(θ,a,Macro_nely, Macro_nelx, Macro_nelz,true)
     


    ##################################################################
    #for each element assemble the Micro_xPhys and add to the KES
    MasksTemp=[]
    for i=1:θ
        append!(MasksTemp,[[]])
    end
    Kes=zeros(24*24,Macro_nely,Macro_nelx,Macro_nelz)
        
    DictEBHM = Dict();
    DictxPhys3D1 = Dict();
    xPhys3D1s1=[]
    # EBHM3Ds=[]
    ii=0;
    thickness=1;
    size3D=20;
    inds=findall(Macro_xPhys.>-100)
    m=@timed begin
        for i = 1:length(inds)
            yy=inds[i][1];xx=inds[i][2];zz=inds[i][3];
            θTempList="";DH="";
            xPhys3D=zeros(Micro_nely,Micro_nelx,Micro_nelz);
            θTemp=Int(aYX[Int((zz-1)*2+1)][Int(yy),Int(xx)]) ;xPhys3D[:,:,1:thickness]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
            θTemp=Int(aYX[Int((zz-1)*2+2)][Int(yy),Int(xx)]) ;xPhys3D[:,:,end-thickness+1:end]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
            θTemp=Int(aZY[Int((xx-1)*2+1)][Int(zz),Int(yy)]) ;xPhys3D[:,1:thickness,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
            θTemp=Int(aZY[Int((xx-1)*2+2)][Int(zz),Int(yy)]) ;xPhys3D[:,end-thickness+1:end,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
            θTemp=Int(aZX[Int((yy-1)*2+1)][Int(zz),Int(xx)]) ;xPhys3D[1:thickness,:,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
            θTemp=Int(aZX[Int((yy-1)*2+2)][Int(zz),Int(xx)]) ;xPhys3D[end-thickness+1:end,:,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
            if haskey(DictxPhys3D1, θTempList)
            else
                xPhys3D1=reshape(xPhys3D,Micro_nely,Micro_nelx,Micro_nelz,1,1)
                xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
                DictxPhys3D1[θTempList]=ii+1
                append!(xPhys3D1s1,[xPhys3D1])
                # append!(EBHM3Ds,[zeros(6,6)])
                ii+=1
            end    
        end
        display("There are $ii unique 3d microstructures.")
        unique=ii
        EBHM3Ds=zeros(unique,6,6)
        xPhys3D1s=zeros(unique,size3D,size3D,size3D)
        for i in 1:unique
            xPhys3D1s[i,:,:,:].=xPhys3D1s1[i]
        end
        # tprintln("counter:")
        # Threads.@threads 
        println("")
        print("counter:")
        ii=1
        Threads.@threads for i in 1:unique
            # θTempList=item.first
            xPhys3D1=xPhys3D1s[i,:,:,:]
            DH, dDH = EBHM3D(xPhys3D1, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
            EBHM3Ds[i,:,:].=DH
            if Threads.nthreads()==1
                print(" $ii,")
                ii+=1
            end
            # tprintln(" $ii,")
        end
        println("done!")
    end
    m1=@timed begin
        for i = 1:length(inds)
            yy=inds[i][1];xx=inds[i][2];zz=inds[i][3];
            θTempList="";DH="";
            xPhys3D=zeros(Micro_nely,Micro_nelx,Micro_nelz);
            θTemp=Int(aYX[Int((zz-1)*2+1)][Int(yy),Int(xx)]) ;θTempList*=string(θTemp);
            θTemp=Int(aYX[Int((zz-1)*2+2)][Int(yy),Int(xx)]) ;θTempList*=string(θTemp);
            θTemp=Int(aZY[Int((xx-1)*2+1)][Int(zz),Int(yy)]) ;θTempList*=string(θTemp);
            θTemp=Int(aZY[Int((xx-1)*2+2)][Int(zz),Int(yy)]) ;θTempList*=string(θTemp);
            θTemp=Int(aZX[Int((yy-1)*2+1)][Int(zz),Int(xx)]) ;θTempList*=string(θTemp);
            θTemp=Int(aZX[Int((yy-1)*2+2)][Int(zz),Int(xx)]) ;θTempList*=string(θTemp);
            
            DH=EBHM3Ds[DictxPhys3D1[θTempList],:,:]
            Kes[:,yy,xx,zz].=elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH)[:]
        end
    end
    # tprintln("it took $(m[2]) seconds to process $ii unique 3d microstructures.")
    display("it took $(m[2]) seconds and $(m1[2]) seconds (total $(m[2]+m1[2]) seconds) to process $ii unique 3d microstructures on $(Threads.nthreads()) cores.")
    
    # m=@timed begin
        # for i = 1:length(inds)
        # Threads.@threads for i = 1:length(inds)
    # for yy=1:Macro_nely
    #     for xx=1:Macro_nelx
    #         for zz=1:Macro_nelz
                # yy=inds[i][1];xx=inds[i][2];zz=inds[i][3];
                # θTempList="";DH="";
                # xPhys3D=zeros(Micro_nely,Micro_nelx,Micro_nelz);
                # θTemp=Int(aYX[Int((zz-1)*2+1)][Int(yy),Int(xx)]) ;xPhys3D[:,:,1:thickness]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                # θTemp=Int(aYX[Int((zz-1)*2+2)][Int(yy),Int(xx)]) ;xPhys3D[:,:,end-thickness+1:end]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                # θTemp=Int(aYZ[Int((xx-1)*2+1)][Int(yy),Int(zz)]) ;xPhys3D[:,1:thickness,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                # θTemp=Int(aYZ[Int((xx-1)*2+2)][Int(yy),Int(zz)]) ;xPhys3D[:,end-thickness+1:end,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                # θTemp=Int(aXZ[Int((yy-1)*2+1)][Int(xx),Int(zz)]) ;xPhys3D[1:thickness,:,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                # θTemp=Int(aXZ[Int((yy-1)*2+2)][Int(xx),Int(zz)]) ;xPhys3D[end-thickness+1:end,:,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                
                # if haskey(DictEBHM, θTempList)
                #     DH=DictEBHM[θTempList]
                # else
                #     xPhys3D1=reshape(xPhys3D,Micro_nely,Micro_nelx,Micro_nelz,1,1)
                #     xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
                #     # xPhys3D1=reshape(Flux.AdaptiveMeanPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
                #     DH, dDH = EBHM3D(xPhys3D1, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
                #     DictEBHM[θTempList]=DH
                #     tprintln(" $i,")
                #     i+=1
                    
                # end

                # Kes[:,yy,xx,zz].=elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH)[:]
    #         end
    #     end
    # end
    #     end
    # end
    # tprintln("it took $(m[2]) seconds to process $i unique 3d microstructures.")


    Kes=reshape(Kes,24*24,Macro_nely*Macro_nelx*Macro_nelz);

    ### IF MGCG
    if Macro_mgcg
        K = Array{Any}(undef, Macro_nl);
        sK = reshape(Kes.*(Emin.+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1)

        K[1,1] = sparse(vec(iK),vec(jK),vec(sK));
        K[1,1] = Macro_Null'*K[1,1]*Macro_Null - (Macro_Null .-sparse(1.0I, Macro_ndof, Macro_ndof)); 


        for l = 1:Macro_nl-1
            K[l+1,1] = Macro_Pu[l,1]'*(K[l,1]*Macro_Pu[l,1]);
        end

        Lfac = factorize(Matrix(Hermitian(K[Macro_nl,1]))).L ;Ufac = Lfac';
        cgtol=1e-10;
        cgmax=100;
        cgiters,cgres,U = MGCG(K,F[:,1],U[:,1],Lfac,Ufac,Macro_Pu,Macro_nl,1,cgtol,cgmax);

    else
        sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;

        U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);

    end

    UU=mapslices(x->[x]'[:], U[edofMat], dims=2)[:]
    KKK=mapslices(x->[reshape(x,24,24)], Kes, dims=1)[:]

    ce=sum(vcat((UU.*KKK)...).*U[edofMat],dims=2)
    ce=reshape(ce,Macro_nely,Macro_nelx,Macro_nelz)

    c = sum(sum(sum((Emin .+Macro_xPhys.^penal*(1-Emin)).*ce)));
    display("Final compliance: $c")

    return Macro_xPhys,Micro_xPhys,DHs,a,aYX,aZY,aZX

end

#based on direction not U
function Uclustering3D_new(θ,Macro_struct, Micro_struct,prob)
    
    penal=3.0;Emin=1e-9;nu=0.3;E0=1;
    

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2]; Macro_Height = Macro_struct[3];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = Int(Macro_struct[4]); Macro_nely  = Int(Macro_struct[5]); Macro_nelz   = Int(Macro_struct[6]);
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Macro_Vol = Macro_struct[7][2]; 
    Macro_Vol_FM    = Macro_struct[7][1];
    Micro_Vol=Micro_Vol_FM    = Micro_struct[7][1];
    Micro_rmin=Micro_struct[8]
    Macro_rmin=Macro_struct[8]

    Macro_max_change=Macro_struct[9];Micro_max_change=Micro_struct[9]

    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;Micro_nele2D = Micro_nelx*Micro_nely;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];
    

    
    # if length(voxels)==1
    voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    # end
    Macro_xPhys = ones(Macro_nely,Macro_nelx,Macro_nelz)
    if Macro_Vol<1.0
        Macro_xPhys,anim=topologyOptimization3d(Macro_nelx,Macro_nely,Macro_nelz,prob,Macro_Vol,Macro_rmin,penal,false)
    end
    scene= GLMakie.volume(permutedims(Macro_xPhys, [2, 1, 3]), algorithm = :iso, isorange = 0.4, isovalue = 1.0,colormap=:grays)
    display(scene)

    KE=lk_H8(nu)
    # PREPARE FINITE ELEMENT ANALYSIS
    U,F,freedofs=prob(Macro_nelx,Macro_nely,Macro_nelz);
    fixeddofs  = setdiff(1:Macro_ndof,freedofs)

    # edofMat = repeat(3 .*nodeids[:] .+1,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nelx+1)*(Macro_nely+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]], Macro_nele, 1);
    nodenrs = reshape(1:(1+Macro_nelx)*(1+Macro_nely)*(1+Macro_nelz),1+Macro_nely,1+Macro_nelx,1+Macro_nelz)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,Macro_nelx*Macro_nely*Macro_nelz,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nely+1)*(Macro_nelx+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]],Macro_nelx*Macro_nely*Macro_nelz,1)

    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*Macro_nele,1))

    ### IF MGCG
    if Macro_mgcg
        Macro_nl=4
        Macro_Pu=Array{Any}(undef, Macro_nl-1);
        for l = 1:Macro_nl-1
            Macro_Pu[l,1] = prepcoarse3(Macro_nelz/2^(l-1),Macro_nely/2^(l-1),Macro_nelx/2^(l-1));
        end
        fixeddofs  = setdiff(1:Macro_ndof,freedofs)
        Macro_N = ones(Macro_ndof); Macro_N[fixeddofs] .= 0;
        Macro_Null = spdiagm(Macro_ndof,Macro_ndof,0 => Macro_N);
    end
    ### IF MGCG
    if Macro_mgcg
        K = Array{Any}(undef, Macro_nl);
        sK = reshape(Kes.*(Emin.+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1)

        K[1,1] = sparse(vec(iK),vec(jK),vec(sK));
        K[1,1] = Macro_Null'*K[1,1]*Macro_Null - (Macro_Null .-sparse(1.0I, Macro_ndof, Macro_ndof)); 


        for l = 1:Macro_nl-1
            K[l+1,1] = Macro_Pu[l,1]'*(K[l,1]*Macro_Pu[l,1]);
        end

        Lfac = factorize(Matrix(Hermitian(K[Macro_nl,1]))).L ;Ufac = Lfac';
        cgtol=1e-10;
        cgmax=100;
        cgiters,cgres,U = MGCG(K,F[:,1],U[:,1],Lfac,Ufac,Macro_Pu,Macro_nl,1,cgtol,cgmax);

    else
        sK = reshape(KE[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;

        U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
    end
    ce = reshape(sum((U[edofMat]*KE).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz)
    c = sum(sum(sum((Emin.+Macro_xPhys.^penal*(E0-Emin)).*ce)))
    display("compliance before $c")

    ##########clustering#########


    Umat=U[edofMat]; 
    ord=reshape(1:(Macro_nely*Macro_nelx*Macro_nelz),Macro_nely,Macro_nelx,Macro_nelz);
    
    #todo plot strain directions
    unit1=10
    displayDeformation3D(Umat,Macro_nely,Macro_nelx,Macro_nelz,unit1,Macro_xPhys)

    # diss=reshape(U_edofMat,Macro_nely, Macro_nelx, Macro_nelz,24);

    ny=Macro_nely;nx=Macro_nelx;nz=Macro_nelz;
    Θxy=zeros(ny ,nx, nz);ΘxyD=zeros(ny ,nx, nz);
    Θz=zeros(ny ,nx, nz);ΘzD=zeros(ny ,nx, nz);
    von_mises=zeros(ny , nx, nz);
    ratio1=zeros(ny , nx, nz);ratio2=zeros(ny , nx, nz);

    xs=ones(nz*ny*nx);ys=ones(nz*ny*nx);zs=ones(nz*ny*nx);vonmises=ones(nz*ny*nx);
    count=1;

    for k in 1:nz
        for i in 1:ny
            for j in 1:nx
                if Macro_xPhys[i,j,k]>0.6
                    # Ue = diss[i,j,k,:];
                    Ue=Umat[ord[i,j,k],:];
                    Θxy[i,j,k],Θz[i,j,k],von_mises[i,j,k],ratio1[i,j,k],ratio2[i,j,k],E,T =getPrincipalStrains3D(Ue);
                    xs[count]=i;ys[count]=j;zs[count]=k;
                    vonmises[count]=von_mises[i,j,k];count+=1;
                end
            end
        end
    end
    ΘxyD=Θxy.*180/π;
    ΘzD=Θz.*180/π;

    

    
    #plot vonmises
    Makie.contour(von_mises, alpha=0.4)
    display(current_figure())

    rect = Rect(Vec(0.0, 0.0,0.0), Vec(1.0, 1.0,1.0))
    mesh = GeometryBasics.mesh(rect)
    Makie.meshscatter(nx.-ys .+1, ny.-xs .+1, zs, markersize = 1, marker=mesh,color = vonmises,
        transparency=false,opacity=0.8,shading = true,shininess=32.0
        ,axis = (; type = Axis3, protrusions = (0, 0, 0, 0),
        viewmode = :fit,aspect=:data, limits = (1, nx+1, 1, ny+1, 1, nz+1)))
    
    
    Makie.inline!(true)   
    display(current_figure())
    Makie.inline!(false)   
    display(current_figure())
    # omg=ommh
    # X=U[edofMat]'
    # X=hcat(Macro_xPhys[:],U[edofMat])'
    wd=2;
    X=hcat(sin.(2*Θxy[:]).*Macro_xPhys[:],cos.(2*Θxy[:]).*Macro_xPhys[:],sin.(2*Θz[:]).*Macro_xPhys[:],cos.(2*Θz[:]).*Macro_xPhys[:],wd.*Macro_xPhys[:])'
    

    # for i=1:3
    #     # display(i)
    #     # XXX=reshape(mean(U[edofMat]',dims=1),Macro_nely,Macro_nelx,Macro_nelz)
    #     # XXX=reshape(sum(U[edofMat]',dims=1),Macro_nely,Macro_nelx,Macro_nelz)
    #     XXX=reshape(U[edofMat]'[i,:],Macro_nely,Macro_nelx,Macro_nelz)
    #     # display(minimum(XXX))
    #     # display(maximum(XXX))
    #     # scene = Scene(resolution = (200, 200))
    #     scene= GLMakie.volume(permutedims(XXX, [2, 1, 3]), algorithm = :mip,transparency=true,colorrange=(minimum(XXX), maximum(XXX)),colormap=:lighttest)
    #     display(scene)
    #     # save("./img/FM3DU_$(Macro_Vol).png",scene)
    # end
    
    


    # # cluster X into θ clusters using K-means
    # # R = kmeans(X, θ; maxiter=200, display=:iter)
    # R = Clustering.kmeans(X, θ; maxiter=500)

    # @assert Clustering.nclusters(R) == θ # verify the number of clusters

    # a = Clustering.assignments(R) # get the assignments of points to clusters
    # c = Clustering.counts(R) # get the cluster sizes
    # M = R.centers; # get the cluster centers

    # D=Distances.pairwise(Euclidean(), M, dims=2)
    # hc = hclust(D, linkage=:single)
    # display(Plots.plot(hc))

    data=X
    D=Distances.pairwise(Euclidean(), data, dims=2)
    hc = hclust(D, linkage=:ward_presquared) # hc = hclust(D, linkage=:single,branchorder=:optimal)
    display(Plots.plot(hc))

    gr(size=(400,100))
    #for elbow curve
    hcElbow=reverse(hc.heights)[1:20]
    display(Plots.plot(1:(length(hcElbow)),hcElbow,label=""))
    hcElbow=reverse(hc.heights)[2:20]
    xy=hcat(1:(length(hcElbow)),hcElbow)
    mapp=mapproject(xy, dist2line=(line=[xy[1:1, :]; xy[end:end, :]], unit=:cartesian))
    display(Plots.plot(mapp[:,3],label=""))
    nclust=(findmax(mapp[:,3])[2]+1)
    display("Best Number of clusters is= $nclust")
    gr(size=(400,300))
    nclust=θ
    a=cutree(hc; k=nclust)
    #visualize tree with only θ clusters
    centers=zeros(size(data)[1],nclust)
    for i in 1:nclust
        R = Clustering.kmeans(data[:,a.==i], 1); # run K-means just to get the center
        M = R.centers; # get the cluster centers
        centers[:,i].=M[:]
    end
    D=Distances.pairwise(Euclidean(), centers, dims=2)
    hc = hclust(D, linkage=:ward_presquared,branchorder=:optimal) # hc = hclust(D, linkage=:single,branchorder=:optimal)
    display(Plots.plot(hc))


    Uclustered=reshape(a,Macro_nely,Macro_nelx,Macro_nelz);

    #############################
    Macro_masksU=[]
    Macro_masks_densU=[]

    t=:darktest
    cscheme=cgrad(t, θ, categorical = true)
    cschemeList=[]
    for i=1:θ
        append!(cschemeList,[cscheme[i]])
    end
    cscheme=cgrad(cschemeList)


    Macro_xPhys_diagU=copy(Macro_xPhys)
    d=1/θ
    for i in 1:θ
        Macro_xPhys_temp=copy(Macro_xPhys)
        out= (Uclustered.==i)

        Macro_xPhys_temp[out].=true
        Macro_xPhys_temp[.!out].=false

        dens=Micro_Vol


        println("Mask $i with density $(round(dens,digits=2)) occupies $(round(sum(out)/length(Macro_xPhys)*100,digits=2))% of the macrostructure.")

        Macro_xPhys_diagU[out].= i

        append!(Macro_masks_densU,[dens])
        append!(Macro_masksU,[Macro_xPhys_temp])
        # display(heatmap(Macro_xPhys_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
    end


    # scene = Scene(resolution = (400, 400))
    scene= GLMakie.volume(permutedims(Macro_xPhys_diagU, [2, 1, 3]), algorithm = :absorption,transparency=true,colorrange=(1, θ),colormap=cscheme)
    display(scene)
    # save("./img/FM3DU1_$(Macro_Vol).png",scene)
    
    # scene = Scene(resolution = (400, 400))#todo fix
    clu=ones(nz*ny*nx);
    count=1;
    for k in 1:nz
        for i in 1:ny
            for j in 1:nx
                if Macro_xPhys[i,j,k]>0.6
                    xs[count]=i;ys[count]=j;zs[count]=k;
                    clu[count]=Macro_xPhys_diagU[i,j,k];count+=1;
                end
            end
        end
    end
    

    Makie.contour(Macro_xPhys_diagU, alpha=0.5,colormap=cscheme,colorrange=(1, θ))
    display(current_figure())



    # Makie.inline!(true)
    # Makie.volume(permutedims(Macro_xPhys_diagU, [2, 1, 3]) , algorithm = :iso,colorrange=(1, θ),isorange = 1.0, isovalue = 1,colormap=cscheme)
    # display(current_figure())

    rect = Rect(Vec(0.0, 0.0,0.0), Vec(1.0, 1.0,1.0))
    mesh = GeometryBasics.mesh(rect)
    Makie.meshscatter(nx.-ys .+1, ny.-xs .+1, zs, markersize = 1, marker=mesh,color = clu,
        transparency=false,opacity=0.8,shading = true,shininess=32.0,colormap=cscheme,colorrange=(1, θ)
        ,axis = (; type = Axis3, protrusions = (0, 0, 0, 0),
        viewmode = :fit,aspect=:data, limits = (1, nx+1, 1, ny+1, 1, nz+1)))

    Makie.inline!(true)   
    display(current_figure())
    Makie.inline!(false)   
    display(current_figure())
    Makie.inline!(true)  
    
    t=:darktest
    cscheme=cgrad(t, θ, categorical = true)
    cschemeList=[]
    for i=1:θ
        append!(cschemeList,[cscheme[i]])
    end
    cscheme11=cgrad(cschemeList)

    Macro_xPhys_diagUUU=copy(Macro_xPhys_diagU)
    Macro_xPhys_diagUUU[Macro_xPhys.<0.6].=NaN
    fig = Figure(resolution=(400*ny, 400), fontsize=10)
    for i in 1:ny
        ax1=Makie.Axis(fig[1, i])
        ax1.aspect = DataAspect()

        Makie.heatmap!(ax1,Macro_xPhys_diagUUU[i,:,:],colormap=cscheme11,colorrange=(1, θ))
    end
    display(fig)
    fig = Figure(resolution=(400*nx, 400), fontsize=10)
    for i in 1:nx
        ax1=Makie.Axis(fig[1, i])
        ax1.aspect = DataAspect()

        Makie.heatmap!(ax1,Macro_xPhys_diagUUU[:,i,:]',colormap=cscheme11,colorrange=(1, θ))
    end
    display(fig)
    fig = Figure(resolution=(400*nz, 400), fontsize=10)
    for i in 1:nz
        ax1=Makie.Axis(fig[1, i])
        ax1.aspect = DataAspect()

        Makie.heatmap!(ax1,Macro_xPhys_diagUUU[:,:,i]',colormap=cscheme11,colorrange=(1, θ))
    end
    display(fig)

    # omg=ommh

    
    # for i=2:θ
    #     Makie.volume!(permutedims(Macro_xPhys_diagU, [2, 1, 3]) , algorithm = :iso,colorrange=(1, θ),isorange = 1.0, isovalue = i,colormap=cscheme)
    # end
    # display(current_figure())
    # scene= volume(Macro_xPhys_diagU, algorithm = :mip,transparency=false,colorrange=(0.0, 1.0),colormap=:lighttest)
    # display(scene)
    # save("./img/FM3DFU_$(Macro_Vol).png",scene)

    return Macro_masksU,Macro_masks_densU,Macro_xPhys_diagU,Macro_xPhys
end
##############################################################
# divide macrostructure into θ parts
function freeMaterial3D(θ,Macro_nely,Macro_nelx,Macro_nelz,Macro_nele,Macro_Vol,iK,jK,Emin,U,F,freedofs,edofMat,voxels=true)
    if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    end
    maxloop = 90; displayflag = 1; E0 = 1; nu = 0.3;

    Ke=lk_H8(nu)
    
    Macro_x = ones(Macro_nely,Macro_nelx,Macro_nelz).*Macro_Vol;
    Macro_xTilde = Macro_x; 
    Macro_xPhys = Macro_x; 

    loopbeta = 0; loop = 0; Macro_change = 1;
    ## free material
    penal1=1;

    while loop < maxloop && Macro_change > 0.01 
        loop = loop+1;


        # FE-ANALYSIS AT TWO SCALES
        sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal1*(1 .-Emin)),24*24*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
        U[freedofs] = K[freedofs,freedofs]\Array(F[freedofs]);
        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);
        c = sum(sum(sum((Emin .+Macro_xPhys.^penal1*(1-Emin)).*ce)));

        Macro_dc = -penal1*(1-Emin)*Macro_xPhys.^(penal1-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx, Macro_nelz);


        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys, Macro_change = OC_reg3(Macro_x, Macro_dc, Macro_dv, Macro_Vol, Macro_nele, 0.2);

        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx, Macro_nelz);
        # PRINT RESULTS
        # println(" FMD It.:$loop Obj.:$(round.(c,digits=2)) Macro_Vol.:$(round.(mean(Macro_xPhys[:] ),digits=2)) Macro_ch.:$(round.(Macro_change,digits=2))")

    end

    # display(heatmap(Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=jet,clims=(0.0, 1.0),aspect_ratio=:equal))
    scene= volume(Macro_xPhys, algorithm = :mip,transparency=true,colorrange=(0.0, 1.0),colormap=:lighttest)
    display(scene)
    # save("./img/FM3D_$(Macro_Vol).png",scene)
    Macro_masks=[]
    Macro_masks_dens=[]

    Macro_xPhys_diag=copy(Macro_xPhys)
    d=1/θ
    for i in 1:θ
        Macro_xPhys_temp=copy(Macro_xPhys)
        out= (Macro_xPhys.> (i*d-d)) .& (Macro_xPhys.<= (i*d))
        println("Mask $i from $(round((i*d-d),digits=2)) to $(round(i*d,digits=2)) occupies $(round(sum(out)/length(Macro_xPhys)*100,digits=2))% of the macrostructure.")
        Macro_xPhys_temp[out].=false
        Macro_xPhys_temp[.!out].=true
        dens=(i*d-d/2)+Emin
        if i==1
            dens=Emin
        elseif i==θ
            dens=1-Emin
        end
        Macro_xPhys_diag[out].= dens
        append!(Macro_masks_dens,[dens])
        append!(Macro_masks,[Macro_xPhys_temp])
        # display(heatmap(Macro_xPhys_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
    end

    # display(Macro_masks_dens)
    # display(heatmap(Macro_xPhys_diag,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, 1.0),fc=jet,aspect_ratio=:equal))
    scene= volume(Macro_xPhys_diag, algorithm = :mip,transparency=false,colorrange=(0.0, 1.0),colormap=:lighttest)
    display(scene)
    # save("./img/FM3DF_$(Macro_Vol).png",scene)

    return Macro_masks,Macro_masks_dens,Macro_xPhys_diag
end

function Uclustering3D(θ,Macro_nely,Macro_nelx,Macro_nelz,Macro_nele,Macro_Vol,Micro_Vol,iK,jK,Emin,U,F,freedofs,edofMat,voxels=true)
    if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    end
    maxloop = 90;
    E0 = 1; nu = 0.3;

    Ke=lk_H8(nu)
    
    Macro_x = ones(Macro_nely,Macro_nelx,Macro_nelz).*Macro_Vol;
    Macro_xTilde = Macro_x; 
    Macro_xPhys = Macro_x; 

    loopbeta = 0; loop = 0; Macro_change = 1;
    ## free material
    penal1=1;

    while loop < maxloop && Macro_change > 0.01 
        loop = loop+1;



        # FE-ANALYSIS AT TWO SCALES
        sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal1*(1 .-Emin)),24*24*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
        U[freedofs] = K[freedofs,freedofs]\Array(F[freedofs]);
        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);
        c = sum(sum(sum((Emin .+Macro_xPhys.^penal1*(1-Emin)).*ce)));

        Macro_dc = -penal1*(1-Emin)*Macro_xPhys.^(penal1-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx, Macro_nelz);


        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys, Macro_change = OC_reg3(Macro_x, Macro_dc, Macro_dv, Macro_Vol, Macro_nele, 0.2);

        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx, Macro_nelz);
        # PRINT RESULTS
        # println(" FMD It.:$loop Obj.:$(round.(c,digits=2)) Macro_Vol.:$(round.(mean(Macro_xPhys[:] ),digits=2)) Macro_ch.:$(round.(Macro_change,digits=2))")

    end

    

    ##########clustering#########

    # display(heatmap(Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=jet,clims=(0.0, 1.0),aspect_ratio=:equal))
    # scene = Scene(resolution = (200, 200))
    
    scene= GLMakie.volume(permutedims(Macro_xPhys, [2, 1, 3]), algorithm = :mip,transparency=true,colorrange=(0.0, 1.0),colormap=:lighttest)
    display(scene)
    # save("./img/FM3DUC_$(Macro_Vol).png",scene)

    # X=U[edofMat]'
    X=hcat(Macro_xPhys[:],U[edofMat])'

    # for i=1:3
    #     # display(i)
    #     # XXX=reshape(mean(U[edofMat]',dims=1),Macro_nely,Macro_nelx,Macro_nelz)
    #     # XXX=reshape(sum(U[edofMat]',dims=1),Macro_nely,Macro_nelx,Macro_nelz)
    #     XXX=reshape(U[edofMat]'[i,:],Macro_nely,Macro_nelx,Macro_nelz)
    #     # display(minimum(XXX))
    #     # display(maximum(XXX))
    #     # scene = Scene(resolution = (200, 200))
    #     scene= GLMakie.volume(permutedims(XXX, [2, 1, 3]), algorithm = :mip,transparency=true,colorrange=(minimum(XXX), maximum(XXX)),colormap=:lighttest)
    #     display(scene)
    #     # save("./img/FM3DU_$(Macro_Vol).png",scene)
    # end
    
    


    # cluster X into θ clusters using K-means
    # R = kmeans(X, θ; maxiter=200, display=:iter)
    R = Clustering.kmeans(X, θ; maxiter=500)

    @assert Clustering.nclusters(R) == θ # verify the number of clusters

    a = Clustering.assignments(R) # get the assignments of points to clusters
    c = Clustering.counts(R) # get the cluster sizes
    M = R.centers; # get the cluster centers

    D=Distances.pairwise(Euclidean(), M, dims=2)
    hc = hclust(D, linkage=:single)
    display(Plots.plot(hc))

    Uclustered=reshape(a,Macro_nely,Macro_nelx,Macro_nelz);

    #############################
    Macro_masksU=[]
    Macro_masks_densU=[]

    t=:darktest #t=:dark
    # theme(t);
    cschemeList=[]
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=cgrad(cschemeList)


    Macro_xPhys_diagU=copy(Macro_xPhys)
    d=1/θ
    for i in 1:θ
        Macro_xPhys_temp=copy(Macro_xPhys)
        out= (Uclustered.==i)

        Macro_xPhys_temp[out].=true
        Macro_xPhys_temp[.!out].=false

        dens=Micro_Vol


        println("Mask $i with density $(round(dens,digits=2)) occupies $(round(sum(out)/length(Macro_xPhys)*100,digits=2))% of the macrostructure.")

        Macro_xPhys_diagU[out].= i

        append!(Macro_masks_densU,[dens])
        append!(Macro_masksU,[Macro_xPhys_temp])
        # display(heatmap(Macro_xPhys_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
    end


    # scene = Scene(resolution = (400, 400))
    scene= GLMakie.volume(permutedims(Macro_xPhys_diagU, [2, 1, 3]), algorithm = :absorption,transparency=true,colorrange=(1, θ),colormap=cscheme)
    display(scene)
    # save("./img/FM3DU1_$(Macro_Vol).png",scene)
    
    # scene = Scene(resolution = (400, 400))#todo fix
    
    Makie.inline!(true)
    Makie.volume(permutedims(Macro_xPhys_diagU, [2, 1, 3]) , algorithm = :iso,colorrange=(1, θ),isorange = 1.0, isovalue = 1,colormap=cscheme)
    
    for i=2:θ
        Makie.volume!(permutedims(Macro_xPhys_diagU, [2, 1, 3]) , algorithm = :iso,colorrange=(1, θ),isorange = 1.0, isovalue = i,colormap=cscheme)
    end
    display(current_figure())
    # scene= volume(Macro_xPhys_diagU, algorithm = :mip,transparency=false,colorrange=(0.0, 1.0),colormap=:lighttest)
    display(scene)
    # save("./img/FM3DFU_$(Macro_Vol).png",scene)

    return Macro_masksU,Macro_masks_densU,Macro_xPhys_diagU
end

function CompliantUclustering3D(θ,Macro_nely,Macro_nelx,Macro_nelz,Macro_nele,Macro_Vol,Micro_Vol,iK,jK,Emin,U,F,freedofs,edofMat,din,dout,voxels=true)
    if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    end
    maxloop = 90; E0 = 1; nu = 0.3;

    Ke=lk_H8(nu)
    
    Macro_x = ones(Macro_nely,Macro_nelx,Macro_nelz).*Macro_Vol;
    Macro_xTilde = Macro_x; 
    Macro_xPhys = Macro_x; 

    loopbeta = 0; loop = 0; Macro_change = 1;
    ## free material
    penal1=1;

    while loop < maxloop && Macro_change > 0.01 
        loop = loop+1;


        # FE-ANALYSIS AT TWO SCALES
        sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal1*(1 .-Emin)),24*24*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); 
        K[din,din] = K[din,din] + 0.1
        K[dout,dout] = K[dout,dout] + 0.1
        K = (K+K')/2;
        U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
        U1 = U[:,1]
        U2 = U[:,2]
        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        # ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);
        ce = reshape(-sum((U1[edofMat]*Ke).*U2[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);

        c  = U[dout,1]

        

        Macro_dc = -penal1*(1-Emin)*Macro_xPhys.^(penal1-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx, Macro_nelz);


        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys, Macro_change = CompliantOC_reg3(Macro_x, Macro_dc, Macro_dv, Macro_Vol, Macro_nele, 0.1);

        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx, Macro_nelz);
        # PRINT RESULTS
        # println("FMD It.:$loop Obj.:$(round.(c,digits=2)) Macro_Vol.:$(round.(mean(Macro_xPhys[:] ),digits=2)) Macro_ch.:$(round.(Macro_change,digits=2))")

    end

    # display(heatmap(Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=jet,clims=(0.0, 1.0),aspect_ratio=:equal))
    # scene = Scene(resolution = (400, 400))
    # scene= volume!(Macro_xPhys, algorithm = :mip,transparency=true,colorrange=(0.0, 1.0),colormap=:lighttest)
    # display(scene)
    # save("./img/FM3DUC_$(Macro_Vol).png",scene)

    ##########clustering#########
    # X=U[edofMat]'
    X=hcat(Macro_xPhys[:],U[edofMat,1])'
    # cluster X into θ clusters using K-means

    for i=1:3
        # display(i)
        # XXX=reshape(mean(U[edofMat]',dims=1),Macro_nely,Macro_nelx,Macro_nelz)
        # XXX=reshape(sum(U[edofMat]',dims=1),Macro_nely,Macro_nelx,Macro_nelz)
        XXX=reshape(U[edofMat]'[i,:],Macro_nely,Macro_nelx,Macro_nelz)
        # display(minimum(XXX))
        # display(maximum(XXX))
        # scene = Scene(resolution = (200, 200))
        scene= GLMakie.volume(permutedims(XXX, [2, 1, 3]) , algorithm = :mip,transparency=true,colorrange=(minimum(XXX), maximum(XXX)),colormap=:lighttest)
        display(scene)
        # save("./img/FM3DU_$(Macro_Vol).png",scene)
    end

    # R = kmeans(X, θ; maxiter=200, display=:iter)
    R = Clustering.kmeans(X, θ; maxiter=500)

    @assert Clustering.nclusters(R) == θ # verify the number of clusters

    a = Clustering.assignments(R) # get the assignments of points to clusters
    c = Clustering.counts(R) # get the cluster sizes
    M = R.centers; # get the cluster centers

    D=Distances.pairwise(Euclidean(), M, dims=2)
    hc = hclust(D, linkage=:single)
    display(Plots.plot(hc))

    Uclustered=reshape(a,Macro_nely,Macro_nelx,Macro_nelz);

    #############################
    Macro_masksU=[]
    Macro_masks_densU=[]

    t=:darktest #t=:dark
    # theme(t);
    cschemeList=[]
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=cgrad(cschemeList)


    Macro_xPhys_diagU=copy(Macro_xPhys)
    d=1/θ
    for i in 1:θ
        Macro_xPhys_temp=copy(Macro_xPhys)
        out= (Uclustered.==i)

        Macro_xPhys_temp[out].=true
        Macro_xPhys_temp[.!out].=false

        dens=Micro_Vol


        println("Mask $i with density $(round(dens,digits=2)) occupies $(round(sum(out)/length(Macro_xPhys)*100,digits=2))% of the macrostructure.")

        Macro_xPhys_diagU[out].= i

        append!(Macro_masks_densU,[dens])
        append!(Macro_masksU,[Macro_xPhys_temp])
        # display(heatmap(Macro_xPhys_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
    end


    # scene = Scene(resolution = (400, 400)) #todo fix
    scene= GLMakie.volume(permutedims(Macro_xPhys_diagU, [2, 1, 3]), algorithm = :absorption,transparency=true,colorrange=(1, θ),colormap=cscheme)
    display(scene)
    # save("./img/FM3DU1C_$(Macro_Vol).png",scene)
    
    Makie.inline!(true)
    Makie.volume(permutedims(Macro_xPhys_diagU, [2, 1, 3]) , algorithm = :iso,colorrange=(1, θ),isorange = 1.0, isovalue = 1,colormap=cscheme)
    
    for i=2:θ
        Makie.volume!(permutedims(Macro_xPhys_diagU, [2, 1, 3]) , algorithm = :iso,colorrange=(1, θ),isorange = 1.0, isovalue = i,colormap=cscheme)
    end
    display(current_figure())
    # save("./img/FM3DFUC_$(Macro_Vol).png",scene)

    return Macro_masksU,Macro_masks_densU,Macro_xPhys_diagU
end

function SlicesUclustering3D(θ,Macro_nely,Macro_nelx,Macro_nelz,prob,Macro_Vol,Macro_rmin,Micro_Vol,edofMat,voxels=true)
    penal=3.0;Emin=1e-9;E0 = 1; nu = 0.3;KE=lk_H8(nu);
    Macro_nele=Macro_nelx*Macro_nely*Macro_nelz;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_Elex =Macro_Eley=Macro_Elez  = 1.0; #todo change if needed
    if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    end
    

    ##############################################
    Macro_xPhys = ones(Macro_nely,Macro_nelx,Macro_nelz)
    if Macro_Vol<1.0
        Macro_xPhys,anim=topologyOptimization3d(Macro_nelx,Macro_nely,Macro_nelz,prob,Macro_Vol,Macro_rmin,penal,false)
    end
    scene= GLMakie.volume(Macro_xPhys, algorithm = :iso, isorange = 0.2, isovalue = 0.8,colormap=:grays)
    display(scene)

    KE=lk_H8(nu)

    # PREPARE FINITE ELEMENT ANALYSIS
    U,F,freedofs=prob(Macro_nelx,Macro_nely,Macro_nelz);
    fixeddofs  = setdiff(1:Macro_ndof,freedofs)

    # edofMat = repeat(3 .*nodeids[:] .+1,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nelx+1)*(Macro_nely+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]], Macro_nele, 1);
    nodenrs = reshape(1:(1+Macro_nelx)*(1+Macro_nely)*(1+Macro_nelz),1+Macro_nely,1+Macro_nelx,1+Macro_nelz)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,Macro_nelx*Macro_nely*Macro_nelz,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nely+1)*(Macro_nelx+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]],Macro_nelx*Macro_nely*Macro_nelz,1)

    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*Macro_nele,1))
 
    sK = reshape(KE[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;

    U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
    ce = reshape(sum((U[edofMat]*KE).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz)
    c = sum(sum(sum((Emin.+Macro_xPhys.^penal*(E0-Emin)).*ce)))
    display("compliance before $c")

    ##############################################    



    ####################

    # dissYX,dissYZ,dissXZ,X,Macro_xPhys_2D=reshapeU2DSlices_new(U[edofMat],Macro_nely, Macro_nelx, Macro_nelz,Macro_xPhys);
    dissYX,dissZY,dissZX,X,Macro_xPhys_2D=reshapeU2DSlices_new(U[edofMat],Macro_nely, Macro_nelx, Macro_nelz,Macro_xPhys);


    ########################################################################
    order=[1,2,3,4,5,6,7,8];

    n=size(X)[1];
    Θ=zeros(n);ΘD=zeros(n);
    von_mises=zeros(n);
    ratio=zeros(n);
    for i=1:n
        Ue = X[i];
        Θ[i] , von_mises[i], ratio[i],α1,α2 =getPrincipalStrains(Ue);
    end
    ΘD=Θ.*180/π;

    wd=2;
    # display(size(X))
    # display(size(Θ))
    # display(size(Macro_xPhys_2D[:]))
    X=hcat(sin.(2*Θ[:]).*Macro_xPhys_2D[:],cos.(2*Θ[:]).*Macro_xPhys_2D[:],wd.*Macro_xPhys_2D[:])'
    
    #####################

    
    ########################################################################
    data=X
    D=Distances.pairwise(Euclidean(), data, dims=2)
    hc = hclust(D, linkage=:ward_presquared) # hc = hclust(D, linkage=:single,branchorder=:optimal)
    display(Plots.plot(hc))

    gr(size=(400,100))
    #for elbow curve
    hcElbow=reverse(hc.heights)[1:20]
    display(Plots.plot(1:(length(hcElbow)),hcElbow,label=""))
    hcElbow=reverse(hc.heights)[2:20]
    xy=hcat(1:(length(hcElbow)),hcElbow)
    mapp=mapproject(xy, dist2line=(line=[xy[1:1, :]; xy[end:end, :]], unit=:cartesian))
    display(Plots.plot(mapp[:,3],label=""))
    nclust=(findmax(mapp[:,3])[2]+1)
    display("Best Number of clusters is= $nclust")
    gr(size=(400,300))
    nclust=θ
    a=cutree(hc; k=nclust)
    #visualize tree with only θ clusters
    centers=zeros(size(data)[1],nclust)
    for i in 1:nclust
        R = Clustering.kmeans(data[:,a.==i], 1); # run K-means just to get the center
        M = R.centers; # get the cluster centers
        centers[:,i].=M[:]
    end
    D=Distances.pairwise(Euclidean(), centers, dims=2)
    hc = hclust(D, linkage=:ward_presquared,branchorder=:optimal) # hc = hclust(D, linkage=:single,branchorder=:optimal)
    display(Plots.plot(hc))

    t=:darktest
    cscheme=cgrad(t, θ, categorical = true)
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[cscheme[i]])
    end
    # cscheme=ColorGradient(cschemeList)
    cscheme1=cgrad(cschemeList)

    ######
    # aYX,aYZ,aXZ=reshape_back_reshapeU2DSlices_new(θ,a,Macro_nely, Macro_nelx, Macro_nelz)
    aYX,aZY,aZX=reshape_back_reshapeU2DSlices_new(θ,a,Macro_nely, Macro_nelx, Macro_nelz,true)

    #################################################
    #cuboct
    cuboct=load("./img/library/1/cuboct_vol_$((Micro_Vol)).jld")["data"];
    cuboct3D ,cuboctDH=visualize3D2DMicrostructure(cuboct,false)
    Ke = elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, cuboctDH);
    sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
    U[freedofs] = K[freedofs,freedofs]\Array(F[freedofs]);
    # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
    ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);
    c = sum(sum(sum((Emin .+Macro_xPhys.^penal*(1-Emin)).*ce)));
    display("cuboct compliance: $c")  


    return a,aYX,aZY,aZX,Macro_xPhys

end

function SlicesCompliantUclustering3D(θ,Macro_nely,Macro_nelx,Macro_nelz,Macro_nele,Macro_Vol,Micro_Vol,iK,jK,Emin,U,F,freedofs,edofMat,din,dout,voxels=true)
    if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    end
    maxloop = 90; E0 = 1; nu = 0.3;
    
    Ke=lk_H8(nu)
    
    Macro_x = ones(Macro_nely,Macro_nelx,Macro_nelz).*Macro_Vol;
    Macro_xTilde = Macro_x; 
    Macro_xPhys = Macro_x; 
    
    loopbeta = 0; loop = 0; Macro_change = 1;
    ## free material
    penal1=1;
    
    while loop < maxloop && Macro_change > 0.01 
        loop = loop+1;
    
    
        # FE-ANALYSIS AT TWO SCALES
        sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal1*(1 .-Emin)),24*24*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); 
        K[din,din] = K[din,din] + 0.1
        K[dout,dout] = K[dout,dout] + 0.1
        K = (K+K')/2;
        U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
        U1 = U[:,1]
        U2 = U[:,2]
        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        # ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);
        ce = reshape(-sum((U1[edofMat]*Ke).*U2[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);
    
        c  = U[dout,1]
    
    
    
        Macro_dc = -penal1*(1-Emin)*Macro_xPhys.^(penal1-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx, Macro_nelz);
    
    
        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys, Macro_change = CompliantOC_reg3(Macro_x, Macro_dc, Macro_dv, Macro_Vol, Macro_nele, 0.1);
    
        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx, Macro_nelz);
        # PRINT RESULTS
        # println("FMD It.:$loop Obj.:$(round.(c,digits=2)) Macro_Vol.:$(round.(mean(Macro_xPhys[:] ),digits=2)) Macro_ch.:$(round.(Macro_change,digits=2))")
    
    end
    Makie.inline!(true)
    # scene = Scene(resolution = (200, 200))
    scene= GLMakie.volume(permutedims(Macro_xPhys, [2, 1, 3]), algorithm = :mip,transparency=true,colorrange=(0.0, 1.0),colormap=:lighttest)
    display(scene)

    Makie.inline!(true)
    # scene = Scene(resolution = (200, 200))
    # scene= volume!(Macro_xPhys, algorithm = :mip,transparency=true,colorrange=(0.0, 1.0),colormap=:lighttest)
    scene= GLMakie.volume(permutedims(Macro_xPhys, [2, 1, 3]), algorithm = :iso,colorrange=(0.0, 1.0),isorange = 0.2, isovalue = 1)
    display(scene)

    U1 = U[:,1];U2 = U[:,2];
    # UU1=reshape(U1[edofMat],Macro_nely, Macro_nelx, Macro_nelz,24);
    # UU2=reshape(U2[edofMat],Macro_nely, Macro_nelx, Macro_nelz,24);
    # size(UU1)

    disY,disX,disZ,X,XX=reshapeU2DSlices(U2[edofMat],Macro_nely, Macro_nelx, Macro_nelz);


    ########################################################################
    # displayAllUPlotGrid(disX)
    # displayAllUPlotGrid(disY)
    displayAllUPlotGrid(disZ)
    display(Plots.heatmap(1.0.-Macro_xPhys[:,:,2],showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))


    ## principal strain
    diss=disZ[2];order=[1,2,3,4,5,6,7,8];
    # diss=disY[2];order=[1,2,5,6,7,8,3,4];
    # diss=disX[2];order=[1,2,5,6,7,8,3,4];

    unit=10
    ny=size(diss)[1];nx=size(diss)[2];

    ϵx =zeros(ny ,nx);ϵy =zeros(ny ,nx);
    αx =zeros(ny ,nx);αy =zeros(ny ,nx);
    α1 =zeros(ny ,nx);α2 =zeros(ny ,nx);α3 =zeros(ny ,nx);α4 =zeros(ny ,nx);
    ϵxy =zeros(ny ,nx);τxy=zeros(ny ,nx);
    u=zeros(ny ,nx);v=zeros(ny ,nx);
    Θ=zeros(ny ,nx);ΘD=zeros(ny ,nx);
    von_mises=zeros(ny ,nx);


    l=100;nu=0.3;ν=0.3;E=100

    a=1;
    b=1;
    x=0;y=0;#at center

    N1=(a-x)*(b-y)/(4*a*b)
    N2=(a+x)*(b-y)/(4*a*b)
    N3=(a+x)*(b+y)/(4*a*b)
    N4=(a-x)*(b+y)/(4*a*b)

    B = (1/2/l)*[-1 0 1 0 1 0 -1 0; 0 -1 0 -1 0 1 0 1; -1 -1 -1 1 1 1 1 -1];
    C = E/(1-ν^2)*[1 ν 0; ν 1 0; 0 0 (1-ν)/2];
    C = E/((1+ν)*(1-2*ν))*[1-ν ν 0; ν 1-ν 0; 0 0 (1-2*ν)/2];


    for i=1:ny
        for j=1:nx
            #based on: https://community.wvu.edu/~bpbettig/MAE456/Exam_Final_practice_answers.pdf

            
            Ue = diss[i,j,:];
            

            N=[N1 0 N2 0 N3 0 N4 0; 0 N1 0 N2 0 N3 0 N4];

            #displacement at center
            u[i,j],v[i,j]=N*Ue

            #in plane strain in center
            ϵ=1/(4*a*b)*[-(b-y) 0 (b-y) 0 (b+y) 0 -(b+y)  0;
                0 -(a-x) 0 -(a+x) 0 (a+x) 0 (a-x);
                -(a-x) -(b-y) -(a+x) (b-y) (a+x) (b+y) (a-x) -(b+y)]*Ue
            ϵx[i,j],ϵy[i,j],ϵxy[i,j]=ϵ

            #in plane stress in center
            α=C*ϵ
            αx[i,j],αy[i,j],τxy[i,j]=α

            #principal stress center
            α1[i,j]=(αx[i,j]+αy[i,j])/2+sqrt(((αx[i,j]-αy[i,j])/2)^2+τxy[i,j]^2)
            α2[i,j]=(αx[i,j]+αy[i,j])/2-sqrt(((αx[i,j]-αy[i,j])/2)^2+τxy[i,j]^2)
            α3[i,j]=ν*(αx[i,j]+αy[i,j])
            α4[i,j]=ν*(αx[i,j]+αy[i,j])
            # Θ   =atan((2*τxy),(αx-αy))/2
            # Θ   =atan((2*ϵxy),(ϵx-ϵy))/2
            #von_mises
            von_mises[i,j]=1/(sqrt(2))*sqrt((α1[i,j]-α2[i,j])^2+(α2[i,j]-α3[i,j])^2+(α3[i,j]-α1[i,j])^2)
            Θ[i,j]   =atan((2*ϵxy[i,j]/2),(ϵx[i,j]-ϵy[i,j]))/2
            Θ[i,j]   =atan((2*τxy[i,j]),(αx[i,j]-αy[i,j]))/2
            if abs(α1[i,j])<abs(α2[i,j])
                Θ[i,j]   -=π/2
            end
        end
    end
    ΘD=Θ.*180/π

    Plots.heatmap(von_mises, aspect_ratio=:equal)#,clim=(0,2e-5))
    xs = 1:Int(nx)
    ys = 1:Int(ny)
    maxi=maximum(abs.(α1))/2
    # ress(y,x)=(cos(Θ[Int(x),Int(y)])*α1[Int(x),Int(y)]/maxi, sin(Θ[Int(x),Int(y)])*α1[Int(x),Int(y)]/maxi)
    # ress1(y,x)=(cos(Θ[Int(x),Int(y)]-π/2)*α2[Int(x),Int(y)]/maxi, sin(Θ[Int(x),Int(y)]-π/2)*α2[Int(x),Int(y)]/maxi)
    ress(y,x)=(cos(Θ[Int(x),Int(y)])/3, -sin(Θ[Int(x),Int(y)])/3)
    ress1(y,x)=(cos(Θ[Int(x),Int(y)]+π/2)/3, sin(Θ[Int(x),Int(y)]+π/2)/3)
    ress2(y,x)=(cos(Θ[Int(x),Int(y)]-π/2)/3, sin(Θ[Int(x),Int(y)]-π/2)/3)
    ress3(y,x)=(cos(Θ[Int(x),Int(y)]+π)/3, sin(Θ[Int(x),Int(y)]+π)/3)
    
    # col(y,x)=norm(ress(y,x))
    xxs = [x for x in xs for y in ys]
    yys = [y for x in xs for y in ys]
    Plots.quiver!(xxs, yys, quiver=ress)
    # Plots.quiver!(xxs, yys, quiver=ress1)
    # Plots.quiver!(xxs, yys, quiver=ress2)
    # display(Plots.quiver!(xxs, yys, quiver=ress3))
    display(Plots.quiver!(xxs, yys, quiver=ress1))




    # displayElementDeformation(unit,diss[1,1,:],order,true);
    # displayElementDeformation(unit,diss[end,5,:],order,true);

    # displayElementDeformation(unit,diss[end,end,:],order,true);


    dissVec=zeros(ny+1 ,nx+1 ,2)
    for i=1:ny
        for j=1:nx
            dissVec[i,j,1]=diss[i,j,1]
            dissVec[i,j,2]=diss[i,j,2]
        end
    end
    for i=1:ny
        dissVec[i,end,1]=diss[i,end,order[3]]
        dissVec[i,end,2]=diss[i,end,order[4]]
    end
    for j=1:nx
        dissVec[end,j,1]=diss[end,j,order[7]]
        dissVec[end,j,2]=diss[end,j,order[8]]
    end
    dissVec[end,end,1]=diss[end,end,order[5]]
    dissVec[end,end,2]=diss[end,end,order[6]]


    UX=dissVec[:,:,2];UY=dissVec[:,:,1]
    plotStrain(nx,ny,unit,diss,order,UX,UY)

    ########################################################################
    X=X'
    # X=XX'
    R = Clustering.kmeans(X, θ; maxiter=1000)

    @assert Clustering.nclusters(R) == θ # verify the number of clusters

    a = Clustering.assignments(R) # get the assignments of points to clusters
    c = Clustering.counts(R) # get the cluster sizes
    M = R.centers; # get the cluster centers

    D=Distances.pairwise(Euclidean(), M, dims=2)
    hc = hclust(D, linkage=:single)
    display(Plots.plot(hc))

    b = [ParallelKMeans.kmeans(X, i; tol=1e-6, max_iters=300, verbose=false).totalcost for i = 2:20]
    display(Plots.plot(b,label=""))

    t=:darktest #t=:dark
    # theme(t);
    cschemeList=[]
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=cgrad(cschemeList); #cscheme=ColorGradient(cschemeList);
    t=:darktest #t=:dark
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=cgrad(cschemeList); #cscheme=ColorGradient(cschemeList);

    slice=zeros(Macro_nelx,2)
    ySlices=slice
    s=0;
    for i=1:Macro_nely*2
        aa=a[(i-1)*Macro_nelx*Macro_nelz+1+s:(i)*Macro_nelx*Macro_nelz+s]
        Uclustered=reshape(aa,Macro_nelx,Macro_nelz);
        ySlices=hcat(ySlices,slice,Uclustered)
        # display(Plots.heatmap(Uclustered,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cscheme,aspect_ratio=:equal))
    end
    display(Plots.heatmap(ySlices,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cscheme,aspect_ratio=:equal))

    slice=zeros(Macro_nely,2)
    xSlices=slice

    s=Macro_nely*2*Macro_nelx*Macro_nelz;
    for i=1:Macro_nelx*2
        aa=a[(i-1)*Macro_nely*Macro_nelz+1+s:(i)*Macro_nely*Macro_nelz+s]
        Uclustered=reshape(aa,Macro_nely,Macro_nelz);
        xSlices=hcat(xSlices,slice,Uclustered)
        # display(Plots.heatmap(Uclustered,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cscheme,aspect_ratio=:equal))
    end
    display(Plots.heatmap(xSlices,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cscheme,aspect_ratio=:equal))

    slice=zeros(Macro_nely,2)
    zSlices=slice

    s=Macro_nely*2*Macro_nelx*Macro_nelz+(Macro_nelx*2)*Macro_nely*Macro_nelz;
    for i=1:Macro_nelz*2
        aa=a[(i-1)*Macro_nely*Macro_nelx+1+s:(i)*Macro_nely*Macro_nelx+s]
        Uclustered=reshape(aa,Macro_nely,Macro_nelx);
        zSlices=hcat(zSlices,slice,Uclustered)
        # display(Plots.heatmap(Uclustered,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cscheme,aspect_ratio=:equal))
    end
    display(Plots.heatmap(zSlices,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cscheme,aspect_ratio=:equal))

    iY=1:Macro_nely*2*Macro_nelx*Macro_nelz
    s=Macro_nely*2*Macro_nelx*Macro_nelz
    iX=s+1:s+Macro_nelx*2*Macro_nely*Macro_nelz
    s=s+Macro_nelx*2*Macro_nely*Macro_nelz
    iZ=s+1:s+Macro_nelz*2*Macro_nely*Macro_nelx
    aY=a[iY];
    aX=a[iX];
    aZ=a[iZ];
    # length(aY)+length(aX)+length(aZ)
    aaY=reshape(aY,Macro_nelx,Macro_nelz,2*Macro_nely);
    aaX=reshape(aX,Macro_nely,Macro_nelz,2*Macro_nelx);
    aaZ=reshape(aZ,Macro_nely,Macro_nelx,2*Macro_nelz);

    return a,aaY,aaX,aaZ

end


##############################################################

function reshapeU2DSlices(U2,Macro_nely, Macro_nelx, Macro_nelz)
    UU2=reshape(U2,Macro_nely, Macro_nelx, Macro_nelz,24);
    #6faces
    f1=[1;2;3;4].-1; #back xy
    f2=[5;6;7;8].-1; #front xy
    f3=[1;3;5;7].-1; #top  xz
    f4=[2;4;6;8].-1; #bottom xz
    f5=[1;2;5;6].-1; #right  yz
    f6=[3;4;7;8].-1; #left yz
    ff1=sort(vcat(f1.*3, f1.*3 .+1 ).+1);
    ff2=sort(vcat(f2.*3, f2.*3 .+1 ).+1);
    ff3=sort(vcat(f3.*3, f3.*3 .+2).+1);
    ff4=sort(vcat(f4.*3, f4.*3 .+2).+1);
    ff5=sort(vcat(f5.*3 .+1 , f5.*3 .+2).+1);
    ff6=sort(vcat(f6.*3 .+1 , f6.*3 .+2).+1);
    #number of displacement maps= 2*Macro_nely* 2*Macro_nelx* 2*Macro_nelz
    #displacement maps
    
    disY=[] #Macro_nely; f3 and f4, dof 1,3
    for i=1:Macro_nely
        append!(disY,[UU2[i,:,:,ff3]])
        append!(disY,[UU2[i,:,:,ff4]])
    end
    disX=[]; #Macro_nelx; f5 and f6, dof 2,3
    for i=1:Macro_nelx
        append!(disX,[UU2[:,i,:,ff5]])
        append!(disX,[UU2[:,i,:,ff6]])
    end
    disZ=[]; #Macro_nelz; f1 and f2, dof 1,2
    for i=1:Macro_nelz
        append!(disZ,[UU2[:,:,i,ff1]])
        append!(disZ,[UU2[:,:,i,ff2]])
    end
    # for i=1:24
    #     display(Plots.heatmap(UU1[:,:,2,i],aspect_ratio=:equal))
    # # end
    # display(Plots.heatmap(disY[1][:,:,1],aspect_ratio=:equal));
    # display(Plots.heatmap(disY[1][:,:,2],aspect_ratio=:equal));
    # display(Plots.heatmap(disY[2][:,:,1],aspect_ratio=:equal));
    # display(Plots.heatmap(disY[2][:,:,2],aspect_ratio=:equal));

    for k=1:Macro_nely*2
        d=copy(disY[k])
        for i in 1:size(d)[1]
            for j in 1:size(d)[2]
                disY[k][i,j,1]=d[i,j,1];disY[k][i,j,2]=d[i,j,2];
                disY[k][i,j,3]=d[i,j,5];disY[k][i,j,4]=d[i,j,6];
                disY[k][i,j,5]=d[i,j,7];disY[k][i,j,6]=d[i,j,8];
                disY[k][i,j,7]=d[i,j,3];disY[k][i,j,8]=d[i,j,4];
            end
        end
    end
    
    for k=1:Macro_nelx*2
        d=copy(disX[k])
        for i in 1:size(d)[1]
            for j in 1:size(d)[2]
                disX[k][i,j,1]=d[i,j,1];disX[k][i,j,2]=d[i,j,2];
                disX[k][i,j,3]=d[i,j,5];disX[k][i,j,4]=d[i,j,6];
                disX[k][i,j,5]=d[i,j,7];disX[k][i,j,6]=d[i,j,8];
                disX[k][i,j,7]=d[i,j,3];disX[k][i,j,8]=d[i,j,4];
            end
        end
    end
    
    #get list for clustering
    X=reshape(disY[1],Macro_nelx*Macro_nelz,8)
    for i=2:Macro_nely*2
        X=vcat(X,reshape(disY[i],Macro_nelx*Macro_nelz,8))
    end
    for i=1:Macro_nelx*2
        X=vcat(X,reshape(disX[i],Macro_nely*Macro_nelz,8))
    end
    for i=1:Macro_nelz*2
        X=vcat(X,reshape(disZ[i],Macro_nelx*Macro_nely,8))
    end

    #cluster using principal stress instead
    XX=zeros(size(X)[1])
    for i in 1:size(X)[1]
        XX[i]=getShapeStrainDisplacement(X[i,:],0,0);
    end


    return disY,disX,disZ,X,XX

end

function getYX(el3D)
    el3D=reshape(el3D,3,8)'
    el3D=el3D[:,[1,2]]
    Ue1=[el3D[1,:] el3D[2,:] el3D[3,:] el3D[4,:]]
    Ue2=[el3D[5,:] el3D[6,:] el3D[7,:] el3D[8,:]]
    return Ue1[:],Ue2[:]
end

function getZY(el3D)
    el3D=reshape(el3D,3,8)'
    el3D=el3D[:,[2,3]]
    # el3D[:,2].=el3D[:,2]
    Ue1=[el3D[8,:] el3D[5,:] el3D[1,:] el3D[4,:] ]
    Ue2=[el3D[7,:] el3D[6,:] el3D[2,:] el3D[3,:] ]
    return Ue1[:],Ue2[:]
end

function getZX(el3D)
    el3D=reshape(el3D,3,8)'
    el3D=el3D[:,[1,3]]
    el3D[:,1].=-el3D[:,1]
    # el3D[:,2].=-el3D[:,2]
    Ue1= [ el3D[8,:] el3D[7,:] el3D[3,:] el3D[4,:]]
    Ue2= [ el3D[5,:] el3D[6,:] el3D[2,:] el3D[1,:]]
    return Ue1[:],Ue2[:]
end

function reshapeU2DSlices_new(Umat,nely, nelx, nelz, Macro_xPhys=ones(nely,nelx,nelz),verbose=false)
    Macro_xPhys_2D=[]
    order=[1,2,3,4,5,6,7,8];
    ord=reshape(1:(nely*nelx*nelz),nely,nelx,nelz)
    UeAll=[]
    dissYX=[]
    for k in 1:nelz
        append!(dissYX,[zeros(nely,nelx,8)])
        append!(dissYX,[zeros(nely,nelx,8)])
        for i in 1:nely
            for j in 1:nelx
                Ue1,Ue2=getYX(Umat[ord[i,j,k],:])
                if verbose
                    points,pointsU=displayElementDeformation(10,Ue1,order,true)
                    points,pointsU=displayElementDeformation(10,Ue2,order,true)
                end
                dissYX[(k-1)*2+1][i,j,:].=Ue1
                dissYX[(k-1)*2+2][i,j,:].=Ue2
                append!(UeAll,[Ue1]);append!(UeAll,[Ue2]);
                append!(Macro_xPhys_2D,[Macro_xPhys[i,j,k]]);
                append!(Macro_xPhys_2D,[Macro_xPhys[i,j,k]]);
            end
        end
        # display("YX")
        # plotVonMiesesStrain(dissYX[(k-1)*2+1],ones(nely,nelx))
        # plotVonMiesesStrain(dissYX[(k-1)*2+2],ones(nely,nelx))
    end
    dissZY=[]
    for k in 1:nelx
        append!(dissZY,[zeros(nelz,nely,8)])
        append!(dissZY,[zeros(nelz,nely,8)])
        for i in 1:nelz
            for j in 1:nely
                Ue1,Ue2=getZY(Umat[ord[j,k,i],:])
                if verbose
                    points,pointsU=displayElementDeformation(10,Ue1,order,true)
                    points,pointsU=displayElementDeformation(10,Ue2,order,true)
                end
                dissZY[(k-1)*2+1][i,j,:].=Ue1
                dissZY[(k-1)*2+2][i,j,:].=Ue2
                append!(UeAll,[Ue1]);append!(UeAll,[Ue2]);
                append!(Macro_xPhys_2D,[Macro_xPhys[j,k,i]]);
                append!(Macro_xPhys_2D,[Macro_xPhys[j,k,i]]);
            end
        end
        # display("ZY")
        # plotVonMiesesStrain(dissZY[(k-1)*2+1],ones(nelz,nely))
        # plotVonMiesesStrain(dissZY[(k-1)*2+2],ones(nelz,nely))
    end
    dissZX=[]
    for k in 1:nely
        append!(dissZX,[zeros(nelz,nelx,8)])
        append!(dissZX,[zeros(nelz,nelx,8)])
        for i in 1:nelz
            for j in 1:nelx
                Ue1,Ue2=getZX(Umat[ord[k,j,i],:])
                if verbose
                    points,pointsU=displayElementDeformation(10,Ue1,order,true)
                    points,pointsU=displayElementDeformation(10,Ue2,order,true)
                end
                dissZX[(k-1)*2+1][i,j,:].=Ue1
                dissZX[(k-1)*2+2][i,j,:].=Ue2
                append!(UeAll,[Ue1]);append!(UeAll,[Ue2]);
                append!(Macro_xPhys_2D,[Macro_xPhys[k,j,i]]);
                append!(Macro_xPhys_2D,[Macro_xPhys[k,j,i]]);
            end
        end
        # display("ZX")
        # plotVonMiesesStrain(dissZX[(k-1)*2+1],ones(nelz,nelx))
        # plotVonMiesesStrain(dissZX[(k-1)*2+2],ones(nelz,nelx))
    end
    return dissYX,dissZY,dissZX,UeAll,Macro_xPhys_2D
end

function reshape_back_reshapeU2DSlices_new(θ,a,nely, nelx, nelz,strainLibrary=false)
    # θ=maximum(a)
    t=:darktest
    cscheme=""
    cscheme1=""
    if strainLibrary
        t=:darktest
        cscheme=cgrad(t, θ-1, categorical = true)
        cschemeList=[]
        append!(cschemeList,[:white])
        for i=1:θ-1
            append!(cschemeList,[cscheme[i]])
        end
        append!(cschemeList,[:black])
        cscheme1=cgrad(cschemeList)
    else
        cschemeList=[]
        # append!(cschemeList,[:white])
        for i=1:θ
            append!(cschemeList,[palette(t)[i]])
        end
        cscheme=cgrad(cschemeList)
        cschemeList=[]
        append!(cschemeList,[:white])
        for i=1:θ
            append!(cschemeList,[cscheme[i]])
        end
        # cscheme=ColorGradient(cschemeList)
        cscheme1=cgrad(cschemeList)
    end
    
    # go from X to slices
    counter=1
    aYX=[]
    slice=zeros(nely,2)
    zSlices=slice
    for k in 1:nelz
        append!(aYX,[zeros(nely,nelx)])
        append!(aYX,[zeros(nely,nelx)])
        for i in 1:nely
            for j in 1:nelx
                aYX[(k-1)*2+1][i,j]=a[counter];counter+=1;
                aYX[(k-1)*2+2][i,j]=a[counter];counter+=1;
            end
        end
        zSlices=hcat(zSlices,slice,aYX[(k-1)*2+1])
        zSlices=hcat(zSlices,slice,aYX[(k-1)*2+2])
    end
    display(Plots.heatmap(zSlices,title="YX",showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cscheme1,aspect_ratio=:equal))

    aZY=[]
    slice=zeros(nelz,2)
    xSlices=slice
    for k in 1:nelx
        append!(aZY,[zeros(nelz,nely)])
        append!(aZY,[zeros(nelz,nely)])
        for i in 1:nelz
            for j in 1:nely
                aZY[(k-1)*2+1][i,j]=a[counter];counter+=1;
                aZY[(k-1)*2+2][i,j]=a[counter];counter+=1;
            end
        end
        xSlices=hcat(xSlices,slice,aZY[(k-1)*2+1])
        xSlices=hcat(xSlices,slice,aZY[(k-1)*2+2])
    end
    display(Plots.heatmap(xSlices,title="ZY",showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cscheme1,aspect_ratio=:equal))

    aZX=[]
    slice=zeros(nelz,2)
    ySlices=slice
    for k in 1:nely
        append!(aZX,[zeros(nelz,nelx)])
        append!(aZX,[zeros(nelz,nelx)])
        for i in 1:nelz 
            for j in 1:nelx
                aZX[(k-1)*2+1][i,j]=a[counter];counter+=1;
                aZX[(k-1)*2+2][i,j]=a[counter];counter+=1;
            end
        end
        ySlices=hcat(ySlices,slice,aZX[(k-1)*2+1])
        ySlices=hcat(ySlices,slice,aZX[(k-1)*2+2])
    end
    display(Plots.heatmap(ySlices,title="ZX",showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cscheme1,aspect_ratio=:equal))
    return aYX,aZY,aZX
end
##############################################################
###display and plot
function plotVonMiesesStrain(diss,Macro_xPhys=ones(size(diss)[1],size(diss)[2]))
    ny=size(diss)[1];nx=size(diss)[2];
    Θ=zeros(ny ,nx);ΘD=zeros(ny ,nx);
    von_mises=zeros(ny ,nx);
    ratio=zeros(ny ,nx);

    for i=1:ny
        for j=1:nx
            #based on: https://community.wvu.edu/~bpbettig/MAE456/Exam_Final_practice_answers.pdf
            Ue = diss[i,j,:];
            Θ[i,j] , von_mises[i,j], ratio[i,j],α1,α2 =getPrincipalStrains(Ue);
        end
    end
    ΘD=Θ.*180/π;

    ratioPlot=copy(ratio)
    maxRatio=maximum(ratioPlot[Macro_xPhys.>0.5])
    minRatio=minimum(ratioPlot[Macro_xPhys.>0.5])
    ratioPlot[Macro_xPhys.<0.5].=NaN

    maxvon_mises=maximum(von_mises[Macro_xPhys.>0.5])
    minvon_mises=minimum(von_mises[Macro_xPhys.>0.5])
    if maxvon_mises>0.1
        maxvon_mises=0.01
    end

    # if minvon_mises<-0.3
    #     minvon_mises=-0.01
    # end
    # gr(size=(300,300))
    # display(von_mises)
    p=Plots.contour(von_mises, fill=true,colorbar=false, aspect_ratio=:equal,fc=:Spectral,clim=(minvon_mises,maxvon_mises), ticks=nothing, axis=nothing,size=(200,200))#,clim=(0,2e-5))

    # display(p)#,clim=(0,2e-5))
    Plots.heatmap(ratioPlot, aspect_ratio=:equal,fc=:jet,legend=false,clim=(-1,1), ticks=nothing, yaxis=nothing, xaxis=nothing,size=(200,200))#,clim=(0,2e-5))
    von_mises[Macro_xPhys.<0.5].=NaN
    # Plots.heatmap(von_mises, aspect_ratio=:equal,fc=:jet)#,clim=(0,2e-5))
    xs = 1:Int(nx)
    ys = 1:Int(ny)

    ress(y,x)=(cos(Θ[Int(x),Int(y)])/1, -sin(Θ[Int(x),Int(y)])/1)

    # col(y,x)=norm(ress(y,x))
    xxs = [x for x in xs for y in ys]
    yys = [y for x in xs for y in ys]
    pp=Plots.quiver!(xxs, yys, quiver=ress)
    # us,vs=ress(xxs,yys)
    # strength = vec(sqrt.(us .^ 2 .+ vs .^ 2))

    # Makie.arrows!(xxs, yys, us, vs, arrowsize = 10, lengthscale = 0.3,arrowcolor = strength, linecolor = strength)
    # # display(pp)
    return von_mises,p, Θ,pp
end


function displayCountourVonMieses(ppp,transpose=false)
    fig = Figure(resolution =(200*length(ppp)*2,250*2))
    t=:Spectral
    cs=cgrad(t, 2, categorical = true)
    

    xs = 1:Int((length(ppp)-1)*sizee)
    ys = 1:Int((length(ppp)-1)*sizee)
    # zs = [ppp[1][x,y] for x in xs, y in ys]
    
    for i in 1:length(ppp)
        
        p=ppp[i]
        if minimum(p)!=maximum(p)
            maxvon_mises=maximum(p)
            # if maxvon_mises>0.1
            #     maxvon_mises=0.01
            #     p[p.>=0.01].=0.01
            # end
            
            if transpose
                p=p'
            end
            # ,colorrange =(minimum(p), maxvon_mises)
            ax, hm = Makie.contourf(fig[1, i][1, 1], xs,ys,p,levels=20 ,colormap = t,axis = (aspect = DataAspect(),), extendhigh  = cs[2], extendlow  = cs[1]) 
            Makie.contour!(fig[1, i][1, 1], xs,ys,p,levels=19,color=:black,axis = (aspect = DataAspect(),))
            hidedecorations!(ax, ticks = false)
            Colorbar(fig[1, i][2, 1], hm,vertical = false)
        end
    end

    display(fig)
end


function displayStressArrows(ppp,transpose=false)
    fig = Figure(resolution =(200*length(ppp)*2,250*2))
    t=:Spectral
    cs=cgrad(t, 2, categorical = true)
    

    xs = 1:Int((length(ppp)-1)*sizee)
    ys = 1:Int((length(ppp)-1)*sizee)
    
    
    for i in 1:length(ppp)
        
        
        Θ=ppp[i]
        if !transpose
            Θ=Θ'
        else
            Θ=Θ'
            # Θ.+=pi/2
        end

        ress(y,x)=(cos.(Θ[Int.(x),Int.(y)])/1, -sin.(Θ[Int.(x),Int.(y)])/1)

        # col(y,x)=norm(ress(y,x))
        x = [x for x in xs for y in ys]
        y = [y for x in xs for y in ys]

        xs = 1:Int((size(Θ)[1]-1))
        ys = 1:Int((size(Θ)[2]-1))

        us = [cos.(Θ[Int.(x),Int.(y)]) for x in xs, y in ys]
        vs = [-sin.(Θ[Int.(x),Int.(y)]) for x in xs, y in ys]
        strength = vec(sqrt.(us .^ 2 .+ vs .^ 2))

        # us=cos.(Θ[Int.(x),Int.(y)])/1
        # vs=-sin.(Θ[Int.(x),Int.(y)])/1
        # strength = vec(sqrt.(us .^ 2 .+ vs .^ 2))
        cc=Θ[Int.(xs),Int.(ys)]
        xxx=cos.(2. .*cc[:]).-cos.(0)
        yyy=sin.(2. .*cc[:]).-sin.(0)
        
        # cc=cos.(2. .*cc[:])
        strength = vec(sqrt.(xxx .^ 2 .+ yyy .^ 2))

        
        ax=Makie.Axis(fig[1, i][1, 1],aspect = DataAspect(), backgroundcolor = "black")
        Makie.arrows!(xs, ys, us, vs, arrowsize = 10, lengthscale = 0.8,
            arrowcolor = strength, linecolor = strength,colormap = t)
        hidedecorations!(ax)

    end

    display(fig)
end

#3d from 2d visuaize DH
function visualize3D2DMicrostructure(micro,verbose=true)
    Micro_nely=Micro_nelx=Micro_nelz=100
    Micro_length =Micro_width=Micro_Height =0.1;
    E0 = 1; Emin = 1e-9; nu = 0.3;penal=3;
    Makie.inline!(true)

    θTempList="";DH="";
    xPhys3D=zeros(Micro_nely,Micro_nelx,Micro_nelz);
    θTemp=1
    thickness=1;

    xPhys3D[1:thickness,:,:]=micro;
    xPhys3D[end-thickness+1:end,:,:]=micro;
    xPhys3D[:,1:thickness,:]=micro;
    xPhys3D[:,end-thickness+1:end,:]=micro;
    xPhys3D[:,:,1:thickness]=micro;
    xPhys3D[:,:,end-thickness+1:end]=micro;


    size3D=20;
    xPhys3D1=reshape(xPhys3D,Micro_nely,Micro_nelx,Micro_nelz,1,1)
    xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D);
    if verbose
        scene= GLMakie.volume( permutedims(xPhys3D1, [2, 1, 3]), algorithm = :iso, isorange = 0.3, isovalue = 1.0,colormap=:grays)
        display(scene)

        #     scene= GLMakie.volume(xPhys3D, algorithm = :iso, isorange = 0.2, isovalue = 0.8,colormap=:grays)
        #     display(scene)
    end

    # xPhys3D1=reshape(Flux.AdaptiveMeanPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
    DH, dDH = EBHM3D(xPhys3D1, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,false);
    dispFlag = 0;plotFlag = 1;outOption = "struct";
    dens=sum(xPhys3D1)/(size(xPhys3D1)[1]*size(xPhys3D1)[2]*size(xPhys3D1)[3])
    if verbose
        props, SH = evaluateCH(DH, dens, outOption, dispFlag);
        fig=visual(DH);
    end
    # save("normal.png", fig)
    return xPhys3D1 ,DH
end

function assembleFull3DStructure(θ,Macro_xPhys,Micro_xPhys,DHs,a,aYX,aZY,aZX,size3D=20,thickness=1,strainLibrary=false)
    # thresh=0.5
    # thresh1=0.75
    # θ=maximum(a)

    t=:darktest
    cscheme=""
    cscheme1=""
    if strainLibrary
        t=:darktest
        cscheme=cgrad(t, θ-1, categorical = true)
        cschemeList=[]
        append!(cschemeList,[:white])
        for i=1:θ-1
            append!(cschemeList,[cscheme[i]])
        end
        append!(cschemeList,[:black])
        cscheme1=cgrad(cschemeList)
        cschemeList=[]
        for i=1:θ-1
            append!(cschemeList,[cscheme[i]])
        end
        append!(cschemeList,[:black])
        cscheme2=cgrad(cschemeList)

        t=:matter
        cschemes=cgrad(t, num+1, categorical = true)
        cschemeLists=[]
        append!(cschemeLists,[:white])
        for i=num:-1:1
            append!(cschemeLists,[cschemes[i]])
        end
        for i=2:num
            append!(cschemeLists,[cschemes[i]])
        end
        for i=num-1:-1:1
            append!(cschemeLists,[cschemes[i]])
        end
        for i=2:num
            append!(cschemeLists,[cschemes[i]])
        end
        append!(cschemeLists,[:black])
        cschemes1=cgrad(cschemeLists)
    else
        cschemeList=[]
        # append!(cschemeList,[:white])
        for i=1:θ
            append!(cschemeList,[palette(t)[i]])
        end
        cscheme=cgrad(cschemeList)
        cschemeList=[]
        append!(cschemeList,[:white])
        for i=1:θ
            append!(cschemeList,[cscheme[i]])
        end
        # cscheme=ColorGradient(cschemeList)
        cscheme1=cgrad(cschemeList)
    end
    

    for i=1:θ
        Micro_xPhys[i][Micro_xPhys[i].<0.5].=0.0
        Micro_xPhys[i][Micro_xPhys[i].>=0.5].=1.0
    end
    sizeMicro=size(Micro_xPhys[1])[1]
    Macro_xPhys_det=zeros(size(Macro_xPhys)[1]*size3D,size(Macro_xPhys)[2]*size3D,size(Macro_xPhys)[3]*size3D);
    Macro_xPhys_det_layers=repeat(Macro_xPhys_det,1,1,1,θ)
    # repeat(Micro_xPhys2[1]',1,1,2)
    for yy in 1:size(Macro_xPhys)[1]
        for xx in 1:size(Macro_xPhys)[2]
            for zz in 1:size(Macro_xPhys)[3]
                θTempList="";DH="";
                xPhys3D=zeros(sizeMicro,sizeMicro,sizeMicro);
                xPhys3D_layers=repeat(xPhys3D,1,1,1,θ)
                for thick in 0:thickness-1
                    θTemp=Int(aYX[Int((zz-1)*2+1)][Int(yy),Int(xx)]) ; xPhys3D[:,:,1+thick]=Micro_xPhys[θTemp].*θTemp; xPhys3D_layers[:,:,1+thick,θTemp]=Micro_xPhys[θTemp];
                    θTemp=Int(aYX[Int((zz-1)*2+2)][Int(yy),Int(xx)]) ; xPhys3D[:,:,end-thick]=Micro_xPhys[θTemp].*θTemp; xPhys3D_layers[:,:,end-thick,θTemp]=Micro_xPhys[θTemp];
                    θTemp=Int(aZY[Int((xx-1)*2+1)][Int(zz),Int(yy)]) ; xPhys3D[:,1+thick,:]=Micro_xPhys[θTemp]'.*θTemp; xPhys3D_layers[:,1+thick,:,θTemp]=Micro_xPhys[θTemp]';
                    θTemp=Int(aZY[Int((xx-1)*2+2)][Int(zz),Int(yy)]) ; xPhys3D[:,end-thick,:]=Micro_xPhys[θTemp]'.*θTemp; xPhys3D_layers[:,end-thick,:,θTemp]=Micro_xPhys[θTemp]';
                    θTemp=Int(aZX[Int((yy-1)*2+1)][Int(zz),Int(xx)]) ; xPhys3D[1+thick,:,:]=Micro_xPhys[θTemp]'.*θTemp; xPhys3D_layers[1+thick,:,:,θTemp]=Micro_xPhys[θTemp]';
                    θTemp=Int(aZX[Int((yy-1)*2+2)][Int(zz),Int(xx)]) ; xPhys3D[end-thick,:,:]=Micro_xPhys[θTemp]'.*θTemp; xPhys3D_layers[end-thick,:,:,θTemp]=Micro_xPhys[θTemp]';
                end
                xPhys3D1=reshape(xPhys3D,sizeMicro,sizeMicro,sizeMicro,1,1)
                xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
                if Macro_xPhys[yy,xx,zz]<0.5
                    xPhys3D1.=0.0
                end

                indI=Int((yy-1)*size3D+1 )
                indJ=Int((xx-1)*size3D+1 )
                indK=Int((zz-1)*size3D+1 )
                Macro_xPhys_det[ indI:indI+size3D-1 , indJ:indJ+size3D-1, indK:indK+size3D-1].=xPhys3D1 
                for θTemp in 1:θ
                    xPhys3D1=reshape(xPhys3D_layers[:,:,:,θTemp],sizeMicro,sizeMicro,sizeMicro,1,1)
                    xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
                    if Macro_xPhys[yy,xx,zz]<0.5
                        xPhys3D1.=0.0
                    end

                    indI=Int((yy-1)*size3D+1 )
                    indJ=Int((xx-1)*size3D+1 )
                    indK=Int((zz-1)*size3D+1 )
                    Macro_xPhys_det_layers[ indI:indI+size3D-1 , indJ:indJ+size3D-1, indK:indK+size3D-1,θTemp].=xPhys3D1 
                end


            end
        end
    end
    # display(Plots.heatmap(1 .-Macro_xPhys_det[:,:,1],showaxis = false,xaxis=nothing,yaxis=nothing,fc=:greys,aspect_ratio=:equal))
    ##### draw 2D
    Macro_nely=size(Macro_xPhys)[1];Macro_nelx=size(Macro_xPhys)[2];Macro_nelz=size(Macro_xPhys)[3];
    nely=size(Macro_xPhys_det)[1];nelx=size(Macro_xPhys_det)[2];nelz=size(Macro_xPhys_det)[3];
    size3D=Int(nely/Macro_nely)
    scaleFig=40
    gr(size=(400,300))
    gr(size=(scaleFig*(Macro_nelz+1)*2,scaleFig*Macro_nely+5))
    

    slice=zeros(nely,Int(size3D/2))
    zSlices=slice
    for i=0:Macro_nelz-1
        zSlices=hcat(zSlices,Macro_xPhys_det[:,:,Int(i*size3D)+1])
        zSlices=hcat(zSlices,slice)
    end
    zSlices=hcat(zSlices,Macro_xPhys_det[:,:,end])
    # gr(size=(scaleFig*(Macro_nelz+1)*2,scaleFig*Macro_nely+5))
    display(Plots.heatmap(zSlices,title="YX",showaxis = false,legend=false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cschemes1,aspect_ratio=:equal))

    slice=zeros(nelz,Int(size3D/2))
    xSlices=slice
    for i=0:Macro_nelx-1
        xSlices=hcat(xSlices,Macro_xPhys_det[:,Int(i*size3D)+1,:]')
        xSlices=hcat(xSlices,slice)
    end
    xSlices=hcat(xSlices,Macro_xPhys_det[:,end,:]')
    # gr(size=(scaleFig*(Macro_nelx+1)*2,scaleFig*Macro_nelz+5))
    display(Plots.heatmap(xSlices,title="ZY",showaxis = false,legend=false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cschemes1,aspect_ratio=:equal))

    slice=zeros(nelz,Int(size3D/2))
    ySlices=slice
    for i=0:Macro_nely-1
        ySlices=hcat(ySlices,Macro_xPhys_det[Int(i*size3D)+1,:,:]')
        ySlices=hcat(ySlices,slice)
    end
    ySlices=hcat(ySlices,Macro_xPhys_det[end,:,:]')
    # gr(size=(scaleFig*(Macro_nely+1)*2,scaleFig*Macro_nelz+5))
    display(Plots.heatmap(ySlices,title="ZX",showaxis = false,legend=false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cschemes1,aspect_ratio=:equal))
    
    
    display(Plots.heatmap(1 .-(zSlices.>0),title="YX",showaxis = false,legend=false,xaxis=nothing,yaxis=nothing,clims=(0.0, 1),fc=:greys,aspect_ratio=:equal))
    display(Plots.heatmap(1 .-(xSlices.>0),title="ZY",showaxis = false,legend=false,xaxis=nothing,yaxis=nothing,clims=(0.0, 1),fc=:greys,aspect_ratio=:equal))
    display(Plots.heatmap(1 .-(ySlices.>0),title="ZX",showaxis = false,legend=false,xaxis=nothing,yaxis=nothing,clims=(0.0, 1),fc=:greys,aspect_ratio=:equal))
    gr(size=(400,300))

    ######
    
    scene= GLMakie.volume( permutedims(Macro_xPhys_det[:,:,:].>0, [2, 1, 3]), algorithm = :iso, isorange = 0.3, isovalue = 1.0,colormap=:grays)
    display(scene)
    return Macro_xPhys_det,Macro_xPhys_det_layers ,cscheme2

end


function assembleFull3DStructureCuboct(Macro_xPhys,fileName,size3D=20,thickness=1,mirrorZ=false)
    # thresh=0.5
    # thresh1=0.75
    # θ=maximum(a)
    cuboct=load("./img/library/1/cuboct_vol_$((0.25)).jld")["data"];
    cuboct3D ,cuboctDH3=visualize3D2DMicrostructure(cuboct,false)
    
    sizeMicro=size(cuboct3D)[1]
    Macro_xPhys_det=zeros(size(Macro_xPhys)[1]*size3D,size(Macro_xPhys)[2]*size3D,size(Macro_xPhys)[3]*size3D);
    # repeat(Micro_xPhys2[1]',1,1,2)
    for yy in 1:size(Macro_xPhys)[1]
        for xx in 1:size(Macro_xPhys)[2]
            for zz in 1:size(Macro_xPhys)[3]
                θTempList="";DH="";

                xPhys3D1=reshape(cuboct3D,sizeMicro,sizeMicro,sizeMicro,1,1)
                xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
                if Macro_xPhys[yy,xx,zz]<0.5
                    xPhys3D1.=0.0
                end

                indI=Int((yy-1)*size3D+1 )
                indJ=Int((xx-1)*size3D+1 )
                indK=Int((zz-1)*size3D+1 )
                Macro_xPhys_det[ indI:indI+size3D-1 , indJ:indJ+size3D-1, indK:indK+size3D-1].=xPhys3D1 


            end
        end
    end
    # display(Plots.heatmap(1 .-Macro_xPhys_det[:,:,1],showaxis = false,xaxis=nothing,yaxis=nothing,fc=:greys,aspect_ratio=:equal))
    ##### draw 2D
    Macro_nely=size(Macro_xPhys)[1];Macro_nelx=size(Macro_xPhys)[2];Macro_nelz=size(Macro_xPhys)[3];
    nely=size(Macro_xPhys_det)[1];nelx=size(Macro_xPhys_det)[2];nelz=size(Macro_xPhys_det)[3];
    size3D=Int(nely/Macro_nely)
    scaleFig=40
    gr(size=(400,300))
    gr(size=(scaleFig*(Macro_nelz+1)*2,scaleFig*Macro_nely+5))
    

    slice=zeros(nely,Int(size3D/2))
    zSlices=slice
    for i=0:Macro_nelz-1
        zSlices=hcat(zSlices,Macro_xPhys_det[:,:,Int(i*size3D)+1])
        zSlices=hcat(zSlices,slice)
    end
    zSlices=hcat(zSlices,Macro_xPhys_det[:,:,end])
    # gr(size=(scaleFig*(Macro_nelz+1)*2,scaleFig*Macro_nely+5))

    slice=zeros(nelz,Int(size3D/2))
    xSlices=slice
    for i=0:Macro_nelx-1
        xSlices=hcat(xSlices,Macro_xPhys_det[:,Int(i*size3D)+1,:]')
        xSlices=hcat(xSlices,slice)
    end
    xSlices=hcat(xSlices,Macro_xPhys_det[:,end,:]')
    # gr(size=(scaleFig*(Macro_nelx+1)*2,scaleFig*Macro_nelz+5))

    slice=zeros(nelz,Int(size3D/2))
    ySlices=slice
    for i=0:Macro_nely-1
        ySlices=hcat(ySlices,Macro_xPhys_det[Int(i*size3D)+1,:,:]')
        ySlices=hcat(ySlices,slice)
    end
    ySlices=hcat(ySlices,Macro_xPhys_det[end,:,:]')
    # gr(size=(scaleFig*(Macro_nely+1)*2,scaleFig*Macro_nelz+5))
    
    
    display(Plots.heatmap(1 .-(zSlices.>0),title="YX",showaxis = false,legend=false,xaxis=nothing,yaxis=nothing,clims=(0.0, 1),fc=:greys,aspect_ratio=:equal))
    display(Plots.heatmap(1 .-(xSlices.>0),title="ZY",showaxis = false,legend=false,xaxis=nothing,yaxis=nothing,clims=(0.0, 1),fc=:greys,aspect_ratio=:equal))
    display(Plots.heatmap(1 .-(ySlices.>0),title="ZX",showaxis = false,legend=false,xaxis=nothing,yaxis=nothing,clims=(0.0, 1),fc=:greys,aspect_ratio=:equal))
    gr(size=(400,300))

    ######
    
    scene= GLMakie.volume( permutedims(Macro_xPhys_det[:,:,:].>0, [2, 1, 3]), algorithm = :iso, isorange = 0.3, isovalue = 1.0,colormap=:grays)
    display(scene)

    if mirrorZ
        Macro_xPhys_det_b=zeros(size(Macro_xPhys_det)[1],size(Macro_xPhys_det)[2],size(Macro_xPhys_det)[3]*2)
        Macro_xPhys_det_b[:,:,Int.(1:size(Macro_xPhys_det)[3])].=Macro_xPhys_det[:,:,end:-1:1]
        Macro_xPhys_det_b[:,:,Int.(size(Macro_xPhys_det)[3]+1:end)].= Macro_xPhys_det
        Macro_xPhys_det=Macro_xPhys_det_b
    end

    Makie.inline!(true)
    nely=size(Macro_xPhys_det)[1]*1.0
    nelx=size(Macro_xPhys_det)[2]
    nelz=size(Macro_xPhys_det)[3]
    voxx=zeros(2+size(Macro_xPhys_det)[1],2+size(Macro_xPhys_det)[2],2+size(Macro_xPhys_det)[3])
    Macro_xPhys_detTemp=copy(Macro_xPhys_det)
    # Macro_xPhys_detTemp[Macro_xPhys_det.<0.6].=0.0
    # Macro_xPhys_detTemp[Macro_xPhys_det.>=0.6].=1.0
    voxx[2:end-1,2:end-1,2:end-1].=Macro_xPhys_detTemp
    points,faces = isosurface(1.0 .-voxx, MarchingCubes(iso=0.8),origin=SVector(-nely/2,-nelx/2,-nelz/2), widths = SVector(nely,nelx,nelz))
    pointss=reshape(collect(Iterators.flatten(points)),3,length(points))
    facess=reshape(collect(Iterators.flatten(faces)),3,length(faces))
    mm=TriMesh([pointss], [facess])
    Makie.inline!(true)
    display(Flux3D.visualize(mm))
    save_trimesh("./objs/$(fileName)_cubcot.stl",mm)


    return Macro_xPhys_det

end

function exportFull3DStructureObj(Macro_xPhys_det,Macro_xPhys_det_layers ,cscheme,fileName,mirrorZ=false,D3D=false)

    if mirrorZ
        Macro_xPhys_det_b=zeros(size(Macro_xPhys_det)[1],size(Macro_xPhys_det)[2],size(Macro_xPhys_det)[3]*2)
        Macro_xPhys_det_b[:,:,Int.(1:size(Macro_xPhys_det)[3])].=Macro_xPhys_det[:,:,end:-1:1]
        Macro_xPhys_det_b[:,:,Int.(size(Macro_xPhys_det)[3]+1:end)].= Macro_xPhys_det
        Macro_xPhys_det=Macro_xPhys_det_b
        Macro_xPhys_det_layers_b=zeros(size(Macro_xPhys_det_layers)[1],size(Macro_xPhys_det_layers)[2],size(Macro_xPhys_det_layers)[3]*2,size(Macro_xPhys_det_layers)[4])
        Macro_xPhys_det_layers_b[:,:,Int.(1:size(Macro_xPhys_det_layers)[3]),:].=Macro_xPhys_det_layers[:,:,end:-1:1,:]
        Macro_xPhys_det_layers_b[:,:,Int.(size(Macro_xPhys_det_layers)[3]+1:end),:].= Macro_xPhys_det_layers
        Macro_xPhys_det_layers=Macro_xPhys_det_layers_b
    end
    if !D3D
        num=5
        t=:matter
        cschemes=cgrad(t, num+1, categorical = true)
        cschemeLists=[]
        for i=num:-1:1
            append!(cschemeLists,[cschemes[i]])
        end
        for i=2:num
            append!(cschemeLists,[cschemes[i]])
        end
        for i=num-1:-1:1
            append!(cschemeLists,[cschemes[i]])
        end
        for i=2:num
            append!(cschemeLists,[cschemes[i]])
        end
        append!(cschemeLists,[:black])
        cschemes1=cgrad(cschemeLists)

        cscheme=cschemes1
        issso=0.8
    else
        issso=0.6
    end

    Makie.inline!(true)
    nely=size(Macro_xPhys_det)[1]*1.0
    nelx=size(Macro_xPhys_det)[2]
    nelz=size(Macro_xPhys_det)[3]
    voxx=zeros(2+size(Macro_xPhys_det)[1],2+size(Macro_xPhys_det)[2],2+size(Macro_xPhys_det)[3])
    Macro_xPhys_detTemp=copy(Macro_xPhys_det)
    # Macro_xPhys_detTemp[Macro_xPhys_det.<0.6].=0.0
    # Macro_xPhys_detTemp[Macro_xPhys_det.>=0.6].=1.0
    voxx[2:end-1,2:end-1,2:end-1].=Macro_xPhys_detTemp
    points,faces = isosurface(1.0 .-voxx, MarchingCubes(iso=issso),origin=SVector(-nely/2,-nelx/2,-nelz/2), widths = SVector(nely,nelx,nelz))
    pointss=reshape(collect(Iterators.flatten(points)),3,length(points))
    facess=reshape(collect(Iterators.flatten(faces)),3,length(faces))
    mm=TriMesh([pointss], [facess])
    Makie.inline!(true)
    display(Flux3D.visualize(mm))
    save_trimesh("./objs/$(fileName)_full.stl",mm)
    count=1
    θ=length(cscheme)
    fig="";ax="";

    if mirrorZ
        fig = Figure(resolution=(600, 600), fontsize=10)
        ax=Axis3(fig[1, 1],aspect = (1,1,1))
    end
               
    for i= 1:θ
        if sum(Macro_xPhys_det_layers[:,:,:,i])!=0
            # display("layer $i not empty")
            voxx=zeros(2+size(Macro_xPhys_det_layers[:,:,:,i])[1],2+size(Macro_xPhys_det_layers[:,:,:,i])[2],2+size(Macro_xPhys_det_layers[:,:,:,i])[3])
            Macro_xPhys_detTemp=copy(Macro_xPhys_det_layers[:,:,:,i])
            # Macro_xPhys_detTemp[Macro_xPhys_det_layers[:,:,:,i].<0.6].=0.0
            # Macro_xPhys_detTemp[Macro_xPhys_det_layers[:,:,:,i].>=0.6].=1.0
            voxx[2:end-1,2:end-1,2:end-1].=Macro_xPhys_detTemp
            points,faces = isosurface(1.0 .-voxx, MarchingCubes(iso=issso),origin=SVector(-nely/2,-nelx/2,-nelz/2), widths = SVector(nely,nelx,nelz))
            pointss=reshape(collect(Iterators.flatten(points)),3,length(points))
            facess=reshape(collect(Iterators.flatten(faces)),3,length(faces))
            mm=TriMesh([pointss], [facess])
            Makie.inline!(true)
            # display(Flux3D.visualize(mm))
            r=Int(round(cscheme[i].r*255))
            g=Int(round(cscheme[i].g*255))
            b=Int(round(cscheme[i].b*255))
            

            save_trimesh("./objs/$(fileName)_$(i)_r_$(r)_g_$(g)_b_$(b).stl",mm)
            brain = load("./objs/$(fileName)_$(i)_r_$(r)_g_$(g)_b_$(b).stl")
            if count==1
                if mirrorZ
                    m=mesh!(ax,brain,color = cscheme[i])
                else
                    m=mesh(brain,color = cscheme[i],figure = (resolution = (500, 500),),shading = false,axis=(aspect = :data,))
                end
                count+=1
            else
                if mirrorZ
                    m=mesh!(ax,brain,color = cscheme[i])
                else
                    m=mesh!(brain,color = cscheme[i])
                end
                # m=mesh!(ax,brain,color = cscheme[i])
            end
        else
            # display("layer $i empty")
        end
    end
    display(current_figure())
    if mirrorZ
        display(fig)
    end
    Makie.inline!(false)
    display(current_figure())
    if mirrorZ
        display(fig)
    end
    Makie.inline!(true)


end

function assembleFull3DStructure3D(θ,Macro_xPhys,Micro_xPhys,DHs,Macro_masks,size3D=20,mirrorZ=false)
    # thresh=0.5
    # thresh1=0.75
    # θ=maximum(a)

    t=:darktest
    cscheme1=cgrad(t, θ, categorical = true)
    cschemeList=[]
    for i=1:θ
        append!(cschemeList,[cscheme1[i]])
    end
    cscheme11=cgrad(cschemeList)
    

    # for i=1:θ
    #     Micro_xPhys[i][Micro_xPhys[i].<0.5].=0.0
    #     Micro_xPhys[i][Micro_xPhys[i].>=0.5].=1.0
    # end
    
    sizeMicro=size(Micro_xPhys[1])[1]
    Macro_xPhys_det=zeros(size(Macro_xPhys)[1]*size3D,size(Macro_xPhys)[2]*size3D,size(Macro_xPhys)[3]*size3D);
    Macro_xPhys_det_layers=repeat(Macro_xPhys_det,1,1,1,θ)
    # repeat(Micro_xPhys2[1]',1,1,2)
    for yy in 1:size(Macro_xPhys)[1]
        for xx in 1:size(Macro_xPhys)[2]
            for zz in 1:size(Macro_xPhys)[3]
                for θtemp in 1:θ
                    if Macro_masks[θtemp][yy,xx,zz]==1
                        xPhys3D1=Micro_xPhys[θtemp]

                        ss=2
                        xPhys3D1=addFabricationConstraints(ss,0,Micro_xPhys[θtemp],size3D,size3D,size3D,false)

                        if Macro_xPhys[yy,xx,zz]<0.5
                            xPhys3D1.=0.0
                        end

                        indI=Int((yy-1)*size3D+1 )
                        indJ=Int((xx-1)*size3D+1 )
                        indK=Int((zz-1)*size3D+1 )
                        Macro_xPhys_det[ indI:indI+size3D-1 , indJ:indJ+size3D-1, indK:indK+size3D-1].=xPhys3D1
                        Macro_xPhys_det_layers[ indI:indI+size3D-1 , indJ:indJ+size3D-1, indK:indK+size3D-1,θtemp].=xPhys3D1
                    end
                end
            end
        end
    end

    # Micro_xPhys_Meshes=[]

    # for i in 1:θ
    #     xPhys=Micro_xPhys[i]

    #     nely=size(xPhys)[1]
    #     nelx=size(xPhys)[2]
    #     nelz=size(xPhys)[3]
    #     voxx=zeros(2+size(xPhys)[1],2+size(xPhys)[2],2+size(xPhys)[3])

    #     Macro_xPhys_detTemp=copy(xPhys)
    #     voxx[2:end-1,2:end-1,2:end-1].=Macro_xPhys_detTemp
    #     points,faces = isosurface(1.0 .-voxx, MarchingCubes(iso=0.4),origin=SVector(-nely/2,-nelx/2,-nelz/2), widths = SVector(nely*1.0,nelx,nelz))
    #     pointss=reshape(collect(Iterators.flatten(points)),3,length(points))
    #     facess=reshape(collect(Iterators.flatten(faces)),3,length(faces))
    #     mm=TriMesh([pointss], [facess])
    #     fileName="mm"
    #     save_trimesh("./objs/3D/$(fileName).stl",mm)
    #     mm = load("./objs/3D/$(fileName).stl");
    #     append!(Micro_xPhys_Meshes,[mm])
    # end
    # ny=size(Macro_xPhys)[1];nx=size(Macro_xPhys)[2];nz=size(Macro_xPhys)[3];

    # fig = Figure( fontsize=10)
    # ax=Axis3(fig[1, 1],aspect = :data,perspectiveness = 1.0,textsize = 10,limits = (0, ny+1, 0, nx+1, -nz-1, nz+1))
    # hidedecorations!(ax)
    # for ii in 1:θ
    #     mask=copy(Macro_masks[ii])
    #     mask[Macro_xPhys.<0.5].=0
    #     ny=size(mask)[1];nx=size(mask)[2];nz=size(mask)[3];

    #     xs=ones(Int(sum(mask)));ys=ones(Int(sum(mask)));zs=ones(Int(sum(mask)));
    #     count=1;
    #     for k in 1:nz
    #             for i in 1:ny
    #                 for j in 1:nx
    #                 if mask[i,j,k]==1.0
    #                     xs[count]=i;ys[count]=j;zs[count]=k;
    #                     count+=1;
    #                 end
                        
    #             end
    #         end
    #     end
        
    #     Makie.meshscatter!(ax,xs, ys, zs.-1, markersize = 0.055, color = cscheme1[ii],marker=Micro_xPhys_Meshes[ii])
    #     Makie.meshscatter!(ax,xs, ys, -zs, markersize = 0.055, color = cscheme1[ii],marker=Micro_xPhys_Meshes[ii])
        
    # end
    # Makie.inline!(true)   
    # display(current_figure())
    # Makie.inline!(false)   
    # display(current_figure())
    # Makie.inline!(true) 
    
    return Macro_xPhys_det,Macro_xPhys_det_layers ,cscheme1

end
################################################################