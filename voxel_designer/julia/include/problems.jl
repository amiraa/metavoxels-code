# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

################################Problems###################


##################2D##################################
## problems
function poiss(nelx,nely)

    ndof= Int(2*(nelx+1)*(nely+1));

    load_x, load_y = Matlab.meshgrid([nelx/1], [nely/2]);
    loadnid  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    fixed_x, fixed_y = Matlab.meshgrid([0.0], 0.0:nely);
    fixednid  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixeddofs = vcat(2 .* fixednid[:] , 2 .*fixednid[:] .-1);
    F = sparse(Int.(2 .*loadnid[:]), [1], [-5.0], 2 .*(nelx .+1) .*(nely .+1),1);

    U = zeros(ndof,1);
    freedofs  = setdiff(1:ndof,fixeddofs);
    
    return U,F,freedofs
end

function cantl2D(nelx,nely)
    ndof= Int(2*(nelx+1)*(nely+1));

    F = sparse( [2 * (nelx+1)*(nely+1)], [1], [-1.], 2*(nely+1)*(nelx+1), 1);
    fixeddofs = 1 : 2*(nely+1);
    alldofs = 1 : 2*(nely+1)*(nelx+1);
    freedofs = setdiff(alldofs, fixeddofs);

    U = zeros(ndof,1);
    return U,F,freedofs

end

function MBB(nelx,nely)

    F = sparse([2],[1],[-1.0],2*(nely+1)*(nelx+1),1)
    U = zeros(2*(nely+1)*(nelx+1),1)
    fixeddofs = union(1:2:2*(nely+1),2*(nelx+1)*(nely+1))
    alldofs = 1:(2*(nely+1)*(nelx+1))
    freedofs = setdiff(alldofs,fixeddofs)
    
    return U,F,freedofs
end

function Bridge(nelx,nely)

    F = sparse([2],[1],[-1.0],2*(nely+1)*(nelx+1),1)
    U = zeros(2*(nely+1)*(nelx+1),1)

    LeftBottomNode=getIndex(1,nely+1,nelx,nely)
    Index=vcat(LeftBottomNode,LeftBottomNode)
    Direction=ones(Int,length(Index)).+1
    Direction[length(Index)]=1
    Direction[length(Index)-1]=1
    Support=[Index Direction]
    fixeddofs = union(Support,2*(nelx+1)*(nely+1))
    display(fixeddofs)

    alldofs = 1:(2*(nely+1)*(nelx+1))
    freedofs = setdiff(alldofs,fixeddofs)
    
    return U,F,freedofs
end

#only normal
function microStructureConnection_normal1(nelx,nely)
    ndof= Int(2*(nelx+1)*(nely+1));

    load_x, load_y = Matlab.meshgrid([nelx/1], [nely/2]);
    loadnid1  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([nelx/1], [nely/2+1.0]);
    loadnid2  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([nelx/1], [nely/2-1.0]);
    loadnid3  = load_x .*(nely .+1) .+(nely .+1 .-load_y);

    load_x, load_y = Matlab.meshgrid([nelx/2], [nely/1]);
    loadnid4  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([nelx/2+1.0], [nely/1]);
    loadnid5  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([nelx/2-1.0], [nely/1]);
    loadnid6  = load_x .*(nely .+1) .+(nely .+1 .-load_y);


    loadnid=vcat(loadnid1,loadnid2,loadnid3,loadnid4,loadnid5,loadnid6);
    direction=[1.0 ;1.0;1.0;0.0;0.0;0.0];
    value=[-0.1;-0.1;-0.1;0.1;0.1;0.1];
    # fixed_x, fixed_y = Matlab.meshgrid([0.0], 0.0:nely);
    fixed_x, fixed_y = Matlab.meshgrid([0.0], [nely/2]);
    fixednid1  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([0.0], [nely/2+1]);
    fixednid2  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([0.0], [nely/2-1]);
    fixednid3  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);

    fixed_x, fixed_y = Matlab.meshgrid([nelx/2], [0.0]);
    fixednid4  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([nelx/2+1], [0.0]);
    fixednid5  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([nelx/2-1], [0.0]);
    fixednid6  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);

    fixednid=[fixednid1;fixednid2;fixednid3;fixednid4;fixednid5;fixednid6];

    fixeddofs = vcat(2 .* fixednid[:] , 2 .*fixednid[:] .-1);


    F = sparse(Int.(2.0 .*loadnid[:]+direction),ones(length(loadnid[:])), value, 2 .*(nelx .+1) .*(nely .+1),1);

    U = zeros(ndof,1);
    freedofs  = setdiff(1:ndof,fixeddofs);
    
    return U,F,freedofs
    
end


#only normal
function microStructureConnection_normal2(nelx,nely)
    ndof= Int(2*(nelx+1)*(nely+1));
    
    load_x, load_y = Matlab.meshgrid([0.0], [nely/2]);
    loadnid1  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([0.0], [nely/2+1]);
    loadnid2  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([0.0], [nely/2-1]);
    loadnid3  = load_x .*(nely .+1) .+(nely .+1 .-load_y);


    load_x, load_y = Matlab.meshgrid([nelx/2], [0.0]);
    loadnid4  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([nelx/2+1], [0.0]);
    loadnid5  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([nelx/2-1], [0.0]);
    loadnid6  = load_x .*(nely .+1) .+(nely .+1 .-load_y);


    loadnid=vcat(loadnid1,loadnid2,loadnid3,loadnid4,loadnid5,loadnid6);
    direction=[1.0 ;1.0;1.0;0.0;0.0;0.0];
    value=[-0.1;-0.1;-0.1;0.1;0.1;0.1];
    # fixed_x, fixed_y = Matlab.meshgrid([0.0], 0.0:nely);
    fixed_x, fixed_y = Matlab.meshgrid([nelx/1], [nely/2]);
    fixednid1  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([nelx/1], [nely/2+1.0]);
    fixednid2  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([nelx/1], [nely/2-1.0]);
    fixednid3  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    
    
    fixed_x, fixed_y = Matlab.meshgrid([nelx/2], [nely/1]);
    fixednid4  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([nelx/2+1.0], [nely/1]);
    fixednid5  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([nelx/2-1.0], [nely/1]);
    fixednid6  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);

    fixednid=[fixednid1;fixednid2;fixednid3;fixednid4;fixednid5;fixednid6];

    fixeddofs = vcat(2 .* fixednid[:] , 2 .*fixednid[:] .-1);


    F = sparse(Int.(2.0 .*loadnid[:]+direction),ones(length(loadnid[:])), value, 2 .*(nelx .+1) .*(nely .+1),1);

    U = zeros(ndof,1);
    freedofs  = setdiff(1:ndof,fixeddofs);
    
    return U,F,freedofs
    
end

# cuboct
function microStructureConnection_shear1(nelx,nely)
    ndof= Int(2*(nelx+1)*(nely+1));

    load_x, load_y = Matlab.meshgrid([nelx/2], [nely/1]); 
    loadnid1  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([nelx/2+1.0], [nely/1]);
    loadnid2  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([nelx/2-1.0], [nely/1]);
    loadnid3  = load_x .*(nely .+1) .+(nely .+1 .-load_y);

    load_x, load_y =  Matlab.meshgrid([nelx/2], [0.0]);
    loadnid4  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y =  Matlab.meshgrid([nelx/2+1], [0.0]);
    loadnid5  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y =  Matlab.meshgrid([nelx/2-1], [0.0]);
    loadnid6  = load_x .*(nely .+1) .+(nely .+1 .-load_y);


    loadnid=vcat(loadnid1,loadnid2,loadnid3,loadnid4,loadnid5,loadnid6);
    direction=[0.0 ;0.0; 0.0;0.0;0.0;0.0];
    value=[-0.1;-0.1;-0.1;-0.1;-0.1;-0.1];
    direction=[1.0 ;1.0; 1.0;1.0;1.0;1.0];
    value=[-0.1;-0.1;-0.1;0.1;0.1;0.1];


    # fixed_x, fixed_y = Matlab.meshgrid([0.0], 0.0:nely);
    fixed_x, fixed_y =Matlab.meshgrid([nelx/1], [nely/2]);
    fixednid1  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y =  Matlab.meshgrid([nelx/1], [nely/2+1.0]);
    fixednid2  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y =  Matlab.meshgrid([nelx/1], [nely/2-1.0]);
    fixednid3  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);


    fixed_x, fixed_y =Matlab.meshgrid([0.0], [nely/2]);
    fixednid4  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([0.0], [nely/2+1]);
    fixednid5  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([0.0], [nely/2-1]);
    fixednid6  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);

    fixednid=[fixednid1;fixednid2;fixednid3;fixednid4;fixednid5;fixednid6];

    fixeddofs = vcat(2 .* fixednid[:] , 2 .*fixednid[:] .-1);


    F = sparse(Int.(2.0 .*loadnid[:]+direction),ones(length(loadnid[:])), value, 2 .*(nelx .+1) .*(nely .+1),1);

    U = zeros(ndof,1);
    freedofs  = setdiff(1:ndof,fixeddofs);
    
    return U,F,freedofs
    
end

# cuboct
function microStructureConnection_shear2(nelx,nely)
    ndof= Int(2*(nelx+1)*(nely+1));

    load_x, load_y = Matlab.meshgrid([nelx/1], [nely/2]);
    loadnid1  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([nelx/1], [nely/2+1.0]);
    loadnid2  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y = Matlab.meshgrid([nelx/1], [nely/2-1.0]);
    loadnid3  = load_x .*(nely .+1) .+(nely .+1 .-load_y);

    load_x, load_y = Matlab.meshgrid([0.0], [nely/2]);
    loadnid4  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y =  Matlab.meshgrid([0.0], [nely/2+1]);
    loadnid5  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    load_x, load_y =  Matlab.meshgrid([0.0], [nely/2-1]);
    loadnid6  = load_x .*(nely .+1) .+(nely .+1 .-load_y);


    loadnid=vcat(loadnid1,loadnid2,loadnid3,loadnid4,loadnid5,loadnid6);
    direction=[0.0 ;0.0; 0.0;0.0; 0.0;0.0];
    value=[-0.1;-0.1;-0.1;0.1;0.1;0.1];


    # fixed_x, fixed_y = Matlab.meshgrid([0.0], 0.0:nely);
    fixed_x, fixed_y =Matlab.meshgrid([nelx/2], [nely/1]);
    fixednid1  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([nelx/2+1.0], [nely/1]);
    fixednid2  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([nelx/2-1.0], [nely/1]);
    fixednid3  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);


    fixed_x, fixed_y = Matlab.meshgrid([nelx/2], [0.0]);
    fixednid4  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([nelx/2+1], [0.0]);
    fixednid5  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixed_x, fixed_y = Matlab.meshgrid([nelx/2-1], [0.0]);
    fixednid6  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);


    fixednid=[fixednid1;fixednid2;fixednid3;fixednid4;fixednid5;fixednid6];

    fixeddofs = vcat(2 .* fixednid[:] , 2 .*fixednid[:] .-1);


    F = sparse(Int.(2.0 .*loadnid[:]+direction),ones(length(loadnid[:])), value, 2 .*(nelx .+1) .*(nely .+1),1);

    U = zeros(ndof,1);
    freedofs  = setdiff(1:ndof,fixeddofs);
    
    return U,F,freedofs
    
end



function inverter2D(Macro_nelx,Macro_nely)
    function getIndex(i,j,nelx,nely)
        return (i-1)*(nely+1)+(j-1)+1
    end

    # DEFINE LOADS AND SUPPORTS 
    LeftUpperNode=getIndex(1,1,Macro_nelx,Macro_nely)
    LeftEdgeNodes = 1:Macro_nely+1
    RigthBottomNode=getIndex(Macro_nelx+1,Macro_nely+1,Macro_nelx,Macro_nely)
    LeftBottomNode=getIndex(1,Macro_nely+1,Macro_nelx,Macro_nely)
    UpperEdgeNodes= 1:Macro_nely+1:getIndex(Macro_nelx+1,1,Macro_nelx,Macro_nely)
    LeftUpperNode= getIndex(1,1,Macro_nelx,Macro_nely)
    RightUpperNode= getIndex(Macro_nelx+1,1,Macro_nelx,Macro_nely)

    Index=[LeftUpperNode];Direction=[1];Value=[0.5]; #direction 1 if x and 2 if y
    Load=[Index Direction Value]

    Index=vcat(Array(UpperEdgeNodes),LeftBottomNode,LeftBottomNode)
    Direction=ones(Int,length(Index)).+1
    Direction[length(Index)]=1
    Direction[length(Index)-1]=1
    Support=[Index Direction]

    Index=vcat(Array(UpperEdgeNodes),LeftBottomNode)
    Direction=ones(Int,length(Index)).+1
    Direction[length(Index)]=1
    Support=[Index Direction]

    DOut=[RightUpperNode,1] #Index,Direction 1 if x and 2 if y

    Index=union(LeftUpperNode,RightUpperNode);
    Direction=ones(Int,length(Index))
    Value=[1.0 ,1.0] # k values.. 1.0 for now
    Spring=[Index Direction Value]

    F = sparse(2 .*Load[:,1] .-2 .+ Load[:,2],ones(size(Load,1)),Load[:,3],2*(Macro_nely+1)*(Macro_nelx+1),2)
    DofDOut = 2 * DOut[1] - 2 +DOut[2]; #only one
    F=Array(F)
    F[DofDOut,2]=-1
    fixeddofs = 2 .*Support[:,1] .-2 .+ Support[:,2]
    NSpring = size(Spring,1);
    s = sparse(2 .*Spring[:,1] .-2 .+ Spring[:,2],ones(size(Spring,1)),Spring[:,3],2*(Macro_nely+1)*(Macro_nelx+1),2)
    m=Array(s)[:,1]
    S= sparse(diagm(m))
    U = zeros(2*(Macro_nely+1)*(Macro_nelx+1),2)
    alldofs = 1:(2*(Macro_nely+1)*(Macro_nelx+1))
    freedofs = setdiff(alldofs,fixeddofs)

    return U,F,freedofs,DofDOut,S
end

function wing2D(Macro_nelx,Macro_nely)
    function getIndex(i,j,nelx,nely)
        return (i-1)*(nely+1)+(j-1)+1
    end

    # DEFINE LOADS AND SUPPORTS 
    LeftUpperNode=getIndex(1,1,Macro_nelx,Macro_nely)
    LeftEdgeNodes = 1:Macro_nely+1
    RigthBottomNode=getIndex(Macro_nelx+1,Macro_nely+1,Macro_nelx,Macro_nely)
    LeftBottomNode=getIndex(1,Macro_nely+1,Macro_nelx,Macro_nely)
    UpperEdgeNodes= 1:Macro_nely+1:getIndex(Macro_nelx+1,1,Macro_nelx,Macro_nely)
    LeftUpperNode= getIndex(1,1,Macro_nelx,Macro_nely)
    RightUpperNode= getIndex(Macro_nelx+1,1,Macro_nelx,Macro_nely)
    RightMiddleNode= getIndex(Macro_nelx+1,Int((Macro_nely)/2),Macro_nelx,Macro_nely)

    LowerMiddleNode=getIndex(Int((Macro_nelx)/2),Macro_nely+1,Macro_nelx,Macro_nely)
    UpperMiddleNode=getIndex(Int((Macro_nelx)/2),1,Macro_nelx,Macro_nely)


    din=LeftUpperNode
    din=UpperMiddleNode
    dindir=1 #Direction 1 if x and 2 if y


    dout=RightUpperNode
    dout=RigthBottomNode
    dout=RightMiddleNode
    doutdir=2 #Direction 1 if x and 2 if y

    Index=vcat(LeftBottomNode,LeftBottomNode)
    Direction=[1,2]
    Support=[Index Direction]
    


    Index=[din];
    Direction=[dindir];
    Value=[0.1]; #direction 1 if x and 2 if y
    Load=[Index Direction Value]

    DOut=[dout,doutdir] #Index,Direction 1 if x and 2 if y

    Index=union(din,dout);
    Direction=[dindir,doutdir]
    Value=[1.0 ,1.0] # k values.. 1.0 for now
    Spring=[Index Direction Value]

    F = sparse(2 .*Load[:,1] .-2 .+ Load[:,2],ones(size(Load,1)),Load[:,3],2*(Macro_nely+1)*(Macro_nelx+1),2)
    DofDOut = 2 * DOut[1] - 2 +DOut[2]; #only one
    F=Array(F)
    F[DofDOut,2]=-1


    fixeddofs = 2 .*Support[:,1] .-2 .+ Support[:,2]
    fixeddofs=sort(fixeddofs)
    NSpring = size(Spring,1);
    s = sparse(2 .*Spring[:,1] .-2 .+ Spring[:,2],ones(size(Spring,1)),Spring[:,3],2*(Macro_nely+1)*(Macro_nelx+1),2)
    m=Array(s)[:,1]
    S= sparse(diagm(m))
    U = zeros(2*(Macro_nely+1)*(Macro_nelx+1),2)
    alldofs = 1:(2*(Macro_nely+1)*(Macro_nelx+1))
    freedofs = setdiff(alldofs,fixeddofs)

    return U,F,freedofs,DofDOut,S
end

function normalTrial(nelx,nely)

    ndof= Int(2*(nelx+1)*(nely+1));

    #load_x, load_y = Matlab.meshgrid([nelx/1.0], [nely/2]);
    load_x, load_y = Matlab.meshgrid(0.0:nelx, [0.0]);
    loadnid  = load_x .*(nely .+1) .+(nely .+1 .-load_y);
    direction=2
    value=-0.01
    
    fixed_x, fixed_y = Matlab.meshgrid(0.0:nelx, [nely.*1.0]);
    fixednid  = fixed_x .*(nely+1) .+(nely .+1 .-fixed_y);
    fixeddofs = vcat(2 .* fixednid[:] , 2 .*fixednid[:] .-1); #in all directions
    
    F = sparse(Int.(direction .*loadnid[:]), ones(length(loadnid[:])), ones(length(loadnid[:])).*value, 2 .*(nelx .+1) .*(nely .+1),1);

    U = zeros(ndof,1);
    freedofs  = setdiff(1:ndof,fixeddofs);
    
    return U,F,freedofs
end

function seat(Macro_nelx,Macro_nely)
    function getIndex(i,j,nelx,nely)
        return (i-1)*(nely+1)+(j-1)+1
    end

    # DEFINE LOADS AND SUPPORTS 
    LeftUpperNode=getIndex(1,1,Macro_nelx,Macro_nely)
    LeftEdgeNodes = 1:Macro_nely+1
    RigthBottomNode=getIndex(Macro_nelx+1,Macro_nely+1,Macro_nelx,Macro_nely)
    LeftBottomNode=getIndex(1,Macro_nely+1,Macro_nelx,Macro_nely)
    UpperEdgeNodes= 1:Macro_nely+1:getIndex(Macro_nelx+1,1,Macro_nelx,Macro_nely)
    BottomEdgeNodes= Macro_nely+1:Macro_nely+1:getIndex(Macro_nelx+1,Macro_nely+1,Macro_nelx,Macro_nely)
    LeftUpperNode= getIndex(1,1,Macro_nelx,Macro_nely)
    RightUpperNode= getIndex(Macro_nelx+1,1,Macro_nelx,Macro_nely)
    RightMiddleNode= getIndex(Macro_nelx+1,Int((Macro_nely)/2),Macro_nelx,Macro_nely)
    LowerMiddleNode=getIndex(Int((Macro_nelx)/2),Macro_nely+1,Macro_nelx,Macro_nely)
    UpperMiddleNode=getIndex(Int((Macro_nelx)/2),1,Macro_nelx,Macro_nely)


    din=LeftBottomNode
    dindir=2 #Direction 1 if x and 2 if y


    dout=RigthBottomNode
    doutdir=2 #Direction 1 if x and 2 if y

    Index=vcat(UpperEdgeNodes,UpperEdgeNodes)
    Direction=ones(length(Index)).*2
    Direction[1:Int(length(Index)/2)].=1
    Support=[Index Direction]
    


    Index=[din];
    Direction=[dindir];
    Value=[-1.0]; #direction 1 if x and 2 if y
    Load=[Index Direction Value]

    DOut=[dout,doutdir] #Index,Direction 1 if x and 2 if y

    Index=union(din,dout);
    Direction=[dindir,doutdir]
    Value=[1.0 ,1.0] # k values.. 1.0 for now
    Spring=[Index Direction Value]

    F = sparse(2 .*Load[:,1] .-2 .+ Load[:,2],ones(size(Load,1)),Load[:,3],2*(Macro_nely+1)*(Macro_nelx+1),2)
    DofDOut = 2 * DOut[1] - 2 +DOut[2]; #only one
    F=Array(F)
    F[DofDOut,2]=-1
    F[DofDOut,2]=1


    fixeddofs = 2 .*Support[:,1] .-2 .+ Support[:,2]
    fixeddofs=sort(fixeddofs)
    NSpring = size(Spring,1);
    s = sparse(2 .*Spring[:,1] .-2 .+ Spring[:,2],ones(size(Spring,1)),Spring[:,3],2*(Macro_nely+1)*(Macro_nelx+1),2)
    m=Array(s)[:,1]
    S= sparse(diagm(m))
    U = zeros(2*(Macro_nely+1)*(Macro_nelx+1),2)
    alldofs = 1:(2*(Macro_nely+1)*(Macro_nelx+1))
    freedofs = setdiff(alldofs,fixeddofs)

    return U,F,freedofs,DofDOut,S
end

#####################3D####################################
function MBB3D(nelx,nely,nelz)
    ndof=3*(nelx+1)*(nely+1)*(nelz+1);
    # USER-DEFINED LOAD DOFs
    m= Matlab.meshgrid(nelx:nelx, 0:0, 0:nelz)
    il=m[1]
    jl=m[2]
    kl=m[3]
    loadnid = kl.*(nelx+1).*(nely+1).+il.*(nely+1).+(nely+1 .-jl)
    loaddof = 3 .*loadnid[:] .- 1; 
    # USER-DEFINED SUPPORT FIXED DOFs
    m= Matlab.meshgrid(0:0,0:nely,0:nelz)# Coordinates
    iif=m[1]
    jf=m[2]
    kf=m[3]
    fixednid = kf.*(nelx+1).*(nely+1) .+iif .*(nely .+1) .+(nely .+1 .-jf) # Node IDs
    fixeddof = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs
    
    
    # DEFINE LOADS AND SUPPORTS (HALF MBB-BEAM)
    F= sparse(loaddof,fill(1,length(loaddof)),fill(-1,length(loaddof)),ndof,1);
    U = zeros(ndof,1)
    alldofs = 1:ndof
    freedofs = setdiff(1:ndof,fixeddof)

    return U,F,freedofs 
end

function stool(nelx,nely,nelz)
    ndof=3*(nelx+1)*(nely+1)*(nelz+1);

    il = [Int(nelx/2.)];
    jl = [ 0];
    kl = [Int(nelz/2.)];
    loadnid = kl.*(nelx+1).*(nely+1).+il.*(nely+1).+(nely+1 .-jl)
    loaddof = 3 .*loadnid[:] .- 1;
    val=-1;


    iif = [0 0 nelx nelx];
    jf = [0 0 0 0];
    kf = [0 nelz 0 nelz];
    fixednid = kf.*(nelx+1).*(nely+1) .+iif .*(nely .+1) .+(nely .+1 .-jf) # Node IDs
    fixeddof = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2]

    F= sparse(loaddof,fill(1,length(loaddof)),fill(val,length(loaddof)),ndof,1);
    U = zeros(ndof,1)
    alldofs = 1:ndof
    freedofs = sort(setdiff(1:ndof,fixeddof))


    il = [Int(nelx/2.)];
    jl = [Int(nely/2.)];
    kl = [0];
    loadnid = kl.*(nelx+1).*(nely+1).+il.*(nely+1).+(nely+1 .-jl)
    loaddof = 3 .*loadnid[:] .- 0; #x is -2 # y is -1 # z is 0
    val=-1;


    iif = [0 0 nelx nelx];
    jf = [0 nely 0 nely];
    kf = [0 0 0 0];
    fixednid = kf.*(nelx+1).*(nely+1) .+iif .*(nely .+1) .+(nely .+1 .-jf) # Node IDs
    fixeddof = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2]

    F= sparse(loaddof,fill(1,length(loaddof)),fill(val,length(loaddof)),ndof,1);
    U = zeros(ndof,1)
    alldofs = 1:ndof
    freedofs = sort(setdiff(1:ndof,fixeddof))

    

    return U,F,freedofs 
end

#LOAD TOP
function stressTest3D1(nelx,nely,nelz)

    ndof=3*(nelx+1)*(nely+1)*(nelz+1);
    # mid=Int((nelx-1)/2)+1
    # il = [mid-1 mid-1 mid+0 mid+0];
    # jl = [mid-1 mid+0 mid-1 mid+0];
    # kl = [nelz nelz nelz nelz]
    m= Matlab.meshgrid(0:nelx,  0:nely, nelz:nelz)
    il=m[1]
    jl=m[2]
    kl=m[3]
    loadnid = kl.*(nelx+1).*(nely+1).+il.*(nely+1).+(nely+1 .-jl)
    loaddof = 3 .*loadnid[:] .- 0; #x is -2 # y is -1 # z is 0
    val=-1;


    iif = [0 0 nelx nelx];
    jf = [0 nely 0 nely];
    kf = [0 0 0 0];

    m= Matlab.meshgrid(0:nelx,  0:nely, 0:0)
    iif=m[1]
    jf=m[2]
    kf=m[3]

    fixednid = kf.*(nelx+1).*(nely+1) .+iif .*(nely .+1) .+(nely .+1 .-jf) # Node IDs
    fixeddof = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2]

    F= sparse(loaddof,fill(1,length(loaddof)),fill(val,length(loaddof)),ndof,1);
    U = zeros(ndof,1)
    alldofs = 1:ndof
    freedofs = sort(setdiff(1:ndof,fixeddof))




    return U,F,freedofs  

end
#LOAD TOP MIDDLE VOXEL
function stressTest3D(nelx,nely,nelz)

    ndof=3*(nelx+1)*(nely+1)*(nelz+1);
    mid=Int((nelx-1)/2)+1
    il = [mid-1 mid-1 mid+0 mid+0];
    jl = [mid-1 mid+0 mid-1 mid+0];
    kl = [nelz nelz nelz nelz]
    # m= Matlab.meshgrid(0:nelx,  0:nely, nelz:nelz)
    # il=m[1]
    # jl=m[2]
    # kl=m[3]
    loadnid = kl.*(nelx+1).*(nely+1).+il.*(nely+1).+(nely+1 .-jl)
    loaddof = 3 .*loadnid[:] .- 0; #x is -2 # y is -1 # z is 0
    val=-1;


    iif = [0 0 nelx nelx];
    jf = [0 nely 0 nely];
    kf = [0 0 0 0];

    m= Matlab.meshgrid(0:nelx,  0:nely, 0:0)
    iif=m[1]
    jf=m[2]
    kf=m[3]

    fixednid = kf.*(nelx+1).*(nely+1) .+iif .*(nely .+1) .+(nely .+1 .-jf) # Node IDs
    fixeddof = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2]

    F= sparse(loaddof,fill(1,length(loaddof)),fill(val,length(loaddof)),ndof,1);
    U = zeros(ndof,1)
    alldofs = 1:ndof
    freedofs = sort(setdiff(1:ndof,fixeddof))




    return U,F,freedofs 

end

function Canteliver3D(nelx,nely,nelz)
    ndof=3*(nelx+1)*(nely+1)*(nelz+1);
    # USER-DEFINED LOAD DOFs
    # m= Matlab.meshgrid(nelx:nelx, 0:0, 0:nelz)
    # il=m[1]
    # jl=m[2]
    # kl=m[3]
    # loadnid = kl.*(nelx+1).*(nely+1).+il.*(nely+1).+(nely+1 .-jl)
    # loaddof = 3 .*loadnid[:] .- 1;

    # m= Matlab.meshgrid(nelx:nelx, Int(nely/2):Int(nely/2), Int(nelz/2):Int(nelz/2))
    m= Matlab.meshgrid(nelx:nelx,  0:0, Int(nelz/2):Int(nelz/2))
    m= Matlab.meshgrid(nelx:nelx,  0:0, 0:0)
    il=m[1]
    jl=m[2]
    kl=m[3]
    loadnid = kl.*(nelx+1).*(nely+1).+il.*(nely+1).+(nely+1 .-jl)
    loaddof = 3 .*loadnid[:] .- 1;
    # loaddof = 3 .*loadnid[:] ;
     
    # USER-DEFINED SUPPORT FIXED DOFs
    m= Matlab.meshgrid(0:0,0:nely,0:nelz)# Coordinates
    iif=m[1]
    jf=m[2]
    kf=m[3]
    fixednid = kf.*(nelx+1).*(nely+1) .+iif .*(nely .+1) .+(nely .+1 .-jf) # Node IDs
    fixeddof = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs
    
    
    # DEFINE LOADS AND SUPPORTS (HALF MBB-BEAM)
    F= sparse(loaddof,fill(1,length(loaddof)),fill(-1,length(loaddof)),ndof,1);
    U = zeros(ndof,1)
    alldofs = 1:ndof
    freedofs = setdiff(1:ndof,fixeddof)

    return U,F,freedofs 
end

function inverter(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)
    # USER-DEFINED LOAD DOFs
    il = [0 nelx]; 
    jl = [nely nely]; 
    kl = [0 0];
    loadnid = kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl);
    loaddof = 3 .*loadnid[:] .- 2;

    loaddof=[getIndex3d(1,1,1,1,nelx+1,nely+1,nelz+1,3),
        getIndex3d(nelx+1,1,1,1,nelx+1,nely+1,nelz+1,3)]
    din = loaddof[1]; dout = loaddof[2];
    F = sparse(loaddof,[1;2],[1;-1],ndof,2);

    # USER-DEFINED SUPPORT FIXED DOFs
    # Top face
    m = Matlab.meshgrid(0:nelx,0:nelz)
    iif=m[1]
    kf=m[2]
    fixednid = kf .*(nelx+1) .*(nely+1) .+iif .*(nely+1) .+1;
    fixeddof_t = 3 .*fixednid[:] .-1;
    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    fixeddof_s = 3*fixednid[:];
    # Two pins
    iif = [0;0]; 
    jf = [0;1]; 
    kf = [nelz;nelz];
    fixednid = kf .*(nelx+1) .*(nely .+1) .+iif .*(nely .+1) .+(nely+1 .-jf)


    fixeddof_p = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs
    # Fixed DOFs
    fixeddof = union(fixeddof_t,fixeddof_s);
    fixeddof = union(fixeddof, fixeddof_p);
    fixeddof = sort(fixeddof);

    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end

function inverterFull(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)
    # USER-DEFINED LOAD DOFs
    il = [0 nelx]; 
    jl = [nely/2 nely/2]; 
    kl = [0 0];
    loadnid =Int.( kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl));
    loaddof = 3 .*loadnid[:] .- 2;

    din = loaddof[1]; 
    dout = loaddof[2];
    F = sparse(loaddof,[1;2],[1;-1],ndof,2);

    # USER-DEFINED SUPPORT FIXED DOFs
    # Top face
    # m = Matlab.meshgrid(0:nelx,0:nelz)
    # iif=m[1]
    # kf=m[2]
    # fixednid = kf .*(nelx+1) .*(nely+1) .+iif .*(nely+1) .+1;
    # fixeddof_t = 3 .*fixednid[:] .-1;

    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    fixeddof_s = 3*fixednid[:];

    # four pins
    iif = [0;0;0;0]; 
    jf = [0;1;nely-1 ;nely]; 
    kf = [nelz;nelz;nelz;nelz];
    fixednid = kf .*(nelx+1) .*(nely .+1) .+iif .*(nely .+1) .+(nely+1 .-jf)



    fixeddof_p = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs
    # Fixed DOFs
    # fixeddof = union(fixeddof_t,fixeddof_s);
    fixeddof = union(fixeddof_s, fixeddof_p);
    fixeddof = sort(fixeddof);

    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end

function inverterFullM(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)

    din=[];dout=[];F=[];
    # USER-DEFINED LOAD DOFs
    il = [0 nelx]; 
    jl = [nely/2 nely/2]; 
    kl = [0 0];
    loadnid =Int.( kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl));
    loaddof = 3 .*loadnid[:] .- 2;

    append!(din,[ loaddof[1]]); 
    append!(dout,[ loaddof[2]]);
    append!(F,[sparse(loaddof,[1;2],[1;-1],ndof,2)]);

    append!(din,[ loaddof[1]]); 
    append!(dout,[ loaddof[2]]);
    append!(F,[sparse(loaddof,[1;2],[1;-1],ndof,2)]);

    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    fixeddof_s = 3*fixednid[:];

    # four pins
    iif = [0;0;0;0]; 
    jf = [0;1;nely-1 ;nely]; 
    kf = [nelz;nelz;nelz;nelz];
    fixednid = kf .*(nelx+1) .*(nely .+1) .+iif .*(nely .+1) .+(nely+1 .-jf)



    fixeddof_p = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs
    # Fixed DOFs
    fixeddof = union(fixeddof_s, fixeddof_p);
    fixeddof = sort(fixeddof);

    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end

function wing(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)
    # USER-DEFINED LOAD DOFs
    # loaddof=[getIndex3d1(1,nely+1,1,1,nelx+1,nely+1,nelz+1,3),
    #     getIndex3d1(nelx+1,nely+1,1,1,nelx+1,nely+1,nelz+1,3)]
    # din = loaddof[1]; 
    # dout = loaddof[2];

    # F = sparse(loaddof,[1;2],[1;-1],ndof,2);

    loaddof=[getIndex3d(1,nely+1,1,1,nelx+1,nely+1,nelz+1,3),
        getIndex3d(nelx+1,nely+1,1,2,nelx+1,nely+1,nelz+1,3)]
    din = loaddof[1]; 
    dout = loaddof[2];

    F = sparse(loaddof,[1;2],[1;1],ndof,2);

    # F = fill(0.0,ndof,2);

    # F[loaddof[1],1]=1
    # F[loaddof[2],2]=-1
    # F[getIndex3d(1,1,1,2,nelx+1,nely+1,nelz+1,3):3:getIndex3d(nelx+1,nely+1,nelz+1,2,nelx+1,nely+1,nelz+1,3),1] .+= -0.01
    # F[getIndex3d(1,1,1,2,nelx+1,nely+1,nelz+1,3),1] = 1
    # F[loaddof[1],1].=F[loaddof[1],1].+1.0
    # F = sparse(loaddof,[1;2],[1;1],ndof,2);
    # F[:]


    # USER-DEFINED SUPPORT FIXED DOFs
    # m= Matlab.meshgrid(0:0,0:nely,0:nelz)# Coordinates
    # iif=m[1]
    # jf=m[2]
    # kf=m[3]
    # fixednid = kf.*(nelx+1).*(nely+1) .+iif .*(nely .+1) .+(nely .+1 .-jf) # Node IDs
    # fixeddof = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs

    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    fixeddof_s = 3*fixednid[:];

    fixeddof=union(fixeddof_s,getIndex3d(1,1,(nelz+1),1:3,nelx+1,nely+1,nelz+1,3))
    fixeddof=union(fixeddof,getIndex3d(1,2,(nelz+1),1:3,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(getIndex3d(1,1,1:(nelz+1),1,nelx+1,nely+1,nelz+1,3),getIndex3d(1,1,1:(nelz+1),2,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,1,1:(nelz+1),3,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,2,1:(nelz+1),1,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,2,1:(nelz+1),2,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,2,1:(nelz+1),3,nelx+1,nely+1,nelz+1,3))
    fixeddof = sort(fixeddof);


    # Displacement vector
    U = zeros(ndof,2);
    freedofs = sort(setdiff(1:ndof,fixeddof));

    return U,F,freedofs,din,dout
end

function wing1(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)
    # USER-DEFINED LOAD DOFs
    # loaddof=[getIndex3d1(1,nely+1,1,1,nelx+1,nely+1,nelz+1,3),
    #     getIndex3d1(nelx+1,nely+1,1,1,nelx+1,nely+1,nelz+1,3)]
    # din = loaddof[1]; 
    # dout = loaddof[2];

    # F = sparse(loaddof,[1;2],[1;-1],ndof,2);

    loaddof=[getIndex3d(nelx/2,nely+1,1,1,nelx+1,nely+1,nelz+1,3),
        getIndex3d(nelx+1,nely+1,1,2,nelx+1,nely+1,nelz+1,3)]
    din = loaddof[1]; 
    dout = loaddof[2];

    F = sparse(loaddof,[1;2],[-1;-1],ndof,2);


    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    fixeddof_s = 3*fixednid[:];

    fixeddof=union(fixeddof_s,getIndex3d(1,1,(nelz+1),1:3,nelx+1,nely+1,nelz+1,3))
    fixeddof=union(fixeddof,getIndex3d(1,2,(nelz+1),1:3,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(getIndex3d(1,1,1:(nelz+1),1,nelx+1,nely+1,nelz+1,3),getIndex3d(1,1,1:(nelz+1),2,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,1,1:(nelz+1),3,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,2,1:(nelz+1),1,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,2,1:(nelz+1),2,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,2,1:(nelz+1),3,nelx+1,nely+1,nelz+1,3))
    fixeddof = sort(fixeddof);


    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end

function wing2(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)
    # USER-DEFINED LOAD DOFs
    # loaddof=[getIndex3d1(1,nely+1,1,1,nelx+1,nely+1,nelz+1,3),
    #     getIndex3d1(nelx+1,nely+1,1,1,nelx+1,nely+1,nelz+1,3)]
    # din = loaddof[1]; 
    # dout = loaddof[2];

    # F = sparse(loaddof,[1;2],[1;-1],ndof,2);

    loaddof=[getIndex3d(nelx+1,nely+1,1,2,nelx+1,nely+1,nelz+1,3),
    getIndex3d(1,nely+1,1,1,nelx+1,nely+1,nelz+1,3)]
    din = loaddof[1]; 
    dout = loaddof[2];

    F = sparse(loaddof,[1;2],[2;1],ndof,2);

    # F = fill(0.0,ndof,2);

    # F[loaddof[1],1]=1
    # F[loaddof[2],2]=-1
    # F[getIndex3d(1,1,1,2,nelx+1,nely+1,nelz+1,3):3:getIndex3d(nelx+1,nely+1,nelz+1,2,nelx+1,nely+1,nelz+1,3),1] .+= -0.01
    # F[getIndex3d(1,1,1,2,nelx+1,nely+1,nelz+1,3),1] = 1
    # F[loaddof[1],1].=F[loaddof[1],1].+1.0
    # F = sparse(loaddof,[1;2],[1;1],ndof,2);
    # F[:]


    # USER-DEFINED SUPPORT FIXED DOFs
    # m= Matlab.meshgrid(0:0,0:nely,0:nelz)# Coordinates
    # iif=m[1]
    # jf=m[2]
    # kf=m[3]
    # fixednid = kf.*(nelx+1).*(nely+1) .+iif .*(nely .+1) .+(nely .+1 .-jf) # Node IDs
    # fixeddof = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs

    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    fixeddof_s = 3*fixednid[:];

    fixeddof=union(fixeddof_s,getIndex3d(1,1,(nelz+1),1:3,nelx+1,nely+1,nelz+1,3))
    fixeddof=union(fixeddof,getIndex3d(1,2,(nelz+1),1:3,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(getIndex3d(1,1,1:(nelz+1),1,nelx+1,nely+1,nelz+1,3),getIndex3d(1,1,1:(nelz+1),2,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,1,1:(nelz+1),3,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,2,1:(nelz+1),1,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,2,1:(nelz+1),2,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,2,1:(nelz+1),3,nelx+1,nely+1,nelz+1,3))
    fixeddof = sort(fixeddof);


    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end

function gripper(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)
    # USER-DEFINED LOAD DOFs

    # il = [0 nelx]; 
    # jl = [nely 0]; 
    # kl = [0 0];
    # loadnid = kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl);
    # loaddof = 3 .*loadnid[:] .- 1 #-2 if x -1 if y and -0 if z

    loaddof=[getIndex3d(1,Int(nely/2),1,1,nelx+1,nely+1,nelz+1,3),
        getIndex3d(nelx+1,nely+1,1,2,nelx+1,nely+1,nelz+1,3),
        getIndex3d(nelx+1,1,1,2,nelx+1,nely+1,nelz+1,3)]
    din = loaddof[1]; 
    dout = [loaddof[2],loaddof[3]];

    F = sparse(loaddof,[1;2;2],[1;1;-1],ndof,2);

    # Side face
    # m = Matlab.meshgrid(0:nelx,0:nely)
    # iif=m[1]
    # jf=m[2]
    # fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    # fixeddof_s = 3*fixednid[:];

    # fixeddof=union(fixeddof_s,getIndex3d(1,1,(nelz+1),1:3,nelx+1,nely+1,nelz+1,3))
    # fixeddof=union(fixeddof,getIndex3d(1,nely+1,(nelz+1),1:3,nelx+1,nely+1,nelz+1,3))

    fixeddof=union(getIndex3d(1,1,1,1:3,nelx+1,nely+1,nelz+1,3),getIndex3d(1,2,1,1:3,nelx+1,nely+1,nelz+1,3))
    fixeddof=union(fixeddof,getIndex3d(1,nely,1,1:3,nelx+1,nely+1,nelz+1,3))
    fixeddof=union(fixeddof,getIndex3d(1,nely+1,1,1:3,nelx+1,nely+1,nelz+1,3))

    # fixeddof=union(getIndex3d(1,1,(nelz+1),1:3,nelx+1,nely+1,nelz+1,3),getIndex3d(1,nely+1,(nelz+1),1:3,nelx+1,nely+1,nelz+1,3))

    fixeddof = sort(fixeddof);


    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end

function gripper(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)
    # USER-DEFINED LOAD DOFs
    # il = [0 nelx]; 
    # jl = [nely nely]; 
    # kl = [0 0];
    # loadnid = kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl);
    # loaddof = 3 .*loadnid[:] .- 2;

    # il = [0 nelx]; 
    # jl = [nely 0]; 
    # kl = [0 0];
    # loadnid = kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl);
    # loaddof = 3 .*loadnid[:] .- 1 #-2 if x -1 if y and -0 if z

    loaddof=[getIndex3d(1,1,1,1,nelx+1,nely+1,nelz+1,3),
        getIndex3d(nelx+1,nely+1,1,2,nelx+1,nely+1,nelz+1,3)]
    din = loaddof[1]; dout = loaddof[2];
    F = sparse(loaddof,[1;2],[1;-1],ndof,2);



    # USER-DEFINED SUPPORT FIXED DOFs
    # Top face
    m = Matlab.meshgrid(0:nelx,0:nelz)
    iif=m[1]
    kf=m[2]
    fixednid = kf .*(nelx+1) .*(nely+1) .+iif .*(nely+1) .+1;
    fixeddof_t = 3 .*fixednid[:] .-1;
    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    fixeddof_s = 3*fixednid[:];
    # Two pins
    iif = [0;0]; 
    jf = [0;1]; 
    kf = [nelz;nelz];
    fixednid = kf .*(nelx+1) .*(nely .+1) .+iif .*(nely .+1) .+(nely+1 .-jf)


    fixeddof_p = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs
    # Fixed DOFs
    fixeddof = union(fixeddof_t,fixeddof_s);
    fixeddof = union(fixeddof, fixeddof_p);
    fixeddof = sort(fixeddof);

    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end

function gripperFullM(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)

    din=[];dout=[];F=[];
    # USER-DEFINED LOAD DOFs
    il = [0 nelx]; 
    jl = [nely/2 0]; 
    kl = [0 0];
    loadnid = Int.(kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl));
    loaddof = 3 .*loadnid[:] .- [2;1] #-2 if x -1 if y and -0 if z

    append!(din,[ loaddof[1]]); 
    append!(dout,[ loaddof[2]]);
    append!(F,[sparse(loaddof,[1;2],[1;-1],ndof,2)]);

    # USER-DEFINED LOAD DOFs
    il = [0 nelx]; 
    jl = [nely/2 nely]; 
    kl = [0 0];
    loadnid = Int.(kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl));
    loaddof = 3 .*loadnid[:] .- [2;1] #-2 if x -1 if y and -0 if z

    append!(din,[ loaddof[1]]); 
    append!(dout,[ loaddof[2]]);
    append!(F,[sparse(loaddof,[1;2],[1;1],ndof,2)]);

    # USER-DEFINED SUPPORT FIXED DOFs
    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    fixeddof_s = 3*fixednid[:];
    
    # four pins
    iif = [0;0;0;0]; 
    jf = [0;1;nely-1 ;nely]; 
    kf = [nelz;nelz;nelz;nelz];
    fixednid = kf .*(nelx+1) .*(nely .+1) .+iif .*(nely .+1) .+(nely+1 .-jf)

    fixeddof_p = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs

    # Fixed DOFs
    fixeddof = union(fixeddof_s, fixeddof_p);
    fixeddof = sort(fixeddof);

    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end

function gripper_invFullM(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)

    din=[];dout=[];F=[];
    # USER-DEFINED LOAD DOFs
    il = [0 nelx]; 
    jl = [nely/2 0]; 
    kl = [0 0];
    loadnid = Int.(kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl));
    loaddof = 3 .*loadnid[:] .- [2;1] #-2 if x -1 if y and -0 if z

    append!(din,[ loaddof[1]]); 
    append!(dout,[ loaddof[2]]);
    append!(F,[sparse(loaddof,[1;2],[1;1],ndof,2)]);

    # USER-DEFINED LOAD DOFs
    il = [0 nelx]; 
    jl = [nely/2 nely]; 
    kl = [0 0];
    loadnid = Int.(kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl));
    loaddof = 3 .*loadnid[:] .- [2;1] #-2 if x -1 if y and -0 if z

    append!(din,[ loaddof[1]]); 
    append!(dout,[ loaddof[2]]);
    append!(F,[sparse(loaddof,[1;2],[1;-1],ndof,2)]);

    # USER-DEFINED SUPPORT FIXED DOFs
    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    fixeddof_s = 3*fixednid[:];
    
    # four pins
    iif = [0;0;0;0]; 
    jf = [0;1;nely-1 ;nely]; 
    kf = [nelz;nelz;nelz;nelz];
    fixednid = kf .*(nelx+1) .*(nely .+1) .+iif .*(nely .+1) .+(nely+1 .-jf)

    fixeddof_p = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs

    # Fixed DOFs
    fixeddof = union(fixeddof_s, fixeddof_p);
    fixeddof = sort(fixeddof);

    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end

function gripper_inv(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)
    # USER-DEFINED LOAD DOFs
    # il = [0 nelx]; 
    # jl = [nely nely]; 
    # kl = [0 0];
    # loadnid = kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl);
    # loaddof = 3 .*loadnid[:] .- 2;

    loaddof=[getIndex3d(1,1,1,1,nelx+1,nely+1,nelz+1,3),
        getIndex3d(nelx+1,nely+1,1,2,nelx+1,nely+1,nelz+1,3)]
    din = loaddof[1]; dout = loaddof[2];
    F = sparse(loaddof,[1;2],[1;1],ndof,2);



    # USER-DEFINED SUPPORT FIXED DOFs
    # Top face
    m = Matlab.meshgrid(0:nelx,0:nelz)
    iif=m[1]
    kf=m[2]
    fixednid = kf .*(nelx+1) .*(nely+1) .+iif .*(nely+1) .+1;
    fixeddof_t = 3 .*fixednid[:] .-1;
    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    fixeddof_s = 3*fixednid[:];
    # Two pins
    iif = [0;0]; 
    jf = [0;1]; 
    kf = [nelz;nelz];
    fixednid = kf .*(nelx+1) .*(nely .+1) .+iif .*(nely .+1) .+(nely+1 .-jf)


    fixeddof_p = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs
    # Fixed DOFs
    fixeddof = union(fixeddof_t,fixeddof_s);
    fixeddof = union(fixeddof, fixeddof_p);
    fixeddof = sort(fixeddof);

    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end

function gripperWhole(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)
    # USER-DEFINED LOAD DOFs
    # il = [0 nelx]; 
    # jl = [nely nely]; 
    # kl = [0 0];
    # loadnid = kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl);
    # loaddof = 3 .*loadnid[:] .- 2;

    loaddof=[getIndex3d(1,Int(nely/2),1,1,nelx+1,nely+1,nelz+1,3),
        getIndex3d(nelx+1,nely+1,1,2,nelx+1,nely+1,nelz+1,3),
        getIndex3d(nelx+1,1,1,2,nelx+1,nely+1,nelz+1,3)]


    din = loaddof[1]; dout = [loaddof[2],loaddof[3]];
    F = sparse(loaddof,[1;2;2],[1;-1;1],ndof,2);



    # USER-DEFINED SUPPORT FIXED DOFs
    # Top face
    # m = Matlab.meshgrid(0:nelx,0:nelz)
    # iif=m[1]
    # kf=m[2]
    # fixednid = kf .*(nelx+1) .*(nely+1) .+iif .*(nely+1) .+1;
    # fixeddof_t = 3 .*fixednid[:] .-1;


    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    fixeddof_s = 3*fixednid[:];


    # four pins
    iif = [0;0;0;0]; 
    jf = [0;1;nely-1 ;nely]; 
    kf = [nelz;nelz;nelz;nelz];
    fixednid = kf .*(nelx+1) .*(nely .+1) .+iif .*(nely .+1) .+(nely+1 .-jf)


    fixeddof_p = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs
    # Fixed DOFs
    fixeddof = union(fixeddof_s, fixeddof_p);
    fixeddof = sort(fixeddof);

    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end

function madcat(nelx,nely,nelz)
    ndof=3*(nelx+1)*(nely+1)*(nelz+1);
    # USER-DEFINED LOAD DOFs
    m= Matlab.meshgrid(0:0,0:nely,0:nelz)# Coordinates
    m= Matlab.meshgrid(0:0,0:0,nelz:nelz)# Coordinates #one node



    il=m[1]
    jl=m[2]
    kl=m[3]
    loadnid = kl.*(nelx+1).*(nely+1).+il.*(nely+1).+(nely+1 .-jl)
    loaddof = 3 .*loadnid[:] .- 0;#x
    # loaddof = 3 .*loadnid[:] .- 1;#y
    # loaddof = 3 .*loadnid[:] .- 2;#z  
    F= sparse(loaddof,fill(1,length(loaddof)),fill(-1,length(loaddof)),ndof,1);


    # USER-DEFINED SUPPORT FIXED DOFs
    m= Matlab.meshgrid(0:nelx,nely:nely,0:nelz)# Coordinates

    iif=m[1]
    jf=m[2]
    kf=m[3]
    fixednid = kf.*(nelx+1).*(nely+1) .+iif .*(nely .+1) .+(nely .+1 .-jf) # Node IDs
    fixeddof = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs
    
    
    # DEFINE LOADS AND SUPPORTS (HALF MBB-BEAM)
    U = zeros(ndof,1)
    alldofs = 1:ndof
    freedofs = setdiff(1:ndof,fixeddof)

    return U,F,freedofs
end

function whatever3d(Macro_nelx,Macro_nely,Macro_nelz)
    Macro_ndof=3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    load_x,load_y,load_z = Matlab.meshgrid([Int(Macro_nelx/2)], [Macro_nely], [Int(Macro_nelz/2)]);
    loadnid = load_z .*(Macro_nelx+1)*(Macro_nely+1) .+ load_x .*(Macro_nely .+1) .+(Macro_nely .+1 .-load_y);

    F = sparse(Int.(3 .*loadnid[:] .- 1) ,[1],[-1],Macro_ndof,1);
    fixed_x,fixed_y,fixed_z = Matlab.meshgrid( [0, Macro_nelx],[0],[0, Macro_nelz] );
    fixednid = fixed_z .*(Macro_nelx .+1)*(Macro_nely+1) .+fixed_x.*(Macro_nely+1) .+(Macro_nely+1 .-fixed_y);
    fixeddofs = vcat(3 .*fixednid[:], 3 .*fixednid[:] .-1, 3 .*fixednid[:] .-2);
    alldofs = 1:Macro_ndof
    U = zeros(Macro_ndof)
    freedofs = setdiff(1:Macro_ndof,fixeddofs)
    return U,F,freedofs 
    
end

function seat3DM(nelx,nely,nelz)
    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)

    din=[];dout=[];F=[];
    # USER-DEFINED LOAD DOFs
    il = [nelx nelx]; 
    jl = [0 nely]; 
    kl = [0 0];
    loadnid = Int.(kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl));
    loaddof = 3 .*loadnid[:] .- [2;2] #-2 if x -1 if y and -0 if z

    append!(din,[ loaddof[1]]); 
    append!(dout,[ loaddof[2]]);
    append!(F,[sparse(loaddof,[1;2],[-1;1],ndof,2)]);

    # USER-DEFINED LOAD DOFs
    il = [nelx nelx]; 
    jl = [0 nely]; 
    kl = [0 0];
    loadnid = Int.(kl .*(nelx+1) .*(nely+1) .+il .*(nely .+1) .+(nely+1 .-jl));
    loaddof = 3 .*loadnid[:] .- [2;1] #-2 if x -1 if y and -0 if z

    append!(din,[ loaddof[1]]); 
    append!(dout,[ loaddof[2]]);
    append!(F,[sparse(loaddof,[1;2],[-1;-0.5],ndof,2)]);

    # USER-DEFINED SUPPORT FIXED DOFs
    # Side face
    m = Matlab.meshgrid(0:nelx,0:nely)
    iif=m[1]
    jf=m[2]
    fixednid = iif .*(nely+1) .+(nely+1 .-jf);
    
    
    fixeddof_s = 3*fixednid[:];
    
    # # four pins
    # iif = [0;0;0;0]; 
    # jf = [0;1;nely-1 ;nely]; 
    # kf = [nelz;nelz;nelz;nelz];
    
    m = Matlab.meshgrid([0],0:nely)
    iif=m[1]
    jf=m[2]
    kf = fill(nelz,length(iif))
    
    
    fixednid = kf .*(nelx+1) .*(nely .+1) .+iif .*(nely .+1) .+(nely+1 .-jf)
    
    
    
    fixeddof_p = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs

    # Fixed DOFs
    fixeddof = union(fixeddof_s, fixeddof_p);
    fixeddof = sort(fixeddof);

    # Displacement vector
    U = zeros(ndof,2);
    freedofs = setdiff(1:ndof,fixeddof);

    return U,F,freedofs,din,dout
end



