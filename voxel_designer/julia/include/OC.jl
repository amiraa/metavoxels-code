#======================================================================================================================#
# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021
#======================================================================================================================#


##############################################################
include("./mmaSolver.jl");


## SUB FUNCTION: OC
function OC2D(x, dc, dv, H, Hs, volfrac, nele, move, beta,fab=false,ss=4)
    l1 = 0; l2 = 1e9;
    xTilde=copy(x);xnew=copy(x);xPhys=copy(x)
    while (l2-l1)/(l1+l2) > 1e-4
        lmid = 0.5*(l2+l1);
        xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(abs.(-dc./dv/lmid))))));
        xTilde[:] = (H*xnew[:] )./Hs; 
        xPhys = 1 .-exp.(-beta .*xTilde) .+xTilde .*exp.(-beta);
        if sum(xPhys[:] ) > volfrac*nele 
            l1 = lmid; 
        else 
            l2 = lmid; 
        end
    end
    if fab
        xnew=addFabConstraint2D(xnew,ss);
    end
    change = maximum(abs.(xnew[:]-x[:] )); x = xnew;
    return x, xPhys, change
end

function OC2D_2(x, dc, dv, H, Hs, volfrac, nele, move, beta,fab=false,ss=4)
    l1 = 0; l2 = 1e9;
    xTilde=copy(x);xnew=copy(x);xPhys=copy(x)
    while (l2-l1)/(l1+l2) > 1e-4
        lmid = 0.5*(l2+l1);
        xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(abs.(-dc./dv/lmid))))));
        xTilde[:] = (H*xnew[:] )./Hs; 
        xPhys = 1 .-exp.(-beta .*xTilde) .+xTilde .*exp.(-beta);
        if sum(xPhys[:] ) > volfrac*nele 
            l1 = lmid; 
        else 
            l2 = lmid; 
        end
    end
    if fab
        xnew=addFabConstraint2D(xnew,ss);
    end
    change = maximum(abs.(xnew[:]-x[:] )); x = xnew;
    return x,xTilde, xPhys, change
end

## SUB FUNCTION: MMA
function OC2DMMA(x, xold1,xold2, dc, dv, H, Hs, volfrac, nele, move, beta,loop,low,upp)

    # display("min dc $(minimum(dc)),max dc $(maximum(dc))")
    xTilde=copy(x);
    nelx=size(x)[1]
    nely=size(x)[2]
    # METHOD OF MOVING ASYMPTOTES
    xval  = x[:];
    # f0val = c;
    f0val = 0;
    df0dx = dc[:];
    fval  = sum(x[:])/(volfrac*nele) - 1;

    fval=[fval,1-sum(x[:])/(volfrac*nele)];
    dfdx  = dv[:]' / (volfrac*nele);

    dfdx  = [dfdx;-dv[:]' / (volfrac*nele)];

    ## Solving with MMA
    m=length(fval);
    # m     = 2;                # The number of general constraints.
    mdof = 1:m;
    a0 = 1;
    a = zeros(m);
    c_MMA = ones(m)*1000;
    c_MMA = ones(m)*10000;
    d = zeros(m);
    n     = nele;
    # xmin=max.(0.0, x[:].-move);
    # xmax=min.(1, x[:].+move);
    xmin  = zeros(n);       #  Column vector with the lower bounds for the variables x_j.
    xmax  = ones(n);        #  Column vector with the upper bounds for the variables x_j.
    
    

    xmma, ymma, zmma, lam, xsi, eta_, mu, zet, s, low, upp = mmasub(m, n, loop, xval[:], xmin[:], xmax[:], xold1[:], xold2[:], f0val,df0dx,fval,dfdx,low,upp,a0,a,c_MMA,d);
    
    # Update MMA Variables
    xnew     = reshape(xmma,nely,nelx);
    xTilde[:] = (H*xnew[:] )./Hs; 
    xPhys = 1 .-exp.(-beta .*xTilde) .+xTilde .*exp.(-beta);
    change = maximum(abs.(xnew[:]-x[:] )); x = xnew;
    xold2    = copy(xold1);
    xold1    = copy(x);
    
    return x, xPhys, change,low,upp,xold1,xold2
end

## SUB FUNCTION: MMA with connectivity
function OC2DMMACON(x, xold1,xold2, dc, dv, H, Hs, volfrac, nele, move, beta,loop,low,upp,L)
    

    # display("min dc $(minimum(dc)),max dc $(maximum(dc))")
    xTilde=copy(x);
    nelx=size(x)[1]
    nely=size(x)[2]
    
    
    ss=6
    x=addFabConstraint2D(x,ss);

    

    # METHOD OF MOVING ASYMPTOTES
    xval  = x[:];
    # f0val = c;
    f0val = 0;
    df0dx = dc[:];
    fval  = sum(x[:])/(volfrac*nele) - 1 ;
    dfdx  = dv[:]' / (volfrac*nele);

    fval,dfdx=connectivityConstraint(x,L,H,Hs,fval,dfdx);

    ## Solving with MMA
    # m     = 1; 
    m=length(fval) # The number of general constraints.
    mdof = 1:m;
    a0 = 1;
    a = zeros(m);
    c_MMA = ones(m)*1000;
    c_MMA = ones(m)*10000;
    d = zeros(m);
    n     = nele;
    xmin  = zeros(n);       #  Column vector with the lower bounds for the variables x_j.
    xmax  = ones(n);        #  Column vector with the upper bounds for the variables x_j.
    
    

    xmma, ymma, zmma, lam, xsi, eta_, mu, zet, s, low, upp = mmasub(m, n, loop, xval[:], xmin[:], xmax[:], xold1[:], xold2[:], f0val,df0dx,fval,dfdx,low,upp,a0,a,c_MMA,d);
    
    # Update MMA Variables
    xnew     = reshape(xmma,nely,nelx);
    xTilde[:] = (H*xnew[:] )./Hs; 
    xPhys = 1 .-exp.(-beta .*xTilde) .+xTilde .*exp.(-beta);
    change = maximum(abs.(xnew[:]-x[:] )); x = xnew;
    xold2    = copy(xold1);
    xold1    = copy(x);
    
    return x, xPhys, change,low,upp,xold1,xold2
end

## SUB FUNCTION: OC
function CompliantOC(x, dc, dv, H, Hs, volfrac, nele, move, beta)
    l1 = 0; l2 = 1e9;
    l1 = 0; l2 = 1e9; 

    xTilde=copy(x);xnew=copy(x);xPhys=copy(x)
    # while (l2-l1)/(l1+l2) > 1e-4
    while (l2-l1)/(l2+l1) > 1e-4 && l2>1e-40
        lmid = 0.5*(l2+l1);
        xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(max.(1e-10,-dc./dv./lmid))))))
        # xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(-dc./dv/lmid)))));
        xTilde[:] = (H*xnew[:] )./Hs; 
        xPhys = 1 .-exp.(-beta .*xTilde) .+xTilde .*exp.(-beta);
        if sum(xPhys[:] ) > volfrac*nele 
            l1 = lmid; 
        else 
            l2 = lmid; 
        end
    end
    change = maximum(abs.(xnew[:]-x[:] )); x = xnew;
    return x, xPhys, change
end

## SUB FUNCTION: OC_reg (no filter or HEAVISIDE)
function OC_reg(x, dc, dv, volfrac, nele, move)
    l1 = 0; l2 = 1e9;
    xnew=copy(x);
    xPhys=copy(x)
    while (l2-l1)/(l1+l2) > 1e-4
        lmid = 0.5*(l2+l1);
        xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(-dc./dv/lmid)))));
        xPhys[:] = xnew[:] 
        if sum(xPhys[:] ) > volfrac*nele 
            l1 = lmid; 
        else 
            l2 = lmid; 
        end
    end
    change = maximum(abs.(xnew[:]-x[:] )); x = xnew;
    return x, xPhys, change
end

function CompliantOC_reg(x, dc, dv, volfrac, nele, move)
    l1 = 0; l2 = 1e9;
    xnew=copy(x);
    xPhys=copy(x)
    # while (l2-l1)/(l1+l2) > 1e-4
    while (l2-l1)/(l2+l1) > 1e-4 && l2>1e-40
        lmid = 0.5*(l2+l1);
        xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(max.(1e-10,-dc./dv./lmid))))))
        # xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(-dc./dv/lmid)))));
        xPhys[:] = xnew[:] 
        if sum(xPhys[:] ) > volfrac*nele 
            l1 = lmid; 
        else 
            l2 = lmid; 
        end
    end
    change = maximum(abs.(xnew[:]-x[:] )); x = xnew;
    return x, xPhys, change
end

##############################################################
## SUB FUNCTION: OC #todo remove as same in 2D
function OC(x, dc, dv, H, Hs, volfrac, nele, move, beta,voxels=true,fabric=false,ss=4)
    nely=size(x)[1]
    nelx=size(x)[2]
    nelz=size(x)[3]
    if length(voxels)==1
        fabric=false
        voxels=ones(size(x)[1],size(x)[2],size(x)[3])
    end
    l1 = 0; l2 = 1e9;
    xTilde=copy(x);xnew=copy(x);xPhys=copy(x)
    while (l2-l1)/(l1+l2) > 1e-3
        lmid = 0.5*(l2+l1);
        if !fabric
            xnew[Bool.(1 .-voxels)] .=0
        end
        
        # xnew = max.(0,max.(x .-move,min.(1,min.(x .+move,x.*sqrt.((-dc./dv/lmid))))));
        xnew = max.(0,max.(x .-move,min.(1,min.(x .+move,x.*sqrt.(abs.(-dc./dv/lmid))))));

        if fabric
            xnew.=addFabricationConstraints(ss,0,xnew,nelx,nely,nelz)
        end
        
        xTilde[:] = (H*xnew[:] )./Hs; 
        xPhys = 1 .-exp.(-beta.*xTilde) .+xTilde .*exp.(-beta);
        if !fabric
            xPhys[Bool.(1 .-voxels)] .=0
        end
        if fabric
            xPhys.=addFabricationConstraints(ss,0,xPhys,nelx,nely,nelz)
        end
        if sum(xPhys[:] ) > volfrac*nele
            l1 = lmid; 
        else
            l2 = lmid; 
        end
    end
    change = maximum(abs.(xnew[:]-x[:] )); x = xnew;
    return x, xPhys, xTilde, change
end


## SUB FUNCTION: OC_reg3 (no filter or HEAVISIDE) #todo remove as same in 2D
function OC_reg3(x, dc, dv, volfrac, nele, move,voxels=true)
    if length(voxels)==1
        voxels=ones(size(x)[1],size(x)[2],size(x)[3])
    end
    l1 = 0; l2 = 1e9;
    xnew=copy(x);
    xPhys=copy(x)
    while (l2-l1)/(l1+l2) > 1e-4
        lmid = 0.5*(l2+l1);
        xnew[Bool.(1 .-voxels)] .=0
        xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(-dc./dv/lmid)))));
        xnew[Bool.(1 .-voxels)] .=0
        xPhys[:] = xnew[:] 
        if sum(xPhys[:] ) > volfrac*nele 
            l1 = lmid; 
        else 
            l2 = lmid; 
        end
    end
    change = maximum(abs.(xnew[:]-x[:] )); x = xnew;
    return x, xPhys, change
end

## SUB FUNCTION: OC
function CompliantOC3(x, dc, dv, H, Hs, volfrac, nele, move, beta,voxels=true)
    if length(voxels)==1
        voxels=ones(size(x)[1],size(x)[2],size(x)[3])
    end

    l1 = 0; l2 = 1e9;
    l1 = 0; l2 = 1e9; 

    xTilde=copy(x);xnew=copy(x);xPhys=copy(x)
    # while (l2-l1)/(l1+l2) > 1e-4
    while (l2-l1)/(l2+l1) > 1e-4 && l2>1e-40
        lmid = 0.5*(l2+l1);
        # xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(max.(1e-10,-dc./dv./lmid))))))
        # xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(-dc./dv/lmid)))));
        xnew[Bool.(1 .-voxels)] .=0
        xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*(max.(1e-10,-dc./dv./lmid)).^0.3))));
        

        xTilde[:] = (H*xnew[:] )./Hs; 
        xPhys = 1 .-exp.(-beta .*xTilde) .+xTilde .*exp.(-beta);
        xPhys[Bool.(1 .-voxels)] .=0
        if sum(xPhys[:] ) > volfrac*nele 
            l1 = lmid; 
        else 
            l2 = lmid; 
        end
    end
    change = maximum(abs.(xnew[:]-x[:] )); x = xnew;
    return x, xPhys, change
end

function CompliantOC_reg3(x, dc, dv, volfrac, nele, move,voxels=true)
    if length(voxels)==1
        voxels=ones(size(x)[1],size(x)[2],size(x)[3])
    end

    l1 = 0; l2 = 1e9;
    xnew=copy(x);
    xPhys=copy(x)
    # while (l2-l1)/(l1+l2) > 1e-4
    while (l2-l1)/(l2+l1) > 1e-4 && l2>1e-40
        lmid = 0.5*(l2+l1);
        xnew[Bool.(1 .-voxels)] .=0
        # xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(max.(1e-10,-dc./dv./lmid))))))
        xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*(max.(1e-10,-dc./dv./lmid)).^0.3))));

        # xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.(-dc./dv/lmid)))));
        xPhys[:] = xnew[:]
        xPhys[Bool.(1 .-voxels)] .=0
 
        if sum(xPhys[:] ) > volfrac*nele 
            l1 = lmid; 
        else 
            l2 = lmid; 
        end
    end
    change = maximum(abs.(xnew[:]-x[:] )); x = xnew;
    return x, xPhys, change
end