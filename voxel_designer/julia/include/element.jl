# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

#####################################FEA KE####################################################################

function lk(E=1,nu=0.3)
    # A11 = [12  3 -6 -3;  3 12  3  0; -6  3 12 -3; -3  0 -3 12];
    # A12 = [-6 -3  0  3; -3 -6 -3 -6;  0 -3 -6  3;  3 -6  3 -6];
    # B11 = [-4  3 -2  9;  3 -4 -9  4; -2 -9 -4 -3;  9  4 -3 -4];
    # B12 = [ 2 -3  4 -9; -3  2  9 -2;  4  9  2  3; -9 -2  3  2];
    # KE = 1/(1-nu^2)/24*([A11 A12;A12' A11]+nu*[B11 B12;B12' B11]);

    k=[1/2-nu/6,1/8+nu/8,-1/4-nu/12,-1/8+3*nu/8,-1/4+nu/12,-1/8-nu/8,nu/6,1/8-3*nu/8]
    KE = E/(1-nu^2)*[  k[0+1] k[1+1] k[2+1] k[3+1] k[4+1] k[5+1] k[6+1] k[7+1];
                       k[1+1] k[0+1] k[7+1] k[6+1] k[5+1] k[4+1] k[3+1] k[2+1];
                       k[2+1] k[7+1] k[0+1] k[5+1] k[6+1] k[3+1] k[4+1] k[1+1];
                       k[3+1] k[6+1] k[5+1] k[0+1] k[7+1] k[2+1] k[1+1] k[4+1];
                       k[4+1] k[5+1] k[6+1] k[7+1] k[0+1] k[1+1] k[2+1] k[3+1];
                       k[5+1] k[4+1] k[3+1] k[2+1] k[1+1] k[0+1] k[7+1] k[6+1];
                       k[6+1] k[3+1] k[4+1] k[1+1] k[2+1] k[7+1] k[0+1] k[5+1];
                       k[7+1] k[2+1] k[1+1] k[4+1] k[3+1] k[6+1] k[5+1] k[0+1] ];
    return KE
end

function lk_H8(nu)
    A = [32 6 -8 6 -6 4 3 -6 -10 3 -3 -3 -4 -8;
        -48 0 0 -24 24 0 0 0 12 -12 0 12 12 12];
    k = 1/144*A'*[1; nu];

    K1 = [k[1]   k[2]   k[2]   k[3]  k[5]  k[5];
            k[2]   k[1]   k[2]   k[4]  k[6]  k[7];
            k[2]   k[2]   k[1]   k[4]  k[7]  k[6];
            k[3]   k[4]   k[4]   k[1]  k[8]  k[8];
            k[5]   k[6]   k[7]   k[8]  k[1]  k[2];
            k[5]   k[7]   k[6]   k[8]  k[2]  k[1]];
    K2 = [k[9]   k[8]   k[12]  k[6]  k[4]  k[7];
            k[8]   k[9]   k[12]  k[5]  k[3]  k[5];
            k[10]  k[10]  k[13]  k[7]  k[4]  k[6];
            k[6]   k[5]   k[11]  k[9]  k[2]  k[10];
            k[4]   k[3]   k[5]   k[2]  k[9]  k[12]
            k[11]  k[4]   k[6]   k[12] k[10] k[13]];
    K3 = [k[6]   k[7]   k[4]   k[9]  k[12] k[8];
            k[7]   k[6]   k[4]   k[10] k[13] k[10];
            k[5]   k[5]   k[3]   k[8]  k[12] k[9];
            k[9]   k[10]  k[2]   k[6]  k[11] k[5];
            k[12]  k[13]  k[10]  k[11] k[6]  k[4];
            k[2]   k[12]  k[9]   k[4]  k[5]  k[3]];
    K4 = [k[14]  k[11]  k[11]  k[13] k[10] k[10];
            k[11]  k[14]  k[11]  k[12] k[9]  k[8];
            k[11]  k[11]  k[14]  k[12] k[8]  k[9];
            k[13]  k[12]  k[12]  k[14] k[7]  k[7];
            k[10]  k[9]   k[8]   k[7]  k[14] k[11];
            k[10]  k[8]   k[9]   k[7]  k[11] k[14]];
    K5 = [k[1]   k[2]   k[8]   k[3]  k[5]  k[4];
            k[2]   k[1]   k[8]   k[4]  k[6]  k[11];
            k[8]   k[8]   k[1]   k[5]  k[11] k[6];
            k[3]   k[4]   k[5]   k[1]  k[8]  k[2];
            k[5]   k[6]   k[11]  k[8]  k[1]  k[8];
            k[4]   k[11]  k[6]   k[2]  k[8]  k[1]];
    K6 = [k[14]  k[11]  k[7]   k[13] k[10] k[12];
            k[11]  k[14]  k[7]   k[12] k[9]  k[2];
            k[7]   k[7]   k[14]  k[10] k[2]  k[9];
            k[13]  k[12]  k[10]  k[14] k[7]  k[11];
            k[10]  k[9]   k[2]   k[7]  k[14] k[7];
            k[12]  k[2]   k[9]   k[11] k[7]  k[14]];
    KE = 1/((nu+1)*(1-2*nu))*[ K1  K2  K3  K4;K2'  K5  K6  K3';K3' K6  K5' K2';K4  K3  K2  K1'];

    return KE
end

## FUNCTION Ke2D - ELEMENT STIFFNESS MATRIX
function Ke2D(nu)
    A11 = [12  3 -6 -3;  3 12  3  0; -6  3 12 -3; -3  0 -3 12];
    A12 = [-6 -3  0  3; -3 -6 -3 -6;  0 -3 -6  3;  3 -6  3 -6];
    B11 = [-4  3 -2  9;  3 -4 -9  4; -2 -9 -4 -3;  9  4 -3 -4];
    B12 = [ 2 -3  4 -9; -3  2  9 -2;  4  9  2  3; -9 -2  3  2];
    KE = 1/(1-nu^2)/24*([A11 A12;A12' A11]+nu*[B11 B12;B12' B11]);
    return KE
end

## FUNCTION Ke3D - ELEMENT STIFFNESS MATRIX
function Ke3D(nu)
    C = [2/9 1/18 1/24 1/36 1/48 5/72 1/3 1/6 1/12];
    A11 = [-C[1] -C[3] -C[3] C[2] C[3] C[3]; -C[3] -C[1] -C[3] -C[3] -C[4] -C[5];
    -C[3] -C[3] -C[1] -C[3] -C[5] -C[4]; C[2] -C[3] -C[3] -C[1] C[3] C[3]; 
        C[3] -C[4] -C[5] C[3] -C[1] -C[3]; C[3] -C[5] -C[4] C[3] -C[3] -C[1]];
    B11 = [C[7] 0 0 0 -C[8] -C[8]; 0 C[7] 0 C[8] 0 0; 0 0 C[7] C[8] 0 0; 
        0 C[8] C[8] C[7] 0 0; -C[8] 0 0 0 C[7] 0; -C[8] 0 0 0 0 C[7]];
    A22 = [-C[1] -C[3] C[3] C[2] C[3] -C[3]; -C[3] -C[1] C[3] -C[3] -C[4] C[5]; 
        C[3] C[3] -C[1] C[3] C[5] -C[4]; C[2] -C[3] C[3] -C[1] C[3] -C[3]; 
        C[3] -C[4] C[5] C[3] -C[1] C[3]; -C[3] C[5] -C[4] -C[3] C[3] -C[1]];
    B22 = [C[7] 0 0 0 -C[8] C[8]; 0 C[7] 0 C[8] 0 0; 0 0 C[7] -C[8] 0 0; 
        0 C[8] -C[8] C[7] 0 0; -C[8] 0 0 0 C[7] 0; C[8] 0 0 0 0 C[7]];
    A12 = [C[6] C[3] C[5] -C[4] -C[3] -C[5]; C[3] C[6] C[5] C[3] C[2] C[3]; 
        -C[5] -C[5] C[4] -C[5] -C[3] -C[4]; -C[4] C[3] C[5] C[6] -C[3] -C[5]; 
        -C[3] C[2] C[3] -C[3] C[6] C[5]; C[5] -C[3] -C[4] C[5] -C[5] C[4]];
    B12 = [-C[9] 0 -C[9] 0 C[8] 0; 0 -C[9] -C[9] -C[8] 0 -C[8]; C[9] C[9] -C[9] 0 C[8] 0; 
        0 -C[8] 0 -C[9] 0 C[9]; C[8] 0 -C[8] 0 -C[9] -C[9]; 0 C[8] 0 -C[9] C[9] -C[9]];
    A13 = [-C[4] -C[5] -C[3] C[6] C[5] C[3]; -C[5] -C[4] -C[3] -C[5] C[4] -C[5]; 
        C[3] C[3] C[2] C[3] C[5] C[6]; C[6] -C[5] -C[3] -C[4] C[5] C[3]; 
        C[5] C[4] -C[5] C[5] -C[4] -C[3]; -C[3] C[5] C[6] -C[3] C[3] C[2]];
    B13 = [0 0 C[8] -C[9] -C[9] 0; 0 0 C[8] C[9] -C[9] C[9]; -C[8] -C[8] 0 0 -C[9] -C[9]; 
        -C[9] C[9] 0 0 0 -C[8]; -C[9] -C[9] C[9] 0 0 C[8]; 0 -C[9] -C[9] C[8] -C[8] 0];
    A14 = [C[2] C[5] C[5] C[4] -C[5] -C[5]; C[5] C[2] C[5] C[5] C[6] C[3]; 
        C[5] C[5] C[2] C[5] C[3] C[6]; C[4] C[5] C[5] C[2] -C[5] -C[5]; 
        -C[5] C[6] C[3] -C[5] C[2] C[5]; -C[5] C[3] C[6] -C[5] C[5] C[2]];
    B14 = [-C[9] 0 0 -C[9] C[9] C[9]; 0 -C[9] 0 -C[9] -C[9] 0; 0 0 -C[9] -C[9] 0 -C[9]; 
        -C[9] -C[9] -C[9] -C[9] 0 0; C[9] -C[9] 0 0 -C[9] 0; C[9] 0 -C[9] 0 0 -C[9]];
    A23 = [C[2] C[5] -C[5] C[4] -C[5] C[5]; C[5] C[2] -C[5] C[5] C[6] -C[3]; 
        -C[5] -C[5] C[2] -C[5] -C[3] C[6]; C[4] C[5] -C[5] C[2] -C[5] C[5]; 
        -C[5] C[6] -C[3] -C[5] C[2] -C[5]; C[5] -C[3] C[6] C[5] -C[5] C[2]];
    B23 = [-C[9] 0 0 -C[9] C[9] -C[9]; 0 -C[9] 0 -C[9] -C[9] 0; 0 0 -C[9] C[9] 0 -C[9]; 
        -C[9] -C[9] C[9] -C[9] 0 0; C[9] -C[9] 0 0 -C[9] 0; -C[9] 0 -C[9] 0 0 -C[9]];
    KE = 1/(1+nu)/(2*nu-1)*([A11 A12 A13 A14; A12' A22 A23 A13'; A13' A23' A22 A12'; A14' A13 A12 A11] + 
        nu*[B11 B12 B13 B14; B12' B22 B23 B13'; B13' B23' B22 B12'; B14' B13 B12 B11]);
    return KE
end

## SUB FUNCTION: elementMatVec2D
function elementMatVec2D(a, b, DH)
    GaussNodes = [-1/sqrt(3); 1/sqrt(3)]; 
    GaussWeigh = [1 1];
    L = [1 0 0 0; 0 0 0 1; 0 1 1 0];
    Ke = zeros(8,8);
    for i = 1:2
        for j = 1:2
            GN_x = GaussNodes[i]; 
            GN_y = GaussNodes[j];

            dN_x = 1/4*[-(1-GN_x)  (1-GN_x) (1+GN_x) -(1+GN_x)];
            dN_y = 1/4*[-(1-GN_y) -(1+GN_y) (1+GN_y)  (1-GN_y)];

            J = [dN_x; dN_y]*[ -a  a  a  -a;  -b  -b  b  b]';
            G = [inv(J) zeros(size(J)); zeros(size(J)) inv(J)];
            dN=zeros(4,8)
            dN[1,1:2:8] = dN_x; 
            dN[2,1:2:8] = dN_y;
            dN[3,2:2:8] = dN_x; 
            dN[4,2:2:8] = dN_y;
            Be = L*G*dN;
            Ke = Ke + GaussWeigh[i]*GaussWeigh[j]*det(J)*Be'*DH*Be;
        end
    end
    return Ke
end
    
## SUB FUNCTION: elementMatVec3D
function elementMatVec3D(a, b, c, DH)
    GN_x=[-1/sqrt(3),1/sqrt(3)]; GN_y=GN_x; GN_z=GN_x; GaussWeigh=[1,1];
    Ke = zeros(24,24); L = zeros(6,9);
    L[1,1] = 1; L[2,5] = 1; L[3,9] = 1;
    L[4,2] = 1; L[4,4] = 1; L[5,6] = 1;
    L[5,8] = 1; L[6,3] = 1; L[6,7] = 1;
    # display(L)
    for ii=1:length(GN_x)
        for jj=1:length(GN_y)
            for kk=1:length(GN_z)
                x = GN_x[ii];y = GN_y[jj];z = GN_z[kk];
                
                dNx = 1/8*[-(1-y)*(1-z)  (1-y)*(1-z)  (1+y)*(1-z) -(1+y)*(1-z) -(1-y)*(1+z)  (1-y)*(1+z)  (1+y)*(1+z) -(1+y)*(1+z)];
                dNy = 1/8*[-(1-x)*(1-z) -(1+x)*(1-z)  (1+x)*(1-z)  (1-x)*(1-z) -(1-x)*(1+z) -(1+x)*(1+z)  (1+x)*(1+z)  (1-x)*(1+z)];
                dNz = 1/8*[-(1-x)*(1-y) -(1+x)*(1-y) -(1+x)*(1+y) -(1-x)*(1+y)  (1-x)*(1-y)  (1+x)*(1-y)  (1+x)*(1+y)  (1-x)*(1+y)];
                
                J = [dNx;dNy;dNz]*[ -a  a  a  -a  -a  a  a  -a ;  -b  -b  b  b  -b  -b  b  b; -c -c -c -c  c  c  c  c]';
                # display(a)
                # display(b)
                # display(c)
                # display(dNx)
                # display(dNy)
                # display(dNz)
                # display(J)
                
                G = [inv(J) zeros(3,3) zeros(3,3);zeros(3,3) inv(J) zeros(3,3);zeros(3,3) zeros(3,3) inv(J)];
                dN=zeros(9,24)
                dN[1,1:3:24] = dNx; dN[2,1:3:24] = dNy; dN[3,1:3:24] = dNz;
                dN[4,2:3:24] = dNx; dN[5,2:3:24] = dNy; dN[6,2:3:24] = dNz;
                dN[7,3:3:24] = dNx; dN[8,3:3:24] = dNy; dN[9,3:3:24] = dNz;
                
                
                Be = L*G*dN;
                # display((G))
                # display(size(dN))
                # display((Be))



                Ke .= Ke .+ GaussWeigh[ii]*GaussWeigh[jj]*GaussWeigh[kk]*det(J)*(Be'*DH*Be);
            end
        end
    end
    # display(Ke)

    return Ke
end

function brick_stiffnessMatrix()
    # elastic matrix formulation
    nu=0.3;
    D = 1.0 /((1+nu)*(1-2*nu))*[1-nu nu nu 0 0 0; nu 1-nu nu 0 0 0;
        nu nu 1-nu 0 0 0; 0 0 0 (1-2*nu)/2 0 0; 0 0 0 0 (1-2*nu)/2 0;
        0 0 0 0 0 (1-2*nu)/2];
    #stiffness matrix formulation
    A = [32 6 -8 6 -6 4 3 -6 -10 3 -3 -3 -4 -8;
        -48 0 0 -24 24 0 0 0 12 -12 0 12 12 12];
    k = 1/144*A'*[1; nu];
    K1 = [k[1] k[2] k[2] k[3] k[5] k[5];
        k[2] k[1] k[2] k[4] k[6] k[7];
        k[2] k[2] k[1] k[4] k[7] k[6];
        k[3] k[4] k[4] k[1] k[8] k[8];
        k[5] k[6] k[7] k[8] k[1] k[2];
        k[5] k[7] k[6] k[8] k[2] k[1]];
    K2 = [k[9] k[8] k[12] k[6] k[4] k[7];
        k[8] k[9] k[12] k[5] k[3] k[5];
        k[10] k[10] k[13] k[7] k[4] k[6];
        k[6] k[5] k[11] k[9] k[2] k[10];
        k[4] k[3] k[5] k[2] k[9] k[12]
        k[11] k[4] k[6] k[12] k[10] k[13]];
    K3 = [k[6] k[7] k[4] k[9] k[12] k[8];
        k[7] k[6] k[4] k[10] k[13] k[10];
        k[5] k[5] k[3] k[8] k[12] k[9];
        k[9] k[10] k[2] k[6] k[11] k[5];
        k[12] k[13] k[10] k[11] k[6] k[4];
        k[2] k[12] k[9] k[4] k[5] k[3]];
    K4 = [k[14] k[11] k[11] k[13] k[10] k[10];
        k[11] k[14] k[11] k[12] k[9] k[8];
        k[11] k[11] k[14] k[12] k[8] k[9];
        k[13] k[12] k[12] k[14] k[7] k[7];
        k[10] k[9] k[8] k[7] k[14] k[11];
        k[10] k[8] k[9] k[7] k[11] k[14]];
    K5 = [k[1] k[2] k[8] k[3] k[5] k[4];
        k[2] k[1] k[8] k[4] k[6] k[11];
        k[8] k[8] k[1] k[5] k[11] k[6];
        k[3] k[4] k[5] k[1] k[8] k[2];
        k[5] k[6] k[11] k[8] k[1] k[8];
        k[4] k[11] k[6] k[2] k[8] k[1]];
    K6 = [k[14] k[11] k[7] k[13] k[10] k[12];
        k[11] k[14] k[7] k[12] k[9] k[2];
        k[7] k[7] k[14] k[10] k[2] k[9];
        k[13] k[12] k[10] k[14] k[7] k[11];
        k[10] k[9] k[2] k[7] k[14] k[7];
        k[12] k[2] k[9] k[11] k[7] k[14]];
    KE = 1/((nu+1)*(1-2*nu))*[ K1 K2 K3 K4;
                                K2' K5 K6 K3';
                                K3' K6 K5' K2';
                                K4 K3 K2 K1'];
    # strain matrix formulation
    B_1=[-0.044658 0 0 0.044658 0 0 0.16667 0;
        0 -0.044658 0 0 -0.16667 0 0 0.16667;
        0 0 -0.044658 0 0 -0.16667 0 0;
        -0.044658 -0.044658 0 -0.16667 0.044658 0 0.16667 0.16667;
        0 -0.044658 -0.044658 0 -0.16667 -0.16667 0 -0.62201;
        -0.044658 0 -0.044658 -0.16667 0 0.044658 -0.62201 0];
    B_2=[0 -0.16667 0 0 -0.16667 0 0 0.16667;
        0 0 0.044658 0 0 -0.16667 0 0;
        -0.62201 0 0 -0.16667 0 0 0.044658 0;
        0 0.044658 -0.16667 0 -0.16667 -0.16667 0 -0.62201;
        0.16667 0 -0.16667 0.044658 0 0.044658 -0.16667 0;
        0.16667 -0.16667 0 -0.16667 0.044658 0 -0.16667 0.16667];
    B_3=[0 0 0.62201 0 0 -0.62201 0 0;
        -0.62201 0 0 0.62201 0 0 0.16667 0;
        0 0.16667 0 0 0.62201 0 0 0.16667;
        0.16667 0 0.62201 0.62201 0 0.16667 -0.62201 0;
        0.16667 -0.62201 0 0.62201 0.62201 0 0.16667 0.16667;
        0 0.16667 0.62201 0 0.62201 0.16667 0 -0.62201];
    B=[B_1 B_2 B_3];
    return KE,B,D
end

####################################
## SUB FUNCTION: EBHM2D
function EBHM2D(den, lx, ly, E0, Emin, nu, penal)
    # the initial definitions of the PUC
    D0=E0/(1-nu^2)*[1 nu 0; nu 1 0; 0 0 (1-nu)/2];  # the elastic tensor
    nely, nelx = size(den);
    nele = nelx*nely;
    dx = lx/nelx; dy = ly/nely;

    U = zeros(2*(nely+1)*(nelx+1),3);


    Ke = elementMatVec2D(dx/2, dy/2, D0);
    Num_node = (1+nely)*(1+nelx);
    nodenrs = reshape(1:Num_node,1+nely,1+nelx);
    edofVec = reshape(2 .*nodenrs[1:end-1,1:end-1] .+1,nele,1);
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*nely.+[2 3 0 1] -2 -1],nele,1)

    # 3D periodic boundary formulation
    alldofs = (1:2*(nely+1)*(nelx+1));
    # n1 = [nodenrs[end,[1,end]],nodenrs[1,[end,1]]];
    # d1 = reshape( [(2*n1-1);2*n1],1,8);
    # n3 = [nodenrs[2:end-1,1]',nodenrs[end,2:end-1]];
    # d3 = reshape( [(2*n3-1);2*n3],1,2*(nelx+nely-2));
    # n4 = [nodenrs[2:end-1,end]',nodenrs[1,2:end-1]];
    # d4 = reshape( [(2*n4-1);2*n4],1,2*(nelx+nely-2));
    # d2 = setdiff(alldofs,[d1,d3,d4] );
    

    n1 = vcat(nodenrs[end,[1,end]],nodenrs[1,[end,1]]);
    d1 = vec(reshape([(2 .* n1 .-1) 2 .*n1]',1,8));
    n3 = [vec(nodenrs[2:(end-1),1]');vec(nodenrs[end,2:(end-1)])];
    d3 = vec(reshape([(2 .*n3 .-1) 2 .*n3]',1,2*(nelx+nely-2)));
    n4 = [vec(nodenrs[2:end-1,end]');vec(nodenrs[1,2:end-1])];
    d4 = vec(reshape([(2 .*n4 .-1) 2 .*n4]',1,2*(nelx+nely-2)));
    d2 = setdiff(vcat(alldofs...),union(union(d1,d3),d4));
    
    e0 = Matrix(1.0I, 3, 3);

    ufixed = zeros(8,3);
    for j = 1:3
        ufixed[3:4,j] = [e0[1,j] e0[3,j]/2; e0[3,j]/2 e0[2,j]]*[lx;0];
        ufixed[7:8,j] = [e0[1,j] e0[3,j]/2; e0[3,j]/2 e0[2,j]]*[0;ly];
        ufixed[5:6,j] = ufixed[3:4,j]+ufixed[7:8,j];
    end
    wfixed = [repeat(ufixed[3:4,:],nely-1,1); repeat(ufixed[7:8,:],nelx-1,1)];

    
    # the reduced elastic equilibrium equation to compute the induced displacement field
    iK = reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1);
    jK = reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1);

    sK = reshape(Ke[:]*(Emin .+den[:]'.^penal*(1 .-Emin)),64*nelx*nely,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K + K')/2;

    # Kr = [K[d2,d2],K[d2,d3]+K[d2,d4];K[d3,d2]+K[d4,d2],K[d3,d3]+K[d4,d3]+K[d3,d4]+K[d4,d4]];
    Kr = vcat(hcat(K[d2,d2] , K[d2,d3]+K[d2,d4]),hcat((K[d3,d2]+K[d4,d2]),(K[d3,d3]+K[d4,d3]+K[d3,d4]+K[d4,d4])));

    # U[d1,:] .= ufixed;
    # U[[d2;d3],:] = Kr\(-[K[d2,d1]; K[d3,d1]+K[d4,d1]]*ufixed-[K[d2,d4]; K[d3,d4]+K[d4,d4]]*wfixed);
    # U[d4,:] = U[d3,:]+wfixed;

    U[d1,:] .= ufixed;
    U[[d2;d3],:] = Kr\(-[K[d2,d1]; K[d3,d1]+K[d4,d1]]*ufixed-[K[d2,d4]; K[d3,d4]+K[d4,d4]]*wfixed);
    U[d4,:] = U[d3,:]+wfixed;

    # homogenization to evaluate macroscopic effective properties
    DH = zeros(3,3); 
    qe = Array{Any,2}(undef, 3, 3); 
    dDH = Array{Any,2}(undef, 3, 3);

    cellVolume = lx*ly;
    for i = 1:3
        for j = 1:3
            U1 = U[:,i]; U2 = U[:,j];
            qe[i,j] = reshape(sum((U1[edofMat]*Ke).*U2[edofMat],dims=2),nely,nelx)/cellVolume;
            DH[i,j] = sum(sum((Emin .+den.^penal*(1 .-Emin)).*qe[i,j] ));
            dDH[i,j] = penal*(1-Emin)*den.^(penal-1).*qe[i,j];
        end
    end
    # disp("--- Homogenized elasticity tensor ---"); disp(DH)
    return DH, dDH
end


## SUB FUNCTION: EBHM3D
function EBHM3D(den, lx, ly, lz, E0, Emin, nu, penal,mgcg=false)
    # the initial definitions of the PUC
    D0 = E0/(1+nu)/(1-2*nu)*
        [ 1-nu   nu   nu     0          0          0     ;
            nu 1-nu   nu     0          0          0     ;
            nu   nu 1-nu     0          0          0     ;
             0    0    0 (1-2*nu)/2     0          0     ;
             0    0    0     0      (1-2*nu)/2     0     ;
             0    0    0     0          0      (1-2*nu)/2];
    
    nely, nelx, nelz = size(den); 
    nele = nelx*nely*nelz;
    dx = lx/nelx; dy = ly/nely; dz = lz/nelz;
    Ke = elementMatVec3D(dx/2, dy/2, dz/2, D0);
    Num_node = (1+nely)*(1+nelx)*(1+nelz);
    nodenrs = reshape(1:Num_node,1+nely,1+nelx,1+nelz);

    U = zeros(3*(nelx+1)*(nely+1)*(nelz+1),6);

    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1] .+1,nelx*nely*nelz,1);

    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*nely.+[3 4 5 0 1 2] -3 -2 -1 3*(nelx+1)*(nely+1).+[0 1 2 3*nely.+[3 4 5 0 1 2] -3 -2 -1]], nele, 1);
    
        # 3D periodic boundary formulation
    # the nodes classification
    n1 = hcat(nodenrs[end, [1 end], 1], nodenrs[1, [end 1], 1], nodenrs[end, [1 end], end] ,nodenrs[1, [end 1], end]);
    d1 = Int.(vec(reshape(vcat(3.0.*n1.-2, 3.0.*n1.-1,3.0.*n1),3*length(n1),1)));
    
    n3 = vcat(vec(reshape(nodenrs[end,1,2:end-1] ,1,length(nodenrs[end,1,2:end-1] ))),               # AE
        vec(reshape(nodenrs[1, 1, 2:end-1] ,1,length(nodenrs[1, 1, 2:end-1] ))),               # DH
        vec(reshape(nodenrs[end,2:end-1,1] ,1,length(nodenrs[end,2:end-1,1] ))),               # AB
        vec(reshape(nodenrs[1, 2:end-1, 1] ,1,length(nodenrs[1, 2:end-1, 1] ))),               # DC
        vec(reshape(nodenrs[2:end-1, 1, 1] ,1,length(nodenrs[2:end-1, 1, 1] ))),               # AD
        vec(reshape(nodenrs[2:end-1,1,end] ,1,length(nodenrs[2:end-1,1,end] ))),               # EH
        vec(reshape(nodenrs[2:end-1, 2:end-1, 1] ,1,length(nodenrs[2:end-1, 2:end-1, 1] ))),   # ABCD
        vec(reshape(nodenrs[2:end-1, 1, 2:end-1] ,1,length(nodenrs[2:end-1, 1, 2:end-1] ))),   # ADHE
        vec(reshape(nodenrs[end,2:end-1,2:end-1] ,1,length(nodenrs[end,2:end-1,2:end-1] ))))';   # ABFE 

    d3 = vec(Int.(reshape( vcat(3.0.*n3.-2 ,3.0.*n3.-1, 3.0.*n3),3*length(n3),1)));

    n4 = vcat(vec(reshape(nodenrs[1, end, 2:end-1] ,1,length((nodenrs[1, end, 2:end-1] )))),           # CG
        vec(reshape(nodenrs[end,end,2:end-1] ,1,length((nodenrs[end,end,2:end-1] )))),           # BF
        vec(reshape(nodenrs[1, 2:end-1, end] ,1,length((nodenrs[1, 2:end-1, end] )))),           # HG
        vec(reshape(nodenrs[end,2:end-1,end] ,1,length((nodenrs[end,2:end-1,end] )))),           # EF
        vec(reshape(nodenrs[2:end-1,end,end] ,1,length((nodenrs[2:end-1,end,end] )))),           # FG
        vec(reshape(nodenrs[2:end-1, end, 1] ,1,length((nodenrs[2:end-1, end, 1] )))),           # BC
        vec(reshape(nodenrs[2:end-1,2:end-1,end] ,1,length((nodenrs[2:end-1,2:end-1,end] )))),   # EFGH
        vec(reshape(nodenrs[2:end-1,end,2:end-1] ,1,length((nodenrs[2:end-1,end,2:end-1] )))),   # BCGF
        vec(reshape(nodenrs[1, 2:end-1, 2:end-1] ,1,length((nodenrs[1, 2:end-1, 2:end-1] )))))';   # DCGH

    d4 = vec(Int.(reshape( vcat(3.0*n4.-2, 3.0*n4.-1 ,3.0*n4),3*length(n4),1)));

    n2 = setdiff(nodenrs[:],[n1[:];n3[:];n4[:]] ); 
    d2 = vec(Int.(reshape( [3.0.*n2.-2 3*n2.-1 3.0.*n2],3*length(n2),1)));
    
    # the imposing of six linearly independent unit test strains
    e = Matrix(1.0I, 6, 6); 
    ufixed = zeros(24,6);
    vert_cor = [0  lx lx  0  0 lx lx  0;
                0   0 ly ly  0  0 ly ly;
                0   0  0  0 lz lz lz lz];
    
    e0 = Matrix(1.0I, 6, 6);            
    for i = 1:6
        epsilon = [e0[i,1] e0[i,4]/2 e0[i,6]/2;
                    e0[i,4]/2   e0[i,2] e0[i,5]/2;
                    e0[i,6]/2 e0[i,5]/2   e0[i,3]];
        ufixed[:,i] = reshape(epsilon*vert_cor,24);
    end
    # 3D boundary constraint equations
    wfixed = [repeat(ufixed[  7:9,:],length((nodenrs[end,1,2:end-1] )),1);                    # C
              repeat(ufixed[  4:6,:]-ufixed[10:12,:],length((nodenrs[1, 1, 2:end-1] )),1);    # B-D
              repeat(ufixed[22:24,:],length((nodenrs[end,2:end-1,1] )),1);                    # H
              repeat(ufixed[13:15,:]-ufixed[10:12,:],length((nodenrs[1, 2:end-1, 1] )),1);    # E-D
              repeat(ufixed[16:18,:],length((nodenrs[2:end-1, 1, 1] )),1);                    # F
              repeat(ufixed[  4:6,:]-ufixed[13:15,:],length((nodenrs[2:end-1,1,end] )),1);    # B-E
              repeat(ufixed[13:15,:],length((nodenrs[2:end-1, 2:end-1, 1] )),1);              # E
              repeat(ufixed[  4:6,:],length((nodenrs[2:end-1, 1, 2:end-1] )),1);              # B
              repeat(ufixed[10:12,:],length((nodenrs[end,2:end-1,2:end-1] )),1)];             # D


    # the reduced elastic equilibrium equation to compute the induced displacement field
    iK = reshape(kron(edofMat,ones(24,1))',24*24*nele,1);
    jK = reshape(kron(edofMat,ones(1,24))',24*24*nele,1);

    sK = reshape(Ke[:]*(Emin .+den[:]'.^penal*(1 .-Emin)),24*24*nele,1);

    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K.+K')./2.0;
    Kr = vcat(hcat(K[d2,d2] , K[d2,d3]+K[d2,d4]),hcat((K[d3,d2]+K[d4,d2]),(K[d3,d3]+K[d4,d3]+K[d3,d4]+K[d4,d4])));

    ndof = 3*(nelx+1)*(nely+1)*(nelz+1);


    if mgcg

        U[d1,:] .= ufixed;
        

        A = Kr
        b = (-[K[d2,d1];K[d3,d1]+K[d4,d1]]*ufixed-[K[d2,d4];K[d3,d4]+K[d4,d4]]*wfixed);
        #200 is good 500 is perfect 150 is goodish 100 is good enough
        U[[d2;d3],1].= Krylov.cg(A, b[:,1],atol=1.0e-10,rtol=1.0e-10,itmax =100,verbose=0)[1]
        U[[d2;d3],2].= Krylov.cg(A, b[:,2],atol=1.0e-10,rtol=1.0e-10,itmax =100,verbose=0)[1]
        U[[d2;d3],3].= Krylov.cg(A, b[:,3],atol=1.0e-10,rtol=1.0e-10,itmax =100,verbose=0)[1]
        U[[d2;d3],4].= Krylov.cg(A, b[:,4],atol=1.0e-10,rtol=1.0e-10,itmax =100,verbose=0)[1]
        U[[d2;d3],5].= Krylov.cg(A, b[:,5],atol=1.0e-10,rtol=1.0e-10,itmax =100,verbose=0)[1]
        U[[d2;d3],6].= Krylov.cg(A, b[:,6],atol=1.0e-10,rtol=1.0e-10,itmax =100,verbose=0)[1]

        U[d4,:] = U[d3,:] + wfixed;

       

    else
        
        U[d1,:] .= ufixed;
        U[ [d2;d3],:] = Kr\(-[K[d2,d1];K[d3,d1]+K[d4,d1]]*ufixed-[K[d2,d4];K[d3,d4]+K[d4,d4]]*wfixed);
        U[d4,:] = U[d3,:] + wfixed;
    end

    

    # homogenization to evaluate macroscopic effective properties
    qe = Array{Any,2}(undef, 6, 6);
    DH = zeros(6,6); 
    dDH = Array{Any,2}(undef, 6, 6);
    cellVolume = lx*ly*lz;
    for i = 1:6
        for j = 1:6
            U1 = U[:,i]; U2 = U[:,j];
            qe[i,j] = reshape(sum((U1[edofMat]*Ke).*U2[edofMat],dims=2),nely,nelx,nelz);
            DH[i,j] = 1.0./cellVolume*sum(sum(sum((Emin .+den.^penal*(1 .-Emin)).*qe[i,j])));
            dDH[i,j] = 1.0./cellVolume*(penal*(1 .-Emin)*den.^(penal-1).*qe[i,j]);
        end
    end
    # disp('--- Homogenized elasticity tensor ---'); disp(DH)
    return DH, dDH
end


#####################################

function evaluateCH(CH,dens)

    U,S,V = svd(CH);
    sigma = S;
    k = sum(sigma .> 1e-15);
    SH = (U[:,1:k] * diagm(0=>(1 ./sigma[1:k])) * V[:,1:k]')'; # more stable SVD (pseudo)inverse
    EH = [1/SH[1,1], 1/SH[2,2], 1/SH[3,3]]; # effective Young's modulus
    GH = [1/SH[4,4], 1/SH[5,5], 1/SH[6,6]]; # effective shear modulus
    vH = [-SH[2,1]/SH[1,1]  -SH[3,1]/SH[1,1]  -SH[3,2]/SH[2,2];
         -SH[1,2]/SH[2,2]  -SH[1,3]/SH[3,3]  -SH[2,3]/SH[3,3]]; # effective Poisson's ratio
        
    props = Dict("CH"=>CH, "SH"=>SH, "EH"=>EH, "GH"=>GH, "vH"=>vH, "density"=>dens);
   
        
    if true
        println("\n--------------------------EFFECTIVE PROPERTIES--------------------------\n")
        println("Youngs Modulus:____E11_____|____E22_____|____E33_____\n")
        println("               $(EH[1]) | $(EH[2]) | $(EH[3])\n\n")
        println("Shear Modulus:_____G23_____|____G31_____|____G12_____\n")
        println("               $(GH[1]) | $(GH[2]) | $(GH[3])\n\n")
        println("Poissons Ratio:____v12_____|____v13_____|____v23_____\n")
        println("               $(vH[1,1]) | $(vH[1,2]) | $(vH[1,3])\n\n")
        println("               ____v21_____|____v31_____|____v32_____\n")
        println("               $(vH[2,1]) | $(vH[2,2]) | $(vH[2,3])\n\n")
        println("------------------------------------------------------------------------")
    end
        
        
    return SH
end

#####################################