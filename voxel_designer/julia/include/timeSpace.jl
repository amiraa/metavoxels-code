# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021

# Based on:
# Function: Space-Time Topology Optimization for Additive Manufacturing: 
#           Concurrent Optimization of Structural Layout and Fabrication Sequence
# Author:  Weiming Wang (wwmdlut@gmail.com)
# Version: 2020-05-14
# Usage:   [xPhys, tPhys] = Space_Time_TopOpt_Gravity(90, 30, 500, 8, 0.6, 0.5);
# volfrac: volume constraint
# nStage:  the number of layers
# nloop:   the number of iterations
# nely:    dimension in y-axis
# nelx:    dimension in x-axis
# Theta:   parameter in objective function
include("./mmaSolver.jl");

function Space_Time_TopOpt_Gravity(nelx, nely, problem,nloop, nStage, volfrac, Theta)
    


    nelx=Int(nelx);nely=Int(nely) ;

    tt=:matter
    p=cgrad(tt, nStage)
    # p=cgrad(tt, nStage, categorical = true, rev = true)
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:nStage
        append!(cschemeList,[p[i]])
    end
    cscheme=cgrad(cschemeList)

    ## Connectivity matrix for continuity 
    lrmin = 2;
    iH = ones(convert(Int,nelx*nely*(2*(ceil(lrmin)-1)+1)^2),1)
    jH = ones(Int,size(iH))
    sH = zeros(size(iH))
    k = 0;
    for i1 = 1:nelx
        for j1 = 1:nely
            e1 = (i1-1)*nely+j1
            for i2 = max(i1-(ceil(lrmin)-1),1):min(i1+(ceil(lrmin)-1),nelx)
                for j2 = max(j1-(ceil(lrmin)-1),1):min(j1+(ceil(lrmin)-1),nely)
                    e2 = (i2-1)*nely+j2
                    if e1 == e2
                        continue;
                    end
                    k = k+1
                    iH[k] = e1
                    jH[k] = e2
                    sH[k] = 1
                end
            end
        end
    end
    L = sparse(vec(iH),vec(jH),vec(sH))
    M = sparse(repeat(sum(L, dims=2), 1, size(L)[2]));

    # E = eye(size(L)); 
    E = sparse(1.0I, size(L)[1], size(L)[2]);
    L = E - L./M;
    # L = sparse(L)

    ## Material properties
    Emax = 1;
    Emin = 1e-9;
    nu = 0.3;

    ## Initialization several parameters
    penal = 3;      # stiffness penalty
    rmin = 2;     # density filter radius

    ## Stiffness matrix for element
    KE=lk();

    ## Density filter
    H ,Hs=make_filter(nelx,nely,rmin);

    ## Definition of gravity force
    II = Array{Float64}(undef, 0); ;J = Array{Float64}(undef, 0);S = Array{Float64}(undef, 0);
    fe = 1 / (nely*nelx);
    for x = 1 : nely
        for y = 1 : nelx
            II = [II; (y-1)*(nely+1) + x; (y-1)*(nely+1)+x+1; y*(nely+1)+x; y*(nely+1)+x+1];
            J = [J; (y-1)*nely + x; (y-1)*nely + x; (y-1)*nely + x; (y-1)*nely + x];
            S = [S; fe/4; fe/4; fe/4; fe/4];
        end
    end
    C = sparse(vec(II), vec(J), vec(S));

    ##
    beta = 1;       # projection parameter
    eta = 0.5;      # projection threshold, fixed at 0.5

    ## Initialize density field
    x = fill(volfrac,nely,nelx);
    xTilde = x;
    xPhys = (tanh.(beta*eta) .+ tanh.(beta*(xTilde .-eta))) / (tanh.(beta*eta)  .+ tanh.(beta*(1 .-eta)));

    # ## Initialize time field (start from left boundary)
    tPhys = zeros(nely, nelx);
    t = range(0, stop=1, length=nelx)
    for i = 1 : nelx
        tPhys[:, i] .= t[i];
    end

    ## Initial time field (start from Lower left)
    s = repeat(1 : nely, 1, nelx);
    yElement = s[:].-0.5;
    t = repeat(1:nelx, 1, nely)';
    xElement = t[:].-0.5;
    V = [xElement yElement];

    vect = V - repeat(V[1, :],  1,size(V)[1])';
    dis2 = sum(vect.*vect, dims=2);
    tPhys = sqrt.(dis2) / maximum(sqrt.(dis2));
    tPhys = reshape(tPhys, nely, nelx);
    # tPhys = ones(nely, nelx);

    ## Freedom of degree and loads
    U,F,freedofs=problem(nelx,nely)

    ## Initialization
    t = tPhys;
    println("tPhys ,min:$(round.(minimum(tPhys),digits=2)),max:$(round.(maximum(tPhys),digits=2))")

    xold1 = x[:];
    xold2 = x[:];
    xold1 = [xold1; zeros(nely*nelx, 1)];
    xold2 = [xold2; zeros(nely*nelx, 1)];
    low = 0;
    upp = 0;
    rou = 10;

    for loop =1:nloop
        ## Parameter for projection on time field
        if  mod(loop, 30) == 0 && rou < 100
            println("rou change.")
            rou = rou + 10;
        end
        
        ## Parameter for projection on density field
        if mod(loop, 20) == 0 && beta < 40
            println("beta change.")
            beta = beta + 2;
        end
        
        ## Ojectives and sensitivities
        dc = zeros(nely, nelx);
        dt = zeros(nely, nelx);
        
        ## Compliance of the whole structure
        c, dcx = Cal_c_ce_whole(nelx, nely, KE, xPhys, Emin, Emax, penal, freedofs, F);
        obj = c;
        
        dx = beta * (1 .-tanh.(beta*(xTilde .-eta)).*tanh.(beta*(xTilde .-eta))) / (tanh.(beta*eta) + tanh.(beta*(1-eta)));
        dc[:] = dc[:] + H*(dcx[:].*dx[:]./Hs);
        
        ## Compliances of the intermediate structures
        tP = range(0, stop=1, length=nStage + 1)
        for i = 1 : nStage
            ti = tP[i+1];
            c, dcx, dct = Cal_c_ce_for_gravity(nelx, nely, KE, xPhys, tPhys, Emin, Emax, penal, ti, C, rou, freedofs);
            obj = obj + Theta[i]*c;
            dc[:] = dc[:] + Theta[i]*H*(dcx[:].*dx[:]./Hs);
            dt[:] = dt[:] + Theta[i]*H*(dct[:]./Hs);
        end
        
        df0 = [dc dt];
        df0dx = df0[:];
        f0val = obj;
        n=length(df0dx);
        
        ## lower and upper bounds for timefield and densityfield
        move = 0.01;    
        xminx=max.(0.0, x[:].-move);
        xmaxx=min.(1, x[:].+move);
        tmove = 0.01;
        xmint=max.(0.0, t[:] .-tmove);
        xmaxt=min.(1, t[:] .+tmove);
        xmin = [xminx; xmint];
        xmax = [xmaxx; xmaxt];
        xval = [x[:]; t[:]];
        
        ## Global volume constraint
        fval = sum(sum(xPhys)) / (nelx*nely*volfrac) - 1;
        print_out = fval[end];

        dv = ones(nely,nelx);
        dv[:] = H*(dv[:].*dx[:]./Hs);
        dfdx = [dv[:]'/(nelx*nely*volfrac) zeros(1, nely*nelx)];
        
        ## Continuity constraint
        Nei = 1:nely;
        LL = L;
        kk = 2*(nely*nelx); # controlling the smoothness of the time field
        A = LL*tPhys[:];
        B = A.^2/(nely*nelx);
        fval = [fval; kk*(sum(B)-1.0e-6)];
        print_out = [print_out; fval[end]/kk];
        dft = kk*2*LL'*A;
        dft = H*(dft./Hs)/(nely*nelx);
        dfdx = [dfdx; zeros(1, nely*nelx) dft'];
        
        ## Start points
        fval = [fval; (tPhys[Nei]'[:] .- 1.0e-9)];
        ss = zeros(length(Nei), nely*nelx);
        for ii = 1 : length(Nei)
            ss[ii, Nei[ii]] = 1;
        end
        dfdx = [dfdx; zeros(length(Nei), nely*nelx) (H*(ss'./repeat(Hs, 1, length(Nei))))'];
        
       ## Volume constraints for intermediate structures
        percent = 1 / nStage;
        tP = range(0, stop=1, length=nStage+1);
        for i = 1 : nStage
            ##
            ti = tP[i+1];
            ft = 1 .- (tanh.(rou*ti) .+ tanh.(rou*(tPhys .- ti)))/(tanh.(rou*ti) .+ tanh.(rou*(1 .-ti)));
            dfdt = -(rou*(tanh.(rou*(tPhys .- ti)).^2 .- 1))/(tanh.(rou*(ti .- 1)) - tanh.(rou*ti));
            xtJoint = xPhys.*ft;
            fval = [fval; sum(xtJoint[:])/(nelx*nely*volfrac) - i*percent];
            print_out = [print_out; fval[end]];
            dfx = ft/(nelx*nely*volfrac);
            dfx = H*(dfx[:].*dx[:]./Hs);
            dft = xPhys.*dfdt/(nelx*nely*volfrac);
            dft = H*(dft[:]./Hs);
            dfdx = [dfdx; dfx[:]' dft[:]'];

            #
            fval = [fval; -sum(xtJoint[:])/(nelx*nely*volfrac) + i*percent - 1.0e-5];
            print_out = [print_out; fval[end]];
            dfdx = [dfdx; -dfx[:]'  0.0.-dft[:]'];
        end
        
        ## Optimizing with MMA solver
        m=length(fval);
        mdof = 1:m;
        a0 = 1;
        a = zeros(m);
        c_ = ones(m).*1000;
        d = zeros(m);


        display("sizes")
        display(m)
        display(size(xval))
        display(size(fval))
        display(size(dfdx))
        display(size(fval[mdof]))
        display(size(dfdx[mdof,:]))

        xmma, ymma, zmma, lam, xsi, eta_, mu, zet, s, low, upp = mmasub(m, n, loop, xval, xmin, xmax, xold1, xold2,f0val, df0dx, fval[mdof], dfdx[mdof,:],low, upp, a0, a, c_, d);
        
        xnew = reshape(xmma, nely, Int(size(xmma)[1]/nely))
        xold2 = xold1;
        xold1 = xval;
        s = xnew[:, 1:nelx];
        
        xTilde[:] = (H*s[:])./Hs;
        xPhys = (tanh.(beta*eta) .+ tanh.(beta*(xTilde .-eta))) / (tanh.(beta*eta) .+ tanh.(beta*(1 .-eta)));
        x = s;
        
        ##
        t = xnew[:, nelx+1 : end];
        tPhys = t;
        tPhys[:] =  (H*t[:])./Hs;
        
        ## Display and show results
        # println("It:$loop Obj:$(round.(obj,digits=2)) Vol:$(round.(sum(xPhys)/(nelx*nely),digits=2)) Cons:$(round.(print_out,digits=2))")
        println("It:$loop Obj:$(round.(obj,digits=2)) Vol:$(round.(sum(xPhys)/(nelx*nely),digits=2)),min:$(round.(minimum(tPhys),digits=2)),max:$(round.(maximum(tPhys),digits=2))")

        if mod(loop, 5) == 0 ||loop==1

            xPhysTemp=copy(( 0.0.-xPhys))
            # xPhysTemp=copy(( 0.0.-xPhys))[end:-1:1,:]
            xPhysTemp=hcat(xPhysTemp,xPhysTemp[:,end:-1:1])
            display(Plots.heatmap(xPhysTemp,xaxis=nothing,showaxis = false,clims=(-1.0, 0.0),yaxis=nothing,legend=nothing,fc=:grays,aspect_ratio=:equal))#,clims=(0.0, 1.0)
            Plots.heatmap(xPhysTemp,xaxis=nothing,showaxis = false,clims=(-1.0, 0.0),yaxis=nothing,legend=nothing,fc=:grays,aspect_ratio=:equal)
            frame(anim1)
            # heatmap(tPhys,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,clims=(0.0, 1.0),aspect_ratio=:equal)#,clims=(0.0, 1.0)
            # display(contour(tPhys, levels = minimum(tPhys):(maximum(tPhys)-minimum(tPhys))/nStage:maximum(tPhys),w=0.1, linecolor=:black))


            display(heatmap(hcat(tPhys,tPhys[:,end:-1:1]),xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,aspect_ratio=:equal))#,clims=(0.0, 1.0)
            # display(contourf!(tPhys, levels = minimum(tPhys):(maximum(tPhys)-minimum(tPhys))/nStage:maximum(tPhys),w=0.1, linecolor=:black))

            # TT=copy(tPhys)
            # TT[Bool.((xPhys).<0.8)].=1.0       
            # display(heatmap(TT,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,clims=(0.0, 1.0),aspect_ratio=:equal,fc=:hot))#,clims=(0.0, 1.0)
            # display(contourf!(TT, levels = minimum(tPhys):(maximum(tPhys)-minimum(tPhys))/nStage:maximum(tPhys),w=0.1, linecolor=:black))

            tPhysTemp=copy(max.(tPhys,Emin))
            tPhysTemp=copy(min.(tPhys,1.0))
            tPhysTemp[xPhys.<0.7].=0.0
            d=1.0/nStage
            # levels=[0.]
            for i in 1:nStage
                out= (tPhysTemp.> (i*d-d)) .& (tPhysTemp.<= (i*d))
                tPhysTemp[out].=i*d
                # append!(levels,[i*d])
            end
            # tPhysTemp=tPhysTemp[end:-1:1,:]
            tPhysTemp=tPhysTemp
            tPhysTemp=hcat(tPhysTemp,tPhysTemp[:,end:-1:1])
            display(heatmap(tPhysTemp,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,aspect_ratio=:equal,fc=cscheme,clims=(0.0, 1.0)))
            (heatmap(tPhysTemp,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,aspect_ratio=:equal,fc=cscheme,clims=(0.0, 1.0)))
            frame(anim2)

            # tPhysTemp=copy(max.(tPhys,Emin))
            # tPhysTemp=copy(min.(tPhys,1.0))
            d=1/nStage
            tPhysTemp=copy(tPhys)
            if (length(tPhysTemp[xPhys.>0.7])>0)
                tPhysTemp=mapValss.(tPhysTemp, minimum(tPhysTemp[xPhys.>0.7]), maximum(tPhysTemp[xPhys.>0.7]), d, 1)
            else                
                tPhysTemp=mapValss.(tPhysTemp, minimum(tPhysTemp[:]), maximum(tPhysTemp[:]), d, 1)
            end
            tPhysTemp[xPhys.<0.7].=0.0
            # levels=[0.]
            for i in 1:nStage
                out= (tPhysTemp.> (i*d-d)) .& (tPhysTemp.<= (i*d))
                tPhysTemp[out].=i*d
                # append!(levels,[i*d])
            end
            tPhysTemp=hcat(tPhysTemp,tPhysTemp[:,end:-1:1])
            display(heatmap(tPhysTemp,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,aspect_ratio=:equal,fc=cscheme,clims=(0.0, 1.0)))
            heatmap(tPhysTemp,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,aspect_ratio=:equal,fc=cscheme,clims=(0.0, 1.0))
            frame(anim3)
                
        end
        
    end
    return xPhys, tPhys,anim1,anim2,anim2

end

function Space_Time_TopOpt_Robot(nelx,nely,problem,nloop, nStage, volfrac,Upper_Lower_Print, Upper_And_Lower_Print, Theta)
    t=:matter
    # p=cgrad(t, nStage, categorical = true, rev = true)
    p=cgrad(t, nStage)
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:nStage
        append!(cschemeList,[p[i]])
    end
    cscheme=cgrad(cschemeList)

    ## Locations of the robots
    tPrint = range(0, stop=1, length=nStage+1);
    xRobot = range(1, stop=nelx, length=nStage + 1);
    yRobot = (zeros(length(xRobot)) .+ 1);
    fPoint = Int.((floor.(xRobot).-1)*(nely.+1).+yRobot);

    if Upper_Lower_Print == 1
        fPointLower = fPoint .+ nely;
        fPointUpper = fPoint;
        fPoint = [fPointUpper[1:2:end] [fPointLower[2:2:end]; 0]];
        fPoint = fPoint';
        fPoint = fPoint[:];
        # fPoint[end] = [];
        fPoint[end] = nelx*(nely+1)+1;
        fPoint = unique(fPoint);
        xRobot = floor.(xRobot[:]);
        yRobot = [yRobot[1:2:end] [yRobot[2:2:end].+nely; zeros(mod(length(xRobot), 2), 1)]]';
        yRobot = yRobot[:];
        yRobot = yRobot[1:length(xRobot), :];
        PT = tPrint[1:end-1];
    elseif Upper_And_Lower_Print == 1
        fPointLower = fPoint .+ nely;
        fPointUpper = fPoint;
        fPoint = [fPointUpper fPointLower];
        xRobot = [xRobot xRobot]';
        xRobot = xRobot[:];
        yRobot = [yRobot yRobot.+nely]';
        yRobot = yRobot[:];
        PT = [tPrint[1:end-1]' tPrint[1:end-1]']';
        PT = PT[:];

    else
        PT = tPrint[1:end-1];
        fPoint[end] = nelx*(nely+1)+1;
    end

    rRobot = nely+20; # radius of robot arm
    s = repeat(1 : nely, 1, nelx);
    yElement = s[:];
    t = repeat(1:nelx, 1,nely)'
    xElement = t[:];
    V = [xElement yElement];


    v = V - repeat([xRobot[1] yRobot[1]], size(V)[1] ,1);
    distance= sqrt.(sum(v.^2, dims=2)');

    for i = 2 : size(xRobot)[1]
        v = V - repeat([xRobot[i] yRobot[i]], size(V)[1] ,1);
        distance=[distance; sqrt.(sum(v.^2, dims=2)')];
    end

    # eControl = cell(length(PT), 1);
    eControl =Array{Any}(undef, length(PT));
    flag = zeros(nely*nelx, length(PT));
    for i = 1 : length(PT)
        t = distance[i, :];
        CI=findall(Bool.(t .< rRobot))
        yt=[ ii[1] for ii in CI ];
        temp = unique(yt);
        eControl[i] = temp;
        flag[temp, i] .= 1;
    end

    ## Lower and Upper bounds for elements
    tUpper = zeros(nely*nelx);
    tLower = zeros(nely*nelx);
    for i = 1 : size(flag, 1)
        CI=findall(Bool.(flag[i, :] .== 1))
        yy=[ i[1] for i in CI ];

        tt = PT[yy];

        tm = minimum(tt); 
        tM = maximum(tt);
        tLower[i] = tm;
        if Bool(Upper_And_Lower_Print)
            tUpper[i] = tM+PT[3]-PT[1];
        else
            tUpper[i] = tM+PT[2]-PT[1];
        end
    end

    ## CONNECTIVITY MATRIX / LAPLACE MATRIX
    lrmin = 2;
    iH = ones(convert(Int,nelx*nely*(2*(ceil(lrmin)-1)+1)^2),1)
    jH = ones(Int,size(iH))
    sH = zeros(size(iH))
    k = 0;
    for i1 = 1:nelx
        for j1 = 1:nely
            e1 = (i1-1)*nely+j1
            for i2 = max(i1-(ceil(lrmin)-1),1):min(i1+(ceil(lrmin)-1),nelx)
                for j2 = max(j1-(ceil(lrmin)-1),1):min(j1+(ceil(lrmin)-1),nely)
                    e2 = (i2-1)*nely+j2
                    if e1 == e2
                        continue;
                    end
                    k = k+1
                    iH[k] = e1
                    jH[k] = e2
                    sH[k] = 1
                end
            end
        end
    end
    L = sparse(vec(iH),vec(jH),vec(sH))
    M = sparse(repeat(sum(L, dims=2), 1, size(L)[2]));
    # E = eye(size(L)); 
    E = sparse(1.0I, size(L)[1], size(L)[2]);
    L = E - L./M;
    # L = sparse(L)

    ## MATERIAL PROPERTIES
    Emax = 1;
    Emin = 1e-9;
    nu = 0.3;

    ## INITIALIZE SVERAL PARAMETERS
    penal = 3;      # stiffness penalty
    rmin = 2;     # density filter radius

    ## PREPARE FINITE ELEMENT ANALYSIS
    ## Stiffness matrix for element
    KE=lk();

    ## PREPARE FILTER DENSITY
    H ,Hs=make_filter(nelx,nely,rmin);

    ## INITIALIZE ITERATION
    beta = 1;       # beta continuation
    eta = 0.5;      # projection threshold, fixed at 0.5
    x = fill(volfrac,nely,nelx);
    xTilde = x;
    xPhys = (tanh.(beta*eta) .+ tanh.(beta*(xTilde .-eta))) / (tanh.(beta*eta)  .+ tanh.(beta*(1 .-eta)));

    ## Initial time field
    s = repeat(1 : nely, 1, nelx);
    yElement = s[:].-0.5;
    t = repeat(1:nelx, 1, nely)';
    xElement = t[:].-0.5;
    V = [xElement yElement];

    vect = V - repeat(V[1, :],  1,size(V)[1])';
    dis2 = sum(vect.*vect, dims=2);
    tPhys = sqrt.(dis2) / maximum(sqrt.(dis2));
    tPhys = reshape(tPhys, nely, nelx);
    t = tPhys;

    ## Freedom of degree and loads
    U,F,freedofs=problem(nelx,nely)

    ##
    xold1 = reshape(x,nely*nelx,1);
    xold2 = reshape(x,nely*nelx,1);
    xold1 = [xold1; tLower];
    xold2 = [xold2; tLower];
    low = 0;
    upp = 0;
    rou = 10;

    for loop= 1:nloop
        
        if mod(loop, 30) == 0 && rou < 100
            println("rou change.")
            rou = rou + 10;
        end
        
        if mod(loop, 20) == 0 && beta < 40
            println("beta change.")
            beta = beta + 2;
        end
        
        ## OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        dv = ones(nely,nelx);
        dx = beta * (1 .-tanh.(beta*(xTilde .-eta)).*tanh.(beta*(xTilde .-eta))) / (tanh.(beta*eta) + tanh.(beta*(1-eta)));
        dv[:] = H*(dv[:].*dx[:]./Hs);
        dc = zeros(nely, nelx);
        dt = zeros(nely, nelx);
        
        ## Objective function
        c, dcx = Cal_c_ce_whole(nelx, nely, KE, xPhys, Emin, Emax, penal, freedofs, F);
        obj = c;
        dc[:] = dc[:] + H*(dcx[:].*dx[:]./Hs);
        
        
        tP = range(0, stop=1, length=nStage + 1)
        for i = 1 : nStage - 1
            fp = [ fPoint[i + 1]];
            force = [-0.5];
        
            if Bool(Upper_And_Lower_Print)
                fp = [fPoint[i+1, 1]; fPoint[i+1, 2]];
                force = [ -0.5;  -0.5];
            end
        
            ti = tP[i+1];
            c, dcx, dct = Cal_c_ce_for_weightOfRobot(nelx, nely, KE, xPhys, tPhys,Emin, Emax, penal, ti, fp, force, rou, freedofs);
            obj = obj + Theta*c;
            dc[:] = dc[:] + Theta*H*(dcx[:].*dx[:]./Hs);
            dt[:] = dt[:] + Theta*H*(dct[:]./Hs);
        end
        
        df0 = [dc dt];
        
        ## UPDATE OF DESIGN VARIABLES AND PHYSICAL DENSITIES
        df0dx = df0[:];
        n=length(df0dx);
        move = 0.01;
        tmove = 0.01;
        xmin=max.(0.0, x[:].-move);
        xmax=min.(1, x[:].+move);
        tmin = max.(tLower, t[:].-tmove);
        tmax = min.(tUpper, t[:].+tmove);
        xmin = [xmin; tmin];
        xmax = [xmax; tmax];
        xval = [x[:]; t[:]];
        f0val = obj;
        
        ## Global volume constraint
        fval = sum(sum(xPhys)) / (nelx*nely*volfrac) - 1;
        dfdx= [dv[:]'/(nelx*nely*volfrac) zeros(1, nely*nelx)];
        
        ## Continuity constraint
        LL = L;
        kk = 2*(nely*nelx); # controlling the smoothness of the time field
        A = LL*tPhys[:];
        B = A.^2/(nely*nelx);
        fval = [fval; kk*(sum(B) .-1.0e-6)];
        dft = kk*2*LL'*A;
        dft = H*(dft./Hs)/(nely*nelx);
        dfdx = [dfdx; zeros(1, nely*nelx)  dft'];
        
        ## Starting point
        fval = [fval; tPhys[1] - 1.0e-9];
        dfdx = [dfdx; zeros(1, nely*nelx) (H*(Matrix(1.0I,1, nely*nelx)'./Hs))'];
        
        ## Volumen constraints per layer
        percent = 1/nStage;
        tP = range(0, stop=1, length=nStage+1);
        for i = 1 : nStage
            ##
            ti = tP[i+1];
            ft = 1 .- (tanh.(rou*ti) .+ tanh.(rou*(tPhys .- ti)))/(tanh.(rou*ti) .+ tanh.(rou*(1 .-ti)));
            dfdt = -(rou*(tanh.(rou*(tPhys .- ti)).^2 .- 1))/(tanh.(rou*(ti .- 1)) - tanh.(rou*ti));
            xtJoint = xPhys.*ft;
            fval = [fval; sum(xtJoint[:])/(nelx*nely*volfrac) - i*percent];
            dfx = ft/(nelx*nely*volfrac);
            dfx = H*(dfx[:].*dx[:]./Hs);
            dft = xPhys.*dfdt/(nelx*nely*volfrac);
            dft = H*(dft[:]./Hs);
            dfdx = [dfdx; dfx[:]' dft[:]'];
            
            #
            fval = [fval; -sum(xtJoint[:])/(nelx*nely*volfrac) .+ i*percent .- 1.0e-5];
            dfdx = [dfdx; -dfx[:]' 0.0.-dft[:]'];
        end
        
        ## Solving with MMA
        m=length(fval);
        mdof = 1:m;
        a0 = 1;
        a = zeros(m);
        c_ = ones(m)*1000;
        d = zeros(m);
        
        xmma, ymma, zmma, lam, xsi, eta_, mu, zet, s, low, upp =  mmasub(m, n, loop, xval, xmin, xmax, xold1, xold2,f0val, df0dx, fval[mdof], dfdx[mdof,:],low, upp, a0, a, c_, d);
        
        xnew = reshape(xmma, nely, Int(size(xmma)[1]/nely))
        xold2 = xold1;
        xold1 = xval;
        s = xnew[:, 1:nelx];
        xTilde[:] = (H*s[:])./Hs;
        xPhys = (tanh.(beta*eta) .+ tanh.(beta*(xTilde .-eta))) / (tanh.(beta*eta) .+ tanh.(beta*(1 .-eta)));
        x = s;
        
        ##
        t = xnew[:, nelx+1 : end];
        tPhys = t;
        tPhys[:] =  (H*t[:])./Hs;
        
        ## Pring results
        fval[2] = fval[2]/kk;
        println(" It:$loop Obj:$(round.(obj,digits=2)) Vol:$(round.(sum(xPhys)/(nelx*nely),digits=2)) Cons:$(round.(fval,digits=2))")

        
        ## Draw
        if mod(loop, 10) == 0 || loop==1
            # mc = cgrad(:thermal, nStage, categorical = true)
            mc = cgrad(:thermal, nStage)
            (Plots.heatmap(( 0.0.-xPhys),xaxis=nothing,showaxis = false,clims=(-1.0, 0.0),yaxis=nothing,legend=nothing,fc=:grays,aspect_ratio=:equal))#,clims=(0.0, 1.0)

            # ss = range(0, stop=size(mc)[1], length=nStage+1);
            ss = range(0, stop=nStage, length=nStage+1);

            if Bool(Upper_Lower_Print)
                rx = zeros(size(fPoint)[1], 1);
                rx[2:2:end] .= fPoint[2:2:end]./(nely.+1) .- 0.5;
                rx[1:2:end] .= (fPoint[1:2:end] .- 1)./(nely.+1) .+ 0.5; 
                ry = yRobot; 
                ry[1:2:end].=0 .+ 0.5
                ry[2:2:end].=nely .+ 0.5
            elseif Bool(Upper_And_Lower_Print)
                rx = zeros(size(fPoint)[1], 2);
                rx[:, 1] .= (fPoint[:, 1] .- 1) ./(nely .+1) .+ 0.5;
                rx[:, 2] .= fPoint[:, 2] ./(nely+1) .- 0.5;
                rx = rx'; rx = rx[:];
                ry = yRobot; 
                ry[1:2:end].=0 .+ 0.5
                ry[2:2:end].=nely .+ 0.5
            else
                rx = (fPoint .- 1)/(nely+1) .+ 0.5;
                ry=0 .+ 0.5
            end

            for kk = 1:nStage
                if Bool(Upper_And_Lower_Print)
                    scatter!([rx[2*kk-1]], [ry[2*kk-1]],c=mc[kk])
                    scatter!([rx[2*kk]], [ry[2*kk]],c=mc[kk])
                elseif Bool(Upper_Lower_Print)
                    scatter!([rx[kk]], [ry[kk]],c=mc[kk])
                end
            end

            display(scatter!([nelx+0.5], [nely+0.5],c=mc[nStage]))
            # # title('Density Field');



            heatmap(tPhys,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,clims=(0.0, 1.0),aspect_ratio=:equal)#,clims=(0.0, 1.0)
            (contourf!(tPhys, levels = minimum(tPhys):(maximum(tPhys)-minimum(tPhys))/nStage:maximum(tPhys),w=0.1, linecolor=:black))
            # ss =  range(0, stop=size(mc)[1], length=nStage+1);
            ss = range(0, stop=nStage, length=nStage+1);

            for kk = 1:nStage
                if Bool(Upper_And_Lower_Print)
                    scatter!([rx[2*kk-1]], [ry[2*kk-1]],c=mc[kk])
                    scatter!([rx[2*kk]], [ry[2*kk]],c=mc[kk])
                elseif Bool(Upper_Lower_Print)
                    scatter!([rx[kk]], [ry[kk]],c=mc[kk])
                end
            end
            #

            display(scatter!([nelx+0.5], [nely+0.5],c=mc[nStage]))


            # heatmap(tPhys,alpha=xPhys',xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,clims=(0.0, 1.0),aspect_ratio=:equal)#,clims=(0.0, 1.0)
            # (contourf!(tPhys, levels = minimum(tPhys):(maximum(tPhys)-minimum(tPhys))/nStage:maximum(tPhys),w=0.1, linecolor=:black))
            TT=copy(tPhys)
            TT[Bool.((xPhys).<0.8)].=1.0       
            (heatmap(TT,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,clims=(0.0, 1.0),aspect_ratio=:equal,fc=:thermal))#,clims=(0.0, 1.0)

            # (heatmap(TT,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,clims=(0.0, 1.0),aspect_ratio=:equal,fc=:hot))#,clims=(0.0, 1.0)
            # (contourf!(TT, levels = minimum(tPhys):(maximum(tPhys)-minimum(tPhys))/nStage:maximum(tPhys),w=0.1, linecolor=:black))
            # ss =  range(0, stop=size(mc)[1], length=nStage+1);
            ss = range(0, stop=nStage, length=nStage+1);

            for kk = 1:nStage
                if Bool(Upper_And_Lower_Print)
                    scatter!([rx[2*kk-1]], [ry[2*kk-1]],c=mc[kk])
                    scatter!([rx[2*kk]], [ry[2*kk]],c=mc[kk])
                elseif Bool(Upper_Lower_Print)
                    scatter!([rx[kk]], [ry[kk]],c=mc[kk])
                end
            end
            #

            display(scatter!([nelx+0.5], [nely+0.5],c=mc[nStage]))
        end
    end
    return xPhys, tPhys

end
## Subfunctions
## Calculation of the compliance of the entire structure
function Cal_c_ce_whole(nelx, nely, KE, xPhys, Emin, Emax, penal, freedofs, F)

    nodenrs = reshape(1:(1+nelx)*(1+nely),1+nely,1+nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,nelx*nely,1)
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*nely.+[2 3 0 1] -2 -1],nelx*nely,1)
    iK = convert(Array{Int},reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1))
    sK = reshape(KE[:]*(Emin.+xPhys[:]'.^penal*(Emax-Emin)),64*nelx*nely,1)

    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2

    ##
    U = zeros(2*(nely+1)*(nelx+1), 1);
    U[freedofs] = K[freedofs, freedofs]\Array(F[freedofs]);
    ce = zeros(nely, nelx);
    ce[1 : nely, 1 : nelx] = reshape(sum((U[edofMat]*KE).*U[edofMat],dims=2), nely, nelx);
    c = sum(sum((Emin.+xPhys.^penal*(Emax-Emin)).*ce))
    dcx = -penal*(Emax-Emin)*xPhys.^(penal-1).*ce;
    return c,dcx
end

## Calculation of the compliance of each intermediate structure
function Cal_c_ce_for_gravity(nelx, nely, KE, xPhys, tPhys, Emin, Emax, penal, ti, C, lamda, freedofs)
    ## Projection of time field
    ft = 1 .- (tanh.(lamda .*ti) .+ tanh.(lamda*(tPhys .- ti)))/(tanh.(lamda*ti) .+ tanh.(lamda*(1 .-ti)));
    dfdt = -(lamda*(tanh.(lamda*(tPhys .- ti)).^2 .- 1))/(tanh.(lamda*(ti .- 1)) .- tanh.(lamda*ti));
    xtJoint = xPhys.*ft;

    ##
    nodenrs = reshape(1:(1+nelx)*(1+nely),1+nely,1+nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,nelx*nely,1)
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*nely.+[2 3 0 1] -2 -1],nelx*nely,1)
    iK = convert(Array{Int},reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1))
    sK = reshape(KE[:]*(Emin .+xtJoint[:]'.^penal*(Emax-Emin)),64*nelx*nely,1);

    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2
    f = -C*xtJoint[:];
    F = zeros((nely+1)*(nelx+1), 2);
    F[:, 2] = f;
    F = F';
    F = F[:];

    ##
    U = zeros(2*(nely+1)*(nelx+1), 1);
    U[freedofs] = K[freedofs, freedofs]\Array(F[freedofs]);

    ##
    ce = zeros(nely, nelx);
    ce[1 : nely, 1 : nelx] = reshape(sum((U[edofMat]*KE).*U[edofMat],dims=2), nely, nelx);
    c = sum(sum((Emin.+xtJoint.^penal*(Emax-Emin)).*ce));
    dcx1 = -penal*(Emax-Emin)*xtJoint.^(penal-1).*ce.*ft;
    dct1 = -penal*(Emax-Emin)*xtJoint.^(penal-1).*ce.*xPhys.*dfdt;
    dcx2 = -(U[2:2:end]'*C)'.*ft[:];
    dct2 = -(U[2:2:end]'*C)'.*xPhys[:].*dfdt[:];
    dcx = 2*dcx2 + dcx1[:];
    dct = 2*dct2 + dct1[:];
    return c, dcx, dct

end

##
function Cal_c_ce_for_weightOfRobot(nelx, nely, KE, xPhys, tPhys,Emin, Emax, penal, ti, fpoint, force, lamda, freedofs)

    ft = 1 .- (tanh.(lamda .*ti) .+ tanh.(lamda*(tPhys .- ti)))/(tanh.(lamda*ti) .+ tanh.(lamda*(1 .-ti)));
    dfdt = -(lamda*(tanh.(lamda*(tPhys .- ti)).^2 .- 1))/(tanh.(lamda*(ti .- 1)) .- tanh.(lamda*ti));
    xtJoint = xPhys.*ft;

    ##
    nodenrs = reshape(1:(1+nelx)*(1+nely),1+nely,1+nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,nelx*nely,1)
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*nely.+[2 3 0 1] -2 -1],nelx*nely,1)
    iK = convert(Array{Int},reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1))
    sK = reshape(KE[:]*(Emin .+xtJoint[:]'.^penal*(Emax-Emin)),64*nelx*nely,1);

    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2

    F = zeros((nely+1)*(nelx+1), 2);
    F = F'; F = F[:];
    F[2*fpoint] = F[2*fpoint]+force;

    ##
    U = zeros(2*(nely+1)*(nelx+1), 1);
    U[freedofs] = K[freedofs, freedofs]\Array(F[freedofs]);

    ce = zeros(nely, nelx); 
    ce[1 : nely, 1 : nelx] = reshape(sum((U[edofMat]*KE).*U[edofMat],dims=2), nely, nelx);
    c = sum(sum((Emin.+xtJoint.^penal*(Emax-Emin)).*ce));
    dcx = -penal*(Emax-Emin)*xtJoint.^(penal-1).*ce.*ft;
    dct = -penal*(Emax-Emin)*xtJoint.^(penal-1).*ce.*xPhys.*dfdt;
    return c, dcx, dct

end


# Publication
# Weiming Wang, Dirk Munro, Charlie C.L. Wang, Fred van keulen, Jun Wu,
# Space-Time Topology Optimization for Additive Manufacturing: 
# Concurrent Optimization of Structural Layout and Fabrication Sequence. 
# Structural and Multidisciplinary Optimization, 61:1-18 (2020).




