#======================================================================================================================#
# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021
#======================================================================================================================#

using Flux3D, Makie
# Makie.AbstractPlotting.set_theme!(show_axis = true, scale=false)
# AbstractPlotting.inline!(true)

import Makie
using LinearAlgebra

using Meshing
using FileIO # MeshIO should also be installed
using GeometryBasics

function addFabricationConstraints(ss,Emin,Micro_x,Micro_nelx,Micro_nely,Micro_nelz)

    ss2=Int(ss/2)
    ss4=1

    Micro_x[[1:ss4;Micro_nely-ss4+1:Micro_nely],:,:].=Emin
    Micro_x[:,[1:ss4;Micro_nelx-ss4+1:Micro_nelx],:].=Emin
    Micro_x[:,:,[1:ss4;Micro_nelz-ss4+1:Micro_nelz]].=Emin

    Micro_x[1:ss,1:ss,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2)].=1.0
    Micro_x[Micro_nelz:-1:end-(ss-1),1:ss,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2)].=1.0
    Micro_x[1:ss,Micro_nelx-ss+1:Micro_nelx,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2)].=1.0
    Micro_x[Micro_nelx-ss+1:Micro_nelx,Micro_nelx-ss+1:Micro_nelx,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2)].=1.0

    Micro_x[1:ss,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),1:ss].=1.0
    Micro_x[1:ss,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),Micro_nelx-ss+1:Micro_nelx].=1.0
    Micro_x[Micro_nelx-ss+1:Micro_nelx,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),1:ss].=1.0
    Micro_x[Micro_nelx-ss+1:Micro_nelx,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),Micro_nelx-ss+1:Micro_nelx].=1.0

    Micro_x[(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),1:ss,1:ss].=1.0
    Micro_x[(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),1:ss,Micro_nelx-ss+1:Micro_nelx].=1.0
    Micro_x[(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),Micro_nelx-ss+1:Micro_nelx,1:ss].=1.0
    Micro_x[(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),Micro_nelx-ss+1:Micro_nelx,Micro_nelx-ss+1:Micro_nelx].=1.0


    # Micro_x[(Int(Micro_nelx/2)-ss4:Int(Micro_nelx/2)+ss4-1).+ss4,(Int(Micro_nelx/2)-ss4:Int(Micro_nelx/2)+ss4-1).+ss4,(Int(Micro_nelx/2)-ss4:Int(Micro_nelx/2)+ss4-1).+ss4] .= Emin
    sss=Int(floor(Micro_nelx/6));
    Micro_x[sss:Micro_nelx-sss,sss:Micro_nelx-sss,sss:Micro_nelx-sss] .= Emin

    return Micro_x
end

function addFabricationConstraints(ss,Emin,Micro_x,Micro_nelx,Micro_nely,Micro_nelz,discrete=false)

    ss2=Int(ss/2)
    ss4=1

    ###
    Micro_x[1:ss4,:,:].=0.0;
    Micro_x[Micro_nely-ss4+1,:,:].=0.0;

    Micro_x[:,1:ss4,:].=0.0;
    Micro_x[:,Micro_nelx-ss4+1,:].=0.0;


    Micro_x[:,:,1:ss4].=0.0;
    Micro_x[:,:,Micro_nelz-ss4+1].=0.0;

    ###############
    if discrete
        inds=findall(x-> x>=0, Micro_x)
        for ind in inds
            #     display(ind)
            X=ind[1];Y=ind[2];Z=ind[3]*2;
            Z1=Micro_nely*2-ind[3]*2;
            
            if min((Micro_nely+1)-Z,min(Z-(0),min((Micro_nely+1)-Y-((Z-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,min(Y-(0)-((Z-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,min(X-(0)-((Z-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,(Micro_nely+1)-X-((Z-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2))))) ==0
                Micro_x[ind]=Emin
            elseif min((Micro_nely+1)-Z1,min(Z1-(0),min((Micro_nely+1)-Y-((Z1-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,min(Y-(0)-((Z1-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,min(X-(0)-((Z1-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,(Micro_nely+1)-X-((Z1-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2))))) ==0
                if(ind[3]<Micro_nely)
                    Micro_x[X,Y,ind[3]+1]=Emin
                end
            else
            end
            
            X=ind[3];Y=ind[2];Z=ind[1]*2;
            Z1=Micro_nely*2-ind[1]*2;
        
            if min((Micro_nely+1)-Z,min(Z-(0),min((Micro_nely+1)-Y-((Z-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,min(Y-(0)-((Z-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,min(X-(0)-((Z-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,(Micro_nely+1)-X-((Z-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2))))) ==0
                Micro_x[ind]=Emin
            elseif min((Micro_nely+1)-Z1,min(Z1-(0),min((Micro_nely+1)-Y-((Z1-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,min(Y-(0)-((Z1-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,min(X-(0)-((Z1-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2,(Micro_nely+1)-X-((Z1-(0))/((Micro_nely+1)-(0)))*((Micro_nely+1)-(0))/2))))) ==0
                if(ind[1]<Micro_nely)
                    Micro_x[ind[1]+1,ind[2],ind[3]]=Emin
                end
            else
            end
            
        end
        Micro_x[Int(Micro_nely/2):Int(Micro_nely/2+1),Int(Micro_nely/2):Int(Micro_nely/2+1),Int(Micro_nely/2):Int(Micro_nely/2+1)].=Emin
    end
    #####################

    Micro_x[1:ss,1:ss,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2)].=1.0
    Micro_x[Micro_nelz:-1:end-(ss-1),1:ss,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2)].=1.0
    Micro_x[1:ss,Micro_nelx-ss+1:Micro_nelx,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2)].=1.0
    Micro_x[Micro_nelx-ss+1:Micro_nelx,Micro_nelx-ss+1:Micro_nelx,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2)].=1.0

    Micro_x[1:ss,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),1:ss].=1.0
    Micro_x[1:ss,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),Micro_nelx-ss+1:Micro_nelx].=1.0
    Micro_x[Micro_nelx-ss+1:Micro_nelx,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),1:ss].=1.0
    Micro_x[Micro_nelx-ss+1:Micro_nelx,(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),Micro_nelx-ss+1:Micro_nelx].=1.0

    Micro_x[(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),1:ss,1:ss].=1.0
    Micro_x[(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),1:ss,Micro_nelx-ss+1:Micro_nelx].=1.0
    Micro_x[(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),Micro_nelx-ss+1:Micro_nelx,1:ss].=1.0
    Micro_x[(Int(Micro_nelx/2)-ss2+1:Int(Micro_nelx/2)+ss2),Micro_nelx-ss+1:Micro_nelx,Micro_nelx-ss+1:Micro_nelx].=1.0
    

    return Micro_x
end

function addFabConstraint2D(x,ss)
    nelx=size(x)[1]
    nely=size(x)[2]
    ss2=Int(ss/2)
    ss4=2


    # sss=Int(nelx/4)
    #sides empty
    # x[1:sss,1:sss].=0.0;
    # x[1:sss,nelx-sss:nelx].=0.0;
    # x[nely-sss:nely,1:sss].=0.0;
    # x[nely-sss:nely,nelx-sss:nelx].=0.0;


    ###
    x[1:ss4,1:Int(nelx/2)-ss2].=0.0;
    x[1:ss4,Int(nelx/2)+ss2:nelx].=0.0;

    x[1:Int(nely/2)-ss2,1:ss4].=0.0;
    x[Int(nely/2)+ss2:nely,1:ss4].=0.0;

    x[nely-ss4+1:nely,1:Int(nelx/2)-ss2].=0.0;
    x[nely-ss4+1:nely,Int(nelx/2)+ss2:nelx].=0.0;

    x[1:Int(nely/2)-ss2,nelx-ss4+1:nelx].=0.0;
    x[Int(nely/2)+ss2:nely,nelx-ss4+1:nelx].=0.0;

    #connection points
    x[1:ss,Int(nelx/2)-ss2:Int(nelx/2)+ss2].=1.0;
    x[Int(nely/2)-ss2:Int(nely/2)+ss2,1:ss].=1.0;

    x[nely-ss+1:nely, Int(nelx/2)-ss2:Int(nelx/2)+ss2].=1.0;
    x[Int(nely/2)-ss2:Int(nely/2)+ss2,nelx-ss+1:nelx].=1.0;

    return x
end

#######################################################
function exportFacesToObj(Micro_xPhys,scale,Micro_rmin)
    # Micro_xPhys=Micro_xPhys4
    # scale=5
    # Micro_rmin1=5;

    Micro_nelx=size(Micro_xPhys[2])[2]
    Micro_nely=size(Micro_xPhys[2])[1]

    # Micro_H1,Micro_H1s = filtering2d(Micro_nelx, Micro_nely, Micro_nelx*Micro_nely, Micro_rmin1);


    # Micro_rmin=6;
    Micro_H,Micro_Hs = filtering2d(Micro_nelx*scale, Micro_nely*scale, Micro_nelx*Micro_nely*scale^2, Micro_rmin);
        
    for i=1:length(Micro_xPhys)
        testFace=copy(Micro_xPhys[i]);
        # testFace=reshape(Micro_H1*(testFace[:]./Micro_H1s),Micro_nely,Micro_nelx);

        testFaceBigger=zeros(Micro_nely*scale,Micro_nelx*scale)
        for i=1:Micro_nely
            for j=1:Micro_nelx
                testFaceBigger[(i-1)*scale+1:(i-1)*scale+scale,(j-1)*scale+1:(j-1)*scale+scale].=testFace[i,j]
            end
        end
        testFaceBiggerF=reshape(Micro_H*(testFaceBigger[:]./Micro_Hs),Micro_nely*scale,Micro_nelx*scale);
        # display(Plots.heatmap(testFace,aspect_ratio=:equal))
        # display(Plots.heatmap(testFaceBigger,aspect_ratio=:equal))
        # display(Plots.heatmap(testFaceBiggerF,aspect_ratio=:equal))
        testFaceBiggerFinal=copy(testFaceBiggerF)
        testFaceBiggerFinal[testFaceBiggerF.<0.5].=0
        testFaceBiggerFinal[testFaceBiggerF.>=0.5].=1.0
        display(Plots.heatmap(testFaceBiggerFinal,aspect_ratio=:equal,fc=:greys))
        voxx=zeros(2+size(testFaceBiggerFinal)[1],2+size(testFaceBiggerFinal)[2],4)
        Micro_xPhysTemp=copy(testFaceBiggerFinal)
        Micro_xPhysTemp[testFaceBiggerFinal.<0.7].=0.0
        Micro_xPhysTemp[testFaceBiggerFinal.>=0.7].=1.0
        voxx[2:end-1,2:end-1,2].=Micro_xPhysTemp
        voxx[2:end-1,2:end-1,3].=Micro_xPhysTemp

        points,faces = isosurface(voxx, MarchingCubes(iso=1))
        pointss=reshape(collect(Iterators.flatten(points)),3,length(points))
        facess=reshape(collect(Iterators.flatten(faces)),3,length(faces))
        mm=TriMesh([pointss], [facess])
        display(visualize(mm))
        save_trimesh("$i.obj",mm)
    end


end

#######################################################

function connectivityConstraint(x,L,H,Hs,fval,dfdx)
    nelx=size(x)[1]
    nely=size(x)[2]
    # tPhys=xPhys;
    xPhys=copy(x)
    xPhys[x.>=0.7].=1.0;
    xPhys[x.<0.7].=0.0;

    # display(Plots.heatmap(xPhys))


    seg = fast_scanning(xPhys, 0.5);
    tPhys=Float32.(labels_map(seg));
    ii=1
    for i=1:length(segment_labels(seg))
        if segment_mean(seg, segment_labels(seg)[i]) ==0
            tPhys[tPhys.==segment_labels(seg)[i]].=0
        else
            tPhys[tPhys.==segment_labels(seg)[i]].=ii
            ii+=1
        end
    end
    # display(Plots.heatmap(tPhys))
    
    # seg = fast_scanning(x, 0.5);
    # tPhys=labels_map(seg);
    # tPhys=tPhys./length(segment_labels(seg))
    # println(segment_labels(seg))
    # print("seg: $(length(segment_labels(seg))),")
    # display(Plots.heatmap(tPhys)) # returns the assigned labels map
    # segment_labels(seg) # returns a list of all assigned labels
    # segment_mean(seg, 1) # returns the mean intensity of label 1
    # segment_pixel_count(seg, 1) # returns the pixel count of label 1
    
    ## Continuity constraint
    Nei = 1:nely;
    LL = L;
    kk = 2*(nely*nelx); # controlling the smoothness of the time field
    # kk = sum(x); # controlling the smoothness of the time field
    A = LL*tPhys[:];
    # A =A .-minimum(A);
    B = A.^2/(nely*nelx);
    fval = [fval;-(kk*(sum(B)-1.0e-6))];
    # print_out = fval[end]/kk;
    dft = kk*2*LL'*A;
    dft = H*(dft./Hs)/(nely*nelx);
    dfdx=[dfdx;-dft']
    

    # fval = [fval; (tPhys[Nei]'[:] .- 1.0e-9)];
    # ss = zeros(length(Nei), nely*nelx);
    # for ii = 1 : length(Nei)
    #     ss[ii, Nei[ii]] = 1;
    # end
    # dfdx = [dfdx; (H*(ss'./repeat(Hs, 1, length(Nei))))'];
        

    return fval,dfdx
end

function connectivityMatrix(nelx,nely)
    lrmin = 2;
    iH = ones(convert(Int,nelx*nely*(2*(ceil(lrmin)-1)+1)^2),1)
    jH = ones(Int,size(iH))
    sH = zeros(size(iH))
    k = 0;
    for i1 = 1:nelx
        for j1 = 1:nely
            e1 = (i1-1)*nely+j1
            for i2 = max(i1-(ceil(lrmin)-1),1):min(i1+(ceil(lrmin)-1),nelx)
                for j2 = max(j1-(ceil(lrmin)-1),1):min(j1+(ceil(lrmin)-1),nely)
                    e2 = (i2-1)*nely+j2
                    if e1 == e2
                        continue;
                    end
                    k = k+1
                    iH[k] = e1
                    jH[k] = e2
                    sH[k] = 1
                end
            end
        end
    end
    L = sparse(vec(iH),vec(jH),vec(sH));
    M =  sparse(repeat(sum(L, dims=2), 1, size(L)[2]));
    E = sparse(1.0I, size(L)[1], size(L)[2]);
    L = E - L./M;
    return L
end

#######################################################