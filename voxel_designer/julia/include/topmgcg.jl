# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021

using LinearAlgebra

#### 2D TOPOLOGY OPTIMIZATION CODE, MGCG ANALYSIS ####
# Example: top2dmgcg(360,240,0.2,3.0,2.5,2,4,1e-10,100)
function top2dmgcg(nelx,nely,volfrac,penal,rmin,ft,nl,cgtol,cgmax)
    anim=Animation()
    ## MATERIAL PROPERTIES
    E0 = 1; Emin = 1e-9;
    nu = 0.3;
    ## PREPARE FINITE ELEMENT ANALYSIS
    KE =Ke2D(nu);
    # Prepare fine grid
    nelem = nelx*nely;
    nx = nelx+1; ny = nely+1;
    ndof = 2*nx*ny;
    # nodenrs[1:ny,1:nx] = reshape(1:ny*nx,ny,nx);
    # edofVec[1:nelem,1] = reshape(2*nodenrs(1:ny-1,1:nx-1)+1,nelem,1);

    nodenrs = reshape(1:ny*nx,ny,nx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,nelx*nely,1)
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*nely.+[2 3 0 1] -2 -1],nelx*nely,1)
    iK = convert(Array{Int},reshape(kron(edofMat,ones(8,1))',64*nelx*nely,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,8))',64*nelx*nely,1))

    # Prologation operators
    # Pu = cell(nl-1,1); 
    Pu=Array{Any}(undef, nl-1);

    for l = 1:nl-1  
        Pu[l,1] = Prepcoarse(nely/2^(l-1),nelx/2^(l-1)); 
    end
    # # Define loads and supports (cantilever)
    F = sparse([Int.(2*(nelx*(nely+1)+nely/2+1))],[1],[-1],2*(nely+1)*(nelx+1),1);
    U = zeros(Int.(2*(nely+1)*(nelx+1)),1);
    fixeddofs = 1:2*(nely+1);

    # DEFINE LOADS AND SUPPORTS (HALF MBB-BEAM)
    # F = sparse([2],[1],[-1.0],2*(nely+1)*(nelx+1),1)
    # U = zeros(2*(nely+1)*(nelx+1),1)
    # fixeddofs = union(1:2:2*(nely+1),2*(nelx+1)*(nely+1))

    # Null space elimination of supports
    N = ones(ndof); 
    N[fixeddofs] .= 0; 
    # Null = spdiags(N,0,ndof,ndof);
    # Null1 = sparse(diagm(N));
    Null = spdiagm(ndof,ndof,0 => N);
    # display(sum(Null1-Null))
    H ,Hs=make_filter(nelx,nely,rmin);
    x = fill(volfrac,nely,nelx);
    xPhys = x;
    loop = 0;
    change = 1;
    ## START ITERATION
    while change > 1e-3 && loop < 100
        loop = loop+1;
        ## FE-ANALYSIS
        K = Array{Any}(undef, nl);
        sK = reshape(KE[:]*(Emin.+xPhys[:]'.^penal*(E0-Emin)),64*nelx*nely,1)


        K[1,1] = sparse(vec(iK),vec(jK),vec(sK));
        K[1,1] = Null'*K[1,1]*Null - (Null .-sparse(1.0I, ndof, ndof)); 

        for l = 1:nl-1
            K[l+1,1] = Pu[l,1]'*(K[l,1]*Pu[l,1]);
        end
        # Lfac = cholesky(Hermitian(K[nl,1])).L; Ufac = Lfac';
        Lfac = factorize(Matrix(Hermitian(K[nl,1]))).L ;Ufac = Lfac';
        cgiters,cgres,U = MGCG(K,F,U,Lfac,Ufac,Pu,nl,1,cgtol,cgmax);

        ## OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        ce = reshape(sum((U[edofMat]*KE).*U[edofMat],dims=2),nely,nelx);
        c = sum(sum((Emin .+xPhys.^penal*(E0-Emin)).*ce));
        dc = -penal*(E0-Emin)*xPhys.^(penal-1).*ce;
        dv = ones(nely,nelx);

        ## FILTERING/MODIFICATION OF SENSITIVITIES
        if ft == 1
            dc[:] = H*(x[:].*dc[:])./Hs./max.(1e-3,x[:]);
        elseif ft == 2
            dc[:] = H*(dc[:]./Hs);
            dv[:] = H*(dv[:]./Hs);
        end
        ## OPTIMALITY CRITERIA UPDATE OF DESIGN VARIABLES AND PHYSICAL DENSITIES
        g = mean(xPhys[:]) - volfrac;
        l1 = 0; l2 = 1e9; move = 0.2;xnew=0;
        while (l2-l1)/(l1+l2) > 1e-6
            lmid = 0.5*(l2+l1);
            xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.((0.0.-dc)./dv./lmid)))))
            gt = g + sum((dv[:].*(xnew[:]-x[:])));
            if gt > 0
                l1 = lmid; 
            else 
                l2 = lmid; 
            end
        end
        change = maximum(abs.(xnew[:].-x[:]));
        x = xnew;
        ## FILTERING OF DESIGN VARIABLES
        if ft == 1
            xPhys = xnew;
        elseif ft == 2
            xPhys[:] = (H*xnew[:])./Hs;
        end
        ## PRINT RESULTS
        println(" It: $loop Comp.: $(round.(c,digits=2)) Vol.: $(round.((mean(xPhys[:])),digits=2)) Chg.: $(round.(change,digits=2)) CGres: $(round.(cgres,digits=2)) CGits: $(round.(cgiters,digits=2)) Penal: $penal");
        ## PLOT DENSITIES
        if loop<20||mod(loop,10)==0
            xxx=1 .- clamp.(xPhys,0,1)
            display(Plots.heatmap(xxx,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
            Plots.heatmap(xxx,xaxis=nothing,showaxis = false,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal)
            frame(anim)
        end
        
    end
    return xPhys,anim;
end
# To reproduce example #2 from the article, run:
# top3dmgcg(48,24,24,0.12,3.0,sqrt[3],1,4,1e-10,100)
function top3dmgcg(nelx,nely,nelz,volfrac,penal,rmin,ft,nl,cgtol,cgmax)
    ## MATERIAL PROPERTIES
    E0 = 1;
    Emin = 1e-9;
    nu = 0.3;
    ## PREPARE FINITE ELEMENT ANALYSIS
    KE = Ke3D(nu);
    # Prepare fine grid
    nelem = nelx*nely*nelz;
    nx = nelx+1; ny = nely+1; nz = nelz+1;
    ndof = 3*nx*ny*nz;

    nodenrs = reshape(1:ny*nz*nx,ny,nz,nx)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,nelem,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*nely.+[3 4 5 0 1 2] -3 -2 -1 3*(nely+1)*(nelz+1).+[0 1 2 3*nely.+[3 4 5 0 1 2] -3 -2 -1]],nelx*nely*nelz,1)
    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*nelem,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*nelem,1))
    
    # Prologation operators
    # Pu = cell(nl-1,1);
    Pu=Array{Any}(undef, nl-1);

    for l = 1:nl-1
        Pu[l,1] = prepcoarse3(nelz/2^(l-1),nely/2^(l-1),nelx/2^(l-1));
    end
    # Define loads and supports (cantilever)
    F = sparse(Int.(3 .*nodenrs[1:nely+1,1,nelx+1][:]),fill(1,length(nodenrs[1:nely+1,1,nelx+1])),-sin.((0:nely)./nely.*pi),Int(ndof),1); # Sine load, bottom right
    U = zeros(ndof,1);
    fixeddofs = 1:3*(nely+1)*(nelz+1);
    freedofs = setdiff(1:ndof,fixeddofs);
    # Null space elimination of supports
    N = ones(ndof); N[fixeddofs] .= 0; 
    # Null = spdiags(N,0,ndof,ndof);
    # Null = sparse(diagm(N));
    Null = spdiagm(ndof,ndof,0 => N);
    # ## PREPARE FILTER

    # H,Hs = make_filter3D(nelx, nely, nelz, rmin);

    iH = ones(Int(nelx*nely*nelz*(2*(ceil(rmin)-1)+1)^3));
    jH = ones(size(iH));
    sH = zeros(size(iH));
    k = 0;
    for i1 = 1:nelx
        for k1 = 1:nelz
            for j1 = 1:nely
                e1 = (i1-1)*nely*nelz + (k1-1)*nely + j1;
                for i2 = max(i1-(ceil(rmin)-1),1):min(i1+(ceil(rmin)-1),nelx)
                    for k2 = max(k1-(ceil(rmin)-1),1):min(k1+(ceil(rmin)-1),nelz)
                        for j2 = max(j1-(ceil(rmin)-1),1):min(j1+(ceil(rmin)-1),nely)
                            e2 = (i2-1)*nely*nelz + (k2-1)*nely + j2;
                            k = k + 1;
                            iH[k] = e1;
                            jH[k] = e2;
                            sH[k] = max(0,rmin-sqrt((i1-i2)^2+(j1-j2)^2+(k1-k2)^2));
                        end
                    end
                end
            end
        end
    end
    H = sparse(vec(iH),vec(jH),vec(sH));
    Hs = sum(H,dims=2);
    ## INITIALIZE ITERATION
    x = volfrac.*ones(nely,nelz,nelx);
    xPhys = x;
    loop = 0;
    change = 1;
    ## START ITERATION
    while change > 1e-2 && loop < 50
        loop = loop+1;
        
        #tic
        #sK = reshape(KE(:)*(Emin+xPhys(:)'.^penal*(E0-Emin)),24*24*nelem,1);
        #K = sparse(iK,jK,sK); K = (K+K')/2;
        #U(freedofs,:) = K(freedofs,freedofs)\F(freedofs,:);
        #toc
        
        ## FE-ANALYSIS
        # K = cell(nl,1);
        K = Array{Any}(undef, nl);
        sK = reshape(KE[:]*(Emin.+xPhys[:]'.^penal*(E0-Emin)),576*nelem,1)

        K[1,1] = sparse(vec(iK),vec(jK),vec(sK));
        K[1,1] = Null'*K[1,1]*Null - (Null .-sparse(1.0I, ndof, ndof)); 

        for l = 1:nl-1
            K[l+1,1] = Pu[l,1]'*(K[l,1]*Pu[l,1]);
        end
        
        # Lfac = cholesky(Hermitian(K[nl,1])).L; Ufac = Lfac';
        Lfac = factorize(Matrix(Hermitian(K[nl,1]))).L ;Ufac = Lfac';
        cgiters,cgres,U = MGCG(K,F,U,Lfac,Ufac,Pu,nl,1,cgtol,cgmax);

        ## OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        ce = reshape(sum((U[edofMat]*KE).*U[edofMat],dims=2),nely,nelz,nelx);
        c = sum(sum((Emin .+xPhys.^penal*(E0-Emin)).*ce));
        dc = -penal*(E0-Emin)*xPhys.^(penal-1).*ce;
        dv = ones(nely,nelz,nelx);
        ## FILTERING/MODIFICATION OF SENSITIVITIES
        if ft == 1
            dc[:] = H*(x[:].*dc[:])./Hs./max.(1e-3,x[:]);
        elseif ft == 2
            dc[:] = H*(dc[:]./Hs);
            dv[:] = H*(dv[:]./Hs);
        end
        ## OPTIMALITY CRITERIA UPDATE OF DESIGN VARIABLES AND PHYSICAL DENSITIES
        g = mean(xPhys[:]) - volfrac;
        l1 = 0; l2 = 1e9; move = 0.2;xnew=0;
        while (l2-l1)/(l1+l2) > 1e-6
            lmid = 0.5*(l2+l1);
            xnew = max.(0,max.(x.-move,min.(1,min.(x.+move,x.*sqrt.((0.0.-dc)./dv./lmid)))))
            gt = g + sum((dv[:].*(xnew[:]-x[:])));
            if gt > 0
                l1 = lmid; 
            else 
                l2 = lmid; 
            end
        end
        change = maximum(abs.(xnew[:].-x[:]));
        x = xnew;
        ## FILTERING OF DESIGN VARIABLES
        if ft == 1
            xPhys = xnew;
        elseif ft == 2
            xPhys[:] = (H*xnew[:])./Hs;
        end
        if loop%5==0
            scene= volume(xPhys, algorithm = :iso, isorange = 0.3, isovalue = 1.0,colormap=:grays)
            display(scene)
        end
        ## PRINT RESULTS
        println(" It: $loop Comp.: $(round.(c,digits=2)) Vol.: $(round.((mean(xPhys[:])),digits=2)) Chg.: $(round.(change,digits=2)) CGres: $(round.(cgres,digits=2)) CGits: $(round.(cgiters,digits=2)) Penal: $penal");
    end
    return xPhys
end
## FUNCTION mgcg - MULTIGRID PRECONDITIONED CONJUGATE GRADIENTS
function MGCG(A,b,u,Lfac,Ufac,Pu,nl,nswp,tol,maxiter)
    r = b - A[1,1]*u;
    res0 = norm(b);
    # Jacobi smoother
    omega = 0.8;
    invD = Array{Any}(undef, nl-1);
    for l = 1:nl-1
        invD[l,1]= 1.0 ./diag(A[l,1],0);
    end
    
    #z = VCycle(A,r,Lfac,Ufac,Pu,1,nl,invD,omega,nswp);
    #p = z;
    #rho = r'*z;
    rho_p= 0;relres=0;p=0;
    ii=1
    
    for i = 1:maxiter
        if ii ==1 || relres >= tol
            # display("MGCG $i")
            z = VCycle(A,r,Lfac,Ufac,Pu,1,nl,invD,omega,nswp);
            rho = r'*z;
            if i == 1
                p = z;
            else
                beta = rho/rho_p;
                p = beta.*p + z;
            end
            q = A[1,1]*p;
            dpr = p'*q;
            alpha = rho/dpr;
            u = u .+ alpha.*p;
            r = r .- alpha.*q;
            rho_p = rho;
            relres = norm(r)/res0;
            ii=ii+1
        end
        #if relres < tol || i>=maxiter
            #break
        #end
        
    end
    return ii,relres,u
end
## FUNCTION VCycle - COARSE GRID CORRECTION
function VCycle(A,r,Lfac,Ufac,Pu,l,nl,invD,omega,nswp)
    # display("VCycle $l")
    z = 0*r;
    z = smthdmpjac(z,A[l,1],r,invD[l,1],omega,nswp);
    Az = A[l,1]*z;
    d = r - Az;
    dh2 = Pu[l,1]'*d;
    if (nl == l+1)
        vh2 = Ufac \ (Lfac \ dh2);
    else
        vh2 = VCycle(A,dh2,Lfac,Ufac,Pu,l+1,nl,invD,omega,nswp);
    end
    v = Pu[l,1]*vh2;
    z = z + v;
    z = smthdmpjac(z,A[l,1],r,invD[l,1],omega,nswp);
    return z
end
## FUNCTIODN smthdmpjac - DAMPED JACOBI SMOOTHER
function smthdmpjac(u,A,b,invD,omega,nswp)
    for i = 1:nswp
        u = u - omega*invD.*(A*u) + omega*invD.*b;
    end
    return u
end
## FUNCTION prepcoarse - PREPARE MG PROLONGATION OPERATOR
function Prepcoarse(ney,nex)
    # Assemble state variable prolongation
    maxnum = Int(nex*ney*20);
    iP = zeros(Int32,maxnum); jP = zeros(Int32,maxnum); sP = zeros(maxnum);
    nexc = nex/2; neyc = ney/2;
    # weights for fixed distances to neighbors on a structured grid 
    vals = [1;0.5;0.25];
    cc = 0;
    for nx = 1:nexc+1
        for ny = 1:neyc+1
            col = Int((nx-1)*(neyc+1)+ny);
            # Coordinate on fine grid
            nx1 = nx*2 - 1; ny1 = ny*2 - 1;
            # Loop over fine nodes within the rectangular domain
            for k = max(nx1-1,1):min(nx1+1,nex+1)
                for l = max(ny1-1,1):min(ny1+1,ney+1)
                    row = Int((k-1)*(ney+1)+l);
                    # Based on squared dist assign weights: 1.0 0.5 0.25
                    ind = Int(1+((nx1-k)^2+(ny1-l)^2));
                    cc = Int(cc+1); iP[cc] = Int(2*row-1); jP[cc] = 2*col-1; sP[cc] = vals[ind];
                    cc = Int(cc+1); iP[cc] = Int(2*row); jP[cc] = 2*col; sP[cc] = vals[ind];
                end
            end
        end
    end
    # Assemble matrices
    Pu = sparse(Int.(iP[1:cc]),jP[1:cc],sP[1:cc]);
    return Pu
end
## FUNCTION prepcoarse - PREPARE MG PROLONGATION OPERATOR
function prepcoarse3(nex,ney,nez)
    # Assemble state variable prolongation
    maxnum = Int(nex*ney*nez*20);
    iP = zeros(Int32,maxnum); jP = zeros(Int32,maxnum); sP = zeros(maxnum);
    nexc = Int(nex/2);
    neyc = Int(ney/2);
    nezc = Int(nez/2);
    # Weights for fixed distances to neighbors on a structured grid 
    vals = [1;0.5;0.25;0.125];
    cc = 0;
    for nx = 1:nexc+1
        for ny = 1:neyc+1
            for nz = 1:nezc+1
                col = Int((nx-1)*(neyc+1)+ny+(nz-1)*(neyc+1)*(nexc+1)); 
                # Coordinate on fine grid
                nx1 = nx*2 - 1; ny1 = ny*2 - 1; nz1 = nz*2 - 1;
                # Loop over fine nodes within the rectangular domain
                for k = max(nx1-1,1):min(nx1+1,nex+1)
                    for l = max(ny1-1,1):min(ny1+1,ney+1)
                        for h = max(nz1-1,1):min(nz1+1,nez+1)
                            row = Int((k-1)*(ney+1)+l+(h-1)*(nex+1)*(ney+1)); 
                            # Based on squared dist assign weights: 1.0 0.5 0.25 0.125
                            ind = Int(1+((nx1-k)^2+(ny1-l)^2+(nz1-h)^2));
                            cc=cc+1; iP[cc]=Int(3*row-2); jP[cc]=3*col-2; sP[cc]=vals[ind];
                            cc=cc+1; iP[cc]=Int(3*row-1); jP[cc]=3*col-1; sP[cc]=vals[ind];
                            cc=cc+1; iP[cc]=Int(3*row); jP[cc]=3*col; sP[cc]=vals[ind];
                        end
                    end
                end
            end
        end
    end
    # Assemble matrices
    # Pu = sparse(iP(1:cc),jP(1:cc),sP(1:cc));
    Pu = sparse(Int.(iP[1:cc]),jP[1:cc],sP[1:cc]);

    return Pu
end

############################################################################
# This code was written by Niels Aage and Boyan S. Lazarov,                #
# Department of Mechanical Engineering, Technical University of Denmark,   #
# and Oded Amir, Faculty of Civil & Environmental Engineering,             #  
# Technion - Israel Institute of Technology.                               #
#                                                                          #
# Contact: naage@mek.dtu.dk, bsl@mek.dtu.dk, odedamir@cv.technion.ac.il    #
#                                                                          #
# Details are discussed in the paper                                       #
# "On multigrid-CG for efficient topology optimization" accepted October   #
# 2013 to Structural and Multidisciplinary Optimization.                   #
#                                                                          #
# Disclaimer:                                                              #
# The authors reserve all rights but do not guarantee that the code is     #
# free from errors. Furthermore, the authors shall not be liable in any    #
# event caused by the use of the program.                                  #
############################################################################






