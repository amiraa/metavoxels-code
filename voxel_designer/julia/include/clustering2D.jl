#======================================================================================================================#
# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021
#======================================================================================================================#

# divide macrostructure into θ parts

using Clustering;
using StatsPlots;
using ParallelKMeans;
using Distances;
using Krylov, IterativeSolvers;
using GMT; #for elbow curve

##############################################################
#####2D
##############################################################

function freeMaterial2D(θ,Macro_nely,Macro_nelx,Macro_nele,Macro_Vol,iK,jK,Emin,U,F,freedofs,edofMat,maxloop)
    maxloop = 200; E0 = 1;
    
    Ke = lk();
    Macro_x = ones(Macro_nely,Macro_nelx).*Macro_Vol;
    Macro_xTilde = Macro_x; 
    Macro_xPhys = Macro_x; 

    loopbeta = 0; loop = 0; Macro_change = 1;
    ## free material
    penal1=1;
    jet_me=ColorGradient([:blue,:cyan,:yellow,:red,:black])
    while loop < maxloop && Macro_change > 0.01 
        loop = loop+1;


        # FE-ANALYSIS AT TWO SCALES
        sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal1*(1 .-Emin)),64*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
        U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx);
        c = sum(sum((Emin .+Macro_xPhys.^penal1*(1 .-Emin)).*ce));
        Macro_dc = -penal1*(1 .-Emin)*Macro_xPhys.^(penal1-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx);


        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys, Macro_change = OC_reg(Macro_x, Macro_dc, Macro_dv, Macro_Vol, Macro_nele, 0.2);

        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx); 

        # PRINT RESULTS
        # println(" FMD It.:$loop Obj.:$(round.(c,digits=2)) Macro_Vol.:$(round.(mean(Macro_xPhys[:] ),digits=2)) Macro_ch.:$(round.(Macro_change,digits=2))")

    end

    display(Plots.heatmap(Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=jet_me,clims=(0.0, 1.0),aspect_ratio=:equal))

    Macro_masks=[]
    Macro_masks_dens=[]

    Macro_xPhys_diag=copy(Macro_xPhys)
    d=1/θ
    for i in 1:θ
        Macro_xPhys_temp=copy(Macro_xPhys)
        out= (Macro_xPhys.> (i*d-d)) .& (Macro_xPhys.<= (i*d))
        println("Mask $i from $(round((i*d-d),digits=2)) to $(round(i*d,digits=2)) occupies $(round(sum(out)/length(Macro_xPhys)*100,digits=2))% of the macrostructure.")
        Macro_xPhys_temp[out].=false
        Macro_xPhys_temp[.!out].=true
        dens=(i*d-d/2)+Emin
        if i==1
            dens=Emin
        elseif i==θ
            dens=1-Emin
        end
        Macro_xPhys_diag[out].= dens
        append!(Macro_masks_dens,[dens])
        append!(Macro_masks,[Macro_xPhys_temp])
        # display(Plots.heatmap(Macro_xPhys_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
    end

    # display(Macro_masks_dens)
    display(Plots.heatmap(Macro_xPhys_diag,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, 1.0),fc=jet_me,aspect_ratio=:equal))

    return Macro_masks,Macro_masks_dens,Macro_xPhys_diag
end

##############################################################
function Uclustering2D(θ,Macro_nelx,Macro_nely,prob,Macro_Vol,Macro_rmin,Micro_Vol,edofMat)

    
    penal=3.0;Emin=1e-9;Ke = lk();Macro_nele=Macro_nelx*Macro_nely;
    Macro_xPhys,anim=topologyOptimizationHeavyside(Macro_nelx,Macro_nely,prob,Macro_Vol,Macro_rmin,penal,1000,false)

    
    # jet_me=ColorGradient([:blue,:cyan,:yellow,:red,:black])
    jet_me=cgrad([:blue,:cyan,:yellow,:red,:black])
    # t=:dark
    t=:darktest
    # theme(t);
    cschemeList=[]
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    # cscheme=ColorGradient(cschemeList)
    cscheme=cgrad(cschemeList)
    
    
    

    display(Plots.heatmap(Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=jet_me,clims=(0.0, 1.0),aspect_ratio=:equal))
    
    # FE-ANALYSIS AT TWO SCALES
    U,F,freedofs=prob(Macro_nelx,Macro_nely);
    iK = convert(Array{Int},reshape(kron(edofMat,ones(8,1))',64*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,8))',64*Macro_nele,1))
    
    sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
    U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
    
    Macro_masks,Macro_xPhys_diag,Micro_centers,Macro_xPhys_NC= UDynamicClustering2D(θ,Macro_xPhys,U[edofMat],1,true)

    return Macro_masks,Macro_xPhys_diag,Micro_centers,Macro_xPhys_NC
   
end

function UDynamicClustering2D(θ,Macro_xPhys,U_edofMat,Micro_centers,verbose=false)

    Macro_nely,Macro_nelx=size(Macro_xPhys)

    t=:darktest
    cschemeList=[]
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    # cscheme=ColorGradient(cschemeList)
    cscheme=cgrad(cschemeList)
    

    ########################################################################
    #visualize deformation/strain
    # display(Plots.heatmap(1.0.-Macro_xPhys[end:-1:1,:],showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))
    diss=reshape(U_edofMat,Macro_nely, Macro_nelx,8);
    
    order=[1,2,3,4,5,6,7,8];
    # gr(size=(50*4,50*3))
    # for j=1:8
    #     display(Plots.heatmap(diss[:,:,j],showaxis = false,xaxis=nothing,yaxis=nothing,legend = false,aspect_ratio=:equal))
    # end
    # gr(size=(400,300))

    

    
    ny=size(diss)[1];nx=size(diss)[2];
    Θ=zeros(ny ,nx);ΘD=zeros(ny ,nx);
    von_mises=zeros(ny ,nx);
    ratio=zeros(ny ,nx);

    for i=1:ny
        for j=1:nx
            #based on: https://community.wvu.edu/~bpbettig/MAE456/Exam_Final_practice_answers.pdf
            Ue = diss[i,j,:];
            Θ[i,j] , von_mises[i,j], ratio[i,j],α1,α2 =getPrincipalStrains(Ue);
        end
    end
    ΘD=Θ.*180/π;

    ratioPlot=copy(ratio)
    maxRatio=maximum(ratioPlot[Macro_xPhys.>=0.5])
    minRatio=minimum(ratioPlot[Macro_xPhys.>=0.5])
    ratioPlot[Macro_xPhys.<0.5].=NaN
    
    display(Plots.heatmap(ratioPlot, aspect_ratio=:equal,fc=:jet,clim=(minRatio,maxRatio)))#,clim=(0,2e-5))
    von_mises[Macro_xPhys.<0.5].=NaN
    Plots.heatmap(von_mises, aspect_ratio=:equal,fc=:jet)#,clim=(0,2e-5))
    xs = 1:Int(nx)
    ys = 1:Int(ny)

    ress(y,x)=(cos(Θ[Int(x),Int(y)])/3, sin(Θ[Int(x),Int(y)])/3)
    
    # col(y,x)=norm(ress(y,x))
    xxs = [x for x in xs for y in ys]
    yys = [y for x in xs for y in ys]

    display(Plots.quiver!(xxs, yys, quiver=ress))

    if verbose #plot displacemnets (to fix)
        dissVec=zeros(ny+1 ,nx+1 ,2)
        for i=1:ny
            for j=1:nx
                dissVec[i,j,1]=diss[i,j,7]
                dissVec[i,j,2]=diss[i,j,8]
            end
        end
        for i=1:ny
            dissVec[i,end,1]=diss[i,end,order[5]]
            dissVec[i,end,2]=diss[i,end,order[6]]
        end
        for j=1:nx
            dissVec[end,j,1]=diss[end,j,order[1]]
            dissVec[end,j,2]=diss[end,j,order[2]]
        end
        dissVec[end,end,1]=diss[end,end,order[3]]
        dissVec[end,end,2]=diss[end,end,order[4]]


        UX=dissVec[:,:,1];UY=dissVec[:,:,2]
        unit=10
        order=[1,2,3,4,5,6,7,8]
        plotStrain(nx,ny,unit,diss,order,UX,UY)
    end


    ########################################################################


    # X=U[edofMat]'
    # X=hcat(Macro_xPhys[:],U[edofMat])'
    # X=hcat(Macro_xPhys[:],Θ[:])'
    wd=2;
    X=hcat(sin.(2*Θ[:]).*Macro_xPhys[:],cos.(2*Θ[:]).*Macro_xPhys[:],wd.*Macro_xPhys[:])'
    # Random.seed!(34568)
    # if length(Micro_centers)==1 #IF FIRST TIME
    #     iseeds = initseeds(KmppAlg(), X, θ)
    #     # display(iseeds)
    #     Micro_centers=X[:,iseeds]
    # end
    # R = Clustering.kmeans!(X, Micro_centers)
    # # R = Clustering.kmeans(X, θ;init=Micro_centers,maxiter=1000)
    # a = Clustering.assignments(R) # get the assignments of points to clusters
    # c = Clustering.counts(R) # get the cluster sizes
    # M = R.centers; # get the cluster centers
    # Micro_centers=M
    # # display(Micro_centers)
    # D=Distances.pairwise(Euclidean(), M, dims=2)
    # hc = hclust(D, linkage=:single)
    # Uclustered=reshape(a,Macro_nely,Macro_nelx);
    # display(Plots.heatmap(Uclustered.-1,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cscheme,aspect_ratio=:equal))

    #new hiearchical clustering
    
    data=X
    D=Distances.pairwise(Euclidean(), data, dims=2)
    hc = hclust(D, linkage=:ward_presquared) # hc = hclust(D, linkage=:single,branchorder=:optimal)
    display(Plots.plot(hc))

    gr(size=(400,100))
    #for elbow curve
    hcElbow=reverse(hc.heights)[1:20]
    display(Plots.plot(1:(length(hcElbow)),hcElbow,label=""))
    hcElbow=reverse(hc.heights)[2:20]
    xy=hcat(1:(length(hcElbow)),hcElbow)
    mapp=mapproject(xy, dist2line=(line=[xy[1:1, :]; xy[end:end, :]], unit=:cartesian))
    display(Plots.plot(mapp[:,3],label=""))
    nclust=(findmax(mapp[:,3])[2]+1)
    display("Best Number of clusters is= $nclust")
    gr(size=(400,300))
    nclust=θ
    a=cutree(hc; k=nclust)
    #visualize tree with only θ clusters
    centers=zeros(size(data)[1],nclust)
    for i in 1:nclust
        R = Clustering.kmeans(data[:,a.==i], 1); # run K-means just to get the center
        M = R.centers; # get the cluster centers
        centers[:,i].=M[:]
    end
    D=Distances.pairwise(Euclidean(), centers, dims=2)
    hc = hclust(D, linkage=:ward_presquared,branchorder=:optimal) # hc = hclust(D, linkage=:single,branchorder=:optimal)
    display(Plots.plot(hc))

    Uclustered=reshape(a,Macro_nely,Macro_nelx);
    # θ=nclust
    
    

    #for smoothness
    A=Uclustered
    indices = CartesianIndices(A)
    neighbors = CartesianIndex(-1, -1):CartesianIndex(1, 1)
    a=copy(A)
    for I in indices
        if I[1]!=1 &&  I[2]!=1 && I[1]!=Macro_nely && I[2]!=Macro_nelx
            count=zeros(θ)
            for N in neighbors
                if I+N in indices
                    
                    count[A[I+N]]+=1
                end
            end
            a[I]=findmax(count)[2]
        end
    end
    Uclustered=a
    display(Plots.heatmap(Uclustered.-1,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=cscheme,aspect_ratio=:equal))

    Macro_masksU=[]
    Macro_masks_densU=[]

    Macro_xPhys_diagU=copy(Macro_xPhys)
    d=1/θ
    for i in 1:θ
        Macro_xPhys_temp=copy(Macro_xPhys)
        out= (Uclustered.==i)
        
        Macro_xPhys_temp[out].=true
        Macro_xPhys_temp[.!out].=false

        # dens=mean(Macro_xPhys[out])

        if verbose
            println("Mask $i occupies $(round(sum(out)/length(Macro_xPhys)*100,digits=2))% of the macrostructure.")
        end
        # Macro_xPhys_diagU[out].= dens
        Macro_xPhys_diagU[out].= i

        append!(Macro_masksU,[Macro_xPhys_temp])
        # display(Plots.heatmap(Macro_xPhys_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
    end
    if verbose
        # display(Macro_masks_dens)
        display(Plots.heatmap(Macro_xPhys_diagU,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(1, θ),fc=cscheme,aspect_ratio=:equal))
        display(Plots.plot(hc))
    end

    
    return Macro_masksU,Macro_xPhys_diagU,Micro_centers,Macro_xPhys

end
############################################################
#2D Clustering for nearest neighbors in strain library
function Uclustering2DStrainLibrary(Macro_struct, Micro_struct,prob,library,verbose=true)

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2];
    Macro_nelx   = Int(Macro_struct[3]); Macro_nely  = Int(Macro_struct[4]);
    Micro_nelx   = Int(Micro_struct[3]); Micro_nely  = Int(Micro_struct[4]);
    Macro_Vol    = Macro_struct[5][2]; 
    Micro_Vol_FM   = Micro_struct[5];
    Macro_Vol_FM    = Macro_struct[5][1];
    Micro_Vol=Micro_Vol_FM;
    Macro_Elex = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely;
    Macro_nele = Int(Macro_nelx*Macro_nely);   Micro_nele = Int(Micro_nelx*Micro_nely);
    Macro_ndof = Int(2*(Macro_nelx+1)*(Macro_nely+1));
    ratioD=library[1];Micro_Vol=library[2];num=library[3];
    jet_me=cgrad([:blue,:cyan,:yellow,:red,:black])
    

    # PREPARE FINITE ELEMENT ANALYSIS
    nodenrs = reshape(1:(Macro_nely+1)*(Macro_nelx+1),1+Macro_nely,1+Macro_nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,Macro_nele,1);
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*Macro_nely.+[2 3 0 1] -2 -1],Macro_nele,1)

    Micro_rmin=(Micro_struct[6])
    Macro_rmin=(Macro_struct[6])


    penal=3.0;Emin=1e-9;
    Ke = lk();

    Macro_xPhys=ones(Macro_nely,Macro_nelx)
    
    if Macro_Vol<1
        Macro_xPhys,anim=topologyOptimizationHeavyside(Macro_nelx,Macro_nely,prob,Macro_Vol,Macro_rmin,penal,1000,false)
    end
    
    display(Plots.heatmap(Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=jet_me,clims=(0.0, 1.0),aspect_ratio=:equal))
    
    # FE-ANALYSIS AT TWO SCALES
    U,F,freedofs=prob(Macro_nelx,Macro_nely);
    iK = convert(Array{Int},reshape(kron(edofMat,ones(8,1))',64*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,8))',64*Macro_nele,1))
    
    sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
    U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
    ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx)
    c = sum(sum((Emin.+Macro_xPhys.^penal*(1-Emin)).*ce))
    display("compliance before $c")
            

    ########################################################################
    #visualize deformation/strain
    # display(Plots.heatmap(1.0.-Macro_xPhys[end:-1:1,:],showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))
    U_edofMat=U[edofMat];
    diss=reshape(U_edofMat,Macro_nely, Macro_nelx,8);
    
    order=[1,2,3,4,5,6,7,8];

    ny=size(diss)[1];nx=size(diss)[2];
    Θ=zeros(ny ,nx);ΘD=zeros(ny ,nx);
    von_mises=zeros(ny ,nx);
    ratio=zeros(ny ,nx);

    for i=1:ny
        for j=1:nx
            #based on: https://community.wvu.edu/~bpbettig/MAE456/Exam_Final_practice_answers.pdf
            Ue = diss[i,j,:];
            Θ[i,j] , von_mises[i,j], ratio[i,j],α1,α2 =getPrincipalStrains(Ue);
        end
    end
    ΘD=Θ.*180/π;

    ratioPlot=copy(ratio)
    maxRatio=maximum(ratioPlot[Macro_xPhys.>=0.5])
    minRatio=minimum(ratioPlot[Macro_xPhys.>=0.5])
    ratioPlot[Macro_xPhys.<0.5].=NaN
    
    display(Plots.heatmap(ratioPlot, aspect_ratio=:equal,fc=:jet,clim=(minRatio,maxRatio)))#,clim=(0,2e-5))
    von_mises[Macro_xPhys.<0.5].=NaN
    Plots.heatmap(von_mises, aspect_ratio=:equal,fc=:jet)#,clim=(0,2e-5))
    xs = 1:Int(nx)
    ys = 1:Int(ny)

    ress(y,x)=(cos(Θ[Int(x),Int(y)])/3,- sin(Θ[Int(x),Int(y)])/3)
    
    # col(y,x)=norm(ress(y,x))
    xxs = [x for x in xs for y in ys]
    yys = [y for x in xs for y in ys]

    display(Plots.quiver!(xxs, yys, quiver=ress))

    if verbose #plot displacemnets (to fix)
        dissVec=zeros(ny+1 ,nx+1 ,2)
        for i=1:ny
            for j=1:nx
                dissVec[i,j,1]=diss[i,j,7]
                dissVec[i,j,2]=diss[i,j,8]
            end
        end
        for i=1:ny
            dissVec[i,end,1]=diss[i,end,order[5]]
            dissVec[i,end,2]=diss[i,end,order[6]]
        end
        for j=1:nx
            dissVec[end,j,1]=diss[end,j,order[1]]
            dissVec[end,j,2]=diss[end,j,order[2]]
        end
        dissVec[end,end,1]=diss[end,end,order[3]]
        dissVec[end,end,2]=diss[end,end,order[4]]


        UX=dissVec[:,:,1];UY=dissVec[:,:,2]
        unit=10
        order=[1,2,3,4,5,6,7,8]
        plotStrain(nx,ny,unit,diss,order,UX,UY)
    end

    

    #################
    E0 = 1; Emin = 1e-9; nu = 0.3;penal=3;

    θ=Int((num-1)*4+1) +1 #cuboct
    # display(θ)
    Micro_xPhys,DHs=loadVisualizeLibrary(library)
    Micro_x=Micro_xPhys
    # t=:dark
    t=:darktest
    # # theme(t);
    # cschemeList=[]
    # for i=1:θ
    #     append!(cschemeList,[palette(t)[i]])
    # end
    # # cscheme=ColorGradient(cschemeList)
    # cscheme=cgrad(cschemeList)
    t=:darktest
    cscheme=cgrad(t, θ-1, categorical = true)
    cschemeList=[]
    # append!(cschemeList,[:white])
    for i=1:θ-1
        append!(cschemeList,[cscheme[i]])
    end
    append!(cschemeList,[:black])
    cscheme=cgrad(cschemeList)


    function findNearestList(theta)
        if theta<0
            theta+=180
        elseif theta>180
            theta-=180
        end
        findnearest(A::AbstractArray,t) = findmin(abs.(A.-t))[2]
        values=[]
        for i in range(0.0, 180, length=θ-1)
            append!(values,[i])
        end
        return values[findnearest(values,theta)]
    end
    function findNearestListIndex(theta)
        if theta<0
            theta+=180
        elseif theta>180
            theta-=180
        end
        findnearest(A::AbstractArray,t) = findmin(abs.(A.-t))[2]
        values=[]
        for i in range(0.0, 180, length=θ-1)
            append!(values,[i])
        end
        return findnearest(values,theta)
    end
    ΘDNearest=findNearestList.(ΘD)
    Uclustered=findNearestListIndex.(ΘD)

    for i=1:ny
        for j=1:nx
            Ue = diss[i,j,:];
            if sum(Ue)==0 || ratio[i,j]>0.99 ||ratio[i,j]<-0.99
                Uclustered[i,j]=θ #if no load cuboct
            end
        end
    end

    Macro_masksU=[]
    Macro_masks_densU=[]

    Macro_xPhys_diagU=copy(Macro_xPhys)
    d=1/θ
    for i in 1:θ
        Macro_xPhys_temp=copy(Macro_xPhys)
        out= (Uclustered.==i)
        
        Macro_xPhys_temp[out].=true
        Macro_xPhys_temp[.!out].=false

        # dens=mean(Macro_xPhys[out])

        # Macro_xPhys_diagU[out].= dens
        Macro_xPhys_diagU[out].= i

        append!(Macro_masksU,[Macro_xPhys_temp])
        # display(Plots.heatmap(Macro_xPhys_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
    end

    Macro_xPhys_diag=copy(Macro_xPhys_diagU);
    Macro_masks=Macro_masksU;
    if verbose
        # display(Macro_masks_dens)
        Macro_xPhys_diagU[Macro_xPhys.<0.75].=NaN
        display(Plots.heatmap(Macro_xPhys_diagU,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(1, θ),fc=cscheme,aspect_ratio=:equal))
    end

    
    # return Macro_masksU,Macro_xPhys_diagU,Micro_centers,Macro_xPhys
    

    thresh=0.5
    thresh1=0.75
    Macro_xPhys_det=zeros(size(Macro_xPhys)[1]*size(Micro_xPhys[1])[1],size(Macro_xPhys)[2]*size(Micro_xPhys[1])[2]);
    Macro_xPhys_det1=zeros(size(Macro_xPhys)[1]*size(Micro_xPhys[1])[1],size(Macro_xPhys)[2]*size(Micro_xPhys[1])[2]);
    sizeMicro=size(Micro_xPhys[1])[1]
    for i in 1:size(Macro_xPhys)[1]
        for j in 1:size(Macro_xPhys)[2]
            ind=Int(Macro_xPhys_diag[i,j])
            micr=copy(Micro_xPhys[ind])
            micr[micr.>thresh1].=1.0;
            micr[micr.<thresh1].=0.0;
            micr.=micr.*(ind.+1)
            indI=Int((i-1)*sizeMicro+1 )
            indJ=Int((j-1)*sizeMicro+1 )
            if Macro_xPhys[i,j]>thresh
                Macro_xPhys_det1[ indI:indI+sizeMicro-1 , indJ:indJ+sizeMicro-1].=micr 
            end
            Macro_xPhys_det[ indI:indI+sizeMicro-1 , indJ:indJ+sizeMicro-1].=micr 
        end
    end
    Macro_xPhys_det[Macro_xPhys_det.<thresh1].=NaN;
    Macro_xPhys_det[Macro_xPhys_det .!= NaN]=Macro_xPhys_det[Macro_xPhys_det .!= NaN].-1;
    Macro_xPhys_det1[Macro_xPhys_det1.<thresh1].=NaN;
    Macro_xPhys_det1[Macro_xPhys_det1 .!= NaN]=Macro_xPhys_det1[Macro_xPhys_det1 .!= NaN].-1;
    display(Plots.heatmap(Macro_xPhys_det,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(1, θ),fc=cscheme,aspect_ratio=:equal))
    display(Plots.heatmap(Macro_xPhys_det1,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(1, θ),fc=cscheme,aspect_ratio=:equal))



    ###########
    # FE-ANALYSIS MACROSCALE
    Kes=zeros(8*8,Macro_nelx*Macro_nely)
    for i=1:θ
        Kes[:,Bool.(Macro_masks[i][:])].=elementMatVec2D(Macro_Elex/2, Macro_Eley/2, DHs[i])[:]
    end

    sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
    U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
    # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
    UU=mapslices(x->[x]'[:], U[edofMat], dims=2)[:]
    KKK=mapslices(x->[reshape(x,8,8)], Kes, dims=1)[:]

    ce=sum(vcat((UU.*KKK)...).*U[edofMat],dims=2)
    ce=reshape(ce,Macro_nely,Macro_nelx)
    c = sum(sum((Emin .+Macro_xPhys.^penal*(1 .-Emin)).*ce));



    display("Final compliance: $c")
    
    ###########

    ############cuboct 0.35
    cuboct=load("./img/library/1/cuboct_vol_$((Micro_Vol)).jld")["data"];
    Kes=zeros(8*8,Macro_nelx*Macro_nely)
    DH, dDH = EBHM2D(cuboct, Micro_length, Micro_width, E0, Emin, nu, penal);
    Kes[:,Bool.(ones(Macro_nelx*Macro_nely))].=elementMatVec2D(Macro_Elex/2, Macro_Eley/2, DH)[:]
    sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
    U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
    # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
    UU=mapslices(x->[x]'[:], U[edofMat], dims=2)[:]
    KKK=mapslices(x->[reshape(x,8,8)], Kes, dims=1)[:]

    ce=sum(vcat((UU.*KKK)...).*U[edofMat],dims=2)
    ce=reshape(ce,Macro_nely,Macro_nelx)
    c = sum(sum((Emin .+Macro_xPhys.^penal*(1 .-Emin)).*ce));
    display("cuboct compliance: $c")

    ###########
    

    return Macro_xPhys,Micro_xPhys,Macro_masks,Macro_xPhys_diag,Micro_Vol,DHs

   
end
##############################################################
function CompliantUclustering2D(θ,Macro_nely,Macro_nelx,Macro_nele,Macro_Vol,Micro_Vol,iK,jK,Emin,U,F,freedofs,edofMat,DofDOut,S)
    maxloop = 100; E0 = 1;

    
    Ke = lk()

    Macro_x = ones(Macro_nely,Macro_nelx).*Macro_Vol;
    Macro_xTilde = Macro_x; 
    Macro_xPhys = Macro_x; 

    loopbeta = 0; loop = 0; Macro_change = 1;
    ## free material
    penal1=1;
    jet_me=ColorGradient([:blue,:cyan,:yellow,:red,:black])

    t=:dark
    # theme(t);
    cschemeList=[]
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=ColorGradient(cschemeList)

    while loop < maxloop && Macro_change > 0.01 
        loop = loop+1;


        # FE-ANALYSIS AT TWO SCALES
        sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal1*(1 .-Emin)),64*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK))+S; K = (K+K')/2;
        U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
        U1=U[:,1]
        U2=U[:,2]
        
        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        # ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx);
        ce=reshape(-sum((U1[edofMat]*Ke).*U2[edofMat],dims=2),Macro_nely,Macro_nelx)
        
        c=U[DofDOut,1]
        # c = sum(sum((Emin .+Macro_xPhys.^penal1*(1 .-Emin)).*ce));
        Macro_dc = -penal1*(1 .-Emin)*Macro_xPhys.^(penal1-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx);


        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys, Macro_change = CompliantOC_reg(Macro_x, Macro_dc, Macro_dv, Macro_Vol, Macro_nele, 0.02);
        # Macro_x, Macro_xPhys, Macro_change = CompliantOC(Macro_x, Macro_dc, Macro_dv, Macro_Vol, Macro_nele, 0.02);

        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx); 

        # PRINT RESULTS
        # println(" FMD It.:$loop Obj.:$(round.(c,digits=2)) Macro_Vol.:$(round.(mean(Macro_xPhys[:] ),digits=2)) Macro_ch.:$(round.(Macro_change,digits=2))")
        # if loop%20==0
        #     display(Plots.heatmap(Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=jet_me,clims=(0.0, 1.0),aspect_ratio=:equal))
        # end

    end

    display(Plots.heatmap(Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=jet_me,clims=(0.0, 1.0),aspect_ratio=:equal))
    
    # FE-ANALYSIS AT TWO SCALES
    sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal1*(1 .-Emin)),64*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
    U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);

    ########################################################################
    #visualize deformation/strain
    # display(Plots.heatmap(1.0.-Macro_xPhys[end:-1:1,:],showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))
    diss=reshape(U[edofMat,2],Macro_nely, Macro_nelx,8);order=[1,2,3,4,5,6,7,8];
    # gr(size=(50*4,50*3))
    # for j=1:8
    #     display(Plots.heatmap(diss[:,:,j],showaxis = false,xaxis=nothing,yaxis=nothing,legend = false,aspect_ratio=:equal))
    # end
    # gr(size=(400,300))

    

    unit=100
    ny=size(diss)[1];nx=size(diss)[2];

    ϵx =zeros(ny ,nx);ϵy =zeros(ny ,nx);
    αx =zeros(ny ,nx);αy =zeros(ny ,nx);
    α1 =zeros(ny ,nx);α2 =zeros(ny ,nx);α3 =zeros(ny ,nx);α4 =zeros(ny ,nx);
    ϵxy =zeros(ny ,nx);τxy=zeros(ny ,nx);
    u=zeros(ny ,nx);v=zeros(ny ,nx);
    Θ=zeros(ny ,nx);ΘD=zeros(ny ,nx);
    von_mises=zeros(ny ,nx);
    l=100;nu=0.3;ν=0.3;E=100;
    a=1;b=1;
    x=0;y=0;#at center
    #shape functions
    N1=(a-x)*(b-y)/(4*a*b)
    N2=(a+x)*(b-y)/(4*a*b)
    N3=(a+x)*(b+y)/(4*a*b)
    N4=(a-x)*(b+y)/(4*a*b)
    B = (1/2/l)*[-1 0 1 0 1 0 -1 0; 0 -1 0 -1 0 1 0 1; -1 -1 -1 1 1 1 1 -1];
    C = E/(1-ν^2)*[1 ν 0; ν 1 0; 0 0 (1-ν)/2];
    C = E/((1+ν)*(1-2*ν))*[1-ν ν 0; ν 1-ν 0; 0 0 (1-2*ν)/2];

    for i=1:ny
        for j=1:nx
            #based on: https://community.wvu.edu/~bpbettig/MAE456/Exam_Final_practice_answers.pdf
            Ue = diss[i,j,:];

            N=[N1 0 N2 0 N3 0 N4 0; 0 N1 0 N2 0 N3 0 N4];

            #displacement at center
            u[i,j],v[i,j]=N*Ue

            #in plane strain in center
            ϵ=1/(4*a*b)*[-(b-y) 0 (b-y) 0 (b+y) 0 -(b+y)  0;
                0 -(a-x) 0 -(a+x) 0 (a+x) 0 (a-x);
                -(a-x) -(b-y) -(a+x) (b-y) (a+x) (b+y) (a-x) -(b+y)]*Ue
            ϵx[i,j],ϵy[i,j],ϵxy[i,j]=ϵ

            #in plane stress in center
            α=C*ϵ
            αx[i,j],αy[i,j],τxy[i,j]=α

            #principal stress center
            α1[i,j]=(αx[i,j]+αy[i,j])/2+sqrt(((αx[i,j]-αy[i,j])/2)^2+τxy[i,j]^2)
            α2[i,j]=(αx[i,j]+αy[i,j])/2-sqrt(((αx[i,j]-αy[i,j])/2)^2+τxy[i,j]^2)
            α3[i,j]=ν*(αx[i,j]+αy[i,j])
            α4[i,j]=ν*(αx[i,j]+αy[i,j])
            # Θ   =atan((2*τxy),(αx-αy))/2
            # Θ   =atan((2*ϵxy),(ϵx-ϵy))/2
            #von_mises
            von_mises[i,j]=1/(sqrt(2))*sqrt((α1[i,j]-α2[i,j])^2+(α2[i,j]-α3[i,j])^2+(α3[i,j]-α1[i,j])^2)
            Θ[i,j]   =atan((2*ϵxy[i,j]/2),(ϵx[i,j]-ϵy[i,j]))/2
            Θ[i,j]   =atan((2*τxy[i,j]),(αx[i,j]-αy[i,j]))/2
            if α2[i,j]<0 && α1[i,j]>0
                Θ[i,j]   +=π
            end
            if abs(α1[i,j])<abs(α2[i,j])
                Θ[i,j]   -=π/2
            end
        end
    end
    ΘD=Θ.*180/π;
    Plots.heatmap(von_mises, aspect_ratio=:equal)#,clim=(0,2e-5))
    xs = 1:Int(nx)
    ys = 1:Int(ny)
    maxi=maximum(abs.(α1))/2
    # ress(y,x)=(cos(Θ[Int(x),Int(y)])*α1[Int(x),Int(y)]/maxi, sin(Θ[Int(x),Int(y)])*α1[Int(x),Int(y)]/maxi)
    # ress1(y,x)=(cos(Θ[Int(x),Int(y)]-π/2)*α2[Int(x),Int(y)]/maxi, sin(Θ[Int(x),Int(y)]-π/2)*α2[Int(x),Int(y)]/maxi)
    ress(y,x)=(cos(Θ[Int(x),Int(y)])/3, sin(Θ[Int(x),Int(y)])/3)
    ress1(y,x)=(cos(Θ[Int(x),Int(y)]+π/2)/3, sin(Θ[Int(x),Int(y)]+π/2)/3)
    ress2(y,x)=(cos(Θ[Int(x),Int(y)]-π/2)/3, sin(Θ[Int(x),Int(y)]-π/2)/3)
    ress3(y,x)=(cos(Θ[Int(x),Int(y)]+π)/3, sin(Θ[Int(x),Int(y)]+π)/3)
    
    # col(y,x)=norm(ress(y,x))
    xxs = [x for x in xs for y in ys]
    yys = [y for x in xs for y in ys]
    # Plots.quiver!(xxs, yys, quiver=ress)
    # Plots.quiver!(xxs, yys, quiver=ress1)
    # Plots.quiver!(xxs, yys, quiver=ress2)
    # display(Plots.quiver!(xxs, yys, quiver=ress3))
    display(Plots.quiver!(xxs, yys, quiver=ress))

    dissVec=zeros(ny+1 ,nx+1 ,2)
    for i=1:ny
        for j=1:nx
            dissVec[i,j,1]=diss[i,j,1]
            dissVec[i,j,2]=diss[i,j,2]
        end
    end
    for i=1:ny
        dissVec[i,end,1]=diss[i,end,order[3]]
        dissVec[i,end,2]=diss[i,end,order[4]]
    end
    for j=1:nx
        dissVec[end,j,1]=diss[end,j,order[7]]
        dissVec[end,j,2]=diss[end,j,order[8]]
    end
    dissVec[end,end,1]=diss[end,end,order[5]]
    dissVec[end,end,2]=diss[end,end,order[6]]


    UX=dissVec[:,:,1];UY=dissVec[:,:,2]
    plotStrain(nx,ny,unit,diss,order,UX,UY)


    ########################################################################

    
    # X=U[edofMat]'
    X=hcat(Macro_xPhys[:],U[edofMat,1])'
    # X=hcat(X,U[edofMat,2])'
    # cluster X into θ clusters using K-means
    # R = kmeans(X, θ; maxiter=200, display=:iter)
    R = Clustering.kmeans(X, θ; maxiter=200)

    @assert Clustering.nclusters(R) == θ # verify the number of clusters

    a = Clustering.assignments(R) # get the assignments of points to clusters
    c = Clustering.counts(R) # get the cluster sizes
    M = R.centers; # get the cluster centers

    D=Distances.pairwise(Euclidean(), M, dims=2)
    hc = hclust(D, linkage=:single)
    display(Plots.plot(hc))

    Uclustered=reshape(a,Macro_nely,Macro_nelx);
    # display(Plots.heatmap(Uclustered,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(0.0, θ),fc=jet_me,aspect_ratio=:equal))

    # b = [ParallelKMeans.kmeans(X, i; tol=1e-6, max_iters=300, verbose=false).totalcost for i = 2:10]
    # display(Plots.plot(b,label=""))

    Macro_masksU=[]
    Macro_masks_densU=[]

    Macro_xPhys_diagU=copy(Macro_xPhys)
    d=1/θ
    for i in 1:θ
        Macro_xPhys_temp=copy(Macro_xPhys)
        out= (Uclustered.==i)
        

        Macro_xPhys_temp[out].=true
        Macro_xPhys_temp[.!out].=false

        # dens=mean(Macro_xPhys[out])
        dens=Micro_Vol

        println("Mask $i with density $(round(dens,digits=2)) occupies $(round(sum(out)/length(Macro_xPhys)*100,digits=2))% of the macrostructure.")

        # Macro_xPhys_diagU[out].= dens
        Macro_xPhys_diagU[out].= i

        append!(Macro_masks_densU,[dens])
        append!(Macro_masksU,[Macro_xPhys_temp])
        # display(Plots.heatmap(Macro_xPhys_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
    end

    # display(Macro_masks_dens)
    display(Plots.heatmap(Macro_xPhys_diagU,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(1, θ),fc=cscheme,aspect_ratio=:equal))

    return Macro_masksU,Macro_masks_densU,Macro_xPhys_diagU

end

##############################################################
