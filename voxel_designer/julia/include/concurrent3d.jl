#======================================================================================================================#
# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021
#======================================================================================================================#


# using Makie
# AbstractPlotting.inline!(true)

# using Images, ImageSegmentation
# using GLMakie;using Makie

using Flux #for maxpool
# gr(size=(400,300))

# one microstructure
function ConTop3D(Macro_struct, Micro_struct, penal, rmin,saveItr=5,mgcg=[false,false],voxels=true)
    # USER-DEFINED LOOP PARAMETERS
    maxloop = 90; displayflag = 1; E0 = 1; Emin = 1e-9; nu = 0.3;
    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2]; Macro_Height = Macro_struct[3];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = Int(Macro_struct[4]); Macro_nely  = Int(Macro_struct[5]); Macro_nelz   = Int(Macro_struct[6]);
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Macro_Vol = Macro_struct[7]; Micro_Vol = Micro_struct[7];
    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];
    # PREPARE FINITE ELEMENT ANALYSIS
    load_x,load_y,load_z = Matlab.meshgrid([Int(Macro_nelx/2)], [Macro_nely], [Int(Macro_nelz/2)]);
    loadnid = load_z .*(Macro_nelx+1)*(Macro_nely+1) .+ load_x .*(Macro_nely .+1) .+(Macro_nely .+1 .-load_y);
    
    F = sparse(Int.(3 .*loadnid[:] .- 1) ,[1],[-1],Macro_ndof,1);

    fixed_x,fixed_y,fixed_z = Matlab.meshgrid( [0, Macro_nelx],[0],[0, Macro_nelz] );
    fixednid = fixed_z .*(Macro_nelx .+1)*(Macro_nely+1) .+fixed_x.*(Macro_nely+1) .+(Macro_nely+1 .-fixed_y);
    fixeddofs = vcat(3 .*fixednid[:], 3 .*fixednid[:] .-1, 3 .*fixednid[:] .-2);

    
    # # USER-DEFINED LOAD DOFs
    # m= Matlab.meshgrid(Macro_nelx:Macro_nelx, 0:0, 0:Macro_nelz)
    # il=m[1]
    # jl=m[2]
    # kl=m[3]
    # loadnid = kl.*(Macro_nelx+1).*(Macro_nely+1).+il.*(Macro_nely+1).+(Macro_nely+1 .-jl)
    # loaddof = 3 .*loadnid[:] .- 1; 
    # # USER-DEFINED SUPPORT FIXED DOFs
    # m= Matlab.meshgrid(0:0,0:Macro_nely,0:Macro_nelz)# Coordinates
    # iif=m[1]
    # jf=m[2]
    # kf=m[3]
    # fixednid = kf.*(Macro_nelx+1).*(Macro_nely+1) .+iif .*(Macro_nely .+1) .+(Macro_nely .+1 .-jf) # Node IDs
    # fixeddof = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs
    # # DEFINE LOADS AND SUPPORTS (HALF MBB-BEAM)
    # F= sparse(loaddof,fill(1,length(loaddof)),fill(-1,length(loaddof)),Macro_ndof,1);
    
    
    alldofs = 1:Macro_ndof
    U = zeros(Macro_ndof)
    freedofs = setdiff(1:Macro_ndof,fixeddofs)

    # nodegrd = reshape(1:(Macro_nely+1)*(Macro_nelx+1),Macro_nely+1,Macro_nelx+1);
    # nodeids = reshape(nodegrd[1:end-1,1:end-1],Macro_nely*Macro_nelx,1);
    # nodeidz = 0:(Macro_nely+1)*(Macro_nelx+1):(Macro_nelz-1)*(Macro_nely+1)*(Macro_nelx+1);
    # nodeids = repeat(nodeids,length(nodeidz)) .+repeat(nodeidz,length(nodeids));
    
    # edofMat = repeat(3 .*nodeids[:] .+1,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nelx+1)*(Macro_nely+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]], Macro_nele, 1);
    nodenrs = reshape(1:(1+Macro_nelx)*(1+Macro_nely)*(1+Macro_nelz),1+Macro_nely,1+Macro_nelx,1+Macro_nelz)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,Macro_nelx*Macro_nely*Macro_nelz,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nely+1)*(Macro_nelx+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]],Macro_nelx*Macro_nely*Macro_nelz,1)
    
    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*Macro_nele,1))
    
    # PREPARE FILTER
    # Macro_H,Macro_Hs = filtering3d(Macro_nelx, Macro_nely, Macro_nelz, Macro_nele, rmin);
    # Micro_H,Micro_Hs = filtering3d(Micro_nelx, Micro_nely, Micro_nelz, Micro_nele, rmin);
    # Macro_H,Macro_Hs = make_filter3D(Macro_nelx, Macro_nely, Macro_nelz, rmin);
    Micro_H,Micro_Hs = make_filter3D(Micro_nelx, Micro_nely, Micro_nelz, rmin);
    
    # INITIALIZE ITERATION
    Macro_x = ones(Macro_nely,Macro_nelx,Macro_nelz).*Macro_Vol;
    Micro_x = ones(Micro_nely,Micro_nelx,Micro_nelz);
    Micro_x[Int(Micro_nely/2):Int(Micro_nely/2)+1,Int(Micro_nelx/2):Int(Micro_nelx/2)+1,Int(Micro_nelz/2):Int(Micro_nelz/2)+1] .= 0;
    beta = 1;
    Macro_xTilde = Macro_x; Micro_xTilde = Micro_x;
    Macro_xPhys = 1 .-exp.(-beta .*Macro_xTilde) .+Macro_xTilde .*exp.(-beta);
    Micro_xPhys = 1 .-exp.(-beta .*Micro_xTilde) .+Micro_xTilde .*exp.(-beta);
    loopbeta = 0; loop = 0; Macro_change = 1; Micro_change = 1;
    while loop < maxloop && ( Macro_change > 0.01 || Micro_change > 0.01)
        loop = loop+1; loopbeta = loopbeta+1;
        # FE-ANALYSIS AT TWO SCALES
        DH, dDH = EBHM3D(Micro_xPhys, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,mgcg);
        Ke = elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH);
        sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
        U[freedofs] = K[freedofs,freedofs]\Array(F[freedofs]);
        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);
        c = sum(sum(sum((Emin .+Macro_xPhys.^penal*(1-Emin)).*ce)));
        Macro_dc = -penal*(1-Emin)*Macro_xPhys.^(penal-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx, Macro_nelz);
        Micro_dc = zeros(Micro_nely, Micro_nelx, Micro_nelz);
        for i = 1:Micro_nele
            dDHe = [dDH[1,1][i] dDH[1,2][i] dDH[1,3][i] dDH[1,4][i] dDH[1,5][i] dDH[1,6][i];
                    dDH[2,1][i] dDH[2,2][i] dDH[2,3][i] dDH[2,4][i] dDH[2,5][i] dDH[2,6][i];
                    dDH[3,1][i] dDH[3,2][i] dDH[3,3][i] dDH[3,4][i] dDH[3,5][i] dDH[3,6][i];
                    dDH[4,1][i] dDH[4,2][i] dDH[4,3][i] dDH[4,4][i] dDH[4,5][i] dDH[4,6][i];
                    dDH[5,1][i] dDH[5,2][i] dDH[5,3][i] dDH[5,4][i] dDH[5,5][i] dDH[5,6][i];
                    dDH[6,1][i] dDH[6,2][i] dDH[6,3][i] dDH[6,4][i] dDH[6,5][i] dDH[6,6][i]];
            dKE = elementMatVec3D(Macro_Elex, Macro_Eley, Macro_Elez, dDHe);
            dce = reshape(sum((U[edofMat]*dKE).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz );
            Micro_dc[i] = -sum(sum(sum((Emin .+Macro_xPhys.^penal*(1-Emin)).*dce)));
        end
        Micro_dv = ones(Micro_nely, Micro_nelx, Micro_nelz);
        # FILTERING AND MODIFICATION OF SENSITIVITIES
        Macro_dx = beta*exp.(-beta.*Macro_xTilde).+exp(-beta); 
        Micro_dx = beta*exp.(-beta.*Micro_xTilde).+exp.(-beta);
        Macro_dc[:] = Macro_H*(Macro_dc[:].*Macro_dx[:]./Macro_Hs); Macro_dv[:] = Macro_H*(Macro_dv[:].*Macro_dx[:]./Macro_Hs);
        Micro_dc[:] = Micro_H*(Micro_dc[:].*Micro_dx[:]./Micro_Hs); Micro_dv[:] = Micro_H*(Micro_dv[:].*Micro_dx[:]./Micro_Hs);
        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys, Macro_xTilde, Macro_change = OC(Macro_x, Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, 0.02, beta,voxels);
        Micro_x, Micro_xPhys, Micro_xTilde, Micro_change = OC(Micro_x, Micro_dc, Micro_dv, Micro_H, Micro_Hs, Micro_Vol, Micro_nele, 0.02, beta);
        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx, Macro_nelz);
        Micro_xPhys = reshape(Micro_xPhys, Micro_nely, Micro_nelx, Micro_nelz);
        # PRINT RESULTS
        println(" It.:$loop Obj.:$(round.(c,digits=2)) Macro_Vol.:$(round.(mean(Macro_xPhys[:]),digits=2) ) Micro_Vol.:$(round.(mean(Micro_xPhys[:] ),digits=2)) Macro_ch.:$(round.(Macro_change,digits=2)) Micro_ch.:$(round.(Micro_change,digits=2))")
        if mod(loop,saveItr)==0
            scene= GLMakie.volume(Macro_xPhys, algorithm = :iso, isorange = 0.2, isovalue = 0.8,colormap=:grays)
            display(scene)

            # save("./img/Macro_xPhys_$(Macro_Vol)_$(penal)_$(rmin)_$(loop).png",scene)

            scene= GLMakie.volume(Micro_xPhys, algorithm = :iso, isorange = 0.2, isovalue = 0.8,colormap=:grays)
            display(scene)

            # save("./img/Micro_xPhys_$(Micro_Vol)_$(penal)_$(rmin)_$(loop).png",scene)

        end
        # UPDATE HEAVISIDE REGULARIZATION PARAMETER
        if beta < 512 && (loopbeta >= 50 || Macro_change <= 0.01 || Micro_change <= 0.01 )
            beta = 2*beta; loopbeta = 0; Macro_change = 1; Micro_change = 1;
            display("Parameter beta increased to $beta.");
        end
    end
    return Macro_xPhys,Micro_xPhys
end

# multiple microstructures (θ)
function MultiConTop3D(θ,Macro_struct, Micro_struct, penal, rmin,saveItr=5,mgcg=[false,false],voxels=true)
    
    
    
    # USER-DEFINED LOOP PARAMETERS
    maxloop = 90; displayflag = 1; E0 = 1; Emin = 1e-9; nu = 0.3;

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2]; Macro_Height = Macro_struct[3];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = Int(Macro_struct[4]); Macro_nely  = Int(Macro_struct[5]); Macro_nelz   = Int(Macro_struct[6]);
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Macro_Vol = Macro_struct[7][2]; 
    Macro_Vol_FM    = Macro_struct[7][1]
    # Micro_Vol = Micro_struct[7];
    
    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];
    # PREPARE FINITE ELEMENT ANALYSIS
    load_x,load_y,load_z = Matlab.meshgrid([Int(Macro_nelx/2)], [Macro_nely], [Int(Macro_nelz/2)]);
    loadnid = load_z .*(Macro_nelx+1)*(Macro_nely+1) .+ load_x .*(Macro_nely .+1) .+(Macro_nely .+1 .-load_y);
    
    F = sparse(Int.(3 .*loadnid[:] .- 1) ,[1],[-1],Macro_ndof,1);

    fixed_x,fixed_y,fixed_z = Matlab.meshgrid( [0, Macro_nelx],[0],[0, Macro_nelz] );
    fixednid = fixed_z .*(Macro_nelx .+1)*(Macro_nely+1) .+fixed_x.*(Macro_nely+1) .+(Macro_nely+1 .-fixed_y);
    fixeddofs = vcat(3 .*fixednid[:], 3 .*fixednid[:] .-1, 3 .*fixednid[:] .-2);

    if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    end
    # # USER-DEFINED LOAD DOFs
    # m= Matlab.meshgrid(Macro_nelx:Macro_nelx, 0:0, 0:Macro_nelz)
    # il=m[1]
    # jl=m[2]
    # kl=m[3]
    # loadnid = kl.*(Macro_nelx+1).*(Macro_nely+1).+il.*(Macro_nely+1).+(Macro_nely+1 .-jl)
    # loaddof = 3 .*loadnid[:] .- 1; 
    # # USER-DEFINED SUPPORT FIXED DOFs
    # m= Matlab.meshgrid(0:0,0:Macro_nely,0:Macro_nelz)# Coordinates
    # iif=m[1]
    # jf=m[2]
    # kf=m[3]
    # fixednid = kf.*(Macro_nelx+1).*(Macro_nely+1) .+iif .*(Macro_nely .+1) .+(Macro_nely .+1 .-jf) # Node IDs
    # fixeddof = [3 .*fixednid[:]; 3 .*fixednid[:].-1; 3 .*fixednid[:].-2] # DOFs
    # # DEFINE LOADS AND SUPPORTS (HALF MBB-BEAM)
    # F= sparse(loaddof,fill(1,length(loaddof)),fill(-1,length(loaddof)),Macro_ndof,1);
    
    
    alldofs = 1:Macro_ndof
    U = zeros(Macro_ndof)
    freedofs = setdiff(1:Macro_ndof,fixeddofs)

    # nodegrd = reshape(1:(Macro_nely+1)*(Macro_nelx+1),Macro_nely+1,Macro_nelx+1);
    # nodeids = reshape(nodegrd[1:end-1,1:end-1],Macro_nely*Macro_nelx,1);
    # nodeidz = 0:(Macro_nely+1)*(Macro_nelx+1):(Macro_nelz-1)*(Macro_nely+1)*(Macro_nelx+1);
    # nodeids = repeat(nodeids,length(nodeidz)) .+repeat(nodeidz,length(nodeids));
    
    # edofMat = repeat(3 .*nodeids[:] .+1,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nelx+1)*(Macro_nely+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]], Macro_nele, 1);
    nodenrs = reshape(1:(1+Macro_nelx)*(1+Macro_nely)*(1+Macro_nelz),1+Macro_nely,1+Macro_nelx,1+Macro_nelz)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,Macro_nelx*Macro_nely*Macro_nelz,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nely+1)*(Macro_nelx+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]],Macro_nelx*Macro_nely*Macro_nelz,1)
    
    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*Macro_nele,1))
    
    # PREPARE FILTER
    # Macro_H,Macro_Hs = filtering3d(Macro_nelx, Macro_nely, Macro_nelz, Macro_nele, rmin);
    # Micro_H,Micro_Hs = filtering3d(Micro_nelx, Micro_nely, Micro_nelz, Micro_nele, rmin);
    Macro_H,Macro_Hs = make_filter3D(Macro_nelx, Macro_nely, Macro_nelz, rmin);
    Micro_H,Micro_Hs = make_filter3D(Micro_nelx, Micro_nely, Micro_nelz, rmin);
    
    
    ##free Material distribution to see how to divide microstructures
    Macro_masks,Micro_Vol,Macro_xPhys_diag=freeMaterial3D(θ,Macro_nely,Macro_nelx,Macro_nelz,Macro_nele,Macro_Vol_FM,iK,jK,Emin,U,F,freedofs,edofMat)
    
    # INITIALIZE ITERATION
    Macro_x = ones(Macro_nely,Macro_nelx,Macro_nelz).*Macro_Vol;


    # Micro_x = ones(Micro_nely,Micro_nelx,Micro_nelz);
    Micro_x=[]
    for i=1:θ
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx,Micro_nelz)]);
        Micro_x[i][Int(Micro_nely/2):Int(Micro_nely/2)+1,Int(Micro_nelx/2):Int(Micro_nelx/2)+1,Int(Micro_nelz/2):Int(Micro_nelz/2)+1] .= 0;
    end

    beta = 1;
    Macro_xTilde = Macro_x; 
    # Micro_xTilde = Micro_x;
    Macro_xPhys = 1 .-exp.(-beta .*Macro_xTilde) .+Macro_xTilde .*exp.(-beta);
    # Micro_xPhys = 1 .-exp.(-beta .*Micro_xTilde) .+Micro_xTilde .*exp.(-beta);
    
    Micro_xTilde = Micro_x[1];
    Micro_xPhys=[];
    Micro_dc = []
    Micro_dv = []
    Micro_change=[]
    DHs=[]
    dDHs=[]
    for i=1:θ
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx, Micro_nelz)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx, Micro_nelz)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM3D(Micro_xPhys[i], Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,mgcg);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end

    #first one always empty
    Micro_x[1]=fill(Emin,Micro_nely,Micro_nelx,Micro_nelz);
    Micro_xPhys[1]=fill(Emin,Micro_nely,Micro_nelx,Micro_nelz);
    Micro_change[1]=0

    #last one always empty
    Micro_x[end]=fill(1-Emin,Micro_nely,Micro_nelx,Micro_nelz);
    Micro_xPhys[end]=fill(1-Emin,Micro_nely,Micro_nelx,Micro_nelz);
    Micro_change[end]=0



    loopbeta = 0; loop = 0; Macro_change = 1; 
    # Micro_change = 1;
    while loop < maxloop && ( Macro_change > 0.01 || Bool.(sum(Micro_change .>= 0.01)<θ))
        loop = loop+1; loopbeta = loopbeta+1;


        # FE-ANALYSIS AT TWO SCALES
        Kes=zeros(24*24,Macro_nelx*Macro_nely*Macro_nelz)

        for i=2:θ
            DH, dDH = EBHM3D(Micro_xPhys[i], Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,mgcg);
            DHs[i]=DH;
            dDHs[i]=dDH;
            Kes[:,Bool.(Macro_masks[i][:])].=elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DHs[i])[:]
        end
        
        # Ke = elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH);#old
        
        sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
        # sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
        U[freedofs] = K[freedofs,freedofs]\Array(F[freedofs]);

        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        # ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);
        
        #get compliance slow try to make it fast!! ??
        # ce=zeros(Macro_nely*Macro_nelx*Macro_nelz)
        # for i=1:Macro_nely*Macro_nelx*Macro_nelz
        #     ce[i]=sum((U[edofMat]*reshape(Kes[:,i],24,24)).*U[edofMat],dims=2)[i]
        # end

        # UU=[]
        # KKK=[]
        # for i=1:Macro_nely*Macro_nelx*Macro_nelz
        #     append!(UU,[U[edofMat][i,:]'])
        #     append!(KKK,[reshape(Kes[:,i],24,24)])
        # end
        UU=mapslices(x->[x]'[:], U[edofMat], dims=2)[:]
        KKK=mapslices(x->[reshape(x,24,24)], Kes, dims=1)[:]

        ce=sum(vcat((UU.*KKK)...).*U[edofMat],dims=2)
        ce=reshape(ce,Macro_nely,Macro_nelx,Macro_nelz)
        
        
        c = sum(sum(sum((Emin .+Macro_xPhys.^penal*(1-Emin)).*ce)));
        Macro_dc = -penal*(1-Emin)*Macro_xPhys.^(penal-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx, Macro_nelz);

        # Micro_dc = zeros(Micro_nely, Micro_nelx, Micro_nelz);

        for i=2:θ-1
            Micro_dc[i]=zeros(Micro_nely, Micro_nelx, Micro_nelz);
            Micro_dv[i]=ones(Micro_nely, Micro_nelx, Micro_nelz);
        end

        for ii=2:θ-1
            for i = 1:Micro_nele
                dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i] dDHs[ii][1,4][i] dDHs[ii][1,5][i] dDHs[ii][1,6][i];
                        dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i] dDHs[ii][2,4][i] dDHs[ii][2,5][i] dDHs[ii][2,6][i];
                        dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i] dDHs[ii][3,4][i] dDHs[ii][3,5][i] dDHs[ii][3,6][i];
                        dDHs[ii][4,1][i] dDHs[ii][4,2][i] dDHs[ii][4,3][i] dDHs[ii][4,4][i] dDHs[ii][4,5][i] dDHs[ii][4,6][i];
                        dDHs[ii][5,1][i] dDHs[ii][5,2][i] dDHs[ii][5,3][i] dDHs[ii][5,4][i] dDHs[ii][5,5][i] dDHs[ii][5,6][i];
                        dDHs[ii][6,1][i] dDHs[ii][6,2][i] dDHs[ii][6,3][i] dDHs[ii][6,4][i] dDHs[ii][6,5][i] dDHs[ii][6,6][i]];
                dKE = elementMatVec3D(Macro_Elex, Macro_Eley, Macro_Elez, dDHe);
                dce = sum((U[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                Micro_dc[ii][i] = -sum(sum(sum((Emin .+Macro_xPhys[Bool.(Macro_masks[ii][:])][:].^penal*(1-Emin)).*dce)));
            end
        end
        # Micro_dv = ones(Micro_nely, Micro_nelx, Micro_nelz);

        # FILTERING AND MODIFICATION OF SENSITIVITIES
        Macro_dx = beta*exp.(-beta.*Macro_xTilde).+exp(-beta);
        Macro_dc[:] = Macro_H*(Macro_dc[:].*Macro_dx[:]./Macro_Hs); 
        Macro_dv[:] = Macro_H*(Macro_dv[:].*Macro_dx[:]./Macro_Hs);

        for i=2:θ-1
            Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta);
            Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
            Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
        end


        # Micro_dx = beta*exp.(-beta.*Micro_xTilde).+exp.(-beta);
        # Micro_dc[:] = Micro_H*(Micro_dc[:].*Micro_dx[:]./Micro_Hs); 
        # Micro_dv[:] = Micro_H*(Micro_dv[:].*Micro_dx[:]./Micro_Hs);

        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys,Macro_xTilde, Macro_change = OC(Macro_x, Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, 0.2, beta);
        
        for i=2:θ-1
            Micro_x[i], Micro_xPhys[i],Micro_xTilde[i], Micro_change1 = OC(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, 0.2, beta);
            Micro_change[i]=Micro_change1;
            Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx,Micro_nelz);
        end
        
        # Micro_x, Micro_xPhys, Micro_change = OC(Micro_x, Micro_dc, Micro_dv, Micro_H, Micro_Hs, Micro_Vol, Micro_nele, 0.02, beta);
        # Micro_xPhys = reshape(Micro_xPhys, Micro_nely, Micro_nelx, Micro_nelz);
        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx, Macro_nelz);


        # PRINT RESULTS
        println("$loop Obj:$(round.(c,digits=2)) Macro_Vol:$(round.(mean(Macro_xPhys[:]),digits=2) ) Micro_Vol:$(round.(mean.(Micro_xPhys[:][:] ),digits=2)) Macro_ch:$(round.(Macro_change,digits=2)) Micro_ch:$(round.(Micro_change,digits=2))")
        if mod(loop,saveItr)==0
            # emp=zeros(size(Macro_xPhys))
            # for i=1:θ
            #     emp[Bool.(1 .-Macro_masks[i])].=Micro_Vol[i]
            # end
            # emp1=copy(emp)
            # emp1[Macro_xPhys.<0.7].=0.0
            # scene= volume(emp1, algorithm = :iso,colorrange=(0.0, 1.0),isorange = 0.1, isovalue = Micro_Vol[2],colormap=:lighttest)
            # scene= volume!(emp1, algorithm = :iso,colorrange=(0.0, 1.0),isorange = 0.1, isovalue = Micro_Vol[3],colormap=:lighttest)
            # scene= volume!(emp1, algorithm = :iso,colorrange=(0.0, 1.0),isorange = 0.1, isovalue = Micro_Vol[4],colormap=:lighttest)
            # scene= volume!(emp1, algorithm = :iso,colorrange=(0.0, 1.0),isorange = 0.1, isovalue = Micro_Vol[5],colormap=:lighttest)
            # display(scene)

            scene= GLMakie.volume(Macro_xPhys, algorithm = :iso,isorange = 0.3, isovalue = 1.0,colormap=:grays)
            display(scene)

            # save("./img/Macro_xPhys_$(Macro_Vol)_$(penal)_$(rmin)_$(loop).png",scene)
            
            for i=2:θ-1
                scene= GLMakie.volume(Micro_xPhys[i], algorithm = :iso,isorange = 0.3, isovalue = 1.0,colormap=:grays)
                display(scene)

                # save("./img/Micro_xPhys_$(i)_$(Micro_Vol[i])_$(penal)_$(rmin)_$(loop).png",scene)
            end

        end
        # # UPDATE HEAVISIDE REGULARIZATION PARAMETER
        # if beta < 512 && (loopbeta >= 50 || Macro_change <= 0.01 || Bool.(sum(Micro_change .<= 0.01)) )
        #     beta = 2*beta; loopbeta = 0; Macro_change = 1; Micro_change[:] = .1;
        #     display("Parameter beta increased to $beta.");
        # end
    end
    
    return Macro_xPhys,Micro_xPhys,Macro_masks,Micro_Vol
end

# multiple microstructures (θ)
function MultiConTop3DU(θ,Macro_struct, Micro_struct,prob, penal,saveItr=5,maxloop = 90,fabric=false,mgcg=[false,false],voxels=true)

    # USER-DEFINED LOOP PARAMETERS
    E0 = 1; Emin = 1e-9; nu = 0.3;

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2]; Macro_Height = Macro_struct[3];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = Int(Macro_struct[4]); Macro_nely  = Int(Macro_struct[5]); Macro_nelz   = Int(Macro_struct[6]);
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Macro_Vol = Macro_struct[7][2]; 
    Macro_Vol_FM    = Macro_struct[7][1]
    Micro_Vol_FM    = Micro_struct[7][1];
    Micro_rmin=(Micro_struct[8])
    Macro_rmin=(Macro_struct[8])
    Micro_move=Micro_struct[9]

    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];

    # PREPARE FINITE ELEMENT ANALYSIS
    U,F,freedofs=prob(Macro_nelx,Macro_nely,Macro_nelz);

    nodenrs = reshape(1:(1+Macro_nelx)*(1+Macro_nely)*(1+Macro_nelz),1+Macro_nely,1+Macro_nelx,1+Macro_nelz)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,Macro_nelx*Macro_nely*Macro_nelz,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nely+1)*(Macro_nelx+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]],Macro_nelx*Macro_nely*Macro_nelz,1)
    
    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*Macro_nele,1))
    
    # PREPARE FILTER
    # Macro_H,Macro_Hs = filtering3d(Macro_nelx, Macro_nely, Macro_nelz, Macro_nele, rmin);
    # Micro_H,Micro_Hs = filtering3d(Micro_nelx, Micro_nely, Micro_nelz, Micro_nele, rmin);
    # Macro_H,Macro_Hs = make_filter3D(Macro_nelx, Macro_nely, Macro_nelz, Macro_rmin);
    Macro_H,Macro_Hs = make_filter3D(Macro_nelx, Macro_nely, Macro_nelz, Macro_rmin);
    Micro_H,Micro_Hs = make_filter3D(Micro_nelx, Micro_nely, Micro_nelz, Micro_rmin);
    
    
    ##free Material distribution to see how to divide microstructures
    Macro_masks,Micro_Vol,Macro_xPhys_diag=Uclustering3D(θ,Macro_nely,Macro_nelx,Macro_nelz,Macro_nele,Macro_Vol_FM,Micro_Vol_FM,iK,jK,Emin,U,F,freedofs,edofMat,voxels)


    # INITIALIZE ITERATION
    Macro_x = ones(Macro_nely,Macro_nelx,Macro_nelz).*Macro_Vol;
    if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    end
    
    ss=4

    # Micro_x = ones(Micro_nely,Micro_nelx,Micro_nelz);
    Micro_x=[]
    for i=1:θ
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx,Micro_nelz)]);
        # Micro_x[i][Int(Micro_nely/2)-5:Int(Micro_nely/2)+5,Int(Micro_nelx/2)-5:Int(Micro_nelx/2)+5,Int(Micro_nelz/2)-5:Int(Micro_nelz/2)+5] .= 0;
        # Micro_x[i][Int(Micro_nely/2):Int(Micro_nely/2)+1,Int(Micro_nelx/2):Int(Micro_nelx/2)+1,Int(Micro_nelz/2):Int(Micro_nelz/2)+1] .= 0;
        Micro_x[i][Int(Micro_nely/2)-2:Int(Micro_nely/2)+2,Int(Micro_nelx/2)-2:Int(Micro_nelx/2)+2,Int(Micro_nelz/2)-2:Int(Micro_nelz/2)+2] .= 0;

        if fabric
            Micro_x[i].=addFabricationConstraints(ss,Emin,Micro_x[i],Micro_nelx,Micro_nely,Micro_nelz)
        end
    end

    beta = 1;
    Macro_xTilde = Macro_x; 
    # Micro_xTilde = Micro_x;
    Macro_xPhys = 1 .-exp.(-beta .*Macro_xTilde) .+Macro_xTilde .*exp.(-beta);
    # Micro_xPhys = 1 .-exp.(-beta .*Micro_xTilde) .+Micro_xTilde .*exp.(-beta);
    
    Micro_xTilde=[]
    Micro_xPhys=[];
    Micro_dc = []
    Micro_dv = []
    Micro_change=[]
    DHs=[]
    dDHs=[]
    for i=1:θ
        append!(Micro_xTilde,[Micro_x[i]])
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde[i]) .+ Micro_xTilde[i] * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx, Micro_nelz)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx, Micro_nelz)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM3D(Micro_xPhys[i], Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end

    ### IF MGCG
    if Macro_mgcg
        Macro_nl=4
        Macro_Pu=Array{Any}(undef, Macro_nl-1);
        for l = 1:Macro_nl-1
            Macro_Pu[l,1] = prepcoarse3(Macro_nelz/2^(l-1),Macro_nely/2^(l-1),Macro_nelx/2^(l-1));
        end
        fixeddofs  = setdiff(1:Macro_ndof,freedofs)
        Macro_N = ones(Macro_ndof); Macro_N[fixeddofs] .= 0;
        Macro_Null = spdiagm(Macro_ndof,Macro_ndof,0 => Macro_N);
    end

    t=:darktest
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=cgrad(cschemeList)



    loopbeta = 0; loop = 0; Macro_change = 1; 
    # Micro_change = 1;
    while loop < maxloop && ( Macro_change > 0.01 || Bool.(sum(Micro_change .>= 0.01)>=1))
        loop = loop+1; loopbeta = loopbeta+1;


        # FE-ANALYSIS AT TWO SCALES
        Kes=zeros(24*24,Macro_nelx*Macro_nely*Macro_nelz)

        Threads.@threads for i=1:θ
            # if fabric
            #     Micro_x[i].=addFabricationConstraints(ss,Emin,Micro_x[i],Micro_nelx,Micro_nely,Micro_nelz)
            #     Micro_xPhys[i].=addFabricationConstraints(ss,Emin,Micro_xPhys[i],Micro_nelx,Micro_nely,Micro_nelz)
            # end
            # scene= GLMakie.volume(Micro_xPhys[i], algorithm = :iso, isorange = 0.2, isovalue = 0.8,colormap=:grays)

            DH, dDH = EBHM3D(Micro_xPhys[i], Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
            DHs[i]=DH;
            dDHs[i]=dDH;
        end

        for i=1:θ
            Kes[:,Bool.(Macro_masks[i][:])].=elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DHs[i])[:]
        end

        ### IF MGCG
        if Macro_mgcg
            K = Array{Any}(undef, Macro_nl);
            sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);

            K[1,1] = sparse(vec(iK),vec(jK),vec(sK));
            K[1,1] = Macro_Null'*K[1,1]*Macro_Null - (Macro_Null .-sparse(1.0I, Macro_ndof, Macro_ndof)); 


            for l = 1:Macro_nl-1
                K[l+1,1] = Macro_Pu[l,1]'*(K[l,1]*Macro_Pu[l,1]);
            end

            Lfac = factorize(Matrix(Hermitian(K[Macro_nl,1]))).L ;Ufac = Lfac';
            cgtol=1e-10;
            cgmax=100;
            cgiters,cgres,U = MGCG(K,F[:,1],U,Lfac,Ufac,Macro_Pu,Macro_nl,1,cgtol,cgmax);

        else
            sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
            # sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
            K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
            U[freedofs] = K[freedofs,freedofs]\Array(F[freedofs]);

        end
        
        # Ke = elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH);#old
        
        
        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        # Ke = elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH);
        # ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);
        
        #get compliance slow try to make it fast!! ??
        # ce=zeros(Macro_nely*Macro_nelx*Macro_nelz)
        # for i=1:Macro_nely*Macro_nelx*Macro_nelz
        #     ce[i]=sum((U[edofMat]*reshape(Kes[:,i],24,24)).*U[edofMat],dims=2)[i]
        # end

        # UU=[]
        # KKK=[]
        # for i=1:Macro_nely*Macro_nelx*Macro_nelz
        #     append!(UU,[U[edofMat][i,:]'])
        #     append!(KKK,[reshape(Kes[:,i],24,24)])
        # end
        UU=mapslices(x->[x]'[:], U[edofMat], dims=2)[:]
        KKK=mapslices(x->[reshape(x,24,24)], Kes, dims=1)[:]


        ce=sum(vcat((UU.*KKK)...).*U[edofMat],dims=2)
        ce=reshape(ce,Macro_nely,Macro_nelx,Macro_nelz)
        
        
        c = sum(sum(sum((Emin .+Macro_xPhys.^penal*(1-Emin)).*ce)));
        Macro_dc = -penal*(1-Emin)*Macro_xPhys.^(penal-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx, Macro_nelz);

        # Micro_dc = zeros(Micro_nely, Micro_nelx, Micro_nelz);

        for i=1:θ
            Micro_dc[i]=zeros(Micro_nely, Micro_nelx, Micro_nelz);
            Micro_dv[i]=ones(Micro_nely, Micro_nelx, Micro_nelz);
        end

        for ii=1:θ
            for i = 1:Micro_nele
                dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i] dDHs[ii][1,4][i] dDHs[ii][1,5][i] dDHs[ii][1,6][i];
                        dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i] dDHs[ii][2,4][i] dDHs[ii][2,5][i] dDHs[ii][2,6][i];
                        dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i] dDHs[ii][3,4][i] dDHs[ii][3,5][i] dDHs[ii][3,6][i];
                        dDHs[ii][4,1][i] dDHs[ii][4,2][i] dDHs[ii][4,3][i] dDHs[ii][4,4][i] dDHs[ii][4,5][i] dDHs[ii][4,6][i];
                        dDHs[ii][5,1][i] dDHs[ii][5,2][i] dDHs[ii][5,3][i] dDHs[ii][5,4][i] dDHs[ii][5,5][i] dDHs[ii][5,6][i];
                        dDHs[ii][6,1][i] dDHs[ii][6,2][i] dDHs[ii][6,3][i] dDHs[ii][6,4][i] dDHs[ii][6,5][i] dDHs[ii][6,6][i]];
                dKE = elementMatVec3D(Macro_Elex, Macro_Eley, Macro_Elez, dDHe);
                dce = sum((U[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                Micro_dc[ii][i] = -sum(sum(sum((Emin .+Macro_xPhys[Bool.(Macro_masks[ii][:])][:].^penal*(1-Emin)).*dce)));
            end
        end
        # Micro_dv = ones(Micro_nely, Micro_nelx, Micro_nelz);

        # FILTERING AND MODIFICATION OF SENSITIVITIES
        Macro_dx = beta*exp.(-beta.*Macro_xTilde).+exp(-beta);
        Macro_dc[:] = Macro_H*(Macro_dc[:].*Macro_dx[:]./Macro_Hs); 
        Macro_dv[:] = Macro_H*(Macro_dv[:].*Macro_dx[:]./Macro_Hs);

        for i=1:θ
            Micro_dx = beta .*exp.(-beta .*Micro_xTilde[i]) .+exp.(-beta);
            Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
            Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
        end


        # Micro_dx = beta*exp.(-beta.*Micro_xTilde).+exp.(-beta);
        # Micro_dc[:] = Micro_H*(Micro_dc[:].*Micro_dx[:]./Micro_Hs); 
        # Micro_dv[:] = Micro_H*(Micro_dv[:].*Micro_dx[:]./Micro_Hs);

        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys,Macro_xTilde, Macro_change = OC(Macro_x, Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, 0.1, beta,voxels);
        
        for i=1:θ
            Micro_x[i], Micro_xPhys[i],Micro_xTilde[i], Micro_change1 = OC(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, Micro_move, beta,voxels,fabric,ss);
            Micro_change[i]=Micro_change1;
            Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx,Micro_nelz);
        end
        
        # Micro_x, Micro_xPhys, Micro_change = OC(Micro_x, Micro_dc, Micro_dv, Micro_H, Micro_Hs, Micro_Vol, Micro_nele, 0.02, beta);
        # Micro_xPhys = reshape(Micro_xPhys, Micro_nely, Micro_nelx, Micro_nelz);
        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx, Macro_nelz);


        # PRINT RESULTS
        println("$loop Obj:$(round.(c,digits=4)) Macro_Vol:$(round.(mean(Macro_xPhys[:] ),digits=2)) Micro_Vol:$(round.(mean.(Micro_xPhys[:][:]),digits=2)) Macro_ch:$(round.(Macro_change,digits=2)) Micro_ch:$(round.(Micro_change,digits=2))")


        if mod(loop,saveItr)==0
            emp=zeros(size(Macro_xPhys))
            for i=1:θ
                emp[Bool.(Macro_masks[i])].=i
            end
            
            emp1=copy(emp)
            emp1[Macro_xPhys.<0.7].=0.0
            # scene = Scene(resolution = (400, 400))
            Makie.inline!(true)
            fig = Figure(resolution=(600, 600), fontsize=10)

            Makie.volume(emp1, algorithm = :iso,colorrange=(0.0, θ),isorange = 1.0, isovalue = 1,colormap=cscheme)

            for i=2:θ
                Makie.volume!(emp1, algorithm = :iso,colorrange=(0.0, θ),isorange = 1.0, isovalue = i,colormap=cscheme)
            end
            display(current_figure())
            # scene= volume(Macro_xPhys, algorithm = :iso,isorange = 0.3, isovalue = 1.0,colormap=:grays)
            

            # save("./img/Macro_xPhys_3_$(Macro_Vol)_$(penal)_$(Macro_rmin)_$(loop).png",scene)
            
            for i=1:θ
                temp=copy(Micro_xPhys[i])
                # display(sum(Micro_xPhys[i]))
                temp[Micro_xPhys[i].<0.6].=0.0
                temp[Micro_xPhys[i].>=0.6].=i
                # scene = Scene(resolution = (200, 200))
                scene= GLMakie.volume(temp,colorrange=(0.0, θ), algorithm = :iso,isorange = 1.0, isovalue = i,colormap=cscheme)
                display(scene)
                # save("./img/Micro_xPhys3U_3_$(i)_$(Micro_Vol[i])_$(Micro_rmin)_$(loop).png",scene)
            end

            # for i=1:θ
            #     scene = Scene(resolution = (400, 400))
            #     scene= volume!(Micro_xPhys[i], algorithm = :iso,isorange = 0.4, isovalue = 0.9,colormap=:grays)
            #     display(scene)
            #     save("./img/Micro_xPhys_$(i)_$(Micro_Vol[i])_$(penal)_$(rmin)_$(loop).png",scene)
            # end

        end
        # # UPDATE HEAVISIDE REGULARIZATION PARAMETER
        # if beta < 512 && (loopbeta >= 50 || Macro_change <= 0.01 || Bool.(sum(Micro_change .<= 0.01)) )
        #     beta = 2*beta; loopbeta = 0; Macro_change = 1; Micro_change[:] = .1;
        #     display("Parameter beta increased to $beta.");
        # end
    end
    
    return Macro_xPhys,Micro_xPhys,Macro_masks,Micro_Vol,DHs
end

# compliant multiple microstructures (θ)
function CompliantMultiConTop3DU(θ,Macro_struct, Micro_struct, prob,penal,saveItr=5,maxloop = 90,fabric=false,mgcg=[false,false],voxels=true)
    

    # USER-DEFINED LOOP PARAMETERS
    E0 = 1; Emin = 1e-9; nu = 0.3;

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2]; Macro_Height = Macro_struct[3];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = Int(Macro_struct[4]); Macro_nely  = Int(Macro_struct[5]); Macro_nelz   = Int(Macro_struct[6]);
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Macro_Vol = Macro_struct[7][2]; 
    Macro_Vol_FM    = Macro_struct[7][1]
    Micro_Vol_FM    = Micro_struct[7][1];
    Micro_rmin=Int(Micro_struct[8])
    Macro_rmin=Int(Macro_struct[8])

    Macro_max_change=Macro_struct[9];Micro_max_change=Micro_struct[9]

    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];

    if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    end


    # PREPARE FINITE ELEMENT ANALYSIS
    U,F,freedofs,din,dout=prob(Macro_nelx,Macro_nely,Macro_nelz)
    fixeddofs  = setdiff(1:Macro_ndof,freedofs)

    # edofMat = repeat(3 .*nodeids[:] .+1,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nelx+1)*(Macro_nely+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]], Macro_nele, 1);
    nodenrs = reshape(1:(1+Macro_nelx)*(1+Macro_nely)*(1+Macro_nelz),1+Macro_nely,1+Macro_nelx,1+Macro_nelz)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,Macro_nelx*Macro_nely*Macro_nelz,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nely+1)*(Macro_nelx+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]],Macro_nelx*Macro_nely*Macro_nelz,1)
    
    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*Macro_nele,1))
    
    # PREPARE FILTER
    # Macro_H,Macro_Hs = filtering3d(Macro_nelx, Macro_nely, Macro_nelz, Macro_nele, rmin);
    # Micro_H,Micro_Hs = filtering3d(Micro_nelx, Micro_nely, Micro_nelz, Micro_nele, rmin);
    Macro_H,Macro_Hs = make_filter3D(Macro_nelx, Macro_nely, Macro_nelz, Macro_rmin);
    Micro_H,Micro_Hs = make_filter3D(Micro_nelx, Micro_nely, Micro_nelz, Micro_rmin);
    
    
    ##free Material distribution to see how to divide microstructures
    Macro_masks,Micro_Vol,Macro_xPhys_diag=CompliantUclustering3D(θ,Macro_nely,Macro_nelx,Macro_nelz,Macro_nele,Macro_Vol_FM,Micro_Vol_FM,iK,jK,Emin,U,F,freedofs,edofMat,din,dout)


    # INITIALIZE ITERATION
    Macro_x = ones(Macro_nely,Macro_nelx,Macro_nelz).*Macro_Vol;

    # Micro_x = ones(Micro_nely,Micro_nelx,Micro_nelz);
    ss=4
    ss2=Int(ss/2)
    ss4=Int(ss/4)
    # ss4=Int(ss/2)
    Micro_x=[]
    for i=1:θ
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx,Micro_nelz).*Micro_Vol[i]]);
        Micro_x[i][Int(Micro_nely/2)-2:Int(Micro_nely/2)+2,Int(Micro_nelx/2)-2:Int(Micro_nelx/2)+2,Int(Micro_nelz/2)-2:Int(Micro_nelz/2)+2] .= 0;
        
        if fabric
            Micro_x[i].=addFabricationConstraints(ss,Emin,Micro_x[i],Micro_nelx,Micro_nely,Micro_nelz)
         end
    end

    beta = 1;
    Macro_xTilde = Macro_x; 
    # Micro_xTilde = Micro_x;
    Macro_xPhys = 1 .-exp.(-beta .*Macro_xTilde) .+Macro_xTilde .*exp.(-beta);
    # Micro_xPhys = 1 .-exp.(-beta .*Micro_xTilde) .+Micro_xTilde .*exp.(-beta);
    
    Micro_xTilde = Micro_x[1];
    Micro_xPhys=[];
    Micro_dc = []
    Micro_dv = []
    Micro_change=[]
    DHs=[]
    dDHs=[]
    for i=1:θ
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx, Micro_nelz)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx, Micro_nelz)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM3D(Micro_xPhys[i], Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end

    ### IF MGCG
    if Macro_mgcg
        Macro_nl=4
        Macro_Pu=Array{Any}(undef, Macro_nl-1);
        for l = 1:Macro_nl-1
            Macro_Pu[l,1] = prepcoarse3(Macro_nelz/2^(l-1),Macro_nely/2^(l-1),Macro_nelx/2^(l-1));
        end
        fixeddofs  = setdiff(1:Macro_ndof,freedofs)
        Macro_N = ones(Macro_ndof); Macro_N[fixeddofs] .= 0;
        Macro_Null = spdiagm(Macro_ndof,Macro_ndof,0 => Macro_N);
    end

    t=:darktest
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=cgrad(cschemeList)

    # to do change penalties increasing and change micro penalty constant like in concurrent 2D

    loopbeta = 0; loop = 0; Macro_change = 1; 
    # Micro_change = 1;
    while loop < maxloop && ( Macro_change > 0.01 || Bool.(sum(Micro_change .>= 0.01)>=1))
        loop = loop+1; loopbeta = loopbeta+1;

        # FE-ANALYSIS AT TWO SCALES
        Kes=zeros(24*24,Macro_nelx*Macro_nely*Macro_nelz)

        Threads.@threads for i=1:θ
            # if fabric
            #     Micro_x[i].=addFabricationConstraints(ss,Emin,Micro_x[i],Micro_nelx,Micro_nely,Micro_nelz)
            #     Micro_xPhys[i].=addFabricationConstraints(ss,Emin,Micro_xPhys[i],Micro_nelx,Micro_nely,Micro_nelz)
            # end

            DH, dDH = EBHM3D(Micro_xPhys[i], Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
            DHs[i]=DH;
            dDHs[i]=dDH;
        end

        for i=1:θ
            Kes[:,Bool.(Macro_masks[i][:])].=elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DHs[i])[:]
        end
        
        # Ke = elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH);#old
        
        ### IF MGCG
        if Macro_mgcg
            K = Array{Any}(undef, Macro_nl);
            sK = reshape(Kes.*(Emin.+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1)

            K[1,1] = sparse(vec(iK),vec(jK),vec(sK));
            K[1,1] = Macro_Null'*K[1,1]*Macro_Null - (Macro_Null .-sparse(1.0I, Macro_ndof, Macro_ndof)); 

            K[1,1][din,din] = K[1,1][din,din] + 0.1;
            K[1,1][dout,dout] = K[1,1][dout,dout] + 0.1;

            for l = 1:Macro_nl-1
                K[l+1,1] = Macro_Pu[l,1]'*(K[l,1]*Macro_Pu[l,1]);
                # K[l+1,1][din,din] = K[l+1,1][din,din] + 0.1;
                # K[l+1,1][dout,dout] = K[l+1,1][dout,dout] + 0.1;
            end

            Lfac = factorize(Matrix(Hermitian(K[Macro_nl,1]))).L ;Ufac = Lfac';
            cgtol=1e-10;
            cgmax=100;
            cgiters,cgres,U1 = MGCG(K,F[:,1],U[:,1],Lfac,Ufac,Macro_Pu,Macro_nl,1,cgtol,cgmax);
            cgiters,cgres,U2 = MGCG(K,F[:,2],U[:,2],Lfac,Ufac,Macro_Pu,Macro_nl,1,cgtol,cgmax);

        else
            sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
            K = sparse(vec(iK),vec(jK),vec(sK)); 
            
            K[din,din] = K[din,din] + 0.1;
            K[dout,dout] = K[dout,dout] + 0.1;
            K = (K+K')/2;

            U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
            U1 = U[:,1]
            U2 = U[:,2]
        end

        

        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        # UU1=[]
        # UU2=[]
        # KKK=[]
        # for i=1:Macro_nely*Macro_nelx*Macro_nelz
        #     append!(UU1,[U1[edofMat][i,:]'])
        #     append!(UU2,[U2[edofMat][i,:]'])
        #     append!(KKK,[reshape(Kes[:,i],24,24)])
        # end

        UU1=mapslices(x->[x]'[:], U1[edofMat], dims=2)[:]
        UU2=mapslices(x->[x]'[:], U2[edofMat], dims=2)[:]
        KKK=mapslices(x->[reshape(x,24,24)], Kes, dims=1)[:]


        ce=-sum(vcat((UU1.*KKK)...).*U2[edofMat],dims=2)
        ce=reshape(ce,Macro_nely,Macro_nelx,Macro_nelz)
        
        c  = U[dout,1]
        # c = sum(sum(sum((Emin .+Macro_xPhys.^penal*(1-Emin)).*ce)));
        Macro_dc = -penal*(1-Emin)*Macro_xPhys.^(penal-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx, Macro_nelz);

        # Micro_dc = zeros(Micro_nely, Micro_nelx, Micro_nelz);

        for i=1:θ
            Micro_dc[i]=zeros(Micro_nely, Micro_nelx, Micro_nelz);
            Micro_dv[i]=ones(Micro_nely, Micro_nelx, Micro_nelz);
        end

        for ii=1:θ
            for i = 1:Micro_nele
                dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i] dDHs[ii][1,4][i] dDHs[ii][1,5][i] dDHs[ii][1,6][i];
                        dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i] dDHs[ii][2,4][i] dDHs[ii][2,5][i] dDHs[ii][2,6][i];
                        dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i] dDHs[ii][3,4][i] dDHs[ii][3,5][i] dDHs[ii][3,6][i];
                        dDHs[ii][4,1][i] dDHs[ii][4,2][i] dDHs[ii][4,3][i] dDHs[ii][4,4][i] dDHs[ii][4,5][i] dDHs[ii][4,6][i];
                        dDHs[ii][5,1][i] dDHs[ii][5,2][i] dDHs[ii][5,3][i] dDHs[ii][5,4][i] dDHs[ii][5,5][i] dDHs[ii][5,6][i];
                        dDHs[ii][6,1][i] dDHs[ii][6,2][i] dDHs[ii][6,3][i] dDHs[ii][6,4][i] dDHs[ii][6,5][i] dDHs[ii][6,6][i]];
                dKE = elementMatVec3D(Macro_Elex, Macro_Eley, Macro_Elez, dDHe);
                # dce = sum((U[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                dce = sum((U2[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U2[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                Micro_dc[ii][i] = -sum(sum(sum((Emin .+Macro_xPhys[Bool.(Macro_masks[ii][:])][:].^penal*(1-Emin)).*dce)));
            end
        end
        # Micro_dv = ones(Micro_nely, Micro_nelx, Micro_nelz);

        # FILTERING AND MODIFICATION OF SENSITIVITIES
        Macro_dx = beta*exp.(-beta.*Macro_xTilde).+exp(-beta);
        Macro_dc[:] = Macro_H*(Macro_dc[:].*Macro_dx[:]./Macro_Hs); 
        Macro_dv[:] = Macro_H*(Macro_dv[:].*Macro_dx[:]./Macro_Hs);

        for i=1:θ
            Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta);
            Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
            Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
        end

        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys,Macro_xTilde, Macro_change = CompliantOC3(Macro_x, Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, Macro_max_change, beta,voxels);
        
        for i=1:θ
            Micro_x[i], Micro_xPhys[i],Micro_xTilde[i], Micro_change1 = OC(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, Micro_max_change, beta,true,fabric,ss);
            Micro_change[i]=Micro_change1;
            Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx,Micro_nelz);
        end
        
       Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx, Macro_nelz);


        # PRINT RESULTS
        println("$loop Obj:$(round.(c,digits=2)) Macro_Vol:$(round.(mean(Macro_xPhys[:]),digits=2) ) Micro_Vol:$(round.(mean.(Micro_xPhys[:][:] ),digits=2)) Macro_ch:$(round.(Macro_change,digits=2)) Micro_ch:$(round.(Micro_change,digits=2))")
        if mod(loop,saveItr)==0
            emp=zeros(size(Macro_xPhys))
            for i=1:θ
                emp[Bool.(Macro_masks[i])].=i
            end
            emp1=copy(emp)
            emp1[Macro_xPhys.<0.7].=0.0
            # scene = Scene(resolution = (400, 400))
            scene= GLMakie.volume(emp1, algorithm = :iso,colorrange=(0.0, θ),isorange = 1.0, isovalue = 1,colormap=cscheme)

            for i=1:θ
                scene= GLMakie.volume!(emp1, algorithm = :iso,colorrange=(0, θ),isorange = 0.1, isovalue = i,colormap=cscheme)
            end

            # scene= volume(Macro_xPhys, algorithm = :iso,isorange = 0.3, isovalue = 1.0,colormap=:grays)
            display(scene)

            # save("./img/Wing_Macro_xPhys_$(Macro_Vol)_$(penal)_$(loop)_$(Int(fabric)).png",scene)
            
            for i=1:θ
                temp=copy(Micro_xPhys[i])
                # display(sum(Micro_xPhys[i]))
                temp[Micro_xPhys[i].<0.6].=0.0
                temp[Micro_xPhys[i].>=0.6].=i
                # scene = Scene(resolution = (200, 200))
                scene= GLMakie.volume(temp,colorrange=(0.0, θ), algorithm = :iso,isorange = 1.0, isovalue = i,colormap=cscheme)
                display(scene)
                save("./img/Wing_Micro_xPhys3U_$(i)_$(Micro_Vol[i])_$(loop)_$(Int(fabric)).png",scene)
            end

            # for i=1:θ
            #     scene = Scene(resolution = (400, 400))
            #     scene= volume!(Micro_xPhys[i], algorithm = :iso,isorange = 0.4, isovalue = 0.9,colormap=:grays)
            #     display(scene)
            #     save("./img/Micro_xPhys_$(i)_$(Micro_Vol[i])_$(penal)_$(rmin)_$(loop).png",scene)
            # end

        end
        # # UPDATE HEAVISIDE REGULARIZATION PARAMETER
        # if beta < 512 && (loopbeta >= 50 || Macro_change <= 0.01 || Bool.(sum(Micro_change .<= 0.01)) )
        #     beta = 2*beta; loopbeta = 0; Macro_change = 1; Micro_change[:] = .1;
        #     display("Parameter beta increased to $beta.");
        # end
    end
    
    return Macro_xPhys,Micro_xPhys,Macro_masks,Micro_Vol,DHs
end

##############################################################

#####3D
##############################################################
# multiple microstructures (θ) with 2D pieces
function SlicesMultiConTop3DU(θ,Macro_struct, Micro_struct, prob,penal,saveItr=5,maxloop = 90,fabric=false,mgcg=[false,false],voxels=true,concurrent=false)
    # USER-DEFINED LOOP PARAMETERS
    E0 = 1; Emin = 1e-9; nu = 0.3;

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2]; Macro_Height = Macro_struct[3];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = Int(Macro_struct[4]); Macro_nely  = Int(Macro_struct[5]); Macro_nelz   = Int(Macro_struct[6]);
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Macro_Vol = Macro_struct[7][2]; 
    Macro_Vol_FM    = Macro_struct[7][1];
    Micro_Vol_FM    = Micro_struct[7][1];
    Micro_rmin=Micro_struct[8]
    Macro_rmin=Macro_struct[8]

    Macro_max_change=Macro_struct[9];Micro_max_change=Micro_struct[9]

    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;Micro_nele2D = Micro_nelx*Micro_nely;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];

    if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    end


    # PREPARE FINITE ELEMENT ANALYSIS
    U,F,freedofs=prob(Macro_nelx,Macro_nely,Macro_nelz);
    fixeddofs  = setdiff(1:Macro_ndof,freedofs)

    # edofMat = repeat(3 .*nodeids[:] .+1,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nelx+1)*(Macro_nely+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]], Macro_nele, 1);
    nodenrs = reshape(1:(1+Macro_nelx)*(1+Macro_nely)*(1+Macro_nelz),1+Macro_nely,1+Macro_nelx,1+Macro_nelz)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,Macro_nelx*Macro_nely*Macro_nelz,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nely+1)*(Macro_nelx+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]],Macro_nelx*Macro_nely*Macro_nelz,1)

    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*Macro_nele,1))

    # PREPARE FILTER
    # Macro_H,Macro_Hs = filtering3d(Macro_nelx, Macro_nely, Macro_nelz, Macro_nele, rmin);
    # Micro_H,Micro_Hs = filtering3d(Micro_nelx, Micro_nely, Micro_nelz, Micro_nele, rmin);
    Macro_H,Macro_Hs = make_filter3D(Macro_nelx, Macro_nely, Macro_nelz, Macro_rmin);
    # Micro_H,Micro_Hs = filtering2d(Micro_nelx, Micro_nely, Micro_nele2D, Micro_rmin);
    Micro_H,Micro_Hs = make_filter(Micro_nelx, Micro_nely, Micro_rmin);

    # Micro_H,Micro_Hs = make_filter3D(Micro_nelx, Micro_nely, Micro_nelz, Micro_rmin);
    # INITIALIZE ITERATION
    Macro_x = ones(Macro_nely,Macro_nelx,Macro_nelz).*Macro_Vol;
    Micro_Vol=[];
    Micro_x=[];
    Micro_xPhys3D=[];
    for i=1:θ
        append!(Micro_Vol,[Micro_Vol_FM])
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx).*Micro_Vol[i]]);
        append!(Micro_xPhys3D ,[zeros(Micro_nely,Micro_nelx,Micro_nelz)]);
    end#########################

    #mma
    Micro_xold1=[]
    Micro_xold2=[]
    Micro_low=[];
    Micro_upp=[];
    for i=1:θ
        append!(Micro_xold1 ,[copy(Micro_x[i])]);
        append!(Micro_xold2 ,[copy(Micro_x[i])]);
        append!(Micro_low ,[0]);
        append!(Micro_upp ,[0]);
    end
    Macro_low=0;
    Macro_upp=0;
    Macro_xold1=copy(Macro_x);
    Macro_xold2=copy(Macro_x);

    beta = 1;
    Macro_xTilde = Macro_x; 
    Macro_xPhys = 1 .-exp.(-beta .*Macro_xTilde) .+Macro_xTilde .*exp.(-beta);

    ##free Material distribution to see how to divide microstructures
    a,aYX,aZY,aZX,Macro_xPhys_NC=SlicesUclustering3D(θ,Macro_nely,Macro_nelx,Macro_nelz,prob,Macro_Vol,Macro_rmin,Micro_Vol_FM,edofMat,voxels)
    if !concurrent
        # println("not concurrent!")
        Macro_xPhys=Macro_xPhys_NC
    end

    

    

    ###### STARTING CIRCLE ######
    # for i=1:θ
    #     append!(Micro_Vol,[Micro_Vol_FM])
    #     append!(Micro_x ,[ones(Micro_nely,Micro_nelx)]);
    #     append!(Micro_xPhys3D ,[zeros(Micro_nely,Micro_nelx,Micro_nelz)]);
    # end
    # for i = 1:Micro_nelx
    #     for j = 1:Micro_nely
    #         if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2) < min(Micro_nelx,Micro_nely)/8
    #             for l=1:θ
    #                 Micro_x[l][j,i] = Emin;
    #             end
    #         end
    #     end
    # end
    ####################################

    

    Micro_xTilde = Micro_x[1];
    Micro_xPhys=[];
    Micro_dc = [];
    Micro_dv = [];
    Micro_change=[];
    DHs=[]
    dDHs=[]
    for i=1:θ
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM2D(Micro_xPhys1, Micro_length, Micro_width, E0, Emin, nu, penal);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end

    ### IF MGCG
    if Macro_mgcg
        Macro_nl=4
        Macro_Pu=Array{Any}(undef, Macro_nl-1);
        for l = 1:Macro_nl-1
            Macro_Pu[l,1] = prepcoarse3(Macro_nelz/2^(l-1),Macro_nely/2^(l-1),Macro_nelx/2^(l-1));
        end
        fixeddofs  = setdiff(1:Macro_ndof,freedofs)
        Macro_N = ones(Macro_ndof); Macro_N[fixeddofs] .= 0;
        Macro_Null = spdiagm(Macro_ndof,Macro_ndof,0 => Macro_N);
    end

    t=:darktest #t=:dark
    t=:darktest #t=:darktest
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=cgrad(cschemeList); #cscheme=ColorGradient(cschemeList);

    # to do change penalties increasing and change micro penalty constant like in concurrent 2D

    Micro_penal=penal
    loopbeta = 0; loop = 0; Macro_change = 1; 

    # Micro_change = 1;
    while loop < maxloop && ( Macro_change > 0.01 || Bool.(sum(Micro_change .>= 0.01)>=1))
        loop = loop+1; loopbeta = loopbeta+1;

        # FE-ANALYSIS AT TWO SCALES
        MasksTemp=[]
        for i=1:θ
            DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, Micro_penal);
            DHs[i]=DH;
            dDHs[i]=dDH;
            append!(MasksTemp,[[]])
        end
        
        #for each element assemble the Micro_xPhys and add to the KES

        Kes=zeros(24*24,Macro_nely,Macro_nelx,Macro_nelz)
        DictEBHM = Dict();
        DictxPhys3D1 = Dict();
        xPhys3D1s1=[]
        # EBHM3Ds=[]
        ii=0;
        thickness=1;
        size3D=20;
        inds=findall(Macro_xPhys.>-100)
        m=@timed begin
            for i = 1:length(inds)
                yy=inds[i][1];xx=inds[i][2];zz=inds[i][3];
                θTempList="";DH="";
                xPhys3D=zeros(Micro_nely,Micro_nelx,Micro_nelz);
                θTemp=Int(aYX[Int((zz-1)*2+1)][Int(yy),Int(xx)]) ;xPhys3D[:,:,1:thickness]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                θTemp=Int(aYX[Int((zz-1)*2+2)][Int(yy),Int(xx)]) ;xPhys3D[:,:,end-thickness+1:end]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                θTemp=Int(aZY[Int((xx-1)*2+1)][Int(zz),Int(yy)]) ;xPhys3D[:,1:thickness,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                θTemp=Int(aZY[Int((xx-1)*2+2)][Int(zz),Int(yy)]) ;xPhys3D[:,end-thickness+1:end,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                θTemp=Int(aZX[Int((yy-1)*2+1)][Int(zz),Int(xx)]) ;xPhys3D[1:thickness,:,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                θTemp=Int(aZX[Int((yy-1)*2+2)][Int(zz),Int(xx)]) ;xPhys3D[end-thickness+1:end,:,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                if haskey(DictxPhys3D1, θTempList)
                else
                    xPhys3D1=reshape(xPhys3D,Micro_nely,Micro_nelx,Micro_nelz,1,1)
                    xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
                    DictxPhys3D1[θTempList]=ii+1
                    append!(xPhys3D1s1,[xPhys3D1])
                    # append!(EBHM3Ds,[zeros(6,6)])
                    ii+=1
                end    
            end
            # display("There are $ii unique 3d microstructures.")
            unique=ii
            EBHM3Ds=zeros(unique,6,6)
            xPhys3D1s=zeros(unique,size3D,size3D,size3D)
            for i in 1:unique
                xPhys3D1s[i,:,:,:].=xPhys3D1s1[i]
            end
            # tprintln("counter:")
            # Threads.@threads 
            Threads.@threads for i in 1:unique
                # θTempList=item.first
                xPhys3D1=xPhys3D1s[i,:,:,:]
                DH, dDH = EBHM3D(xPhys3D1, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
                EBHM3Ds[i,:,:].=DH
                # tprintln(" $ii,")
            end
        end
        m1=@timed begin
            for i = 1:length(inds)
                yy=inds[i][1];xx=inds[i][2];zz=inds[i][3];
                θTempList="";DH="";
                xPhys3D=zeros(Micro_nely,Micro_nelx,Micro_nelz);
                θTemp=Int(aYX[Int((zz-1)*2+1)][Int(yy),Int(xx)]) ;θTempList*=string(θTemp);
                θTemp=Int(aYX[Int((zz-1)*2+2)][Int(yy),Int(xx)]) ;θTempList*=string(θTemp);
                θTemp=Int(aZY[Int((xx-1)*2+1)][Int(zz),Int(yy)]) ;θTempList*=string(θTemp);
                θTemp=Int(aZY[Int((xx-1)*2+2)][Int(zz),Int(yy)]) ;θTempList*=string(θTemp);
                θTemp=Int(aZX[Int((yy-1)*2+1)][Int(zz),Int(xx)]) ;θTempList*=string(θTemp);
                θTemp=Int(aZX[Int((yy-1)*2+2)][Int(zz),Int(xx)]) ;θTempList*=string(θTemp);
            
                DH=EBHM3Ds[DictxPhys3D1[θTempList],:,:]
                Kes[:,yy,xx,zz].=elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH)[:]
            end
        end
        # tprintln("it took $(m[2]) seconds to process $ii unique 3d microstructures.")
        display("it took $(m[2]) seconds and $(m1[2]) seconds (total $(m[2]+m1[2]) seconds) to process $ii unique 3d microstructures on $(Threads.nthreads()) cores.")
        
        # DictEBHM = Dict();
        # i=1;
        # thickness=1;
        # size3D=20;
        # print("counter")
        # for yy=1:Macro_nely
        #     for xx=1:Macro_nelx
        #         for zz=1:Macro_nelz
        #             θTempList="";DH="";
        #             xPhys3D=zeros(Micro_nely,Micro_nelx,Micro_nelz);
        #             θTemp=Int(aYX[Int((zz-1)*2+1)][Int(yy),Int(xx)]) ;xPhys3D[:,:,1:thickness]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
        #             θTemp=Int(aYX[Int((zz-1)*2+2)][Int(yy),Int(xx)]) ;xPhys3D[:,:,end-thickness+1:end]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
        #             θTemp=Int(aYZ[Int((xx-1)*2+1)][Int(yy),Int(zz)]) ;xPhys3D[:,1:thickness,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
        #             θTemp=Int(aYZ[Int((xx-1)*2+2)][Int(yy),Int(zz)]) ;xPhys3D[:,end-thickness+1:end,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
        #             θTemp=Int(aXZ[Int((yy-1)*2+1)][Int(xx),Int(zz)]) ;xPhys3D[1:thickness,:,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
        #             θTemp=Int(aXZ[Int((yy-1)*2+2)][Int(xx),Int(zz)]) ;xPhys3D[end-thickness+1:end,:,:]=Micro_x[θTemp]';append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                    
        #             if haskey(DictEBHM, θTempList)
        #                 DH=DictEBHM[θTempList]
        #             else
        #                 xPhys3D1=reshape(xPhys3D,Micro_nely,Micro_nelx,Micro_nelz,1,1)
        #                 xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
        #                 # xPhys3D1=reshape(Flux.AdaptiveMeanPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
        #                 DH, dDH = EBHM3D(xPhys3D1, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
        #                 DictEBHM[θTempList]=DH
        #                 print(" $i,")
        #                 i+=1
                        
        #             end

        #             Kes[:,yy,xx,zz].=elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH)[:]
        #         end
        #     end
        # end


        Kes=reshape(Kes,24*24,Macro_nely*Macro_nelx*Macro_nelz);

        ### IF MGCG
        if Macro_mgcg
            K = Array{Any}(undef, Macro_nl);
            sK = reshape(Kes.*(Emin.+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1)

            K[1,1] = sparse(vec(iK),vec(jK),vec(sK));
            K[1,1] = Macro_Null'*K[1,1]*Macro_Null - (Macro_Null .-sparse(1.0I, Macro_ndof, Macro_ndof)); 


            for l = 1:Macro_nl-1
                K[l+1,1] = Macro_Pu[l,1]'*(K[l,1]*Macro_Pu[l,1]);
            end

            Lfac = factorize(Matrix(Hermitian(K[Macro_nl,1]))).L ;Ufac = Lfac';
            cgtol=1e-10;
            cgmax=100;
            cgiters,cgres,U = MGCG(K,F[:,1],U[:,1],Lfac,Ufac,Macro_Pu,Macro_nl,1,cgtol,cgmax);

        else
            sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
            K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;

            U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);

        end

        UU=mapslices(x->[x]'[:], U[edofMat], dims=2)[:]
        KKK=mapslices(x->[reshape(x,24,24)], Kes, dims=1)[:]

        ce=sum(vcat((UU.*KKK)...).*U[edofMat],dims=2)
        ce=reshape(ce,Macro_nely,Macro_nelx,Macro_nelz)

        c = sum(sum(sum((Emin .+Macro_xPhys.^penal*(1-Emin)).*ce)));

        if concurrent
            Macro_dc = -penal*(1-Emin)*Macro_xPhys.^(penal-1).*ce;
            Macro_dv = ones(Macro_nely, Macro_nelx, Macro_nelz);

            # FILTERING AND MODIFICATION OF SENSITIVITIES
            Macro_dx = beta*exp.(-beta.*Macro_xTilde).+exp(-beta);
            Macro_dc[:] = Macro_H*(Macro_dc[:].*Macro_dx[:]./Macro_Hs); 
            Macro_dv[:] = Macro_H*(Macro_dv[:].*Macro_dx[:]./Macro_Hs);

            # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
            Macro_x, Macro_xPhys,Macro_xTilde, Macro_change = OC(Macro_x, Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, Macro_max_change, beta,voxels);

        end

        for i=1:θ
            Micro_dc[i]=zeros(Micro_nely, Micro_nelx);
            Micro_dv[i]=ones(Micro_nely, Micro_nelx);
        end


        # disY,disX,disZ,U22D,XX=reshapeU2DSlices(U[edofMat],Macro_nely, Macro_nelx, Macro_nelz);
        dissYX,dissZY,dissZX,U22D1,Macro_xPhys_2D=reshapeU2DSlices_new(U[edofMat],Macro_nely, Macro_nelx, Macro_nelz,Macro_xPhys);

        U22D=zeros(size(U22D1)[1],size(U22D1[1])[1]) #me being lazy trying to fix dimensions
        for i in 1:size(U22D1)[1]
            U22D[i,:].=U22D1[i]
        end
        
        for ii=1:θ
            for i = 1:Micro_nele2D
                dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i];
                        dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i];
                        dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i]];
                dKE = elementMatVec2D(Macro_Elex, Macro_Eley, dDHe);
                dce = sum((U22D[a.==ii,:]*dKE).*U22D[a.==ii,:],dims=2);
                Micro_dc[ii][i] = -sum(sum((Emin .+Macro_xPhys[:][MasksTemp[ii]][:].^Micro_penal*(1 .-Emin)).*dce));
            end
        end


        for i=1:θ
            Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta);
            Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
            Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
        end

        
        for i=1:θ
            ss=6
            Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
            Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
            Micro_x[i], Micro_xPhys[i], Micro_change1 = OC2D(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele2D, 0.2, beta,true,ss);
            # Micro_x[i], Micro_xPhys[i], Micro_change1,Micro_low[i],Micro_upp[i],Micro_xold1[i],Micro_xold2[i]= OC2DMMA(Micro_x[i],Micro_xold1[i],Micro_xold2[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele2D, 0.2, beta,loop,Micro_low[i],Micro_upp[i]);
            Micro_change[i]=Micro_change1;
            Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx);
            Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
            Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
        end

        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx, Macro_nelz);
        # PRINT RESULTS
        println()
        println("$loop Obj:$(round.(c,digits=2)) Macro_Vol:$(round.(mean(Macro_xPhys[:]),digits=2) ) Micro_Vol:$(round.(mean.(Micro_xPhys[:][:] ),digits=2)) Macro_ch:$(round.(Macro_change,digits=2)) Micro_ch:$(round.(Micro_change,digits=2))")
        if mod(loop,saveItr)==0
            # emp=zeros(size(Macro_xPhys))
            # for i=1:θ
            #     emp[Bool.(Macro_masks[i])].=i
            # end
            # emp1=copy(emp)
            # emp1[Macro_xPhys.<0.7].=0.0
            # scene = Scene(resolution = (400, 400))
            # for i=1:θ
            #     scene= volume!(emp1, algorithm = :iso,colorrange=(0, θ),isorange = 0.1, isovalue = i,colormap=cscheme)
            # end
            # scene = Scene(resolution = (400, 400))
            scene= GLMakie.volume(Macro_xPhys, algorithm = :iso,isorange = 0.2, isovalue = 0.8,colormap=:grays)
            display(scene)

            # save("./img/Wing_Macro_xPhys_$(Macro_Vol)_$(penal)_$(loop)_$(Int(fabric)).png",scene)

            space=fill(0,Micro_nely,1)
            Micro_xPhys_all=space;
            Micro_xPhys_all1=space;
            for i=1:θ
                Micro_xPhys_temp=copy(Micro_xPhys[i])
                Micro_xPhys_temp1=copy(Micro_xPhys[i])
                thres=0.75;
                Micro_xPhys_temp[Micro_xPhys_temp.<thres].=0.0
                Micro_xPhys_temp[Micro_xPhys_temp.>=thres].=i
                Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys_temp);
                Micro_xPhys_all=hcat(Micro_xPhys_all,space);
                Micro_xPhys_all1=hcat(Micro_xPhys_all1,Micro_xPhys_temp1);
                Micro_xPhys_all1=hcat(Micro_xPhys_all1,space);
            end
            display(Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
            display(Plots.heatmap(1.0.-Micro_xPhys_all1,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))
            (Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))


            # for i=1:θ
            #     scene = Scene(resolution = (400, 400))
            #     scene= volume!(Micro_xPhys[i], algorithm = :iso,isorange = 0.4, isovalue = 0.9,colormap=:grays)
            #     display(scene)
            #     save("./img/Micro_xPhys_$(i)_$(Micro_Vol[i])_$(penal)_$(rmin)_$(loop).png",scene)
            # end

        end
        # # UPDATE HEAVISIDE REGULARIZATION PARAMETER
        # if beta < 512 && (loopbeta >= 50 || Macro_change <= 0.01 || Bool.(sum(Micro_change .<= 0.01)) )
        #     beta = 2*beta; loopbeta = 0; Macro_change = 1; Micro_change[:] = .1;
        #     display("Parameter beta increased to $beta.");
        # end
    end
     ##calculate and display properties
     n=3
     for i=1:θ
         Micro_xPhys_temp=copy(Micro_xPhys[i])
         Micro_xPhys_temp[Micro_xPhys_temp.<0.75].=0.0
         Micro_xPhys_temp[Micro_xPhys_temp.>=0.75].=i
         
         display(Plots.heatmap(repeat(Micro_xPhys_temp,n,n),clims=(0, θ),fc=cscheme,aspect_ratio=:equal,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing))
         savefig("./img/finalArrayMicro$(i).png") # Saves the CURRENT_PLOT as a .png
         
         U,S,V = svd(DHs[i]);
         sigma = S;
         k = sum(sigma .> 1e-15);
         SH = (U[:,1:k] * diagm(0=>(1 ./sigma[1:k])) * V[:,1:k]')'; # more stable SVD (pseudo)inverse
         EH = [1/SH[1,1], 1/SH[2,2]]; # effective Young's modulus
         GH = [1/SH[3,3]]; # effective shear modulus
         vH = [-SH[2,1]/SH[1,1],-SH[1,2]/SH[2,2]]; # effective Poisson's ratio
         display( "young:$(round.(EH,digits=3)), poisson:$(round.(vH,digits=3)), shear:$(round.(GH,digits=3))" )
 
         
         # savefig("./img/finalArrayMicro1$(i).png") # Saves the CURRENT_PLOT as a .png
 
     end

    return Macro_xPhys,Micro_xPhys,DHs,a,aYX,aZY,aZX

    
end

# compliant multiple microstructures (θ) with 2D pieces
function SlicesCompliantMultiConTop3DU(θ,Macro_struct, Micro_struct, prob,penal,saveItr=5,maxloop = 90,fabric=false,mgcg=[false,false],voxels=true)
    # USER-DEFINED LOOP PARAMETERS
    E0 = 1; Emin = 1e-9; nu = 0.3;

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2]; Macro_Height = Macro_struct[3];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = Int(Macro_struct[4]); Macro_nely  = Int(Macro_struct[5]); Macro_nelz   = Int(Macro_struct[6]);
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Macro_Vol = Macro_struct[7][2]; 
    Macro_Vol_FM    = Macro_struct[7][1];
    Micro_Vol_FM    = Micro_struct[7][1];
    Micro_rmin=(Micro_struct[8])
    Macro_rmin=(Macro_struct[8])

    Macro_max_change=Macro_struct[9];Micro_max_change=Micro_struct[9]

    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;Micro_nele2D = Micro_nelx*Micro_nely;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];

    if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    end


    # PREPARE FINITE ELEMENT ANALYSIS

    U,F,freedofs,din,dout=prob(Macro_nelx,Macro_nely,Macro_nelz)
    fixeddofs  = setdiff(1:Macro_ndof,freedofs)

    # edofMat = repeat(3 .*nodeids[:] .+1,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nelx+1)*(Macro_nely+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]], Macro_nele, 1);
    nodenrs = reshape(1:(1+Macro_nelx)*(1+Macro_nely)*(1+Macro_nelz),1+Macro_nely,1+Macro_nelx,1+Macro_nelz)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,Macro_nelx*Macro_nely*Macro_nelz,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nely+1)*(Macro_nelx+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]],Macro_nelx*Macro_nely*Macro_nelz,1)

    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*Macro_nele,1))

    # PREPARE FILTER
    # Macro_H,Macro_Hs = filtering3d(Macro_nelx, Macro_nely, Macro_nelz, Macro_nele, rmin);
    # Micro_H,Micro_Hs = filtering3d(Micro_nelx, Micro_nely, Micro_nelz, Micro_nele, rmin);
    Macro_H,Macro_Hs = make_filter3D(Macro_nelx, Macro_nely, Macro_nelz, Macro_rmin);
    # Micro_H,Micro_Hs = filtering2d(Micro_nelx, Micro_nely, Micro_nele2D, Micro_rmin);
    Micro_H,Micro_Hs = make_filter(Micro_nelx, Micro_nely, Micro_rmin);
    

    ##free Material distribution to see how to divide microstructures
    a,aaY,aaX,aaZ=SlicesCompliantUclustering3D(θ,Macro_nely,Macro_nelx,Macro_nelz,Macro_nele,Macro_Vol,Micro_Vol_FM,iK,jK,Emin,U,F,freedofs,edofMat,din,dout,voxels)


    # INITIALIZE ITERATION
    Macro_x = ones(Macro_nely,Macro_nelx,Macro_nelz).*Macro_Vol;
    Micro_x=[];Micro_Vol=[];
    Micro_xPhys3D=[]

    for i=1:θ
        append!(Micro_Vol,[Micro_Vol_FM])
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx).*Micro_Vol[i]]);
        append!(Micro_xPhys3D ,[zeros(Micro_nely,Micro_nelx,Micro_nelz)]);
    end

    # for i=1:θ
    #     append!(Micro_Vol,[Micro_Vol_FM])
    #     append!(Micro_x ,[ones(Micro_nely,Micro_nelx)]);
    #     append!(Micro_xPhys3D ,[zeros(Micro_nely,Micro_nelx,Micro_nelz)]);

    # end

    # for i = 1:Micro_nelx
    #     for j = 1:Micro_nely
    #         if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2) < min(Micro_nelx,Micro_nely)/8
    #             for l=1:θ
    #                 Micro_x[l][j,i] = Emin;
    #             end
    #         end
    #     end
    # end

    #mma
    Micro_xold1=[]
    Micro_xold2=[]
    Micro_low=[];
    Micro_upp=[];
    for i=1:θ
        append!(Micro_xold1 ,[copy(Micro_x[i])]);
        append!(Micro_xold2 ,[copy(Micro_x[i])]);
        append!(Micro_low ,[0]);
        append!(Micro_upp ,[0]);
    end
    Macro_low=0;
    Macro_upp=0;
    Macro_xold1=copy(Macro_x);
    Macro_xold2=copy(Macro_x);

    beta = 1;
    Macro_xTilde = Macro_x; 
    Macro_xPhys = 1 .-exp.(-beta .*Macro_xTilde) .+Macro_xTilde .*exp.(-beta);

    Micro_xTilde = Micro_x[1];
    Micro_xPhys=[];
    Micro_dc = [];
    Micro_dv = [];
    Micro_change=[];
    DHs=[]
    dDHs=[]
    for i=1:θ
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM2D(Micro_xPhys1, Micro_length, Micro_width, E0, Emin, nu, penal);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end

    ### IF MGCG
    if Macro_mgcg
        Macro_nl=4
        Macro_Pu=Array{Any}(undef, Macro_nl-1);
        for l = 1:Macro_nl-1
            Macro_Pu[l,1] = prepcoarse3(Macro_nelz/2^(l-1),Macro_nely/2^(l-1),Macro_nelx/2^(l-1));
        end
        fixeddofs  = setdiff(1:Macro_ndof,freedofs)
        Macro_N = ones(Macro_ndof); Macro_N[fixeddofs] .= 0;
        Macro_Null = spdiagm(Macro_ndof,Macro_ndof,0 => Macro_N);
    end

    t=:darktest #t=:dark
    t=:darktest #t=:darktest
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=cgrad(cschemeList); #cscheme=ColorGradient(cschemeList);

    # to do change penalties increasing and change micro penalty constant like in concurrent 2D

    Micro_penal=penal
    loopbeta = 0; loop = 0; Macro_change = 1; 

    # Micro_change = 1;
    while loop < maxloop && ( Macro_change > 0.01 || Bool.(sum(Micro_change .>= 0.01)>=1))
        loop = loop+1; loopbeta = loopbeta+1;

        # FE-ANALYSIS AT TWO SCALES
        MasksTemp=[]
        for i=1:θ
            DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, Micro_penal);
            DHs[i]=DH;
            dDHs[i]=dDH;
            append!(MasksTemp,[[]])
        end
        
        #for each element assemble the Micro_xPhys and add to the KES

        Kes=zeros(24*24,Macro_nely,Macro_nelx,Macro_nelz)
        
        DictEBHM = Dict();
        i=1;
        thickness=1;
        size3D=20;
        for yy=1:Macro_nely
            for xx=1:Macro_nelx
                for zz=1:Macro_nelz
                    θTempList="";DH="";
                    xPhys3D=zeros(Micro_nely,Micro_nelx,Micro_nelz);
                    θTemp=aaY[xx,zz,(yy-1)*2+1] ;xPhys3D[1:thickness,:,:]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                    θTemp=aaY[xx,zz,(yy-1)*2+2] ;xPhys3D[end-thickness+1:end,:,:]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                    θTemp=aaX[yy,zz,(xx-1)*2+1] ;xPhys3D[:,1:thickness,:]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                    θTemp=aaX[yy,zz,(xx-1)*2+2] ;xPhys3D[:,end-thickness+1:end,:]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                    θTemp=aaZ[yy,xx,(zz-1)*2+1] ;xPhys3D[:,:,1:thickness]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                    θTemp=aaZ[yy,xx,(zz-1)*2+2] ;xPhys3D[:,:,end-thickness+1:end]=Micro_x[θTemp];append!(MasksTemp[θTemp],[i]);θTempList*=string(θTemp);
                    
                    if haskey(DictEBHM, θTempList)
                        DH=DictEBHM[θTempList]
                    else
                        xPhys3D1=reshape(xPhys3D,Micro_nely,Micro_nelx,Micro_nelz,1,1)
                        xPhys3D1=reshape(Flux.AdaptiveMaxPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
                        # xPhys3D1=reshape(Flux.AdaptiveMeanPool((size3D, size3D,size3D))(xPhys3D1),size3D,size3D,size3D)
                        DH, dDH = EBHM3D(xPhys3D1, Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
                        DictEBHM[θTempList]=DH
                        i+=1
                    end

                    Kes[:,yy,xx,zz].=elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH)[:]
                end
            end
        end


        Kes=reshape(Kes,24*24,Macro_nely*Macro_nelx*Macro_nelz);

        ### IF MGCG
        if Macro_mgcg
            K = Array{Any}(undef, Macro_nl);
            sK = reshape(Kes.*(Emin.+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1)

            K[1,1] = sparse(vec(iK),vec(jK),vec(sK));
            K[1,1] = Macro_Null'*K[1,1]*Macro_Null - (Macro_Null .-sparse(1.0I, Macro_ndof, Macro_ndof)); 

            K[1,1][din,din] = K[1,1][din,din] + 0.1;
            K[1,1][dout,dout] = K[1,1][dout,dout] + 0.1;

            for l = 1:Macro_nl-1
                K[l+1,1] = Macro_Pu[l,1]'*(K[l,1]*Macro_Pu[l,1]);
                # K[l+1,1][din,din] = K[l+1,1][din,din] + 0.1;
                # K[l+1,1][dout,dout] = K[l+1,1][dout,dout] + 0.1;
            end

            Lfac = factorize(Matrix(Hermitian(K[Macro_nl,1]))).L ;Ufac = Lfac';
            cgtol=1e-10;
            cgmax=100;
            cgiters,cgres,U1 = MGCG(K,F[:,1],U[:,1],Lfac,Ufac,Macro_Pu,Macro_nl,1,cgtol,cgmax);
            cgiters,cgres,U2 = MGCG(K,F[:,2],U[:,2],Lfac,Ufac,Macro_Pu,Macro_nl,1,cgtol,cgmax);

        else
            sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
            K = sparse(vec(iK),vec(jK),vec(sK)); 

            K[din,din] = K[din,din] + 0.1;
            K[dout,dout] = K[dout,dout] + 0.1;
            K = (K+K')/2;

            U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
            U1 = U[:,1]
            U2 = U[:,2]
        end

        UU1=mapslices(x->[x]'[:], U1[edofMat], dims=2)[:]
        UU2=mapslices(x->[x]'[:], U2[edofMat], dims=2)[:]
        KKK=mapslices(x->[reshape(x,24,24)], Kes, dims=1)[:]

        ce=-sum(vcat((UU1.*KKK)...).*U2[edofMat],dims=2)
        ce=reshape(ce,Macro_nely,Macro_nelx,Macro_nelz)

        c  = U[dout,1]
        # c = sum(sum(sum((Emin .+Macro_xPhys.^penal*(1-Emin)).*ce)));
        Macro_dc = -penal*(1-Emin)*Macro_xPhys.^(penal-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx, Macro_nelz);


        for i=1:θ
            Micro_dc[i]=zeros(Micro_nely, Micro_nelx);
            Micro_dv[i]=ones(Micro_nely, Micro_nelx);
        end


        disY,disX,disZ,U22D,XX=reshapeU2DSlices(U2[edofMat],Macro_nely, Macro_nelx, Macro_nelz);
        for ii=1:θ
            for i = 1:Micro_nele2D
                dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i];
                        dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i];
                        dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i]];
                dKE = elementMatVec2D(Macro_Elex, Macro_Eley, dDHe);
                dce = sum((U22D[a.==ii,:]*dKE).*U22D[a.==ii,:],dims=2);
                Micro_dc[ii][i] = -sum(sum((Emin .+Macro_xPhys[:][MasksTemp[ii]][:].^Micro_penal*(1 .-Emin)).*dce));
            end
        end



        # FILTERING AND MODIFICATION OF SENSITIVITIES
        Macro_dx = beta*exp.(-beta.*Macro_xTilde).+exp(-beta);
        Macro_dc[:] = Macro_H*(Macro_dc[:].*Macro_dx[:]./Macro_Hs); 
        Macro_dv[:] = Macro_H*(Macro_dv[:].*Macro_dx[:]./Macro_Hs);

        for i=1:θ
            Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta);
            Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
            Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
        end

        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys, Macro_change = CompliantOC3(Macro_x, Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, Macro_max_change, beta,voxels);

        for i=1:θ
            ss=6
            Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
            Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
            Micro_x[i], Micro_xPhys[i], Micro_change1 = OC2D(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele2D, 0.2, beta,true,ss);
            # Micro_x[i], Micro_xPhys[i], Micro_change1,Micro_low[i],Micro_upp[i],Micro_xold1[i],Micro_xold2[i]= OC2DMMA(Micro_x[i],Micro_xold1[i],Micro_xold2[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele2D, 0.2, beta,loop,Micro_low[i],Micro_upp[i]);
            Micro_change[i]=Micro_change1;
            Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx);
            Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
            Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
        end

        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx, Macro_nelz);
        # PRINT RESULTS
        println("$loop Obj:$(round.(c,digits=2)) Macro_Vol:$(round.(mean(Macro_xPhys[:]),digits=2) ) Micro_Vol:$(round.(mean.(Micro_xPhys[:][:] ),digits=2)) Macro_ch:$(round.(Macro_change,digits=2)) Micro_ch:$(round.(Micro_change,digits=2))")
        if mod(loop,saveItr)==0
            # emp=zeros(size(Macro_xPhys))
            # for i=1:θ
            #     emp[Bool.(Macro_masks[i])].=i
            # end
            # emp1=copy(emp)
            # emp1[Macro_xPhys.<0.7].=0.0
            # scene = Scene(resolution = (400, 400))
            # for i=1:θ
            #     scene= volume!(emp1, algorithm = :iso,colorrange=(0, θ),isorange = 0.1, isovalue = i,colormap=cscheme)
            # end
            # scene = Scene(resolution = (400, 400))
            scene= GLMakie.volume(Macro_xPhys, algorithm = :iso,isorange = 0.3, isovalue = 1.0,colormap=:grays)
            display(scene)

            # save("./img/Wing_Macro_xPhys_$(Macro_Vol)_$(penal)_$(loop)_$(Int(fabric)).png",scene)

            space=fill(0,Micro_nely,1)
            Micro_xPhys_all=space;
            Micro_xPhys_all1=space;
            for i=1:θ
                Micro_xPhys_temp=copy(Micro_xPhys[i])
                Micro_xPhys_temp1=copy(Micro_xPhys[i])
                thres=0.75;
                Micro_xPhys_temp[Micro_xPhys_temp.<thres].=0.0
                Micro_xPhys_temp[Micro_xPhys_temp.>=thres].=i
                Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys_temp);
                Micro_xPhys_all=hcat(Micro_xPhys_all,space);
                Micro_xPhys_all1=hcat(Micro_xPhys_all1,Micro_xPhys_temp1);
                Micro_xPhys_all1=hcat(Micro_xPhys_all1,space);
            end
            display(Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
            display(Plots.heatmap(1.0.-Micro_xPhys_all1,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))
            (Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))


            # for i=1:θ
            #     scene = Scene(resolution = (400, 400))
            #     scene= volume!(Micro_xPhys[i], algorithm = :iso,isorange = 0.4, isovalue = 0.9,colormap=:grays)
            #     display(scene)
            #     save("./img/Micro_xPhys_$(i)_$(Micro_Vol[i])_$(penal)_$(rmin)_$(loop).png",scene)
            # end

        end
        # # UPDATE HEAVISIDE REGULARIZATION PARAMETER
        # if beta < 512 && (loopbeta >= 50 || Macro_change <= 0.01 || Bool.(sum(Micro_change .<= 0.01)) )
        #     beta = 2*beta; loopbeta = 0; Macro_change = 1; Micro_change[:] = .1;
        #     display("Parameter beta increased to $beta.");
        # end
    end
     ##calculate and display properties
     n=3
     for i=1:θ
         Micro_xPhys_temp=copy(Micro_xPhys[i])
         Micro_xPhys_temp[Micro_xPhys_temp.<0.75].=0.0
         Micro_xPhys_temp[Micro_xPhys_temp.>=0.75].=i
         
         display(Plots.heatmap(repeat(Micro_xPhys_temp,n,n),clims=(0, θ),fc=cscheme,aspect_ratio=:equal,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing))
         savefig("./img/finalArrayMicro$(i).png") # Saves the CURRENT_PLOT as a .png
         
         U,S,V = svd(DHs[i]);
         sigma = S;
         k = sum(sigma .> 1e-15);
         SH = (U[:,1:k] * diagm(0=>(1 ./sigma[1:k])) * V[:,1:k]')'; # more stable SVD (pseudo)inverse
         EH = [1/SH[1,1], 1/SH[2,2]]; # effective Young's modulus
         GH = [1/SH[3,3]]; # effective shear modulus
         vH = [-SH[2,1]/SH[1,1],-SH[1,2]/SH[2,2]]; # effective Poisson's ratio
         display( "young:$(round.(EH,digits=3)), poisson:$(round.(vH,digits=3)), shear:$(round.(GH,digits=3))" )
 
         
         # savefig("./img/finalArrayMicro1$(i).png") # Saves the CURRENT_PLOT as a .png
 
     end

    return Macro_xPhys,Micro_xPhys,DHs,a

    
end

##############################################################
# multiple microstructures (θ) based on strain direction
function MultiConTop3DU_new(θ,Macro_struct, Micro_struct,prob, penal,saveItr=5,maxloop = 90,fab=[false,false,false],mgcg=[false,false],voxels=true,concurrent=false)

    # USER-DEFINED LOOP PARAMETERS
    E0 = 1; Emin = 1e-9; nu = 0.3;
    fabric=fab[1];
    discrete=fab[2];
    connect=fab[3];

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2]; Macro_Height = Macro_struct[3];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2]; Micro_Height = Micro_struct[3];
    Macro_nelx   = Int(Macro_struct[4]); Macro_nely  = Int(Macro_struct[5]); Macro_nelz   = Int(Macro_struct[6]);
    Micro_nelx   = Int(Micro_struct[4]); Micro_nely  = Int(Micro_struct[5]); Micro_nelz   = Int(Micro_struct[6]);
    Macro_Vol = Macro_struct[7][2]; 
    Macro_Vol_FM    = Macro_struct[7][1]
    Micro_Vol_FM    = Micro_struct[7][1];
    Micro_rmin=(Micro_struct[8])
    Macro_rmin=(Macro_struct[8])
    Micro_move=Micro_struct[9]


    Macro_Elex   = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely; Macro_Elez = Macro_Height/Macro_nelz;
    Macro_nele = Macro_nelx*Macro_nely*Macro_nelz; Micro_nele = Micro_nelx*Micro_nely*Micro_nelz;
    Macro_ndof = 3*(Macro_nelx+1)*(Macro_nely+1)*(Macro_nelz+1);
    Macro_mgcg=mgcg[1];Micro_mgcg=mgcg[2];

    # PREPARE FINITE ELEMENT ANALYSIS
    U,F,freedofs=prob(Macro_nelx,Macro_nely,Macro_nelz);

    nodenrs = reshape(1:(1+Macro_nelx)*(1+Macro_nely)*(1+Macro_nelz),1+Macro_nely,1+Macro_nelx,1+Macro_nelz)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,Macro_nelx*Macro_nely*Macro_nelz,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1 3*(Macro_nely+1)*(Macro_nelx+1).+[0 1 2 3*Macro_nely.+[3 4 5 0 1 2] -3 -2 -1]],Macro_nelx*Macro_nely*Macro_nelz,1)
    
    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*Macro_nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*Macro_nele,1))
    
    # PREPARE FILTER
    # Macro_H,Macro_Hs = filtering3d(Macro_nelx, Macro_nely, Macro_nelz, Macro_nele, rmin);
    Micro_H,Micro_Hs = make_filter3D(Micro_nelx, Micro_nely, Micro_nelz, Micro_rmin);
    
    
    ##free Material distribution to see how to divide microstructures
    Macro_masks,Micro_Vol,Macro_xPhys_diag,Macro_xPhys_NC=Uclustering3D_new(θ,Macro_struct, Micro_struct,prob)
    if !concurrent
        # println("not concurrent!")
        Macro_xPhys=Macro_xPhys_NC
    end

    #for 3dplotting
    ny=Macro_nely;nx=Macro_nelx;nz=Macro_nelz;count=1;
    xs=ones(nz*ny*nx);ys=ones(nz*ny*nx);zs=ones(nz*ny*nx);clu=ones(nz*ny*nx);
    rect = Rect(Vec(0.0, 0.0,0.0), Vec(1.0, 1.0,1.0));
    mesh = GeometryBasics.mesh(rect);
    for k in 1:nz
        for i in 1:ny
            for j in 1:nx
                if Macro_xPhys[i,j,k]>0.6
                    xs[count]=i;ys[count]=j;zs[count]=k;
                    clu[count]=Macro_xPhys_diag[i,j,k];count+=1;
                end
            end
        end
    end


    # INITIALIZE ITERATION
    Macro_x = ones(Macro_nely,Macro_nelx,Macro_nelz).*Macro_Vol;
    if length(voxels)==1
        voxels=ones(Macro_nely,Macro_nelx,Macro_nelz)
    end
    
    ss=2

    # Micro_x = ones(Micro_nely,Micro_nelx,Micro_nelz);
    Micro_x=[]
    for ii=1:θ
        append!(Micro_x ,[Micro_Vol_FM.*ones(Micro_nely,Micro_nelx,Micro_nelz)]);
        # append!(Micro_x ,[ones(Micro_nely,Micro_nelx,Micro_nelz)]);
        # Micro_x[i][Int(Micro_nely/2)-5:Int(Micro_nely/2)+5,Int(Micro_nelx/2)-5:Int(Micro_nelx/2)+5,Int(Micro_nelz/2)-5:Int(Micro_nelz/2)+5] .= 0;
        # Micro_x[i][Int(Micro_nely/2):Int(Micro_nely/2)+1,Int(Micro_nelx/2):Int(Micro_nelx/2)+1,Int(Micro_nelz/2):Int(Micro_nelz/2)+1] .= 0;
        # Micro_x[i][Int(Micro_nely/2)-2:Int(Micro_nely/2)+2,Int(Micro_nelx/2)-2:Int(Micro_nelx/2)+2,Int(Micro_nelz/2)-2:Int(Micro_nelz/2)+2] .= 0;
        for i = 1:Micro_nelx
            for j = 1:Micro_nely 
                for k = 1:Micro_nelz
                    vall=3
                    if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2+(k-Micro_nelz/2-0.5)^2) < min(min(Micro_nelx,Micro_nely),Micro_nelz)/vall
                        Micro_x[ii][k,j,i] = Micro_Vol_FM/3.0;
                    end
                end
            end
        end
        if fabric
            Micro_x[ii].=addFabricationConstraints(ss,Emin,Micro_x[ii],Micro_nelx,Micro_nely,Micro_nelz,discrete)
        end
    end

    beta = 1;
    Macro_xTilde = Macro_x; 
    # Micro_xTilde = Micro_x;
    # Macro_xPhys = 1 .-exp.(-beta .*Macro_xTilde) .+Macro_xTilde .*exp.(-beta);
    # Micro_xPhys = 1 .-exp.(-beta .*Micro_xTilde) .+Micro_xTilde .*exp.(-beta);
    
    Micro_xTilde=[]
    Micro_xPhys=[];
    Micro_dc = []
    Micro_dv = []
    Micro_change=[]
    DHs=[]
    dDHs=[]
    for i=1:θ
        append!(Micro_xTilde,[Micro_x[i]])
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde[i]) .+ Micro_xTilde[i] * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx, Micro_nelz)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx, Micro_nelz)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM3D(Micro_xPhys[i], Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end

    ### IF MGCG
    if Macro_mgcg
        Macro_nl=4
        Macro_Pu=Array{Any}(undef, Macro_nl-1);
        for l = 1:Macro_nl-1
            Macro_Pu[l,1] = prepcoarse3(Macro_nelz/2^(l-1),Macro_nely/2^(l-1),Macro_nelx/2^(l-1));
        end
        fixeddofs  = setdiff(1:Macro_ndof,freedofs)
        Macro_N = ones(Macro_ndof); Macro_N[fixeddofs] .= 0;
        Macro_Null = spdiagm(Macro_ndof,Macro_ndof,0 => Macro_N);
    end

    # t=:darktest
    # # theme(t);
    # cschemeList=[]
    # append!(cschemeList,[:white])
    # for i=1:θ
    #     append!(cschemeList,[palette(t)[i]])
    # end
    # cscheme=cgrad(cschemeList)
    # t=:darktest #t=:dark
    # # theme(t);
    # cschemeList=[]
    # for i=1:θ
    #     append!(cschemeList,[palette(t)[i]])
    # end
    # cscheme11=cgrad(cschemeList)

    t=:darktest
    cscheme=cgrad(t, θ, categorical = true)
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[cscheme[i]])
    end
    # cscheme=ColorGradient(cschemeList)
    cscheme=cgrad(cschemeList)


    t=:darktest
    cscheme1=cgrad(t, θ, categorical = true)
    cschemeList=[]
    for i=1:θ
        append!(cschemeList,[cscheme1[i]])
    end
    cscheme11=cgrad(cschemeList)


    loopbeta = 0; loop = 0; Macro_change = 1; 
    # Micro_change = 1;
    while loop < maxloop && ( Macro_change > 0.01 || Bool.(sum(Micro_change .>= 0.01)>=1))
        loop = loop+1; loopbeta = loopbeta+1;


        # FE-ANALYSIS AT TWO SCALES
        Kes=zeros(24*24,Macro_nelx*Macro_nely*Macro_nelz)

        Threads.@threads for i=1:θ
            # if fabric
            #     Micro_x[i].=addFabricationConstraints(ss,Emin,Micro_x[i],Micro_nelx,Micro_nely,Micro_nelz)
            #     Micro_xPhys[i].=addFabricationConstraints(ss,Emin,Micro_xPhys[i],Micro_nelx,Micro_nely,Micro_nelz)
            # end
            # scene= GLMakie.volume(Micro_xPhys[i], algorithm = :iso, isorange = 0.2, isovalue = 0.8,colormap=:grays)

            DH, dDH = EBHM3D(Micro_xPhys[i], Micro_length, Micro_width, Micro_Height, E0, Emin, nu, penal,Micro_mgcg);
            DHs[i]=DH;
            dDHs[i]=dDH;
        end

        for i=1:θ
            Kes[:,Bool.(Macro_masks[i][:])].=elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DHs[i])[:]
        end

        ### IF MGCG
        if Macro_mgcg
            K = Array{Any}(undef, Macro_nl);
            sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);

            K[1,1] = sparse(vec(iK),vec(jK),vec(sK));
            K[1,1] = Macro_Null'*K[1,1]*Macro_Null - (Macro_Null .-sparse(1.0I, Macro_ndof, Macro_ndof)); 


            for l = 1:Macro_nl-1
                K[l+1,1] = Macro_Pu[l,1]'*(K[l,1]*Macro_Pu[l,1]);
            end

            Lfac = factorize(Matrix(Hermitian(K[Macro_nl,1]))).L ;Ufac = Lfac';
            cgtol=1e-10;
            cgmax=100;
            cgiters,cgres,U = MGCG(K,F[:,1],U,Lfac,Ufac,Macro_Pu,Macro_nl,1,cgtol,cgmax);

        else
            sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
            # sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),24*24*Macro_nele,1);
            K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
            U[freedofs] = K[freedofs,freedofs]\Array(F[freedofs]);

        end
        
        # Ke = elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH);#old
        
        
        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        # Ke = elementMatVec3D(Macro_Elex/2, Macro_Eley/2, Macro_Elez/2, DH);
        # ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx,Macro_nelz);
        
        #get compliance slow try to make it fast!! ??
        # ce=zeros(Macro_nely*Macro_nelx*Macro_nelz)
        # for i=1:Macro_nely*Macro_nelx*Macro_nelz
        #     ce[i]=sum((U[edofMat]*reshape(Kes[:,i],24,24)).*U[edofMat],dims=2)[i]
        # end

        # UU=[]
        # KKK=[]
        # for i=1:Macro_nely*Macro_nelx*Macro_nelz
        #     append!(UU,[U[edofMat][i,:]'])
        #     append!(KKK,[reshape(Kes[:,i],24,24)])
        # end
        UU=mapslices(x->[x]'[:], U[edofMat], dims=2)[:]
        KKK=mapslices(x->[reshape(x,24,24)], Kes, dims=1)[:]


        ce=sum(vcat((UU.*KKK)...).*U[edofMat],dims=2)
        ce=reshape(ce,Macro_nely,Macro_nelx,Macro_nelz)
        
        
        c = sum(sum(sum((Emin .+Macro_xPhys.^penal*(1-Emin)).*ce)));
        if concurrent
            Macro_dc = -penal*(1-Emin)*Macro_xPhys.^(penal-1).*ce;
            Macro_dv = ones(Macro_nely, Macro_nelx, Macro_nelz);
            # FILTERING AND MODIFICATION OF SENSITIVITIES
            Macro_dx = beta*exp.(-beta.*Macro_xTilde).+exp(-beta);
            Macro_dc[:] = Macro_H*(Macro_dc[:].*Macro_dx[:]./Macro_Hs); 
            Macro_dv[:] = Macro_H*(Macro_dv[:].*Macro_dx[:]./Macro_Hs);

            # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
            Macro_x, Macro_xPhys,Macro_xTilde, Macro_change = OC(Macro_x, Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, 0.1, beta,voxels);
        
        end

        # Micro_dc = zeros(Micro_nely, Micro_nelx, Micro_nelz);

        for i=1:θ
            Micro_dc[i]=zeros(Micro_nely, Micro_nelx, Micro_nelz);
            Micro_dv[i]=ones(Micro_nely, Micro_nelx, Micro_nelz);
        end

        for ii=1:θ
            for i = 1:Micro_nele
                dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i] dDHs[ii][1,4][i] dDHs[ii][1,5][i] dDHs[ii][1,6][i];
                        dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i] dDHs[ii][2,4][i] dDHs[ii][2,5][i] dDHs[ii][2,6][i];
                        dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i] dDHs[ii][3,4][i] dDHs[ii][3,5][i] dDHs[ii][3,6][i];
                        dDHs[ii][4,1][i] dDHs[ii][4,2][i] dDHs[ii][4,3][i] dDHs[ii][4,4][i] dDHs[ii][4,5][i] dDHs[ii][4,6][i];
                        dDHs[ii][5,1][i] dDHs[ii][5,2][i] dDHs[ii][5,3][i] dDHs[ii][5,4][i] dDHs[ii][5,5][i] dDHs[ii][5,6][i];
                        dDHs[ii][6,1][i] dDHs[ii][6,2][i] dDHs[ii][6,3][i] dDHs[ii][6,4][i] dDHs[ii][6,5][i] dDHs[ii][6,6][i]];
                dKE = elementMatVec3D(Macro_Elex, Macro_Eley, Macro_Elez, dDHe);
                dce = sum((U[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                Micro_dc[ii][i] = -sum(sum(sum((Emin .+Macro_xPhys[Bool.(Macro_masks[ii][:])][:].^penal*(1-Emin)).*dce)));
            end
        end
        # Micro_dv = ones(Micro_nely, Micro_nelx, Micro_nelz);
        

        for i=1:θ
            Micro_dx = beta .*exp.(-beta .*Micro_xTilde[i]) .+exp.(-beta);

            if connect
                Q=DHs[i];dQ=dDHs[i];
                # shear
                c=-(DHs[i][4,4]+DHs[i][5,5]+DHs[i][6,6]);
                dc=-(dDHs[i][4,4]+dDHs[i][5,5]+dDHs[i][6,6]);
                # bulk
                # c = -(Q[1,1]+Q[2,2]+Q[3,3]+Q[1,2]+Q[2,3]+Q[1,3]);
                # dc = -(dQ[1,1]+dQ[2,2]+dQ[3,3]+dQ[1,2]+dQ[2,3]+dQ[1,3]);
                Micro_dc[i].+=dc .*50;

            end

            Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
            Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
        end


        # Micro_dx = beta*exp.(-beta.*Micro_xTilde).+exp.(-beta);
        # Micro_dc[:] = Micro_H*(Micro_dc[:].*Micro_dx[:]./Micro_Hs); 
        # Micro_dv[:] = Micro_H*(Micro_dv[:].*Micro_dx[:]./Micro_Hs);

        
        for i=1:θ
            Micro_x[i], Micro_xPhys[i],Micro_xTilde[i], Micro_change1 = OC(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, Micro_move, beta,voxels,fabric,ss);
            Micro_change[i]=Micro_change1;
            Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx,Micro_nelz);
            if fabric
                Micro_x[i].=addFabricationConstraints(ss,Emin,Micro_x[i],Micro_nelx,Micro_nely,Micro_nelz,discrete)
                Micro_xPhys[i].=addFabricationConstraints(ss,Emin,Micro_xPhys[i],Micro_nelx,Micro_nely,Micro_nelz,discrete)
            end
        end
        
        # Micro_x, Micro_xPhys, Micro_change = OC(Micro_x, Micro_dc, Micro_dv, Micro_H, Micro_Hs, Micro_Vol, Micro_nele, 0.02, beta);
        # Micro_xPhys = reshape(Micro_xPhys, Micro_nely, Micro_nelx, Micro_nelz);
        # Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx, Macro_nelz);


        # PRINT RESULTS
        println("$loop Obj:$(round.(c,digits=5)) Macro_Vol:$(round.(mean(Macro_xPhys[:]),digits=2) ) Micro_Vol:$(round.(mean.(Micro_xPhys[:][:] ),digits=2)) Macro_ch:$(round.(Macro_change,digits=2)) Micro_ch:$(round.(Micro_change,digits=2))")
        if mod(loop,saveItr)==0
            # emp=zeros(size(Macro_xPhys))
            # for i=1:θ
            #     emp[Bool.(Macro_masks[i])].=i
            # end
            
            # emp1=copy(emp)
            # emp1[Macro_xPhys.<0.7].=0.0
            # # scene = Scene(resolution = (400, 400))
            # Makie.inline!(true)
            # fig = Figure(resolution=(600, 600), fontsize=10)

            # Makie.volume(emp1, algorithm = :iso,colorrange=(0.0, θ),isorange = 1.0, isovalue = 1,colormap=cscheme)

            # for i=2:θ
            #     Makie.volume!(emp1, algorithm = :iso,colorrange=(0.0, θ),isorange = 1.0, isovalue = i,colormap=cscheme)
            # end
            # display(current_figure())
            # scene= volume(Macro_xPhys, algorithm = :iso,isorange = 0.3, isovalue = 1.0,colormap=:grays)
            if concurrent
                count=1;
                for k in 1:nz
                    for i in 1:ny
                        for j in 1:nx
                            if Macro_xPhys[i,j,k]>0.6
                                xs[count]=i;ys[count]=j;zs[count]=k;
                                clu[count]=Macro_xPhys_diag[i,j,k];count+=1;
                            end
                        end
                    end
                end
            end
            
            Makie.meshscatter(nx.-ys .+1, ny.-xs .+1, zs, markersize = 1, marker=mesh,color = clu,
                transparency=false,opacity=0.8,shading = true,shininess=32.0,colormap=cscheme11,colorrange=(1, θ)
                ,axis = (; type = Axis3, protrusions = (0, 0, 0, 0),
                viewmode = :fit,aspect=:data, limits = (1, nx+1, 1, ny+1, 1, nz+1)))
            display(current_figure())
        
            # save("./img/Macro_xPhys_3_$(Macro_Vol)_$(penal)_$(Macro_rmin)_$(loop).png",scene)
            
            fig = Figure(resolution=(400*θ, 400), fontsize=10)

            for i=1:θ
                temp=copy(Micro_xPhys[i]) .+i .-1
                ax=Axis3(fig[1, i],aspect = :data,viewmode =:fit,perspectiveness = 0.0,textsize = 10)

                scene= GLMakie.volume!(ax,permutedims(temp, [2, 1, 3]),colorrange=(0.0, θ), algorithm = :iso,isorange = 0.5,
                    isovalue = i,colormap=cscheme11)
                
                
            end
            display(fig)
            if discrete
                elevation=50*pi/180
                azimuth=30*pi/180
                fig1 = Figure(resolution=(400*6, 400*θ), fontsize=10)
                for i=1:θ
                    xPhys=copy(Micro_xPhys[i])

                    pieces=[getTriangle(xPhys),
                        getTriangle(mapslices(rotr90,xPhys,dims=[1,3])),
                        getTriangle(mapslices(rotl90,xPhys,dims=[1,3])),
                        getTriangle(mapslices(rot180,xPhys,dims=[1,3])),
                        getTriangle(mapslices(rotr90,xPhys,dims=[2,3])),
                        getTriangle(mapslices(rotl90,xPhys,dims=[2,3]))]
                    for ii=1:6
                        tri=pieces[ii]
                        temp=copy(tri[:,:,1:Int(Micro_nelz/2)+1]) .+i .-1

                        ax=Axis3(fig1[i, ii],aspect = :data,viewmode =:fit,perspectiveness = 0.0,textsize = 10,elevation = elevation, azimuth = azimuth)
                        hidedecorations!(ax)
                        scene= GLMakie.volume!(ax,permutedims(temp, [2, 1, 3]),colorrange=(0.0, θ), algorithm = :iso,isorange = 0.6,
                                isovalue = i,colormap=cscheme11)
                    end

                end
                display(fig1)
            end

        end

        # # UPDATE HEAVISIDE REGULARIZATION PARAMETER
        # if beta < 512 && (loopbeta >= 50 || Macro_change <= 0.01 || Bool.(sum(Micro_change .<= 0.01)) )
        #     beta = 2*beta; loopbeta = 0; Macro_change = 1; Micro_change[:] = .1;
        #     display("Parameter beta increased to $beta.");
        # end
    end
    
    return Macro_xPhys,Micro_xPhys,Macro_masks,Micro_Vol,DHs
end
##############################################################
#======================================================================================================================#
# Main references:                                                                                                     #
#                                                                                                                      #
# (1) Jie Gao, Zhen Luo, Liang Xia, Liang Gao. Concurrent topology optimization of multiscale composite structures     #
# in Matlab. Accepted in Structural and multidisciplinary optimization.                                                #
#                                                                                                                      #
# (2) Xia L, Breitkopf P. Design of materials using topology optimization and energy-based homogenization approach in  #
# Matlab. # Structural and multidisciplinary optimization, 2015, 52(6): 1229-1241.                                     #
#                                                                                                                      #                                                                   #
#======================================================================================================================#
