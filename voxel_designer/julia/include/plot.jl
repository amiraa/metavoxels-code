# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2020

#####################################Ployt####################################################################
function topplot3d(xPhys)
    ix=[]
    iy=[]
    iz=[]
    for j in 1:nely
        for i in 1:nelx
            for k in 1:nelz
                if(xPhys[j,i,k]>0.0)
                    append!(ix,i)
                    append!(iy,j)
                    append!(iz,k)
                end
            end
        end
    end
    # r = 4.0
    # lim = FRect3D((-4,-4,-4*r),(8,8,8*r))
    return Plots.scatter(ix,iz,iy,color="black",label="",markersize =4, aspect_ratio=:equal,markerstrokealpha = 0.2,markeralpha = 0.6,markershape = :square,camera = (30, 60),xlim=(0,nelx),zlim=(0,nely),ylim=(-10,10))#,markershape = :square
end

function plotDisplacement(nelx,nely,nelz,xPhysC,getProblem=inverter,factor=0.04,cameraX=30,cameraY=60)
    U,ndof,freedofs=getDisplacement1(xPhysC,nelx,nely,nelz,getProblem)
    displacement=reshape((U[:,1]),3,(nely+1),(nelx+1),(nelz+1));

    anim1=Animation()
    for step in 0:10
        ix=[]
        iy=[]
        iz=[]
        exg=factor*step
        for j in 1:nely
            for i in 1:nelx
                for k in 1:nelz
                    if (xPhysC[j,i,k]>0.5)
                        append!(ix,i+exg*(displacement[1,j,i,k]))
                        append!(iy,j+exg*(displacement[2,j,i,k]))
                        append!(iz,k+exg*(displacement[3,j,i,k]))
                    end
                end
            end
        end
        # r = 4.0
        # lim = FRect3D((-4,-4,-4*r),(8,8,8*r))
        Plots.scatter(ix,iz,iy,color="black",label="",markersize =4, aspect_ratio=:equal,markerstrokealpha = 0.2,markeralpha = 0.6,markershape = :square,camera = (cameraX,cameraY),xlim=(0,nelx*1.5),zlim=(-0.5*nely,nely*1.5),ylim=(-10,10))#,markershape = :square
        frame(anim1)
    end
    return anim1
    
end

function plotDisplacement3D(nelx,nely,nelz,prob,volfrac,rmin,penal,factor=0.04,cameraX=30,cameraY=60)

    # Max and min stiffness
    Emin=1e-9
    Emax=1.0
    nu=0.3

    border=4

    ndof = 3*(nelx+1)*(nely+1)*(nelz+1)
    # Allocate design variables (as array), initialize and allocate sens.
    x= ones(Float64,nely,nelx,nelz)
    xold=copy(x)
    xPhys=copy(x)
    g=0 # must be initialized to use the NGuyen/Paulino OC approach
    dc=zeros(Float64,(nely,nelx,nelz))

    # FE: Build the index vectors for the for coo matrix format.
    KE=lk_H8(nu)
    nele = nelx*nely*nelz

    nodenrs = reshape(1:(1+nelx)*(1+nely)*(1+nelz),1+nely,1+nelx,1+nelz)
    edofVec = reshape(3*nodenrs[1:end-1,1:end-1,1:end-1].+1,nelx*nely*nelz,1)
    edofMat = repeat(edofVec,1,24).+repeat([0 1 2 3*nely.+[3 4 5 0 1 2] -3 -2 -1 3*(nely+1)*(nelx+1).+[0 1 2 3*nely.+[3 4 5 0 1 2] -3 -2 -1]],nelx*nely*nelz,1)

    iK = convert(Array{Int},reshape(kron(edofMat,ones(24,1))',24*24*nele,1))
    jK = convert(Array{Int},reshape(kron(edofMat,ones(1,24))',24*24*nele,1))


    U,F,freedofs=prob(nelx,nely,nelz)

    # FE-ANALYSIS
    sK = reshape(KE[:]*(Emin.+xPhys[:]'.^penal*(Emax-Emin)),24*24*nelx*nely*nelz,1)
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2
    @timed U[freedofs] = K[freedofs,freedofs] \ Array(F[freedofs])
    # Objective function and sensitivity analysis
    ce = reshape(sum((U[edofMat]*KE).*U[edofMat],dims=2),nely,nelx,nelz)
    c = sum(sum(sum((Emin.+xPhys.^penal*(Emax-Emin)).*ce)))
    Umat=U[edofMat]; 
    ord=reshape(1:(nely*nelx*nelz),nely,nelx,nelz);

    gr(size=(800,600))


    function getYX3(el3D)
        el3D=reshape(el3D,3,8)'
        Ue1=[el3D[1,:] el3D[2,:] el3D[3,:] el3D[4,:]]
        Ue2=[el3D[5,:] el3D[6,:] el3D[7,:] el3D[8,:]]
        return reshape(Ue1,3,4)',reshape(Ue2,3,4)'
    end
    # cameraX=60 # (azimuthal, elevation)
    # cameraY=30

    # border=4

    # cameraX=90 # (azimuthal,
    # cameraY=0 #  elevation)
    anim1=Animation()
    gr(size=(600,600))
    ix=[];iy=[];iz=[];
    for step in 0:0.05:1
        ix=[];iy=[];iz=[];
        exg=factor*step
        for k in 1:nelz
            for i in 1:nely
                for j in 1:nelx
                    Ue1,Ue2=getYX3(Umat[ord[i,j,k],:])
                    append!(iy,-i+exg*Ue1[4,2]);    append!(ix,j+exg*Ue1[4,1]);append!(iz,k+exg*Ue1[4,3]);
                    append!(iy,-(i+1)+exg*Ue1[2,2]);append!(ix,(j+1)+exg*Ue1[2,1]);append!(iz,k+exg*Ue1[2,3]);
                    append!(iy,-(i)+exg*Ue1[3,2]);  append!(ix,(j+1)+exg*Ue1[3,1]);append!(iz,k+exg*Ue1[3,3]);
                    append!(iy,-(i+1)+exg*Ue1[1,2]);append!(ix,(j)+exg*Ue1[1,1]);append!(iz,k+exg*Ue1[1,3]);

                    append!(iy,-i+exg*Ue2[4,2]);    append!(ix,j+exg*Ue2[4,1]);append!(iz,(k+1)+exg*Ue2[4,3]);
                    append!(iy,-(i+1)+exg*Ue2[2,2]);append!(ix,(j+1)+exg*Ue2[2,1]);append!(iz,(k+1)+exg*Ue2[2,3]);
                    append!(iy,-(i)+exg*Ue2[3,2]);  append!(ix,(j+1)+exg*Ue2[3,1]);append!(iz,(k+1)+exg*Ue2[3,3]);
                    append!(iy,-(i+1)+exg*Ue2[1,2]);append!(ix,(j)+exg*Ue2[1,1]);append!(iz,(k+1)+exg*Ue2[1,3]);

                end
            end
        end

        Plots.scatter(iy,ix,iz,color="black",label="",markersize =4, yaxis ="xxx", xaxis ="yyy", zaxis ="zzz",markerstrokealpha = 0.2,markeralpha = 0.2,markershape = :square,camera = (cameraX,cameraY),xlim=(-border-nely,border),ylim=(-border,nelx+border),zlim=(-border,nelz+border))#,markershape = :square
        frame(anim1)
    end
    display("here")
    return anim1
    gif(anim1, "anim.gif", fps = 10)
end

function make_bitmap(p,nx,ny,alpha)
    color = [1 0 0; 0 0 .45; 0 1 0; 0 0 0; 1 1 1];
    I = zeros(nx*ny,3);
    for j = 1:p
        I[:,1:3] = I[:,1:3] + alpha[:,j] .*color[j,1:3]';
    end
    # I = imresize(reshape(I,ny,nx,3),10,'bilinear');
    I=reshape(I,ny,nx,3)
    II=hcat(I[:,end:-1:1,:],I)
    return II
end

function make_bitmap_compliant(p,nx,ny,alpha)
    color = [1 0 0; 0 0 .45; 0 1 0; 0 0 0; 1 1 1];
    I = zeros(nx*ny,3);
    for j = 1:p
        I[:,1:3] = I[:,1:3] + alpha[:,j] .*color[j,1:3]';
    end
    # I = imresize(reshape(I,ny,nx,3),10,'bilinear');
    I=reshape(I,ny,nx,3)
    # II=hcat(I[:,end:-1:1,:],I)
    II=vcat(I[end:-1:1,:,:],I)
    return II
end

function make_bitmap_3d(p,nx,ny,nz,alpha)
    color = [1 0 0; 0 0 1; 0 1 0; 0 0 0; 1 1 1];
    I = zeros(nx*ny*nz,3);
    for j = 1:p
        I[:,1:3] = I[:,1:3] + alpha[:,j] .*color[j,1:3]';
    end
    # I = imresize(reshape(I,ny,nx,3),10,'bilinear');
    I=reshape(I,ny,nx,nz,3)
    II=hcat(I[:,end:-1:1,:,:],I)
    return II
end

function topplotmulti3d(nelx,nely,nelz,I,threshold)
    ixr=[]
    iyr=[]
    izr=[]
    
    ixg=[]
    iyg=[]
    izg=[]
    
    ixb=[]
    iyb=[]
    izb=[]
    for j in 1:nely
        for i in 1:nelx
            for k in 1:nelz
                if(I[j,i,k,1]>threshold)
                    append!(ixr,i)
                    append!(iyr,j)
                    append!(izr,k)
                end
                if(I[j,i,k,2]>threshold)
                    append!(ixg,i)
                    append!(iyg,j)
                    append!(izg,k)
                end
                if(I[j,i,k,3]>threshold)
                    append!(ixb,i)
                    append!(iyb,j)
                    append!(izb,k)
                end
            end
        end
    end
    p=Plots.scatter(ixr,izr,iyr,color="red",label="",markersize =4, aspect_ratio=:equal,markerstrokealpha = 0.2,markeralpha = 0.6,markershape = :square,camera = (30, 60),xlim=(0,nelx),zlim=(0,nely),ylim=(-10,10))#,markershape = :square
    p=Plots.scatter!(ixg,izg,iyg,color="green",label="",markersize =4, aspect_ratio=:equal,markerstrokealpha = 0.2,markeralpha = 0.6,markershape = :square,camera = (30, 60),xlim=(0,nelx),zlim=(0,nely),ylim=(-10,10))#,markershape = :square
    p=Plots.scatter!(ixb,izb,iyb,color="blue",label="",markersize =4, aspect_ratio=:equal,markerstrokealpha = 0.2,markeralpha = 0.6,markershape = :square,camera = (30, 60),xlim=(0,nelx),zlim=(0,nely),ylim=(-10,10))#,markershape = :square
    return p

end

function topplotmulti3d_mirror(nelx,nely,nelz,I,threshold,vertical)
    ixr=[]
    iyr=[]
    izr=[]
    
    ixg=[]
    iyg=[]
    izg=[]
    
    ixb=[]
    iyb=[]
    izb=[]
    if(vertical)
        for j in 1:nely
            for i in 1:nelx
                for k in 1:nelz
                    if(I[j,i,k,1]>threshold)
                        append!(ixr,i)
                        append!(ixr,i)
                        append!(iyr,nely-j)
                        append!(iyr,j+nely-1)
                        append!(izr,k)
                        append!(izr,k)
                    end
                    if(I[j,i,k,2]>threshold)
                        append!(ixg,i)
                        append!(ixg,i)
                        append!(iyg,nely-j)
                        append!(iyg,j+nely-1)
                        append!(izg,k)
                        append!(izg,k)
                    end
                    if(I[j,i,k,3]>threshold)
                        append!(ixb,i)
                        append!(ixb,i)
                        append!(iyb,nely-j)
                        append!(iyb,j+nely-1)
                        append!(izb,k)
                        append!(izb,k)
                    end
                end
            end
        end
    else
        for j in 1:nely
            for i in 1:nelx
                for k in 1:nelz
                    if(I[j,i,k,1]>threshold)
                        append!(ixr,nelx-i+nelx-1)
                        append!(ixr,i)
                        append!(iyr,j)
                        append!(izr,k)
                        append!(iyr,j)
                        append!(izr,k)
                    end
                    if(I[j,i,k,2]>threshold)
                        append!(ixg,nelx-i+nelx-1)
                        append!(ixg,i)
                        append!(iyg,j)
                        append!(izg,k)
                        append!(iyg,j)
                        append!(izg,k)
                    end
                    if(I[j,i,k,3]>threshold)
                        append!(ixb,nelx-i+nelx-1)
                        append!(ixb,i)
                        append!(iyb,j)
                        append!(izb,k)
                        append!(iyb,j)
                        append!(izb,k)
                        
                    end
                end
            end
        end
    end
    p=Plots.scatter(ixr,izr,iyr,color="red",label="",markersize =4, aspect_ratio=:equal,markerstrokealpha = 0.2,markeralpha = 0.6,markershape = :square,camera = (30, 60),xlim=(0,nelx),zlim=(0,nely),ylim=(-10,10))#,markershape = :square
    p=Plots.scatter!(ixg,izg,iyg,color="green",label="",markersize =4, aspect_ratio=:equal,markerstrokealpha = 0.2,markeralpha = 0.6,markershape = :square,camera = (30, 60),xlim=(0,nelx),zlim=(0,nely),ylim=(-10,10))#,markershape = :square
    p=Plots.scatter!(ixb,izb,iyb,color="blue",label="",markersize =4, aspect_ratio=:equal,markerstrokealpha = 0.2,markeralpha = 0.6,markershape = :square,camera = (30, 60),xlim=(0,nelx),zlim=(0,nely),ylim=(-10,10))#,markershape = :square
    return p

end

function plotBoundaryConditions2D(nelx,nely,prob)
    gr(size=(200,200))
    U,F,freedofs=prob(nelx,nely)
    FF=reshape(F,2,nely+1,nelx+1)
    display("Forces:")
    display(Plots.heatmap(FF[1,:,:], aspect_ratio=:equal))
    display(Plots.heatmap(FF[2,:,:], aspect_ratio=:equal))
    U.=-1
    U[freedofs].=1.0
    UU= reshape(U,2,nely+1,nelx+1)
    display("Supports:")
    display(Plots.heatmap(UU[1,:,:], aspect_ratio=:equal))
    display(Plots.heatmap(UU[2,:,:], aspect_ratio=:equal))
    gr(size=(400,400))
end

function plotBoundaryConditions2DCompliant(nelx,nely,prob)
    gr(size=(200,200))
    U,F,freedofs,DofDOut,S=prob(nelx,nely);
    FF1=reshape(F[:,1],2,nely+1,nelx+1)
    FF2=reshape(F[:,2],2,nely+1,nelx+1)
    display("Force in:")
    display(Plots.heatmap(FF1[1,:,:], aspect_ratio=:equal))
    display(Plots.heatmap(FF1[2,:,:], aspect_ratio=:equal))
    display("Force out:")
    display(Plots.heatmap(FF2[1,:,:], aspect_ratio=:equal))
    display(Plots.heatmap(FF2[2,:,:], aspect_ratio=:equal))
    U=U[:,1].-1
    U[freedofs].=1.0
    UU= reshape(U,2,nely+1,nelx+1)
    display("Supports:")
    display(Plots.heatmap(UU[1,:,:], aspect_ratio=:equal))
    display(Plots.heatmap(UU[2,:,:], aspect_ratio=:equal))
    gr(size=(400,400))
end

function plotBoundaryConditions3D(nelx,nely,nelz,U,F,freedofs,z=1)
    display("Supports")
    UU=U[:,1]
    UU[freedofs].=1.0
    UUU=reshape(UU,3,nely+1,nelx+1,nelz+1)[1,:,:,z]
    display(Plots.heatmap(UUU,aspect_ratio=:equal))
    UUU=reshape(UU,3,nely+1,nelx+1,nelz+1)[2,:,:,z]
    display(Plots.heatmap(UUU,aspect_ratio=:equal))
    UUU=reshape(UU,3,nely+1,nelx+1,nelz+1)[3,:,:,z]
    display(Plots.heatmap(UUU,aspect_ratio=:equal))

    if size(F)[2]>1
        UU=U[:,2]
        UU[freedofs].=1.0
        UUU=reshape(UU,3,nely+1,nelx+1,nelz+1)[1,:,:,z]
        display(Plots.heatmap(UUU,aspect_ratio=:equal))
        UUU=reshape(UU,3,nely+1,nelx+1,nelz+1)[2,:,:,z]
        display(Plots.heatmap(UUU,aspect_ratio=:equal))
        UUU=reshape(UU,3,nely+1,nelx+1,nelz+1)[3,:,:,z]
        display(Plots.heatmap(UUU,aspect_ratio=:equal))
    end

    
    display("Forces")
    display("din")
    FF=F[:,1]
    FFF=Array(reshape(FF,3,nely+1,nelx+1,nelz+1)[1,:,:,z])
    display(Plots.heatmap(FFF,aspect_ratio=:equal))
    FFF=Array(reshape(FF,3,nely+1,nelx+1,nelz+1)[2,:,:,z])
    display(Plots.heatmap(FFF,aspect_ratio=:equal))
    FFF=Array(reshape(FF,3,nely+1,nelx+1,nelz+1)[3,:,:,z])
    display(Plots.heatmap(FFF,aspect_ratio=:equal))
    if size(F)[2]>1
        display("dout")
        FF=F[:,2]
        FFF=Array(reshape(FF,3,nely+1,nelx+1,nelz+1)[1,:,:,z])
        display(Plots.heatmap(FFF,aspect_ratio=:equal))
        FFF=Array(reshape(FF,3,nely+1,nelx+1,nelz+1)[2,:,:,z])
        display(Plots.heatmap(FFF,aspect_ratio=:equal))
        FFF=Array(reshape(FF,3,nely+1,nelx+1,nelz+1)[3,:,:,z])
        display(Plots.heatmap(FFF,aspect_ratio=:equal))
    end
end

function topplot3d(xPhys,nelx,nely,nelz)
    ix=[]
    iy=[]
    iz=[]
    for j in 1:Int(nely/1)
        for i in 1:Int(nelx/1)
            for k in 1:Int(nelz/1)
                if(xPhys[j,i,k]>0.9)
                    append!(ix,i)
                    append!(iy,j)
                    append!(iz,k)
                end
            end
        end
    end
    # r = 4.0
    # lim = FRect3D((-4,-4,-4*r),(8,8,8*r))
    return Plots.scatter(ix,iz,iy,color="black",label="",markersize =4, aspect_ratio=:equal,markerstrokealpha = 1.0,markeralpha = 1.0,markershape = :square,camera = (30, 60),xlim=(0,nelx),zlim=(0,nelz),ylim=(0,nely))#,markershape = :square
end

function dispAnimation2DCompliant(prob,Macro_xPhys)
    Macro_nelx=size(Macro_xPhys)[2]
    Macro_nely=size(Macro_xPhys)[1]
    Emin=1e-9
    Macro_Elex = 12/120;
    Macro_nele = Int(Macro_nelx*Macro_nely);   
    # Micro_nele = Int(Micro_nelx*Micro_nely);
    Macro_ndof = Int(2*(Macro_nelx+1)*(Macro_nely+1));

    # PREPARE FINITE ELEMENT ANALYSIS
    nodenrs = reshape(1:(Macro_nely+1)*(Macro_nelx+1),1+Macro_nely,1+Macro_nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,Macro_nele,1);
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*Macro_nely.+[2 3 0 1] -2 -1],Macro_nele,1)


    ##########################
    # DEFINE LOADS AND SUPPORTS 
    U,F,freedofs,DofDOut,S=prob(Macro_nelx,Macro_nely)


    #########################

    iK = reshape(kron(edofMat,ones(8,1))',64*Macro_nele,1);
    jK = reshape(kron(edofMat,ones(1,8))',64*Macro_nele,1);


    # FE-ANALYSIS AT TWO SCALES
    KE=lk()

    sK = reshape(KE[:]*(Emin.+xPhys[:]'.^penal*(1.0-Emin)),64*nelx*nely,1)

    K = sparse(vec(iK),vec(jK),vec(sK))+S; K = (K+K')/2;
    U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
    U1=U[:,1];
    U2=U[:,2];
    UU1=reshape(U1[edofMat],Macro_nely,Macro_nelx,8);
    UU2=reshape(U2[edofMat],Macro_nely,Macro_nelx,8);
    # UU1=reshape(U1[edofMat],8,Macro_nely,Macro_nelx);
    # UU2=reshape(U2[edofMat],8,Macro_nely,Macro_nelx);
    
    f=copy(Macro_xPhys)
    f[f.<0.95].=0
    f[f.>=0.95].=1
    CI=findall(x->x>0.99, f)
    CI=findall(Bool.(f))
    xs=[ i[1] for i in CI ];
    ys=[ i[2] for i in CI ];

    function getDisY1(x,y,scale)
        return y.+UU1[x,y,1]*scale
    end

    function getDisX1(x,y,scale)
        return x.+UU1[x,y,2]*scale
    end

    function getDisY2(x,y,scale)
        return y.+UU2[x,y,1]*scale
    end

    function getDisX2(x,y,scale)
        return x.+UU2[x,y,2]*scale
    end
    dispAnim=Animation()
    for scale=1:20
    #     display(Plots.scatter(ys,xs,label="",markersize =1, aspect_ratio=:equal))
        (Plots.scatter(getDisY2.(xs,ys,scale),getDisX2.(xs,ys,scale),label="",markersize =1, aspect_ratio=:equal,xlim=(1-10,Macro_nelx+10),ylim=(1-10,Macro_nely+10)))
        frame(dispAnim)
    end
    display(gif(dispAnim ,fps=10))

    dispAnim=Animation()
    for scale=1:20
    #     display(Plots.scatter(ys,xs,label="",markersize =1, aspect_ratio=:equal))
        (Plots.scatter(getDisY1.(xs,ys,scale),getDisX1.(xs,ys,scale),label="",markersize =1, aspect_ratio=:equal,xlim=(1-10,Macro_nelx+10),ylim=(1-10,Macro_nely+10)))
        frame(dispAnim)
    end
    display(gif(dispAnim ,fps=10))
    return dispAnim
end

function dispAnimation2DCompliant(prob,Macro_xPhys,disScale=1)
    Macro_nelx=size(Macro_xPhys)[2]
    Macro_nely=size(Macro_xPhys)[1]
    Emin=1e-9
    Macro_Elex = 12/120;
    Macro_Eley = 12/120;
    Macro_nele = Int(Macro_nelx*Macro_nely);   
    # Micro_nele = Int(Micro_nelx*Micro_nely);
    Macro_ndof = Int(2*(Macro_nelx+1)*(Macro_nely+1));

    # PREPARE FINITE ELEMENT ANALYSIS
    nodenrs = reshape(1:(Macro_nely+1)*(Macro_nelx+1),1+Macro_nely,1+Macro_nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,Macro_nele,1);
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*Macro_nely.+[2 3 0 1] -2 -1],Macro_nele,1)


    ##########################
    # DEFINE LOADS AND SUPPORTS 
    U,F,freedofs,DofDOut,S=prob(Macro_nelx,Macro_nely)

    #########################

    iK = reshape(kron(edofMat,ones(8,1))',64*Macro_nele,1);
    jK = reshape(kron(edofMat,ones(1,8))',64*Macro_nele,1);
    
    Ke=lk()

    sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
    U[freedofs,1] = K[freedofs,freedofs]\Array(F[freedofs,1]);
    UU=reshape(U[edofMat,1],Macro_nely,Macro_nelx,8);
    
    
    f=copy(Macro_xPhys)
    threshold=0.5
    f[f.<threshold].=0
    f[f.>=threshold].=1
    CI=findall(x->x>threshold, f)
    CI=findall(Bool.(f))
    xs=[ i[1] for i in CI ];
    ys=[ i[2] for i in CI ];
    

    function getDisY(x,y,scale)
        return y.+UU[x,y,1]*scale*disScale
    end

    function getDisX(x,y,scale)
        return x.+UU[x,y,2]*scale*disScale
    end
    
    ###
    dispAnim=Animation()
    for scale=0:10
        (Plots.scatter(getDisY.(xs,ys,scale),getDisX.(xs,ys,scale),label="",markersize =1, aspect_ratio=:equal,xlim=(1-10,Macro_nelx+10),ylim=(1-10,Macro_nely+10)))
        frame(dispAnim)
    end
    display(gif(dispAnim ,fps=5))
    
    ###

    
    return dispAnim
end


function dispAnimation2DCompliant1(prob,Macro_xPhys,DHs)
    Macro_nelx=size(Macro_xPhys)[2]
    Macro_nely=size(Macro_xPhys)[1]
    Emin=1e-9
    Macro_Elex = 12/120;
    Macro_nele = Int(Macro_nelx*Macro_nely);   
    # Micro_nele = Int(Micro_nelx*Micro_nely);
    Macro_ndof = Int(2*(Macro_nelx+1)*(Macro_nely+1));

    # PREPARE FINITE ELEMENT ANALYSIS
    nodenrs = reshape(1:(Macro_nely+1)*(Macro_nelx+1),1+Macro_nely,1+Macro_nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,Macro_nele,1);
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*Macro_nely.+[2 3 0 1] -2 -1],Macro_nele,1)


    ##########################
    # DEFINE LOADS AND SUPPORTS 
    U,F,freedofs,DofDOut,S=prob(Macro_nelx,Macro_nely)


    #########################

    iK = reshape(kron(edofMat,ones(8,1))',64*Macro_nele,1);
    jK = reshape(kron(edofMat,ones(1,8))',64*Macro_nele,1);


    # FE-ANALYSIS AT TWO SCALES
    Kes=zeros(8*8,Macro_nelx*Macro_nely)

    for i=1:θ
        Kes[:,Bool.(Macro_masks[i][:])].=elementMatVec2D(Macro_Elex/2, Macro_Elex/2, DHs[i])[:]
    end

    sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK))+S; K = (K+K')/2;
    U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
    U1=U[:,1];
    U2=U[:,2];
    UU1=reshape(U1[edofMat],Macro_nely,Macro_nelx,8);
    UU2=reshape(U2[edofMat],Macro_nely,Macro_nelx,8);
    # UU1=reshape(U1[edofMat],8,Macro_nely,Macro_nelx);
    # UU2=reshape(U2[edofMat],8,Macro_nely,Macro_nelx);
    
    f=copy(Macro_xPhys)
    f[f.<0.95].=0
    f[f.>=0.95].=1
    CI=findall(x->x>0.99, f)
    CI=findall(Bool.(f))
    xs=[ i[1] for i in CI ];
    ys=[ i[2] for i in CI ];

    function getDisY1(x,y,scale)
        return y.+UU1[x,y,1]*scale
    end

    function getDisX1(x,y,scale)
        return x.+UU1[x,y,2]*scale
    end

    function getDisY2(x,y,scale)
        return y.+UU2[x,y,1]*scale
    end

    function getDisX2(x,y,scale)
        return x.+UU2[x,y,2]*scale
    end
    dispAnim=Animation()
    for scale=1:10
    #     display(Plots.scatter(ys,xs,label="",markersize =1, aspect_ratio=:equal))
        (Plots.scatter(getDisY1.(xs,ys,scale),getDisX1.(xs,ys,scale),label="",markersize =1, aspect_ratio=:equal,xlim=(1-10,Macro_nelx+10),ylim=(1-10,Macro_nely+10)))
        frame(dispAnim)
    end
    display(gif(dispAnim ,fps=10))
    return dispAnim
end

function dispAnimation2D(prob,Macro_xPhys,Micro_xPhys,DH,disScale=1)
    
    ############visualize input microstructure############
    Micro_nelx=size(Micro_xPhys)[2];Micro_nely=size(Micro_xPhys)[1];
    
    space=fill(0,Micro_nely,1)
    Micro_xPhys_all=space;
    Micro_xPhys_all1=space;

    thres=0.75;
    Micro_xPhys_temp=copy(Micro_xPhys)
    Micro_xPhys_temp[Micro_xPhys_temp.<thres].=0.0
    Micro_xPhys_temp[Micro_xPhys_temp.>=thres].=1
    Micro_xPhys_temp1=copy(Micro_xPhys)
    Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys_temp);
    Micro_xPhys_all=hcat(Micro_xPhys_all,space);
    Micro_xPhys_all1=hcat(Micro_xPhys_all1,Micro_xPhys_temp1);
    Micro_xPhys_all1=hcat(Micro_xPhys_all1,space);
    
    display(Plots.heatmap(1.0.-Micro_xPhys_all1,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:grays,aspect_ratio=:equal))

    ##calculate and display properties
    n=3
    Micro_xPhys_temp=copy(Micro_xPhys)
    Micro_xPhys_temp[Micro_xPhys_temp.<0.75].=0.0
    Micro_xPhys_temp[Micro_xPhys_temp.>=0.75].=1

    display(Plots.heatmap(repeat(1.0.-Micro_xPhys_temp,n,n),clims=(0, 1),fc=:grays,aspect_ratio=:equal,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing))

    display(DH)
    U,S,V = svd(DH);
    sigma = S;
    k = sum(sigma .> 1e-15);
    SH = (U[:,1:k] * diagm(0=>(1 ./sigma[1:k])) * V[:,1:k]')'; # more stable SVD (pseudo)inverse
    EH = [1/SH[1,1], 1/SH[2,2]]; # effective Young's modulus
    GH = [1/SH[3,3]]; # effective shear modulus
    vH = [-SH[2,1]/SH[1,1],-SH[1,2]/SH[2,2]]; # effective Poisson's ratio
    display( "young:$(round.(EH,digits=3)), poisson:$(round.(vH,digits=3)), shear:$(round.(GH,digits=3))" )
  

    ############################################
    
    
    Macro_nelx=size(Macro_xPhys)[2]
    Macro_nely=size(Macro_xPhys)[1]
    Emin=1e-9
    Macro_Elex = 12/120;
    Macro_Eley = 12/120;
    Macro_nele = Int(Macro_nelx*Macro_nely);   
    # Micro_nele = Int(Micro_nelx*Micro_nely);
    Macro_ndof = Int(2*(Macro_nelx+1)*(Macro_nely+1));

    # PREPARE FINITE ELEMENT ANALYSIS
    nodenrs = reshape(1:(Macro_nely+1)*(Macro_nelx+1),1+Macro_nely,1+Macro_nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,Macro_nele,1);
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*Macro_nely.+[2 3 0 1] -2 -1],Macro_nele,1)


    ##########################
    # DEFINE LOADS AND SUPPORTS 
    U,F,freedofs=prob(Macro_nelx,Macro_nely)
    U_reg=copy(U)

    #########################

    iK = reshape(kron(edofMat,ones(8,1))',64*Macro_nele,1);
    jK = reshape(kron(edofMat,ones(1,8))',64*Macro_nele,1);

    # FE-ANALYSIS 
    Ke = elementMatVec2D(Macro_Elex/2, Macro_Eley/2, DH);
    sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
    U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
    UU=reshape(U[edofMat],Macro_nely,Macro_nelx,8);
    
    Ke=lk()
    # Ke=lk(EH[1],vH[1])
    # Ke=lk(EH[1])
    sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
    K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
    U_reg[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
    UU_reg=reshape(U_reg[edofMat],Macro_nely,Macro_nelx,8);
    
    
    f=copy(Macro_xPhys)
    threshold=0.5
    f[f.<threshold].=0
    f[f.>=threshold].=1
    CI=findall(x->x>threshold, f)
    CI=findall(Bool.(f))
    xs=[ i[1] for i in CI ];
    ys=[ i[2] for i in CI ];
    

    function getDisY(x,y,scale)
        return y.+UU[x,y,1]*scale*disScale
    end

    function getDisX(x,y,scale)
        return x.+UU[x,y,2]*scale*disScale
    end
    
    function getDisY_reg(x,y,scale)
        return y.+UU_reg[x,y,1]*scale*disScale
    end

    function getDisX_reg(x,y,scale)
        return x.+UU_reg[x,y,2]*scale*disScale
    end

    
    ##
    dispAnim=Animation()
    for scale=0:10
        (Plots.scatter(getDisY_reg.(xs,ys,scale),getDisX_reg.(xs,ys,scale),label="",markersize =1, aspect_ratio=:equal,xlim=(1-10,Macro_nelx+10),ylim=(1-10,Macro_nely+10)))
        frame(dispAnim)
    end
    display(gif(dispAnim ,fps=5))
    
    ###
    dispAnim=Animation()
    for scale=0:10
        (Plots.scatter(getDisY.(xs,ys,scale),getDisX.(xs,ys,scale),label="",markersize =1, aspect_ratio=:equal,xlim=(1-10,Macro_nelx+10),ylim=(1-10,Macro_nely+10)))
        frame(dispAnim)
    end
    display(gif(dispAnim ,fps=5))
    
    ###

    
    return dispAnim
end

