#======================================================================================================================#
# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2021
#======================================================================================================================#

# using Clustering
# using StatsPlots
# using ParallelKMeans
# using Distances
# gr(size=(400,300))
# one microstructure
function ConTop2D(Macro_struct, Micro_struct, penal, rmin,saveItr=5)
    ## USER-DEFINED LOOP PARAMETERS
    maxloop = 200; E0 = 1; Emin = 1e-9; nu = 0.3;

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2];
    Macro_nelx   = Int(Macro_struct[3]); Macro_nely  = Int(Macro_struct[4]);
    Micro_nelx   = Int(Micro_struct[3]); Micro_nely  = Int(Micro_struct[4]);
    Macro_Vol    = Macro_struct[5]; Micro_Vol   = Micro_struct[5];

    Macro_Elex = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely;
    Macro_nele = Int(Macro_nelx*Macro_nely);   Micro_nele = Int(Micro_nelx*Micro_nely);
    Macro_ndof = Int(2*(Macro_nelx+1)*(Macro_nely+1));

    # PREPARE FINITE ELEMENT ANALYSIS

    # load_x,load_y,load_z = Matlab.meshgrid([Macro.nelx], [Macro.nely/2], 0:Macro.nelz);
    # loadnid =load_z*(Macro.nelx+1)*(Macro.nely+1)+load_x*(Macro.nely+1)+(Macro.nely+1-load_y);
    # F = sparse(3*loadnid(:) - 1,1,-1,Macro.ndof,1);
    # U = zeros(Macro.ndof,1);
    # fixed_x,fixed_y,fixed_z = Matlab.meshgrid([0],0:Macro.nely,0:Macro.nelz);
    # fixednid =fixed_z*(Macro.nelx+1)*(Macro.nely+1)+fixed_x*(Macro.nely+1)+(Macro.nely+1-fixed_y);
    # fixeddofs = [3*fixednid(:); 3*fixednid(:)-1; 3*fixednid(:)-2];
    # freedofs = setdiff(1:Macro_ndof,fixeddofs);


    #load case 2
    # load_x, load_y = Matlab.meshgrid([Macro_nelx/2], [0]);
    # fixed_x, fixed_y = Matlab.meshgrid([0 ;Macro_nelx], [0]);
    # fixeddofs = vcat(2 .*fixednid[:], 2 .*fixednid[1] .-1);

    # #load case 1
    load_x, load_y = Matlab.meshgrid([Macro_nelx/1], [Macro_nely/2]);
    loadnid  = load_x .*(Macro_nely .+1) .+(Macro_nely .+1 .-load_y);
    fixed_x, fixed_y = Matlab.meshgrid([0.0], 0.0:Macro_nely);
    fixednid  = fixed_x .*(Macro_nely+1) .+(Macro_nely .+1 .-fixed_y);
    fixeddofs = vcat(2 .* fixednid[:] , 2 .*fixednid[:] .-1);


    F = sparse(Int.(2 .*loadnid[:]), [1], [-1.0], 2 .*(Macro_nelx .+1) .*(Macro_nely .+1),1);
    U = zeros(Macro_ndof,1);

    freedofs  = setdiff(1:Macro_ndof,fixeddofs);


    nodenrs = reshape(1:(Macro_nely+1)*(Macro_nelx+1),1+Macro_nely,1+Macro_nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,Macro_nele,1);
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*Macro_nely.+[2 3 0 1] -2 -1],Macro_nele,1)

    iK = reshape(kron(edofMat,ones(8,1))',64*Macro_nele,1);
    jK = reshape(kron(edofMat,ones(1,8))',64*Macro_nele,1);
    # PREPARE FILTER
    Macro_H,Macro_Hs = filtering2d(Macro_nelx, Macro_nely, Macro_nele, rmin);
    Micro_H,Micro_Hs = filtering2d(Micro_nelx, Micro_nely, Micro_nele, rmin);


    # INITIALIZE ITERATION
    Macro_x = ones(Macro_nely,Macro_nelx).*Macro_Vol;
    Micro_x = ones(Micro_nely,Micro_nelx);

    for i = 1:Micro_nelx
        for j = 1:Micro_nely
            if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2) < min(Micro_nelx,Micro_nely)/3
                Micro_x[j,i] = 0;
            end
        end
    end

    beta = 1;

    Macro_xTilde = Macro_x; 
    Micro_xTilde = Micro_x;
    Macro_xPhys = 1 .- exp.(-beta .*Macro_xTilde) .+ Macro_xTilde * exp.(-beta);
    Micro_xPhys = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
    
    loopbeta = 0; loop = 0; Macro_change = 1; Micro_change = 1;
    while loop < maxloop || Macro_change > 0.01 || Micro_change > 0.01
        loop = loop+1; loopbeta = loopbeta+1;
        # FE-ANALYSIS AT TWO SCALES
        DH, dDH = EBHM2D(Micro_xPhys, Micro_length, Micro_width, E0, Emin, nu, penal);

        Ke = elementMatVec2D(Macro_Elex/2, Macro_Eley/2, DH);
        sK = reshape(Ke[:]*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
        U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx);
        c = sum(sum((Emin .+Macro_xPhys.^penal*(1 .-Emin)).*ce));
        Macro_dc = -penal*(1 .-Emin)*Macro_xPhys.^(penal-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx);
        Micro_dc = zeros(Micro_nely, Micro_nelx);
        for i = 1:Micro_nele
            dDHe = [dDH[1,1][i] dDH[1,2][i] dDH[1,3][i];
                    dDH[2,1][i] dDH[2,2][i] dDH[2,3][i];
                    dDH[3,1][i] dDH[3,2][i] dDH[3,3][i]];
            dKE = elementMatVec2D(Macro_Elex, Macro_Eley, dDHe);
            dce = reshape(sum((U[edofMat]*dKE).*U[edofMat],dims=2),Macro_nely,Macro_nelx);
            Micro_dc[i] = -sum(sum((Emin .+Macro_xPhys.^penal*(1 .-Emin)).*dce));
        end
        Micro_dv = ones(Micro_nely, Micro_nelx);
        
        # FILTERING AND MODIFICATION OF SENSITIVITIES
        Macro_dx = beta .*exp.(-beta .*Macro_xTilde) .+exp.(-beta); 
        Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta);
        Macro_dc[:] = Macro_H*(Macro_dc[:].*Macro_dx[:]./Macro_Hs); 
        Macro_dv[:] = Macro_H*(Macro_dv[:].*Macro_dx[:]./Macro_Hs);
        Micro_dc[:] = Micro_H*(Micro_dc[:].*Micro_dx[:]./Micro_Hs); 
        Micro_dv[:] = Micro_H*(Micro_dv[:].*Micro_dx[:]./Micro_Hs);

        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys, Macro_change = OC2D(Macro_x, Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, 0.2, beta);
        Micro_x, Micro_xPhys, Micro_change = OC2D(Micro_x, Micro_dc, Micro_dv, Micro_H, Micro_Hs, Micro_Vol, Micro_nele, 0.2, beta);
        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx); 
        Micro_xPhys = reshape(Micro_xPhys, Micro_nely, Micro_nelx);
        # PRINT RESULTS
        println("$loop Obj:$(round.(c,digits=2)) Macro_Vol:$(round.(mean(Macro_xPhys[:] ),digits=2)) Micro_Vol:$(round.(mean(Micro_xPhys[:] ),digits=2)) Macro_ch:$(round.(Macro_change,digits=2)) Micro_ch:$(round.(Micro_change,digits=2))")
        if mod(loop,saveItr)==0
            display(Plots.heatmap(1 .-Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
            Plots.heatmap(1 .-Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal)

            frame(Macro_anim)

            display(Plots.heatmap(1 .-Micro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
            Plots.heatmap(1 .-Micro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal)

            frame(Micro_anim)

        end
        # colormap(gray); imagesc(1-Macro_xPhys); caxis( [0 1] ); axis equal; axis off; drawnow;
        # colormap(gray); imagesc(1-Micro_xPhys); caxis( [0 1] ); axis equal; axis off; drawnow;

        ## UPDATE HEAVISIDE REGULARIZATION PARAMETER
        if beta < 512 && (loopbeta >= 50 || Macro_change <= 0.01 || Micro_change <= 0.01)
            beta = 2*beta; loopbeta = 0; Macro_change = 1; Micro_change = 1;
            display("Parameter beta increased to $beta.");
        end
    end
    return Macro_xPhys,Micro_xPhys
end

##############################################################

# multiple microstructures (θ)
function MultiConTop2D(θ,Macro_struct, Micro_struct,prob, penal, rmin,saveItr=5)
    ## USER-DEFINED LOOP PARAMETERS
    maxloop = 200; E0 = 1; Emin = 1e-9; nu = 0.3;

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2];
    Macro_nelx   = Int(Macro_struct[3]); Macro_nely  = Int(Macro_struct[4]);
    Micro_nelx   = Int(Micro_struct[3]); Micro_nely  = Int(Micro_struct[4]);
    Macro_Vol    = Macro_struct[5][2]; 
    # Micro_Vol   = Micro_struct[5];

    Macro_Vol_FM    = Macro_struct[5][1]

    Macro_Elex = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely;
    Macro_nele = Int(Macro_nelx*Macro_nely);   Micro_nele = Int(Micro_nelx*Micro_nely);
    Macro_ndof = Int(2*(Macro_nelx+1)*(Macro_nely+1));

    # PREPARE FINITE ELEMENT ANALYSIS
    nodenrs = reshape(1:(Macro_nely+1)*(Macro_nelx+1),1+Macro_nely,1+Macro_nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,Macro_nele,1);
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*Macro_nely.+[2 3 0 1] -2 -1],Macro_nele,1)
    

    U,F,freedofs=prob(Macro_nelx,Macro_nely)

    iK = reshape(kron(edofMat,ones(8,1))',64*Macro_nele,1);
    jK = reshape(kron(edofMat,ones(1,8))',64*Macro_nele,1);
    # PREPARE FILTER
    Macro_H,Macro_Hs = filtering2d(Macro_nelx, Macro_nely, Macro_nele, rmin);
    Micro_H,Micro_Hs = filtering2d(Micro_nelx, Micro_nely, Micro_nele, rmin);


    ##free Material distribution to see how to divide microstructures
    Macro_masks,Micro_Vol,Macro_xPhys_diag=freeMaterial2D(θ,Macro_nely,Macro_nelx,Macro_nele,Macro_Vol_FM,iK,jK,Emin,U,F,freedofs,edofMat)

    jet_me=ColorGradient([:white,:cyan,:yellow,:red,:black])


    # INITIALIZE ITERATION
    Macro_x = ones(Macro_nely,Macro_nelx).*Macro_Vol;
    Micro_x=[]
    for i=1:θ
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx)]);
    end

    for i = 1:Micro_nelx
        for j = 1:Micro_nely
            if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2) < min(Micro_nelx,Micro_nely)/3
                for l=1:θ
                    Micro_x[l][j,i] = Emin;
                end
            end
        end
    end

    beta = 1;

    Macro_xTilde = Macro_x;
    Macro_xPhys = 1 .- exp.(-beta .*Macro_xTilde) .+ Macro_xTilde * exp.(-beta);


    Micro_xTilde = Micro_x[1];
    Micro_xPhys=[];
    Micro_dc = []
    Micro_dv = []
    Micro_change=[]
    DHs=[]
    dDHs=[]
    for i=1:θ
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM2D(Micro_xPhys1, Micro_length, Micro_width, E0, Emin, nu, penal);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end

    #first one always empty
    Micro_x[1]=fill(Emin,Micro_nely,Micro_nelx);
    Micro_xPhys[1]=fill(Emin,Micro_nely,Micro_nelx);
    Micro_change[1]=0

    #last one always empty
    Micro_x[end]=fill(1-Emin,Micro_nely,Micro_nelx);
    Micro_xPhys[end]=fill(1-Emin,Micro_nely,Micro_nelx);
    Micro_change[end]=0

    loopbeta = 0; loop = 0; Macro_change = 1; 
    while loop < maxloop && ( Macro_change > 0.01 || Bool.(sum(Micro_change .>= 0.01)<θ))
    # while loop < maxloop || Macro_change > 0.01 || Bool.(sum(Micro_change .>= 0.01))
        loop = loop+1; loopbeta = loopbeta+1;

        # FE-ANALYSIS AT TWO SCALES
        Kes=zeros(8*8,Macro_nelx*Macro_nely)

        for i=2:θ-1
            DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, penal);
            DHs[i]=DH;
            dDHs[i]=dDH;
            Kes[:,Bool.(Macro_masks[i][:])].=elementMatVec2D(Macro_Elex/2, Macro_Eley/2, DHs[i])[:]
        end


        # Ke = elementMatVec2D(Macro_Elex/2, Macro_Eley/2, DHs[2]); #old
        
        sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
        U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS

        # ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx); #old

        #get compliance slow try to make it fast!! ??
        # ce=zeros(Macro_nely*Macro_nelx)
        # for i=1:Macro_nely*Macro_nelx
        #     ce[i]=sum((U[edofMat]*reshape(Kes[:,i],8,8)).*U[edofMat],dims=2)[i]
        # end
        UU=[]
        KKK=[]
        for i=1:Macro_nely*Macro_nelx
            append!(UU,[U[edofMat][i,:]'])
            append!(KKK,[reshape(Kes[:,i],8,8)])
        end
        ce=sum(vcat((UU.*KKK)...).*U[edofMat],dims=2)
        ce=reshape(ce,Macro_nely,Macro_nelx)

        c = sum(sum((Emin .+Macro_xPhys.^penal*(1 .-Emin)).*ce));
        Macro_dc = -penal*(1 .-Emin)*Macro_xPhys.^(penal-1).*ce;
        Macro_dv = ones(Macro_nely, Macro_nelx);

        
        for i=2:θ-1
            Micro_dc[i]=zeros(Micro_nely, Micro_nelx);
            Micro_dv[i]=ones(Micro_nely, Micro_nelx);
        end

        for ii=2:θ-1
            for i = 1:Micro_nele
                dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i];
                        dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i];
                        dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i]];
                dKE = elementMatVec2D(Macro_Elex, Macro_Eley, dDHe);
                dce = sum((U[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                Micro_dc[ii][i] = -sum(sum((Emin .+Macro_xPhys[Bool.(Macro_masks[ii][:])][:].^penal*(1 .-Emin)).*dce));
            end
        end
        
        
        
        # FILTERING AND MODIFICATION OF SENSITIVITIES
        Macro_dx = beta .*exp.(-beta .*Macro_xTilde) .+exp.(-beta); 
        Macro_dc[:] = Macro_H*(Macro_dc[:].*Macro_dx[:]./Macro_Hs); 
        Macro_dv[:] = Macro_H*(Macro_dv[:].*Macro_dx[:]./Macro_Hs);
        for i=2:θ-1
            Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta);
            Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
            Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
        end

        # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
        Macro_x, Macro_xPhys, Macro_change = OC2D(Macro_x, Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, 0.2, beta);
        
        for i=2:θ-1
            Micro_x[i], Micro_xPhys[i], Micro_change1 = OC2D(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, 0.2, beta);
            Micro_change[i]=Micro_change1;
            Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx);
        end
        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx); 
        
        # PRINT RESULTS
        println("$loop Obj:$(round.(c,digits=2)) Macro_Vol:$(round.(mean(Macro_xPhys[:] ),digits=2)) Micro_Vol:$(round.(mean.(Micro_xPhys[:][:]),digits=2)) Macro_ch:$(round.(Macro_change,digits=2)) Micro_ch:$(round.(Micro_change,digits=2))")
        if mod(loop,saveItr)==0

            Macro_xPhys_diag_temp=copy(Macro_xPhys_diag)
            Macro_xPhys_diag_temp[Macro_xPhys.<0.75].=0.0

            display(Plots.heatmap(Macro_xPhys_diag_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0.0, 1.0),fc=jet_me,aspect_ratio=:equal))
            # Plots.heatmap(Macro_xPhys_diag_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0.0, 1.0),fc=jet_me,aspect_ratio=:equal)

            # display(Plots.heatmap(1 .-Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
            # Plots.heatmap(1 .-Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal)
            # frame(Macro_anim)

            space=fill(0,Micro_nely,1)
            Micro_xPhys_all=hcat(Micro_xPhys[1],space);

            for i=2:θ
                Micro_xPhys_temp=copy(Micro_xPhys[i])
                Micro_xPhys_temp[Micro_xPhys_temp.<0.75].=0.0
                Micro_xPhys_temp[Micro_xPhys_temp.>=0.75].=Micro_Vol[i]

                Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys_temp);
                Micro_xPhys_all=hcat(Micro_xPhys_all,space);
            end
            display(Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0.0, 1.0),fc=jet_me,aspect_ratio=:equal))
            # Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0.0, 1.0),fc=jet_me,aspect_ratio=:equal)

            # Micro_xPhys_all=hcat(Micro_xPhys[1],space);
            
            # for i=2:θ
            #     Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys[i]);
            #     Micro_xPhys_all=hcat(Micro_xPhys_all,space);
            # end

            # display(Plots.heatmap(1 .-Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
            # Plots.heatmap(1 .-Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal)
            # frame(Micro_anim)
            

        end

        ## UPDATE HEAVISIDE REGULARIZATION PARAMETER
        # if beta < 512 && (loopbeta >= 50 || Macro_change <= 0.01 || Bool.(sum(Micro_change .<= 0.01)))
        #     beta = 2*beta; loopbeta = 0; Macro_change = 1; Micro_change[:] = .1;
        #     display("Parameter beta increased to $beta.");
        # end
    end

    
    return Macro_xPhys,Micro_xPhys,Macro_masks,Micro_Vol
end

##############################################################
# multiple microstructures (θ) with U clustering
function MultiConTop2DU(θ,Macro_struct, Micro_struct,prob, penal;saveItr=5,maxloop = 200,dynamicClustering=200,concurrent=false)
    ## USER-DEFINED LOOP PARAMETERS
    E0 = 1; Emin = 1e-9; nu = 0.3;
    beta = 1;

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2];
    Macro_nelx   = Int(Macro_struct[3]); Macro_nely  = Int(Macro_struct[4]);
    Micro_nelx   = Int(Micro_struct[3]); Micro_nely  = Int(Micro_struct[4]);
    Macro_Vol    = Macro_struct[5][2]; 
    Micro_Vol_FM   = Micro_struct[5];
    Macro_Vol_FM    = Macro_struct[5][1];
    Micro_Vol=Micro_Vol_FM;

    Macro_Elex = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely;
    Macro_nele = Int(Macro_nelx*Macro_nely);   Micro_nele = Int(Micro_nelx*Micro_nely);
    Macro_ndof = Int(2*(Macro_nelx+1)*(Macro_nely+1));

    # PREPARE FINITE ELEMENT ANALYSIS
    nodenrs = reshape(1:(Macro_nely+1)*(Macro_nelx+1),1+Macro_nely,1+Macro_nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,Macro_nele,1);
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*Macro_nely.+[2 3 0 1] -2 -1],Macro_nele,1)

    Micro_rmin=(Micro_struct[6])
    Macro_rmin=(Macro_struct[6])
    

    U,F,freedofs=prob(Macro_nelx,Macro_nely)

    iK = reshape(kron(edofMat,ones(8,1))',64*Macro_nele,1);
    jK = reshape(kron(edofMat,ones(1,8))',64*Macro_nele,1);
    # PREPARE FILTER
    Macro_H,Macro_Hs = make_filter(Macro_nelx, Macro_nely, Macro_rmin);
    Micro_H,Micro_Hs = make_filter(Micro_nelx, Micro_nely, Micro_rmin);

    ## Connectivity matrix for continuity 
    # L=connectivityMatrix(Micro_nelx,Micro_nely);


    
    
    # jet_me=ColorGradient([:white,:cyan,:yellow,:red,:black])
    # jet_me=ColorGradient([:blue,:cyan,:yellow,:red,:black])
    # t=:dark
    t=:darktest
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    # cscheme=ColorGradient(cschemeList)
    cscheme=cgrad(cschemeList)


    ######### INITIALIZE MACRO #########
    Macro_x = ones(Macro_nely,Macro_nelx).*Macro_Vol;

    Macro_xTilde = Macro_x;
    Macro_xPhys = 1 .- exp.(-beta .*Macro_xTilde) .+ Macro_xTilde * exp.(-beta);
    #mma
    Macro_low=0; Macro_upp=0;
    Macro_xold1=copy(Macro_x);Macro_xold2=copy(Macro_x);

    ##free Material distribution to see how to divide microstructures
    Macro_masks,Macro_xPhys_diag,Micro_centers,Macro_xPhys_NC=Uclustering2D(θ,Macro_nelx,Macro_nely,prob,Macro_Vol_FM,Macro_rmin,Micro_Vol,edofMat)
    if !concurrent
        # println("not concurrent!")
        Macro_xPhys=Macro_xPhys_NC
    end



    ######### INITIALIZE MICRO #########
    Micro_x=[]
    for i=1:θ
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx)]);
    end

    for i = 1:Micro_nelx
        for j = 1:Micro_nely
            if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2) < min(Micro_nelx,Micro_nely)/3
                for l=1:θ
                    Micro_x[l][j,i] = Emin;
                end
            end
        end
    end

    Micro_x=[]
    for i=1:θ
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx).*Micro_Vol]);
    end

    #mma
    Micro_xold1=[];Micro_xold2=[]
    Micro_low=[];Micro_upp=[];
    for i=1:θ
        append!(Micro_xold1 ,[copy(Micro_x[i])]);append!(Micro_xold2 ,[copy(Micro_x[i])]);
        append!(Micro_low ,[0]);append!(Micro_upp ,[0]);
    end
    
    Micro_xTilde = Micro_x[1];
    Micro_xPhys=[];
    Micro_dc = []; Micro_dv = [];
    Micro_change=[];
    DHs=[];dDHs=[];

    for i=1:θ
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM2D(Micro_xPhys1, Micro_length, Micro_width, E0, Emin, nu, penal);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end


    ######################################################

    xxxx=1;
    loopbeta = 0; loop = 0; Macro_change = 1;
    while loop < maxloop && ( Macro_change > 0.01 || sum(Micro_change .>= 0.01)>=1)

        # while loop < maxloop || Macro_change > 0.01 || Bool.(sum(Micro_change .>= 0.01))
        loop = loop+1; loopbeta = loopbeta+1;

        # FE-ANALYSIS MACROSCALE
        Kes=zeros(8*8,Macro_nelx*Macro_nely)
        for i=1:θ
            DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, penal);
            DHs[i]=DH;
            dDHs[i]=dDH;
            Kes[:,Bool.(Macro_masks[i][:])].=elementMatVec2D(Macro_Elex/2, Macro_Eley/2, DHs[i])[:]
        end

        # Ke = elementMatVec2D(Macro_Elex/2, Macro_Eley/2, DHs[2]); #old
        
        sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
        K = sparse(vec(iK),vec(jK),vec(sK)); K = (K+K')/2;
        U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);

        if mod(loop,dynamicClustering)==0 && concurrent
            println("dyanmic clustering")
            Macro_masks,Macro_xPhys_diag,Micro_centers,Macro_xPhys_NC= UDynamicClustering2D(θ,Macro_xPhys,U[edofMat],Micro_centers,false)
            for i=1:θ
                Micro_x[i]=ones(Micro_nely,Micro_nelx)*Micro_Vol
                Micro_xTilde = ones(Micro_nely,Micro_nelx)*Micro_Vol
                Micro_xPhys[i] = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
            end
            xxxx=10;
        else
            xxxx=1;
        end
        

        # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS
        UU=mapslices(x->[x]'[:], U[edofMat], dims=2)[:]
        KKK=mapslices(x->[reshape(x,8,8)], Kes, dims=1)[:]

        ce=sum(vcat((UU.*KKK)...).*U[edofMat],dims=2)
        ce=reshape(ce,Macro_nely,Macro_nelx)
        c = sum(sum((Emin .+Macro_xPhys.^penal*(1 .-Emin)).*ce));

        ###only if concurrent update Macro_xPhys
        if concurrent
            # println("concurrent!")

            Macro_dc = -penal*(1 .-Emin)*Macro_xPhys.^(penal-1).*ce;
            Macro_dv = ones(Macro_nely, Macro_nelx);

            
            # FILTERING AND MODIFICATION OF SENSITIVITIES
            Macro_dx = beta .*exp.(-beta .*Macro_xTilde) .+exp.(-beta); 
            Macro_dc[:] = Macro_H*(Macro_dc[:].*Macro_dx[:]./Macro_Hs); 
            Macro_dv[:] = Macro_H*(Macro_dv[:].*Macro_dx[:]./Macro_Hs);
            # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
            Macro_x, Macro_xPhys, Macro_change = OC2D(Macro_x, Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, 0.2, beta);
            # Macro_x, Macro_xPhys, Macro_change,Macro_low,Macro_upp,Macro_xold1,Macro_xold2 = OC2DMMA(Macro_x, Macro_xold1,Macro_xold2,Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, 0.2, beta,loop,Macro_low,Macro_upp);
        end
        
        # FE-ANALYSIS MICROSCALE and optimization (if dynamic more than once)
        for xxx in 1:xxxx

            for i=1:θ
                Micro_dc[i]=zeros(Micro_nely, Micro_nelx);
                Micro_dv[i]=ones(Micro_nely, Micro_nelx);
                ###
                DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, penal);
                DHs[i]=DH;
                dDHs[i]=dDH;
            end
            

            for ii=1:θ
                for i = 1:Micro_nele
                    dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i];
                            dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i];
                            dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i]];
                    dKE = elementMatVec2D(Macro_Elex, Macro_Eley, dDHe);
                    dce = sum((U[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                    Micro_dc[ii][i] = -sum(sum((Emin .+Macro_xPhys[Bool.(Macro_masks[ii][:])][:].^penal*(1 .-Emin)).*dce));
                end
            end

            for i=1:θ
                Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta); ## check Micro_xTilde right???????
                Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
                Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
            end
            
            for i=1:θ
                # if !connectivity
                    ss=6
                    Micro_x[i]=addFabConstraint2D(Micro_x[i],ss);
                    Micro_xPhys[i]=addFabConstraint2D(Micro_xPhys[i],ss);
                    Micro_x[i], Micro_xPhys[i], Micro_change1 = OC2D(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol, Micro_nele, 0.2, beta,true,ss);
                    # Micro_x[i], Micro_xPhys[i], Micro_change1,Micro_low[i],Micro_upp[i],Micro_xold1[i],Micro_xold2[i]= OC2DMMA(Micro_x[i],Micro_xold1[i],Micro_xold2[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, 0.2, beta,loop,Micro_low[i],Micro_upp[i]);
                # else
                #     Micro_x[i], Micro_xPhys[i], Micro_change1,Micro_low[i],Micro_upp[i],Micro_xold1[i],Micro_xold2[i]= OC2DMMACON(Micro_x[i],Micro_xold1[i],Micro_xold2[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, 0.2, beta,loop,Micro_low[i],Micro_upp[i],L);
                # end
                Micro_change[i]=Micro_change1;
                Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx);
            end
        end

        Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx); 
        
        # PRINT AND DISPLAY RESULTS
        println("$loop Obj:$(round.(c,digits=4)) Macro_Vol:$(round.(mean(Macro_xPhys[:] ),digits=2)) Micro_Vol:$(round.(mean.(Micro_xPhys[:][:]),digits=2)) Macro_ch:$(round.(Macro_change,digits=2)) Micro_ch:$(round.(Micro_change,digits=2))")
        if mod(loop,saveItr)==0

            Macro_xPhys_diag_temp=copy(Macro_xPhys_diag)
            Macro_xPhys_diag_temp[Macro_xPhys.<0.75].=0.0

            display(Plots.heatmap(Macro_xPhys_diag_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
            (Plots.heatmap(Macro_xPhys_diag_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
            frame(Macro_anim)
            # display(Plots.heatmap(1 .-Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
            # Plots.heatmap(1 .-Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal)
            # frame(Macro_anim)

            
            space=fill(0,Micro_nely,1)
            Micro_xPhys_all=space;
            for i=1:θ
                Micro_xPhys_temp=copy(Micro_xPhys[i])
                Micro_xPhys_temp[Micro_xPhys_temp.<0.75].=0.0
                Micro_xPhys_temp[Micro_xPhys_temp.>=0.75].=i
                Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys_temp);
                Micro_xPhys_all=hcat(Micro_xPhys_all,space);
            end

            display(Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
            (Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))

            # Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(1, θ),fc=jet_me,aspect_ratio=:equal)
            frame(Micro_anim)

            

        end

        ## UPDATE HEAVISIDE REGULARIZATION PARAMETER
        # if beta < 512 && (loopbeta >= 50 || Macro_change <= 0.01 || Bool.(sum(Micro_change .<= 0.01)))
        #     beta = 2*beta; loopbeta = 0; Macro_change = 1; Micro_change[:] = .1;
        #     display("Parameter beta increased to $beta.");
        # end
    end

    ##END LOOP calculate and display properties
    n=3
    for i=1:θ
        Micro_xPhys_temp=copy(Micro_xPhys[i])
        Micro_xPhys_temp[Micro_xPhys_temp.<0.75].=0.0
        Micro_xPhys_temp[Micro_xPhys_temp.>=0.75].=i
        
        display(Plots.heatmap(repeat(Micro_xPhys_temp,n,n),clims=(0, θ),fc=cscheme,aspect_ratio=:equal,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing))
        savefig("./img/finalArrayMicro$(i).png") # Saves the CURRENT_PLOT as a .png
        
        U,S,V = svd(DHs[i]);
        sigma = S;
        k = sum(sigma .> 1e-15);
        SH = (U[:,1:k] * diagm(0=>(1 ./sigma[1:k])) * V[:,1:k]')'; # more stable SVD (pseudo)inverse
        EH = [1/SH[1,1], 1/SH[2,2]]; # effective Young's modulus
        GH = [1/SH[3,3]]; # effective shear modulus
        vH = [-SH[2,1]/SH[1,1],-SH[1,2]/SH[2,2]]; # effective Poisson's ratio
        display( "young:$(round.(EH,digits=3)), poisson:$(round.(vH,digits=3)), shear:$(round.(GH,digits=3))" )

        
        # savefig("./img/finalArrayMicro1$(i).png") # Saves the CURRENT_PLOT as a .png

    end

    t=:darktest
    cschemeList=[]
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=cgrad(cschemeList)

    thresh=0.5
    thresh1=0.75
    Macro_xPhys_det=zeros(size(Macro_xPhys)[1]*size(Micro_xPhys[1])[1],size(Macro_xPhys)[2]*size(Micro_xPhys[1])[2]);
    Macro_xPhys_det1=zeros(size(Macro_xPhys)[1]*size(Micro_xPhys[1])[1],size(Macro_xPhys)[2]*size(Micro_xPhys[1])[2]);
    sizeMicro=size(Micro_xPhys[1])[1]
    for i in 1:size(Macro_xPhys)[1]
        for j in 1:size(Macro_xPhys)[2]
            ind=Int(Macro_xPhys_diag[i,j])
            micr=copy(Micro_xPhys[ind])
            micr[micr.>thresh1].=1.0;
            micr[micr.<thresh1].=0.0;
            micr.=micr.*(ind.+1)
            indI=Int((i-1)*sizeMicro+1 )
            indJ=Int((j-1)*sizeMicro+1 )
            if Macro_xPhys[i,j]>thresh
                Macro_xPhys_det1[ indI:indI+sizeMicro-1 , indJ:indJ+sizeMicro-1].=micr 
            end
            Macro_xPhys_det[ indI:indI+sizeMicro-1 , indJ:indJ+sizeMicro-1].=micr 
        end
    end
    Macro_xPhys_det[Macro_xPhys_det.<thresh1].=NaN;
    Macro_xPhys_det[Macro_xPhys_det .!= NaN]=Macro_xPhys_det[Macro_xPhys_det .!= NaN].-1;
    Macro_xPhys_det1[Macro_xPhys_det1.<thresh1].=NaN;
    Macro_xPhys_det1[Macro_xPhys_det1 .!= NaN]=Macro_xPhys_det1[Macro_xPhys_det1 .!= NaN].-1;
    display(Plots.heatmap(Macro_xPhys_det,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(1, θ),fc=cscheme,aspect_ratio=:equal))
    display(Plots.heatmap(Macro_xPhys_det1,showaxis = false,xaxis=nothing,yaxis=nothing,clims=(1, θ),fc=cscheme,aspect_ratio=:equal))

           
    return Macro_xPhys,Micro_xPhys,Macro_masks,Macro_xPhys_diag,Micro_Vol,DHs
end

##############################################################


##############################################################
# compliant multiple microstructures (θ) with U clustering
function CompliantMultiConTop2DU(θ,Macro_struct, Micro_struct,prob, penal,saveItr=5, maxloop = 200,connectivity=false)
    ## USER-DEFINED LOOP PARAMETERS
    E0 = 1; Emin = 1e-9; nu = 0.3;

    Macro_length = Macro_struct[1]; Macro_width = Macro_struct[2];
    Micro_length = Micro_struct[1]; Micro_width = Micro_struct[2];
    Macro_nelx   = Int(Macro_struct[3]); Macro_nely  = Int(Macro_struct[4]);
    Micro_nelx   = Int(Micro_struct[3]); Micro_nely  = Int(Micro_struct[4]);
    Macro_Vol    = Macro_struct[5][2]; 
    Micro_Vol_FM   = Micro_struct[5];
    Macro_Vol_FM    = Macro_struct[5][1]

    Micro_rmin=Int(Micro_struct[6])
    Macro_rmin=Int(Macro_struct[6])


    Macro_Elex = Macro_length/Macro_nelx; Macro_Eley = Macro_width/Macro_nely;
    Macro_nele = Int(Macro_nelx*Macro_nely);   Micro_nele = Int(Micro_nelx*Micro_nely);
    Macro_ndof = Int(2*(Macro_nelx+1)*(Macro_nely+1));

    

    # PREPARE FINITE ELEMENT ANALYSIS
    nodenrs = reshape(1:(Macro_nely+1)*(Macro_nelx+1),1+Macro_nely,1+Macro_nelx);
    edofVec = reshape(2*nodenrs[1:end-1,1:end-1].+1,Macro_nele,1);
    edofMat = repeat(edofVec,1,8).+repeat([0 1 2*Macro_nely.+[2 3 0 1] -2 -1],Macro_nele,1)
    

    ##########################
    # DEFINE LOADS AND SUPPORTS 
    U,F,freedofs,DofDOut,S=prob(Macro_nelx,Macro_nely)


    #########################

    iK = reshape(kron(edofMat,ones(8,1))',64*Macro_nele,1);
    jK = reshape(kron(edofMat,ones(1,8))',64*Macro_nele,1);
    # PREPARE FILTER
    Macro_H,Macro_Hs = filtering2d(Macro_nelx, Macro_nely, Macro_nele, Macro_rmin);
    Micro_H,Micro_Hs = filtering2d(Micro_nelx, Micro_nely, Micro_nele, Micro_rmin);


    ##free Material distribution to see how to divide microstructures
    Macro_masks,Micro_Vol,Macro_xPhys_diag=CompliantUclustering2D(θ,Macro_nely,Macro_nelx,Macro_nele,Macro_Vol_FM,Micro_Vol_FM,iK,jK,Emin,U,F,freedofs,edofMat,DofDOut,S)

    ## Connectivity matrix for continuity 
    L=connectivityMatrix(Micro_nelx,Micro_nely);

    # jet_me=ColorGradient([:white,:cyan,:yellow,:red,:black])
    # jet_me=ColorGradient([:blue,:cyan,:yellow,:red,:black])
    t=:dark
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ
        append!(cschemeList,[palette(t)[i]])
    end
    cscheme=ColorGradient(cschemeList)


    # INITIALIZE ITERATION
    Macro_x = ones(Macro_nely,Macro_nelx).*Macro_Vol;
    Micro_x=[]
    for i=1:θ
        append!(Micro_x ,[ones(Micro_nely,Micro_nelx)]);
    end

    for i = 1:Micro_nelx
        for j = 1:Micro_nely
            if sqrt((i-Micro_nelx/2-0.5)^2+(j-Micro_nely/2-0.5)^2) < min(Micro_nelx,Micro_nely)/3
                for l=1:θ
                    Micro_x[l][j,i] = Emin;
                end
            end
        end
    end

    #mma
    Micro_xold1=[]
    Micro_xold2=[]
    Micro_low=[];
    Micro_upp=[];
    for i=1:θ
        append!(Micro_xold1 ,[copy(Micro_x[i])]);
        append!(Micro_xold2 ,[copy(Micro_x[i])]);
        append!(Micro_low ,[0]);
        append!(Micro_upp ,[0]);
    end
    Macro_low=0;
    Macro_upp=0;
    Macro_xold1=copy(Macro_x);
    Macro_xold2=copy(Macro_x);
    

    beta = 1;

    Macro_xTilde = Macro_x;
    Macro_xPhys = 1 .- exp.(-beta .*Macro_xTilde) .+ Macro_xTilde * exp.(-beta);


    Micro_xTilde = Micro_x[1];
    Micro_xPhys=[];
    Micro_dc = [];
    Micro_dv = [];
    Micro_change=[];
    DHs=[]
    dDHs=[]
    for i=1:θ
        Micro_xPhys1 = 1 .- exp.(-beta .*Micro_xTilde) .+ Micro_xTilde * exp.(-beta);
        append!(Micro_xPhys ,[Micro_xPhys1]);
        append!(Micro_dc ,[zeros(Micro_nely, Micro_nelx)]);
        append!(Micro_dv ,[ones(Micro_nely, Micro_nelx)]);
        append!(Micro_change ,[1.0]);
        DH, dDH = EBHM2D(Micro_xPhys1, Micro_length, Micro_width, E0, Emin, nu, penal);
        append!(DHs ,[DH]);
        append!(dDHs ,[dDH]);
    end

    Micro_penal=3.0;

    # loopbeta = 0; loop = 0; Macro_change = 1;
    for penal=1.0:4
        loopbeta = 0; loop = 0; Macro_change = 1;
        while loop < maxloop && ( Macro_change > 0.01 || sum(Micro_change .>= 0.01)>=1)
            loop = loop+1; loopbeta = loopbeta+1;

            # FE-ANALYSIS AT TWO SCALES
            Kes=zeros(8*8,Macro_nelx*Macro_nely)

            for i=1:θ
                DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, Micro_penal);
                DHs[i]=DH;
                dDHs[i]=dDH;
                Kes[:,Bool.(Macro_masks[i][:])].=elementMatVec2D(Macro_Elex/2, Macro_Eley/2, DHs[i])[:]
            end


            # Ke = elementMatVec2D(Macro_Elex/2, Macro_Eley/2, DHs[2]); #old
            
            sK = reshape(Kes.*(Emin .+Macro_xPhys[:]'.^penal*(1 .-Emin)),64*Macro_nele,1);
            K = sparse(vec(iK),vec(jK),vec(sK))+S; K = (K+K')/2;
            U[freedofs,:] = K[freedofs,freedofs]\Array(F[freedofs,:]);
            U1=U[:,1]
            U2=U[:,2]
            # OBJECTIVE FUNCTION AND SENSITIVITY ANALYSIS

            # ce = reshape(sum((U[edofMat]*Ke).*U[edofMat],dims=2),Macro_nely,Macro_nelx); #old

            
            # ce=zeros(Macro_nely*Macro_nelx)
            # for i=1:Macro_nely*Macro_nelx
            #     ce[i]=sum((U[edofMat]*reshape(Kes[:,i],8,8)).*U[edofMat],dims=2)[i]
            # end

            # UU1=[]
            # UU2=[]
            # KKK=[]
            # for i=1:Macro_nely*Macro_nelx
            #     append!(UU1,[U1[edofMat][i,:]'])
            #     append!(UU2,[U2[edofMat][i,:]'])
            #     append!(KKK,[reshape(Kes[:,i],8,8)])
            # end
            UU1=mapslices(x->[x]'[:], U1[edofMat], dims=2)[:]
            UU2=mapslices(x->[x]'[:], U2[edofMat], dims=2)[:]
            KKK=mapslices(x->[reshape(x,8,8)], Kes, dims=1)[:]

            ce=-sum(vcat((UU1.*KKK)...).*U2[edofMat],dims=2)
            ce=reshape(ce,Macro_nely,Macro_nelx)

            # c = sum(sum((Emin .+Macro_xPhys.^penal*(1 .-Emin)).*ce));
            c=U[DofDOut,1]

            
            Macro_dc = -penal*(1 .-Emin)*Macro_xPhys.^(penal-1).*ce;
            Macro_dv = ones(Macro_nely, Macro_nelx);

            
            for i=1:θ
                Micro_dc[i]=zeros(Micro_nely, Micro_nelx);
                Micro_dv[i]=ones(Micro_nely, Micro_nelx);
            end

            for ii=1:θ
                for i = 1:Micro_nele
                    dDHe = [dDHs[ii][1,1][i] dDHs[ii][1,2][i] dDHs[ii][1,3][i];
                            dDHs[ii][2,1][i] dDHs[ii][2,2][i] dDHs[ii][2,3][i];
                            dDHs[ii][3,1][i] dDHs[ii][3,2][i] dDHs[ii][3,3][i]];
                    dKE = elementMatVec2D(Macro_Elex, Macro_Eley, dDHe);
                    # dce = sum((U[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                    # dce = sum((U1[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U1[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                    dce = sum((U2[edofMat[Bool.(Macro_masks[ii][:]),:]]*dKE).*U2[edofMat[Bool.(Macro_masks[ii][:]),:]],dims=2);
                    Micro_dc[ii][i] = -sum(sum((Emin .+Macro_xPhys[Bool.(Macro_masks[ii][:])][:].^Micro_penal*(1 .-Emin)).*dce));
                end
            end
            
            
            
            # FILTERING AND MODIFICATION OF SENSITIVITIES
            Macro_dx = beta .*exp.(-beta .*Macro_xTilde) .+exp.(-beta); 
            Macro_dc[:] = Macro_H*(Macro_dc[:].*Macro_dx[:]./Macro_Hs); 
            Macro_dv[:] = Macro_H*(Macro_dv[:].*Macro_dx[:]./Macro_Hs);
            for i=1:θ
                Micro_dx = beta .*exp.(-beta .*Micro_xTilde) .+exp.(-beta);
                Micro_dc[i][:] = Micro_H*(Micro_dc[i][:].*Micro_dx[:]./Micro_Hs); 
                Micro_dv[i][:] = Micro_H*(Micro_dv[i][:].*Micro_dx[:]./Micro_Hs);
            end

            # OPTIMALITY CRITERIA UPDATE MACRO AND MICRO ELELMENT DENSITIES
            Macro_x, Macro_xPhys, Macro_change = CompliantOC(Macro_x, Macro_dc, Macro_dv, Macro_H, Macro_Hs, Macro_Vol, Macro_nele, 0.1, beta);
            
            for i=1:θ
                if !connectivity
                    # Micro_x[i], Micro_xPhys[i], Micro_change1 = OC2D(Micro_x[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, 0.2, beta);
                    Micro_x[i], Micro_xPhys[i], Micro_change1,Micro_low[i],Micro_upp[i],Micro_xold1[i],Micro_xold2[i]= OC2DMMA(Micro_x[i],Micro_xold1[i],Micro_xold2[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, 0.2, beta,loop,Micro_low[i],Micro_upp[i]);
                else
                    Micro_x[i], Micro_xPhys[i], Micro_change1,Micro_low[i],Micro_upp[i],Micro_xold1[i],Micro_xold2[i]= OC2DMMACON(Micro_x[i],Micro_xold1[i],Micro_xold2[i], Micro_dc[i], Micro_dv[i], Micro_H, Micro_Hs, Micro_Vol[i], Micro_nele, 0.2, beta,loop,Micro_low[i],Micro_upp[i],L);              
                end
                Micro_change[i]=Micro_change1;
                Micro_xPhys[i] = reshape(Micro_xPhys[i], Micro_nely, Micro_nelx);
            end
            Macro_xPhys = reshape(Macro_xPhys, Macro_nely, Macro_nelx); 
            
            # PRINT RESULTS
            println("$loop Obj:$(round.(c,digits=2)) Macro_Vol:$(round.(mean(Macro_xPhys[:] ),digits=2)) Micro_Vol:$(round.(mean.(Micro_xPhys[:][:]),digits=2)) Macro_ch:$(round.(Macro_change,digits=2)) Micro_ch:$(round.(Micro_change,digits=2))")
            if mod(loop,saveItr)==0

                Macro_xPhys_diag_temp=copy(Macro_xPhys_diag)
                Macro_xPhys_diag_temp[Macro_xPhys.<0.75].=0.0

                display(Plots.heatmap(Macro_xPhys_diag_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
                (Plots.heatmap(Macro_xPhys_diag_temp,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
                frame(Macro_anim)
                # display(Plots.heatmap(1 .-Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal))
                # Plots.heatmap(1 .-Macro_xPhys,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,fc=:grays,clims=(0.0, 1.0),aspect_ratio=:equal)
                # frame(Macro_anim)

                
                space=fill(0,Micro_nely,1)
                Micro_xPhys_all=space;
                for i=1:θ
                    Micro_xPhys_temp=copy(Micro_xPhys[i])
                    thres=0.75;
                    Micro_xPhys_temp[Micro_xPhys_temp.<thres].=0.0
                    Micro_xPhys_temp[Micro_xPhys_temp.>=thres].=i
                    Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys_temp);
                    Micro_xPhys_all=hcat(Micro_xPhys_all,space);
                end

                display(Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))
                (Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme,aspect_ratio=:equal))

                # Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(1, θ),fc=jet_me,aspect_ratio=:equal)
                frame(Micro_anim)
                

            end

            ## UPDATE HEAVISIDE REGULARIZATION PARAMETER
            # if beta < 512 && (loopbeta >= 50 || Macro_change <= 0.01 || Bool.(sum(Micro_change .<= 0.01)))
            #     beta = 2*beta; loopbeta = 0; Macro_change = 1; Micro_change[:] = .1;
            #     display("Parameter beta increased to $beta.");
            # end
        end
    end

    ##calculate and display properties
    n=3
    for i=1:θ
        Micro_xPhys_temp=copy(Micro_xPhys[i])
        Micro_xPhys_temp[Micro_xPhys_temp.<0.75].=0.0
        Micro_xPhys_temp[Micro_xPhys_temp.>=0.75].=i
        
        display(Plots.heatmap(repeat(Micro_xPhys_temp,n,n),clims=(0, θ),fc=cscheme,aspect_ratio=:equal,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing))
        savefig("./img/finalArrayMicro$(i).png") # Saves the CURRENT_PLOT as a .png
        
        U,S,V = svd(DHs[i]);
        sigma = S;
        k = sum(sigma .> 1e-15);
        SH = (U[:,1:k] * diagm(0=>(1 ./sigma[1:k])) * V[:,1:k]')'; # more stable SVD (pseudo)inverse
        EH = [1/SH[1,1], 1/SH[2,2]]; # effective Young's modulus
        GH = [1/SH[3,3]]; # effective shear modulus
        vH = [-SH[2,1]/SH[1,1],-SH[1,2]/SH[2,2]]; # effective Poisson's ratio
        display( "young:$(round.(EH,digits=3)), poisson:$(round.(vH,digits=3)), shear:$(round.(GH,digits=3))" )

        
        # savefig("./img/finalArrayMicro1$(i).png") # Saves the CURRENT_PLOT as a .png

    end
    return Macro_xPhys,Micro_xPhys,Macro_masks,Macro_xPhys_diag,Micro_Vol,DHs
end


#######################################################



#======================================================================================================================#
# Main references:                                                                                                     #
#                                                                                                                      #
# (1) Jie Gao, Zhen Luo, Liang Xia, Liang Gao. Concurrent topology optimization of multiscale composite structures     #
# in Matlab. Accepted in Structural and multidisciplinary optimization.                                                #
#                                                                                                                      #
# (2) Xia L, Breitkopf P. Design of materials using topology optimization and energy-based homogenization approach in  #
# Matlab. # Structural and multidisciplinary optimization, 2015, 52(6): 1229-1241.                                     #
#                                                                                                                      #                                                                  #
#======================================================================================================================#
