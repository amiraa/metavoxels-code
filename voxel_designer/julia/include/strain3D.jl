#======================================================================================================================#
# Amira Abdel-Rahman
# (c] Massachusetts Institute of Technology 2022
#======================================================================================================================#
using Rotations

# https://www.mathworks.com/matlabcentral/fileexchange/72112-principal-strains-in-3d-problems
# Input:
# e: stress vector in the form [epsilon_xx epsilon_yy epsilon_zz gamma_xy gamma_yz gamma_zx]
# Output:
# E: principal strains magnitude vector
# T: principal strains orientation matrix
function Principal_Strain_3D(e)
    #Input:
    #e: stress vector in the form [epsilon_xx epsilon_yy epsilon_zz gamma_xy gamma_yz gamma_zx]
    #Output:
    #E: principal strains magnitude vector
    #T: principal strains orientation matrix 
    e[4:6]=e[4:6]/2;    #Convert total shear strain to average shear strain
    ee=[e[1] e[4] e[6]; e[4] e[2] e[5]; e[6] e[5] e[3]];
    
    T =eigvecs(ee)
    E =eigvals(ee);
    # E=diagm(E);  
    # display(T) 
    # display(E) 
    if E[1]<=0 &&E[2]<=0&&E[3]<=0
        # display("negative")
        I = sortperm(abs.(E),rev=true);   
        # display(E)           
        E=sort(abs.(E),rev=true);
        # E=-E; 
        # display(E)
        # display(I)
    elseif E[1]>=0 &&E[2]>=0&&E[3]>=0
        # display("postive")
        I = sortperm(E,rev=true);   
        # display(E)           
        E=sort(E,rev=true); 
        # display(E)
        # display(I)
    else
        # display("mixed")
        # I = sortperm(E,rev=true);   
        I = sortperm(abs.(E),rev=true);   

        # display(E)           
        # E=sort(E,rev=true); 
        E=sort(abs.(E),rev=true);

        # I=[1,2,3]
        # E=abs.(E)
        # display(E)
        # display(I)
    end
    # display(E)
    T=T[:,I];  
    return E,T
end

function getStress3D(Ue)
    #based on https://arxiv.org/ftp/arxiv/papers/2104/2104.01210.pdf
    KE,B,D=brick_stiffnessMatrix();
    MISES=0; #von Mises stress vector
    S=zeros(6);

    # display(Ue)

    temp=(D*B*Ue)'; #x(i)^q*
    #  𝝈𝒊 = (𝜎𝑖𝑥, 𝜎𝑖𝑦, 𝜎𝑖𝑧, 𝜎𝑖𝑥𝑦, 𝜎𝑖𝑦𝑧, 𝜎𝑖𝑧𝑥 )
    # 𝜎𝑣𝑚𝑖 = (𝜎𝑖𝑥^2 + 𝜎𝑖𝑦^2 + 𝜎𝑖𝑧^2 − 𝜎𝑖𝑥𝜎𝑖𝑦 − 𝜎𝑖𝑦𝜎𝑖𝑧 − 𝜎𝑖𝑧𝜎𝑖𝑥 + 3𝜏𝑖𝑥𝑦^2 + 3𝜏𝑖𝑦𝑧^2 + 3𝜏𝑖𝑧𝑥^2)^(1/2)
    S=temp;
    MISES=sqrt(0.5*((temp[1]-temp[2])^2+(temp[1]-temp[3])^2+(temp[2]-temp[3])^2+6*sum(temp[4:6].^2)));
    # display(temp)
    return S,MISES
end

function getPrincipalStrains3D(Ue)
    E,MISES=getStress3D(copy(Ue))
    E,T=Principal_Strain_3D(E)
    R=RotMatrix{3}(T)
    a=rotation_axis(R)
    q=SVector{3}(1,0,0)
    a=R*q
    x=a[1];y=a[2];z=a[3];
    r = sqrt(x*x + y*y + z*z)
    t = atan(y/x);tθ=t*180/π; #t = angle on x-y plane, 
    p = acos(z/r);pθ=p*180/π; # p = angle off of z-axis
    # display("Pricipal Strain direction $(a), θ on x-y plane is $tθ, and from z plane is $pθ")
    
    return t,p,MISES,E[1]/E[2],E[2]/E[3],a,E,T

end
####################################################
## SUB FUNCTION: display_3D
function display_3D(rho)
    nely,nelx,nelz = size(rho);
    hx = 1; hy = 1; hz = 1;
    face = [1 2 3 4; 2 6 7 3; 4 3 7 8; 1 5 8 4; 1 2 6 5; 5 6 7 8];
    # set(gcf,'Name','ISO display','NumberTitle','off');
    for k = 1:nelz
        z = (k-1)*hz;
        for i = 1:nelx
            x = (i-1)*hx;
            for j = 1:nely
                y = nely*hy - (j-1)*hy;
                if (rho[j,i,k] > 0)
                    vert = [x y z; x y-hx z; x+hx y-hx z; x+hx y z; x y z+hx;x y-hx z+hx; x+hx y-hx z+hx;x+hx y z+hx];
                    vert[:,[2 3]] = vert[:,[3 2]]; vert[:,2,:] = -vert[:,2,:];
                    # patch('Faces',face,'Vertices',vert,'FaceColor',[0.2+0.8*(1-rho(j,i,k)),0.2+0.8*(1-rho(j,i,k)),0.2+0.8*(1-rho(j,i,k))] );
                    # hold on;
                end
            end
        end
    end
    # axis equal; axis tight; axis off; box on; view( [30,30] ); pause(1e-6);
end

function displayElementDeformation3D(Ue,unit)
    Ue1 =[
        unit,0.0,0, #1
        unit,unit,0, #2
        0,unit,0, #3
        0,0,0, #4
        unit,0,unit, #5 
        unit,unit,unit, #6
        0,unit,unit, #7
        0,0,unit, #8
    ]

    Uee1=reshape(Ue1,3,8)

    fig = Figure(resolution=(300, 300), fontsize=10)

    pointsX=[];pointsY=[];pointsZ=[];
    for i =1:4
        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]]);
    end
    append!(pointsY,[Uee1[1,1]]);append!(pointsX,[Uee1[2,1]]);append!(pointsZ,[Uee1[3,1]]);
    # Makie.lines(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ))
    for i =5:8
        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]])
    end
    append!(pointsY,[Uee1[1,5]]);append!(pointsX,[Uee1[2,5]]);append!(pointsZ,[Uee1[3,5]]);
    Makie.lines(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:black, linestyle = :dash,figure=(resolution=(400, 400), fontsize=10),axis = (; type = Axis3, protrusions = (0, 0, 0, 0),
            viewmode = :fit, limits = (-unit*0.5, unit+unit*0.5, -unit*0.5-unit, unit+unit*0.5-unit, -unit*0.5, unit+unit*0.5)))
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,4]]);append!(pointsX,[Uee1[2,4]]);append!(pointsZ,[Uee1[3,4]]);
    append!(pointsY,[Uee1[1,8]]);append!(pointsX,[Uee1[2,8]]);append!(pointsZ,[Uee1[3,8]]);
    Makie.lines!(Float16.(pointsX),- Float16.(pointsY),Float16.(pointsZ),color =:black, linestyle = :dash)
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,2]]);append!(pointsX,[Uee1[2,2]]);append!(pointsZ,[Uee1[3,2]]);
    append!(pointsY,[Uee1[1,6]]);append!(pointsX,[Uee1[2,6]]);append!(pointsZ,[Uee1[3,6]]);
    Makie.lines!(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:black, linestyle = :dash)
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,3]]);append!(pointsX,[Uee1[2,3]]);append!(pointsZ,[Uee1[3,3]]);
    append!(pointsY,[Uee1[1,7]]);append!(pointsX,[Uee1[2,7]]);append!(pointsZ,[Uee1[3,7]]);
    Makie.lines!(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:black, linestyle = :dash)
    
    
    Ueee=reshape(copy(Ue),3,8)
    Uee1=reshape(Ue1,3,8)
    Ueee[2,:].=-Ueee[2,:];
    Uee1.=Uee1.+Ueee[[2,1,3],:]

    pointsX=[];pointsY=[];pointsZ=[];
    for i =1:4
        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]]);
    end
    append!(pointsY,[Uee1[1,1]]);append!(pointsX,[Uee1[2,1]]);append!(pointsZ,[Uee1[3,1]]);
    # Makie.lines(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ))
    for i =5:8
        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]])
    end
    append!(pointsY,[Uee1[1,5]]);append!(pointsX,[Uee1[2,5]]);append!(pointsZ,[Uee1[3,5]]);
    Makie.lines!(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:blue)
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,4]]);append!(pointsX,[Uee1[2,4]]);append!(pointsZ,[Uee1[3,4]]);
    append!(pointsY,[Uee1[1,8]]);append!(pointsX,[Uee1[2,8]]);append!(pointsZ,[Uee1[3,8]]);
    Makie.lines!(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:blue)
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,2]]);append!(pointsX,[Uee1[2,2]]);append!(pointsZ,[Uee1[3,2]]);
    append!(pointsY,[Uee1[1,6]]);append!(pointsX,[Uee1[2,6]]);append!(pointsZ,[Uee1[3,6]]);
    Makie.lines!(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:blue)
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,3]]);append!(pointsX,[Uee1[2,3]]);append!(pointsZ,[Uee1[3,3]]);
    append!(pointsY,[Uee1[1,7]]);append!(pointsX,[Uee1[2,7]]);append!(pointsZ,[Uee1[3,7]]);
    Makie.lines!(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:blue)



    E,MISES=getStress3D(copy(Ue))
    E,T=Principal_Strain_3D(E)
    R=RotMatrix{3}(T)
    a=rotation_axis(R)
    q=SVector{3}(1,0,0)
    a=R*q
    x=a[1];y=a[2];z=a[3];
    r = sqrt(x*x + y*y + z*z)
    t = atan(y/x);tθ=t*180/π; #t = angle on x-y plane, 
    p = acos(z/r);pθ=p*180/π; # p = angle off of z-axis
    display("Pricipal Strain direction $(a), θ on x-y plane is $tθ, and from z plane is $pθ")
    # display(R)
    # display(a)

    # pointsX=[];pointsY=[];pointsZ=[];
    # append!(pointsY,[0+unit/2]);append!(pointsX,[0+unit/2]);append!(pointsZ,[0+unit/2]);
    # append!(pointsY,[-a[2]*10+unit/2]);append!(pointsX,[a[1]*10+unit/2]);append!(pointsZ,[a[3]*10+unit/2]);
    # Makie.lines!(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:blue, linestyle = :dash)

    Makie.arrows!([Point3f(unit/2-a[1]*unit/4, -unit/2-a[2]*unit/4, unit/2-a[3]*unit/4)], [Vec3f(a[1]*5, a[2]*5, a[3]*5)], fxaa=true, linecolor = :red, arrowcolor = :red,
    # Makie.arrows!([Point3f(0+unit/2, 0+unit/2, 0+unit/2)], [Vec3f(a[1]*10+unit/2, a[2]*10+unit/2, a[3]*10+unit/2)], fxaa=true, linecolor = :red, arrowcolor = :red,
    linewidth = 0.1, arrowsize = Vec3f(1.0, 1.0, 1.0),
    align = :origin, axis=(type=Axis3,)) #align = :origin



    display(current_figure())
    Makie.inline!(false)
    display(current_figure())
    Makie.inline!(true)
    return E,T

end

function displayElementDeformation3D1(Ue,unit)
    Ue1 =[
        unit,0.0,0, #1
        unit,unit,0, #2
        0,unit,0, #3
        0,0,0, #4
        unit,0,unit, #5 
        unit,unit,unit, #6
        0,unit,unit, #7
        0,0,unit, #8
    ]

    Uee1=reshape(Ue1,3,8)

    fig = Figure(resolution=(300, 300), fontsize=10)

    pointsX=[];pointsY=[];pointsZ=[];
    for i =1:4
        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]]);
    end
    append!(pointsY,[Uee1[1,1]]);append!(pointsX,[Uee1[2,1]]);append!(pointsZ,[Uee1[3,1]]);
    # Makie.lines(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ))
    for i =5:8
        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]])
    end
    append!(pointsY,[Uee1[1,5]]);append!(pointsX,[Uee1[2,5]]);append!(pointsZ,[Uee1[3,5]]);
    Makie.lines(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),color =:black, linestyle = :dash,figure=(resolution=(400, 400), fontsize=10),axis = (; type = Axis3, protrusions = (0, 0, 0, 0),
            viewmode = :fit, limits = (-unit*0.5, unit+unit*0.5, -unit*0.5, unit+unit*0.5, -unit*0.5, unit+unit*0.5)))
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,4]]);append!(pointsX,[Uee1[2,4]]);append!(pointsZ,[Uee1[3,4]]);
    append!(pointsY,[Uee1[1,8]]);append!(pointsX,[Uee1[2,8]]);append!(pointsZ,[Uee1[3,8]]);
    Makie.lines!(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),color =:black, linestyle = :dash)
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,2]]);append!(pointsX,[Uee1[2,2]]);append!(pointsZ,[Uee1[3,2]]);
    append!(pointsY,[Uee1[1,6]]);append!(pointsX,[Uee1[2,6]]);append!(pointsZ,[Uee1[3,6]]);
    Makie.lines!(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),color =:black, linestyle = :dash)
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,3]]);append!(pointsX,[Uee1[2,3]]);append!(pointsZ,[Uee1[3,3]]);
    append!(pointsY,[Uee1[1,7]]);append!(pointsX,[Uee1[2,7]]);append!(pointsZ,[Uee1[3,7]]);
    Makie.lines!(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),color =:black, linestyle = :dash)
    
    
    Ueee=reshape(copy(Ue),3,8)
    Uee1=reshape(Ue1,3,8)
    Ueee[2,:].=-Ueee[2,:];
    Uee1.=Uee1.+Ueee[[2,1,3],:]

    pointsX=[];pointsY=[];pointsZ=[];
    for i =1:4
        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]]);
    end
    append!(pointsY,[Uee1[1,1]]);append!(pointsX,[Uee1[2,1]]);append!(pointsZ,[Uee1[3,1]]);
    # Makie.lines(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ))
    for i =5:8
        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]])
    end
    append!(pointsY,[Uee1[1,5]]);append!(pointsX,[Uee1[2,5]]);append!(pointsZ,[Uee1[3,5]]);
    Makie.lines!(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),color =:blue)
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,4]]);append!(pointsX,[Uee1[2,4]]);append!(pointsZ,[Uee1[3,4]]);
    append!(pointsY,[Uee1[1,8]]);append!(pointsX,[Uee1[2,8]]);append!(pointsZ,[Uee1[3,8]]);
    Makie.lines!(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),color =:blue)
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,2]]);append!(pointsX,[Uee1[2,2]]);append!(pointsZ,[Uee1[3,2]]);
    append!(pointsY,[Uee1[1,6]]);append!(pointsX,[Uee1[2,6]]);append!(pointsZ,[Uee1[3,6]]);
    Makie.lines!(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),color =:blue)
    pointsX=[];pointsY=[];pointsZ=[];
    append!(pointsY,[Uee1[1,3]]);append!(pointsX,[Uee1[2,3]]);append!(pointsZ,[Uee1[3,3]]);
    append!(pointsY,[Uee1[1,7]]);append!(pointsX,[Uee1[2,7]]);append!(pointsZ,[Uee1[3,7]]);
    Makie.lines!(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),color =:blue)



    E,MISES=getStress3D(copy(Ue))
    E,T=Principal_Strain_3D(E)
    R=RotMatrix{3}(T)
    a=rotation_axis(R)
    q=SVector{3}(1,0,0)
    a=R*q
    x=a[1];y=a[2];z=a[3];
    r = sqrt(x*x + y*y + z*z)
    t = atan(y/x);tθ=t*180/π; #t = angle on x-y plane, 
    p = acos(z/r);pθ=p*180/π; # p = angle off of z-axis
    display("Pricipal Strain direction $(a), θ on x-y plane is $tθ, and from z plane is $pθ")
    # display(R)
    # display(a)

    # pointsX=[];pointsY=[];pointsZ=[];
    # append!(pointsY,[0+unit/2]);append!(pointsX,[0+unit/2]);append!(pointsZ,[0+unit/2]);
    # append!(pointsY,[-a[2]*10+unit/2]);append!(pointsX,[a[1]*10+unit/2]);append!(pointsZ,[a[3]*10+unit/2]);
    # Makie.lines!(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:blue, linestyle = :dash)

    Makie.arrows!([Point3f(unit/2-a[1]*unit/4, -(-unit/2-a[2]*unit/4), unit/2-a[3]*unit/4)], [Vec3f(a[1]*5, -a[2]*5, a[3]*5)], fxaa=true, linecolor = :red, arrowcolor = :red,
    # Makie.arrows!([Point3f(0+unit/2, 0+unit/2, 0+unit/2)], [Vec3f(a[1]*10+unit/2, a[2]*10+unit/2, a[3]*10+unit/2)], fxaa=true, linecolor = :red, arrowcolor = :red,
    linewidth = 0.1, arrowsize = Vec3f(1.0, 1.0, 1.0),
    align = :origin, axis=(type=Axis3,)) #align = :origin



    display(current_figure())
    Makie.inline!(false)
    display(current_figure())
    Makie.inline!(true)
    return E,T

end

function displayDeformation3D(Umat,Macro_nely,Macro_nelx,Macro_nelz,unit,Macro_xPhys=ones(Macro_nely,Macro_nelx,Macro_nelz))
    ny=Macro_nely;nx=Macro_nelx;nz=Macro_nelz;
    ord=reshape(1:(Macro_nely*Macro_nelx*Macro_nelz),Macro_nely,Macro_nelx,Macro_nelz);

    fig = Figure(resolution=(500, 500), fontsize=10)
    Makie.scatter(Point3f(0,0,0),
        axis=(type=Axis3, aspect = :data,))
    # Makie.lines!(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:black, linestyle = :dash,figure=(resolution=(400, 400), fontsize=10),axis = (; type = Axis3, protrusions = (0, 0, 0, 0),
    #     viewmode = :fit, limits = (-unit*0.5, unit+unit*0.5, -unit*0.5-unit, unit+unit*0.5-unit, -unit*0.5, unit+unit*0.5)))

    for k in 1:nz
        for i in 1:ny
            for j in 1:nx
                if Macro_xPhys[i,j,k]>0.6
                    Ue=copy(Umat[ord[i,j,k],:]);
                    Ue1 =[
                        unit,0.0,0, #1
                        unit,unit,0, #2
                        0,unit,0, #3
                        0,0,0, #4
                        unit,0,unit, #5 
                        unit,unit,unit, #6
                        0,unit,unit, #7
                        0,0,unit, #8
                    ]

                    Uee1=reshape(copy(Ue1),3,8)
                    Uee1[1,:].+=i*unit;
                    Uee1[2,:].+=j*unit;
                    Uee1[3,:].+=k*unit;

                    # fig = Figure(resolution=(300, 300), fontsize=10)

                    pointsX=[];pointsY=[];pointsZ=[];
                    for i =1:4
                        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]]);
                    end
                    append!(pointsY,[Uee1[1,1]]);append!(pointsX,[Uee1[2,1]]);append!(pointsZ,[Uee1[3,1]]);
                    # Makie.lines(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ))
                    for i =5:8
                        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]])
                    end
                    append!(pointsY,[Uee1[1,5]]);append!(pointsX,[Uee1[2,5]]);append!(pointsZ,[Uee1[3,5]]);
                    Makie.lines!(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),linewidth=0.5,color =:black, linestyle = :dash)
                    # Makie.lines!(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:black, linestyle = :dash,figure=(resolution=(400, 400), fontsize=10),axis = (; type = Axis3, protrusions = (0, 0, 0, 0),
                    #         viewmode = :fit, limits = (-unit*0.5, unit+unit*0.5, -unit*0.5-unit, unit+unit*0.5-unit, -unit*0.5, unit+unit*0.5)))
                    pointsX=[];pointsY=[];pointsZ=[];
                    append!(pointsY,[Uee1[1,4]]);append!(pointsX,[Uee1[2,4]]);append!(pointsZ,[Uee1[3,4]]);
                    append!(pointsY,[Uee1[1,8]]);append!(pointsX,[Uee1[2,8]]);append!(pointsZ,[Uee1[3,8]]);
                    Makie.lines!(Float16.(pointsX),Float16.(pointsY),Float16.(pointsZ),color =:black,linewidth=0.5, linestyle = :dash)
                    pointsX=[];pointsY=[];pointsZ=[];
                    append!(pointsY,[Uee1[1,2]]);append!(pointsX,[Uee1[2,2]]);append!(pointsZ,[Uee1[3,2]]);
                    append!(pointsY,[Uee1[1,6]]);append!(pointsX,[Uee1[2,6]]);append!(pointsZ,[Uee1[3,6]]);
                    Makie.lines!(Float16.(pointsX),Float16.(pointsY),Float16.(pointsZ),color =:black,linewidth=0.5, linestyle = :dash)
                    pointsX=[];pointsY=[];pointsZ=[];
                    append!(pointsY,[Uee1[1,3]]);append!(pointsX,[Uee1[2,3]]);append!(pointsZ,[Uee1[3,3]]);
                    append!(pointsY,[Uee1[1,7]]);append!(pointsX,[Uee1[2,7]]);append!(pointsZ,[Uee1[3,7]]);
                    Makie.lines!(Float16.(pointsX),Float16.(pointsY),Float16.(pointsZ),color =:black,linewidth=0.5, linestyle = :dash)
                    
                    
                    Ueee=reshape(copy(Ue),3,8)
                    Uee1=reshape(copy(Ue1),3,8)
                    Uee1[1,:].+=i*unit;
                    Uee1[2,:].+=j*unit;
                    Uee1[3,:].+=k*unit;
                    # Ueee[2,:].=-Ueee[2,:];
                    Uee1.=Uee1.+Ueee[[2,1,3],:]

                    pointsX=[];pointsY=[];pointsZ=[];
                    for i =1:4
                        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]]);
                    end
                    append!(pointsY,[Uee1[1,1]]);append!(pointsX,[Uee1[2,1]]);append!(pointsZ,[Uee1[3,1]]);
                    # Makie.lines(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ))
                    for i =5:8
                        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);append!(pointsZ,[Uee1[3,i]])
                    end
                    append!(pointsY,[Uee1[1,5]]);append!(pointsX,[Uee1[2,5]]);append!(pointsZ,[Uee1[3,5]]);
                    Makie.lines!(Float16.(pointsX),Float16.(pointsY),Float16.(pointsZ),linewidth=0.5,color =:blue)
                    pointsX=[];pointsY=[];pointsZ=[];
                    append!(pointsY,[Uee1[1,4]]);append!(pointsX,[Uee1[2,4]]);append!(pointsZ,[Uee1[3,4]]);
                    append!(pointsY,[Uee1[1,8]]);append!(pointsX,[Uee1[2,8]]);append!(pointsZ,[Uee1[3,8]]);
                    Makie.lines!(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),linewidth=0.5,color =:blue)
                    pointsX=[];pointsY=[];pointsZ=[];
                    append!(pointsY,[Uee1[1,2]]);append!(pointsX,[Uee1[2,2]]);append!(pointsZ,[Uee1[3,2]]);
                    append!(pointsY,[Uee1[1,6]]);append!(pointsX,[Uee1[2,6]]);append!(pointsZ,[Uee1[3,6]]);
                    Makie.lines!(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),linewidth=0.5,color =:blue)
                    pointsX=[];pointsY=[];pointsZ=[];
                    append!(pointsY,[Uee1[1,3]]);append!(pointsX,[Uee1[2,3]]);append!(pointsZ,[Uee1[3,3]]);
                    append!(pointsY,[Uee1[1,7]]);append!(pointsX,[Uee1[2,7]]);append!(pointsZ,[Uee1[3,7]]);
                    Makie.lines!(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ),linewidth=0.5,color =:blue)



                    E,MISES=getStress3D(copy(Ue))
                    E,T=Principal_Strain_3D(E)
                    R=RotMatrix{3}(T)
                    a=rotation_axis(R)
                    q=SVector{3}(1,0,0)
                    a=R*q
                    x=a[1];y=a[2];z=a[3];
                    r = sqrt(x*x + y*y + z*z)
                    t = atan(y/x);tθ=t*180/π; #t = angle on x-y plane, 
                    p = acos(z/r);pθ=p*180/π; # p = angle off of z-axis
                    # display("Pricipal Strain direction $(a), θ on x-y plane is $tθ, and from z plane is $pθ")
                    # display(R)
                    # display(a)

                    # pointsX=[];pointsY=[];pointsZ=[];
                    # append!(pointsY,[0+unit/2]);append!(pointsX,[0+unit/2]);append!(pointsZ,[0+unit/2]);
                    # append!(pointsY,[-a[2]*10+unit/2]);append!(pointsX,[a[1]*10+unit/2]);append!(pointsZ,[a[3]*10+unit/2]);
                    # Makie.lines!(Float16.(pointsX), -Float16.(pointsY),Float16.(pointsZ),color =:blue, linestyle = :dash)

                    Makie.arrows!([Point3f(unit/2-a[1]*unit/4 +j*unit, -(-unit/2-a[2]*unit/4)+i*unit, unit/2-a[3]*unit/4 +k*unit)], [Vec3f(a[1]*5, -a[2]*5, a[3]*5)], fxaa=true, linecolor = :red, arrowcolor = :red,
                        linewidth = 0.2, arrowsize = Vec3f(1.5, 1.5, 1.5),
                        align = :origin, axis=(type=Axis3,)) #align = :origin
                end

            end
        end
    end      
    Makie.inline!(true)   
    display(current_figure())
    Makie.inline!(false)
    display(current_figure())
    Makie.inline!(true)

end



####################################################

