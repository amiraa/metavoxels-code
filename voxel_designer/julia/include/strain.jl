#======================================================================================================================#
# Amira Abdel-Rahman
# (c) Massachusetts Institute of Technology 2022
#======================================================================================================================#

###################################################################
using Evolutionary


function getPrincipalStrains(Ue)

    #based on: https://community.wvu.edu/~bpbettig/MAE456/Exam_Final_practice_answers.pdf
    # and https://link.springer.com/article/10.1007/s00158-020-02755-5

    E=1;
    ν=0.3; nu=0.27;

    x=0;y=0;#at center
    a=1;b=1;

    #shape functions
    N1=(a-x)*(b-y)/(4*a*b)
    N2=(a+x)*(b-y)/(4*a*b)
    N3=(a+x)*(b+y)/(4*a*b)
    N4=(a-x)*(b+y)/(4*a*b)

    # B = (1/2/l)*[-1 0 1 0 1 0 -1 0; 0 -1 0 -1 0 1 0 1; -1 -1 -1 1 1 1 1 -1];
    # C = E/(1-ν^2)*[1 ν 0; ν 1 0; 0 0 (1-ν)/2];
    # C = E/((1+ν)*(1-2*ν))*[1-ν ν 0; ν 1-ν 0; 0 0 (1-2*ν)/2];

    N=[N1 0 N2 0 N3 0 N4 0; 0 N1 0 N2 0 N3 0 N4];

    #displacement at center
    u,v=N*Ue
    
    #in plane strain in center
    ϵ=1/(4*a*b)*[-(b-y) 0 (b-y) 0 (b+y) 0 -(b+y)  0;
        0 -(a-x) 0 -(a+x) 0 (a+x) 0 (a-x);
        -(a-x) -(b-y) -(a+x) (b-y) (a+x) (b+y) (a-x) -(b+y)]*Ue
    ϵx,ϵy,ϵxy=ϵ

    #in plane stress 
    C = E/(1-ν^2)*[1 ν 0; ν 1 0; 0 0 (1-ν)/2];
    C = E/((1+ν)*(1-2*ν))*[1-ν ν 0; ν 1-ν 0; 0 0 (1-2*ν)/2];
    α=C*ϵ
    αx,αy,τxy=α

    #principal stress 
    α1=(αx+αy)/2+sqrt(((αx-αy)/2)^2+τxy^2)
    α2=(αx+αy)/2-sqrt(((αx-αy)/2)^2+τxy^2)
    α3=ν*(αx+αy) #αz

    #von_mises
    von_mises=1/(sqrt(2))*sqrt((α1-α2)^2+(α2-α3)^2+(α3-α1)^2)
    
    
    #Θ   =atan((2*ϵxy/2),(ϵx-ϵy))/2
    # Θ   =atan((2*τxy),(αx-αy))/2

    
    Θ1   =atan((α1-αx),τxy)
    Θ2   =atan((-τxy),(α1-αx))

    # display(α1)
    # display(α2)

    ratio=min(abs(α1),abs(α2))/max(abs(α1),abs(α2))

    if abs(α1)>=abs(α2)
        # println("compression")
        return Θ1,von_mises, ratio,α1,α2
    else
        # println("tension")
        return Θ2,-von_mises, -ratio,α1,α2
    end
end

function getStrain(x,verbose=false)

    x1=x[1];
    y1=x[2];
    x2=x[3];
    y2=x[4];
    x3=x[5];
    y3=x[6];
    
    Ue=[x1,y1,0,0,x2,y2,x3,y3]

    Θ, von_mises ,ratio,α1,α2=getPrincipalStrains(Ue)
    
    
    # return Θ 
    return Θ ,ratio,α1,α2

end

function optimizeForAngle(thetaD,ratioD=0.0)
    order=[1,2,3,4,5,6,7,8];
    function getStrainTheta(x)
        theta=thetaD*pi/180;
        Θ,ratio=getStrain(x);
        return abs(theta-Θ)
    end
    
    function getStrainTheta1(x)
        theta=thetaD*pi/180;
        x1=x[1];
        y1=x[2];
        x2=x[3];
        y2=x[4];
        x3=x[5];
        y3=x[6];
        
        Ue=normalize([x1,y1,0,0,x2,y2,x3,y3])
        x=[Ue[1],Ue[2],Ue[5],Ue[6],Ue[7],Ue[8]]
        Θ,ratio=getStrain(x);
    
        return  abs(theta-Θ)+abs(ratio-ratioD)
    end
    ratio =-1000;
    Θ =-1000
    α1=-1
    α2=1
    iteration=0
    x=0
    while (abs(ratio-ratioD)>0.1||abs(Θ*180/pi-thetaD)>0.5||α1*α2<0.0) && iteration<10
        x0 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0];
        res =Evolutionary.optimize(getStrainTheta1, x0, CMAES())
        x=res.minimizer;
        x=x/maximum(abs.(x))
        Θ,ratio,α1,α2=getStrain(x)
        display("thetaD $thetaD , Θ $(Θ*180/pi), RATIO $(ratio), α1 $α1 , α2 $α2");
        if abs(ratio-ratioD)>0.1
            display("ratio not reached, try again!!!")
        end
        if abs(Θ*180/pi-thetaD)>0.5
            display("theta not reached, try again!!!")
        end
        if α1*α2<0.0
            display("negative ratio, try again!!!")
        end
        iteration+=1
    end
    
    x1=x[1];y1=x[2];x2=x[3];y2=x[4];x3=x[5];y3=x[6];
    Ue=normalize([x1,y1,0,0,x2,y2,x3,y3])

    points,pointsU=displayElementDeformation(10,Ue,order,true)

    return Ue
end


###################################################################

function displayAllUPlotGrid(disss)
    plotss=[]
    dims=8
    for j=1:dims
        for i=1:length(disss)
            append!(plotss,[Plots.heatmap(disss[i][:,:,j],showaxis = false,xaxis=nothing,yaxis=nothing,legend = false,aspect_ratio=:equal,
                        top_margin = Plots.Measures.Length{:mm,Float64}(-2),
                        bottom_margin = Plots.Measures.Length{:mm,Float64}(-2),
                        right_margin = Plots.Measures.Length{:mm,Float64}(-4),
                        left_margin = Plots.Measures.Length{:mm,Float64}(-4)
                        )]);
        end
    end
    display(Plots.plot(plotss..., layout = (dims, length(disss))))
end

function plotStrain(nx,ny,unit,diss,order,UX,UY)
    Plots.plot()

    for i=1:ny
        for j=1:nx
            points,pointsU=displayElementDeformation(unit,diss[ny-i+1,j,:],order,false)
            Plots.plot!(points[:,1].+j*unit,points[:,2].+i*unit,legend=false,aspect_ratio=:equal,linecolor=:black)
            Plots.plot!(points[:,1].-pointsU[1,1].+pointsU[:,1].+j*unit,points[:,2].-pointsU[1,2].+pointsU[:,2].+i*unit,legend=false,aspect_ratio=:equal,linestyle=:dash,linecolor=:blue)

        end
    end
    display(Plots.plot!())

    Plots.plot()
    for i=1:ny
        for j=1:nx
            points,pointsU=displayElementDeformation(unit,diss[ny-i+1,j,:],order,false)
            Plots.plot!(points[:,1].+j*unit.-unit,points[:,2].+i*unit.-unit,legend=false,aspect_ratio=:equal,linecolor=:black)
            Plots.plot!(points[:,1].+pointsU[:,1].+j*unit.-unit,points[:,2].+pointsU[:,2].+i*unit.-unit,legend=false,aspect_ratio=:equal,linestyle=:dash,linecolor=:blue)

        end
    end
    Plots.plot!()

    xs = (0:unit:(Int(ny))*unit)
    ys = (0:unit:(Int(nx))*unit)
    xxs = [x for x in xs for y in ys]
    yys = [y for x in xs for y in ys]

    # UX=UX*2
    # UY=UY*2

    
    ress(x,y)=(UX'[Int((x/unit)+1),end+1-Int((y/unit)+1)], UY'[ Int((x/unit)+1),end+1-Int((y/unit)+1)])
    col(x,y)=norm(res(x,y))
    xxs = [x for x in xs for y in ys]
    yys = [y for x in xs for y in ys]

    display(Plots.quiver!( yys,xxs, quiver=ress,c=:cyan))
    
    xs = (0:unit:(Int(ny))*unit)
    ys = (0:unit:(Int(nx))*unit)
    xxs = [x for x in xs for y in ys]
    yys = [y for x in xs for y in ys]
    Plots.plot()
    
    for i=1:ny
        for j=1:nx
            points,pointsU=displayElementDeformation(unit,diss[ny-i+1,j,:],order,false)
            Plots.plot!(points[:,1].+j*unit.-unit,points[:,2].+i*unit.-unit,legend=false,aspect_ratio=:equal,linecolor=:black)
        end
    end
    Plots.plot!()

    ress1(x,y)=(UX'[Int((x/unit)+1),Int((y/unit)+1)]/5, UY'[Int((x/unit)+1),Int((y/unit)+1)]/5)


    display(Plots.quiver!( yys,xxs, quiver=ress1,c=:cyan))
end

function displayElementDeformation(unit,disss,order,preview)
    gr(size=(150,100))
    points=zeros(5,2)
    pointsU=zeros(5,2)
    
    points[1,1]=0;
    points[1,2]=0;
    
    points[2,1]=unit;
    points[2,2]=0;
    
    points[3,1]=unit;
    points[3,2]=unit;
    
    points[4,1]=0;
    points[4,2]=unit;
    
    points[5,:].=points[1,:];


    lines=[]

    pointsU[1,1]=disss[order[1]]
    pointsU[1,2]=disss[order[2]]
    
    pointsU[2,1]=disss[order[3]]
    pointsU[2,2]=disss[order[4]]

    pointsU[3,1]=disss[order[5]]
    pointsU[3,2]=disss[order[6]]

    pointsU[4,1]=disss[order[7]]
    pointsU[4,2]=disss[order[8]]


    pointsU[5,:].=pointsU[1,:];
    

    if preview
        # Plots.plot(points[:,2],points[:,1],legend=false, aspect_ratio=:equal,linecolor=:black)
        # display(Plots.plot!(points[:,2]+pointsU[:,2],points[:,1]+pointsU[:,1],legend=false,linecolor=:blue))

        # Plots.plot(points[:,2],points[:,1],legend=false,xlim=(-1*unit,1*unit),ylim=(-1*unit,1*unit), aspect_ratio=:equal)
        Plots.plot((points[:,2]),points[:,1],legend=false, aspect_ratio=:equal,linecolor=:black)
        display(Plots.plot!((points[:,2]+pointsU[:,2].-pointsU[1,2]),points[:,1]+pointsU[:,1].-pointsU[1,1],legend=false,linecolor=:blue))
    end
    gr(size=(400,300))
    return points,pointsU
end

function getShapeStrainDisplacement(Ue,x,y)
    # i=1;j=1;
    # Ue=diss[i,j,:];
    # Ue=[0,0,2,0,1,-1,0,0]*10e-6;


    E=1
    ν=0.3;nu=0.27


    a=1;
    b=1;

    N1=(a-x)*(b-y)/(4*a*b)
    N2=(a+x)*(b-y)/(4*a*b)
    N3=(a+x)*(b+y)/(4*a*b)
    N4=(a-x)*(b+y)/(4*a*b)

    N=[N1 0 N2 0 N3 0 N4 0; 0 N1 0 N2 0 N3 0 N4];

    u,v=N*Ue
    
    #in plane strain in center
    ϵ=1/(4*a*b)*[-(b-y) 0 (b-y) 0 (b+y) 0 -(b+y)  0;
        0 -(a-x) 0 -(a+x) 0 (a+x) 0 (a-x);
        -(a-x) -(b-y) -(a+x) (b-y) (a+x) (b+y) (a-x) -(b+y)]*Ue
    ϵx,ϵy,ϵxy=ϵ

    #in plane stress 
    C = E/(1-ν^2)*[1 ν 0; ν 1 0; 0 0 (1-ν)/2];
    C = E/((1+ν)*(1-2*ν))*[1-ν ν 0; ν 1-ν 0; 0 0 (1-2*ν)/2];
    α=C*ϵ
    αx,αy,τxy=α

    #principal stress 
    α1=(αx+αy)/2+sqrt(((αx-αy)/2)^2+τxy^2)
    α2=(αx+αy)/2-sqrt(((αx-αy)/2)^2+τxy^2)
    α3=ν*(αx+αy)
    α4=ν*(αx+αy)

    #von_mises
    von_mises=1/(sqrt(2))*sqrt((α1-α2)^2+(α2-α3)^2+(α3-α1)^2)
    
    
    #Θ   =atan((2*ϵxy/2),(ϵx-ϵy))/2
    Θ   =atan((2*τxy),(αx-αy))/2
    
    if α2<0 && α1>0
        Θ   +=π
    end
    
    
    if abs(α1)<abs(α2)
        Θ   -=π/2
    end

    return Θ
    
    # if main
    #     return (cos(Θ)/scale, sin(Θ)/scale)
    # else
    #     return (cos(Θ+ π/2)/scale, sin(Θ + π/2)/scale)
    # end
end

###################################################################


function displayElementDeformation2D(Ue,unit)
    Ue1 =[
        unit,1, #1
        unit,unit, #2
        0,unit, #3
        0,0 #4

    ]

    Uee1=reshape(Ue1,2,4)

    fig = Figure(resolution=(100, 100), fontsize=10)

    pointsX=[];pointsY=[];
    for i =1:4
        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);
    end
    append!(pointsY,[Uee1[1,1]]);append!(pointsX,[Uee1[2,1]]);
    
    Makie.lines(Float16.(pointsX), Float16.(pointsY),color =:black, linestyle = :dash,figure=(resolution=(300, 300), fontsize=10),axis = (; limits = (-unit, 2*unit, -unit, 2*unit)))
    Ueee=reshape(Ue,2,4)
    Uee1=reshape(Ue1,2,4)
    Uee1.=Uee1.+Ueee[[2,1],:]

    pointsX=[];pointsY=[];
    for i =1:4
        append!(pointsY,[Uee1[1,i]]);append!(pointsX,[Uee1[2,i]]);
    end
    append!(pointsY,[Uee1[1,1]]);append!(pointsX,[Uee1[2,1]]);
    # Makie.lines(Float16.(pointsX), Float16.(pointsY),Float16.(pointsZ))
 
    Makie.lines!(Float16.(pointsX), Float16.(pointsY),color =:blue)
    

    display(current_figure())
end
###################################################################


function fetchMicrostructureConnect(theta,library)
    ratioD=library[1];Micro_Vol=library[2];num=Int(library[3]);
    findnearest(A::AbstractArray,t) = findmin(abs.(A.-t))[2]
    values=[]
    for i in range(0.0, 45, length=num)
        append!(values,[i])
    end
    function fetchMicrostructureConnect1(thetaD)
        thetaD=values[findnearest(values,thetaD)]
        # n=load("./img/library/111/MinS_2_MinV_11_MaxS_8_Theta_$(thetaD)_vol_$(Micro_Vol).jld")["data"];
        n=load("./img/library/1/Connect_Micro_theta_$(thetaD)_ratio_$(ratioD)_vol_$(Micro_Vol).jld")["data"];

        
        

        return n
    end
    
    if theta>180
        theta=(theta%360)-180
    end

    n=fetchMicrostructureConnect1(theta)
    

    if theta>45 && theta <=90
        n=fetchMicrostructureConnect1(abs(theta-45-45))
        n=rotr90(n)
        n.=n[end:-1:1,:]
    elseif theta>90 && theta <=135
        n=fetchMicrostructureConnect1(theta-90)
        n=rotr90(n)
    elseif theta>135 && theta <=180
        n=fetchMicrostructureConnect1(abs(theta-135-45))
        n.=n[end:-1:1,:]
    end
        
    
    # gr(size=(800/10,600/10))
    # display(Plots.heatmap(1.0.-n,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, 1),fc=:greys,aspect_ratio=:equal))
    # gr(size=(800,600))
    return n
end

###################################################################

function loadVisualizeLibrary(library)

    
    ratioD=library[1];Micro_Vol=library[2];num=Int(library[3]);
    θ=Int((num-1)*4+1)

    display("Loaded $num library microstructures with ratio $ratioD and volume $Micro_Vol !")
    Micro_xPhys=[]
    DHs=[]
    for i in range(0.0, 180, length=θ)
        
        append!(DHs,[[]])
        append!(Micro_xPhys,[fetchMicrostructureConnect(i,library)])
    end

    cuboct=load("./img/library/1/cuboct_vol_$((Micro_Vol)).jld")["data"];
    append!(DHs,[[]])
    append!(Micro_xPhys,[cuboct])
    θ+=1


    E0 = 1; Emin = 1e-9; nu = 0.3;penal=3;Micro_length=1.0;Micro_width=1.0;
    for i=1:θ
        DH, dDH = EBHM2D(Micro_xPhys[i], Micro_length, Micro_width, E0, Emin, nu, penal);
        DHs[i]=DH;
    end

    # t=:dark
    t=:darktest
    # # theme(t);
    # cschemeList=[]
    # for i=1:θ
    #     append!(cschemeList,[palette(t)[i]])
    # end
    # # cscheme=ColorGradient(cschemeList)
    # cscheme=cgrad(cschemeList)
    cscheme=cgrad(t, θ-1, categorical = true)
    t=:matter
    cschemes=cgrad(t, num+1, categorical = true)
    cschemeLists=[]
    append!(cschemeLists,[:white])
    for i=num:-1:1
        append!(cschemeLists,[cschemes[i]])
    end
    for i=2:num
        append!(cschemeLists,[cschemes[i]])
    end
    for i=num-1:-1:1
        append!(cschemeLists,[cschemes[i]])
    end
    for i=2:num
        append!(cschemeLists,[cschemes[i]])
    end
    append!(cschemeLists,[:black])
    cschemes1=cgrad(cschemeLists)

    t=:darktest
    # theme(t);
    cschemeList=[]
    append!(cschemeList,[:white])
    for i=1:θ-1
        append!(cschemeList,[cscheme[i]])
    end
    append!(cschemeList,[:black])
    # cscheme=ColorGradient(cschemeList)
    cscheme1=cgrad(cschemeList)
    Micro_nely=size(Micro_xPhys[1])[1]
    space=fill(0,Micro_nely,1)
    Micro_xPhys_all=space;
    for i=1:θ
        Micro_xPhys_temp=copy(Micro_xPhys[i])
        Micro_xPhys_temp[Micro_xPhys_temp.<0.75].=0.0
        Micro_xPhys_temp[Micro_xPhys_temp.>=0.75].=i
        Micro_xPhys_all=hcat(Micro_xPhys_all,Micro_xPhys_temp);
        Micro_xPhys_all=hcat(Micro_xPhys_all,space);
    end
    gr(size=(40*θ,100))
    display(Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cschemes1,aspect_ratio=:equal))

    display(Plots.heatmap(Micro_xPhys_all,showaxis = false,xaxis=nothing,yaxis=nothing,legend=nothing,clims=(0, θ),fc=cscheme1,aspect_ratio=:equal))

    gr(size=(400,300))
    return Micro_xPhys,DHs
end

