// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

var gridSize=10;
var occupancy=[];

function createTruss(truss){ //todo move to geometry library
    
    var setupEmpty={//empty
        nodes: [
            ],
        edges: [
            ],
    
        //material properties - AISI 1095 Carbon Steel (Spring Steel)
        ndofs   : 3*6,
    
        animation :  {
        
            showDisplacement : true,
            exaggeration : 20e2,
            speed:3.0
            
        },
        viz :  {
            minStress:10e6,
            maxStress: -10e6,
            colorMaps:[coolwarm,YlGnBu, winter ,jet],
            colorMap:0,
            
        },
        
    };
    // console.log(setupEmpty)
    var setup=JSON.parse(JSON.stringify(setupEmpty));

    var material={
        area:2.38*2.38/5,
        density:0.028,
        stiffness:4000
    };
    var force=new THREE.Vector3(0,0,0);
    var restrained=false;
    var fixed=[true,true,true,true,true,true];
    var free=[false,false,false,true,true,true];
    var rollerX=[true,false,false,true,true,true];
    var rollerY=[false,true,false,true,true,true];
    var rollerZ=[false,false,true,true,true,true];

    for(var i=0;i<truss.nnx;i++){
        for(var j=0;j<truss.nny;j++){
            for(var k=0;k<truss.nnz;k++){
                //example 1
                // if ((i==0&& j==0) ||(i==truss.nnx-1&& j==0)){
                //     restrained=true;
                // }else{
                //     restrained=false;
                // }
                // if((i==1&& j==0) ){
                //     force=new THREE.Vector3(0,-10,0);
                // }else{
                //     force=new THREE.Vector3(0,0,0);
                // }

                // //example 2
                // if ((i==0)){
                //     restrained=true;
                // }else{
                //     restrained=false;
                // }
                // if((i==4&& j==1) ){
                //     force=new THREE.Vector3(0,-10,0);
                // }else{
                //     force=new THREE.Vector3(0,0,0);
                // }

                // //example 3
                // if ((i==0&& j==0) ||(i==truss.nnx-1&& j==0)){
                //     restrained=true;
                // }else{
                //     restrained=false;
                // }
                // if(j==0 && (i!=0&&i!=truss.nnx-1) ){
                //     force=new THREE.Vector3(0,-10,0);
                // }else{
                //     force=new THREE.Vector3(0,0,0);
                // }

                // inverter
                // if ((i==0&& j==0) ||(i==0&& j==truss.nny-1)){
                //     restrained=true;
                // }else{
                //     restrained=false;
                // }
                // if((i==0&& j==1) ){
                //     force=new THREE.Vector3(10,0,0);
                // }else if((i==2&& j==1)){
                //     force=new THREE.Vector3(-10,0,0);
                // }else{
                //     force=new THREE.Vector3(0,0,0);
                // }

                // gripper
                // if ((i==0&& j==0) ||(i==0&& j==truss.nny-1)){
                //     restrained=true;
                // }else{
                //     restrained=false;
                // }
                // if((i==0&& j==2) ){
                //     force=new THREE.Vector3(10,0,0);
                // }else if((i==(truss.nnx-1)&& j==1)){
                //     force=new THREE.Vector3(0,10,0);
                // }else if((i==(truss.nnx-1)&& j==3)){
                //     force=new THREE.Vector3(0,-10,0);
                // }else{
                //     force=new THREE.Vector3(0,0,0);
                // }

                //3d
                // if((i==1&& j==0&&k==1) ){
                //     force=new THREE.Vector3(0,-10,0);
                // }else{
                //     force=new THREE.Vector3(0,0,0);
                // }


                //3d inverter & gripper
                if((i==1&& j==0&&k==1) ){
                    force=new THREE.Vector3(0,10,0);
                }else{
                    force=new THREE.Vector3(0,0,0);
                }



                addNode(setup,new THREE.Vector3(i*truss.dx,j*truss.dy,k*truss.dz),restrained,force,material);

                //3d 
                if (j==0 && (k==1||i==1)&&!(k==1&&i==1)){
                    setup.nodes[setup.nodes.length-1].restrained_degrees_of_freedom=fixed;
                }else{
                    setup.nodes[setup.nodes.length-1].restrained_degrees_of_freedom=free;
                }

                //3d inverter
                // if((i==1&& j==truss.nny-1&&k==1) ){
                //     setup.nodes[setup.nodes.length-1].fixedDisplacement=new THREE.Vector3(0,1,0);
                // }

                //3d gripper
                if(j==truss.nny-1){
                    if((i==1&&k==0) ){
                        setup.nodes[setup.nodes.length-1].fixedDisplacement=new THREE.Vector3(0,0,-1);
                    }else if((i==0&&k==1) ){
                        setup.nodes[setup.nodes.length-1].fixedDisplacement=new THREE.Vector3(-1,0,0);
                    }else if((i==1&&k==2) ){
                        setup.nodes[setup.nodes.length-1].fixedDisplacement=new THREE.Vector3(0,0,1);
                    }else if((i==2&&k==1) ){
                        setup.nodes[setup.nodes.length-1].fixedDisplacement=new THREE.Vector3(1,0,0);
                    }

                }
                

            }
        }
    }

    if(truss.localConnections){
        for(var i=0;i<truss.nnx*truss.nny*truss.nnz;i++){
            var x=parseInt(setup.nodes[i].position.x/truss.dx);
            var y=parseInt(setup.nodes[i].position.y/truss.dy);
            var z=parseInt(setup.nodes[i].position.z/truss.dz);
            
            if(y!=truss.nny-1){
                var x1=x;
                var y1=y+1;
                var z1=z;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }
            if(x!=truss.nnx-1){
                var x1=x+1;
                var y1=y;
                var z1=z;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }
            if(x!=truss.nnx-1 &&y!=truss.nny-1 ){
                var x1=x+1;
                var y1=y+1;
                var z1=z;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }
            if(x!=truss.nnx-1 && y>0){
                var x1=x+1;
                var y1=y-1;
                var z1=z;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }

            if(z!=truss.nnz-1){
                var x1=x;
                var y1=y;
                var z1=z+1;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }

            if(z!=truss.nnz-1&&y!=truss.nny-1){
                var x1=x;
                var y1=y+1;
                var z1=z+1;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }

            if(z!=truss.nnz-1&&x!=truss.nnx-1){
                var x1=x+1;
                var y1=y;
                var z1=z+1;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }

            if(z!=truss.nnz-1&&y>0){
                var x1=x;
                var y1=y-1;
                var z1=z+1;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }

            if(z!=truss.nnz-1&&x>0){
                var x1=x-1;
                var y1=y;
                var z1=z+1;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }

            if(z!=truss.nnz-1&&y!=truss.nny-1&&x!=truss.nnx-1){
                var x1=x+1;
                var y1=y+1;
                var z1=z+1;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }

            if(z!=truss.nnz-1&&y>0&&x>0){
                var x1=x-1;
                var y1=y-1;
                var z1=z+1;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }

            if(z!=truss.nnz-1&&y>0&&x!=truss.nnx-1){
                var x1=x+1;
                var y1=y-1;
                var z1=z+1;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }

            if(z!=truss.nnz-1&&y!=truss.nny-1&&x>0){
                var x1=x-1;
                var y1=y+1;
                var z1=z+1;
                addEdge(setup,x*truss.nnz*truss.nny+y*truss.nnz+z,(x1)*truss.nnz*truss.nny+(y1)*truss.nnz+z1,material);
            }



            
   
        }

        

    }else{
        for(var i=0;i<truss.nnx*truss.nny*truss.nnz-1;i++){
            for(var j=0;j<truss.nnx*truss.nny*truss.nnz;j++){
                if(i!=j){
                    addEdge(setup,i,j,material);
                }
            }
        }
    }
    




    
    setup.hierarchical=false;

    // var data = {
    //     key: 'value'
    // };
    truss.setup=setup;
    
    

    
    var materials=[];
    var supports=[];
    var loads=[];
    var fixedDisplacements=[];
    // setup.materials=json.materials;
    // setup.supports=json.supports;
    // setup.loads=json.loads;
    // setup.fixedDisplacements=json.fixedDisplacements;

    // var materials1=json.materials;
    // var supports1=json.supports;
    // var loads1=json.loads;
    // var fixedDisplacements1=json.fixedDisplacements;
    // supports=[
    //     [

    //     ]
    // ];
    // var materials1=[];
    // var supports1=[];
    // var loads1=[];
    // var fixedDisplacements1=[];

    // for (var i=1;i<materials1.length;i++ ){
    //     var material1=materials1[i];
    //     var boundingMaterial=new _rhino3dm.BoundingBox(
    //         [
    //             material1[0].min.x,
    //             material1[0].min.y,
    //             material1[0].min.z
    //         ], 
    //         [
    //             material1[0].max.x,
    //             material1[0].max.y,
    //             material1[0].max.z
    //     ]);
    //     materials.push([ boundingMaterial,material1[1]]);
    // }

    // for (var i=0;i<supports1.length;i++ ){
    //     var support1=supports1[i];
    //     var boundingSupport=new _rhino3dm.BoundingBox(
    //         [
    //             support1[0].min.x,
    //             support1[0].min.y,
    //             support1[0].min.z
    //         ], 
    //         [
    //             support1[0].max.x,
    //             support1[0].max.y,
    //             support1[0].max.z
    //     ]);
    //     supports.push([ boundingSupport,support1[1]]);


    // }

    // for (var i=0;i<loads1.length;i++ ){
    //     var load1=loads1[i];
    //     var boundingLoad=new _rhino3dm.BoundingBox(
    //         [
    //             load1[0].min.x,
    //             load1[0].min.y,
    //             load1[0].min.z
    //         ], 
    //         [
    //             load1[0].max.x,
    //             load1[0].max.y,
    //             load1[0].max.z
    //     ]);
    //     loads.push([ boundingLoad,load1[1]]);
    // }

    // for (var i=0;i<fixedDisplacements1.length;i++ ){
    //     var fixedDisplacement1=fixedDisplacements1[i];
    //     var boundingFixedDisplacement=new _rhino3dm.BoundingBox(
    //         [
    //             fixedDisplacement1[0].min.x,
    //             fixedDisplacement1[0].min.y,
    //             fixedDisplacement1[0].min.z
    //         ], 
    //         [
    //             fixedDisplacement1[0].max.x,
    //             fixedDisplacement1[0].max.y,
    //             fixedDisplacement1[0].max.z
    //     ]);
    //     fixedDisplacements.push([ boundingFixedDisplacement,fixedDisplacement1[1]]);
    // }


    // changeMaterialFromBox(setup,materials);
    // restrainFromBox(setup,supports);
    // loadFromBox(setup,loads);
    // displacementFromBox(setup,fixedDisplacements);
    
    

    // // setup.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
    // setup.viz.minStress=10e6;
    // setup.viz.maxStress=-10e6;

    // setup.viz.exaggeration=1.0;
    // setup.animation.exaggeration=1.0;
    // setup.viz.colorMaps=[];

    // setup.numTimeSteps=json.numTimeSteps;
    // setup.maxNumFiles=json.maxNumFiles;


    // setup.poisson=json.poisson;
    // setup.scale=json.scale;
    // setup.linear=json.linear;
    // setup.globalDamping=json.globalDamping;
    // setup.thermal=json.thermal;
    

    // saveJSON();
    // console.log(setup)
    return setup;

    

    console.log("Success!")
    

}


