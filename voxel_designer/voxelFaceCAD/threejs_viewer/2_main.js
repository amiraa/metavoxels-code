var cschemeList;
var faces=[];
var Macro_xPhys;
var Micro_xPhys;
var a,aaX,aaY,aaZ;
var thickness=0.1/2;
var shift=0.5;


function init() {
	var scene = new THREE.Scene();
	// var gui = new dat.GUI();

	
	var plane = getPlane(30);
	

	plane.name = 'plane-1';
	plane.rotation.x = Math.PI/2;
	plane.position.y = -shift-thickness;
	// scene.add(plane);
	// scene.add(boxGrid);
	
	////////////////1/////////////////////

	var light = new THREE.AmbientLight(0xaaaaaa);
	// light.intensity = 2;
	scene.add(light);
	// var light2 = new THREE.DirectionalLight(0xaaaaaa);
	// light2.position.set(1, 10, 10);
	// scene.add(light2);

	var pointLight = getPointLight(1);
	pointLight.position.y = 10;
	pointLight.position.z = 0;
	pointLight.intensity = 1;
	// pointLight.add(sphere);
	scene.add(pointLight);
	/////////////////////////////////////

	$.getJSON("../../finalCantelever3D.json", function(json) {
		console.log(json)

		cschemeList=json.cschemeList;
		Macro_xPhys=json.Macro_xPhys;
		Micro_xPhys=json.Micro_xPhys;
		a=json.a;
		aaY=json.aaY;
		aaX=json.aaX;
		aaZ=json.aaZ;
		// console.log(a)

		var theta=Micro_xPhys.length;
		for (var i=1;i<=theta;i++){
			// loadFace(i);
			// loadFace(i,0,i,0,Math.PI/2,0,0)
		}
		var Macro_nely=Macro_xPhys.length;
		var Macro_nelx=Macro_xPhys[0].length;
		var Macro_nelz=Macro_xPhys[0][0].length;
		console.log(Macro_nely)
		console.log(Macro_nelx)
		console.log(Macro_nelz)

		var count=0;
		
		var b=getBox(6, (Macro_nelx+2)/2+2, 2);
		b.position.z=-1;
		b.position.x= -0.5;
		b.position.y= (Macro_nelx+2)/4;
		scene.add(b)
		

		for(i=0;i<Macro_nely;i++){
			for(j=0;j<Macro_nelx;j++){
				for(k=0;k<Macro_nelz;k++){
					if(Macro_xPhys[i][j][k]>0.75){
						var aa=count*6;

						loadFace(aaX[(j)*2+0][i][k],i,k,j+thickness,0,0,0)
						loadFace(aaX[(j)*2+1][i][k],i,k,j+1-thickness,0,0,0)
						
						loadFace(aaY[(k)*2+0][i][j],i,k-shift+thickness,j+shift,Math.PI/2,0,0)
						loadFace(aaY[(k)*2+1][i][j],i,k+1-shift-thickness,j+shift,Math.PI/2,0,0)
						
						loadFace(aaZ[(i)*2+0][j][k],i-shift+thickness,k,j+shift,Math.PI/2,Math.PI/2,0)
						loadFace(aaZ[(i)*2+1][j][k],i+1-shift-thickness,k,j+shift,Math.PI/2,Math.PI/2,0)

						loadFace(aaX[(j)*2+0][i][k],-i-1,k,j+thickness,0,0,0,-1)
						loadFace(aaX[(j)*2+1][i][k],-i-1,k,j+1-thickness,0,0,0,-1)
						
						loadFace(aaY[(k)*2+0][i][j],-i-1,k-shift+thickness,j+shift,Math.PI/2,0,0,-1)
						loadFace(aaY[(k)*2+1][i][j],-i-1,k+1-shift-thickness,j+shift,Math.PI/2,0,0,-1)
						
						loadFace(aaZ[(i)*2+0][j][k],-(i-shift+thickness)-1,k,j+shift,Math.PI/2,Math.PI/2,0,-1)
						loadFace(aaZ[(i)*2+1][j][k],-(i+1-shift-thickness)-1,k,j+shift,Math.PI/2,Math.PI/2,0,-1)

						
						// loadFace(aaX[(j)*2+0][i][k],i,-k+Macro_nelz,j+thickness,0,0,0)
						// loadFace(aaX[(j)*2+1][i][k],i,-k+Macro_nelz,j+1-thickness,0,0,0)
						
						// loadFace(aaY[(k)*2+0][i][j],i,-k+Macro_nelz+shift-thickness,j+shift,Math.PI/2,0,0)
						// loadFace(aaY[(k)*2+1][i][j],i,-k+Macro_nelz-1+shift+thickness,j+shift,Math.PI/2,0,0)
						
						// loadFace(aaZ[(i)*2+0][j][k],i-shift+thickness,-k+Macro_nelz,j+shift,Math.PI/2,Math.PI/2,0)
						// loadFace(aaZ[(i)*2+1][j][k],i+1-shift-thickness,-k+Macro_nelz,j+shift,Math.PI/2,Math.PI/2,0)

					}
					count++;
			
				}
			}
		}
		console.log(count)
		console.log(a.length)

		
	});
	


	var camera = new THREE.PerspectiveCamera(
		45,
		window.innerWidth/window.innerHeight,
		1,
		1000
	);

	camera.position.x = 0;
	camera.position.y = 4;
	camera.position.z = 15;

	camera.lookAt(new THREE.Vector3(0, 0, 0));

	var renderer = new THREE.WebGLRenderer({
		antialias: true, // to get smoother output
		preserveDrawingBuffer: false, // no screenshot -> faster?
	  });
	// renderer.shadowMap.enabled = true;
	renderer.setSize(window.innerWidth, window.innerHeight);
	// renderer.setClearColor('rgb(120, 120, 120)');
	renderer.setClearColor(0xffffff);
	document.getElementById('webgl').appendChild(renderer.domElement);

	var controls = new THREE.OrbitControls(camera, renderer.domElement);

	update(renderer, scene, camera, controls);

	return scene;
}

function update(renderer, scene, camera, controls) {
	renderer.render(
		scene,
		camera
	);

	controls.update();

	requestAnimationFrame(function() {
		update(renderer, scene, camera, controls);
	})
}
function getPointLight(intensity) {
	var light = new THREE.PointLight(0xffffff, intensity);
	light.castShadow = true;

	return light;
}
function getPlane(size) {
	var geometry = new THREE.PlaneGeometry(size, size);
	var material = new THREE.MeshPhongMaterial({
		color: 'rgb(120, 120, 120)',
		side: THREE.DoubleSide
	});
	var mesh = new THREE.Mesh(
		geometry,
		material 
	);
	mesh.receiveShadow = true;

	return mesh;
}

function loadFace(name,x,y,z,rx,ry,rz,flip=1) {
	
	// console.log(cschemeList[name-1])
	var i=name-1;
	var color='rgb('+parseInt(cschemeList[i][0]*255) +', '+parseInt(cschemeList[i][1]*255) +','+parseInt(cschemeList[i][2]*255)  +')';
	// console.log(color)
	// instantiate a loader
	const loader = new THREE.OBJLoader();
	var material = new THREE.MeshPhongMaterial({
		color: color,
		side:THREE.DoubleSide
	});

	// load a resource
	loader.load(
		// resource URL
		'obj/'+name+'.obj',
		// called when resource is loaded
		function ( object ) {

			// object.scale.x = -0.5;
			object.scale.x = flip*0.5;
			object.scale.y = 0.5;
			object.scale.z = thickness*2;

			// object.rotation.x=Math.PI/2;

			object.rotation.x=rx;
			object.rotation.y=ry;
			object.rotation.z=rz;

			object.position.x=x;
			object.position.y=y;
			object.position.z=z;

			

			object.name=name;
			object.children[0].receiveShadow = true;
			object.children[0].material=material;
			
			// faces.push(object)
			scene.add( object );

		},
		// called when loading is in progresses
		function ( xhr ) {

			// console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

		},
		// called when loading has errors
		function ( error ) {

			// console.log( 'An error happened' );

		}
	);
	
}

function getBox(w, h, d) {
	var geometry = new THREE.BoxGeometry(w, h, d);
	var material = new THREE.MeshPhongMaterial({
		color: 'rgb(120, 120, 120)'
	});
	var mesh = new THREE.Mesh(
		geometry,
		material 
	);
	mesh.castShadow = true;

	return mesh;
}

var scene = init();