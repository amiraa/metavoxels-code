// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

const { exec } = require("child_process");

var express = require('express');
var fs = require('fs');
var app = express();
var path = require('path');
var setup,cmods;

// will parse incoming JSON data and convert it into an object literal for you
app.use(express.json({limit: '50mb', extended: true}));
app.use(express.urlencoded({limit: '50mb', extended: true}));

//serve the html
app.use(express.static(__dirname + '/../')); // exposes index.html, per below


app.post("/", function(req, res) {
  // each key in req.body will match the keys in the data object that you passed in
  var myObject = req.body.data;
  setup=JSON.parse(myObject.foo);
  cmods=setup.cmods;
  
  if(setup.run){
    console.log("run CMODS!");
    runCMODS();
  }else{
    console.log("reading data");
  }

  // console.log(setup);
  // myObject.foo === "bar"
  res.send("I am done");
});

port = 8080;
app.listen(port);
console.log(`Open http://localhost:${port}/demos/indexCMODS.html in your browser`);



function runCMODS(){
  var STL_particles_command_old= "./STL_particles "+cmods.STL_particles.nlattice+" < "+cmods.STL_particles.stlName+".stl";
  var particles_bonds_command_old= " | ./particles_bonds "+cmods.particles_bonds.grid+" "+cmods.particles_bonds.block+"";
  var bonds_stress_strain_command1_old= " | ./bonds_stress_strain1 "+cmods.bonds_stress_strain.grid+" "+cmods.bonds_stress_strain.block+" "+cmods.bonds_stress_strain.spring+" "
      +cmods.bonds_stress_strain.mass+" "+cmods.bonds_stress_strain.dissipation
      +" "+cmods.bonds_stress_strain.dt+" "+cmods.bonds_stress_strain.nloop
      +" "+cmods.bonds_stress_strain.fraction+" "+cmods.bonds_stress_strain.fraction+" "+cmods.bonds_stress_strain.step
      +" "+cmods.bonds_stress_strain.bond+"";
  var bonds_stress_strain_command_old= " | ./bonds_stress_strain "+cmods.bonds_stress_strain.grid+" "+cmods.bonds_stress_strain.block+" "+cmods.bonds_stress_strain.spring+" "
      +cmods.bonds_stress_strain.mass+" "+cmods.bonds_stress_strain.dissipation
      +" "+cmods.bonds_stress_strain.dt+" "+cmods.bonds_stress_strain.nloop
      +" "+cmods.bonds_stress_strain.fraction+" "+cmods.bonds_stress_strain.step
      +" "+cmods.bonds_stress_strain.bond+"";
  var strain_GL_command_old= " | ./strain_GL "+cmods.strain_GL.size+" "+cmods.strain_GL.scale+" "+cmods.strain_GL.rx+" "+cmods.strain_GL.ry+" "+cmods.strain_GL.rz+" "+cmods.strain_GL.sxyz+" "+cmods.strain_GL.perspective+"";



  var STL_particles_command= "./STL_particles nlattice="+cmods.STL_particles.nlattice+" < "+cmods.STL_particles.stlName+".stl";
  var particles_bonds_command= " | ./particles_bonds grid="+cmods.particles_bonds.grid+" block="+cmods.particles_bonds.block+"";

  var bonds_stress_strain_command_= " | ./bonds_stress_strain grid="+cmods.bonds_stress_strain.grid+" block="+cmods.bonds_stress_strain.block+" spring="+cmods.bonds_stress_strain.spring
  +" mass="+cmods.bonds_stress_strain.mass+" dissipation="+cmods.bonds_stress_strain.dissipation
  +" dt="+cmods.bonds_stress_strain.dt+" loop="+cmods.bonds_stress_strain.nloop+" step="+cmods.bonds_stress_strain.step
  +" bond="+cmods.bonds_stress_strain.bond+"";

  var bonds_stress_strain_command= " | ./bonds_stress_strain2 grid="+cmods.bonds_stress_strain.grid+" block="+cmods.bonds_stress_strain.block+" spring="+cmods.bonds_stress_strain.spring
  +" mass="+cmods.bonds_stress_strain.mass+" dissipation="+cmods.bonds_stress_strain.dissipation
  +" dt="+cmods.bonds_stress_strain.dt+" loop="+cmods.bonds_stress_strain.nloop+" step="+cmods.bonds_stress_strain.step
  +" bond="+cmods.bonds_stress_strain.bond+"";

  
  if(cmods.bonds_stress_strain.load.type=="zlimit"){
    bonds_stress_strain_command+=" load="+cmods.bonds_stress_strain.load.type+" percent="+cmods.bonds_stress_strain.load.percent;
  }else if(cmods.bonds_stress_strain.load.type=="ycylinder"){
    bonds_stress_strain_command+=" load="+cmods.bonds_stress_strain.load.type
    +" xmin="+cmods.bonds_stress_strain.load.cx.min+" zmin="+cmods.bonds_stress_strain.load.cz.min+" rmin="+cmods.bonds_stress_strain.load.cr.min
    +" xmax="+cmods.bonds_stress_strain.load.cx.max+" zmax="+cmods.bonds_stress_strain.load.cz.max+" rmax="+cmods.bonds_stress_strain.load.cr.max;
  }else if(cmods.bonds_stress_strain.load.type=="loadbox"){
    bonds_stress_strain_command+=" load="+cmods.bonds_stress_strain.load.type
    +" lxmin="+cmods.bonds_stress_strain.load.lmin.x+" lymin="+cmods.bonds_stress_strain.load.lmin.y+" lzmin="+cmods.bonds_stress_strain.load.lmin.z
    +" lxmax="+cmods.bonds_stress_strain.load.lmax.x+" lymax="+cmods.bonds_stress_strain.load.lmax.y+" lzmax="+cmods.bonds_stress_strain.load.lmax.z
    +" sxmin="+cmods.bonds_stress_strain.load.smin.x+" symin="+cmods.bonds_stress_strain.load.smin.y+" szmin="+cmods.bonds_stress_strain.load.smin.z
    +" sxmax="+cmods.bonds_stress_strain.load.smax.x+" symax="+cmods.bonds_stress_strain.load.smax.y+" szmax="+cmods.bonds_stress_strain.load.smax.z;
  }

  var strain_GL_command= " | ./strain_GL size="+cmods.strain_GL.size+" scale="+cmods.strain_GL.scale+" rx="+cmods.strain_GL.rx+" ry="+cmods.strain_GL.ry+" rz="+cmods.strain_GL.rz+" sxyz="+cmods.strain_GL.sxyz+" perspective="+cmods.strain_GL.perspective+"";

  var command= STL_particles_command+ particles_bonds_command+bonds_stress_strain_command +strain_GL_command;
  console.log(command)

  // var command= "./STL_particles 100 < "+stlName+".stl | ./particles_bonds 1024 1024 | ./bonds_stress_strain 512 512 30000000 1 100 0.0001 1000 0.04 -0.0002 15 | ./strain_GL 1.5 200 1.55 0. -0.0 0.25 0.05";


  exec(command, (error, stdout, stderr) => {
      if (error) {
          console.log(`error: ${error.message}`);
          return;
      }
      if (stderr) {
          console.log(`stderr: ${stderr}`);
          return;
      }
      console.log(`stdout: ${stdout}`);
  });

}

