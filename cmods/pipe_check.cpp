//
// pipe_check.cpp
//    test pipe performance
//    (c) MIT CBA Neil Gershenfeld 8/11/20
//    pipe_check 0 | pipe_check 1
//
#include <iostream>
#include <chrono>
using namespace std;

int main(int argc, char** argv) {
   if (argc != 2) {
      cerr << "command line: pipe_check 0 | pipe_check 1" << endl;
      return 1;
      }
   int type = stoi(argv[1]);
   int npts = 1e8;
   if (type == 0) {
      cerr << "send: " << npts << " points" << endl;
      for (int count = 0; count < npts; ++count)
         cout.write((char*)&type,sizeof(int));
      }
   else {
      auto t0 = chrono::high_resolution_clock::now();        
      for (int count = 0; count < npts; ++count)
         cin.read((char*)&type,sizeof(int));
      auto t1 = chrono::high_resolution_clock::now();        
      float dt = chrono::duration_cast<std::chrono::microseconds>(t1-t0).count();
      printf("receive: %f s, %g B/s\n",dt/1e6,sizeof(int)*1e6*(npts/dt));
      }
   //
   // return
   //
   return 0;
   }
