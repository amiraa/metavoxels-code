

function signed_distance1(mesh, point)
    verts = GeometryTypes.vertices(mesh)
    fs = GeometryTypes.faces(mesh)
    int_dist = maximum([ReferenceDistance.interior_distance(verts[f], point) for f in fs])
    if int_dist <= 0
        return int_dist
    else
        ext_dist = minimum([ReferenceDistance.exterior_distance(verts[f], point) for f in fs])
        return ext_dist
    end
end

function signed_distance1(mesh)
    point -> signed_distance1(mesh, point )
end


function adjustedSignedDistance(mesh, point)
    tol=0.00
    verts = GeometryTypes.vertices(mesh)
    fs = GeometryTypes.faces(mesh)
    int=false
    int_dist = maximum([ReferenceDistance.interior_distance(verts[f], point) for f in fs])
    if int_dist <= 0
        int=true
    end
    int_dist = minimum([ReferenceDistance.interior_distance(verts[f], point) for f in fs])
    if int_dist >= 0
        if int
            int_dist = minimum([int_dist,tol])
        end
        return -int_dist
    else
        ext_dist = minimum([ReferenceDistance.exterior_distance(verts[f], point) for f in fs])
        if int
            ext_dist = minimum([ext_dist,tol])
        end
        return ext_dist
    end
end

function adjustedSignedDistance(mesh )
    point -> adjustedSignedDistance(mesh, point )
end

