//
// CPU_check.cpp
//    get and test CPU information
//    (c) MIT CBA Neil Gershenfeld 8/7/20
//
#include <iostream>
#include <fstream>
#include <thread>
#include <chrono>
using namespace std;

void sum(float *buf,int index,int nthreads,int npts) {
   int start = npts*(index/((float) nthreads));
   int end = npts*((index+1)/((float) nthreads));
   for (int i = start; i < end; ++i)
      for (int j = 0; j < npts; ++j)
         buf[i] += 1.0;
   }
int main(int argc, char** argv) {
   int npts = 1e5;
   //
   // read CPU info
   //
   ifstream cpuinfo;
   cpuinfo.open("/proc/cpuinfo",ifstream::in);
   char c = cpuinfo.get();
   while (cpuinfo.good()) {
      cout << c;
      c = cpuinfo.get();
      }
   cpuinfo.close();
   //
   // time Flops serial
   //
   float *buf = new float[npts];
   printf("add %dx%d floats with 1 thread:\n",npts,npts);
   auto t0 = chrono::high_resolution_clock::now();        
   sum(buf,0,1,npts);
   auto t1 = chrono::high_resolution_clock::now();        
   float dt = chrono::duration_cast<std::chrono::microseconds>(t1-t0).count();
   printf("   %f s, %f G/s\n",dt/1e6,npts*(npts/dt)/1000.0);
   //
   // time Flops parallel
   //
   int nthreads = thread::hardware_concurrency();
   thread threads[nthreads];
   printf("add %dx%d floats with %d threads:\n",npts,npts,nthreads);
   t0 = chrono::high_resolution_clock::now();        
   for (int i = 0; i < nthreads; ++i)
      threads[i] = thread(sum,buf,i,nthreads,npts);
   for (int i = 0; i < nthreads; ++i)
      threads[i].join();
   t1 = chrono::high_resolution_clock::now();        
   dt = chrono::duration_cast<std::chrono::microseconds>(t1-t0).count();
   printf("   %f s, %f G/s\n",dt/1e6,npts*(npts/dt)/1000.0);
   //
   // return
   //
   return 0;
   }
