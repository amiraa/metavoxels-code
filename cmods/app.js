const { exec } = require("child_process");

var setup={
    cmods:[]
};
setup.cmods={
    STL_particles:{
        nlattice:"100",
        stlName:'test',
    },
    particles_bonds:{
        grid:"1024",
        block:"1024",
    },
    bonds_stress_strain:{
        grid:"512",
        block: "512",
        spring: "30000000",
        mass: "1",
        dissipation: "100",
        dt: "0.0001",
        nloop: "1000",
        fraction: "0.04",
        step: "-0.0002",
        bond:"15"

    },
    strain_GL:{
        size:"1.5",
        scale:"200",
        rx:"1.55",
        ry:"0.0",
        rz:"0.0",
        sxyz:"0.25",
        perspective:"0.05"

    }

}
var cmods=setup.cmods;

var STL_particles_command= "./STL_particles "+cmods.STL_particles.nlattice+" < "+cmods.STL_particles.stlName+".stl";
var particles_bonds_command= " | ./particles_bonds "+cmods.particles_bonds.grid+" "+cmods.particles_bonds.block+"";
var bonds_stress_strain_command= " | ./bonds_stress_strain "+cmods.bonds_stress_strain.grid+" "+cmods.bonds_stress_strain.block+" "+cmods.bonds_stress_strain.spring+" "+cmods.bonds_stress_strain.mass+" "+cmods.bonds_stress_strain.dissipation+" "+cmods.bonds_stress_strain.dt+" "+cmods.bonds_stress_strain.nloop+" "+cmods.bonds_stress_strain.fraction+" "+cmods.bonds_stress_strain.step+" "+cmods.bonds_stress_strain.bond+"";
var strain_GL_command= " | ./strain_GL "+cmods.strain_GL.size+" "+cmods.strain_GL.scale+" "+cmods.strain_GL.rx+" "+cmods.strain_GL.ry+" "+cmods.strain_GL.rz+" "+cmods.strain_GL.sxyz+" "+cmods.strain_GL.perspective+"";

var command= STL_particles_command+ particles_bonds_command+bonds_stress_strain_command +strain_GL_command;
console.log(command)

// var command= "./STL_particles 100 < "+stlName+".stl | ./particles_bonds 1024 1024 | ./bonds_stress_strain 512 512 30000000 1 100 0.0001 1000 0.04 -0.0002 15 | ./strain_GL 1.5 200 1.55 0. -0.0 0.25 0.05";


exec(command, (error, stdout, stderr) => {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    }
    if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    }
    console.log(`stdout: ${stdout}`);
});