//
// particles_STL.cpp
//    input particles, output STL
//    (c) MIT CBA Neil Gershenfeld 6/16/20
//
// includes
//
#include <iostream>
#include <string>
#include <cstring>
using namespace std;
//
// variables
//
int particles,triangles;
char buf[80];
float d,xmin,xmax,ymin,ymax,zmin,zmax;
float *px,*py,*pz;
float zero = 0;
//
// output triangle
//
void output(float x0,float x1,float x2,
      float y0,float y1,float y2,
      float z0,float z1,float z2) {
   cout.write((char *)&zero,4); // normal
   cout.write((char *)&zero,4);
   cout.write((char *)&zero,4);
   cout.write((char *)&x0,4); // vertices
   cout.write((char *)&y0,4);
   cout.write((char *)&z0,4);
   cout.write((char *)&x1,4);
   cout.write((char *)&y1,4);
   cout.write((char *)&z1,4);
   cout.write((char *)&x2,4);
   cout.write((char *)&y2,4);
   cout.write((char *)&z2,4);
   cout.write((char *)&zero,2); // attribute
   }
//
// main
//
int main(int argc, char** argv) {
   if (argc == 1) {
      cerr << "command line: particle source | particles_STL [arguments] > output.stl" << endl;
      cerr << "   size=(particle size, 1=lattice pitch)" << endl;
      return 1;
      }
   float size;
   if (nullptr == strstr(argv[1],"size=")) {
      cerr << "particles_STL argument not recognized" << endl;
      return 1;
      }
   else
      size = stof(argv[1]+5);
   //
   // read and parse inputs
   //
   string s;
   while (1) {
      getline(cin,s);
      if (s.compare("number:") == 0) {
         getline(cin,s);
         particles = stoi(s);
         px = new float[particles];
         py = new float[particles];
         pz = new float[particles];
         }
      else if (s.compare("pitch:") == 0) {
         getline(cin,s);
         d = size*stof(s);
         }
      else if (s.compare("particles:") == 0) {
         for (int i = 0; i < particles; ++i) {
            cin.read((char *)&px[i],4);
            cin.read((char *)&py[i],4);
            cin.read((char *)&pz[i],4);
            }
         }
      else if (s.compare("end:") == 0)
         break;
      }
   cerr << "particles_STL:" << endl;
   cerr << "   input " << particles << " particles" << endl;
   //
   // write STL
   //
   float x0,y0,z0,x1,y1,z1,x2,y2,z2;
   triangles = 8*particles;
   cout.write(buf,80); // header
   cout.write((char *)&triangles,4); // number
   for (int i = 0; i < particles; ++i) { // particles
      output(px[i]-d/2,px[i]+d/2,px[i],
         py[i]-d/2,py[i]-d/2,py[i],
         pz[i],pz[i],pz[i]+d/2);
      output(px[i]+d/2,px[i]+d/2,px[i],
         py[i]-d/2,py[i]+d/2,py[i],
         pz[i],pz[i],pz[i]+d/2);
      output(px[i]+d/2,px[i]-d/2,px[i],
         py[i]+d/2,py[i]+d/2,py[i],
         pz[i],pz[i],pz[i]+d/2);
      output(px[i]-d/2,px[i]-d/2,px[i],
         py[i]+d/2,py[i]-d/2,py[i],
         pz[i],pz[i],pz[i]+d/2);
      output(px[i]+d/2,px[i]-d/2,px[i],
         py[i]-d/2,py[i]-d/2,py[i],
         pz[i],pz[i],pz[i]-d/2);
      output(px[i]+d/2,px[i]+d/2,px[i],
         py[i]+d/2,py[i]-d/2,py[i],
         pz[i],pz[i],pz[i]-d/2);
      output(px[i]-d/2,px[i]+d/2,px[i],
         py[i]+d/2,py[i]+d/2,py[i],
         pz[i],pz[i],pz[i]-d/2);
      output(px[i]-d/2,px[i]-d/2,px[i],
         py[i]-d/2,py[i]+d/2,py[i],
         pz[i],pz[i],pz[i]-d/2);
      }
   }
