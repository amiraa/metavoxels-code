todo
- multi-GPU
- raster visualization for large # particles
- process tees
- particles_stress_strain
- plot_stress_strain
- process graph GUI
- boundary condition GUI
- mixed particle types

videos
- expt, FEA comparison: https://vimeo.com/439736234/ae8f73d4fd
- strucural failure: https://vimeo.com/439736310/3855dcc4d1
- running NASTRAN comparison: https://vimeo.com/439823787/f7ab0d402a
- rheology: https://vimeo.com/426082370

commands
- https://gitlab.cba.mit.edu/neilg/cmods/-/blob/master/Makefile

performance
- https://gitlab.cba.mit.edu/neilg/cmods/-/blob/master/numbers.md

CUDA
- https://docs.nvidia.com/cuda/
- https://developer.nvidia.com/cuda-downloads

remote desktop
- http://academy.cba.mit.edu/classes/interface_application_programming/remote.html

GPU
- gaming laptop
    - https://www.amazon.com/MAINGEAR-Vector-Gaming-Laptop-i7-10750H/dp/B087KGR7VG
- cloud
    - https://aws.amazon.com/ec2/instance-types/
    - https://aws.amazon.com/ec2/instance-types/p3/
    - https://aws.amazon.com/ec2/pricing/on-demand/
