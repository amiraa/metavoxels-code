var color1= 0xffffff; /*white*/
var color2= '#020227';  /*kohly*/
var color3= 0x1c5c61; /*teal*/
var color4= "#fa6e70"; //red/orange
var color5="#380152"; //purple
var color6="#696767"; //grey
var color7="#03dbfc"; //blue


var fileName="setupScaling";


var setup,Graph,gData,scene,camera,renderer,orbit,gui,guiSupport,guiLoad;
var scale=10.0;


var supportCount=0;
var supportBoxes=[];
var supportControls=[];
var guiSupports=[];

var loadCount=0;
var loadBoxes=[];
var loadControls=[];
var guiLoads=[];




//////////////init///////////////////////
$.getJSON("../json/"+fileName+".json", function(json) {
    setup=json.setup;
    [scene,camera,renderer,orbit]=loadGraph(setup,scale);
    

    setup.supports=[];
    setup.loads=[];


    gui = new dat.GUI();
    guiCreate (setup);

});



////////////////////////////////////////////////////////////////////////




function getNodeColor(restrained,loaded){
    if (restrained){
        return 0xfa6e70;
    }else if(loaded){
        return 0x03dbfc;
    }else{
        return color3;
    }
}

////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////
function guiCreate(setup){

	var f1 = gui.addFolder('Displacement Animation');
	f1.add(setup.animation, 'showDisplacement');
	f1.add(setup.animation, 'exaggeration', 0, 1000);
	f1.add(setup.animation, 'speed', 0, 10);

	var f2 = gui.addFolder('Stresses Visualization');
	f2.add(setup.viz, 'minStress', -1000, 0).listen();
	f2.add(setup.viz, 'maxStress', 0, 1000).listen();
    f2.add(setup.viz, 'colorMap', {coolwarm:0, YlGnBu:1, winter:2,jet:3});
    
    
    guiAddSupports();
    

	// gui.add(setup, 'solve');

	// for (j in f2.__controllers) f2.__controllers[j].onChange (updateColors.bind(this)); //todo check three
	// for (j in f1.__controllers) f1.__controllers[j].onChange (renderLast.bind(this)); //todo check three
	
}


function updateNodeColors(){
    for(var j=0;j<supportCount;j++){

        var box = new THREE.Box3();
        supportBoxes[j].geometry.computeBoundingBox();
        box.copy( supportBoxes[j].geometry.boundingBox ).applyMatrix4( supportBoxes[j].matrixWorld );

        

        Graph.d3Force('box', () => {

            gData.nodes.forEach(node => {

                node.restrained=box.containsPoint(new THREE.Vector3(node.px,node.py,node.pz));

            });
        });
    
    }

    for(var j=0;j<loadCount;j++){

        var box1 = new THREE.Box3();
        loadBoxes[j].geometry.computeBoundingBox();
        box1.copy( loadBoxes[j].geometry.boundingBox ).applyMatrix4( loadBoxes[j].matrixWorld );

        

        Graph.d3Force('box', () => {

            gData.nodes.forEach(node => {

                node.loaded=box1.containsPoint(new THREE.Vector3(node.px,node.py,node.pz));

            });
        });
    
    }




    
    Graph.nodeThreeObject(({ restrained,loaded }) => new THREE.Mesh(
        new THREE.BoxGeometry(scale, scale, scale),
        new THREE.MeshLambertMaterial({
        color: getNodeColor(restrained,loaded),
        transparent: true,
        opacity: 0.8
        })
    ))

}






