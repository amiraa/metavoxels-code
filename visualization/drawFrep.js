// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

var gridSize=10;
var occupancy=[];

function drawFromFrep(frep,pos,hierarchical){
    rhino3dm().then(async m => {
        console.log('Loaded rhino3dm.');
    
        _rhino3dm = m; // global
    
        var voxelSize=1;

        var setupEmpty={//empty
            nodes: [
                ],
            edges: [
                ],
        
            //material properties - AISI 1095 Carbon Steel (Spring Steel)
            ndofs   : 3*6,
        
            animation :  {
            
                showDisplacement : true,
                exaggeration : 20e2,
                speed:3.0
                
            },
            viz :  {
                minStress:10e6,
                maxStress: -10e6,
                colorMaps:[coolwarm,YlGnBu, winter ,jet],
                colorMap:0,
                
            },
            
        };

        console.log(setupEmpty)
    
        setup=JSON.parse(JSON.stringify(setupEmpty));

        var material={
            area:2.38*2.38,
            density:0.028,
            stiffness:4000
        };

        if(hierarchical){
            createLatticeFromFrep(setup,voxelSize,frep,createHierarchalVoxel,material,pos.clone());
        }else{
            createLatticeFromFrep(setup,voxelSize,frep,createVoxel,material,pos.clone());
        }
        drawGraph(setup,scale,hierarchical);
    

        // const position=pos.clone();


        // var voxelList=json.voxelList;
        
        // setup.hierarchical=json.hierarchical;
        
        



        // if(setup.hierarchical){
        //     setup.voxelSize=voxelSize;
        //     gridSize=json.gridSize*2.0;
            
        //     for (var i=0;i<gridSize;++i){
        //         occupancy.push([]);
        //         for (var j=0;j<gridSize;++j){
        //             occupancy[i].push([]);
        //             for (var k=0;k<gridSize;++k){
        //                 occupancy[i][j].push(-1);
        //             }
        //         }
        //     }
        // }


        // for (var count1=0;count1<voxelList.length;count1++){
        //     var i1=voxelList[count1][0][0]+gridSize/2.0;
        //     var j1=voxelList[count1][0][1]+gridSize/2.0;
        //     var k1=voxelList[count1][0][2]+gridSize/2.0;
        //     var m1=voxelList[count1][1];


        //     if(setup.hierarchical){
        //         var shift=true;
        //         createHierarchalVoxel(setup,voxelSize,new THREE.Vector3(i1*voxelSize+position.x,j1*voxelSize+position.y,k1*voxelSize+position.z),m1,shift);
        //     }else{
        //         createVoxel(setup,voxelSize,new THREE.Vector3(i1*voxelSize+position.x,j1*voxelSize+position.y,k1*voxelSize+position.z),m1);

        //     }
        // }


    
        
        
    
        
        // var materials=[];
        // var supports=[];
        // var loads=[];
        // var fixedDisplacements=[];
        // setup.materials=json.materials;
        // setup.supports=json.supports;
        // setup.loads=json.loads;
        // setup.fixedDisplacements=json.fixedDisplacements;
    
        // var materials1=json.materials;
        // var supports1=json.supports;
        // var loads1=json.loads;
        // var fixedDisplacements1=json.fixedDisplacements;
    
        // for (var i=1;i<materials1.length;i++ ){
        //     var material1=materials1[i];
        //     var boundingMaterial=new _rhino3dm.BoundingBox(
        //         [
        //             material1[0].min.x,
        //             material1[0].min.y,
        //             material1[0].min.z
        //         ], 
        //         [
        //             material1[0].max.x,
        //             material1[0].max.y,
        //             material1[0].max.z
        //     ]);
        //     materials.push([ boundingMaterial,material1[1]]);
        // }
    
        // for (var i=0;i<supports1.length;i++ ){
        //     var support1=supports1[i];
        //     var boundingSupport=new _rhino3dm.BoundingBox(
        //         [
        //             support1[0].min.x,
        //             support1[0].min.y,
        //             support1[0].min.z
        //         ], 
        //         [
        //             support1[0].max.x,
        //             support1[0].max.y,
        //             support1[0].max.z
        //     ]);
        //     supports.push([ boundingSupport,support1[1]]);
    
    
        // }
    
        // for (var i=0;i<loads1.length;i++ ){
        //     var load1=loads1[i];
        //     var boundingLoad=new _rhino3dm.BoundingBox(
        //         [
        //             load1[0].min.x,
        //             load1[0].min.y,
        //             load1[0].min.z
        //         ], 
        //         [
        //             load1[0].max.x,
        //             load1[0].max.y,
        //             load1[0].max.z
        //     ]);
        //     loads.push([ boundingLoad,load1[1]]);
        // }
    
        // for (var i=0;i<fixedDisplacements1.length;i++ ){
        //     var fixedDisplacement1=fixedDisplacements1[i];
        //     var boundingFixedDisplacement=new _rhino3dm.BoundingBox(
        //         [
        //             fixedDisplacement1[0].min.x,
        //             fixedDisplacement1[0].min.y,
        //             fixedDisplacement1[0].min.z
        //         ], 
        //         [
        //             fixedDisplacement1[0].max.x,
        //             fixedDisplacement1[0].max.y,
        //             fixedDisplacement1[0].max.z
        //     ]);
        //     fixedDisplacements.push([ boundingFixedDisplacement,fixedDisplacement1[1]]);
        // }
    
    
        // changeMaterialFromBox(setup,materials);
        // restrainFromBox(setup,supports);
        // loadFromBox(setup,loads);
        // displacementFromBox(setup,fixedDisplacements);
        
        
    
        // // setup.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
        // setup.viz.minStress=10e6;
        // setup.viz.maxStress=-10e6;
    
        // setup.viz.exaggeration=1.0;
        // setup.animation.exaggeration=1.0;
        // setup.viz.colorMaps=[];
    
        // setup.numTimeSteps=json.numTimeSteps;
        // setup.maxNumFiles=json.maxNumFiles;
    
    
        // setup.poisson=json.poisson;
        // setup.scale=json.scale;
        // setup.linear=json.linear;
        // setup.globalDamping=json.globalDamping;
        // setup.thermal=json.thermal;
        
    
        // saveJSON();
        console.log(setup)

        // var coneGroup=new THREE.Group();
        // coneGroup.name="coneGroup";
        // for(var x=-10;x<10;x++){
        //   for(var y=-10;y<0;y++){
        //     for(var z=0;z<16;z++){
        //       if(cone(x,y,z)>0){
        //         var nomSize=1.0;
        //         var mesh=new THREE.Mesh(
        //           new THREE.BoxGeometry(scale*nomSize*0.9, scale*nomSize*0.9, scale*nomSize*0.9),
        //           new THREE.MeshLambertMaterial({color: color3,transparent: true,opacity: 0.8})
        //         );
        //         mesh.position.x=x*scale;
        //         mesh.position.y=z*scale-scale/2.0;
        //         mesh.position.z=y*scale;
        //         coneGroup.add(mesh);
        //       }
        //     }
        //   }
        // }
        // Graph.scene().add(coneGroup);
    
        console.log("Success!")
    });

}


