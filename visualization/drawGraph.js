// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

var color1= 0xffffff; /*white*/
var color2= '#020227';  /*kohly*/
var color3= 0x1c5c61; /*teal*/
var color4= 0xfa6e70; //red/orange
var color5=0x380152; //purple
var color6=0x696767; //grey
var color7=0x03dbfc; //blue

function drawGraph(container,setup,scale,animate=false,speed=100,exaggeration=1000.0){
    
    var gData = {
        nodes: setup.nodes.map(node => ({ 
            id: (node.parent=="11")?node.id:(node.parent+node.id),
            px:node.position.x*scale,
            py:node.position.y*scale,
            pz:node.position.z*scale,
            dx:node.displacement.x*scale,
            dy:node.displacement.y*scale,
            dz:node.displacement.z*scale,
            restrained:node.restrained_degrees_of_freedom[0]&&node.restrained_degrees_of_freedom[1]&&node.restrained_degrees_of_freedom[2],
            loaded:(node.force.x==0&&node.force.y==0&&node.force.z==0)?false:true,
            displaced:(node.fixedDisplacement===undefined ||(node.fixedDisplacement.x==0&&node.fixedDisplacement.y==0&&node.fixedDisplacement.z==0))?false:true,
            size:(node.material===undefined) ? 10:(( setup.multiscale===undefined || !setup.multiscale)?(scale*0.8):(node.scale*scale*0.8**(1/node.scale))),
            opacity: (node.parent=="11"|| setup.multiscale===undefined ||node.parent=="")?0.8:0.0,
        })),
        links: setup.edges
            .filter(edge => edge.id)
            .map(edge => ({
                source: (setup.multiscale===undefined || !setup.multiscale ||edge.sourceNodalCoordinate==0)?'n'+edge.source:('n'+edge.source+'n'+edge.sourceNodalCoordinate),
                target: (setup.multiscale===undefined || !setup.multiscale ||edge.targetNodalCoordinate==0)?'n'+edge.target:('n'+edge.target+'n'+edge.targetNodalCoordinate),
                color:getColor(setup.viz,edge.stress)
            }))
    };
    //color=interpolateLinearly(i / l, this.setup.viz.colorMaps[this.setup.viz.colorMap]);

    var Graph = ForceGraph3D({ controlType: 'orbit' }).backgroundColor(color2)
        (document.getElementById(container))
        .d3Force('center', null)
        .d3Force('charge', null)
        .linkWidth(1.0)
        .linkOpacity(1.0)
        // .linkColor(color3)
        .nodeThreeObject(({ id,restrained,size,loaded,displaced,opacity }) => new THREE.Mesh(
            new THREE.BoxGeometry(size, size, size),
            new THREE.MeshLambertMaterial({
                color: restrained?(color4):(loaded?color7:(displaced?color5:color3)),
                transparent: opacity>0.0?false:true,
                opacity: opacity
            })
        ))
        .d3Force('box', () => {

            gData.nodes.forEach(node => {
                node.fx=node.px;
                node.fy=node.py;
                node.fz=node.pz;

            });
        })
        
        .cooldownTime(Infinity)
        .graphData(gData);
    //


    var helper = new THREE.GridHelper( 50, 50 );
    helper.position.y = -5.0*scale;
    helper.material.opacity = 0.5;
    helper.material.transparent = true;
    helper.scale.x=1.0*scale
    helper.scale.z=1.0*scale
    Graph.scene().add(helper);
    
    if (animate){
        showDisplacement(speed,exaggeration);
    }
    

    function drawConstraintBoundingBoxes(setup,scene,scale){

        //grid helper
        var helper = new THREE.GridHelper( 100, 100 );
        helper.position.y = -5.0*scale;
        helper.material.opacity = 0.5;
        helper.material.transparent = true;
        helper.scale.x=2.0*scale
        helper.scale.z=2.0*scale
        scene.add(helper);

        let supports=setup.supports;
        let loads=setup.loads;
        let mat=setup.materials;
        let disps=setup.fixedDisplacements;
        if (supports ) {
            for (var i=0;i< supports.length;i++) {
                let s=supports[i][0];
                drawBox(s.min,s.max,color4,scene,scale);
            }
        }
        if (loads ) {
            for (var i=0;i< loads.length;i++) {
                let l=loads[i][0];
                drawBox(l.min,l.max,color7,scene,scale);
            }
            
        }
        if (disps ) {
            for (var i=0;i< disps.length;i++) {
                let l=disps[i][0];
                drawBox(l.min,l.max,color7,scene,scale);
            }
            
        }
        if (mat ) {
            
            for (var i=0;i< mat.length;i++) {
                let l=mat[i][0];
                // console.log(l)
                drawBox(l.min,l.max,color5,scene,scale);
            }
            
        }
        
    };

    function drawBox (min,max,color,scene,scale) {
        var box = new THREE.Box3(new THREE.Vector3(min.x*scale,min.y*scale,min.z*scale),new THREE.Vector3(max.x*scale,max.y*scale,max.z*scale));
        var helper = new THREE.Box3Helper( box, color );
        scene.add( helper );
        // todo add name??
    };

    

    function showDisplacement(speed,exaggeration){
        var count=0;
        var totalCount=0;
        var increment=true;

        setInterval(() => {

            Graph.d3Force('box', () => {
    
              gData.nodes.forEach(node => {
    
    
                node.fx=node.px+count/speed*node.dx*exaggeration;
                node.fy=node.py+count/speed*node.dy*exaggeration;
                node.fz=node.pz+count/speed*node.dz*exaggeration;
    
              });
            });
    
    
            if(count>speed){
              increment=false;
            }else if (count<0){
              increment=true;
            }
    
    
            if(increment){
              count++;
            }else{
              count--;
            }
            // totalCount++;
            // console.log(totalCount);
          }, 1);

    }

    return Graph;
  

}

function getColor(viz,stress){
    // console.log(stress)
    var val=map(stress,viz.minStress,viz.maxStress,1.0,0.0);
    color=interpolateLinearly(val, viz.colorMaps[viz.colorMap]);
    return new THREE.Color(color[0],color[1],color[2]).getHex();

}

function updateGraph(Graph,setup,scale){
    var gData = {
        nodes: setup.nodes.map(node => ({ 
            id: (node.parent=="11")?node.id:(node.parent+node.id),
            px:node.position.x*scale,
            py:node.position.y*scale,
            pz:node.position.z*scale,
            dx:node.displacement.x*scale,
            dy:node.displacement.y*scale,
            dz:node.displacement.z*scale,
            restrained:node.restrained_degrees_of_freedom[0],
            loaded:(node.force.x==0&&node.force.y==0&&node.force.z==0)?false:true,
            displaced:(node.fixedDisplacement===undefined ||(node.fixedDisplacement.x==0&&node.fixedDisplacement.y==0&&node.fixedDisplacement.z==0))?false:true,
            size:(node.material===undefined) ? scale:(( setup.multiscale===undefined || !setup.multiscale)?(100*scale*0.8):(100*node.scale*scale*0.8**(1/node.scale))),
            opacity: (node.parent=="11"|| setup.multiscale===undefined ||node.parent=="")?0.8:0.0,
        })),
        links: setup.edges
            .filter(edge => edge.id)
            .map(edge => ({
                source: (setup.multiscale===undefined || !setup.multiscale ||edge.sourceNodalCoordinate==0)?'n'+edge.source:('n'+edge.source+'n'+edge.sourceNodalCoordinate),
                target: (setup.multiscale===undefined || !setup.multiscale ||edge.targetNodalCoordinate==0)?'n'+edge.target:('n'+edge.target+'n'+edge.targetNodalCoordinate),
                color:getColor(setup.viz,edge.stress)
            }))
    };

    Graph.graphData(gData).d3Force('box', () => {

        gData.nodes.forEach(node => {
            node.fx=node.px;
            node.fy=node.py;
            node.fz=node.pz;

        });
    })
    
    .cooldownTime(Infinity);

}