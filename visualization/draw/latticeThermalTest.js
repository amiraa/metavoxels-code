
// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

///////////call from node app.js code///////////////
// wait for the rhino3dm web assembly to load asynchronously
// rhino3dm().then(function(m) {// todo change call to app.js
// 	_rhino3dm = m; // global
// 	initRhino3dm(1,10,createVoxel,true,true);
// 	saveJSON();
// });
/////////////////////////////////////

var three; //todo change location
var three1; //todo change location
var setup=JSON.parse(JSON.stringify(setupEmpty));
var gridSize=20;
// var setup1=JSON.parse(JSON.stringify(setupEmpty));
var globalSetup={
    exaggeration:1.0,
    speed:3.0,
    updateStress:false
};

rhino3dm().then(async m => {
    console.log('Loaded rhino3dm.');

    _rhino3dm = m; // global

    var latticeSizeX=7;
    var latticeSizeY=2;
    var latticeSizeZ=2;


    var voxelSize=5.0;
    var voxelSize=0.001;
    

    var material={
      area:voxelSize*voxelSize,
      density:0.028,
      stiffness:2000,
      poissonRatio:0.3
    };

    var material={
      area:voxelSize*voxelSize,
      density:1e3,
      stiffness:1e6,
      poissonRatio:0.0,
      cTE:0.01
    };

    const position=new THREE.Vector3(0,0,0);
    
    setup=JSON.parse(JSON.stringify(setupEmpty));

    setup.hierarchical=true;
    
    setup.voxelSize=voxelSize;
    voxelSize=voxelSize/15.0;

    createLattice(setup,voxelSize,latticeSizeX,latticeSizeY,latticeSizeZ,createHierarchalVoxel,material);

    var support=new _rhino3dm.BoundingBox(
      [
          -voxelSize/2+position.x,
          -voxelSize/2+position.y,
          -voxelSize/2+position.z
      ], [
          voxelSize/2+position.x,
          voxelSize/2+position.y +voxelSize*(latticeSizeY-1),
          voxelSize/2+position.z +voxelSize*(latticeSizeZ-1)
    ]);


    var load=new _rhino3dm.BoundingBox(
        [
            -voxelSize/2+position.x +voxelSize*(latticeSizeX-1),
            -voxelSize/2+position.y,
            -voxelSize/2+position.z
        ], [
            voxelSize/2+position.x +voxelSize*(latticeSizeX-1),
            voxelSize/2+position.y +voxelSize*(latticeSizeY-1),
            voxelSize/2+position.z +voxelSize*(latticeSizeZ-1)
    ]);
    

    var dof=[true,true,true,true,true,true];
    var supports=[[support,dof]];
    var loads=[[load,{x:0.0,y:0.0,z:0.0}]];

    restrainFromBox(setup,supports);
    // loadFromBox(setup,loads);
    
    setup.supports=[
        // [{
        //     min:[
        //         support.min[0],
        //         support.min[1],
        //         support.min[2]
        //     ],
        //     max:[
        //         support.max[0],
        //         support.max[1],
        //         support.max[2]
        //     ]
        // }]
    ];
    setup.loads=[
    //   [{
    //       min:[
    //           load.min[0],
    //           load.min[1],
    //           load.min[2]
    //       ],
    //       max:[
    //           load.max[0],
    //           load.max[1],
    //           load.max[2]
    //       ]
    //   }],
      // [{
      //     min:[
      //         load1.min[0],
      //         load1.min[1],
      //         load1.min[2]
      //     ],
      //     max:[
      //         load1.max[0],
      //         load1.max[1],
      //         load1.max[2]
      //     ]
      // }]
    ];

    // latticeSetup(setup,latticeSize,voxelSize,createHierarchalVoxel,{x:0,y:-400,z:0},material);


    // setup.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
    setup.viz.minStress=10e6;
    setup.viz.maxStress=-10e6;

    setup.viz.exaggeration=1.0;
    setup.numTimeSteps=100;

    setup.floorEnabled=false;
    setup.gravityEnabled=false;
    setup.staticSimulation=true; 
    setup.poisson=false;
    setup.scale=1e5;
    setup.linear=true;
    setup.globalDamping=0.15;
    setup.thermal=true;
    

    saveJSON();
});



