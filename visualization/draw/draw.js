
// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

// const { json } = require("body-parser");

var gridSize=10;
var occupancy=[];

rhino3dm().then(async m => {
    // console.log('Loaded rhino3dm.');

    _rhino3dm = m; // global

    var voxelSize=json.voxelSize;

    setup=JSON.parse(JSON.stringify(setupEmpty));

    if(!json.rhino){
        if(!(json.useVoxelList)){ //if lattice
            var material=json.materials[0][1];

            var latticeSizeX=json.latticeSizeX;
            var latticeSizeY=json.latticeSizeY;
            var latticeSizeZ=json.latticeSizeZ;

            gridSize=json.gridSize;

            // console.log(material.area)

            const position=new THREE.Vector3(0,0,0);
            
            setup.hierarchical=json.hierarchical;
            
            setup.voxelSize=voxelSize;

            if(setup.hierarchical){
                createLattice(setup,voxelSize,latticeSizeX,latticeSizeY,latticeSizeZ,createHierarchalVoxel,material);
            }else{
                createLattice(setup,voxelSize,latticeSizeX,latticeSizeY,latticeSizeZ,createVoxel,material);
            }
            

        }else{ //if voxel list
            const position=new THREE.Vector3(0,0,0);
            var voxelList=json.voxelList;
            setup.hierarchical=json.hierarchical;

            if(setup.hierarchical){
                setup.voxelSize=voxelSize;
                gridSize=json.gridSize*2.0;
                
                for (var i=0;i<gridSize;++i){
                    occupancy.push([]);
                    for (var j=0;j<gridSize;++j){
                        occupancy[i].push([]);
                        for (var k=0;k<gridSize;++k){
                            occupancy[i][j].push(-1);
                        }
                    }
                }
            }

            //if multiscale sort first to make sure that the scale 2 is at the end
            if (typeof json.multiscale !== 'undefined' && json.multiscale){
                voxelList.sort((a, b) => (a[1].scale > b[1].scale) ? 1 : -1);
                setup.virtualNodes=[]
                setup.multiscale=json.multiscale;
            }
            
                
            for (var count1=0;count1<voxelList.length;count1++){
                var i1=voxelList[count1][0][0]+gridSize/2.0;
                var j1=voxelList[count1][0][1]+gridSize/2.0;
                var k1=voxelList[count1][0][2]+gridSize/2.0;
                var m1=voxelList[count1][1];


                if(setup.hierarchical){
                    if (typeof json.multiscale === 'undefined'|| !json.multiscale){
                        var shift=true;
                        createHierarchalVoxel(setup,voxelSize,new THREE.Vector3(i1*voxelSize+position.x,j1*voxelSize+position.y,k1*voxelSize+position.z),m1,shift);

                    }else if(json.multiscale){ //multiscale from voxelList
                        
                        var shift=true;
                        if(parseInt(m1.scale)==1){
                            createHierarchalVoxel(setup,voxelSize,new THREE.Vector3(i1*voxelSize+position.x,j1*voxelSize+position.y,k1*voxelSize+position.z),m1,shift,"");
                        }else if(parseInt(m1.scale)==2){
                            createHierarchalVoxel(setup,voxelSize,new THREE.Vector3(i1*voxelSize+position.x,j1*voxelSize+position.y,k1*voxelSize+position.z),m1,shift,"");
                        }

                    }
                    
                }else{
                    var i1=voxelList[count1][0][0];
                    var j1=voxelList[count1][0][1];
                    var k1=voxelList[count1][0][2];
                    createVoxel(setup,voxelSize,new THREE.Vector3(i1*voxelSize+position.x,j1*voxelSize+position.y,k1*voxelSize+position.z),m1);

                }
            }

            //if multiscale add virtual nodes at the end of list
            if (typeof json.multiscale !== 'undefined' && json.multiscale){
                setup.nodes.push(...setup.virtualNodes)
                setup.virtualNodes=[]
            }
            
            

        } 

    }else{ //from rhino
        if(!(json.useVoxelList)){//from rhino
            var material=json.materials[0][1];

            var latticeSizeX=json.latticeSizeX;
            var latticeSizeY=json.latticeSizeY;
            var latticeSizeZ=json.latticeSizeZ;

            setup.hierarchical=json.hierarchical;
            
            setup.voxelSize=voxelSize;
            if (typeof json.useMaterialList === 'undefined'){
                setup.useMaterialList=false;
            }else{
                setup.useMaterialList=json.useMaterialList;
                if(setup.useMaterialList){
                    setup.materialList=json.materialList;
                }
            }

            createGeometryFromRhino(setup,json.rhinoFileName,json.layerIndex,voxelSize,latticeSizeX,latticeSizeY,latticeSizeZ,material);
        }else{
            //todo write from voxelList (from rhino)
        }
    }
    

    
    var materials=[];
    var supports=[];
    var loads=[];
    var fixedDisplacements=[];
    setup.materials=json.materials;
    setup.supports=json.supports;
    setup.loads=json.loads;
    setup.fixedDisplacements=json.fixedDisplacements;

    var materials1=json.materials;
    var supports1=json.supports;
    var loads1=json.loads;
    var fixedDisplacements1=json.fixedDisplacements;

    for (var i=1;i<materials1.length;i++ ){
        var material1=materials1[i];
        var boundingMaterial=new _rhino3dm.BoundingBox(
            [
                material1[0].min.x,
                material1[0].min.y,
                material1[0].min.z
            ], 
            [
                material1[0].max.x,
                material1[0].max.y,
                material1[0].max.z
        ]);
        materials.push([ boundingMaterial,material1[1]]);
    }

    for (var i=0;i<supports1.length;i++ ){
        var support1=supports1[i];
        var boundingSupport=new _rhino3dm.BoundingBox(
            [
                support1[0].min.x,
                support1[0].min.y,
                support1[0].min.z
            ], 
            [
                support1[0].max.x,
                support1[0].max.y,
                support1[0].max.z
        ]);
        supports.push([ boundingSupport,support1[1]]);


    }

    for (var i=0;i<loads1.length;i++ ){
        var load1=loads1[i];
        var boundingLoad=new _rhino3dm.BoundingBox(
            [
                load1[0].min.x,
                load1[0].min.y,
                load1[0].min.z
            ], 
            [
                load1[0].max.x,
                load1[0].max.y,
                load1[0].max.z
        ]);
        loads.push([ boundingLoad,load1[1]]);
    }

    for (var i=0;i<fixedDisplacements1.length;i++ ){
        var fixedDisplacement1=fixedDisplacements1[i];
        var boundingFixedDisplacement=new _rhino3dm.BoundingBox(
            [
                fixedDisplacement1[0].min.x,
                fixedDisplacement1[0].min.y,
                fixedDisplacement1[0].min.z
            ], 
            [
                fixedDisplacement1[0].max.x,
                fixedDisplacement1[0].max.y,
                fixedDisplacement1[0].max.z
        ]);
        fixedDisplacements.push([ boundingFixedDisplacement,fixedDisplacement1[1]]);
    }


    if (typeof json.useMaterialList === 'undefined'){
        if (!json.useMaterialList){
            changeMaterialFromBox(setup,materials);
        }
    }

    
    restrainFromBox(setup,supports);
    loadFromBox(setup,loads);
    displacementFromBox(setup,fixedDisplacements);
    
    

    // setup.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
    setup.viz.minStress=10e6;
    setup.viz.maxStress=-10e6;

    setup.viz.exaggeration=1.0;
    setup.animation.exaggeration=1.0;
    setup.viz.colorMaps=[];

    setup.numTimeSteps=json.numTimeSteps;
    setup.maxNumFiles=json.maxNumFiles;


    setup.poisson=json.poisson;
    setup.scale=json.scale;
    setup.linear=json.linear;
    setup.globalDamping=json.globalDamping;
    setup.thermal=json.thermal;
    setup.mu=json.mu;
    setup.tendonIds=json.tendonIds;

    // console.log(setup);
    

    saveJSONSetup();
    if ((typeof json.logging === 'undefined')||json.logging){
        console.log("Success! Created structure with "+setup.nodes.length+" nodes and "+setup.edges.length+" edges.")
    }

    
});



