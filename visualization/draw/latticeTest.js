
// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

///////////call from node app.js code///////////////
// wait for the rhino3dm web assembly to load asynchronously
// rhino3dm().then(function(m) {// todo change call to app.js
// 	_rhino3dm = m; // global
// 	initRhino3dm(1,10,createVoxel,true,true);
// 	saveJSON();
// });
/////////////////////////////////////

var three; //todo change location
var three1; //todo change location
var setup=JSON.parse(JSON.stringify(setupEmpty));
// var setup1=JSON.parse(JSON.stringify(setupEmpty));
var globalSetup={
    exaggeration:10e3,
    speed:3.0,
    updateStress:false

};
// var ex=gui.add(globalSetup, 'exaggeration', 0, 10e4).listen();
// var sp=gui.add(globalSetup, 'speed', 0, 5).listen();
// var sp=gui.add(globalSetup, 'updateStress').listen();

var static=false;



// ex.onChange(updateExSpeed);
// sp.onChange();


rhino3dm().then(async m => {
    console.log('Loaded rhino3dm.');

    _rhino3dm = m; // global


    var material={
		area:1.0,
		density:0.028,
		stiffness:10000000
    };
    
    var material2={
		area:1.0,
		density:0.028,
		stiffness:1000000
	};
    
    
    setup=JSON.parse(JSON.stringify(setupEmpty));

    setup.hierarchical=false;
    setup.voxelSize=voxelSize;

    latticeSetup(setup,latticeSize,voxelSize,createVoxel,{x:0,y:-400,z:0});


    setup.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
    setup.viz.minStress=10e6;
    setup.viz.maxStress=-10e6;

    setup.viz.exaggeration=10e3;
    // setup.solve=solveParallel;
    setup.numTimeSteps=100;

    setup.floorEnabled=false;
    setup.gravityEnabled=false;
    setup.staticSimulation=true; 
    setup.poisson=false;
    setup.scale=1;
    setup.linear=true;

    


    // three= new threejs(setup,"webgl1","graph");
    // three.init();


    ////////////////////////////////////
    setup1=JSON.parse(JSON.stringify(setupEmpty));

    setup1.hierarchical=true;
    setup1.voxelSize=voxelSize;

    latticeSetup(setup1,latticeSize,voxelSize,createHierarchalVoxel,{x:0,y:-400,z:0});


    setup1.viz.colorMaps=[];
    setup1.viz.minStress=10e6;
    setup1.viz.maxStress=-10e6;

    setup1.viz.exaggeration=10e3;
    setup1.numTimeSteps=100;
    setup.globalDamping=0.2;
    // setup1.solve=solveParallel;

     

    // more stuff

    saveJSON();
});

function solveParallel(){
    document.getElementById("footer2").innerHTML = "Running...";
    var numTimeSteps=250;
    /////////////////////////////////////////////////////
    

    var dt=0.0251646; //?? todo change to recommended step
    var dt=0.01; //?? todo change to recommended step
    

    setTimeout(function() { 
        simulateParallel( setup,three.setup.numTimeSteps,dt,static,2); 
        simulateParallel(setup1,three.setup.numTimeSteps,dt,static,2);
    }, 1);

    // var dt=0.0251646; //?? todo change to recommended step
    // simulateParallel(setup1,numTimeSteps,dt);

    ///////////////////////////////////////
    // updateColors();
    // three.animate();
    // three1.animate();
    
    /////////////////////

}

function updateExSpeed(){
    three.setup.animation.exaggeration=globalSetup.exaggeration;
    three1.setup.animation.exaggeration=globalSetup.exaggeration;
    three.setup.animation.speed=globalSetup.speed;
    three1.setup.animation.speed=globalSetup.speed;

}

function getForce(x,t){
    var a0=10000.0;
    var l=1.0 ;
    var f=2.0;
    var amp=0.1;
    var x=parseInt(x/voxelSize)/10.0;
    var t=t/100.0;
    var ax=1-0.825*(x-1)+1.0625*(x*x-1);
    return a0* ax *Math.sin(2*Math.PI*(x/l-f*t+amp));

}
