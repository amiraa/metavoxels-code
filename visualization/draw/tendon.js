// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

///////////call from node app.js code///////////////
// wait for the rhino3dm web assembly to load asynchronously
// rhino3dm().then(function(m) {// todo change call to app.js
// 	_rhino3dm = m; // global
// 	initRhino3dm(1,10,createVoxel,true,true);
// 	saveJSON();
// });
/////////////////////////////////////

var setup=JSON.parse(JSON.stringify(setupEmpty));

var latticeSize=4;
var voxelSize=5.0;


rhino3dm().then(async m => {
    console.log('Loaded rhino3dm.');

    // document.getElementById("footer2").innerHTML = "Loaded rhino3dm.";
    _rhino3dm = m; // global

    var material={
		area:2.38*2.38,
		density:0.028,
		stiffness:3000
    };

    var material2={
		area:2.38*2.38,
		density:0.028,
		stiffness:500
    };
    

    setup.hierarchical=false;
    setup.voxelSize=voxelSize;

    const position=new THREE.Vector3(0,0,0);

    ///
    var support=new _rhino3dm.BoundingBox(
        [
            -voxelSize/2+position.x,
            -voxelSize/2+position.y,
            -voxelSize/2+position.z
        ], [
            voxelSize/2+position.x,
            voxelSize/2+position.y,
            voxelSize/2+position.z
        ]);
    
    // var tendons=[];
    

    var temp=voxelSize*(latticeSize);
    var load=new _rhino3dm.BoundingBox(
        [
            -voxelSize/2+position.x,
            -voxelSize/2+position.y+voxelSize/4,
            temp+position.z+voxelSize/4
        ], 
        [
            -voxelSize/2+position.x+voxelSize/4,
            voxelSize/2+position.y-voxelSize/4,
            temp+voxelSize/2+position.z
    ]);
    var load1=new _rhino3dm.BoundingBox(
        [
            voxelSize/2+position.x,
            -voxelSize/2+position.y+voxelSize/4,
            -voxelSize/2+position.z
        ],     
        [
            voxelSize/2+position.x+voxelSize/4,
            voxelSize/2+position.y-voxelSize/4,
            temp+voxelSize/2+position.z
    ]);
    var dof=[true,true,true,true,true,true];
    var supports=[[support,dof]];
    var loads=[[load1,{x:0.0,y:0,z:-0.1}]];
    

    

    ///

    setup.supports=[
        [{
            min:[
                support.min[0],
                support.min[1],
                support.min[2]
            ],
            max:[
                support.max[0],
                support.max[1],
                support.max[2]
            ]
        }]
    ];

    setup.loads=[
        // [{
        //     min:[
        //         load.min[0],
        //         load.min[1],
        //         load.min[2]
        //     ],
        //     max:[
        //         load.max[0],
        //         load.max[1],
        //         load.max[2]
        //     ]
        // }],
        [{
            min:[
                load1.min[0],
                load1.min[1],
                load1.min[2]
            ],
            max:[
                load1.max[0],
                load1.max[1],
                load1.max[2]
            ]
        }]
    ];
    

    lateralLoadSetup(setup,position,latticeSize+1,voxelSize,createVoxel,supports,loads,material);

    var temp=voxelSize*(latticeSize+1)-voxelSize;
    var mat1=new _rhino3dm.BoundingBox(
        [
            -voxelSize/2+position.x-voxelSize/4,
            -voxelSize/2+position.y,
            -voxelSize/2+position.z
        ], 
        [
            -voxelSize/2+position.x+voxelSize/4,
            voxelSize/2+position.y,
            voxelSize/2+position.z+temp
    ]);
    var mat2=new _rhino3dm.BoundingBox(
        [
            voxelSize/2+position.x-voxelSize/4,
            -voxelSize/2+position.y,
            -voxelSize/2+position.z
        ], 
        [
            voxelSize/2+position.x+voxelSize/4,
            voxelSize/2+position.y,
            voxelSize/2+position.z+temp
    ]);
    var diffMaterialBox=[[mat1,material2],[mat2,material2]];
    changeMaterialFromBox(setup,diffMaterialBox);

    setup.mat=[
        [{
            min:[
                mat1.min[0],
                mat1.min[1],
                mat1.min[2]
            ],
            max:[
                mat1.max[0],
                mat1.max[1],
                mat1.max[2]
            ]
        }],
        [{
            min:[
                mat2.min[0],
                mat2.min[1],
                mat2.min[2]
            ],
            max:[
                mat2.max[0],
                mat2.max[1],
                mat2.max[2]
            ]
        }]
    ];
    

    

    for (var i=4;i<=latticeSize;i++){
        // var temp=voxelSize*i-voxelSize;
        var temp=voxelSize*1-voxelSize;
        var load=new _rhino3dm.BoundingBox(
            [
                -voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ], 
            [
                -voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);
        var load1=new _rhino3dm.BoundingBox(
            [
                voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ],     
            [
                voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);
        var temp=voxelSize*(i+1)-voxelSize;
        var load_1=new _rhino3dm.BoundingBox(
            [
                -voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ], 
            [
                -voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);
        var load1_1=new _rhino3dm.BoundingBox(
            [
                voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ],     
            [
                voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);

        
        var id1=idFromBox(setup,load);
        var id2=idFromBox(setup,load_1);
        if(id1.length>0&&id2.length>0){
            var E=0;
            // addEdge(setup,id2[0], id1[0], material.area, material.density , E);
            // setup.edges[setup.edges.length-1].loaded=(latticeSize-i)*-100;

        }

        var id1=idFromBox(setup,load1);
        var id2=idFromBox(setup,load1_1);
        if(id1.length>0&&id2.length>0){
            var E=1;
            addEdge(setup,id2[0], id1[0], material.area, material.density , E);
            setup.edges[setup.edges.length-1].loaded=0.5;

        }
    
    }

    for (var i=1;i<=latticeSize;i++){
        var temp=voxelSize*i-voxelSize;
        var load=new _rhino3dm.BoundingBox(
            [
                -voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ], 
            [
                -voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);
        var load1=new _rhino3dm.BoundingBox(
            [
                voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ],     
            [
                voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);
        var temp=voxelSize*(i+1)-voxelSize;
        var load_1=new _rhino3dm.BoundingBox(
            [
                -voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ], 
            [
                -voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);
        var load1_1=new _rhino3dm.BoundingBox(
            [
                voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ],     
            [
                voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);

        //down tendons
        var id1=idFromBox(setup,load);
        var id2=idFromBox(setup,load_1);
        if(id1.length>0&&id2.length>0){
            var E=1;
            // addEdge(setup,id2[0], id1[0], material.area, material.density , E);
            // setup.edges[setup.edges.length-1].loaded=(latticeSize-i)*-100;

        }

        //up tendons
        var id1=idFromBox(setup,load1);
        var id2=idFromBox(setup,load1_1);
        if(id1.length>0&&id2.length>0){
            var E=1;
            addEdge(setup,id2[0], id1[0], material.area, material.density , E);
            setup.edges[setup.edges.length-1].loaded=i*i*i*0.3;
        }
    }

    for (var i=1;i<=latticeSize;i++){
        var temp=voxelSize*i-voxelSize;
        var load=new _rhino3dm.BoundingBox(
            [
                -voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ], 
            [
                -voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);
        var load1=new _rhino3dm.BoundingBox(
            [
                voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ],     
            [
                voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);
        var temp=voxelSize*(i+1)-voxelSize;
        var load_1=new _rhino3dm.BoundingBox(
            [
                -voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ], 
            [
                -voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);
        var load1_1=new _rhino3dm.BoundingBox(
            [
                voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4
            ],     
            [
                voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp+voxelSize/2+position.z
        ]);


        //perpendicular tendons
        var id1=idFromBox(setup,load_1);
        var id2=idFromBox(setup,load1_1);
        if(id1.length>0&&id2.length>0){
            var E=1;
            addEdge(setup,id2[0], id1[0], material.area, material.density , E);
            setup.edges[setup.edges.length-1].loaded=-i*i*i*0.5;

        }
    }

   

    ////////////////////////////////////
    

    setup.floorEnabled=false;
    setup.gravityEnabled=false;
    setup.staticSimulation=true;
    setup.poisson=false;
    setup.scale=1;
    setup.linear=true;  
    setup.globalDamping=0.2;


    

    saveJSON()


});
