// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

///////////call from node app.js code///////////////
// wait for the rhino3dm web assembly to load asynchronously
// rhino3dm().then(function(m) {// todo change call to app.js
// 	_rhino3dm = m; // global
// 	initRhino3dm(1,10,createVoxel,true,true);
// 	saveJSON();
// });
/////////////////////////////////////

var three; //todo change location
var three1; //todo change location
var setup=JSON.parse(JSON.stringify(setupEmpty));
var setup1=JSON.parse(JSON.stringify(setupEmpty));
var globalSetup={
    exaggeration:0.5,
    speed:3.0,
    updateStress:false

};
// var ex=gui.add(globalSetup, 'exaggeration', 0, 10e4).listen();
// var sp=gui.add(globalSetup, 'speed', 0, 5).listen();
// var sp=gui.add(globalSetup, 'updateStress').listen();

var static=true;

var latticeSize=15;
var voxelSize=5.0;

// ex.onChange(updateExSpeed);
// sp.onChange();


rhino3dm().then(async m => {
    console.log('Loaded rhino3dm.');

    // document.getElementById("footer2").innerHTML = "Loaded rhino3dm.";
    _rhino3dm = m; // global

    var material={
		area:2.38*2.38,
		density:0.028,
		stiffness:2000
    };
    
    var material2={
		area:2.38*2.38,
		density:0.028,
		stiffness:2000
	};

    setup.hierarchical=false;
    setup.voxelSize=voxelSize;

    const position=new THREE.Vector3(0,0,0);
    const position2=new THREE.Vector3(voxelSize*2,0,0);

    ///
    //try bounding box conditions
    var support=new _rhino3dm.BoundingBox([-voxelSize/2+position.x,-voxelSize/2+position.y,-voxelSize/2+position.z], [voxelSize/2+position.x,voxelSize/2+position.y,voxelSize/2+position.z]);
    
    var temp=voxelSize*5-voxelSize;
    
    var load=new _rhino3dm.BoundingBox([-voxelSize/2+position.x,-voxelSize/2+position.y,temp-voxelSize/2+position.z], [voxelSize/2+position.x,voxelSize/2+position.y,temp+voxelSize/2+position.z]);

    var load=new _rhino3dm.BoundingBox([-voxelSize/2+position.x,
        -voxelSize/2+position.y+voxelSize/4,
        temp+position.z+voxelSize/4], 
        [voxelSize/2+position.x-voxelSize/4,
            voxelSize/2+position.y-voxelSize/4,
            temp+voxelSize/2+position.z]);

    var load_1=new _rhino3dm.BoundingBox([voxelSize/2+position.x,
                -voxelSize/2+position.y+voxelSize/4,
                temp+position.z+voxelSize/4], 
                [voxelSize/2+position.x+voxelSize/4,
                    voxelSize/2+position.y-voxelSize/4,
                    temp+voxelSize/2+position.z]);

    var temp1=voxelSize*10-voxelSize;
    var load1=new _rhino3dm.BoundingBox([voxelSize/2+position.x,
        -voxelSize/2+position.y+voxelSize/4,
        temp1+position.z+voxelSize/4], 
        [voxelSize/2+position.x+voxelSize/4,
            voxelSize/2+position.y-voxelSize/4,
            temp1+voxelSize/2+position.z]);

    var load1_1=new _rhino3dm.BoundingBox([-voxelSize/2+position.x,
            -voxelSize/2+position.y+voxelSize/4,
            temp1+position.z+voxelSize/4], 
            [voxelSize/2+position.x-voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp1+voxelSize/2+position.z]);

    var temp2=voxelSize*latticeSize-voxelSize;
    var load2=new _rhino3dm.BoundingBox([-voxelSize/2+position.x,
        -voxelSize/2+position.y+voxelSize/4,
        temp2+position.z+voxelSize/4], 
        [voxelSize/2+position.x-voxelSize/4,
            voxelSize/2+position.y-voxelSize/4,
            temp2+voxelSize/2+position.z]);

    var load2_1=new _rhino3dm.BoundingBox([voxelSize/2+position.x,
            -voxelSize/2+position.y+voxelSize/4,
            temp2+position.z+voxelSize/4], 
            [voxelSize/2+position.x+voxelSize/4,
                voxelSize/2+position.y-voxelSize/4,
                temp2+voxelSize/2+position.z]);
        

    var dof=[true,true,true,true,true,true];
    var supports=[[support,dof]];
    // var loads=[[load,{x:0.0,y:0,z:-0.1}],[load1,{x:0.0,y:0,z:-0.1}],[load2,{x:0.0,y:0,z:-0.1}]];
    var loads=[[load,{x:0.0,y:0,z:-1.0}],[load1,{x:0.0,y:0,z:-1.0}],[load2,{x:0.0,y:0,z:-1.0}],[load_1,{x:0.0,y:0,z:-2.0}],[load1_1,{x:0.0,y:0,z:-2.0}],[load2_1,{x:0.0,y:0,z:-2.0}]];
    // var loads=[[load,{x:0.0,y:0,z:-1.0}],[load_1,{x:0.0,y:0,z:-2.0}]];
    // var diffMaterialBox=[[matB,material2]];

    ///
    

    lateralLoadSetup(setup,position,latticeSize,voxelSize,createVoxel,supports,loads,material);
    // changeMaterialFromBox(setup,diffMaterialBox);


    // setup.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
    setup.viz.minStress=10e6;
    setup.viz.maxStress=-10e6;
    setup.viz.exaggeration=0.5;

    // setup.viz.exaggeration=globalSetup.exaggeration;
    // setup.solve=solveParallel;
    // setup.numTimeSteps=100;
    // setup.supports=supports;
    // setup.loads=loads;

   
    // three= new threejs(setup,"webgl1","graph",static);
    // three.init();
    // three.drawConstraintBoundingBoxes([support],[load]);
   

    ////////////////////////////////////

    setup1.hierarchical=true;
    setup1.voxelSize=voxelSize;

    

    lateralLoadSetup(setup1,position,latticeSize,voxelSize,createHieraricalVoxel,supports,loads,material);
    // changeMaterialFromBox(setup1,diffMaterialBox);




    setup1.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
    setup1.viz.minStress=10e6;
    setup1.viz.maxStress=-10e6;

    // setup1.viz.exaggeration=globalSetup.exaggeration;
    setup1.solve=solveParallel;
    setup1.supports=supports;
    setup1.loads=loads;

    setup.floorEnabled=true;
    setup.gravityEnabled=true;
    setup.staticSimulation=false;
    setup.poisson=false;
    setup.scale=1;
    setup.linear=true;
    setup.globalDamping=0.2;

    saveJSON()
    

    // three1= new threejs(setup1,"webgl","threejs",static);
    // three1.init();

    // gui.add(three.setup,"numTimeSteps", 0, 500).listen();
    // gui.add(three.setup,"solve");
    
    
    
    // document.getElementById("footer2").innerHTML = "Press solve for structure simulation.";

    // more stuff
});

function solveParallel(){
    document.getElementById("footer2").innerHTML = "Running...";
    var numTimeSteps=250;
    /////////////////////////////////////////////////////
    

    var dt=0.0251646; //?? todo change to recommended step
    var dt=0.01; //?? todo change to recommended step
    

    setTimeout(function() { 
        simulateParallel( setup,three.setup.numTimeSteps,dt,static,2); 
        simulateParallel(setup1,three.setup.numTimeSteps,dt,static,2);
    }, 1);

    // var dt=0.0251646; //?? todo change to recommended step
    // simulateParallel(setup1,numTimeSteps,dt);

    ///////////////////////////////////////
    // updateColors();
    // three.animate();
    // three1.animate();
    
    /////////////////////
    

}

function updateExSpeed(){
    three.setup.animation.exaggeration=globalSetup.exaggeration;
    three1.setup.animation.exaggeration=globalSetup.exaggeration;
    three.setup.animation.speed=globalSetup.speed;
    three1.setup.animation.speed=globalSetup.speed;

}

function getForce(x,t){
    var a0=10000.0;
    var l=1.0 ;
    var f=2.0;
    var amp=0.1;
    var x=parseInt(x/voxelSize)/10.0;
    var t=t/100.0;
    var ax=1-0.825*(x-1)+1.0625*(x*x-1);
    return a0* ax *Math.sin(2*Math.PI*(x/l-f*t+amp));

}
