// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

///////////call from node app.js code///////////////
// wait for the rhino3dm web assembly to load asynchronously
// rhino3dm().then(function(m) {// todo change call to app.js
// 	_rhino3dm = m; // global
// 	initRhino3dm(1,10,createVoxel,true,true);
// 	saveJSON();
// });
/////////////////////////////////////

var setup=JSON.parse(JSON.stringify(setupEmpty));

var latticeSize=5;
var voxelSize=0.001;


rhino3dm().then(async m => {
    console.log('Loaded rhino3dm.');

    // document.getElementById("footer2").innerHTML = "Loaded rhino3dm.";
    _rhino3dm = m; // global

    var material={
      area:voxelSize*voxelSize,
      density:1e3,
      stiffness:1e6,
      poissonRatio:0.3
    };


    setup.hierarchical=false;
    setup.voxelSize=voxelSize;

    const position=new THREE.Vector3(0,0,0);
    const position1=new THREE.Vector3(1*voxelSize/15,0,0);
    const position2=new THREE.Vector3(2*voxelSize/15,0,0);

    const force=new THREE.Vector3(0,0,0);
    const force1=new THREE.Vector3(1e-3,0.0,0.0);


    addNode(setup,position,  true, force);
    addNode(setup,position1,false, force);
    addNode(setup,position2,false, force1);
    addEdge(setup,0, 1, material);
    addEdge(setup,1, 2, material);


    ////////////////////////////////////
    
    setup.floorEnabled=false;
    setup.gravityEnabled=false;
    setup.staticSimulation=true;
    setup.poisson=true; 
    setup.globalDamping=0.3;
    setup.scale=1e5;
    setup.linear=true;

    saveJSON();


});
