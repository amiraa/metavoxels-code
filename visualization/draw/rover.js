// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

///////////call from node app.js code///////////////
// wait for the rhino3dm web assembly to load asynchronously
// rhino3dm().then(function(m) {// todo change call to app.js
// 	_rhino3dm = m; // global
// 	initRhino3dm(1,10,createVoxel,true,true);
// 	saveJSON();
// });
/////////////////////////////////////

var three; //todo change location
var setup=JSON.parse(JSON.stringify(setupEmpty));

var globalSetup={
    exaggeration:0.5,
    speed:3.0,
    updateStress:false

};
// var ex=gui.add(globalSetup, 'exaggeration', 0, 10e4).listen();
// var sp=gui.add(globalSetup, 'speed', 0, 5).listen();
// var sp=gui.add(globalSetup, 'updateStress').listen();

var static=true;

var latticeSize=15;
var voxelSize=5.0;

rhino3dm().then(async m => {
    console.log('Loaded rhino3dm.');

    // document.getElementById("footer2").innerHTML = "Loaded rhino3dm.";
    _rhino3dm = m; // global

    var material={
		area:2.38*2.38,
		density:0.028,
		stiffness:2000
    };

    var material1={
		area:2.38*2.38,
		density:0.028,
		stiffness:500
    };
    

    setup.hierarchical=false;
    setup.voxelSize=voxelSize;

    const position=new THREE.Vector3(0,0,0);
    const position2=new THREE.Vector3(voxelSize*2,0,0);

    var voxelList=[
        [0,0,0,material],
        [0,0,1,material],
        [0,0,2,material],
        [0,0,3,material],
        [0,0,4,material],

        [-1,0,0,material1],
        [-2,0,0,material1],
        [-3,0,0,material1],

        [1,0,0,material1],
        [2,0,0,material1],
        [3,0,0,material1],

        [-1,0,4,material1],
        [-2,0,4,material1],
        [-3,0,4,material1],

        [1,0,4,material1],
        [2,0,4,material1],
        [3,0,4,material1]
        ];

    for (var count=0;count<voxelList.length;count++){
        const i=voxelList[count][0];
        const j=voxelList[count][1];
        const k=voxelList[count][2];
        const m=voxelList[count][3];
        createVoxel(setup,voxelSize,new THREE.Vector3(i*voxelSize+position.x,j*voxelSize+position.y,k*voxelSize+position.z),m);
    }

    var support=new _rhino3dm.BoundingBox(
        [
            -voxelSize/2+position.x,
            -voxelSize/2+position.y,
            -voxelSize/2+position.z
        ], 
        [
            voxelSize/2+position.x,
            voxelSize/2+position.y,
            voxelSize/2+position.z+voxelSize*5.0
        ]);

    var load=new _rhino3dm.BoundingBox(
        [
            -voxelSize/4+position.x+voxelSize*1.0,
            -voxelSize/2+position.y,
            -voxelSize/2+position.z
        ], 
        [
            voxelSize/4+position.x+voxelSize*1.0,
            voxelSize/2+position.y,
            voxelSize/2+position.z+voxelSize*5.0
        ]);
        
    var load1=new _rhino3dm.BoundingBox(
        [
            -voxelSize/4+position.x-voxelSize*1.0,
            -voxelSize/2+position.y,
            -voxelSize/2+position.z
        ], 
        [
            voxelSize/4+position.x-voxelSize*1.0,
            voxelSize/2+position.y,
            voxelSize/2+position.z+voxelSize*5.0
        ]);


    var loads=[[load,{x:0.0,y:0,z:1.0}],[load1,{x:0.0,y:0,z:2.0}]];

    var dof=[true,true,true,true,true,true];
    var supports=[[support,dof]];

    // restrainFromBox(setup,supports);
	loadFromBox(setup,loads);


    // setup.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
    setup.viz.minStress=10e6;
    setup.viz.maxStress=-10e6;
    setup.viz.exaggeration=0.5;

    // setup.viz.exaggeration=globalSetup.exaggeration;
    // setup.solve=solveParallel;
    // setup.numTimeSteps=100;

    // setup.supports=[
    //     [{
    //         min:[
    //             support.min[0],
    //             support.min[1],
    //             support.min[2]
    //         ],
    //         max:[
    //             support.max[0],
    //             support.max[1],
    //             support.max[2]
    //         ]
    //     }]
    // ];
    setup.loads=[
        [{
            min:[
                load.min[0],
                load.min[1],
                load.min[2]
            ],
            max:[
                load.max[0],
                load.max[1],
                load.max[2]
            ]
        }],
        [{
            min:[
                load1.min[0],
                load1.min[1],
                load1.min[2]
            ],
            max:[
                load1.max[0],
                load1.max[1],
                load1.max[2]
            ]
        }]
    ];

    setup.floorEnabled=true;
    setup.gravityEnabled=true;
    setup.staticSimulation=false;
    setup.poisson=false;
    setup.scale=1;
    setup.linear=true;
    setup.globalDamping=0.2;

   
    // three= new threejs(setup,"webgl1","graph",static);
    // three.init();
    // three.drawConstraintBoundingBoxes([support],[load]);
   
    ////////////////////////////////////

    saveJSON();
    
    // more stuff
});


function updateExSpeed(){
    three.setup.animation.exaggeration=globalSetup.exaggeration;
    three1.setup.animation.exaggeration=globalSetup.exaggeration;
    three.setup.animation.speed=globalSetup.speed;
    three1.setup.animation.speed=globalSetup.speed;
}
