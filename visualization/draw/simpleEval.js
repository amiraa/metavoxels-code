// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

///////////call from node app.js code///////////////
// wait for the rhino3dm web assembly to load asynchronously
// rhino3dm().then(function(m) {// todo change call to app.js
// 	_rhino3dm = m; // global
// 	initRhino3dm(1,10,createVoxel,true,true);
// 	saveJSON();
// });
/////////////////////////////////////

var setup=JSON.parse(JSON.stringify(setupEmpty));

var latticeSize=5;
var voxelSize=0.001;


rhino3dm().then(async m => {
    console.log('Loaded rhino3dm.');

    // document.getElementById("footer2").innerHTML = "Loaded rhino3dm.";
    _rhino3dm = m; // global

    var material={
		area:voxelSize*voxelSize,
		density:1e3,
		stiffness:1e6,
    nu:0.0
    };

    setup.hierarchical=false;
    setup.voxelSize=voxelSize;

    const position=new THREE.Vector3(0,0,0);
    const position1=new THREE.Vector3(voxelSize/15,0,0);

    var i=50;
    const force=new THREE.Vector3(0,0,i*2e-3);

    addNode(setup,position, true, force);
    addNode(setup,position1,false, force);
    addEdge(setup,0, 1, material);


    ////////////////////////////////////

    setup.floorEnabled=false;
    setup.gravityEnabled=false;
    setup.staticSimulation=true; 
    setup.poisson=false;
    setup.scale=1e5;
    setup.linear=true;
    setup.globalDamping=0.2;


    saveJSON();


});
