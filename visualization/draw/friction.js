
// Amira Abdel-Rahman
// (c) Massachusetts Institute of Technology 2020

///////////call from node app.js code///////////////
// wait for the rhino3dm web assembly to load asynchronously
// rhino3dm().then(function(m) {// todo change call to app.js
// 	_rhino3dm = m; // global
// 	initRhino3dm(1,10,createVoxel,true,true);
// 	saveJSON();
// });
/////////////////////////////////////

var three; //todo change location
var three1; //todo change location
var setup=JSON.parse(JSON.stringify(setupEmpty));
var setup1=JSON.parse(JSON.stringify(setupEmpty));
var globalSetup={
    exaggeration:1,
    speed:3.0,
    updateStress:false

};

var ex=gui.add(globalSetup, 'exaggeration', 0, 10e4).listen();
var sp=gui.add(globalSetup, 'speed', 0, 5).listen();
var sp=gui.add(globalSetup, 'updateStress').listen();

var static=false;

var latticeSize=5;
var voxelSize=5;

ex.onChange(updateExSpeed);
sp.onChange();


rhino3dm().then(async m => {
    console.log('Loaded rhino3dm.');

    document.getElementById("footer2").innerHTML = "Loaded rhino3dm.";
    _rhino3dm = m; // global

    var material={
		area:1.0,
		density:0.028,
		stiffness:10000000
    };
    
    var material2={
		area:1.0,
		density:0.028,
		stiffness:1000000
	};

    setup.hierarchical=false;
    setup.voxelSize=voxelSize;

    const position=new THREE.Vector3(0,0,0);
    const position2=new THREE.Vector3(0,-voxelSize/2.0,0);

    ///
    //try bounding box conditions
    var support=new _rhino3dm.BoundingBox([-voxelSize/2+position.x,-voxelSize/2+position.y,-voxelSize/2+position.z], [voxelSize/2+position.x,voxelSize/2+position.y,voxelSize/2+position.z]);
    
    var temp=voxelSize*latticeSize-voxelSize;
    
    var load=new _rhino3dm.BoundingBox([-voxelSize/2+position.x,-voxelSize/2+position.y,0-voxelSize/2+position.z], [voxelSize/2+position.x,voxelSize/2+position.y,temp+voxelSize/2+position.z]);

    

    // var load=new _rhino3dm.BoundingBox([-voxelSize/2+position.x,
    //     -voxelSize/2+position.y+voxelSize/4,
    //     temp+position.z+voxelSize/4], 
    //     [voxelSize/2+position.x-voxelSize/4,
    //         voxelSize/2+position.y-voxelSize/4,
    //         temp+voxelSize/2+position.z]);

    var temp1=voxelSize*latticeSize-3*voxelSize;

    var matB=new _rhino3dm.BoundingBox([-voxelSize/2+position2.x,-voxelSize/2+position2.y,temp1-voxelSize/2+position2.z], [voxelSize/2+position2.x,voxelSize/2+position2.y,temp]);

    var dof=[true,true,true,true,true,true];
    var supports=[[support,dof]];
    var supports=[];
    var loads=[[load,{x:0,y:0,z:-400}]];
    var diffMaterialBox=[[matB,material2]];

    ///
    

    lateralLoadSetup(setup,position,latticeSize,voxelSize,createVoxel,supports,loads,material);
    // changeMaterialFromBox(setup,diffMaterialBox);


    setup.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
    setup.viz.minStress=10e6;
    setup.viz.maxStress=-10e6;

    setup.viz.exaggeration=globalSetup.exaggeration;
    setup.solve=solveParallel;
    setup.numTimeSteps=100;
    setup.supports=supports;
    setup.loads=loads;

   
    three= new threejs(setup,"webgl1","graph",static);
    three.init();
    three.drawConstraintBoundingBoxes([support],[load]);
   

    ////////////////////////////////////

    setup1.hierarchical=true;
    setup1.voxelSize=voxelSize;

    

    lateralLoadSetup(setup1,position2,latticeSize,voxelSize,createHieraricalVoxel,supports,loads,material);
    // changeMaterialFromBox(setup1,diffMaterialBox);

    setup1.viz.colorMaps=[YlGnBu,coolwarm, winter ,jet];
    setup1.viz.minStress=10e6;
    setup1.viz.maxStress=-10e6;

    setup1.viz.exaggeration=globalSetup.exaggeration;
    setup1.solve=solveParallel;
    setup1.supports=supports;
    setup1.loads=loads;

    

    three1= new threejs(setup1,"webgl","threejs",static);
    three1.init();

    gui.add(three.setup,"numTimeSteps", 0, 500).listen();
    gui.add(three.setup,"solve");
    
    
    
    document.getElementById("footer2").innerHTML = "Press solve for structure simulation.";

    

    // more stuff
});

function solveParallel(){
    document.getElementById("footer2").innerHTML = "Running...";
    var numTimeSteps=250;
    /////////////////////////////////////////////////////
    

    var dt=0.0251646; //?? todo change to recommended step
    var dt=0.01; //?? todo change to recommended step
    

    setTimeout(function() { 
        simulateParallel( setup,three.setup.numTimeSteps,dt,static,2); 
        simulateParallel(setup1,three.setup.numTimeSteps,dt,static,2);
    }, 1);

    // var dt=0.0251646; //?? todo change to recommended step
    // simulateParallel(setup1,numTimeSteps,dt);

    ///////////////////////////////////////
    // updateColors();
    // three.animate();
    // three1.animate();
    
    /////////////////////

}

function updateExSpeed(){
    three.setup.animation.exaggeration=globalSetup.exaggeration;
    three1.setup.animation.exaggeration=globalSetup.exaggeration;
    three.setup.animation.speed=globalSetup.speed;
    three1.setup.animation.speed=globalSetup.speed;

}

function getForceOld(x,t){
    var a0=10000.0;
    var l=1.0 ;
    var f=2.0;
    var amp=0.1;
    var x=parseInt(x/voxelSize)/10.0;
    var t=t/100.0;
    var ax=1-0.825*(x-1)+1.0625*(x*x-1);
    return a0* ax *Math.sin(2*Math.PI*(x/l-f*t+amp));

}

function getForce(currPos,t){
    var a0=10000.0;
    var l=1.0 ;
    var f=2.0;
    var amp=0.1;
    var x=parseInt(currPos.z/voxelSize)/10.0;
    var t=t/100.0;
    var ax=1-0.825*(x-1)+1.0625*(x*x-1);
    var temp=a0* ax *Math.sin(2*Math.PI*(x/l-f*t+amp));
    var curForce=new THREE.Vector3(temp*10000.0, 0.0,0.0);

    return curForce;

}



// function getForce(currPos,t){
//     // var a0=10000.0;
//     // var l=1.0 ;
//     // var f=2.0;
//     // var amp=0.1;
//     // var x=parseInt(x/voxelSize)/10.0;
//     // var t=t/100.0;
//     // var ax=1-0.825*(x-1)+1.0625*(x*x-1);
//     // return a0* ax *Math.sin(2*Math.PI*(x/l-f*t+amp));

//     //get
//     var f=1000;
//     var vv=new THREE.Vector3(currPos.x , currPos.y,currPos.z );
//     var v=vv.normalize();
//     if(t==0){
//         v=new THREE.Vector3(1 , 0,1);
//     }
//     var n=new THREE.Vector3(1 , 0,1);
//     return n.multiplyScalar(-f);
//     return v.multiplyScalar(-f);

// }
