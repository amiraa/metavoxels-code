var setup=
{
	nodes: [
		{
			id: 'n0', 
			parent: '11',
			degrees_of_freedom:[0,1,2,3,4,5] ,
			restrained_degrees_of_freedom:[true,true,true,true,true,true],
			position: { x: 0, y: 0,z:0 },
			currPosition:new THREE.Vector3(0,0,0),
			force:{ x: 0, y: 0,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },
			
		},
		{
			id: 'n1', 
			parent: '11',
			degrees_of_freedom:[6,7,8,9,10,11]  ,
			restrained_degrees_of_freedom:[false,false,false,false,false,false],
			position: { x: 0, y: 5,z:0  },
			currPosition:new THREE.Vector3(0,5,0),
			force:{ x: 0, y: 0,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },

		},
		{
			id: 'n2',
			parent: '11',
			degrees_of_freedom:[12,13,14,15,16,17,18]  ,
			restrained_degrees_of_freedom:[true,true,true,true,true,true],
			position: { x: 5, y: 0,z:0  },
			currPosition:new THREE.Vector3(5,0,0),
			force:{ x: 0, y: 0,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },

		},
		{
			id: 'n3',
			parent: '11',
			degrees_of_freedom:[12,13,14,15,16,17,18]  ,
			restrained_degrees_of_freedom:[false,false,false,false,false,false],
			position: { x: 5, y: 5,z:0  },
			currPosition:new THREE.Vector3(5,5,0),
			force:{ x: 0, y: -0,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },

		},
		{
			id: 'n4',
			parent: '11',
			degrees_of_freedom:[12,13,14,15,16,17,18]  ,
			restrained_degrees_of_freedom:[true,true,true,true,true,true],
			position: { x: 0, y: 0,z:5  },
			currPosition:new THREE.Vector3(0,0,5),
			force:{ x: 0, y: 0,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },

		},
		{
			id: 'n5',
			parent: '11',
			degrees_of_freedom:[12,13,14,15,16,17,18]  ,
			restrained_degrees_of_freedom:[false,false,false,false,false,false],
			position: { x: 0, y: 5,z:5  },
			currPosition:new THREE.Vector3(0,5,5),
			force:{ x: 0, y: -0,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },

		},
		{
			id: 'n6',
			parent: '11',
			degrees_of_freedom:[12,13,14,15,16,17,18]  ,
			restrained_degrees_of_freedom:[true,true,true,true,true,true],
			position: { x: 5, y: 0,z:5  },
			currPosition:new THREE.Vector3(5,0,5),
			force:{ x: 0, y: 0,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },

		},
		{
			id: 'n7',
			parent: '11',
			degrees_of_freedom:[12,13,14,15,16,17,18]  ,
			restrained_degrees_of_freedom:[false,false,false,false,false,false],
			position: { x: 5, y: 5,z:5  },
			currPosition:new THREE.Vector3(5,5,5),
			force:{ x: 0, y: -0,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },

		},
		{
			id: 'n8',
			parent: '11',
			degrees_of_freedom:[12,13,14,15,16,17,18]  ,
			restrained_degrees_of_freedom:[false,false,false,false,false,false],
			position: { x: 5, y: 10,z:5  },
			currPosition:new THREE.Vector3(5,10,5),
			force:{ x: 0, y: -0,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },

		},
		{
			id: 'n9',
			parent: '11',
			degrees_of_freedom:[12,13,14,15,16,17,18]  ,
			restrained_degrees_of_freedom:[false,false,false,false,false,false],
			position: { x: 0, y: 10,z:5  },
			currPosition:new THREE.Vector3(0,10,5),
			force:{ x: 0, y: -0,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },

		},
		{
			id: 'n10',
			parent: '11',
			degrees_of_freedom:[12,13,14,15,16,17,18]  ,
			restrained_degrees_of_freedom:[false,false,false,false,false,false],
			position: { x: 5, y: 10,z:0  },
			currPosition:new THREE.Vector3(5,10,0),
			force:{ x: 0, y: 0,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },

		},
		{
			id: 'n11', 
			parent: '11',
			degrees_of_freedom:[6,7,8,9,10,11]  ,
			restrained_degrees_of_freedom:[false,false,false,false,false,false],
			position: { x: 0, y: 10,z:0  },
			currPosition:new THREE.Vector3(0,10,0),
			force:{ x: -0, y: -1000,z:0 },
			displacement: { x: 0, y: 0,z:0 },
			angle: { x: 0, y: 0,z:0 },
			orient: new THREE.Quaternion(),
			linMom:new THREE.Vector3(0,0,0),
			angMom:new THREE.Vector3(0,0,0),
			intForce:new THREE.Vector3(0,0,0),
			intMoment:new THREE.Vector3(0,0,0),
			moment:{ x: 0, y: 0,z:0 },

		}
	],
	
	edges: [
        { 
            id: 'e0', source: 0, target: 1 ,area:1.0,density:1000,stiffness:1000000,stress:0,
            currentRestLength:0,
            axis:Y_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e1', source: 1, target: 3 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:X_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e2', source: 2, target: 3 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Y_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e3', source: 0, target: 2 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:X_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e4', source: 4, target: 5 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Y_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		
		{ 
            id: 'e5', source: 4, target: 6 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:X_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e6', source: 0, target: 4 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Z_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		
		{ 
            id: 'e7', source: 2, target: 6 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Z_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e8', source: 1, target: 5 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Z_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e9', source: 5, target: 7 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:X_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e10', source: 6, target: 7 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Y_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e11', source: 3, target: 7 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Z_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e12', source: 7, target: 8 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Y_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e13', source: 5, target: 9 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Y_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e14', source: 9, target: 8 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:X_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e15', source: 3, target: 10 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Y_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e16', source: 10, target: 8 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Z_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e17', source: 1, target: 11 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Y_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e18', source: 11, target: 10 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:X_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

		},
		{ 
            id: 'e19', source: 11, target: 9 ,area:1.0,density:0.284,stiffness:30.0e6,stress:0,
            currentRestLength:0,
            axis:Z_AXIS,
            pos2: new THREE.Vector3(0,0,0),
            angle1v: new THREE.Vector3(0,0,0),
            angle2v: new THREE.Vector3(0,0,0),
            angle1:new THREE.Quaternion(),
            angle2:new THREE.Quaternion(),
            currentTransverseArea:0,
            currentTransverseStrainSum:0

        },
        
	],

	//material properties - AISI 1095 Carbon Steel (Spring Steel)
	ndofs   : 3*6,

	animation :  {
	
		showDisplacement : true,
		exaggeration : 10e1,
		speed:3.0
		
	},

	viz :  {
	
		
		minStress:-20,
		maxStress: 10,
		colorMaps:[YlGnBu, winter, coolwarm,jet],
		colorMap:0,
		
	},

	bar:false
	
};